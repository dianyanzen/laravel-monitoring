<?php

namespace App\Http\Controllers;

use DB;
use Session;
use DateTime;
use PDF;
use Illuminate\Http\Request;
use App\Http\Controllers\SharedController as Shr;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class SettingController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function user(Request $request) 
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 135;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$data = array(
		 		"stitle"=>'Setting User',
		 		"mtitle"=>'Setting User',
		 		"my_url"=>'User',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
    		);
			return view('User/main',$data);
			}else{
			return redirect()->route('logout');
		}
	}

    public function menu_master(Request $request) 
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 135;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$data = array(
		 		"stitle"=>'Menu Master',
		 		"mtitle"=>'Menu Master',
		 		"my_url"=>'User',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
    		);
			return view('menu_master/main',$data);
			}else{
			return redirect()->route('logout');
		}
	}

	public function user_level(Request $request) 
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 134;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$data = array(
		 		"stitle"=>'Setting User Level',
		 		"mtitle"=>'Setting User Level',
		 		"my_url"=>'UserLevel',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
    		);
			return view('User_level/main',$data);
			}else{
			return redirect()->route('logout');
		}
	}

	public function set_helpdesk(Request $request) 
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 178;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$data = array(
		 		"stitle"=>'Setting Helpdesk',
		 		"mtitle"=>'Setting Helpdesk',
		 		"my_url"=>'Helpdesk',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
    		);
			return view('User_helpdesk/main',$data);
			}else{
			return redirect()->route('logout');
		}
	}
	public function set_helpdesk_add(Request $request) 
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 178;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$data = array(
		 		"stitle"=>'Setting Helpdesk',
		 		"mtitle"=>'Setting Helpdesk',
		 		"my_url"=>'Helpdesk',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
    		);
			return view('User_helpdesk_add/main',$data);
			}else{
			return redirect()->route('logout');
		}
	}
		
	public function set_helpdesk_edit(Request $request, $id) 
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 178;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
		    $helpdesk_id = $id;
		    $c_head = $this->sett_get_helpdesk($helpdesk_id);
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$data = array(
		 		"stitle"=>'Setting Helpdesk',
		 		"mtitle"=>'Setting Helpdesk',
		 		"my_url"=>'Helpdesk',
		 		"type_tgl"=>'Tanggal',
		 		"chead"=>$c_head,
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
    		);
			return view('User_helpdesk_edit/main',$data);
			}else{
			return redirect()->route('logout');
		}
	}
		
		
	public function set_helpdesk_delete(Request $request, $id) 
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 178;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
		    $helpdesk_id = $id;
		    $c_head = $this->sett_get_helpdesk($helpdesk_id);
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$data = array(
		 		"stitle"=>'Setting Helpdesk',
		 		"mtitle"=>'Setting Helpdesk',
		 		"my_url"=>'Helpdesk',
		 		"type_tgl"=>'Tanggal',
		 		"chead"=>$c_head,
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
    		);
			return view('User_helpdesk_delete/main',$data);
			}else{
			return redirect()->route('logout');
		}
	}
	public function set_helpdesk_see(Request $request, $id) 
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 178;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
		    $helpdesk_id = $id;
		    $c_head = $this->sett_get_helpdesk($helpdesk_id);
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$data = array(
		 		"stitle"=>'Setting Helpdesk',
		 		"mtitle"=>'Setting Helpdesk',
		 		"my_url"=>'Helpdesk',
		 		"type_tgl"=>'Tanggal',
		 		"chead"=>$c_head,
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
    		);
			return view('User_helpdesk_lihat/main',$data);
			}else{
			return redirect()->route('logout');
		}
	}


	public function menu_user(Request $request) 
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 168;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$data = array(
		 		"stitle"=>'Setting User Menu',
		 		"mtitle"=>'Setting User Menu',
		 		"my_url"=>'MenuUser',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
    		);
			return view('User_menu/main',$data);
			}else{
			return redirect()->route('logout');
		}
	}

	public function see_menu_user(Request $request, $id) 
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 168;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
		    if($id != null){
		    $menu_id = $id;
		    $c_head = $this->sett_detail_menu_settting($menu_id);
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$data = array(
		 		"stitle"=>'Select Menu',
		 		"mtitle"=>'Select Menu',
		 		"my_url"=>'UserLevel',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"chead"=>$c_head,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
    		);
			return view('user_menu_lihat/main',$data);
			}else{
				return redirect()->route('logout');
			}
			}else{
			return redirect()->route('logout');
		}
	}
	public function edit_menu_user(Request $request, $id) 
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 168;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
		    if($id != null){
		    $menu_id = $id;
		    $c_head = $this->sett_detail_menu_settting($menu_id);
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$data = array(
		 		"stitle"=>'Edit Menu',
		 		"mtitle"=>'Edit Menu',
		 		"my_url"=>'UserLevel',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"chead"=>$c_head,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
    		);
			return view('user_menu_edit/main',$data);
			}else{
				return redirect()->route('logout');
			}
			}else{
			return redirect()->route('logout');
		}
	}
	public function user_activity(Request $request) 
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 162;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$data = array(
		 		"stitle"=>'Setting Activity',
		 		"mtitle"=>'Setting Activity',
		 		"my_url"=>'activity_id',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
    		);
			return view('User_activity/main',$data);
			}else{
			return redirect()->route('logout');
		}
	}

	public function activity_add(Request $request) 
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 162;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$data = array(
		 		"stitle"=>'Tambah Activity',
		 		"mtitle"=>'Tambah Activity',
		 		"my_url"=>'activity_add',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
    		);
			return view('user_activity_add/main',$data);
			}else{
			return redirect()->route('logout');
		}
	}

	public function edit_activity(Request $request, $id) 
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 162;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
		    if($id != null){
		    $activity_id = $id;
		    $c_head = $this->sett_get_activity_data($activity_id);
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$data = array(
		 		"stitle"=>'Edit Activity',
		 		"mtitle"=>'Edit Activity',
		 		"my_url"=>'UserLevel',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"chead"=>$c_head,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
    		);
			return view('user_activity_edit/main',$data);
			}else{
				return redirect()->route('logout');
			}
			}else{
			return redirect()->route('logout');
		}
	}
	public function user_add(Request $request) 
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 134;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$data = array(
		 		"stitle"=>'Tambah User Level',
		 		"mtitle"=>'Tambah User Level',
		 		"my_url"=>'UserLevel',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
    		);
			return view('User_level_add/main',$data);
			}else{
			return redirect()->route('logout');
		}
	}
	public function userid_add(Request $request) 
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 135;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
				$no_prop = Shr::do_get_no_prop();
				$no_kab = Shr::do_get_no_kab();
				$data = array(
			 		"stitle"=>'Add User Id',
			 		"mtitle"=>'Add User Id',
			 		"my_url"=>'Add',
			 		"type_tgl"=>'Tanggal',
			 		"menu"=>$menu,
       				"akses_kec"=>$isakses_kec,
       				"akses_kel"=>$isakses_kel,
			 		"no_prop"=>$no_prop,
			 		"no_kab"=>$no_kab,
			 		"user_id"=>$request->session()->get('S_USER_ID'),
			 		"user_nik"=>$request->session()->get('S_NIK'),
			 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
			 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
			 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
			 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
			 		"user_level"=>$request->session()->get('S_USER_LEVEL')
	    		);
				return view('Userid_add/main',$data);
				}else{
			return redirect()->route('logout');
		}
	}
	public function seting_app(Request $request) 
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 135;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
				$no_prop = Shr::do_get_no_prop();
				$no_kab = Shr::do_get_no_kab();
				$data_master = [];
				$data_master['no_prop'] =$no_prop;
				$data_master['no_kab'] =$no_kab;
				$data_master['nm_prop'] =Shr::do_get_nm_prop();
				$data_master['nm_kab'] =Shr::do_get_nm_kab();
				$data_master['siak_dblink'] =Shr::do_get_siak_dblink();
				$data_master['cetak_dblink'] =Shr::do_get_cetak_dblink();
				$data_master['rekam_dblink'] =Shr::do_get_rekam_dblink();
				$data_master['master_dblink'] =Shr::do_get_master_dblink();
				$data_master['default_pass'] =Shr::do_get_default_pass();
				$data_master['dkb_bio'] =Shr::do_get_dkb_bio();
				$data_master['dkb_kk'] =Shr::do_get_dkb_keluarga();
				$data_master['dkb_cutoff'] =Shr::do_get_cut_off_date();
				$data_master['dkb_tahun'] =Shr::do_get_dkb();
				$data_master['give_kec'] =Shr::get_give_kec();
				$data_master['give_kel'] =Shr::get_give_kel();
				$data = array(
			 		"stitle"=>'Add User Id',
			 		"mtitle"=>'Add User Id',
			 		"my_url"=>'Add',
			 		"type_tgl"=>'Tanggal',
			 		"menu"=>$menu,
       				"akses_kec"=>$isakses_kec,
       				"akses_kel"=>$isakses_kel,
			 		"data"=>$data_master,
			 		"no_prop"=>$no_prop,
			 		"no_kab"=>$no_kab,
			 		"user_id"=>$request->session()->get('S_USER_ID'),
			 		"user_nik"=>$request->session()->get('S_NIK'),
			 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
			 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
			 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
			 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
			 		"user_level"=>$request->session()->get('S_USER_LEVEL')
	    		);
				return view('Setting_app/main',$data);
				}else{
			return redirect()->route('logout');
		}
	}
	public function seting_pejabat(Request $request) 
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 164;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
				$data = array(
			 		"stitle"=>'List Pejabat',
			 		"mtitle"=>'List Pejabat',
			 		"my_url"=>'Add',
			 		"type_tgl"=>'Tanggal',
			 		"menu"=>$menu,
       				"akses_kec"=>$isakses_kec,
       				"akses_kel"=>$isakses_kel,
			 		"user_id"=>$request->session()->get('S_USER_ID'),
			 		"user_nik"=>$request->session()->get('S_NIK'),
			 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
			 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
			 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
			 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
			 		"user_level"=>$request->session()->get('S_USER_LEVEL')
	    		);
				return view('User_pejabat/main',$data);
				}else{
			return redirect()->route('logout');
		}
	}

	public function add_pejabat(Request $request) 
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 164;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$data = array(
		 		"stitle"=>'Tambah Pejabat',
		 		"mtitle"=>'Tambah Pejabat',
		 		"my_url"=>'UserLevel',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
    		);
			return view('User_pejabat_add/main',$data);
			}else{
			return redirect()->route('logout');
		}

	}public function edit_pejabat(Request $request, $id) 
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 164;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$datpeg = $this->sett_edit_pejabat_list($id);
			$data = array(
		 		"stitle"=>'Edit Pejabat',
		 		"mtitle"=>'Edit Pejabat',
		 		"my_url"=>'UserLevel',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"data"=>$datpeg,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
    		);
			return view('user_pejabat_edit/main',$data);
			}else{
			return redirect()->route('logout');
		}

	}
	public function menu_settting(Request $request)
     {
          $draw = intval($request->draw);
          $start = intval($request->start);
          $length = intval($request->length);
          $group_id = $request->group_id;
          $rekap_menu = $this->sett_menu_settting($group_id);
          $data = array();
          $i = 0;
          foreach($rekap_menu as $r) {
          		$i ++;
               $data[] = array(
               		$i,
                    $r->menu_id,
                    $r->parent_id,
                    $r->parent_title,
                    $r->title,
                    $r->menu_level,
                    $r->level_head,
                    $r->level_sub,
                    '<button type="button" class="btn btn-info btn-xs" id="btn-lihat" onclick="lihat('.$r->menu_id.');" style="margin-top: 5px;">Select <i class="mdi  mdi-magnify fa-fw"></i></button> &nbsp <button type="button" class="btn btn-primary btn-xs" id="btn-edit" onclick="edit('.$r->menu_id.');" style="margin-top: 5px;">Edit <i class="mdi  mdi-tooltip-edit fa-fw"></i></button>'
                   
               );
          }

          $output = array(
               "draw" => $draw,
                 "recordsTotal" => count($rekap_menu),
                 "recordsFiltered" => count($rekap_menu),
                 "data" => $data
            );
          return $output;
          exit();
      }
	public function get_level(Request $request)
	{
		header("Content-Type: application/json", true);
		$r = $this->sett_get_level();
		return $r;
	}
	public function get_group(Request $request)
	{
		header("Content-Type: application/json", true);
		$r = $this->sett_get_group();
		return $r;
	}
	public function get_group_user(Request $request)
	{
		header("Content-Type: application/json", true);
		$r = $this->sett_get_group_user();
		return $r;
	}
	public function get_atasan(Request $request)
	{
		header("Content-Type: application/json", true);
		$r = $this->sett_get_atasan();
		return $r;
	}
	public function get_group_lvl(Request $request)
	{
		header("Content-Type: application/json", true);
		$lvl = $request->lvl;
		$r = $this->sett_get_group_lvl($lvl);
		return $r;
	}
	public function get_parent(Request $request)
	{
		header("Content-Type: application/json", true);
		$menu_level = $request->menu_level;
		$r = $this->sett_get_parent($menu_level);
		return $r;
	}
	public function get_child(Request $request)
	{
		header("Content-Type: application/json", true);
		$menu_id = $request->menu_id;
		$r = $this->sett_get_child($menu_id);
		$data["child"] = $r;
		return $data;
	}
	public function check_lvl(Request $request) 
	{
		if($request->lvl_cd != null){
			$lvl_cd = $request->lvl_cd;
			$j = $this->sett_check_lvl($lvl_cd);
			if($j[0]->jml > 0){
				$data["success"] = TRUE;
				$data["is_exists"] = 1;
				$data["lvl_name"] = ucfirst(strtolower($j[0]->level_name));
        		return $data;
			}else{
				$data["success"] = TRUE;
				$data["is_exists"] = 0;
        		return $data;
			}
		}else{
			return redirect()->route('logout');
		}
	}
	public function check_userid(Request $request) 
	{
		if($request->user_id != null){
			$user_id = $request->user_id;
			$j = $this->sett_check_jml_userid($user_id);
			if($j[0]->jml > 0){
				$j = $this->sett_check_userid($user_id);
				$data["success"] = TRUE;
				$data["is_exists"] = 1;
				$data["nama_lgkp"] = ucfirst(strtolower($j[0]->nama_lgkp));
        		return $data;
			}else{
				$data["success"] = TRUE;
				$data["is_exists"] = 0;
        		return $data;
			}
		}else{
			return redirect()->route('logout');
		}
	}
	public function save_pejabat(Request $request) 
	{
		if($request->nip != null){
			$nip = $request->nip;
			$nama = strtoupper($request->nama);
			$jabatan = strtoupper($request->jabatan);
			$struktural = $request->struktural;
			$j = $this->sett_max_pejabat();
			$this->sett_save_pejabat($j,$nip,$nama,$jabatan,$struktural);

			$data["success"] = TRUE;
			$data["message"] = "Data Berhasil Di Simpan";
       		return $data;
		}else{
			return redirect()->route('logout');
		}
	}
	public function edit_pejabat_id(Request $request) 
	{
		if($request->pejabat_id != null){
			$pejabat_id = $request->pejabat_id;
			$nip = $request->nip;
			$nama = strtoupper($request->nama);
			$jabatan = strtoupper($request->jabatan);
			$struktural = $request->struktural;
			$this->sett_edit_pejabat_id($pejabat_id,$nip,$nama,$jabatan,$struktural);

			$data["success"] = TRUE;
			$data["message"] = "Data Berhasil Di Update";
       		return $data;
		}else{
			return redirect()->route('logout');
		}
	}
	public function do_edit_menu_id(Request $request) 
	{
		if($request->menu_id != null){
			$menu_id = $request->menu_id;
			$title = $request->title;
			$parent_id = $request->parent_id;
			$menu_level = $request->menu_level;
			$level_head = $request->level_head;
			$level_sub = $request->level_sub;
			$this->sett_do_edit_menu_id($menu_id,$title,$parent_id,$menu_level,$level_head,$level_sub);

			$data["success"] = TRUE;
			$data["message"] = "Data Berhasil Di Update";
       		return $data;
		}else{
			return redirect()->route('logout');
		}
	}

	public function edit_menu_id(Request $request) 
	{
		if($request->menu_id != null){
			$menu_id = $request->menu_id;
			$title = $request->title;
			$parent_id = $request->parent_id;
			$parent_old = $request->parent_old;
			$menu_level = $request->menu_level;
			$level_head = $request->level_head;
			$level_sub = $request->level_sub;
			$this->sett_edit_parent_id($menu_id,$parent_id,$parent_old,$menu_level);
			$this->sett_edit_menu_id($menu_id,$title,$parent_id,$menu_level,$level_head,$level_sub);

			$data["success"] = TRUE;
			$data["message"] = "Data Berhasil Di Update";
       		return $data;
		}else{
			return redirect()->route('logout');
		}
	}
	public function save_lvl(Request $request) 
	{
		if($request->lvl_cd != null){
			$lvl_cd = $request->lvl_cd;
			$lvl_nm = strtoupper($request->lvl_nm);
			$kdgroup = $request->kdgroup;
			$this->sett_save_lvl($lvl_cd,$lvl_nm,$kdgroup,$request->session()->get('S_USER_ID'));
			$this->sett_save_akses($lvl_cd);

			$data["success"] = TRUE;
			$data["message"] = "Data Berhasil Di Simpan";
       		return $data;
		}else{
			return redirect()->route('logout');
		}
	}
	public function save_activity(Request $request) 
	{
		if($request->activity_nm != null){
			$activity_nm = strtoupper($request->activity_nm);
			$kdgroup = $request->kdgroup;
			$this->sett_save_activity($activity_nm,$kdgroup,$request->session()->get('S_USER_ID'));

			$data["success"] = TRUE;
			$data["message"] = "Data Berhasil Di Simpan";
       		return $data;
		}else{
			return redirect()->route('logout');
		}
	}
	public function update_lvl(Request $request) 
	{
		if($request->lvl_cd != null){
			$lvl_cd = $request->lvl_cd;
			$lvl_nm = strtoupper($request->lvl_nm);
			$this->sett_update_lvl($lvl_cd,$lvl_nm,$request->session()->get('S_USER_ID'));

			$data["success"] = TRUE;
			$data["message"] = "Data Berhasil Di Simpan";
       		return $data;
		}else{
			return redirect()->route('logout');
		}
	}
	public function update_activity(Request $request) 
	{
		if($request->activity_cd != null){
			$activity_cd = $request->activity_cd;
			$activity_nm = strtoupper($request->activity_nm);
			$this->sett_update_activity($activity_cd,$activity_nm);

			$data["success"] = TRUE;
			$data["message"] = "Data Berhasil Di Simpan";
       		return $data;
		}else{
			return redirect()->route('logout');
		}
	}
	public function edit_userid(Request $request) 
	{
		if($request->user_id != null){
			$user_id = $request->user_id;
			$nama_lgkp = strtoupper($request->nama_lgkp);
			$nama_dpn = strtoupper($request->nama_dpn);
			$nik = $request->nik;
			$tmpt_lhr = strtoupper($request->tmpt_lhr);
			$tgl_lhr = $request->tgl_lhr;
			$jenis_klmin = $request->jenis_klmin;
			$gol_drh = $request->gol_drh;
			$nama_kantor = strtoupper($request->nama_kantor);
			$alamat_kantor = strtoupper($request->alamat_kantor);
			$telp = $request->telp;
			$alamat_rumah = strtoupper($request->alamat_rumah);
			$kdgroup = $request->kdgroup;
			$kdlvl = $request->kdlvl;
			$kdprop = $request->kdprop;
			$kdkab = $request->kdkab;
			$kdkec = $request->kdkec;
			$kdkel = $request->kdkel;
			$no_rw = $request->no_rw;
			$no_rt = $request->no_rt;
			$user_siak = $request->user_siak;
			$user_bcard = $request->user_bcard;
			$user_benrol = $request->user_benrol;
			$is_monitoring = $request->is_monitoring;
			$is_gisa = $request->is_gisa;
			$is_absen = $request->is_absen;
			$is_asn = $request->is_asn;
			$absensi_checking = $request->absensi_checking;
			$is_active = $request->is_active;
			$ipaddress_check = $request->ipaddress_check;
			$is_show = $request->is_show;
			$is_atasan_satu = $request->is_atasan_satu;
			$is_atasan_dua = $request->is_atasan_dua;
			$this->sett_update_userid_data($user_id,$nama_lgkp,$nama_dpn,$nik,$tmpt_lhr,$tgl_lhr,$jenis_klmin,$gol_drh,$nama_kantor,$alamat_kantor,$telp,$alamat_rumah,$kdgroup,$kdlvl,$kdprop,$kdkab,$kdkec,$kdkel,$no_rw,$no_rt,$user_siak,$user_bcard,$user_benrol,$is_monitoring,$is_gisa,$is_absen,$is_asn,$absensi_checking,$is_active,$ipaddress_check,$is_show,$request->session()->get('S_USER_ID'));
			$this->sett_update_atasan($user_id,$is_atasan_satu,$is_atasan_dua);
			$data["success"] = TRUE;
			$data["message"] = "Data Berhasil Di Simpan";
       		return $data;
		}else{
			return redirect()->route('logout');
		}
	}
	public function edit_setting(Request $request) 
	{
		if($request->no_prop != null){
			$no_prop = $request->no_prop;
			Shr::do_update_no_prop($no_prop,$request->session()->get('S_USER_ID'));

			$no_kab = $request->no_kab;
			Shr::do_update_no_kab($no_kab,$request->session()->get('S_USER_ID'));

			$nm_prop = $request->nm_prop;
			Shr::do_update_nm_prop($nm_prop,$request->session()->get('S_USER_ID'));

			$nm_kab = $request->nm_kab;
			Shr::do_update_nm_kab($nm_kab,$request->session()->get('S_USER_ID'));

			$siak_dblink = $request->siak_dblink;
			Shr::do_update_siak_dblink($siak_dblink,$request->session()->get('S_USER_ID'));

			$cetak_dblink = $request->cetak_dblink;
			Shr::do_update_cetak_dblink($cetak_dblink,$request->session()->get('S_USER_ID'));

			$rekam_dblink = $request->rekam_dblink;
			Shr::do_update_rekam_dblink($rekam_dblink,$request->session()->get('S_USER_ID'));

			$master_dblink = $request->master_dblink;
			Shr::do_update_master_dblink($master_dblink,$request->session()->get('S_USER_ID'));

			$dkb_bio = $request->dkb_bio;
			Shr::do_update_dkb_bio($dkb_bio,$request->session()->get('S_USER_ID'));

			$dkb_kk = $request->dkb_kk;
			Shr::do_update_dkb_kk($dkb_kk,$request->session()->get('S_USER_ID'));

			$dkb_tahun = $request->dkb_tahun;
			Shr::do_update_dkb_tahun($dkb_tahun,$request->session()->get('S_USER_ID'));

			$dkb_cutoff = $request->dkb_cutoff;
			Shr::do_update_dkb_cutoff($dkb_cutoff,$request->session()->get('S_USER_ID'));

			$default_pass = $request->default_pass;
			Shr::do_update_default_pass($default_pass,$request->session()->get('S_USER_ID'));

			$give_kec = $request->give_kec;
			Shr::do_update_give_kec($give_kec,$request->session()->get('S_USER_ID'));

			$give_kel = $request->give_kel;
			Shr::do_update_give_kel($give_kel,$request->session()->get('S_USER_ID'));
			
			$data["success"] = TRUE;
			$data["message"] = "Data Berhasil Di Simpan";
       		return $data;
		}else{
			return redirect()->route('logout');
		}
	}
	public function add_userid(Request $request) 
	{
		if($request->user_id != null){
			$user_id = $request->user_id;
			$nama_lgkp = strtoupper($request->nama_lgkp);
			$nama_dpn = strtoupper($request->nama_dpn);
			$nik = $request->nik;
			$tmpt_lhr = strtoupper($request->tmpt_lhr);
			$tgl_lhr = $request->tgl_lhr;
			$jenis_klmin = $request->jenis_klmin;
			$gol_drh = $request->gol_drh;
			$nama_kantor = strtoupper($request->nama_kantor);
			$alamat_kantor = strtoupper($request->alamat_kantor);
			$telp = $request->telp;
			$alamat_rumah = strtoupper($request->alamat_rumah);
			$kdgroup = $request->kdgroup;
			$kdlvl = $request->kdlvl;
			$kdprop = $request->kdprop;
			$kdkab = $request->kdkab;
			$kdkec = $request->kdkec;
			$kdkel = $request->kdkel;
			$no_rw = $request->no_rw;
			$no_rt = $request->no_rt;
			$user_siak = $request->user_siak;
			$user_bcard = $request->user_bcard;
			$user_benrol = $request->user_benrol;
			$is_monitoring = $request->is_monitoring;
			$is_gisa = $request->is_gisa;
			$is_absen = $request->is_absen;
			$is_asn = $request->is_asn;
			$absensi_checking = $request->absensi_checking;
			$is_active = $request->is_active;
			$ipaddress_check = $request->ipaddress_check;
			$is_show = $request->is_show;
			$new_password = md5($request->new_password);
			$is_atasan_satu = $request->is_atasan_satu;
			$is_atasan_dua = $request->is_atasan_dua;
			$this->sett_insert_userid_data($user_id,$nama_lgkp,$nama_dpn,$nik,$tmpt_lhr,$tgl_lhr,$jenis_klmin,$gol_drh,$nama_kantor,$alamat_kantor,$telp,$alamat_rumah,$kdgroup,$kdlvl,$kdprop,$kdkab,$kdkec,$kdkel,$no_rw,$no_rt,$user_siak,$user_bcard,$user_benrol,$is_monitoring,$is_gisa,$is_absen,$is_asn,$absensi_checking,$is_active,$ipaddress_check,$is_show,$new_password,$request->session()->get('S_USER_ID'));
			$this->sett_insert_atasan($user_id,$is_atasan_satu,$is_atasan_dua);

			$data["success"] = TRUE;
			$data["message"] = "Data Berhasil Di Simpan";
       		return $data;
		}else{
			return redirect()->route('logout');
		}
	}
	public function do_akses(Request $request) 
	{
		if($request->lvl_cd != null){
			$lvl_cd = $request->lvl_cd;
			$menu_id = array();
			$menu_id = $request->checkedids;
			$this->sett_def_akses($lvl_cd);
			foreach( $menu_id  as $r)
			{
			

				$this->sett_change_akses($lvl_cd,$r);
			}
			$data["success"] = TRUE;
			$data["message"] = "Data Berhasil Di Simpan";
       		return $data;
		}else{
			return redirect()->route('logout');
		}
	}
	public function delete_lvl(Request $request) 
	{
		if($request->lvl_cd != null){
			$lvl_cd = $request->lvl_cd;
			$this->sett_delete_lvl($lvl_cd);
			$this->sett_delete_akses($lvl_cd);
			$this->sett_backup_user();
			$this->sett_delete_akun($lvl_cd);
			$data["success"] = TRUE;
			$data["message"] = "Data Berhasil Di Hapus";
       		return $data;
		}else{
			return redirect()->route('logout');
		}
	}
	public function delete_pejabat(Request $request) 
	{
		if($request->pejabat != null){
			$pejabat = $request->pejabat;
			$this->sett_delete_pejabat($pejabat);
			$data["success"] = TRUE;
			$data["message"] = "Data Berhasil Di Hapus";
       		return $data;
		}else{
			return redirect()->route('logout');
		}
	}
	public function delete_activity_id(Request $request) 
	{
		if($request->activity_id != null){
			$activity_id = $request->activity_id;
			$this->sett_delete_activity_id($activity_id);
			$this->sett_backup_daily();
			$this->sett_delete_daily($activity_id);
			$data["success"] = TRUE;
			$data["message"] = "Activity Berhasil Di Hapus";
       		return $data;
		}else{
			return redirect()->route('logout');
		}
	}
	public function delete_user_id(Request $request){
		header('Content-type: application/json');
		if ($request->user_id != null){
			$user_id = $request->user_id;
			$this->sett_del_user_backup($user_id);
			$this->sett_backup_user();
			$this->sett_delete_user($user_id);
			$this->sett_delete_atasan($user_id);
			$output = array(
	    			"message_type"=>1,
	    			"message"=>"User Ini Telah Dihapus !"
	    	);
			return $output;	
		}
	}
	public function get_user_level(Request $request)
     {
          $draw = intval($request->draw);
          $start = intval($request->start);
          $length = intval($request->length);
          $group_id = $request->group_id;

          $user_lvl = $this->sett_get_user_level($group_id);

          $data = array();
          $i = 0;
          foreach($user_lvl as $r) {
          		$i ++;
               $data[] = array(
               		$i,
                    $r->user_level,
                    $r->level_name,
                    $r->group_name,
                    '<button type="button" class="btn btn-info btn-xs" id="btn-lihat" onclick="lihat('.$r->user_level.');" style="margin-top: 5px;">Select <i class="mdi  mdi-magnify fa-fw"></i></button> &nbsp <button type="button" class="btn btn-primary btn-xs" id="btn-edit" onclick="edit('.$r->user_level.');" style="margin-top: 5px;">Edit <i class="mdi  mdi-tooltip-edit fa-fw"></i></button> &nbsp <button type="button" class="btn btn-warning btn-xs" id="btn-ubah" onclick="akses('.$r->user_level.');" style="margin-top: 5px;">Privilege Access <i class="mdi  mdi-apps fa-fw"></i></button> &nbsp <button type="button" class="btn btn-danger btn-xs" id="btn-delete" onclick="hapus('.$r->user_level.');" style="margin-top: 5px;">Delete <i class="mdi mdi-delete-forever fa-fw"></i></button>'
               );
          }

          $output = array(
               "draw" => $draw,
                 "recordsTotal" => count($user_lvl),
                 "recordsFiltered" => count($user_lvl),
                 "data" => $data
            );
          return $output;
          exit();
		}

	public function get_user_helpdesk(Request $request)
     {
          $draw = intval($request->draw);
          $start = intval($request->start);
          $length = intval($request->length);
          $group_id = $request->group_id;

          $helpdesk = $this->sett_get_user_helpdesk();

          $data = array();
          $i = 0;
          foreach($helpdesk as $r) {
          		$i ++;
               $data[] = array(
               		$i,
                    $r->helpdesk_id,
                    $r->helpdesk_description,
                    '<button type="button" class="btn btn-info btn-xs" id="btn-lihat" onclick="lihat('.$r->helpdesk_id.');" style="margin-top: 5px;">Select <i class="mdi  mdi-magnify fa-fw"></i></button> &nbsp <button type="button" class="btn btn-primary btn-xs" id="btn-edit" onclick="edit('.$r->helpdesk_id.');" style="margin-top: 5px;">Edit <i class="mdi  mdi-tooltip-edit fa-fw"></i></button> &nbsp <button type="button" class="btn btn-danger btn-xs" id="btn-delete" onclick="hapus('.$r->helpdesk_id.');" style="margin-top: 5px;">Delete <i class="mdi mdi-delete-forever fa-fw"></i></button>'
               );
          }

          $output = array(
               "draw" => $draw,
                 "recordsTotal" => count($helpdesk),
                 "recordsFiltered" => count($helpdesk),
                 "data" => $data
            );
          return $output;
          exit();
		}
	public function get_user_activity(Request $request)
     {
          $draw = intval($request->draw);
          $start = intval($request->start);
          $length = intval($request->length);
          $group_id = $request->group_id;

          $user_lvl = $this->sett_get_activity($group_id);

          $data = array();
          $i = 0;
          foreach($user_lvl as $r) {
          		$i ++;
               $data[] = array(
               		$i,
                    $r->activity,
                    $r->level_name,
                    '<button type="button" class="btn btn-primary btn-xs" id="btn-edit" onclick="edit('.$r->activity_id.');" style="margin-top: 5px;">Edit <i class="mdi  mdi-tooltip-edit fa-fw"></i></button> &nbsp <button type="button" class="btn btn-danger btn-xs" id="btn-delete" onclick="hapus('.$r->activity_id.');" style="margin-top: 5px;">Delete <i class="mdi mdi-delete-forever fa-fw"></i></button>'
               );
          }

          $output = array(
               "draw" => $draw,
                 "recordsTotal" => count($user_lvl),
                 "recordsFiltered" => count($user_lvl),
                 "data" => $data
            );
          return $output;
          exit();
		}
	public function get_user_list(Request $request)
     {
          $draw = intval($request->draw);
          $start = intval($request->start);
          $length = intval($request->length);
          $level_id = $request->level_id;
          $user_id = $request->user_id;
          $user_nm = $request->user_nm;

          $user_data = $this->sett_get_user_list($level_id,$user_id,$user_nm);

          $data = array();
          $i = 0;
          foreach($user_data as $r) {
          		$i ++;
               $data[] = array(
               		$i,
                    $r->user_id,
                    $r->nama_lgkp,
                    $r->level_name,
                    '<button type="button" class="btn btn-info btn-xs" id="btn-lihat" onclick="lihat(\''.$r->user_id.'\');" style="margin-top: 5px;">Select <i class="mdi  mdi-magnify fa-fw"></i></button> &nbsp <button type="button" class="btn btn-primary btn-xs" id="btn-edit" onclick="edit(\''.$r->user_id.'\');" style="margin-top: 5px;">Edit <i class="mdi  mdi-tooltip-edit fa-fw"></i></button> &nbsp <button type="button" class="btn btn-warning btn-xs" id="btn-ubah" onclick="reset(\''.$r->user_id.'\');" style="margin-top: 5px;">Reset Password <i class="mdi  mdi-sync fa-fw"></i></button> &nbsp <button type="button" class="btn btn-danger btn-xs" id="btn-delete" onclick="hapus(\''.$r->user_id.'\');" style="margin-top: 5px;">Delete <i class="mdi mdi-delete-forever fa-fw"></i></button>'
               );
          }

          $output = array(
               "draw" => $draw,
                 "recordsTotal" => count($user_data),
                 "recordsFiltered" => count($user_data),
                 "data" => $data
            );
          return $output;
          exit();
		}
	public function get_pejabat_list(Request $request)
     {
          $draw = intval($request->draw);
          $start = intval($request->start);
          $length = intval($request->length);
          $nama = $request->nama;
          $nip = $request->nip;

          $user_data = $this->sett_get_pejabat_list($nama,$nip);

          $data = array();
          $i = 0;
          foreach($user_data as $r) {
          		$i ++;
               $data[] = array(
               		$i,
               		$r->nip,
                    $r->nama,
                    $r->jabatan,
                    $r->struktural,
                    '<button type="button" class="btn btn-primary btn-xs" id="btn-edit" onclick="edit('.$r->pejabat_id.');" style="margin-top: 5px;">Edit <i class="mdi  mdi-tooltip-edit fa-fw"></i></button> &nbsp <button type="button" class="btn btn-danger btn-xs" id="btn-delete" onclick="hapus('.$r->pejabat_id.');" style="margin-top: 5px;">Delete <i class="mdi mdi-delete-forever fa-fw"></i></button>'
               );
          }

          $output = array(
               "draw" => $draw,
                 "recordsTotal" => count($user_data),
                 "recordsFiltered" => count($user_data),
                 "data" => $data
            );
          return $output;
          exit();
		}

	public function see_user(Request $request, $id) 
	{
			$menu_id = 134;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
		    if($id != null){
		    $user_level = $id;
		    $c_head = $this->sett_get_head_group($user_level);
		    $theuser = $this->sett_get_user_group($user_level);
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$data = array(
		 		"stitle"=>'Setting User Level',
		 		"mtitle"=>'Setting User Level',
		 		"my_url"=>'UserLevel',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"chead"=>$c_head,
		 		"theuser"=>$theuser,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
    		);
			return view('User_level_lihat/main',$data);
			}else{
				return redirect()->route('logout');
			}
	}
	public function see_user_id(Request $request, $user_id) 
	{
			$menu_id = 135;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
		    if($user_id != null){
			    $theuser = $this->sett_get_theuser($user_id);
				$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
				$isakses_kec = Shr::get_give_kec();
				$isakses_kel = Shr::get_give_kel();
				$data = array(
			 		"stitle"=>'Setting User Id',
			 		"mtitle"=>'Setting User Id',
			 		"my_url"=>'UserLevel',
			 		"type_tgl"=>'Tanggal',
			 		"menu"=>$menu,
       				"akses_kec"=>$isakses_kec,
       				"akses_kel"=>$isakses_kel,
			 		"theuser"=>$theuser,
			 		"user_id"=>$request->session()->get('S_USER_ID'),
			 		"user_nik"=>$request->session()->get('S_NIK'),
			 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
			 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
			 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
			 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
			 		"user_level"=>$request->session()->get('S_USER_LEVEL')
	    		);
				return view('Userid_lihat/main',$data);
			}else{
				return redirect()->route('logout');
			}
	}
	public function edit_user_id(Request $request, $user_id) 
	{
			$menu_id = 135;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
		    if($user_id != null){
			    $theuser = $this->sett_get_theuser($user_id);
				$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
				$isakses_kec = Shr::get_give_kec();
				$isakses_kel = Shr::get_give_kel();
				$data = array(
			 		"stitle"=>'Setting User Id',
			 		"mtitle"=>'Setting User Id',
			 		"my_url"=>'UserLevel',
			 		"type_tgl"=>'Tanggal',
			 		"menu"=>$menu,
       				"akses_kec"=>$isakses_kec,
       				"akses_kel"=>$isakses_kel,
			 		"theuser"=>$theuser,
			 		"user_id"=>$request->session()->get('S_USER_ID'),
			 		"user_nik"=>$request->session()->get('S_NIK'),
			 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
			 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
			 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
			 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
			 		"user_level"=>$request->session()->get('S_USER_LEVEL')
	    		);
				return view('Userid_edit/main',$data);
			}else{
				return redirect()->route('logout');
			}
	}
	public function change_user(Request $request, $id) 
	{
			$menu_id = 134;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
		    if($id != null){
		    $user_level = $id;
		    $c_head = $this->sett_get_head_group($user_level);
		    $theuser = $this->sett_get_user_group($user_level);
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$data = array(
		 		"stitle"=>'Setting User Level',
		 		"mtitle"=>'Setting User Level',
		 		"my_url"=>'UserLevel',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"chead"=>$c_head,
		 		"theuser"=>$theuser,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
    		);
			return view('User_level_change/main',$data);
			}else{
				return redirect()->route('logout');
			}
	}
	public function edit_user(Request $request, $id) 
	{
			$menu_id = 134;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
		    if($id != null){
		    $user_level = $id;
		    $c_head = $this->sett_get_head_group($user_level);
		    $theuser = $this->sett_get_user_group($user_level);
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$data = array(
		 		"stitle"=>'Setting User Level',
		 		"mtitle"=>'Setting User Level',
		 		"my_url"=>'UserLevel',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"chead"=>$c_head,
		 		"theuser"=>$theuser,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
    		);
			return view('User_level_edit/main',$data);
			}else{
				return redirect()->route('logout');
			}
	}
	public function delete_user(Request $request, $id) 
	{
			$menu_id = 134;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
		    if($id != null){
		    $user_level = $id;
		    $c_head = $this->sett_get_head_group($user_level);
		    $theuser = $this->sett_get_user_group($user_level);
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$data = array(
		 		"stitle"=>'Delete User Level',
		 		"mtitle"=>'Delete User Level',
		 		"my_url"=>'UserLevel',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"chead"=>$c_head,
		 		"theuser"=>$theuser,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
    		);
			return view('User_level_delete/main',$data);
			}else{
				return redirect()->route('logout');
			}
	}

	public function get_group_tree_one(Request $request) 
	{
		$user_level = $request->user_level;
		$data = Shr::do_get_menu_all($user_level);

		$menu = array();
		$i=0;
		foreach( $data as $r )
		{
		    $menu[$i]['text'] = $r->title;
			$menu[$i]['id'] = $r->menu_id;
			$menu[$i]['disabled'] = true;
			$menu[$i]['checked'] = ($r->active_menu != 0) ? true : false;
			$menu[$i]['children'] = $this->get_group_tree_two($r->menu_id,$user_level);
			$i++;
		}
		return $menu;
	}
	public function get_group_tree_two($menu_id,$user_level) 
	{
		$lvl = $user_level;
		$data = Shr::do_get_sub_menu_all($menu_id,$lvl);
		$sub_menu = array();
		$i=0;
		foreach( $data as $r )
		{
		    $sub_menu[$i]['text'] = $r->title;
			$sub_menu[$i]['id'] = $r->menu_id;
			$sub_menu[$i]['disabled'] = true;
			$sub_menu[$i]['checked'] = ($r->active_menu != 0) ? true : false;
			$sub_menu[$i]['children'] = $this->get_group_tree_three($r->menu_id,$lvl);
			$i++;
		}
		return $sub_menu;
	}
	public function get_group_tree_three($menu_id,$user_level) 
	{
		$lvl = $user_level;
		$data = Shr::do_get_sub_menu_all($menu_id,$lvl);
		$sub_menu = array();
		$i=0;
		foreach( $data as $r )
		{
		    $sub_menu[$i]['text'] = $r->title;
			$sub_menu[$i]['id'] = $r->menu_id;
			$sub_menu[$i]['disabled'] = true;
			$sub_menu[$i]['checked'] = ($r->active_menu != 0) ? true : false;
			$sub_menu[$i]['children'] = array();
			$i++;
		}
		return $sub_menu;
	}
	public function edit_tree_one(Request $request) 
	{
		$user_level = $request->user_level;
		$data = Shr::do_get_menu_all($user_level);
		$menu = array();
		$i=0;
		foreach( $data as $r )
		{
		    $menu[$i]['text'] = $r->title;
			$menu[$i]['id'] = $r->menu_id;
			$menu[$i]['checked'] = ($r->active_menu != 0) ? true : false;
			$menu[$i]['children'] = $this->edit_tree_two($r->menu_id,$user_level);
			$i++;
		}
		return $menu;
	}
	public function edit_tree_two($menu_id,$user_level) 
	{
		$lvl = $user_level;
		$data = Shr::do_get_sub_menu_all($menu_id,$lvl);
		$sub_menu = array();
		$i=0;
		foreach( $data as $r )
		{
		    $sub_menu[$i]['text'] = $r->title;
			$sub_menu[$i]['id'] = $r->menu_id;
			$sub_menu[$i]['checked'] = ($r->active_menu != 0) ? true : false;
			$sub_menu[$i]['children'] = $this->edit_tree_three($r->menu_id,$lvl);
			$i++;
		}
		return $sub_menu;
	}
	public function edit_tree_three($menu_id,$user_level) 
	{
		$lvl = $user_level;
		$data = Shr::do_get_sub_menu_all($menu_id,$lvl);
		$sub_menu = array();
		$i=0;
		foreach( $data as $r )
		{
		    $sub_menu[$i]['text'] = $r->title;
			$sub_menu[$i]['id'] = $r->menu_id;
			$sub_menu[$i]['checked'] = ($r->active_menu != 0) ? true : false;
			$sub_menu[$i]['children'] = array();
			$i++;
		}
		return $sub_menu;
	}
	public function max_helpdesk(Request $request) 
	{
			$j = $this->sett_max_helpdesk();
			$data["jml"] = $j;
       		return $data;
	}

	public function save_helpdesk(Request $request) 
	{
		if($request->helpdesk_cd != null){
			$helpdesk_cd = $request->helpdesk_cd;
			$helpdesk_nm = strtoupper($request->helpdesk_nm);
			$this->sett_save_helpdesk($helpdesk_cd,$helpdesk_nm,$request->session()->get('S_USER_ID'));
			$data["success"] = TRUE;
			$data["message"] = "Data Berhasil Di Simpan";
       		return $data;
		}else{
			return redirect()->route('logout');
		}
	}

	public function update_helpdesk(Request $request) 
	{
		if($request->helpdesk_id != null){
			$helpdesk_id = $request->helpdesk_id;
			$helpdesk_nm = $request->helpdesk_nm;
			$this->sett_edit_helpdesk_cd($helpdesk_id,$helpdesk_nm);
			$data["success"] = TRUE;
			$data["message"] = "Data Berhasil Di Update";
       		return $data;
		}else{
			return redirect()->route('logout');
		}
	}

	public function delete_helpdesk(Request $request) 
	{
		if($request->helpdesk_id != null){
			$helpdesk_id = $request->helpdesk_id;
			$this->sett_delete_helpdesk($helpdesk_id);
			$data["success"] = TRUE;
			$data["message"] = "Data Berhasil Di Hapus";
       		return $data;
		}else{
			return redirect()->route('logout');
		}
	}
	public function get_helpdesk_option(Request $request)
	{
		header("Content-Type: application/json", true);
		$r = $this->sett_get_all_helpdesk();
		return $r;
	}

	public function sett_get_group(){
      $sql = "SELECT LEVEL_CODE, LEVEL_NAME FROM SIAK_USER_GROUP ORDER BY LEVEL_CODE";
      $r = DB::select($sql);
      return $r;
    }
    public function sett_get_group_user(){
      $sql = "SELECT USER_LEVEL, LEVEL_NAME FROM SIAK_ACTIVITY_LEVEL ORDER BY  USER_LEVEL";
      $r = DB::select($sql);
      return $r;
    }
    public function sett_get_atasan(){
      $sql = "SELECT PEJABAT_ID, NIP, NAMA, JABATAN,IS_STRUKTURAL FROM SIAK_PEJABAT WHERE IS_STRUKTURAL =1 ORDER BY PEJABAT_ID";
      $r = DB::select($sql);
      return $r;
    }
    public function sett_get_group_lvl($lvl){
      $sql = "SELECT USER_LEVEL, LEVEL_NAME, GROUP_LEVEL FROM SIAK_USER_LEVEL WHERE GROUP_LEVEL = $lvl ORDER BY USER_LEVEL";
      $r = DB::select($sql);
      return $r;
    }
    public function sett_get_activity_data($activity_id){
      $sql = "SELECT A.ACTIVITY_ID, A.ACTIVITY, B.LEVEL_NAME FROM SIAK_ACTIVITY A INNER JOIN SIAK_USER_LEVEL B ON A.USER_LEVEL = B.USER_LEVEL WHERE A.ACTIVITY_ID = $activity_id";
      $r = DB::select($sql);
      return $r;
    }
        public function sett_get_level(){
            $sql = "SELECT USER_LEVEL, LEVEL_NAME FROM SIAK_USER_LEVEL ORDER BY USER_LEVEL";
            $r = DB::select($sql);
            return $r;
        }
        public function sett_get_head_group($user_level){
            $sql = "SELECT A.USER_LEVEL, A.LEVEL_NAME, B.LEVEL_NAME GROUP_NAME, B.LEVEL_CODE FROM SIAK_USER_LEVEL A INNER JOIN SIAK_USER_GROUP B ON A.GROUP_LEVEL = B.LEVEL_CODE WHERE A.USER_LEVEL = $user_level";
            $r = DB::select($sql);
            return $r;
        }
        public function sett_get_activity($lvl){
        $sql = "SELECT A.ACTIVITY_ID, A.ACTIVITY, B.LEVEL_NAME FROM SIAK_ACTIVITY A INNER JOIN SIAK_USER_LEVEL B ON A.USER_LEVEL = B.USER_LEVEL WHERE A.ACTIVITY_ID <> 0 AND A.USER_LEVEL = $lvl ORDER BY A.ACTIVITY_ID";
        $r = DB::select($sql);
        return $r;
      }
        public function sett_get_user_level($group_id){
            $sql = "SELECT A.USER_LEVEL, A.LEVEL_NAME, B.LEVEL_NAME GROUP_NAME, B.LEVEL_CODE  FROM SIAK_USER_LEVEL A INNER JOIN SIAK_USER_GROUP B ON A.GROUP_LEVEL = B.LEVEL_CODE WHERE A.GROUP_LEVEL = $group_id ORDER BY USER_LEVEL";
            $r = DB::select($sql);
            return $r;
        }
        public function sett_get_user_helpdesk(){
            $sql = "SELECT A.HELPDESK_ID, A.HELPDESK_DESCRIPTION FROM SIAK_MASTER_HELPDESK A  ORDER BY A.HELPDESK_ID";
            $r = DB::select($sql);
            return $r;
        }
        public function sett_get_user_list($lvl_id = 0,$user_id = '',$user_nm = ''){
            $sql = "";
            $sql .= "SELECT A.USER_ID,A.NAMA_LGKP, A.NIK,A.TMPT_LHR, A.TGL_LHR, CASE WHEN A.JENIS_KLMIN = 1 THEN 'LAKI-LAKI' ELSE 'PEREMPUAN' END JENIS_KLMIN, A.NAMA_KANTOR, A.TELP, B.LEVEL_NAME, C.LEVEL_NAME GROUP_NAME FROM SIAK_USER_PLUS A INNER JOIN SIAK_USER_LEVEL B ON A.USER_LEVEL = B.USER_LEVEL INNER JOIN SIAK_USER_GROUP C ON B.GROUP_LEVEL = C.LEVEL_CODE WHERE 1=1 ";
            if ($lvl_id != 0){
                $sql .=" AND A.USER_LEVEL = $lvl_id";    
            }
            if ($user_id != ''){
                $sql .=" AND UPPER(A.USER_ID) LIKE UPPER('$user_id')";    
            }
            if ($user_nm != ''){
                $sql .=" AND UPPER(A.NAMA_LGKP) LIKE UPPER('$user_nm')";    
            }
                $sql .=" ORDER BY A.USER_LEVEL, A.NO_KEC, A.NO_KEL, A.NAMA_LGKP";   
            $r = DB::select($sql);
            return $r;
        }
        public function sett_get_pejabat_list($nama = '',$nip = ''){
            $sql = "";
            $sql .= "SELECT PEJABAT_ID, NIP, JABATAN, NAMA, CASE WHEN IS_STRUKTURAL = 1 THEN 'YA' ELSE '-' END STRUKTURAL FROM SIAK_PEJABAT WHERE 1=1 ";
            if ($nama != ''){
                $sql .=" AND UPPER(NAMA) LIKE UPPER('%$nama%')";    
            }
            if ($nip != ''){
                $sql .=" AND NIP LIKE '%$nip%'";    
            }
                $sql .=" ORDER BY IS_STRUKTURAL DESC, PEJABAT_ID";   
            $r = DB::select($sql);
            return $r;
        }
        public function sett_edit_pejabat_list($pejabat_id){
            $sql = "";
            $sql .= "SELECT PEJABAT_ID, NIP, JABATAN, NAMA, CASE WHEN IS_STRUKTURAL = 1 THEN 'YA' ELSE '-' END STRUKTURAL, IS_STRUKTURAL FROM SIAK_PEJABAT WHERE 1=1 AND PEJABAT_ID = $pejabat_id";
                $sql .=" ORDER BY IS_STRUKTURAL DESC, PEJABAT_ID";   
            $r = DB::select($sql);
            return $r;
        }
        public function sett_get_user_group($group_id){
            $sql = "SELECT USER_ID, NAMA_LGKP, NIK,TMPT_LHR, TO_CHAR(TGL_LHR,'DD-MM-YYYY') TGL_LHR,JENIS_KLMIN,NAMA_KANTOR,TELP,ALAMAT_RUMAH FROM SIAK_USER_PLUS WHERE USER_LEVEL = $group_id ORDER BY USER_LEVEL, TO_NUMBER(NO_KEC), NAMA_LGKP";
            $r = DB::select($sql);
            return $r;
        } 
        public function sett_check_lvl($user_level){
           $sql = "SELECT COALESCE(COUNT(1),0) JML, COALESCE(MAX(CASE WHEN LEVEL_NAME IS NOT NULL  THEN LEVEL_NAME ELSE '-' END ),'-') LEVEL_NAME FROM SIAK_USER_LEVEL WHERE USER_LEVEL = $user_level";
            $r = DB::select($sql);
            return $r;
        }   
        public function sett_check_userid($user_id){
           $sql = "SELECT COALESCE(COUNT(1),0) JML, COALESCE(MAX(CASE WHEN NAMA_LGKP IS NOT NULL  THEN NAMA_LGKP ELSE '-' END ),'-') NAMA_LGKP FROM SIAK_USER_PLUS WHERE USER_ID = '$user_id'";
            $r = DB::select($sql);
            return $r;
        }  
        public function sett_check_jml_userid($user_id){
           $sql = "SELECT COUNT(1) JML  FROM SIAK_USER_PLUS WHERE USER_ID = '$user_id'";
            $r = DB::select($sql);
            return $r;
        }
        public function sett_save_lvl($lvl_cd,$lvl_nm,$kdgroup,$user_id){
            $sql = "INSERT INTO SIAK_USER_LEVEL (USER_LEVEL,LEVEL_NAME,GROUP_LEVEL,CREATED_DATE,CREATED_BY) VALUES ($lvl_cd,'$lvl_nm',$kdgroup,SYSDATE,'$user_id')";
            $r = DB::insert($sql);
            return $r;
        }
        public function sett_max_pejabat(){
            $sql = "SELECT CASE WHEN MAX(PEJABAT_ID) +1 IS NULL THEN 1 ELSE MAX(PEJABAT_ID) +1 END JML FROM SIAK_PEJABAT";
            $r = DB::select($sql);
            return $r[0]->jml;
            
            
        }
        public function sett_save_pejabat($id,$nip,$nama,$jabatan,$struktural){
            $sql = "INSERT INTO SIAK_PEJABAT (PEJABAT_ID,NIP,JABATAN,NAMA,IS_STRUKTURAL) VALUES ($id,$nip,'$jabatan','$nama',$struktural)";
            $r = DB::insert($sql);
            return $r;
        }
        public function sett_edit_pejabat_id($pejabat_id,$nip,$nama,$jabatan,$struktural){
            $sql = "UPDATE SIAK_PEJABAT 
                       SET 
                       NIP = $nip
                       , JABATAN = UPPER('$jabatan')
                       , NAMA = UPPER('$nama')
                       , IS_STRUKTURAL = $struktural 
                     WHERE PEJABAT_ID = $pejabat_id";
            $r = DB::update($sql);
            return $r;
        }
        public function sett_save_activity($activity_nm,$kdgroup,$user_id){
            $sql = "INSERT INTO SIAK_ACTIVITY (ACTIVITY_ID, USER_LEVEL, ACTIVITY, CREATED_DT, CREATED_BY) VALUES (ACTIVITY_ID.NEXTVAL, $kdgroup, UPPER('$activity_nm'), SYSDATE, '$user_id')";
            $r = DB::insert($sql);
            return $r;
        }
        public function sett_update_lvl($lvl_cd,$lvl_nm,$user_id){
            $sql = "UPDATE SIAK_USER_LEVEL 
                       SET LEVEL_NAME = UPPER('$lvl_nm'), MODIFIED_BY = '$user_id', MODIFIED_DATE = SYSDATE 
                     WHERE USER_LEVEL = $lvl_cd";
            $r = DB::update($sql);
            return $r;
        }
        public function sett_update_activity($activity_cd,$activity_nm){
            $sql = "UPDATE SIAK_ACTIVITY 
                       SET ACTIVITY = UPPER('$activity_nm')
                     WHERE ACTIVITY_ID = $activity_cd";
            $r = DB::update($sql);
            return $r;
        }

        public function sett_update_userid_data($user_id,$nama_lgkp,$nama_dpn,$nik,$tmpt_lhr,$tgl_lhr,$jenis_klmin,$gol_drh,$nama_kantor,$alamat_kantor,$telp,$alamat_rumah,$kdgroup,$kdlvl,$kdprop,$kdkab,$kdkec,$kdkel,$no_rw,$no_rt,$user_siak,$user_bcard,$user_benrol,$is_monitoring,$is_gisa,$is_absen,$is_asn,$absensi_checking,$is_active,$ipaddress_check,$is_show,$changed_by){
            $sql = ""; 
            $sql .= "UPDATE SIAK_USER_PLUS 
                       SET 
                      NAMA_LGKP = '$nama_lgkp' 
                      , NIK = $nik
                      , TMPT_LHR = '$tmpt_lhr'
                      , TGL_LHR = TO_DATE('$tgl_lhr','DD-MM-YYYY')
                      , JENIS_KLMIN = $jenis_klmin
                      , GOL_DRH = $gol_drh
                      , NAMA_KANTOR = '$nama_kantor'
                      , ALAMAT_KANTOR = '$alamat_kantor'
                      , TELP = '$telp'
                      , ALAMAT_RUMAH = '$alamat_rumah'
                      , USER_LEVEL = $kdlvl
                      , NO_PROP = $kdprop
                      , NO_KAB = $kdkab
                      , NO_KEC = $kdkec
                      , NO_KEL = $kdkel
                      , NAMA_DPN = '$nama_dpn' ";
                      if ($user_siak == ''){
                        $sql .= ", USER_SIAK = NULL " ;
                      }else{
                        $sql .= ", USER_SIAK = '$user_siak' " ;
                      }
                      if ($user_bcard == ''){
                        $sql .= ", USER_BCARD = NULL " ;
                      }else{
                        $sql .= ", USER_BCARD = '$user_bcard' " ;
                      }
                      if ($user_benrol == ''){
                        $sql .= ", USER_BENROL = NULL " ;
                      }else{
                        $sql .= ", USER_BENROL = '$user_benrol' " ;
                      }
             $sql .= "
                      , IS_MONITORING = $is_monitoring
                      , IS_GISA = $is_gisa
                      , NO_RW = $no_rw
                      , NO_RT = $no_rt
                      , IS_ABSEN = $is_absen
                      , IS_ASN = $is_asn
                      , ABSENSI_CHECKING = $absensi_checking
                      , IS_ACTIVE = $is_active
                      , IPADDRESS_CHECK = $ipaddress_check
                      , IS_SHOW = $is_show
                      , CHANGED_BY = '$changed_by'
                      , CHANGED_DT =  SYSDATE
                     WHERE USER_ID = '$user_id'";
                   
            $r = DB::update($sql);
            return $r;
        }
        public function sett_insert_userid_data($user_id,$nama_lgkp,$nama_dpn,$nik,$tmpt_lhr,$tgl_lhr,$jenis_klmin,$gol_drh,$nama_kantor,$alamat_kantor,$telp,$alamat_rumah,$kdgroup,$kdlvl,$kdprop,$kdkab,$kdkec,$kdkel,$no_rw,$no_rt,$user_siak,$user_bcard,$user_benrol,$is_monitoring,$is_gisa,$is_absen,$is_asn,$absensi_checking,$is_active,$ipaddress_check,$is_show,$new_password,$created_by){
            $sql = ""; 
            $sql .= "INSERT INTO SIAK_USER_PLUS (USER_ID
                    , NAMA_LGKP
                    , NIK
                    , TMPT_LHR
                    , TGL_LHR
                    , JENIS_KLMIN
                    , GOL_DRH
                    , NAMA_KANTOR
                    , ALAMAT_KANTOR
                    , TELP
                    , ALAMAT_RUMAH
                    , USER_LEVEL
                    , NO_PROP
                    , NO_KAB
                    , NO_KEC
                    , NO_KEL
                    , NAMA_DPN ";
                    if ($user_siak != ''){
                        $sql .= ", USER_SIAK " ;
                    }
                    if ($user_bcard != ''){
                        $sql .= ", USER_BCARD " ;
                    }
                    if ($user_benrol != ''){
                        $sql .= ", USER_BENROL " ;
                    }
           $sql .= ", IS_MONITORING
                    , IS_GISA
                    , NO_RW
                    , NO_RT
                    , IS_ABSEN
                    , IS_ASN
                    , ABSENSI_CHECKING
                    , IS_ACTIVE
                    , IPADDRESS_CHECK
                    , IS_SHOW
                    , USER_PWD
                    , CREATED_BY
                    , CREATED_DT) VALUES (
                      '$user_id' 
                      , '$nama_lgkp' 
                      , $nik
                      , '$tmpt_lhr'
                      , TO_DATE('$tgl_lhr','DD-MM-YYYY')
                      , $jenis_klmin
                      , $gol_drh
                      , '$nama_kantor'
                      , '$alamat_kantor'
                      , '$telp'
                      , '$alamat_rumah'
                      , $kdlvl
                      , $kdprop
                      , $kdkab
                      , $kdkec
                      , $kdkel
                      , '$nama_dpn' ";
                    if ($user_siak != ''){
                        $sql .= ", '$user_siak' " ;
                    }
                    if ($user_bcard != ''){
                        $sql .= ", '$user_bcard' " ;
                    }
                    if ($user_benrol != ''){
                        $sql .= ", '$user_benrol' " ;
                    }
           $sql .= "  , $is_monitoring
                      , $is_gisa
                      , $no_rw
                      , $no_rt
                      , $is_absen
                      , $is_asn
                      , $absensi_checking
                      , $is_active
                      , $ipaddress_check
                      , $is_show
                      , '$new_password'
                      , '$created_by'
                      ,  SYSDATE)";
                   
            $r = DB::insert($sql);
            return $r;
        }
        public function sett_def_akses($lvl_cd){
            $sql = "UPDATE SIAK_AKSES 
                       SET IS_ACTIVE = 0
                     WHERE USER_LEVEL = $lvl_cd";
            $r = DB::update($sql);
            return $r;
        }
        public function sett_change_akses($lvl_cd,$menu_id){
            $sql = "UPDATE SIAK_AKSES C
                       SET IS_ACTIVE = 1
                     WHERE EXISTS (
                        SELECT 1 FROM SIAK_AKSES A INNER JOIN
                    (SELECT MENU_ID FROM SIAK_MENU WHERE MENU_ID = $menu_id OR MENU_ID IN 
                    (SELECT PARENT_ID FROM SIAK_MENU WHERE MENU_ID = $menu_id OR MENU_ID IN (SELECT PARENT_ID FROM SIAK_MENU WHERE MENU_ID = $menu_id))) 
                    B ON A.MENU_ID = B.MENU_ID WHERE A.USER_LEVEL = $lvl_cd AND A.MENU_ID = C.MENU_ID AND A.USER_LEVEL = C.USER_LEVEL)";
            $r = DB::update($sql);
            return $r;
        }
        public function sett_save_akses($lvl_cd){
            $sql = "INSERT INTO SIAK_AKSES (USER_LEVEL,MENU_ID,IS_ACTIVE)
                    SELECT $lvl_cd USER_LEVEL, A.MENU_ID, 0 IS_ACTIVE FROM SIAK_MENU A LEFT JOIN SIAK_AKSES B ON A.MENU_ID = B.MENU_ID AND USER_LEVEL = $lvl_cd WHERE B.MENU_ID IS NULL ORDER BY A.MENU_ID";
            $r = DB::insert($sql);
            return $r;
        }
        public function sett_insert_atasan($user_id,$is_atasan_satu,$is_atasan_dua){
            $sql = "INSERT INTO SIAK_ATASAN (USER_ID,ATASAN_SATU,ATASAN_DUA) VALUES ('$user_id',$is_atasan_satu,$is_atasan_dua)";
            $r = DB::insert($sql);
            return $r;
        }
        public function sett_update_atasan($user_id,$is_atasan_satu,$is_atasan_dua){
            $sql = "UPDATE SIAK_ATASAN 
                       SET ATASAN_SATU = $is_atasan_satu, ATASAN_DUA = $is_atasan_dua
                     WHERE USER_ID = '$user_id'";
            $r = DB::update($sql);
            return $r;
        }
        public function sett_delete_lvl($lvl_cd){
            $sql = "DELETE FROM SIAK_USER_LEVEL WHERE USER_LEVEL = $lvl_cd";
            $r = DB::delete($sql);
            return $r;
        }
        public function sett_delete_pejabat($pejabat){
            $sql = "DELETE FROM SIAK_PEJABAT WHERE PEJABAT_ID = $pejabat";
            $r = DB::delete($sql);
            return $r;
        }
        public function sett_delete_akses($lvl_cd){
            $sql = "DELETE FROM SIAK_AKSES WHERE USER_LEVEL = $lvl_cd";
            $r = DB::delete($sql);
            return $r;
        }
        public function sett_delete_akun($lvl_cd){
            $sql = "DELETE FROM SIAK_USER_PLUS WHERE USER_LEVEL = $lvl_cd";
            $r = DB::delete($sql);
            return $r;
        }  
        public function sett_get_theuser($user_id){
            $sql = "SELECT 
                  A.USER_ID
                  , A.NAMA_LGKP
                  , A.NAMA_DPN
                  , A.NIK
                  , A.TMPT_LHR
                  , TO_CHAR(A.TGL_LHR,'DD-MM-YYYY') TGL_LHR
                  , CASE WHEN A.JENIS_KLMIN = 1 THEN 'LAKI-LAKI' ELSE 'PEREMPUAN' END AS JENIS_KLMIN
                  , A.JENIS_KLMIN KLMN
                  , UPPER(F5_GET_REF_WNI(A.GOL_DRH, 401)) GOL_DRH
                  , A.GOL_DRH GLDRH
                  , A.NAMA_KANTOR
                  , A.ALAMAT_KANTOR
                  , A.TELP
                  , A.ALAMAT_RUMAH
                  , B.USER_LEVEL
                  , B.LEVEL_NAME
                  , C.LEVEL_NAME GROUP_NAME
                  , C.LEVEL_CODE GROUP_CODE
                  , A.NO_PROP
                  , UPPER(F5_GET_NAMA_PROVINSI(A.NO_PROP)) NAMA_PROP
                  , A.NO_KAB
                  , UPPER(F5_GET_NAMA_KABUPATEN(A.NO_PROP,A.NO_KAB)) NAMA_KAB
                  , A.NO_KEC
                  , UPPER(F5_GET_NAMA_KECAMATAN(A.NO_PROP,A.NO_KAB,A.NO_KEC)) NAMA_KEC
                  , A.NO_KEL
                  , A.NO_RW
                  , A.NO_RT
                  , UPPER(F5_GET_NAMA_KELURAHAN(A.NO_PROP,A.NO_KAB,A.NO_KEC,A.NO_KEL)) NAMA_KEL
                  , A.USER_SIAK
                  , A.USER_BCARD
                  , A.USER_BENROL
                  , CASE WHEN  A.IS_MONITORING = 1 THEN 'YA' ELSE 'TIDAK' END IS_MONITORING
                  , CASE WHEN  A.IS_GISA = 1 THEN 'YA' ELSE 'TIDAK' END  IS_GISA 
                  , CASE WHEN  A.IS_ABSEN = 1 THEN 'YA' ELSE 'TIDAK' END  IS_ABSEN 
                  , CASE WHEN  A.IS_ASN = 1 THEN 'YA' ELSE 'TIDAK' END  IS_ASN 
                  , CASE WHEN  A.ABSENSI_CHECKING = 1 THEN 'YA' ELSE 'TIDAK' END  ABSENSI_CHECKING 
                  , CASE WHEN  A.IS_ACTIVE = 1 THEN 'YA' ELSE 'TIDAK' END  IS_ACTIVE 
                  , CASE WHEN  A.IPADDRESS_CHECK = 1 THEN 'YA' ELSE 'TIDAK' END  IPADDRESS_CHECK 
                  , CASE WHEN  A.IS_SHOW = 1 THEN 'YA' ELSE 'TIDAK' END  IS_SHOW 
                  , CASE WHEN  E.PEJABAT_ID IS NULL THEN 0 ELSE E.PEJABAT_ID END  PEJABAT_SATU 
                  , CASE WHEN  E.NAMA IS NULL THEN '-' ELSE E.NAMA END  NAMA_SATU 
                  , CASE WHEN  F.PEJABAT_ID IS NULL THEN 0 ELSE F.PEJABAT_ID END  PEJABAT_DUA
                  , CASE WHEN  F.NAMA IS NULL THEN '-' ELSE F.NAMA END  NAMA_DUA 
                FROM SIAK_USER_PLUS A 
                INNER JOIN SIAK_USER_LEVEL B 
                ON A.USER_LEVEL = B.USER_LEVEL 
                INNER JOIN SIAK_USER_GROUP C 
                ON B.GROUP_LEVEL = C.LEVEL_CODE
                LEFT JOIN SIAK_ATASAN D 
                ON A.USER_ID = D.USER_ID
                LEFT JOIN SIAK_PEJABAT E 
                ON D.ATASAN_SATU = E.PEJABAT_ID
                LEFT JOIN SIAK_PEJABAT F 
                ON D.ATASAN_DUA = F.PEJABAT_ID
                 WHERE 1=1 AND A.USER_ID = '$user_id'";
            $r = DB::select($sql);
            return $r;
        }

        public function sett_del_user_backup($user_id){
            $sql = "DELETE FROM SIAK_USER_PLUS_HIST WHERE USER_ID = '$user_id'";
            $r = DB::delete($sql);
            return $r;
        }
        public function sett_delete_user($user_id){
            $sql = "DELETE FROM SIAK_USER_PLUS WHERE USER_ID = '$user_id'";
            $r = DB::delete($sql);
            return $r;
        }
        public function sett_delete_atasan($user_id){
            $sql = "DELETE FROM SIAK_ATASAN WHERE USER_ID = '$user_id'";
            $r = DB::delete($sql);
            return $r;
        }
        public function sett_backup_user(){
            $sql = "INSERT INTO SIAK_USER_PLUS_HIST (USER_ID, NAMA_LGKP, NIK, TMPT_LHR, TGL_LHR, JENIS_KLMIN, GOL_DRH, PENDIDIKAN, PEKERJAAN, NAMA_KANTOR, ALAMAT_KANTOR, TELP, ALAMAT_RUMAH, USER_LEVEL, USER_PWD, NO_PROP, NO_KAB, NO_KEC, NO_KEL, NAMA_DPN, USER_SIAK, USER_BCARD, USER_BENROL, IS_MONITORING, IS_GISA, NO_RW, NO_RT, IS_ABSEN, IS_ASN, ABSENSI_CHECKING, IS_ACTIVE, IPADDRESS_CHECK, IS_SHOW, CREATED_BY, CREATED_DT, CHANGED_BY, CHANGED_DT) (SELECT B.USER_ID, B.NAMA_LGKP, B.NIK, B.TMPT_LHR, B.TGL_LHR, B.JENIS_KLMIN, B.GOL_DRH, B.PENDIDIKAN, B.PEKERJAAN, B.NAMA_KANTOR, B.ALAMAT_KANTOR, B.TELP, B.ALAMAT_RUMAH, B.USER_LEVEL, B.USER_PWD, B.NO_PROP, B.NO_KAB, B.NO_KEC, B.NO_KEL, B.NAMA_DPN, B.USER_SIAK, B.USER_BCARD, B.USER_BENROL, B.IS_MONITORING, B.IS_GISA, B.NO_RW, B.NO_RT, B.IS_ABSEN, B.IS_ASN, B.ABSENSI_CHECKING, B.IS_ACTIVE, B.IPADDRESS_CHECK, B.IS_SHOW, B.CREATED_BY, B.CREATED_DT, B.CHANGED_BY, B.CHANGED_DT FROM SIAK_USER_PLUS B WHERE NOT EXISTS (SELECT 1 FROM SIAK_USER_PLUS_HIST A WHERE A.USER_ID = B.USER_ID))";
            $r = DB::insert($sql);
            return $r;
        }
         public function sett_delete_activity_id($activity_id){
            $sql = "DELETE FROM SIAK_ACTIVITY WHERE ACTIVITY_ID = $activity_id";
            $r = DB::delete($sql);
            return $r;
        }
         public function sett_delete_daily($activity_id){
            $sql = "DELETE FROM SIAK_DAILY WHERE ACTIVITY_ID = $activity_id";
            $r = DB::delete($sql);
            return $r;
        }
        public function sett_backup_daily(){
            $sql = "INSERT INTO SIAK_DAILY_BCK (DAILY_ID, ACTIVITY_ID, USER_ID,START_ACTIVITY,END_ACTIVITY,DESCRIPTION,CREATED_BY,CREATED_DT,NUMBER_ACTIVITY)
                    SELECT DAILY_ID, ACTIVITY_ID, USER_ID,START_ACTIVITY,END_ACTIVITY,DESCRIPTION,CREATED_BY,CREATED_DT,NUMBER_ACTIVITY FROM SIAK_DAILY B WHERE NOT EXISTS(SELECT 1 FROM SIAK_DAILY_BCK C WHERE C.DAILY_ID = B.DAILY_ID)";
            $r = DB::insert($sql);
            return $r;
        }
        public function sett_menu_settting($group_id = 0){
            if ($group_id != 0){
              $sql = "SELECT A.MENU_ID, A.PARENT_ID,CASE WHEN (SELECT B.TITLE FROM SIAK_MENU B WHERE B.MENU_ID = A.PARENT_ID) IS NULL THEN 'Root' ELSE (SELECT B.TITLE FROM SIAK_MENU B WHERE B.MENU_ID = A.PARENT_ID) END PARENT_TITLE, A.TITLE, A.MENU_LEVEL,A.LEVEL_HEAD, A.LEVEL_SUB FROM SIAK_MENU A WHERE 1=1 AND A.MENU_LEVEL = $group_id ORDER BY A.MENU_LEVEL,A.LEVEL_HEAD, A.PARENT_ID,A.LEVEL_SUB";
            }else{
              $sql = "SELECT A.MENU_ID, A.PARENT_ID,CASE WHEN (SELECT B.TITLE FROM SIAK_MENU B WHERE B.MENU_ID = A.PARENT_ID) IS NULL THEN 'Root' ELSE (SELECT B.TITLE FROM SIAK_MENU B WHERE B.MENU_ID = A.PARENT_ID) END PARENT_TITLE, A.TITLE, A.MENU_LEVEL,A.LEVEL_HEAD, A.LEVEL_SUB FROM SIAK_MENU A WHERE 1=1  ORDER BY A.MENU_LEVEL,A.LEVEL_HEAD, A.PARENT_ID,A.LEVEL_SUB";
            }
            
            $r = DB::select($sql);
            return $r;
        }
        public function sett_detail_menu_settting($menu_id = 0){
            $sql = "SELECT A.MENU_ID, A.PARENT_ID,CASE WHEN (SELECT B.TITLE FROM SIAK_MENU B WHERE B.MENU_ID = A.PARENT_ID) IS NULL THEN 'Root' ELSE (SELECT B.TITLE FROM SIAK_MENU B WHERE B.MENU_ID = A.PARENT_ID) END PARENT_TITLE, A.TITLE, A.MENU_LEVEL,A.LEVEL_HEAD, A.LEVEL_SUB FROM SIAK_MENU A WHERE 1=1 AND A.MENU_ID = $menu_id ORDER BY A.MENU_LEVEL,A.LEVEL_HEAD, A.PARENT_ID,A.LEVEL_SUB";
            $r = DB::select($sql);
            return $r;
        }
        public function sett_get_parent($menu_level = 0){
          if ($menu_level == 1){
            $sql = "SELECT 0 MENU_ID, 'Root' TITLE FROM DUAL";
          }else if ($menu_level == 2){
             $sql = "SELECT MENU_ID, TITLE FROM SIAK_MENU WHERE IS_ACTIVE = 0 AND MENU_LEVEL =1 ORDER BY LEVEL_HEAD, LEVEL_SUB";
          }else{
            $sql = "SELECT MENU_ID, TITLE FROM SIAK_MENU WHERE IS_ACTIVE = 0 AND MENU_LEVEL =2 ORDER BY PARENT_ID,LEVEL_HEAD, LEVEL_SUB";
          }
            $r = DB::select($sql);
            return $r;
        }
        public function sett_get_child($menu_id = 0){
            $sql = "SELECT COUNT(1) JML FROM SIAK_MENU WHERE PARENT_ID = $menu_id";
            $r = DB::select($sql);
            return $r[0]->jml;
          }
        public function sett_do_edit_menu_id($menu_id,$title,$parent_id,$menu_level,$level_head,$level_sub){
            $sql = "UPDATE SIAK_MENU 
                       SET 
                       PARENT_ID = $parent_id
                       , TITLE = '$title'
                       , MENU_LEVEL = $menu_level
                       , LEVEL_HEAD = $level_head
                       , LEVEL_SUB = $level_sub
                     WHERE MENU_ID = $menu_id";
            $r = DB::update($sql);
            return $r;
        }
        public function sett_max_helpdesk(){
            $sql = "SELECT MAX(HELPDESK_ID) + 1 JML FROM SIAK_MASTER_HELPDESK";
            $r = DB::select($sql);
            return $r[0]->jml;
        }

        public function sett_save_helpdesk($helpdesk_cd,$helpdesk_nm,$user_id){
            $sql = "INSERT INTO SIAK_MASTER_HELPDESK (HELPDESK_ID,HELPDESK_DESCRIPTION,CREATED_DT,CREATED_BY) VALUES ($helpdesk_cd,'$helpdesk_nm',SYSDATE,'$user_id')";
            $r = DB::insert($sql);
            return $r;
        }

        public function sett_get_helpdesk($helpdesk_id){
            $sql = "SELECT A.HELPDESK_ID, A.HELPDESK_DESCRIPTION FROM SIAK_MASTER_HELPDESK A  WHERE A.HELPDESK_ID = $helpdesk_id";
            $r = DB::select($sql);
            return $r;
        }
        public function sett_get_all_helpdesk(){
            $sql = "SELECT A.HELPDESK_ID, A.HELPDESK_DESCRIPTION FROM SIAK_MASTER_HELPDESK A";
            $r = DB::select($sql);
            return $r;
        }

        public function sett_edit_helpdesk_cd($helpdesk_cd,$helpdesk_nm){
            $sql = "UPDATE SIAK_MASTER_HELPDESK 
                       SET 
                       HELPDESK_DESCRIPTION = UPPER('$helpdesk_nm')
                     WHERE HELPDESK_ID = $helpdesk_cd";
            $r = DB::update($sql);
            return $r;
        }

        public function sett_delete_helpdesk($helpdesk_cd){
            $sql = "DELETE FROM SIAK_MASTER_HELPDESK WHERE HELPDESK_ID = $helpdesk_cd";
            $r = DB::delete($sql);
            return $r;
        }
}
