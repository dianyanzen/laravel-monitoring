<?php

namespace App\Http\Controllers;

use Auth;
use DB;
use Session;
use DateTime;
use PDF;
use Validator;
use App\User; 
use App\Message;
use App\Notification;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\cache;
use Intervention\Image\Facades\Image;
use App\Http\Controllers\SharedController as Shr;
use App\Http\Controllers\AndroidApiController as AndrApi;
use Spatie\Browsershot\Browsershot;
use App\Events\NewMessageNotification;


// use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class AndroidApiController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
   public $successStatus = 200;

   public static function get_data_suket(Request $request)
	{
		header('Content-type: application/json');
		$nik = $request->nik;
		$count = AndrApi::get_count_suket($nik);	
		if ($count >0){
			$output = AndrApi::get_data_suket_andro($nik);
			$data["NIK"] = $output[0]->nik;
			$data["NO_KK"] = $output[0]->no_kk;
			$data["NAMA_LGKP"] = $output[0]->nama_lgkp;
			$data["TMPT_LHR"] = $output[0]->tmpt_lhr;
			$data["TGL_LHR"] = $output[0]->tgl_lhr;
			$data["JENIS_KLMIN"] = $output[0]->jenis_klmin;
			$data["STAT_HBKEL"] = $output[0]->stat_hbkel;
			$data["AKTA_LHR"] = $output[0]->akta_lhr;
			$data["NO_AKTA_LHR"] = $output[0]->no_akta_lhr;
			$data["ALAMAT"] = $output[0]->alamat;
			$data["RT"] = $output[0]->rt;
			$data["RW"] = $output[0]->rw;
			$data["NAMA_KEC"] = $output[0]->nama_kec;
			$data["NAMA_KEL"] = $output[0]->nama_kel;
			$data["AGAMA"] = $output[0]->agama;
			$data["JENIS_PKRJN"] = $output[0]->jenis_pkrjn;
			$data["STAT_KWN"] = $output[0]->stat_kwn;
			$data["NO_AKTA_KWN"] = $output[0]->no_akta_kwn;
			$data["CURRENT_STATUS_CODE"] = $output[0]->current_status_code;
			$data["SUKET_BY"] = $output[0]->suket_by;
			$data["SUKET_DT"] = $output[0]->suket_dt;
			$data["KTP_BY"] = ($output[0]->ktp_by == '32730000yanzen') ? '32730000dns' : $output[0]->ktp_by;
			$data["KTP_DT"] = $output[0]->ktp_dt;
			$response["data"] = $data;
			$response["jumlah"] = $count;
		}else{
			$response["data"] = "";
			$response["jumlah"] = 0;
		}
        return $response;
	}
	public static function listen()
	{
		header('Content-type: application/json');
			$response["data"] = 1;
        return $response;
	}

    //Region Api Dashboard
    public static function get_dashboard_time()
	{
		header('Content-type: application/json');
		$get_dashboard = DB::select("SELECT TO_CHAR(MAX(CREATED_DT),'DD-MM-YYYY') LAST_UPDATE, TO_CHAR(MAX(CREATED_DT),'HH24:MI:SS') LAST_UPDATE_TIME,TO_CHAR(MAX(CREATED_DT),'YYYY-MM-DD HH24:MI:SS') LAST_UPDATE_FULLTIME  FROM SIAK_DASHBOARD");
		$response["dashboard_date"] = $get_dashboard[0]->last_update;
		$response["dashboard_time"] = $get_dashboard[0]->last_update_time;
		$time = new DateTime($get_dashboard[0]->last_update_fulltime);
		$diff = $time->diff(new DateTime()); 
		$minutes = ($diff->days * 24 * 60) + ($diff->h * 60) + $diff->i;
		if ($minutes > 10){
			$response["dashboard_between"] = 0;
			$response["dashboard_minutes"] = $minutes;
		}else{
			$response["dashboard_between"] = 1;
			$response["dashboard_minutes"] = $minutes;
		}

        return $response;
	}
    public static function get_dashboard_m1()
	{
		header('Content-type: application/json');
		$get_dashboard = DB::select("SELECT CASE WHEN SUM(VAL) IS NULL THEN 0 ELSE SUM(VAL) END JML FROM VW_SIAK_DASHBOARD WHERE ID = 'GET_PEREKAMAN'");
		$response["perekaman_today"] = $get_dashboard[0]->jml;
        return $response;
	}
    public static function get_dashboard_m2()
	{
		header('Content-type: application/json');
		$get_dashboard = DB::select("SELECT 
				CASE WHEN (CASE WHEN (SELECT (D.AMBIL-D.RUSAK-D.PENGEMBALIAN) CETAK_LUDO FROM SIAK_MONEV_BLANGKO_LUDO D WHERE TRUNC(D.TANGGAL) = TRUNC(SYSDATE)) IS NOT NULL THEN 
				(CASE WHEN B.VAL IS NULL THEN 0 ELSE B.VAL END) + 
				(SELECT (D.AMBIL-D.RUSAK-D.PENGEMBALIAN) CETAK_LUDO FROM SIAK_MONEV_BLANGKO_LUDO D WHERE TRUNC(D.TANGGAL) = TRUNC(SYSDATE)) ELSE CASE WHEN B.VAL IS NULL THEN 0 ELSE B.VAL END END) IS NULL THEN 0 ELSE (CASE WHEN  (SELECT (D.AMBIL-D.RUSAK-D.PENGEMBALIAN) CETAK_LUDO FROM SIAK_MONEV_BLANGKO_LUDO D WHERE TRUNC(D.TANGGAL) = TRUNC(SYSDATE)) IS NOT NULL THEN 
				(CASE WHEN B.VAL IS NULL THEN 0 ELSE B.VAL END) + 
				(SELECT (D.AMBIL-D.RUSAK-D.PENGEMBALIAN) CETAK_LUDO FROM SIAK_MONEV_BLANGKO_LUDO D WHERE TRUNC(D.TANGGAL) = TRUNC(SYSDATE)) ELSE CASE WHEN B.VAL IS NULL THEN 0 ELSE B.VAL END END) END AS JML
				FROM VW_SIAK_DASHBOARD B WHERE ID = 'GET_PENCETAKAN_KTP'");
		$response["pencetakan_today"] = $get_dashboard[0]->jml;
        return $response;
	}
	public static function update_prr(Request $request) 
	{
			header('Content-type: application/json');
			DB::delete("DELETE FROM SIAK_DASHBOARD WHERE ID = 'GET_PRR' AND TRUNC(CREATED_DT) = TRUNC(SYSDATE)");
			$data["success"] = TRUE;
			$data["message"] = "Data Berhasil Di Hapus";
       		return $data;
	}
	public static function update_sfe(Request $request) 
	{
			header('Content-type: application/json');
			DB::delete("DELETE FROM SIAK_DASHBOARD WHERE ID = 'GET_SFE' AND TRUNC(CREATED_DT) = TRUNC(SYSDATE)");
			$data["success"] = TRUE;
			$data["message"] = "Data Berhasil Di Hapus";
       		return $data;
	}
	public static function get_time_m3()
	{
		header('Content-type: application/json');
		$get_dashboard = DB::select("SELECT NVL(MAX(TO_CHAR(CREATED_DT,'HH24:MI')),'00:00') JML FROM SIAK_DASHBOARD WHERE ID = 'GET_PRR' AND TRUNC(CREATED_DT) = TRUNC(SYSDATE)");
		$response["jam_prr"] = $get_dashboard[0]->jml;
        return $response;
	}
	public static function get_time_m4()
	{
		header('Content-type: application/json');
		$get_dashboard = DB::select("SELECT NVL(MAX(TO_CHAR(CREATED_DT,'HH24:MI')),'00:00') JML FROM SIAK_DASHBOARD WHERE ID = 'GET_SFE' AND TRUNC(CREATED_DT) = TRUNC(SYSDATE)");
		$response["jam_sfe"] = $get_dashboard[0]->jml;
        return $response;
	}
    public static function get_dashboard_m3()
	{
		header('Content-type: application/json');
		$get_dashboard = DB::select("SELECT CASE WHEN SUM(VAL) IS NULL THEN 0 ELSE SUM(VAL) END JML FROM VW_SIAK_DASHBOARD WHERE ID = 'GET_PRR'");
		$response["sisa_prr"] = $get_dashboard[0]->jml;
        return $response;
	}
    public static function get_dashboard_m4()
	{
		header('Content-type: application/json');
		$get_dashboard = DB::select("SELECT CASE WHEN SUM(VAL) IS NULL THEN 0 ELSE SUM(VAL) END JML FROM VW_SIAK_DASHBOARD WHERE ID = 'GET_SFE'");
		$response["sisa_sfe"] = $get_dashboard[0]->jml;
        return $response;
	}
    public static function get_dashboard_m5()
	{
		header('Content-type: application/json');
		$get_dashboard = DB::select("SELECT CASE WHEN SUM(VAL) IS NULL THEN 0 ELSE SUM(VAL) END JML FROM VW_SIAK_DASHBOARD WHERE ID = 'GET_SISA_SUKET'");
		$response["sisa_suket"] = $get_dashboard[0]->jml;
        return $response;
	}
    public static function get_dashboard_m6()
	{
		header('Content-type: application/json');
		$get_dashboard = DB::select("SELECT CASE WHEN SUM(VAL) IS NULL THEN 0 ELSE SUM(VAL) END JML FROM VW_SIAK_DASHBOARD WHERE ID = 'GET_BLANGKO_OUT'");
		$response["blangko_out"] = $get_dashboard[0]->jml;
        return $response;
	}
    public static function get_dashboard_m7()
	{
		header('Content-type: application/json');
		$get_dashboard = DB::select("SELECT CASE WHEN SUM(VAL) IS NULL THEN 0 ELSE SUM(VAL) END JML FROM VW_SIAK_DASHBOARD WHERE ID = 'GET_DUPLICATE'");
		$response["duplicate"] = $get_dashboard[0]->jml;
        return $response;
	}
    public static function get_dashboard_m8()
	{
		header('Content-type: application/json');
		$get_dashboard = DB::select("SELECT CASE WHEN SUM(VAL) IS NULL THEN 0 ELSE SUM(VAL) END JML FROM VW_SIAK_DASHBOARD WHERE ID = 'GET_BLANGKO_SISA'");
		$response["sisa_blangko"] = $get_dashboard[0]->jml;
        return $response;
	}
    public static function get_dashboard_kk()
	{
		header('Content-type: application/json');
		$get_dashboard = DB::select("SELECT CASE WHEN SUM(VAL) IS NULL THEN 0 ELSE SUM(VAL) END JML FROM VW_SIAK_DASHBOARD WHERE ID = 'GET_CETAK_KK'");
		$response["pen_kk"] = $get_dashboard[0]->jml;
        return $response;
	}
    public static function get_dashboard_kia()
	{
		header('Content-type: application/json');
		$get_dashboard = DB::select("SELECT CASE WHEN SUM(VAL) IS NULL THEN 0 ELSE SUM(VAL) END JML FROM VW_SIAK_DASHBOARD WHERE ID = 'GET_CETAK_KIA'");
		$response["pen_kia"] = $get_dashboard[0]->jml;
        return $response;
	}
    public static function get_dashboard_nik()
	{
		header('Content-type: application/json');
		$get_dashboard = DB::select("SELECT CASE WHEN SUM(VAL) IS NULL THEN 0 ELSE SUM(VAL) END JML FROM VW_SIAK_DASHBOARD WHERE ID = 'GET_NIK_BARU'");
		$response["pen_nik_baru"] = $get_dashboard[0]->jml;
        return $response;
	}
    public static function get_dashboard_akta_lu()
	{
		header('Content-type: application/json');
		$get_dashboard = DB::select("SELECT CASE WHEN SUM(VAL) IS NULL THEN 0 ELSE SUM(VAL) END JML FROM VW_SIAK_DASHBOARD WHERE ID = 'GET_AKTA_LHR_UM'");
		$response["pen_lahir_lu"] = $get_dashboard[0]->jml;
        return $response;
	}
    public static function get_dashboard_akta_lt()
	{
		header('Content-type: application/json');
		$get_dashboard = DB::select("SELECT CASE WHEN SUM(VAL) IS NULL THEN 0 ELSE SUM(VAL) END JML FROM VW_SIAK_DASHBOARD WHERE ID = 'GET_AKTA_LHR_LT'");
		$response["pen_lahir_lt"] = $get_dashboard[0]->jml;
        return $response;
	}
    public static function get_dashboard_akta_mt()
	{
		header('Content-type: application/json');
		$get_dashboard = DB::select("SELECT CASE WHEN SUM(VAL) IS NULL THEN 0 ELSE SUM(VAL) END JML FROM VW_SIAK_DASHBOARD WHERE ID = 'GET_AKTA_MT'");
		$response["pen_mati"] = $get_dashboard[0]->jml;
        return $response;
	}
    public static function get_dashboard_akta_kwn()
	{
		header('Content-type: application/json');
		$get_dashboard = DB::select("SELECT CASE WHEN SUM(VAL) IS NULL THEN 0 ELSE SUM(VAL) END JML FROM VW_SIAK_DASHBOARD WHERE ID = 'GET_AKTA_KWN'");
		$response["pen_kawin"] = $get_dashboard[0]->jml;
        return $response;
	}
    public static function get_dashboard_akta_cry()
	{
		header('Content-type: application/json');
		$get_dashboard = DB::select("SELECT CASE WHEN SUM(VAL) IS NULL THEN 0 ELSE SUM(VAL) END JML FROM VW_SIAK_DASHBOARD WHERE ID = 'GET_AKTA_CRY'");
		$response["pen_cerai"] = $get_dashboard[0]->jml;
        return $response;
	}
    public static function get_dashboard_pdh_akab()
	{
		header('Content-type: application/json');
		$get_dashboard = DB::select("SELECT CASE WHEN SUM(VAL) IS NULL THEN 0 ELSE SUM(VAL) END JML FROM VW_SIAK_DASHBOARD WHERE ID = 'GET_PINDAH_ANTAR_KAB'");
		$response["pen_pindah_akab"] = $get_dashboard[0]->jml;
        return $response;
	}
    public static function get_dashboard_pdh_akec()
	{
		header('Content-type: application/json');
		$get_dashboard = DB::select("SELECT CASE WHEN SUM(VAL) IS NULL THEN 0 ELSE SUM(VAL) END JML FROM VW_SIAK_DASHBOARD WHERE ID = 'GET_PINDAH_ANTAR_KEC'");
		$response["pen_pindah_akec"] = $get_dashboard[0]->jml;
        return $response;
	}
    public static function get_dashboard_pdh_dkec()
	{
		header('Content-type: application/json');
		$get_dashboard = DB::select("SELECT CASE WHEN SUM(VAL) IS NULL THEN 0 ELSE SUM(VAL) END JML FROM VW_SIAK_DASHBOARD WHERE ID = 'GET_PINDAH_DALAM_KEC'");
		$response["pen_pindah_dkec"] = $get_dashboard[0]->jml;
        return $response;
	}
    public static function get_dashboard_dtg_akab()
	{
		header('Content-type: application/json');
		$get_dashboard = DB::select("SELECT CASE WHEN SUM(VAL) IS NULL THEN 0 ELSE SUM(VAL) END JML FROM VW_SIAK_DASHBOARD WHERE ID = 'GET_DATANG_ANTAR_KAB'");
		$response["pen_datang_akab"] = $get_dashboard[0]->jml;
        return $response;
	}
    public static function get_dashboard_dtg_akec()
	{
		header('Content-type: application/json');
		$get_dashboard = DB::select("SELECT CASE WHEN SUM(VAL) IS NULL THEN 0 ELSE SUM(VAL) END JML FROM VW_SIAK_DASHBOARD WHERE ID = 'GET_DATANG_ANTAR_KEC'");
		$response["pen_datang_akec"] = $get_dashboard[0]->jml;
        return $response;
	}
    public static function get_dashboard_dtg_dkec()
	{
		header('Content-type: application/json');
		$get_dashboard = DB::select("SELECT CASE WHEN SUM(VAL) IS NULL THEN 0 ELSE SUM(VAL) END JML FROM VW_SIAK_DASHBOARD WHERE ID = 'GET_DATANG_DALAM_KEC'");
		$response["pen_datang_dkec"] = $get_dashboard[0]->jml;
        return $response;
	}
	//End Region

	//Region Api Dashboard Ktpel
	public static function get_data_rkm()
	{
		header('Content-type: application/json');
		 $sql = "SELECT 
                        SUM( CASE WHEN TO_CHAR(CREATED,'DD/MM/YYYY') = 
                        TO_CHAR(SYSDATE,'DD/MM/YYYY') THEN 1 ELSE 0 END) AS TODAY 
                        , SUM( CASE WHEN CURRENT_STATUS_CODE = 'BIO_CAPTURED' AND TO_CHAR(CREATED,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') THEN 1 ELSE 0 END) AS N_BIO_CAPTURED
                        , SUM( CASE WHEN CURRENT_STATUS_CODE = 'SENT_FOR_ENROLLMENT' AND TO_CHAR(CREATED,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') THEN 1 ELSE 0 END) AS N_SENT_FOR_ENROLLMENT
                        , SUM( CASE WHEN CURRENT_STATUS_CODE = 'PRINT_READY_RECORD' AND TO_CHAR(CREATED,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') THEN 1 ELSE 0 END) AS N_PRINT_READY_RECORD
                        , SUM( CASE WHEN CURRENT_STATUS_CODE = 'DUPLICATE_RECORD' AND TO_CHAR(CREATED,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') THEN 1 ELSE 0 END) AS N_DUPLICATE_RECORD
                        , SUM( CASE WHEN CURRENT_STATUS_CODE = 'PROCESSING' AND TO_CHAR(CREATED,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') THEN 1 ELSE 0 END) AS N_PROCESSING
                        , SUM( CASE WHEN CURRENT_STATUS_CODE = 'ADJUDICATE_RECORD' AND TO_CHAR(CREATED,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') THEN 1 ELSE 0 END) AS N_ADJUDICATE_RECORD
                        , SUM( CASE WHEN CURRENT_STATUS_CODE = 'ENROLL_FAILURE_AT_CENTRAL' AND TO_CHAR(CREATED,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') THEN 1 ELSE 0 END) AS N_ENROLL_FAILURE_AT_CENTRAL
                        , SUM(CASE WHEN CURRENT_STATUS_CODE <> 'SENT_FOR_ENROLLMENT' AND CURRENT_STATUS_CODE <> 'BIO_CAPTURED' AND CURRENT_STATUS_CODE <> 'DUPLICATE_RECORD' AND CURRENT_STATUS_CODE <> 'PRINT_READY_RECORD' AND CURRENT_STATUS_CODE <> 'PROCESSING' AND CURRENT_STATUS_CODE <> 'ADJUDICATE_RECORD' AND CURRENT_STATUS_CODE <> 'ENROLL_FAILURE_AT_CENTRAL' AND TO_CHAR(CREATED,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') THEN 1 ELSE 0 END) AS N_OTHER 
                        , SUM( CASE WHEN TO_CHAR(CREATED,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') THEN 1 ELSE 0 END) AS YESTERDAY
                        , SUM( CASE WHEN CURRENT_STATUS_CODE = 'BIO_CAPTURED' AND TO_CHAR(CREATED,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') THEN 1 ELSE 0 END) AS Y_BIO_CAPTURED
                        , SUM( CASE WHEN CURRENT_STATUS_CODE = 'SENT_FOR_ENROLLMENT' AND TO_CHAR(CREATED,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') THEN 1 ELSE 0 END) AS Y_SENT_FOR_ENROLLMENT
                        , SUM( CASE WHEN CURRENT_STATUS_CODE = 'PRINT_READY_RECORD' AND TO_CHAR(CREATED,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') THEN 1 ELSE 0 END) AS Y_PRINT_READY_RECORD
                        , SUM( CASE WHEN CURRENT_STATUS_CODE = 'DUPLICATE_RECORD' AND TO_CHAR(CREATED,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') THEN 1 ELSE 0 END) AS Y_DUPLICATE_RECORD
                        , SUM( CASE WHEN CURRENT_STATUS_CODE = 'PROCESSING' AND TO_CHAR(CREATED,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') THEN 1 ELSE 0 END) AS Y_PROCESSING
                        , SUM( CASE WHEN CURRENT_STATUS_CODE = 'ADJUDICATE_RECORD' AND TO_CHAR(CREATED,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') THEN 1 ELSE 0 END) AS Y_ADJUDICATE_RECORD
                        , SUM( CASE WHEN CURRENT_STATUS_CODE = 'ENROLL_FAILURE_AT_CENTRAL' AND TO_CHAR(CREATED,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') THEN 1 ELSE 0 END) AS Y_ENROLL_FAILURE_AT_CENTRAL
                        , SUM(CASE WHEN CURRENT_STATUS_CODE <> 'SENT_FOR_ENROLLMENT' AND CURRENT_STATUS_CODE <> 'BIO_CAPTURED' AND CURRENT_STATUS_CODE <> 'DUPLICATE_RECORD' AND CURRENT_STATUS_CODE <> 'PRINT_READY_RECORD' AND CURRENT_STATUS_CODE <> 'PROCESSING' AND CURRENT_STATUS_CODE <> 'ADJUDICATE_RECORD' AND CURRENT_STATUS_CODE <> 'ENROLL_FAILURE_AT_CENTRAL' AND TO_CHAR(CREATED,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') THEN 1 ELSE 0 END) AS Y_OTHER 
                        FROM DEMOGRAPHICS WHERE EXISTS(SELECT 1 FROM FACES WHERE DEMOGRAPHICS.NIK = FACES.NIK)";
		 $r = DB::connection('db221')->select($sql);
            $output = array(
               "rekam_today"=>$r[0]->today,
                "rekam_yesterday"=>$r[0]->yesterday,
                "bio_today"=>$r[0]->n_bio_captured,
                "bio_yesterday"=>$r[0]->y_bio_captured,
                "sfe_today"=>$r[0]->n_sent_for_enrollment,
                "sfe_yesterday"=>$r[0]->y_sent_for_enrollment,
                "prr_today"=>$r[0]->n_print_ready_record,
                "prr_yesterday"=>$r[0]->y_print_ready_record,
                "durec_today"=>$r[0]->n_duplicate_record,
                "durec_yesterday"=>$r[0]->y_duplicate_record,
                "process_today"=>$r[0]->n_processing,
                "process_yesterday"=>$r[0]->y_processing,
                "adjudicate_today"=>$r[0]->n_adjudicate_record,
                "adjudicate_yesterday"=>$r[0]->y_adjudicate_record,
                "enroll_failure_today"=>$r[0]->n_enroll_failure_at_central,
                "enroll_failure_yesterday"=>$r[0]->y_enroll_failure_at_central,
                "other_today"=>$r[0]->n_other,
                "other_yesterday"=>$r[0]->y_other
            );
        return $output;
	}
	public static function get_data_ctk()
	{
		header('Content-type: application/json');
		 $sql = "SELECT 
                        SUM( CASE WHEN (CASE WHEN TO_CHAR(A.LAST_UPDATE,'DD/MM/YYYY') IS NULL THEN TO_CHAR(A.PERSONALIZED_DATE,'DD/MM/YYYY') ELSE TO_CHAR(A.LAST_UPDATE,'DD/MM/YYYY') END =  TO_CHAR(SYSDATE,'DD/MM/YYYY')) THEN 1 ELSE 0 END) AS HARI_INI, 
                        SUM( CASE WHEN (CASE WHEN TO_CHAR(A.LAST_UPDATE,'DD/MM/YYYY') IS NULL THEN TO_CHAR(A.PERSONALIZED_DATE,'DD/MM/YYYY') ELSE TO_CHAR(A.LAST_UPDATE,'DD/MM/YYYY') END =  TO_CHAR(SYSDATE-1,'DD/MM/YYYY')) THEN 1 ELSE 0 END) AS KEMARIN
                        FROM CARD_MANAGEMENT@DB5CETAK A INNER JOIN BIODATA_WNI B ON A.NIK = B.NIK LEFT JOIN SIAK_CETAK_GOIB@YZDB C ON  CASE WHEN UPPER(A.LAST_UPDATED_USERNAME) IS NULL THEN UPPER(A.CREATED_USERNAME) ELSE UPPER(A.LAST_UPDATED_USERNAME) END =  UPPER(C.USER_BCARD) AND C.IS_ACTIVE = 1  WHERE 1=1 AND C.USER_BCARD IS NULL
                        ";
		 $r = DB::connection('db222')->select($sql);
		 $output = array(
            "cetak_today"=>$r[0]->hari_ini,
            "cetak_yesterday"=>$r[0]->kemarin
         );
        return $output;
	}
	public static function get_data_suket_dashboard()
	{
		header('Content-type: application/json');
		$sql = "SELECT 
                            (SELECT COUNT(DISTINCT(A.NIK)) AS JUMLAH FROM T7_HIST_SUKET A WHERE TO_CHAR(A.PRINTED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY')) N_SUKET,
                            (SELECT COUNT(DISTINCT(A.NIK)) AS JUMLAH FROM T7_HIST_SUKET A WHERE TO_CHAR(A.PRINTED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND NOT EXISTS(SELECT 1 FROM SIAK_REQ_CETAK_KTP@YZDB B WHERE A.NIK = B.NIK AND TO_CHAR(B.REQ_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY')) AND NOT EXISTS(SELECT 1 FROM SIAK_REQ_CETAK_KTP@YZDB B WHERE A.NIK = B.NIK AND TO_CHAR(B.REQ_DATE,'DD/MM/YYYY') <> TO_CHAR(SYSDATE,'DD/MM/YYYY'))) N_NREQ_JUMLAH,
                            (SELECT COUNT(DISTINCT(A.NIK)) AS JUMLAH FROM T7_HIST_SUKET A WHERE TO_CHAR(A.PRINTED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND NOT EXISTS(SELECT 1 FROM SIAK_REQ_CETAK_KTP@YZDB B WHERE A.NIK = B.NIK AND TO_CHAR(B.REQ_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY')) AND EXISTS(SELECT 1 FROM SIAK_REQ_CETAK_KTP@YZDB B WHERE A.NIK = B.NIK AND TO_CHAR(B.REQ_DATE,'DD/MM/YYYY') <> TO_CHAR(SYSDATE,'DD/MM/YYYY'))) N_DREQ_JUMLAH,
                            (SELECT COUNT(DISTINCT(A.NIK)) AS JUMLAH  FROM T7_HIST_SUKET A WHERE TO_CHAR(A.PRINTED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND EXISTS(SELECT 1 FROM SIAK_REQ_CETAK_KTP@YZDB B WHERE A.NIK = B.NIK AND TO_CHAR(B.REQ_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY'))) AS N_REQ_CETAK,
                            (SELECT COUNT(DISTINCT(A.NIK)) AS JUMLAH  FROM SIAK_REQ_CETAK_KTP@YZDB A WHERE TO_CHAR(A.REQ_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY')) AS N_REQ_JUMLAH,
                            (SELECT COUNT(DISTINCT(A.NIK)) AS JUMLAH  FROM SIAK_REQ_CETAK_KTP@YZDB A WHERE TO_CHAR(A.REQ_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND NOT EXISTS(SELECT 1 FROM T7_HIST_SUKET B WHERE A.NIK = B.NIK AND TO_CHAR(B.PRINTED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY'))) AS N_REQ_BELUM,
                            (SELECT COUNT(DISTINCT(A.NIK)) AS JUMLAH FROM T7_HIST_SUKET A WHERE TO_CHAR(A.PRINTED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY')) Y_SUKET,
                            (SELECT COUNT(DISTINCT(A.NIK)) AS JUMLAH FROM T7_HIST_SUKET A WHERE TO_CHAR(A.PRINTED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') AND NOT EXISTS(SELECT 1 FROM SIAK_REQ_CETAK_KTP@YZDB B WHERE A.NIK = B.NIK AND TO_CHAR(B.REQ_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY')) AND NOT EXISTS(SELECT 1 FROM SIAK_REQ_CETAK_KTP@YZDB B WHERE A.NIK = B.NIK AND TO_CHAR(B.REQ_DATE,'DD/MM/YYYY') <> TO_CHAR(SYSDATE-1,'DD/MM/YYYY'))) Y_NREQ_JUMLAH,
                            (SELECT COUNT(DISTINCT(A.NIK)) AS JUMLAH FROM T7_HIST_SUKET A WHERE TO_CHAR(A.PRINTED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') AND NOT EXISTS(SELECT 1 FROM SIAK_REQ_CETAK_KTP@YZDB B WHERE A.NIK = B.NIK AND TO_CHAR(B.REQ_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY')) AND EXISTS(SELECT 1 FROM SIAK_REQ_CETAK_KTP@YZDB B WHERE A.NIK = B.NIK AND TO_CHAR(B.REQ_DATE,'DD/MM/YYYY') <> TO_CHAR(SYSDATE-1,'DD/MM/YYYY'))) Y_DREQ_JUMLAH,
                            (SELECT COUNT(DISTINCT(A.NIK)) AS JUMLAH  FROM T7_HIST_SUKET A WHERE TO_CHAR(A.PRINTED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') AND EXISTS(SELECT 1 FROM SIAK_REQ_CETAK_KTP@YZDB B WHERE A.NIK = B.NIK AND TO_CHAR(B.REQ_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY'))) AS Y_REQ_CETAK,
                            (SELECT COUNT(DISTINCT(A.NIK)) AS JUMLAH  FROM SIAK_REQ_CETAK_KTP@YZDB A WHERE TO_CHAR(A.REQ_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY')) AS Y_REQ_JUMLAH,
                            (SELECT COUNT(DISTINCT(A.NIK)) AS JUMLAH  FROM SIAK_REQ_CETAK_KTP@YZDB A WHERE TO_CHAR(A.REQ_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') AND NOT EXISTS(SELECT 1 FROM T7_HIST_SUKET B WHERE A.NIK = B.NIK AND TO_CHAR(B.PRINTED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY'))) AS Y_REQ_BELUM
                            FROM DUAL";
		 $r = DB::connection('db222')->select($sql);
		  $output = array(
                   "suket_today"=>$r[0]->n_suket,
                    "suket_yesterday"=>$r[0]->y_suket,
                    "dreq_jumlah_yesterday"=>$r[0]->y_dreq_jumlah,
                    "dreq_jumlah_today"=>$r[0]->n_dreq_jumlah,
                    "nreq_jumlah_today"=>$r[0]->n_nreq_jumlah,
                    "nreq_jumlah_yesterday"=>$r[0]->y_nreq_jumlah,
                    "req_jumlah_today"=>$r[0]->n_req_jumlah,
                    "req_jumlah_yesterday"=>$r[0]->y_req_jumlah,
                    "req_cetak_today"=>$r[0]->n_req_cetak,
                    "req_cetak_yesterday"=>$r[0]->y_req_cetak,
                    "req_belum_today"=>$r[0]->n_req_belum,
                    "req_belum_yesterday"=>$r[0]->y_req_belum
                );
        return $output;
	}
	public static function get_data_kia()
	{
		header('Content-type: application/json');
		$sql = "SELECT 
                        SUM( CASE WHEN TO_CHAR(A.PRINTED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') THEN 1 ELSE 0 END) AS HARI_INI, 
                        SUM( CASE WHEN TO_CHAR(A.PRINTED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') THEN 1 ELSE 0 END) AS KEMARIN
                        FROM T5_SEQN_KIA_PRINT A WHERE EXISTS(SELECT 1 FROM BIODATA_WNI B WHERE A.NIK =B.NIK) 
                        ";
		 $r = DB::connection('db222')->select($sql);
		 $output = array(
                   "kia_today"=>$r[0]->hari_ini,
                    "kia_yesterday"=>$r[0]->kemarin
                );
        return $output;
	}

	public static function dashboard_biodata()
	{
		header('Content-type: application/json');
		 $sql = "SELECT
                        (SELECT COUNT(DISTINCT(B.NO_KK)) AS JUMLAH from DATA_KELUARGA B INNER JOIN BIODATA_WNI A ON A.NO_KK = B.NO_KK WHERE 
                        A.FLAG_STATUS = 0 AND TO_CHAR(B.TGL_INSERTION,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY')) N_KK,
                        (select COUNT(1) AS COUNT from BIODATA_WNI WHERE TO_CHAR(TGL_ENTRI,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY')) N_BIO,
                        (select COUNT(DISTINCT(B.NO_KK)) AS JUMLAH from DATA_KELUARGA B INNER JOIN BIODATA_WNI A ON A.NO_KK = B.NO_KK WHERE 
                        A.FLAG_STATUS = 0 AND TO_CHAR(B.TGL_INSERTION,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY')) Y_KK,
                        (select COUNT(1) AS COUNT from BIODATA_WNI WHERE TO_CHAR(TGL_ENTRI,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY')) Y_BIO
                        FROM DUAL
                        ";
		 $r = DB::connection('db222')->select($sql);
		$output = array(
                   "kk_n"=>$r[0]->n_kk,
                   "bio_n"=>$r[0]->n_bio,
                   "kk_y"=>$r[0]->y_kk,
                   "bio_y"=>$r[0]->y_bio
                  
                );
        return $output;
	}

	public static function dashboard_mobilitas()
	{
		header('Content-type: application/json');
		 $sql = "SELECT
                        (SELECT COUNT(DISTINCT(A.NO_PINDAH)) AS COUNT FROM PINDAH_HEADER A INNER JOIN PINDAH_DETAIL B ON A.NO_PINDAH = B.NO_PINDAH
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND A.FROM_NO_PROP = 32 AND A.FROM_NO_KAB = 73) N_PINDAH_H,
                        (SELECT COUNT(A.NO_PINDAH) AS COUNT FROM PINDAH_HEADER A INNER JOIN PINDAH_DETAIL B ON A.NO_PINDAH = B.NO_PINDAH
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND A.FROM_NO_PROP = 32 AND A.FROM_NO_KAB = 73) N_PINDAH_D,
                        (SELECT COUNT(DISTINCT(A.NO_DATANG)) AS COUNT FROM DATANG_HEADER A INNER JOIN DATANG_DETAIL B ON A.NO_DATANG = B.NO_DATANG
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND A.NO_PROP = 32 AND A.NO_KAB = 73) N_DATANG_H,
                        (SELECT COUNT(A.NO_DATANG) AS COUNT FROM DATANG_HEADER A INNER JOIN DATANG_DETAIL B ON A.NO_DATANG = B.NO_DATANG
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND A.NO_PROP = 32 AND A.NO_KAB = 73) N_DATANG_D,
                        (SELECT COUNT(DISTINCT(A.NO_PINDAH)) AS COUNT FROM PINDAH_HEADER A INNER JOIN PINDAH_DETAIL B ON A.NO_PINDAH = B.NO_PINDAH
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') AND A.FROM_NO_PROP = 32 AND A.FROM_NO_KAB = 73) Y_PINDAH_H,
                        (SELECT COUNT(A.NO_PINDAH) AS COUNT FROM PINDAH_HEADER A INNER JOIN PINDAH_DETAIL B ON A.NO_PINDAH = B.NO_PINDAH
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') AND A.FROM_NO_PROP = 32 AND A.FROM_NO_KAB = 73) Y_PINDAH_D,
                        (SELECT COUNT(DISTINCT(A.NO_DATANG)) AS COUNT FROM DATANG_HEADER A INNER JOIN DATANG_DETAIL B ON A.NO_DATANG = B.NO_DATANG
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') AND A.NO_PROP = 32 AND A.NO_KAB = 73) Y_DATANG_H,
                        (SELECT COUNT(A.NO_DATANG) AS COUNT FROM DATANG_HEADER A INNER JOIN DATANG_DETAIL B ON A.NO_DATANG = B.NO_DATANG
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') AND A.NO_PROP = 32 AND A.NO_KAB = 73) Y_DATANG_D
                        FROM DUAL
                        ";
		 $r = DB::connection('db222')->select($sql);
		$output = array(
                   "pindah_hn"=>$r[0]->n_pindah_h,
                   "pindah_dn"=>$r[0]->n_pindah_d,
                   "datang_hn"=>$r[0]->n_datang_h,
                   "datang_dn"=>$r[0]->n_datang_d,
                   "pindah_hy"=>$r[0]->y_pindah_h,
                   "pindah_dy"=>$r[0]->y_pindah_d,
                   "datang_hy"=>$r[0]->y_datang_h,
                   "datang_dy"=>$r[0]->y_datang_d
                );
        return $output;
	}

	public static function dashboard_mobilitas_detail()
	{
		header('Content-type: application/json');
		 $sql = "SELECT
                        (SELECT COUNT(DISTINCT(A.NO_PINDAH)) AS COUNT FROM PINDAH_HEADER A INNER JOIN PINDAH_DETAIL B ON A.NO_PINDAH = B.NO_PINDAH
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND A.KLASIFIKASI_PINDAH IN (4,5) AND A.FROM_NO_PROP = 32  AND A.FROM_NO_KAB = 73) N_PINDAH_H_ANTARKAB,
                        (SELECT COUNT(A.NO_PINDAH) AS COUNT FROM PINDAH_HEADER A INNER JOIN PINDAH_DETAIL B ON A.NO_PINDAH = B.NO_PINDAH
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND A.KLASIFIKASI_PINDAH IN (4,5) AND A.FROM_NO_PROP = 32  AND A.FROM_NO_KAB = 73) N_PINDAH_D_ANTARKAB,
                            (SELECT COUNT(DISTINCT(A.NO_PINDAH)) AS COUNT FROM PINDAH_HEADER A INNER JOIN PINDAH_DETAIL B ON A.NO_PINDAH = B.NO_PINDAH
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND A.KLASIFIKASI_PINDAH IN (3) AND A.FROM_NO_PROP = 32  AND A.FROM_NO_KAB = 73) N_PINDAH_H_ANTARKEC,
                        (SELECT COUNT(A.NO_PINDAH) AS COUNT FROM PINDAH_HEADER A INNER JOIN PINDAH_DETAIL B ON A.NO_PINDAH = B.NO_PINDAH
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND A.KLASIFIKASI_PINDAH IN (3) AND A.FROM_NO_PROP = 32  AND A.FROM_NO_KAB = 73) N_PINDAH_D_ANTARKEC,
                            (SELECT COUNT(DISTINCT(A.NO_PINDAH)) AS COUNT FROM PINDAH_HEADER A INNER JOIN PINDAH_DETAIL B ON A.NO_PINDAH = B.NO_PINDAH
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND A.KLASIFIKASI_PINDAH NOT IN (3,4,5) AND A.FROM_NO_PROP = 32  AND A.FROM_NO_KAB = 73) N_PINDAH_H_DALAMKEC,
                        (SELECT COUNT(A.NO_PINDAH) AS COUNT FROM PINDAH_HEADER A INNER JOIN PINDAH_DETAIL B ON A.NO_PINDAH = B.NO_PINDAH
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND A.KLASIFIKASI_PINDAH NOT IN (3,4,5) AND A.FROM_NO_PROP = 32  AND A.FROM_NO_KAB = 73) N_PINDAH_D_DALAMKEC,
                        (SELECT COUNT(DISTINCT(A.NO_DATANG)) AS COUNT FROM DATANG_HEADER A INNER JOIN DATANG_DETAIL B ON A.NO_DATANG = B.NO_DATANG
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND A.KLASIFIKASI_PINDAH IN (4,5) AND A.NO_PROP = 32  AND A.NO_KAB = 73) N_DATANG_H_ANTARKAB,
                        (SELECT COUNT(A.NO_DATANG) AS COUNT FROM DATANG_HEADER A INNER JOIN DATANG_DETAIL B ON A.NO_DATANG = B.NO_DATANG
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND A.KLASIFIKASI_PINDAH IN (4,5) AND A.NO_PROP = 32  AND A.NO_KAB = 73) N_DATANG_D_ANTARKAB,
                            (SELECT COUNT(DISTINCT(A.NO_DATANG)) AS COUNT FROM DATANG_HEADER A INNER JOIN DATANG_DETAIL B ON A.NO_DATANG = B.NO_DATANG
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND A.KLASIFIKASI_PINDAH IN (3) AND A.NO_PROP = 32  AND A.NO_KAB = 73) N_DATANG_H_ANTARKEC,
                        (SELECT COUNT(A.NO_DATANG) AS COUNT FROM DATANG_HEADER A INNER JOIN DATANG_DETAIL B ON A.NO_DATANG = B.NO_DATANG
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND A.KLASIFIKASI_PINDAH IN (3) AND A.NO_PROP = 32  AND A.NO_KAB = 73) N_DATANG_D_ANTARKEC,
                            (SELECT COUNT(DISTINCT(A.NO_DATANG)) AS COUNT FROM DATANG_HEADER A INNER JOIN DATANG_DETAIL B ON A.NO_DATANG = B.NO_DATANG
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND A.KLASIFIKASI_PINDAH NOT IN (3,4,5) AND A.NO_PROP = 32  AND A.NO_KAB = 73) N_DATANG_H_DALAMKEC,
                        (SELECT COUNT(A.NO_DATANG) AS COUNT FROM DATANG_HEADER A INNER JOIN DATANG_DETAIL B ON A.NO_DATANG = B.NO_DATANG
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND A.KLASIFIKASI_PINDAH NOT IN (3,4,5) AND A.NO_PROP = 32  AND A.NO_KAB = 73) N_DATANG_D_DALAMKEC,
                        (SELECT COUNT(DISTINCT(A.NO_PINDAH)) AS COUNT FROM PINDAH_HEADER A INNER JOIN PINDAH_DETAIL B ON A.NO_PINDAH = B.NO_PINDAH
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') AND A.KLASIFIKASI_PINDAH IN (4,5) AND A.FROM_NO_PROP = 32  AND A.FROM_NO_KAB = 73) Y_PINDAH_H_ANTARKAB,
                        (SELECT COUNT(A.NO_PINDAH) AS COUNT FROM PINDAH_HEADER A INNER JOIN PINDAH_DETAIL B ON A.NO_PINDAH = B.NO_PINDAH
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') AND A.KLASIFIKASI_PINDAH IN (4,5) AND A.FROM_NO_PROP = 32  AND A.FROM_NO_KAB = 73) Y_PINDAH_D_ANTARKAB,
                            (SELECT COUNT(DISTINCT(A.NO_PINDAH)) AS COUNT FROM PINDAH_HEADER A INNER JOIN PINDAH_DETAIL B ON A.NO_PINDAH = B.NO_PINDAH
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') AND A.KLASIFIKASI_PINDAH IN (3) AND A.FROM_NO_PROP = 32  AND A.FROM_NO_KAB = 73) Y_PINDAH_H_ANTARKEC,
                        (SELECT COUNT(A.NO_PINDAH) AS COUNT FROM PINDAH_HEADER A INNER JOIN PINDAH_DETAIL B ON A.NO_PINDAH = B.NO_PINDAH
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') AND A.KLASIFIKASI_PINDAH IN (3) AND A.FROM_NO_PROP = 32  AND A.FROM_NO_KAB = 73) Y_PINDAH_D_ANTARKEC,
                            (SELECT COUNT(DISTINCT(A.NO_PINDAH)) AS COUNT FROM PINDAH_HEADER A INNER JOIN PINDAH_DETAIL B ON A.NO_PINDAH = B.NO_PINDAH
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') AND A.KLASIFIKASI_PINDAH NOT IN (3,4,5) AND A.FROM_NO_PROP = 32  AND A.FROM_NO_KAB = 73) Y_PINDAH_H_DALAMKEC,
                        (SELECT COUNT(A.NO_PINDAH) AS COUNT FROM PINDAH_HEADER A INNER JOIN PINDAH_DETAIL B ON A.NO_PINDAH = B.NO_PINDAH
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') AND A.KLASIFIKASI_PINDAH NOT IN (3,4,5) AND A.FROM_NO_PROP = 32  AND A.FROM_NO_KAB = 73) Y_PINDAH_D_DALAMKEC,
                        (SELECT COUNT(DISTINCT(A.NO_DATANG)) AS COUNT FROM DATANG_HEADER A INNER JOIN DATANG_DETAIL B ON A.NO_DATANG = B.NO_DATANG
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') AND A.KLASIFIKASI_PINDAH IN (4,5) AND A.NO_PROP = 32  AND A.NO_KAB = 73) Y_DATANG_H_ANTARKAB,
                        (SELECT COUNT(A.NO_DATANG) AS COUNT FROM DATANG_HEADER A INNER JOIN DATANG_DETAIL B ON A.NO_DATANG = B.NO_DATANG
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') AND A.KLASIFIKASI_PINDAH IN (4,5) AND A.NO_PROP = 32  AND A.NO_KAB = 73) Y_DATANG_D_ANTARKAB,
                            (SELECT COUNT(DISTINCT(A.NO_DATANG)) AS COUNT FROM DATANG_HEADER A INNER JOIN DATANG_DETAIL B ON A.NO_DATANG = B.NO_DATANG
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') AND A.KLASIFIKASI_PINDAH IN (3) AND A.NO_PROP = 32  AND A.NO_KAB = 73) Y_DATANG_H_ANTARKEC,
                        (SELECT COUNT(A.NO_DATANG) AS COUNT FROM DATANG_HEADER A INNER JOIN DATANG_DETAIL B ON A.NO_DATANG = B.NO_DATANG
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') AND A.KLASIFIKASI_PINDAH IN (3) AND A.NO_PROP = 32  AND A.NO_KAB = 73) Y_DATANG_D_ANTARKEC,
                            (SELECT COUNT(DISTINCT(A.NO_DATANG)) AS COUNT FROM DATANG_HEADER A INNER JOIN DATANG_DETAIL B ON A.NO_DATANG = B.NO_DATANG
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') AND A.KLASIFIKASI_PINDAH NOT IN (3,4,5) AND A.NO_PROP = 32  AND A.NO_KAB = 73) Y_DATANG_H_DALAMKEC,
                        (SELECT COUNT(A.NO_DATANG) AS COUNT FROM DATANG_HEADER A INNER JOIN DATANG_DETAIL B ON A.NO_DATANG = B.NO_DATANG
                            WHERE 1=1 AND TO_CHAR(A.CREATED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') AND A.KLASIFIKASI_PINDAH NOT IN (3,4,5) AND A.NO_PROP = 32  AND A.NO_KAB = 73) Y_DATANG_D_DALAMKEC
                        FROM DUAL
                        ";
		 $r = DB::connection('db222')->select($sql);
		$output = array(
                   "pindah_hn_akab"=>$r[0]->n_pindah_h_antarkab,
                   "pindah_dn_akab"=>$r[0]->n_pindah_d_antarkab,
                   "pindah_hn_akec"=>$r[0]->n_pindah_h_antarkec,
                   "pindah_dn_akec"=>$r[0]->n_pindah_d_antarkec,
                   "pindah_hn_dkec"=>$r[0]->n_pindah_h_dalamkec,
                   "pindah_dn_dkec"=>$r[0]->n_pindah_d_dalamkec,
                   "datang_hn_akab"=>$r[0]->n_datang_h_antarkab,
                   "datang_dn_akab"=>$r[0]->n_datang_d_antarkab,
                   "datang_hn_akec"=>$r[0]->n_datang_h_antarkec,
                   "datang_dn_akec"=>$r[0]->n_datang_d_antarkec,
                   "datang_hn_dkec"=>$r[0]->n_datang_h_dalamkec,
                   "datang_dn_dkec"=>$r[0]->n_datang_d_dalamkec,
                   "pindah_hy_akab"=>$r[0]->y_pindah_h_antarkab,
                   "pindah_dy_akab"=>$r[0]->y_pindah_d_antarkab,
                   "pindah_hy_akec"=>$r[0]->y_pindah_h_antarkec,
                   "pindah_dy_akec"=>$r[0]->y_pindah_d_antarkec,
                   "pindah_hy_dkec"=>$r[0]->y_pindah_h_dalamkec,
                   "pindah_dy_dkec"=>$r[0]->y_pindah_d_dalamkec,
                   "datang_hy_akab"=>$r[0]->y_datang_h_antarkab,
                   "datang_dy_akab"=>$r[0]->y_datang_d_antarkab,
                   "datang_hy_akec"=>$r[0]->y_datang_h_antarkec,
                   "datang_dy_akec"=>$r[0]->y_datang_d_antarkec,
                   "datang_hy_dkec"=>$r[0]->y_datang_h_dalamkec,
                   "datang_dy_dkec"=>$r[0]->y_datang_d_dalamkec
                );
        return $output;
	}

	public static function dashboard_capil()
	{
		header('Content-type: application/json');
		 $sql = "SELECT
                        (SELECT COUNT(1) AS COUNT FROM CAPIL_LAHIR A WHERE TO_CHAR(A.ADM_TGL_ENTRY,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND A.ADM_AKTA_NO LIKE '%LU%' AND EXISTS(SELECT 1 FROM BIODATA_WNI B WHERE A.BAYI_NIK =B.NIK) AND A.ADM_NO_PROV = 32 AND A.ADM_NO_KAB= 73 AND A.BAYI_ORG_ASING = 'T' AND A.BAYI_DOM_LAHIR = 'D') N_LAHIR_U,
                        (SELECT COUNT(1) AS COUNT FROM CAPIL_LAHIR A WHERE TO_CHAR(A.ADM_TGL_ENTRY,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND A.ADM_AKTA_NO LIKE '%LT%' AND EXISTS(SELECT 1 FROM BIODATA_WNI B WHERE A.BAYI_NIK =B.NIK) AND A.ADM_NO_PROV = 32 AND A.ADM_NO_KAB= 73 AND A.BAYI_ORG_ASING = 'T' AND A.BAYI_DOM_LAHIR = 'D') N_LAHIR_T,
                        (SELECT COUNT(1) AS COUNT FROM CAPIL_MATI  WHERE  TO_CHAR(ADM_TGL_ENTRY,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND ADM_NO_PROV = 32 AND ADM_NO_KAB= 73  AND MATI_LUAR_NEGERI = 'T' AND MATI_DOM_MATI = 'D') N_MATI,
                        (SELECT COUNT(1) AS COUNT FROM CAPIL_KAWIN  WHERE  TO_CHAR(KAWIN_TGL_LAPOR,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND ADM_NO_PROV =32AND ADM_NO_KAB = 73) N_KAWIN,
                        (SELECT COUNT(1) AS COUNT FROM CAPIL_CERAI  WHERE  TO_CHAR(CERAI_TGL_LAPOR,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND ADM_NO_PROV =32AND ADM_NO_KAB = 73) N_CERAI,
                        (SELECT COUNT(1) AS COUNT FROM CAPIL_LAHIR A WHERE TO_CHAR(A.ADM_TGL_ENTRY,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') AND A.ADM_AKTA_NO LIKE '%LU%' AND EXISTS(SELECT 1 FROM BIODATA_WNI B WHERE A.BAYI_NIK =B.NIK) AND A.ADM_NO_PROV = 32 AND A.ADM_NO_KAB= 73 AND A.BAYI_ORG_ASING = 'T' AND A.BAYI_DOM_LAHIR = 'D') Y_LAHIR_U,
                        (SELECT COUNT(1) AS COUNT FROM CAPIL_LAHIR A WHERE TO_CHAR(A.ADM_TGL_ENTRY,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') AND A.ADM_AKTA_NO LIKE '%LT%' AND EXISTS(SELECT 1 FROM BIODATA_WNI B WHERE A.BAYI_NIK =B.NIK) AND A.ADM_NO_PROV = 32 AND A.ADM_NO_KAB= 73 AND A.BAYI_ORG_ASING = 'T' AND A.BAYI_DOM_LAHIR = 'D') Y_LAHIR_T,
                        (SELECT COUNT(1) AS COUNT FROM CAPIL_MATI  WHERE  TO_CHAR(ADM_TGL_ENTRY,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') AND ADM_NO_PROV = 32 AND ADM_NO_KAB= 73  AND MATI_LUAR_NEGERI = 'T' AND MATI_DOM_MATI = 'D') Y_MATI,
                        (SELECT COUNT(1) AS COUNT FROM CAPIL_KAWIN  WHERE  TO_CHAR(KAWIN_TGL_LAPOR,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') AND ADM_NO_PROV =32AND ADM_NO_KAB = 73) Y_KAWIN,
                        (SELECT COUNT(1) AS COUNT FROM CAPIL_CERAI  WHERE  TO_CHAR(CERAI_TGL_LAPOR,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') AND ADM_NO_PROV =32AND ADM_NO_KAB = 73) Y_CERAI
                        FROM DUAL
                        ";
		 $r = DB::connection('db222')->select($sql);
		$output = array(
                   "capil_nulahir"=>$r[0]->n_lahir_u,
                   "capil_ntlahir"=>$r[0]->n_lahir_t,
                   "capil_nmati"=>$r[0]->n_mati,
                   "capil_nkawin"=>$r[0]->n_kawin,
                   "capil_ncerai"=>$r[0]->n_cerai,
                   "capil_yulahir"=>$r[0]->y_lahir_u,
                   "capil_ytlahir"=>$r[0]->y_lahir_t,
                   "capil_ymati"=>$r[0]->y_mati,
                   "capil_ykawin"=>$r[0]->y_kawin,
                   "capil_ycerai"=>$r[0]->y_cerai
                );
        return $output;
	}
	public static function get_count_suket($nik){
			if (!empty($nik)){
				if (is_numeric($nik)){
					$sql = "SELECT COUNT(1) AS JML FROM BIODATA_WNI WHERE NIK = $nik";
					$r = DB::connection('db222')->select($sql);
					return $r[0]->jml;
				}else{
					return 0;
				}
			}else{
				return 0;
			}
	}
	public static function get_data_suket_andro($nik){
			$sql = " SELECT 
					  B.NIK
					  , B.NO_KK
					  , B.NAMA_LGKP
					  , B.TMPT_LHR
					  , TO_CHAR(B.TGL_LHR,'DD-MM-YYYY') AS TGL_LHR
					  , TRUNC(MONTHS_BETWEEN(SYSDATE,B.TGL_LHR)/12) UMUR
					  , CASE WHEN B.JENIS_KLMIN = 1 THEN 'LAKI-LAKI' ELSE 'PEREMPUAN' END AS JENIS_KLMIN
					  , D.ALAMAT
					  , LPAD(TO_CHAR(D.NO_RT), 3, '0') AS RT
					  , LPAD(TO_CHAR(D.NO_RW), 3, '0') AS RW
					  , F5_GET_NAMA_KECAMATAN(B.NO_PROP,B.NO_KAB,B.NO_KEC) NAMA_KEC
					  , F5_GET_NAMA_KELURAHAN(B.NO_PROP,B.NO_KAB,B.NO_KEC,B.NO_KEL) NAMA_KEL
					  , (SELECT UPPER(C.DESCRIP) FROM REF_SIAK_WNI C WHERE C.NO = B.AGAMA AND SECT =501) AS AGAMA
					  , (SELECT UPPER(C.DESCRIP) FROM REF_SIAK_WNI C WHERE C.NO = B.JENIS_PKRJN AND SECT =201) AS JENIS_PKRJN
					  , (SELECT UPPER(C.DESCRIP) FROM REF_SIAK_WNI C WHERE C.NO = B.STAT_HBKEL AND SECT =301) AS STAT_HBKEL
					  , CASE WHEN B.AKTA_LHR = 1 THEN 'TIDAK ADA' ELSE 'ADA' END AS AKTA_LHR
					  , CASE WHEN B.NO_AKTA_LHR IS NULL THEN '-' ELSE B.NO_AKTA_LHR END AS NO_AKTA_LHR
					  , CASE 
					    WHEN B.STAT_KWN = 2 AND B.AKTA_KWN = 1 THEN 'KAWIN TIDAK TERCATAT' 
					    WHEN B.STAT_KWN = 2 AND B.AKTA_KWN = 2 THEN 'KAWIN TERCATAT'
					    ELSE(SELECT UPPER(C.DESCRIP) FROM REF_SIAK_WNI C WHERE C.NO = B.STAT_KWN AND SECT =601) END AS STAT_KWN
					  , CASE WHEN B.NO_AKTA_KWN IS NULL THEN '-' ELSE B.NO_AKTA_KWN END AS NO_AKTA_KWN
					  , CASE WHEN A.CURRENT_STATUS_CODE IS NULL THEN '-' ELSE A.CURRENT_STATUS_CODE END AS CURRENT_STATUS_CODE
					  , CASE WHEN F.PRINTED_BY IS NULL THEN '-' ELSE F.PRINTED_BY END AS SUKET_BY
					  , CASE WHEN TO_CHAR(F.PRINTED_DATE,'DD-MM-YYYY') IS NULL THEN '-' ELSE TO_CHAR(F.PRINTED_DATE,'DD-MM-YYYY') END AS SUKET_DT
					  , CASE WHEN  TO_CHAR(G.LAST_UPDATE,'DD-MM-YYYY') IS NULL AND TO_CHAR(G.PERSONALIZED_DATE,'DD-MM-YYYY') IS NULL THEN '-' WHEN TO_CHAR(G.LAST_UPDATE,'DD-MM-YYYY') IS NULL AND TO_CHAR(G.PERSONALIZED_DATE,'DD-MM-YYYY') IS NOT NULL THEN TO_CHAR(G.PERSONALIZED_DATE,'DD-MM-YYYY') ELSE  TO_CHAR(G.LAST_UPDATE,'DD-MM-YYYY') END AS KTP_DT
					  , CASE WHEN TO_CHAR(G.CREATED_USERNAME) IS NULL THEN '-' ELSE TO_CHAR(G.CREATED_USERNAME) END AS KTP_BY 
					  FROM BIODATA_WNI B LEFT JOIN DATA_KELUARGA D ON B.NO_KK = D.NO_KK LEFT JOIN DEMOGRAPHICS_ALL A  ON A.NIK = B.NIK LEFT JOIN (SELECT NIK, PRINTED_DATE,PRINTED_BY FROM 
					  (SELECT NIK,PRINTED_DATE,PRINTED_BY, RANK() OVER (PARTITION BY NIK ORDER BY PRINTED_DATE DESC) RNK FROM T7_HIST_SUKET WHERE TIPE = 1) WHERE RNK = 1) F ON F.NIK = B.NIK LEFT JOIN 
					  (SELECT NIK,CREATED,CREATED_USERNAME,LAST_UPDATE,PERSONALIZED_DATE FROM (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME,LAST_UPDATE,CREATED, RANK() OVER (PARTITION BY NIK ORDER BY  PERSONALIZED_DATE DESC, LAST_UPDATE DESC, CHIP_ID DESC) RNK FROM CARD_MANAGEMENT) WHERE RNK = 1) G ON G.NIK =B.NIK WHERE B.NIK = $nik";
			$r = DB::connection('db222')->select($sql);
			return $r;
		}

	// Region Setting API
	public static function get_helpdesk_option()
	{
		header("Content-Type: application/json", true);
		 $sql = "SELECT A.HELPDESK_ID, A.HELPDESK_DESCRIPTION FROM SIAK_MASTER_HELPDESK A";
         $r = DB::select($sql);
		return $r;
	}
	public static function fotocetak(Request $request)
	{
			$nik = $request->nik;
			 if ($nik != null){
			 	$sql = "SELECT FACE FROM FACES WHERE NIK=  $nik";
			 $r = DB::connection('db2')->select($sql);
			 $img = $r[0]->face;
	         echo "<img src='data:image/png;base64,".base64_encode($img)."'>";
	     }else{
	     	echo "Data TidaK Ditemukan";
	     }
		 
	}
	public static function fotorekam(Request $request)
	{
			$nik = $request->nik;
			 if ($nik != null){
			 	$sql = "SELECT FACE FROM FACES WHERE NIK=  $nik";
			 $r = DB::connection('db221')->select($sql);
			 $img = $r[0]->face;
	         echo "<img src='data:image/png;base64,".base64_encode($img)."'>";
	     }else{
	     	echo "Data TidaK Ditemukan";
	     }
		 
	}

	public static function cek_nik(Request $request) 
	{
	    $nik = $request->nik;
	    $query=DB::select("SELECT COUNT(1) JUMLAH FROM USERS WHERE NIK = ".$nik."");
	    if($query[0]->jumlah > 0){
	      return response()->json([
	        'status' => 'error',
	        'data' => 'NIK sudah pernah terdaftar'
	      ]);
	    }
	}
	public static function get_data_kk(Request $request)
	{
		header('Content-type: application/json');
		$no_kk = $request->no_kk;
		$count = AndrApi::get_count_data_kk($no_kk);	
		if ($count >0){
			$output = AndrApi::src_get_data_kk($no_kk);
			$response["data"] = $output[0];
			$response["jumlah"] = $count;
		}else{
			$response["data"] = "";
			$response["jumlah"] = 0;
		}
        return $response;
	}
	public static function get_data_nik(Request $request)
	{
		header('Content-type: application/json');
		$nik = $request->nik;
		$count = AndrApi::get_count_data_nik($nik);	
		if ($count >0){
			$output = AndrApi::src_get_data_nik($nik);
			$response["data"] = $output[0];
			$response["jumlah"] = $count;
		}else{
			$response["data"] = "";
			$response["jumlah"] = 0;
		}
        return $response;
	}
	public static function get_cek_data_nik(Request $request)
	{
		header('Content-type: application/json');
		$nik = $request->nik;
		$count = AndrApi::get_count_data_nik($nik);	
		if ($count >0){
			$output = AndrApi::src_get_data_nik($nik);
			// $response["data"] = $output[0];
			$response["jumlah"] = $count;
		}else{
			// $response["data"] = "";
			$response["jumlah"] = 0;
		}
        return $response;
	}


	public static function insert_nik_blokir(Request $request)
	{
		header('Content-type: application/json');
		$nik = $request->nik;
		$ket = strtoupper($request->ket);
		$count = AndrApi::get_count_blokir($nik);	
		if ($count >0){
			$output = AndrApi::src_update_nik_blokir($nik,$ket);
			$response["data"] = 'Nik Berhasil Di Blokir';
		}else{
			$output = AndrApi::src_inst_nik_blokir($nik,$ket);
			
			$response["data"] = 'Nik Berhasil Di Blokir';
		}
        return $response;
	}


	public static function insert_nik_unblokir(Request $request)
	{
		header('Content-type: application/json');
		$nik = $request->nik;
		$ket = strtoupper($request->ket);
		$count = AndrApi::get_count_blokir($nik);	
		if ($count >0){
			$output = AndrApi::src_update_nik_unblokir($nik,$ket);
			$response["data"] = 'Nik Berhasil Di Buka Kembali';
		}else{
			$response["data"] = 'Nik Tidak Terdaftar Pada Daftar Blokir';
		}
        return $response;
	}

	public static function get_count_data_kk($no_kk){
			if (!empty($no_kk)){
				if (is_numeric($no_kk)){
					$sql = "SELECT COUNT(1) AS JML FROM DATA_KELUARGA WHERE NO_KK = $no_kk";
					$r = DB::connection('db222')->select($sql);
					return $r[0]->jml;
				}else{
					return 0;
				}
			}else{
				return 0;
			}
		}

	public static function src_get_data_kk($no_kk){
			$sql = " SELECT 
					  D.NO_KK
					  , D.NIK_KK
                      , D.NAMA_KEP
                      , D.NO_PROP
                      , D.NO_KAB
                      , D.NO_KEC
                      , D.NO_KEL
                      , F5_GET_NAMA_PROVINSI(D.NO_PROP) NAMA_PROP
                      , F5_GET_NAMA_KABUPATEN(D.NO_PROP,D.NO_KAB) NAMA_KAB
                      , F5_GET_NAMA_KECAMATAN(D.NO_PROP,D.NO_KAB,D.NO_KEC) NAMA_KEC
					  , F5_GET_NAMA_KELURAHAN(D.NO_PROP,D.NO_KAB,D.NO_KEC,D.NO_KEL) NAMA_KEL
                      
					  FROM  DATA_KELUARGA D WHERE D.NO_KK = $no_kk";
			$r = DB::connection('db222')->select($sql);
			return $r;
		}

	public static function get_count_data_nik($nik){
			if (!empty($nik)){
				if (is_numeric($nik)){
					$sql = "SELECT COUNT(1) AS JML FROM BIODATA_WNI WHERE NIK = $nik";
					$r = DB::connection('db222')->select($sql);
					return $r[0]->jml;
				}else{
					return 0;
				}
			}else{
				return 0;
			}
		}

	public static function get_count_blokir($nik){
			if (!empty($nik)){
				if (is_numeric($nik)){
					$sql = "SELECT COUNT(1) AS JML FROM SIAK_KTP_BLOKIR WHERE NIK = $nik";
					$r = DB::select($sql);
					return $r[0]->jml;
				}else{
					return 0;
				}
			}else{
				return 0;
			}
		}

	public static function src_get_data_nik($nik){
			$sql = " SELECT 
					  A.NIK
                      , A.NAMA_LGKP
                      , A.JENIS_KLMIN
                      , UPPER(F5_GET_REF_WNI(F5_TO_NUMBER(A.JENIS_KLMIN,7), 801)) JENIS_KLMIN_DESC
                      , TO_CHAR(A.TGL_LHR,'DD-MM-YYYY') TGL_LHR
                      , TO_CHAR(A.TGL_LHR,'HH24:MI') JAM_LHR
                      , A.TMPT_LHR
                      , A.JENIS_PKRJN
                      , UPPER(F5_GET_REF_WNI(A.JENIS_PKRJN, 201)) JENIS_PKRJN_DESC
                      , B.NO_KK
					  , B.NIK_KK
                      , B.NAMA_KEP
                      , B.NO_PROP
                      , B.NO_KAB
                      , B.NO_KEC
                      , B.NO_KEL
                      , F5_GET_NAMA_PROVINSI(B.NO_PROP) NAMA_PROP
                      , F5_GET_NAMA_KABUPATEN(B.NO_PROP,B.NO_KAB) NAMA_KAB
                      , F5_GET_NAMA_KECAMATAN(B.NO_PROP,B.NO_KAB,B.NO_KEC) NAMA_KEC
					  , F5_GET_NAMA_KELURAHAN(B.NO_PROP,B.NO_KAB,B.NO_KEC,B.NO_KEL) NAMA_KEL
                      , B.NO_RT
                      , B.NO_RW
                      , B.ALAMAT
                      , C.NIK NIK_IBU
                      , C.NAMA_LGKP NAMA_LGKP_IBU
                      , TO_CHAR(C.TGL_LHR,'DD-MM-YYYY') TGL_LHR_IBU
                      , C.TMPT_LHR TMPT_LHR_IBU
                      , C.JENIS_PKRJN JENIS_PKRJN_IBU
                      , UPPER(F5_GET_REF_WNI(C.JENIS_PKRJN, 201)) JENIS_PKRJN_DESC_IBU
                      , TO_CHAR(C.TGL_KWN,'DD-MM-YYYY') TGL_KAWIN_IBU
                      , CASE WHEN C.NIK IS NOT NULL THEN '1 - WARGA NEGARA INDONESIA' END KEBANGSAAN_IBU
                      , D.NO_KK NO_KK_IBU
                      , D.NIK_KK NIK_KK_IBU
                      , D.NAMA_KEP NAMA_KEP_IBU
                      , D.NO_PROP NO_PROP_IBU
                      , D.NO_KAB NO_KAB_IBU
                      , D.NO_KEC NO_KEC_IBU
                      , D.NO_KEL NO_KEL_IBU
                      , F5_GET_NAMA_PROVINSI(D.NO_PROP)  NAMA_PROP_IBU
                      , F5_GET_NAMA_KABUPATEN(D.NO_PROP,D.NO_KAB)  NAMA_KAB_IBU
                      , F5_GET_NAMA_KECAMATAN(D.NO_PROP,D.NO_KAB,D.NO_KEC)  NAMA_KEC_IBU
                      , F5_GET_NAMA_KELURAHAN(D.NO_PROP,D.NO_KAB,D.NO_KEC,D.NO_KEL)  NAMA_KEL_IBU
                      , D.NO_RT NO_RT_IBU
                      , D.NO_RW NO_RW_IBU
                      , D.ALAMAT ALAMAT_IBU
                      , E.NIK NIK_AYAH
                      , E.NAMA_LGKP NAMA_LGKP_AYAH
                      , TO_CHAR(E.TGL_LHR,'DD-MM-YYYY') TGL_LHR_AYAH
                      , E.TMPT_LHR TMPT_LHR_AYAH
                      , E.JENIS_PKRJN JENIS_PKRJN_AYAH
                      , UPPER(F5_GET_REF_WNI(E.JENIS_PKRJN, 201)) JENIS_PKRJN_DESC_AYAH
                      , TO_CHAR(E.TGL_KWN,'DD-MM-YYYY') TGL_KAWIN_AYAH
                      , CASE WHEN E.NIK IS NOT NULL THEN '1 - WARGA NEGARA INDONESIA' END KEBANGSAAN_AYAH
                      , F.NO_KK NO_KK_AYAH
                      , F.NIK_KK NIK_KK_AYAH
                      , F.NAMA_KEP NAMA_KEP_AYAH
                      , F.NO_PROP NO_PROP_AYAH
                      , F.NO_KAB NO_KAB_AYAH
                      , F.NO_KEC NO_KEC_AYAH
                      , F.NO_KEL NO_KEL_AYAH
                      , F5_GET_NAMA_PROVINSI(F.NO_PROP)  NAMA_PROP_AYAH
                      , F5_GET_NAMA_KABUPATEN(F.NO_PROP,F.NO_KAB)  NAMA_KAB_AYAH
                      , F5_GET_NAMA_KECAMATAN(F.NO_PROP,F.NO_KAB,F.NO_KEC)  NAMA_KEC_AYAH
                      , F5_GET_NAMA_KELURAHAN(F.NO_PROP,F.NO_KAB,F.NO_KEC,F.NO_KEL)  NAMA_KEL_AYAH
                      , F.NO_RT NO_RT_AYAH
                      , F.NO_RW NO_RW_AYAH
                      , F.ALAMAT ALAMAT_AYAH
					  FROM  BIODATA_WNI A 
                      INNER JOIN DATA_KELUARGA B ON A.NO_KK = B.NO_KK 
                      LEFT JOIN BIODATA_WNI C ON A.NIK_IBU = C.NIK 
                      LEFT JOIN DATA_KELUARGA D ON C.NO_KK = D.NO_KK
                      LEFT JOIN BIODATA_WNI E ON A.NIK_AYAH = E.NIK 
                      LEFT JOIN DATA_KELUARGA F ON E.NO_KK = F.NO_KK
                      WHERE A.NIK = $nik";
			$r = DB::connection('db222')->select($sql);
			return $r;
		}

	public static function src_inst_nik_blokir($nik, $ket)
    {
        $query=DB::insert("INSERT INTO SIAK_KTP_BLOKIR (NIK ,STATUS ,KET ,CREATED_DT )
      				VALUES ($nik,1,'$ket',SYSDATE)");
    }

	public static function src_update_nik_blokir($nik, $ket)
    {
        $query=DB::update("UPDATE SIAK_KTP_BLOKIR SET STATUS=1, KET = '$ket', UPDATED_DT = SYSDATE WHERE NIK = $nik");
    }

	public static function src_update_nik_unblokir($nik, $ket)
    {
        $query=DB::update("UPDATE SIAK_KTP_BLOKIR SET STATUS=0, KET = '$ket', UPDATED_DT = SYSDATE WHERE NIK = $nik");
    }

	public static function cek_negara(Request $request)
    {
        $query=DB::connection('db222')->select("SELECT NO ,DESCRIP ,KODE_NUM ,ALPHA2 FROM REF_NEGARA ORDER BY NO");
          return $query;
    }
     public static function mst_wna(Request $request)
    {
        $sect=$request->sect;
        $query=DB::connection('db222')->select("SELECT NO ,DESCRIP ,SECT FROM REF_SIAK_WNA WHERE SECT = ".$sect."");
          return $query;
    }
    public static function cek_user_petugas(Request $request)
    {
        $query=DB::select("SELECT ID ,NIK ,NAME FROM USERS");
        return $query;
    }

    public static function get_master_combobox()
    {
      $expiresAt = Carbon::createFromFormat('Y-m-d', Carbon::now()->format('Y-m-d')) ->addDays(30);
      if (Cache::has('e-punten-get-data-master-register-prov')){
        $prov = Cache::get('e-punten-get-data-master-register-prov');
      } else {
        $prov=DB::connection('db222')->select("SELECT NO_PROP,NO_PROP || ' - ' || NAMA_PROP NAMA_PROP FROM SETUP_PROP ORDER BY NO_PROP ASC");
        Cache::put('e-punten-get-data-master-register-prov', $prov, $expiresAt);
      }

      if (Cache::has('e-punten-get-data-master-register-kec')){
        $kecamatan = Cache::get('e-punten-get-data-master-register-kec');
      } else {
        $kecamatan=DB::connection('db222')->select("SELECT NO_KEC,NO_KEC || ' - ' || NAMA_KEC NAMA_KEC FROM SETUP_KEC WHERE NO_PROP=32 AND NO_KAB=73 ORDER BY NO_KEC ASC");
        Cache::put('e-punten-get-data-master-register-kec', $kecamatan, $expiresAt);
      }

      if (Cache::has('e-punten-get-data-master-register-jenis_klmin')){
        $jenis_klmin = Cache::get('e-punten-get-data-master-register-jenis_klmin');
      } else {
        $jenis_klmin=DB::connection('db222')->select("SELECT NO,DESCRIP JENIS_KLMIN FROM REF_SIAK_WNI WHERE SECT = 801 ORDER BY NO ASC");
        Cache::put('e-punten-get-data-master-register-jenis_klmin', $jenis_klmin, $expiresAt);
      }

      if (Cache::has('e-punten-get-data-master-register-agama')){
        $agama = Cache::get('e-punten-get-data-master-register-agama');
      } else {
        $agama=DB::connection('db222')->select("SELECT NO,NO || ' - ' || DESCRIP AGAMA FROM REF_SIAK_WNI WHERE SECT = 501 ORDER BY NO ASC");
        Cache::put('e-punten-get-data-master-register-agama', $agama, $expiresAt);
      }

      if (Cache::has('e-punten-get-data-master-register-stat_kwn')){
        $stat_kwn = Cache::get('e-punten-get-data-master-register-stat_kwn');
      } else {
        $stat_kwn=DB::connection('db222')->select("SELECT NO,NO || ' - ' || DESCRIP STAT_KWN FROM REF_SIAK_WNI WHERE SECT = 601 ORDER BY NO ASC");
        Cache::put('e-punten-get-data-master-register-stat_kwn', $stat_kwn, $expiresAt);
      }

      if (Cache::has('e-punten-get-data-master-register-gol_drh')){
        $gol_drh = Cache::get('e-punten-get-data-master-register-gol_drh');
      } else {
        $gol_drh=DB::connection('db222')->select("SELECT NO,NO || ' - ' || DESCRIP GOL_DRH FROM REF_SIAK_WNI WHERE SECT = 401 ORDER BY NO ASC");
        Cache::put('e-punten-get-data-master-register-gol_drh', $gol_drh, $expiresAt);
      }

      if (Cache::has('e-punten-get-data-master-register-pendidikan')){
        $pendidikan = Cache::get('e-punten-get-data-master-register-pendidikan');
      } else {
        $pendidikan=DB::connection('db222')->select("SELECT NO,NO || ' - ' || DESCRIP PENDIDIKAN FROM REF_SIAK_WNI WHERE SECT = 101 ORDER BY NO ASC");
        Cache::put('e-punten-get-data-master-register-pendidikan', $pendidikan, $expiresAt);
      }
      if (Cache::has('e-punten-get-data-master-register-pekerjaan')){
        $pekerjaan = Cache::get('e-punten-get-data-master-register-pekerjaan');
      } else {
        $pekerjaan=DB::connection('db222')->select("SELECT NO,NO || ' - ' || DESCRIP PEKERJAAN FROM REF_SIAK_WNI WHERE SECT = 201 ORDER BY NO ASC");
        Cache::put('e-punten-get-data-master-register-pekerjaan', $pekerjaan, $expiresAt);
      }
      if (Cache::has('e-punten-get-data-master-register-wnaklmin')){
        $wnaklmin = Cache::get('e-punten-get-data-master-register-wnaklmin');
      } else {
        $wnaklmin=DB::connection('db222')->select("SELECT NO,NO || ' - ' || DESCRIP WNAKLMIN FROM REF_SIAK_WNA WHERE SECT = 801 ORDER BY NO ASC");
        Cache::put('e-punten-get-data-master-register-wnaklmin', $wnaklmin, $expiresAt);
      }
      if (Cache::has('e-punten-get-data-master-register-wnakwn')){
        $wnakwn = Cache::get('e-punten-get-data-master-register-wnakwn');
      } else {
        $wnakwn=DB::connection('db222')->select("SELECT NO,NO || ' - ' || DESCRIP WNAKWN FROM REF_SIAK_WNA WHERE SECT = 701 ORDER BY NO ASC");
        Cache::put('e-punten-get-data-master-register-wnakwn', $wnakwn, $expiresAt);
      }
      if (Cache::has('e-punten-get-data-master-register-wnahbkel')){
        $wnahbkel = Cache::get('e-punten-get-data-master-register-wnahbkel');
      } else {
        $wnahbkel=DB::connection('db222')->select("SELECT NO,NO || ' - ' || DESCRIP WNAHBKEL FROM REF_SIAK_WNA WHERE SECT = 301 ORDER BY NO ASC");
        Cache::put('e-punten-get-data-master-register-wnahbkel', $wnahbkel, $expiresAt);
      }

      if (Cache::has('e-punten-get-data-master-register-stat_hbkel')){
        $stat_hbkel = Cache::get('e-punten-get-data-master-register-stat_hbkel');
      } else {
        $stat_hbkel=DB::connection('db222')->select("SELECT NO,NO || ' - ' || DESCRIP STAT_HBKEL FROM REF_SIAK_WNI WHERE SECT = 301 ORDER BY NO ASC");
        Cache::put('e-punten-get-data-master-register-stat_hbkel', $stat_hbkel, $expiresAt);
      }

      if (Cache::has('e-punten-get-data-master-register-alasan')){
        $alasan = Cache::get('e-punten-get-data-master-register-alasan');
      } else {
        $alasan=DB::connection('db222')->select("SELECT NO,NO || ' - ' || DESCRIP ALASAN FROM REF_SIAK_WNI WHERE SECT = 901 ORDER BY NO ASC");
        Cache::put('e-punten-get-data-master-register-alasan', $alasan, $expiresAt);
      }

      return compact('prov', 'kecamatan', 'jenis_klmin', 'agama', 'stat_kwn', 'gol_drh', 'pendidikan', 'pekerjaan', 'wnaklmin', 'wnakwn', 'wnahbkel', 'stat_hbkel', 'alasan');
    }
    public static function get_master_combobox_agama()
    {
      $agama=DB::connection('db222')->select("SELECT NO,DESCRIP AGAMA FROM REF_SIAK_WNI WHERE SECT = 501 ORDER BY NO ASC");
      return $agama;
    }

    public static function get_master_combobox_stat_kwn()
    {
      $stat_kwn=DB::connection('db222')->select("SELECT NO,DESCRIP STAT_KWN FROM REF_SIAK_WNI WHERE SECT = 601 ORDER BY NO ASC");
      return $stat_kwn;
    }

    public static function get_master_combobox_gol_drh()
    {
      $gol_drh=DB::connection('db222')->select("SELECT NO,DESCRIP GOL_DRH FROM REF_SIAK_WNI WHERE SECT = 401 ORDER BY NO ASC");
      return $gol_drh;
    }

    public static function get_master_combobox_pendidikan()
    {
      $pendidikan=DB::connection('db222')->select("SELECT NO,DESCRIP PENDIDIKAN FROM REF_SIAK_WNI WHERE SECT = 101 ORDER BY NO ASC");
      return $pendidikan;
    }

    public static function get_master_combobox_pekerjaan()
    {
      $pekerjaan=DB::connection('db222')->select("SELECT NO,DESCRIP||' ('||NO||')' PEKERJAAN FROM REF_SIAK_WNI WHERE SECT = 201 ORDER BY NO ASC");
      return $pekerjaan;
    }

    public static function get_master_combobox_jenis_klmin_wna()
    {
      $wnaklmin=DB::connection('db222')->select("SELECT NO,DESCRIP||' ('||NO||')' WNAKLMIN FROM REF_SIAK_WNA WHERE SECT = 801 ORDER BY NO ASC");
      return $wnaklmin;
    }

    public static function get_master_combobox_stat_kwn_wna()
    {
      $wnakwn=DB::connection('db222')->select("SELECT NO,DESCRIP||' ('||NO||')' WNAKWN FROM REF_SIAK_WNA WHERE SECT = 701 ORDER BY NO ASC");
      return $wnakwn;
    }

    public static function get_master_combobox_stat_hbkel_wna()
    {
      $wnahbkel=DB::connection('db222')->select("SELECT NO,DESCRIP||' ('||NO||')' WNAHBKEL FROM REF_SIAK_WNA WHERE SECT = 301 ORDER BY NO ASC");
      return $wnahbkel;
    }

    public static function get_master_combobox_prov()
    {
      $prov=DB::connection('db222')->select("SELECT NO_PROP,NO_PROP || ' - ' || NAMA_PROP NAMA_PROP FROM SETUP_PROP ORDER BY NO_PROP ASC");
      return $prov;
    }

    public static function get_master_combobox_kab(Request $request)
    {
	    if(!empty($request->no_prop)){
	    	$expiresAt = Carbon::createFromFormat('Y-m-d', Carbon::now()->format('Y-m-d')) ->addDays(30);
		      if (Cache::has('e-punten-data-req-kab2-'.$request->no_prop)){
		        $kabupaten = Cache::get('e-punten-data-req-kab2-'.$request->no_prop);
		      } else {
		        $kabupaten=DB::connection('db222')->select("SELECT NO_KAB,NO_KAB || ' - ' || NAMA_KAB NAMA_KAB FROM SETUP_KAB WHERE NO_PROP= ".$request->no_prop."  ORDER BY NO_KAB ASC");
		        Cache::put('e-punten-data-req-kab2-'.$request->no_prop, $kabupaten, $expiresAt);
		      }
		      return compact('kabupaten');
	    }
    }

    public static function get_master_combobox_kec(Request $request)
    {
    	if(!empty($request->no_prop) && !empty($request->no_kab)){
	      $expiresAt = Carbon::createFromFormat('Y-m-d', Carbon::now()->format('Y-m-d')) ->addDays(30);
	      if (Cache::has('e-punten-data-req-kec-'.$request->no_prop.$request->no_kab)){
	        $kecamatan = Cache::get('e-punten-data-req-kec-'.$request->no_prop.$request->no_kab);
	      } else {
	        $kecamatan=DB::connection('db222')->select("SELECT NO_KEC,NO_KEC || ' - ' || NAMA_KEC NAMA_KEC FROM SETUP_KEC WHERE NO_PROP= ".$request->no_prop."
	        AND NO_KAB= ".$request->no_kab." ORDER BY NO_KEC ASC");
	        Cache::put('e-punten-data-req-kec-'.$request->no_prop.$request->no_kab, $kecamatan, $expiresAt);
	      }

	      return compact('kecamatan');
	  	}
    }

    public static function get_master_combobox_kec2(Request $request)
    {
    	if(!empty($request->no_prop) && !empty($request->no_kab) && !empty($request->no_kec)){
	      $expiresAt = Carbon::createFromFormat('Y-m-d', Carbon::now()->format('Y-m-d')) ->addDays(30);
	      if (Cache::has('e-punten-data-req-kec2-'.$request->no_prop.$request->no_kab.$request->no_kec)){
	        $kecamatan = Cache::get('e-punten-data-req-kec2-'.$request->no_prop.$request->no_kab.$request->no_kec);
	      } else {
	        $kecamatan=DB::connection('db222')->select("SELECT NO_KEC,NO_KEC || ' - ' || NAMA_KEC NAMA_KEC FROM SETUP_KEC WHERE NO_PROP= ".$request->no_prop."
	        AND NO_KAB= ".$request->no_kab."
	        AND NO_KEC= ".$request->no_kec." ORDER BY NO_KEC ASC");
	        Cache::put('e-punten-data-req-kec2-'.$request->no_prop.$request->no_kab.$request->no_kec, $kecamatan, $expiresAt);
	      }

	      return compact('kecamatan');
	  	}
    }

    public static function get_master_combobox_kel(Request $request)
    {
	        $kelurahan= DB::connection('db222')->select("SELECT NO_KEL,NO_KEL || ' - ' || NAMA_KEL NAMA_KEL FROM SETUP_KEL WHERE NO_PROP= ".$request->no_prop." AND NO_KAB= ".$request->no_kab." AND NO_KEC= ".$request->no_kec." ORDER BY NO_KEL ASC");
	      
	       return response()->json([
              'kelurahan' => $kelurahan
            ]);
    }

    public static function get_master_combobox_kel2(Request $request)
    {
	        $kelurahan= DB::connection('db222')->select("SELECT NO_KEL,NO_KEL || ' - ' || NAMA_KEL NAMA_KEL FROM SETUP_KEL WHERE NO_PROP= ".$request->no_prop." AND NO_KAB= ".$request->no_kab." AND NO_KEC= ".$request->no_kec." AND NO_KEC= ".$request->no_kel." ORDER BY NO_KEL ASC");
	      
	       return response()->json([
              'kelurahan' => $kelurahan
            ]);
    }

    public static function get_master_combobox_dest_kel(Request $request)
    {
	  	$kelurahan= DB::select("SELECT NO_KEL,NO_KEL || ' - ' || NAMA_KEL NAMA_KEL FROM SETUP_KEL WHERE NO_PROP= ".$request->prov."
	        AND NO_KAB= ".$request->kab." AND NO_KEC= ".$request->no_kec." ORDER BY NO_KEL ASC");
	      
	       return response()->json([
              'kelurahan' => $kelurahan
            ]);
    }



    public static function get_report_user()
    {
      $ptgs=DB::select("SELECT USER_ID, NAMA_LGKP, NRP FROM SIAK_USER_PLUS WHERE IS_ABSEN = 1 ORDER BY NAMA_LGKP");
      return $ptgs;
    }



	public static function sendnotification($jenis = '',$pengajuan_id = null,$jenis_layanan = 0, $nik = null, $msg = '',$ntf = '',$from_user_id ='')
	{
		$android_token = AndrApi::get_token($nik);

			if(count($android_token) >0){
				if ($android_token[0]->android_token != null){
					$json_data = [
					    "to" => $android_token[0]->android_token,
					    "notification" => [
					        "body" => $msg,
					        "title" => $ntf,
					    ],
					    "data" => [
					        "jenis_notif" => strtolower($ntf),
					    ],
					    "priority"=> "high"
					];

					$data = json_encode($json_data);
					//FCM API end-point
					$url = 'https://fcm.googleapis.com/fcm/send';
					//api_key in Firebase Console -> Project Settings -> CLOUD MESSAGING -> Server key
					$server_key = 'AAAAOTO6XZg:APA91bGEOYxIa83hKqEi-26YJIBgCKo9FkDSg_bYSMtt1lT07yW1OtoYoWhwXJXLyqInBl68IV9CKeVP7gBBEula22FrzO4UM7AONG3qde4C9fCuVBQDcpaGlinbHCMw2g8-fi2iWRD7';
					//header with content_type api key
					$headers = array(
					    'Content-Type:application/json',
					    'Authorization:key='.$server_key
					);
					//CURL request to route notification to FCM connection server (provided by Google)
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_URL, $url);
					curl_setopt($ch, CURLOPT_POST, true);
					curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
					curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
					$result = curl_exec($ch);
					if ($result === FALSE) {
					    die('Oops! FCM Send Error: ' . curl_error($ch));
					}
					curl_close($ch);
				}
			}
		if ($pengajuan_id == null){
			$sql = "INSERT INTO NOTIFICATION (ID ,TYPE ,PENGAJUAN_ID ,JENIS_PENGAJUAN ,NIK ,FROM_USER_ID ,TO_USER_ID ,MESSAGE ,CREATED_AT )
      				VALUES (NOTIFICATION_ID_SEQ.NEXTVAL,'$jenis',NULL,'$jenis_layanan','$nik','$from_user_id','0','$msg',SYSDATE)";
		}else{
			$sql = "INSERT INTO NOTIFICATION (ID ,TYPE ,PENGAJUAN_ID ,JENIS_PENGAJUAN ,NIK ,FROM_USER_ID ,TO_USER_ID ,MESSAGE ,CREATED_AT )
      				VALUES (NOTIFICATION_ID_SEQ.NEXTVAL,'$jenis',$pengajuan_id,'$jenis_layanan','$nik','$from_user_id','$nik','$msg',SYSDATE)";
		}
      	$r = DB::insert($sql);
      	

	}

	public static function chat_event($text = '', $from_id = 0, $to_id = 0, $nama_lgkp, $img_chat)
    {
        $message = new Message;
        $message->setAttribute('from_id', $from_id);
        $message->setAttribute('to_id', $to_id);
        $message->setAttribute('text', $text);
        $message->setAttribute('img', $img_chat);
        $message->setAttribute('read', 0);
        $message->setAttribute('created_at', Carbon::now());
        $message->setAttribute('updated_at', Carbon::now());
        $message->save();
        event(new NewMessageNotification($message));
        	$android_token = AndrApi::get_token($to_id);
			if(count($android_token) >0){
				if ($android_token[0]->android_token != null){
					$json_data = [
					    "to" => $android_token[0]->android_token,
					    "notification" => [
					        "body" => $text,
					        "title" => 'Pesan Baru',
					    ],
					    "data" => [
					        "jenis_notif" => 'pesan',
					        'from_id' => $from_id,
					        'jam' => date("H:i"),
					        'nama_lgkp' => $nama_lgkp,
					        'waktu' => Carbon::now()->format('d-m-Y'),
					        'to_id' =>  $to_id,
					        'created_at' => Carbon::now()->format('d-m-Y'),
					        'text' => $text,
					        'pic' => $img_chat,
					        'tanggal' => date("Ymd")
					    ],
					    "priority"=> "high"
					];

					$data = json_encode($json_data);
					//FCM API end-point
					$url = 'https://fcm.googleapis.com/fcm/send';
					//api_key in Firebase Console -> Project Settings -> CLOUD MESSAGING -> Server key
					$server_key = 'AAAAOTO6XZg:APA91bGEOYxIa83hKqEi-26YJIBgCKo9FkDSg_bYSMtt1lT07yW1OtoYoWhwXJXLyqInBl68IV9CKeVP7gBBEula22FrzO4UM7AONG3qde4C9fCuVBQDcpaGlinbHCMw2g8-fi2iWRD7';
					//header with content_type api key
					$headers = array(
					    'Content-Type:application/json',
					    'Authorization:key='.$server_key
					);
					//CURL request to route notification to FCM connection server (provided by Google)
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_URL, $url);
					curl_setopt($ch, CURLOPT_POST, true);
					curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
					curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
					$result = curl_exec($ch);
					if ($result === FALSE) {
					    die('Oops! FCM Send Error: ' . curl_error($ch));
					}
					curl_close($ch);
				}
			}
        
    }

    public function all_active_chat(Request $request)
  {
  	header('Content-type: application/json');
  	$nik = $request->nik;
       $sql ="SELECT 
       		 A.NIK ID
       		, INITCAP(A.NAME) NAMA_LGKP
       		, A.PIC
            , CASE WHEN C.TEXT IS NULL THEN NULL
               WHEN LENGTH(C.TEXT) >= 20 THEN SUBSTR(C.TEXT,0,20) || ' ...'
                 ELSE C.TEXT END MESSAGE
            , CASE WHEN C.JML IS NULL THEN 0 ELSE C.JML END UNREAD
            , TO_CHAR(C.CREATED_AT,'DDMMYYYYHH24MISS') WAKTU
            FROM USERS A  
            INNER JOIN 
                (SELECT ID, TYPE_MSG, SOURCE_NIK, SOURCE_MSG, DELETED_FROM, DELETED_TO, TEXT, READ, CREATED_AT, UPDATED_AT, OPR_READ, (SELECT COUNT(1) FROM MESSAGES X WHERE X.READ = 0 AND X.FROM_ID = A.SOURCE_NIK AND X.TO_ID = $nik ) JML FROM(  
                SELECT  ID, TYPE_MSG, SOURCE_NIK, SOURCE_MSG, DELETED_FROM, DELETED_TO, TEXT, READ, CREATED_AT, UPDATED_AT, OPR_READ, RANK() OVER (PARTITION BY SOURCE_MSG ORDER BY CREATED_AT DESC,UPDATED_AT DESC, ID DESC) RNK FROM 
                (SELECT ID, TYPE_MSG, SOURCE_NIK, SOURCE_MSG, DELETED_FROM, DELETED_TO, TEXT, READ, CREATED_AT, UPDATED_AT, OPR_READ, RNK FROM 
                (SELECT ID, 'Receive' TYPE_MSG, FROM_ID SOURCE_NIK, FROM_ID||TO_ID SOURCE_MSG, DELETED_FROM, DELETED_TO, TEXT, READ, CREATED_AT, UPDATED_AT, OPR_READ, RANK() OVER (PARTITION BY FROM_ID ORDER BY CREATED_AT DESC,UPDATED_AT DESC, ID DESC) RNK FROM MESSAGES WHERE TO_ID = $nik) 
                UNION ALL
                (SELECT ID, 'Sent' TYPE_MSG, TO_ID SOURCE_NIK,TO_ID||FROM_ID SOURCE_MSG, DELETED_FROM, DELETED_TO, TEXT, READ, CREATED_AT, UPDATED_AT, OPR_READ, RANK() OVER (PARTITION BY TO_ID ORDER BY CREATED_AT DESC,UPDATED_AT DESC, ID DESC) RNK FROM MESSAGES WHERE FROM_ID = $nik))) A WHERE RNK =1) C ON A.NIK = C.SOURCE_NIK
            WHERE A.NIK <> $nik
            ORDER BY C.CREATED_AT DESC NULLS LAST, NAMA_LGKP";
            $contact = DB::select($sql);
           return $contact;
    }

    public function all_kontak(Request $request)
    {
    	$petugas = DB::select("SELECT A.NIK FROM USERS A INNER JOIN SIAK_USER_PLUS B ON A.NIK = B.NIK WHERE B.IS_EPUNTEN = 1");
      $array = json_decode(json_encode($petugas), true);
        $contacts = User::whereIn('nik', $array)
        ->select(DB::raw("nik id,INITCAP(name) nama_lgkp,pic"))
        ->orderBy('name', 'asc')->paginate(50);
     
      return response()->json($contacts);
    }

    public function epunten_versi(Request $request)
    {
    	$versi = DB::select("SELECT SYSTEM_VALUE_NUM VAL FROM SIAK_MASTER WHERE SYSTEM_TYPE = 'INIT_SETING' AND SYSTEM_CODE = 'EPUNTEN_VERS'");
     
      	return response()->json($versi[0]->val);
    }

    public function epunten_persyaratan(Request $request)
    {
    	$cd = $request->cd;
    	$versi = DB::connection('webpunten')->select("SELECT DESCRIP VAL FROM REF_PERSYARATAN WHERE CD = '$cd'");
     
      	return response()->json($versi[0]->val);
    }

    public function get_message(Request $request)
    {
    	$mynik = $request->mynik;
    	$tonik = $request->tonik;
    	DB::update("UPDATE MESSAGES SET READ = 1 WHERE TO_ID = $mynik AND FROM_ID = $tonik");
       Message::where('from_id', $tonik)->where('to_id', $mynik)->update(['read' => '1']);
	      $messages = Message::where(function($q) use ($tonik,$mynik) {
	        $q->where('from_id', $mynik);
	        $q->where('to_id', $tonik);
	      })->orWhere(function($q) use ($tonik,$mynik) {
	        $q->where('from_id', $tonik);
	        $q->where('to_id', $mynik);
	      })
	      ->select(DB::raw("messages.*, to_char(created_at, 'DD/MM/YYYY HH24:MI:SS') tanggal"))
	      ->orderBy('created_at','desc')
	      ->paginate(10);

	      return response()->json($messages);
     
	  }

    public function insert_bsre_kk(Request $request)
    {
    	$no_kk = $request->no_kk;
    	$sql ="INSERT INTO BSRE_KARTU_KELUARGA
					 (SEQN_ID, NO_DOC, NAMA_KEP, ALAMAT, NO_RT, NO_RW, DUSUN, TELP, NO_PROP, NO_KAB, NO_KEC, NO_KEL, REQ_BY, REQ_DATE, CERT_STATUS, PEJABAT_NIP, PEJABAT_NAMA_LGKP, PEJABAT_JABATAN, PEJABAT_PROCESS_DATE, URL_DOKUMEN, COUNT_PRINT, SENT_STATUS, SENT_DATE, ACTIVE_STATUS, ACTIVE_DATE, PEJABAT_PROCCESS_BY, IS_UPT, EMAIL, SMS_PHONE, VERSI, PERMEN, TIPE)
					SELECT 
						'3273_siakepunten'|| substr((SYSDATE - DATE '1970-01-01' ) * 86400,0,10) || '_' || substr(RAWTOHEX(A.NO_KK),0,13) SEQ_ID, A.NO_KK NO_DOC, A.NAMA_KEP, A.ALAMAT, A.NO_RT, A.NO_RW, A.DUSUN, A.TELP, A.NO_PROP, A.NO_KAB, A.NO_KEC, A.NO_KEL, 'siakepunten' REQ_BY, sysdate REQ_DATE, 1 CERT_STATUS, B.NIP_PEJ PEJABAT_NIP, B.NAMA_PEJ PEJABAT_NAMA_LGKP, B.JABATAN PEJABAT_JABATAN, NULL PEJABAT_PROCESS_DATE, NULL URL_DOKUMEN, A.COUNT_KK COUNT_PRINT, 0 SENT_STATUS, NULL SENT_DATE, 0 ACTIVE_STATUS, NULL ACTIVE_DATE, NULL PEJABAT_PROCCESS_BY, NULL IS_UPT, A.EMAIL EMAIL, A.SMS_PHONE SMS_PHONE, 20 VERSI, 0 PERMEN, 1 TIPE
					FROM DATA_KELUARGA A
					INNER JOIN SETUP_KAB B 
					ON A.NO_PROP = B.NO_PROP 
					AND A.NO_KAB = B.NO_KAB 
					LEFT JOIN (SELECT NO_DOC,CERT_STATUS FROM (SELECT NO_DOC,CERT_STATUS, RANK() OVER (PARTITION BY NO_DOC ORDER BY  REQ_DATE DESC, SEQN_ID DESC) RNK FROM BSRE_KARTU_KELUARGA) WHERE RNK = 1) C ON A.NO_KK = C.NO_DOC 
					WHERE A.NO_KK = $no_kk AND (C.NO_DOC IS NULL OR C.CERT_STATUS NOT IN (1,9,2,3))";
        $r = DB::connection('db222')->insert($sql);
        $output = array(
                    "message_type"=>1,
                    "message"=>"Pengajuan Ini Telah Dihapus !"
        );
        return $output; 
     
	  }

    public static function insert_bsre_kk_epunten($no_kk)
    {
    	$sql ="INSERT INTO BSRE_KARTU_KELUARGA
					 (SEQN_ID, NO_DOC, NAMA_KEP, ALAMAT, NO_RT, NO_RW, DUSUN, TELP, NO_PROP, NO_KAB, NO_KEC, NO_KEL, REQ_BY, REQ_DATE, CERT_STATUS, PEJABAT_NIP, PEJABAT_NAMA_LGKP, PEJABAT_JABATAN, PEJABAT_PROCESS_DATE, URL_DOKUMEN, COUNT_PRINT, SENT_STATUS, SENT_DATE, ACTIVE_STATUS, ACTIVE_DATE, PEJABAT_PROCCESS_BY, IS_UPT, EMAIL, SMS_PHONE, VERSI, PERMEN, TIPE)
					SELECT 
						'3273_siakepunten'|| substr((SYSDATE - DATE '1970-01-01' ) * 86400,0,10) || '_' || substr(RAWTOHEX(A.NO_KK),0,13) SEQ_ID, A.NO_KK NO_DOC, A.NAMA_KEP, A.ALAMAT, A.NO_RT, A.NO_RW, A.DUSUN, A.TELP, A.NO_PROP, A.NO_KAB, A.NO_KEC, A.NO_KEL, 'siakepunten' REQ_BY, sysdate REQ_DATE, 1 CERT_STATUS, B.NIP_PEJ PEJABAT_NIP, B.NAMA_PEJ PEJABAT_NAMA_LGKP, B.JABATAN PEJABAT_JABATAN, NULL PEJABAT_PROCESS_DATE, NULL URL_DOKUMEN, A.COUNT_KK COUNT_PRINT, 0 SENT_STATUS, NULL SENT_DATE, 0 ACTIVE_STATUS, NULL ACTIVE_DATE, NULL PEJABAT_PROCCESS_BY, NULL IS_UPT, A.EMAIL EMAIL, A.SMS_PHONE SMS_PHONE, 20 VERSI, 0 PERMEN, 1 TIPE
					FROM DATA_KELUARGA A
					INNER JOIN SETUP_KAB B 
					ON A.NO_PROP = B.NO_PROP 
					AND A.NO_KAB = B.NO_KAB 
					LEFT JOIN (SELECT NO_DOC,CERT_STATUS FROM (SELECT NO_DOC,CERT_STATUS, RANK() OVER (PARTITION BY NO_DOC ORDER BY  REQ_DATE DESC, SEQN_ID DESC) RNK FROM BSRE_KARTU_KELUARGA) WHERE RNK = 1) C ON A.NO_KK = C.NO_DOC 
					WHERE A.NO_KK = $no_kk AND (C.NO_DOC IS NULL OR C.CERT_STATUS NOT IN (1,9,2,3))";
        $r = DB::connection('db222')->insert($sql);

        $sql ="UPDATE DATA_KELUARGA A SET A.CERT_STATUS = 1, A.STATUS_PUBLISH = NULL, A.CERT_CODE = NULL WHERE A.NO_KK = $no_kk AND (A.CERT_STATUS IS NULL OR A.CERT_STATUS NOT IN (1,9,2,3))";
        $r = DB::connection('db222')->update($sql);
     
	  }

    public function chat_send(Request $request)
    {
    	$from_id = $request->mynik;
    	$to_id = $request->tonik;
    	$nama_lgkp = $request->nama_lgkp;
    	$text = $request->text;


    	$destinationPath=$this->pengajuan_path().'/pengajuan/chat/'.$from_id.'/ori/';
        $imgName = 'pengajuan/chat/'.$from_id.'/ori/';

    	if (!file_exists($destinationPath)) {
          mkdir($destinationPath, 0777, true);
        }

    	if($request->hasFile('imgchat')){
        	$photo=$request->file('imgchat')->getClientOriginalName();
        	$extension = $request->file('imgchat')->getClientOriginalExtension();
        	$filename = "imgchat_".time().".".$extension;
        	$request->file('imgchat')->move($destinationPath,$filename);
        	$img_chat=$imgName.$filename;
        	if(empty($text)){
        		$text = 'PIC';
        	}
        }else{
        	$img_chat = '';
        }
    	 AndrApi::chat_event($text, $from_id, $to_id, $nama_lgkp, $img_chat);
		 $output = array(
	                "message_type"=>1,
	                "message"=>"Pesan Terkirim !"
	        );
	        return $output; 
     
    }

    public static function chat_send_api($from_id = null,$to_id = null,$nama_lgkp = null, $text = '')
	{

    	
        $img_chat = '';
    	 AndrApi::chat_event($text, $from_id, $to_id, $nama_lgkp, $img_chat);
		 $output = array(
	                "message_type"=>1,
	                "message"=>"Pesan Terkirim !"
	        );
	        return $output; 
	}

    public function notif(Request $request)
  {
    $nik = $request->nik;
    $notif=Notification::where('deleted_at', null)
    ->where('to_user_id', $nik)
    ->orderBy('seen_at','asc')
    ->orderBy('created_at','desc')
    ->paginate(50);
    return $notif;
  }

	public static function get_token($nik)
    {
		 $sql = "SELECT ANDROID_TOKEN FROM USERS WHERE NIK = $nik";
         $r = DB::select($sql);
		return $r;
    }

    public static function pengajuan_path(){
	  return str_replace('\public', '', base_path());
	}


    public function update_tte_roket(Request $request)
    {

    	 AndrApi::do_update_tte_roket();
		 $output = array(
	                "message_type"=>1,
	                "message"=>"Prosedure Telah Di Eksekusi !"
	        );
	        return $output; 
    }


    public static function do_update_tte_roket()
    {

		try {
		    \DB::connection('db222')->transaction(function(){
		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("UPDATE DATA_KELUARGA A SET 
				A.CERT_STATUS = 1 
				WHERE EXISTS (SELECT 1 FROM BSRE_KARTU_KELUARGA B WHERE A.NO_KK = B.NO_DOC AND B.CERT_STATUS IN (1,2,3,5,9))"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("UPDATE BSRE_KARTU_KELUARGA SET 
				REQ_DATE = SYSDATE
				, PEJABAT_PROCESS_DATE = NULL
				, URL_DOKUMEN = NULL  
				, COUNT_PRINT = 0 
				, SENT_STATUS = 0
				, SENT_DATE = NULL
				, ACTIVE_STATUS = 0
				, ACTIVE_DATE = NULL
				, PEJABAT_PROCCESS_BY = NULL
				, CERT_STATUS = 1
				WHERE CERT_STATUS IN (1,2,3,5,9)"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("UPDATE DATA_KELUARGA A SET 
				A.CERT_STATUS = 1 
				WHERE EXISTS (SELECT 1 FROM BSRE_KARTU_KELUARGA B WHERE A.NO_KK = B.NO_DOC AND PEJABAT_PROCESS_DATE IS NULL)"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("UPDATE BSRE_KARTU_KELUARGA SET 
				REQ_DATE = SYSDATE
				, PEJABAT_PROCESS_DATE = NULL
				, URL_DOKUMEN = NULL  
				, COUNT_PRINT = 0 
				, SENT_STATUS = 0
				, SENT_DATE = NULL
				, ACTIVE_STATUS = 0
				, ACTIVE_DATE = NULL
				, PEJABAT_PROCCESS_BY = NULL
				, CERT_STATUS = 1
				WHERE PEJABAT_PROCESS_DATE IS NULL"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("UPDATE BSRE_BIODATA SET 
				REQ_DATE = SYSDATE
				, PEJABAT_PROCESS_DATE = NULL
				, URL_DOKUMEN = NULL  
				, COUNT_PRINT = 0 
				, SENT_STATUS = 0
				, SENT_DATE = NULL
				, ACTIVE_STATUS = 0
				, ACTIVE_DATE = NULL
				, PEJABAT_PROCCESS_BY = NULL
				, CERT_STATUS = 1
				WHERE CERT_STATUS IN (1,2,3,5,9)"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("UPDATE BSRE_BIODATA SET 
				REQ_DATE = SYSDATE
				, PEJABAT_PROCESS_DATE = NULL
				, URL_DOKUMEN = NULL  
				, COUNT_PRINT = 0 
				, SENT_STATUS = 0
				, SENT_DATE = NULL
				, ACTIVE_STATUS = 0
				, ACTIVE_DATE = NULL
				, PEJABAT_PROCCESS_BY = NULL
				, CERT_STATUS = 1
				WHERE PEJABAT_PROCESS_DATE IS NULL"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("UPDATE BSRE_BIODATA_SURKET SET 
				REQ_DATE = SYSDATE
				, PEJABAT_PROCESS_DATE = NULL
				, URL_DOKUMEN = NULL  
				, COUNT_PRINT = 0 
				, SENT_STATUS = 0
				, SENT_DATE = NULL
				, ACTIVE_STATUS = 0
				, ACTIVE_DATE = NULL
				, PEJABAT_PROCCESS_BY = NULL
				, CERT_STATUS = 1
				WHERE CERT_STATUS IN (1,2,3,5,9)"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("UPDATE BSRE_BIODATA_SURKET SET 
				REQ_DATE = SYSDATE
				, PEJABAT_PROCESS_DATE = NULL
				, URL_DOKUMEN = NULL  
				, COUNT_PRINT = 0 
				, SENT_STATUS = 0
				, SENT_DATE = NULL
				, ACTIVE_STATUS = 0
				, ACTIVE_DATE = NULL
				, PEJABAT_PROCCESS_BY = NULL
				, CERT_STATUS = 1
				WHERE PEJABAT_PROCESS_DATE IS NULL"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("UPDATE BSRE_BIODATA_SKTT SET 
				REQ_DATE = SYSDATE
				, PEJABAT_PROCESS_DATE = NULL
				, URL_DOKUMEN = NULL  
				, COUNT_PRINT = 0 
				, SENT_STATUS = 0
				, SENT_DATE = NULL
				, ACTIVE_STATUS = 0
				, ACTIVE_DATE = NULL
				, PEJABAT_PROCCESS_BY = NULL
				, CERT_STATUS = 1
				WHERE CERT_STATUS IN (1,2,3,5,9)"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("UPDATE BSRE_BIODATA_SKTT SET 
				REQ_DATE = SYSDATE
				, PEJABAT_PROCESS_DATE = NULL
				, URL_DOKUMEN = NULL  
				, COUNT_PRINT = 0 
				, SENT_STATUS = 0
				, SENT_DATE = NULL
				, ACTIVE_STATUS = 0
				, ACTIVE_DATE = NULL
				, PEJABAT_PROCCESS_BY = NULL
				, CERT_STATUS = 1
				WHERE PEJABAT_PROCESS_DATE IS NULL"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("UPDATE BIODATA_WNA A SET  A.CERT_STATUS_BIO = 1  WHERE EXISTS (SELECT 1 FROM BSRE_BIODATA_OA B WHERE A.NIK = B.NO_DOC AND PEJABAT_PROCESS_DATE IS NULL)"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("UPDATE BIODATA_WNA A SET  A.CERT_STATUS_BIO = 1  WHERE EXISTS (SELECT 1 FROM BSRE_BIODATA_OA B WHERE A.NIK = B.NO_DOC AND PEJABAT_PROCESS_DATE IS NULL)"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("UPDATE BSRE_BIODATA_SKTT SET 
				REQ_DATE = SYSDATE
				, PEJABAT_PROCESS_DATE = NULL
				, URL_DOKUMEN = NULL  
				, COUNT_PRINT = 0 
				, SENT_STATUS = 0
				, SENT_DATE = NULL
				, ACTIVE_STATUS = 0
				, ACTIVE_DATE = NULL
				, PEJABAT_PROCCESS_BY = NULL
				, CERT_STATUS = 1
				WHERE CERT_STATUS IN (1,2,3,5,9)"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("UPDATE BSRE_BIODATA_SKTT SET 
				REQ_DATE = SYSDATE
				, PEJABAT_PROCESS_DATE = NULL
				, URL_DOKUMEN = NULL  
				, COUNT_PRINT = 0 
				, SENT_STATUS = 0
				, SENT_DATE = NULL
				, ACTIVE_STATUS = 0
				, ACTIVE_DATE = NULL
				, PEJABAT_PROCCESS_BY = NULL
				, CERT_STATUS = 1
				WHERE PEJABAT_PROCESS_DATE IS NULL"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("UPDATE BIODATA_WNA A SET  A.CERT_STATUS = 1  WHERE EXISTS (SELECT 1 FROM BSRE_BIODATA_SKTT B WHERE A.NIK = B.NO_DOC AND PEJABAT_PROCESS_DATE IS NULL)"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("UPDATE BIODATA_WNA A SET  A.CERT_STATUS = 1  WHERE EXISTS (SELECT 1 FROM BSRE_BIODATA_SKTT B WHERE A.NIK = B.NO_DOC AND B.CERT_STATUS IN (1,2,3,5,9))"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("UPDATE PINDAH_HEADER A SET 
				A.CERT_STATUS = 1 
				WHERE EXISTS (SELECT 1 FROM BSRE_PERPINDAHAN B WHERE A.NO_PINDAH = B.NO_DOC AND B.CERT_STATUS IN (1,2,3,5,9))"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("UPDATE BSRE_PERPINDAHAN SET 
				REQ_DATE = SYSDATE
				, PEJABAT_PROCESS_DATE = NULL
				, URL_DOKUMEN = NULL  
				, COUNT_PRINT = 0 
				, SENT_STATUS = 0
				, SENT_DATE = NULL
				, ACTIVE_STATUS = 0
				, ACTIVE_DATE = NULL
				, PEJABAT_PROCCESS_BY = NULL
				, CERT_STATUS = 1
				WHERE CERT_STATUS IN (1,2,3,5,9)"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("UPDATE BSRE_PERPINDAHAN SET 
				REQ_DATE = SYSDATE
				, PEJABAT_PROCESS_DATE = NULL
				, URL_DOKUMEN = NULL  
				, COUNT_PRINT = 0 
				, SENT_STATUS = 0
				, SENT_DATE = NULL
				, ACTIVE_STATUS = 0
				, ACTIVE_DATE = NULL
				, PEJABAT_PROCCESS_BY = NULL
				, CERT_STATUS = 1
				WHERE PEJABAT_PROCESS_DATE IS NULL"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("UPDATE CAPIL_MATI A SET 
				A.CERT_STATUS = 1 
				WHERE EXISTS (SELECT 1 FROM BSRE_KEMATIAN B WHERE A.ADM_AKTA_NO = B.NO_DOKUMEN AND B.CERT_STATUS IN (1,2,3,5,9))"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("UPDATE BSRE_KEMATIAN SET 
				REQ_DATE = SYSDATE
				, PEJABAT_PROCESS_DATE = NULL
				, URL_DOKUMEN = NULL  
				, COUNT_PRINT = 0 
				, SENT_STATUS = 0
				, SENT_DATE = NULL
				, ACTIVE_STATUS = 0
				, ACTIVE_DATE = NULL
				, PEJABAT_PROCCESS_BY = NULL
				, CERT_STATUS = 1
				WHERE PEJABAT_PROCESS_DATE IS NULL"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("UPDATE BSRE_KEMATIAN SET 
				REQ_DATE = SYSDATE
				, PEJABAT_PROCESS_DATE = NULL
				, URL_DOKUMEN = NULL  
				, COUNT_PRINT = 0 
				, SENT_STATUS = 0
				, SENT_DATE = NULL
				, ACTIVE_STATUS = 0
				, ACTIVE_DATE = NULL
				, PEJABAT_PROCCESS_BY = NULL
				, CERT_STATUS = 1
				WHERE CERT_STATUS IN (1,2,3,5,9)"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("UPDATE CAPIL_LAHIR_TNP_ASAL_USUL A SET 
				A.CERT_STATUS = 1 
				WHERE EXISTS (SELECT 1 FROM BSRE_KELAHIRAN_TAU B WHERE A.ADM_AKTA_NO = B.NO_DOKUMEN AND B.CERT_STATUS IN (1,2,3,5,9))"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("UPDATE BSRE_KELAHIRAN_TAU SET 
				REQ_DATE = SYSDATE
				, PEJABAT_PROCESS_DATE = NULL
				, URL_DOKUMEN = NULL  
				, COUNT_PRINT = 0 
				, SENT_STATUS = 0
				, SENT_DATE = NULL
				, ACTIVE_STATUS = 0
				, ACTIVE_DATE = NULL
				, PEJABAT_PROCCESS_BY = NULL
				, CERT_STATUS = 1
				WHERE PEJABAT_PROCESS_DATE IS NULL"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("UPDATE BSRE_KELAHIRAN_TAU SET 
				REQ_DATE = SYSDATE
				, PEJABAT_PROCESS_DATE = NULL
				, URL_DOKUMEN = NULL  
				, COUNT_PRINT = 0 
				, SENT_STATUS = 0
				, SENT_DATE = NULL
				, ACTIVE_STATUS = 0
				, ACTIVE_DATE = NULL
				, PEJABAT_PROCCESS_BY = NULL
				, CERT_STATUS = 1
				WHERE CERT_STATUS IN (1,2,3,5,9)"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("UPDATE CAPIL_LAHIR_MATI A SET 
				A.CERT_STATUS = 1 
				WHERE EXISTS (SELECT 1 FROM BSRE_LAHIR_MATI B WHERE A.BAYI_NO = B.NO_DOKUMEN AND B.CERT_STATUS IN (1,2,3,5,9))"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("UPDATE BSRE_LAHIR_MATI SET 
				REQ_DATE = SYSDATE
				, PEJABAT_PROCESS_DATE = NULL
				, URL_DOKUMEN = NULL  
				, COUNT_PRINT = 0 
				, SENT_STATUS = 0
				, SENT_DATE = NULL
				, ACTIVE_STATUS = 0
				, ACTIVE_DATE = NULL
				, PEJABAT_PROCCESS_BY = NULL
				, CERT_STATUS = 1
				WHERE PEJABAT_PROCESS_DATE IS NULL"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("UPDATE BSRE_LAHIR_MATI SET 
				REQ_DATE = SYSDATE
				, PEJABAT_PROCESS_DATE = NULL
				, URL_DOKUMEN = NULL  
				, COUNT_PRINT = 0 
				, SENT_STATUS = 0
				, SENT_DATE = NULL
				, ACTIVE_STATUS = 0
				, ACTIVE_DATE = NULL
				, PEJABAT_PROCCESS_BY = NULL
				, CERT_STATUS = 1
				WHERE CERT_STATUS IN (1,2,3,5,9)"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("UPDATE CAPIL_BAKAK A SET 
				A.CERT_STATUS = 1 
				WHERE EXISTS (SELECT 1 FROM BSRE_BAKAK B WHERE A.BAYI_NO_AKTA_LAHIR = B.NO_DOKUMEN AND B.CERT_STATUS IN (1,2,3,5,9))"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("UPDATE BSRE_BAKAK SET 
				REQ_DATE = SYSDATE
				, PEJABAT_PROCESS_DATE = NULL
				, URL_DOKUMEN = NULL  
				, COUNT_PRINT = 0 
				, SENT_STATUS = 0
				, SENT_DATE = NULL
				, ACTIVE_STATUS = 0
				, ACTIVE_DATE = NULL
				, PEJABAT_PROCCESS_BY = NULL
				, CERT_STATUS = 1
				WHERE PEJABAT_PROCESS_DATE IS NULL"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("UPDATE BSRE_BAKAK SET 
				REQ_DATE = SYSDATE
				, PEJABAT_PROCESS_DATE = NULL
				, URL_DOKUMEN = NULL  
				, COUNT_PRINT = 0 
				, SENT_STATUS = 0
				, SENT_DATE = NULL
				, ACTIVE_STATUS = 0
				, ACTIVE_DATE = NULL
				, PEJABAT_PROCCESS_BY = NULL
				, CERT_STATUS = 1
				WHERE CERT_STATUS IN (1,2,3,5,9)"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("UPDATE CAPIL_LAHIR A 
				SET
				A.CERT_STATUS = 1
				, A.STATUS_PUBLISH = NULL
				, A.CERT_CODE = NULL
				, A.JABATAN_PEJABAT = NULL
				, A.JABATAN_VERIFIKATOR = NULL
				, A.KEY_PEJABAT = NULL
				, A.KEY_VERIFIKATOR = NULL
				WHERE EXISTS (SELECT 1 FROM BSRE_KELAHIRAN B WHERE A.ADM_AKTA_NO = B.NO_DOKUMEN AND B.CERT_STATUS IN (1,2,3,5,9))"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("UPDATE BSRE_KELAHIRAN SET 
				 PEJABAT_PROCESS_DATE = NULL
				, URL_DOKUMEN = NULL  
				, COUNT_PRINT = 0 
				, SENT_STATUS = 0
				, SENT_DATE = NULL
				, PEJABAT_PROCCESS_BY = NULL
				, CERT_STATUS = 1
				WHERE CERT_STATUS IN (1,2,3,5,9)"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("UPDATE CAPIL_LAHIR A 
				SET
				A.CERT_STATUS = 1
				, A.STATUS_PUBLISH = NULL
				, A.CERT_CODE = NULL
				, A.JABATAN_PEJABAT = NULL
				, A.JABATAN_VERIFIKATOR = NULL
				, A.KEY_PEJABAT = NULL
				, A.KEY_VERIFIKATOR = NULL
				WHERE EXISTS (SELECT 1 FROM BSRE_KELAHIRAN B WHERE A.ADM_AKTA_NO = B.NO_DOKUMEN AND PEJABAT_PROCESS_DATE IS NULL)"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("UPDATE BSRE_KELAHIRAN SET 
				 PEJABAT_PROCESS_DATE = NULL
				, URL_DOKUMEN = NULL  
				, COUNT_PRINT = 0 
				, SENT_STATUS = 0
				, SENT_DATE = NULL
				, PEJABAT_PROCCESS_BY = NULL
				, CERT_STATUS = 1
				WHERE PEJABAT_PROCESS_DATE IS NULL"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("UPDATE CAPIL_KAWIN A SET 
				A.CERT_STATUS = 1 
				WHERE EXISTS (SELECT 1 FROM BSRE_PERKAWINAN B WHERE A.ADM_AKTA_NO = B.NO_DOKUMEN AND B.CERT_STATUS IN (1,2,3,5,9))"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("UPDATE BSRE_PERKAWINAN SET 
				REQ_DATE = SYSDATE
				, PEJABAT_PROCESS_DATE = NULL
				, URL_DOKUMEN = NULL  
				, COUNT_PRINT = 0 
				, SENT_STATUS = 0
				, SENT_DATE = NULL
				, ACTIVE_STATUS = 0
				, ACTIVE_DATE = NULL
				, PEJABAT_PROCCESS_BY = NULL
				, CERT_STATUS = 1
				WHERE PEJABAT_PROCESS_DATE IS NULL"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("UPDATE BSRE_PERKAWINAN SET 
				REQ_DATE = SYSDATE
				, PEJABAT_PROCESS_DATE = NULL
				, URL_DOKUMEN = NULL  
				, COUNT_PRINT = 0 
				, SENT_STATUS = 0
				, SENT_DATE = NULL
				, ACTIVE_STATUS = 0
				, ACTIVE_DATE = NULL
				, PEJABAT_PROCCESS_BY = NULL
				, CERT_STATUS = 1
				WHERE CERT_STATUS IN (1,2,3,5,9)"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("UPDATE CAPIL_CERAI A SET 
				A.CERT_STATUS = 1 
				WHERE EXISTS (SELECT 1 FROM BSRE_PERCERAIAN B WHERE A.ADM_AKTA_NO = B.NO_DOKUMEN AND B.CERT_STATUS IN (1,2,3,5,9))"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("UPDATE BSRE_PERCERAIAN SET 
				REQ_DATE = SYSDATE
				, PEJABAT_PROCESS_DATE = NULL
				, URL_DOKUMEN = NULL  
				, COUNT_PRINT = 0 
				, SENT_STATUS = 0
				, SENT_DATE = NULL
				, ACTIVE_STATUS = 0
				, ACTIVE_DATE = NULL
				, PEJABAT_PROCCESS_BY = NULL
				, CERT_STATUS = 1
				WHERE PEJABAT_PROCESS_DATE IS NULL"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("UPDATE BSRE_PERCERAIAN SET 
				REQ_DATE = SYSDATE
				, PEJABAT_PROCESS_DATE = NULL
				, URL_DOKUMEN = NULL  
				, COUNT_PRINT = 0 
				, SENT_STATUS = 0
				, SENT_DATE = NULL
				, ACTIVE_STATUS = 0
				, ACTIVE_DATE = NULL
				, PEJABAT_PROCCESS_BY = NULL
				, CERT_STATUS = 1
				WHERE CERT_STATUS IN (1,2,3,5,9)"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("UPDATE CAPIL_AKUI_ANAK A SET 
				A.CERT_STATUS = 1 
				WHERE EXISTS (SELECT 1 FROM BSRE_PENGAKUAN_ANAK B WHERE A.ANAK_NO = B.NO_DOKUMEN AND B.CERT_STATUS IN (1,2,3,5,9))"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("UPDATE BSRE_PENGAKUAN_ANAK SET 
				REQ_DATE = SYSDATE
				, PEJABAT_PROCESS_DATE = NULL
				, URL_DOKUMEN = NULL  
				, COUNT_PRINT = 0 
				, SENT_STATUS = 0
				, SENT_DATE = NULL
				, ACTIVE_STATUS = 0
				, ACTIVE_DATE = NULL
				, PEJABAT_PROCCESS_BY = NULL
				, CERT_STATUS = 1
				WHERE PEJABAT_PROCESS_DATE IS NULL"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("UPDATE BSRE_PENGAKUAN_ANAK SET 
				REQ_DATE = SYSDATE
				, PEJABAT_PROCESS_DATE = NULL
				, URL_DOKUMEN = NULL  
				, COUNT_PRINT = 0 
				, SENT_STATUS = 0
				, SENT_DATE = NULL
				, ACTIVE_STATUS = 0
				, ACTIVE_DATE = NULL
				, PEJABAT_PROCCESS_BY = NULL
				, CERT_STATUS = 1
				WHERE CERT_STATUS IN (1,2,3,5,9)"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("UPDATE CAPIL_SAH_ANAK A SET 
				A.CERT_STATUS = 1 
				WHERE EXISTS (SELECT 1 FROM BSRE_PENGESAHAN_ANAK B WHERE A.ANAK_NO = B.NO_DOKUMEN AND B.CERT_STATUS IN (1,2,3,5,9))"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("UPDATE BSRE_PENGESAHAN_ANAK SET 
				REQ_DATE = SYSDATE
				, PEJABAT_PROCESS_DATE = NULL
				, URL_DOKUMEN = NULL  
				, COUNT_PRINT = 0 
				, SENT_STATUS = 0
				, SENT_DATE = NULL
				, ACTIVE_STATUS = 0
				, ACTIVE_DATE = NULL
				, PEJABAT_PROCCESS_BY = NULL
				, CERT_STATUS = 1
				WHERE PEJABAT_PROCESS_DATE IS NULL"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("UPDATE BSRE_PENGESAHAN_ANAK SET 
				REQ_DATE = SYSDATE
				, PEJABAT_PROCESS_DATE = NULL
				, URL_DOKUMEN = NULL  
				, COUNT_PRINT = 0 
				, SENT_STATUS = 0
				, SENT_DATE = NULL
				, ACTIVE_STATUS = 0
				, ACTIVE_DATE = NULL
				, PEJABAT_PROCCESS_BY = NULL
				, CERT_STATUS = 1
				WHERE CERT_STATUS IN (1,2,3,5,9)"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("DELETE FROM BSRE_KARTU_KELUARGA A WHERE CERT_STATUS=1 AND NOT EXISTS(SELECT 1 FROM DATA_KELUARGA B WHERE A.NO_DOC = B.NO_KK)"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("DELETE FROM BSRE_KELAHIRAN A WHERE CERT_STATUS=1 AND NOT EXISTS(SELECT 1 FROM CAPIL_LAHIR B WHERE A.NO_DOKUMEN = B.ADM_AKTA_NO)"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("DELETE FROM BSRE_KEMATIAN A WHERE CERT_STATUS=1 AND NOT EXISTS(SELECT 1 FROM CAPIL_MATI B WHERE A.NO_DOKUMEN = B.ADM_AKTA_NO)"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("DELETE FROM BSRE_PERKAWINAN A WHERE CERT_STATUS=1 AND NOT EXISTS(SELECT 1 FROM CAPIL_KAWIN B WHERE A.NO_DOKUMEN = B.ADM_AKTA_NO)"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("DELETE FROM BSRE_PERCERAIAN A WHERE CERT_STATUS=1 AND NOT EXISTS(SELECT 1 FROM CAPIL_CERAI B WHERE A.NO_DOKUMEN = B.ADM_AKTA_NO)"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("DELETE FROM BSRE_PENGAKUAN_ANAK A WHERE CERT_STATUS=1 AND NOT EXISTS(SELECT 1 FROM CAPIL_AKUI_ANAK B WHERE A.NO_DOKUMEN = B.ANAK_NO)"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("DELETE FROM BSRE_PENGESAHAN_ANAK A WHERE CERT_STATUS=1 AND NOT EXISTS(SELECT 1 FROM CAPIL_SAH_ANAK B WHERE A.NO_DOKUMEN = B.ANAK_NO)"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("DELETE FROM BSRE_BIODATA_OA A WHERE CERT_STATUS=1 AND NOT EXISTS(SELECT 1 FROM BIODATA_WNA B WHERE A.NO_DOC = B.NIK)"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("DELETE FROM BSRE_BIODATA_SKTT A WHERE CERT_STATUS=1 AND NOT EXISTS(SELECT 1 FROM BIODATA_WNA B WHERE A.NO_DOC = B.NIK)"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("DELETE FROM BSRE_BAKAK A WHERE CERT_STATUS=1 AND NOT EXISTS(SELECT 1 FROM CAPIL_BAKAK B WHERE A.NO_DOKUMEN = B.BAYI_NO_AKTA_LAHIR)"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("DELETE FROM BSRE_KELAHIRAN_TAU A WHERE CERT_STATUS=1 AND NOT EXISTS(SELECT 1 FROM CAPIL_LAHIR_TNP_ASAL_USUL B WHERE A.NO_DOKUMEN = B.ADM_AKTA_NO)"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("DELETE FROM BSRE_LAHIR_MATI A WHERE CERT_STATUS=1 AND NOT EXISTS(SELECT 1 FROM CAPIL_LAHIR_MATI B WHERE A.NO_DOKUMEN = B.BAYI_NO)"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("UPDATE DATA_KELUARGA A SET A.CERT_STATUS = 0, CERT_CODE = NULL  WHERE A.CERT_STATUS = 1 AND NOT EXISTS (SELECT 1 FROM BSRE_KARTU_KELUARGA B WHERE A.NO_KK = B.NO_DOC)"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("UPDATE CAPIL_LAHIR A SET A.CERT_STATUS = 0, CERT_CODE = NULL  WHERE A.CERT_STATUS = 1 AND NOT EXISTS (SELECT 1 FROM BSRE_KELAHIRAN B WHERE A.ADM_AKTA_NO = B.NO_DOKUMEN)"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("UPDATE CAPIL_MATI A SET A.CERT_STATUS = 0, CERT_CODE = NULL  WHERE A.CERT_STATUS = 1 AND NOT EXISTS (SELECT 1 FROM BSRE_KEMATIAN B WHERE A.ADM_AKTA_NO = B.NO_DOKUMEN)"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("UPDATE CAPIL_KAWIN A SET A.CERT_STATUS = 0, CERT_CODE = NULL  WHERE A.CERT_STATUS = 1 AND NOT EXISTS (SELECT 1 FROM BSRE_PERKAWINAN B WHERE A.ADM_AKTA_NO = B.NO_DOKUMEN)"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("UPDATE CAPIL_CERAI A SET A.CERT_STATUS = 0, CERT_CODE = NULL  WHERE A.CERT_STATUS = 1 AND NOT EXISTS (SELECT 1 FROM BSRE_PERCERAIAN B WHERE A.ADM_AKTA_NO = B.NO_DOKUMEN)"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("UPDATE CAPIL_AKUI_ANAK A SET A.CERT_STATUS = 0, CERT_CODE = NULL  WHERE A.CERT_STATUS = 1 AND NOT EXISTS (SELECT 1 FROM BSRE_PENGAKUAN_ANAK B WHERE A.ANAK_NO = B.NO_DOKUMEN)"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("UPDATE CAPIL_SAH_ANAK A SET A.CERT_STATUS = 0, CERT_CODE = NULL  WHERE A.CERT_STATUS = 1 AND NOT EXISTS (SELECT 1 FROM BSRE_PENGESAHAN_ANAK B WHERE A.ANAK_NO = B.NO_DOKUMEN)"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("UPDATE CAPIL_BAKAK A SET A.CERT_STATUS = 0, CERT_CODE = NULL  WHERE A.CERT_STATUS = 1 AND NOT EXISTS (SELECT 1 FROM BSRE_BAKAK B WHERE A.BAYI_NO_AKTA_LAHIR = B.NO_DOKUMEN)"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("UPDATE BIODATA_WNA A SET A.CERT_STATUS_BIO = 0, CERT_CODE = NULL  WHERE A.CERT_STATUS_BIO = 1 AND NOT EXISTS (SELECT 1 FROM BSRE_BIODATA_OA B WHERE A.NIK = B.NO_DOC)"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("UPDATE BIODATA_WNA A SET A.CERT_STATUS = 0, CERT_CODE = NULL  WHERE A.CERT_STATUS = 1 AND NOT EXISTS (SELECT 1 FROM BSRE_BIODATA_SKTT B WHERE A.NIK = B.NO_DOC)"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("UPDATE CAPIL_LAHIR_TNP_ASAL_USUL A SET A.CERT_STATUS = 0, CERT_CODE = NULL  WHERE A.CERT_STATUS = 1 AND NOT EXISTS (SELECT 1 FROM BSRE_KELAHIRAN_TAU B WHERE A.ADM_AKTA_NO = B.NO_DOKUMEN)"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("UPDATE CAPIL_LAHIR_MATI A SET A.CERT_STATUS = 0, CERT_CODE = NULL  WHERE A.CERT_STATUS = 1 AND NOT EXISTS (SELECT 1 FROM BSRE_LAHIR_MATI B WHERE A.BAYI_NO = B.NO_DOKUMEN)"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("DELETE FROM BSRE_BIODATA A WHERE CERT_STATUS =1
				AND NOT EXISTS (SELECT 1 FROM BIODATA_WNI B WHERE A.NO_DOC = B.NIK)"));
		    });

		    \DB::connection('db222')->commit();
		} catch (\Exception $e){ 
		    \DB::connection('db222')->rollback();
		    //do something with $e->getMessage();
		}
     
    }


    public function update_siak_pindah(Request $request)
    {

    	 AndrApi::do_update_siak_pindah();
		 $output = array(
	                "message_type"=>1,
	                "message"=>"Prosedure Telah Di Eksekusi !"
	        );
	        return $output; 
    }

    public static function do_update_siak_pindah()
    {

		try {
		    \DB::connection('db222')->transaction(function(){
		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("INSERT INTO DEMOGRAPHICS@DB221 SELECT  A.NIK ,A.NAMA_LGKP ,A.TMPT_LHR ,A.NAMA_PROP ,A.NAMA_KAB ,A.NAMA_KEC ,A.NAMA_KEL ,A.NO_PROP ,A.NO_KAB ,A.NO_KEC ,A.NO_KEL ,A.AGAMA ,A.JENIS_KLMIN ,A.GOL_DRH ,A.STAT_KWN ,A.TGL_LHR ,A.IS_DIRTY ,A.CURRENT_STATUS_CODE ,A.CREATED ,A.CREATED_USERNAME ,A.NO_KTP ,A.NO_PASPOR ,A.NO_RT ,A.NO_RW ,A.PNYDNG_CCT ,A.KLAIN_FSK ,A.JENIS_PKRJN ,A.KEBANGSAAN ,A.GELAR ,A.NO_AKTA_LHR ,A.NO_AKTA_KWN ,A.NO_AKTA_CRAI ,A.NAMA_PET_REG ,A.NAMA_LGKP_IBU ,A.NIK_IBU ,A.NAMA_LGKP_AYAH ,A.NIK_AYAH ,A.NO_KK ,A.STAT_KTP ,A.NAMA_KET_RT ,A.NAMA_KET_RW ,A.STAT_HBKEL ,A.KET_AGAMA ,A.TMPT_SBL ,A.TGL_PJG_KTP ,A.TGL_AKH_PASPOR ,A.TGL_KWN ,A.TGL_CRAI ,A.TGL_ENTRI ,A.TGL_UBAH ,A.TGL_CETAK_KTP ,A.TGL_GANTI_KTP ,A.GLR_AGAMA ,A.GLR_BANGSAWAN ,A.GLR_AKADEMIS ,A.EXCEPTION_CODE ,A.LAST_UPDATED ,A.LAST_UPDATED_USERNAME ,A.UPLOAD_LOCATION ,A.REGION_ID ,A.LOCAL_ID ,A.AKTA_LHR ,A.AKTA_KWN ,A.AKTA_CRAI ,A.PDDK_AKH ,A.STAT_HIDUP ,A.FLAG_STATUS ,A.NAMA_KEP ,A.ALAMAT ,A.DUSUN ,A.KODE_POS ,A.TELP ,A.KWRNGRN ,A.DOK_IMGR ,A.TGL_DTBIT ,A.TGL_AKH_DOK ,A.SIAK_FLAG ,A.COUNT_KTP ,A.NO_DOK FROM DEMOGRAPHICS@DB2 A LEFT JOIN DEMOGRAPHICS@DB221 B ON A.NIK = B.NIK WHERE A.CURRENT_STATUS_CODE = 'PRINT_READY_RECORD' AND B.NIK IS NULL"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("DELETE FROM YZ_SIAK_PINDAH"));
		        
		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("INSERT INTO  YZ_SIAK_PINDAH
					     SELECT C.NIK NIK
								, C.NO_KK NO_KK
								, C.NAMA_LGKP NAMA_LENGKAP
								, C.JENIS_KLMIN JENIS_KELAMIN
								, CASE WHEN C.JENIS_KLMIN = 1 THEN 'LAKI-LAKI' ELSE 'PEREMPUAN' END AS JENIS_KELAMIN_DESC
								, C.TMPT_LHR TEMPAT_LAHIR
								, C.TGL_LHR TANGGAL_LAHIR
								, TRUNC(MONTHS_BETWEEN(TO_DATE('01-01-2016','DD-MM-YYYY'),C.TGL_LHR)/12) UMUR
								, C.AGAMA AGAMA
								, UPPER(F5_GET_REF_WNI(F5_TO_NUMBER(C.AGAMA,7), 501)) AGAMA_DESC
								, C.PDDK_AKH PENDIDIKAN
								, UPPER(F5_GET_REF_WNI(F5_TO_NUMBER(C.PDDK_AKH,7), 101)) PENDIDIKAN_DESC
								, C.JENIS_PKRJN JENIS_PEKERJAAN
								, UPPER(F5_GET_REF_WNI(F5_TO_NUMBER(C.JENIS_PKRJN,7), 201)) JENIS_PEKERJAAN_DESC
								, C.GOL_DRH GOL_DRH
								, UPPER(F5_GET_REF_WNI(F5_TO_NUMBER(C.GOL_DRH,7), 401)) GOL_DRH_DESC
								, C.STAT_KWN STATUS_PERKAWINAN
								, UPPER(F5_GET_REF_WNI(F5_TO_NUMBER(C.STAT_KWN,7), 601)) STATUS_PERKAWINAN_DESC
								, C.STAT_HBKEL HUBUNGAN_DLM_KELUARGA
								, UPPER(F5_GET_REF_WNI(F5_TO_NUMBER(C.STAT_HBKEL,7), 301)) HUBUNGAN_DLM_KELUARGA_DESC
								, 'WNI' KEWARGANEGARAAN
								, '-' NO_PASPOR
								, C.NIK_AYAH NIK_AYAH
								, C.NAMA_LGKP_AYAH NAMA_AYAH
								, C.NIK_IBU NIK_IBU
								, C.NAMA_LGKP_IBU NAMA_IBU
								, C.NO_PROP NO_PROP
								, F5_GET_NAMA_PROVINSI(C.NO_PROP) NAMA_PROP
								, C.NO_KAB NO_KAB
								, F5_GET_NAMA_KABUPATEN(C.NO_PROP,C.NO_KAB) NAMA_KAB
								, C.NO_KEC NO_KEC
								, F5_GET_NAMA_KECAMATAN(C.NO_PROP,C.NO_KAB,C.NO_KEC) NAMA_KEC
								, C.NO_KEL NO_KEL
								, F5_GET_NAMA_KELURAHAN(C.NO_PROP,C.NO_KAB,C.NO_KEC,C.NO_KEL) NAMA_KEL 
								, CREATED_DATE
								FROM (SELECT MAX(A.CREATED_DATE) CREATED_DATE,DEST_NO_PROP,DEST_NO_KAB,KLASIFIKASI_PINDAH, NIK FROM PINDAH_HEADER A 
					            INNER JOIN PINDAH_DETAIL B ON A.NO_PINDAH = B.NO_PINDAH GROUP BY  NIK ,KLASIFIKASI_PINDAH,DEST_NO_PROP,DEST_NO_KAB) B
					            INNER JOIN BIODATA_WNI C ON B.NIK = C.NIK 
					            WHERE B.DEST_NO_PROP = 32 AND B.DEST_NO_KAB = 73 AND KLASIFIKASI_PINDAH > 3 
					            AND (C.TGL_LHR<=ADD_MONTHS(SYSDATE,-12*17) OR C.STAT_KWN<>1) 
								AND (C.FLAG_STATUS = '0' OR (C.FLAG_STATUS = '2' AND C.FLAG_PINDAH IN(1,2,3))) 
								AND C.NO_PROP = 32 AND C.NO_KAB =73 ORDER BY CREATED_DATE DESC"));
		        
		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("DELETE FROM YZ_SIAK_PINDAH A WHERE EXISTS (SELECT 1 FROM FACES@DB2 B WHERE A.NIK = B.NIK)"));
		        
		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("DELETE FROM YZ_SIAK_WKTP"));
		        
		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("INSERT INTO  YZ_SIAK_WKTP
           			 SELECT B.NIK, B.NO_KK, B.NO_KEC, B.NAMA_LGKP FROM BIODATA_WNI B LEFT JOIN DEMOGRAPHICS_ALL@DB2 A 
           			 ON A.NIK = B.NIK WHERE A.NIK IS NULL 
           			 AND (B.TGL_LHR<=ADD_MONTHS(SYSDATE,-12*17) OR B.STAT_KWN<>1) 
           			 AND (B.FLAG_STATUS = '0' OR (B.FLAG_STATUS = '2' AND B.FLAG_PINDAH IN(1,2,3)))"));

		        
		    });

		    \DB::connection('db222')->commit();
		} catch (\Exception $e){ 
		    \DB::connection('db222')->rollback();
		    //do something with $e->getMessage();
		}
     
    }


    public function update_anjungan_kia(Request $request)
    {

    	 AndrApi::do_update_anjungan_kia();
		 $output = array(
	                "message_type"=>1,
	                "message"=>"Prosedure Telah Di Eksekusi !"
	        );
	        return $output; 
    }

    public static function do_update_anjungan_kia()
    {

		try {
		    \DB::connection('db222')->transaction(function(){
		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("INSERT INTO LTS_KIA
						SELECT A.NIK, C.TGL_CETAK
						, C.TGL_CETAK_BY
						, C.TGL_GANTI
						, C.TGL_GANTI_BY
						, C.TGL_PERPANJANG
						, C.TGL_PERPANJANG_BY
						, 'CETAK_KIA' CREATED_BY
						, SYSDATE CREATED_DATE
						, C.LAST_PRINTED
						, 'N' EXPIRED_STATUS
						, C.ANAK_KE
						, C.COUNT_CETAK_KIA  FROM BIODATA_WNI A 
						INNER JOIN CAPIL_LAHIR B ON TO_CHAR(A.NIK) = B.BAYI_NIK AND A.NO_AKTA_LHR = B.ADM_AKTA_NO
						LEFT JOIN LTS_KIA C ON A.NIK = C.NIK
						WHERE C.NIK IS NULL AND TRUNC(MONTHS_BETWEEN(SYSDATE,A.TGL_LHR)/12) < 5"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("INSERT INTO T5_SEQN_KIA_PRINT
						SELECT A.SEQN_ID,
						A.NO_DOKUMEN,
						A.NIK,
						A.NAMA_LGKP,
						A.JENIS_KLMIN,
						A.NO_KK,
						A.ALAMAT,
						A.PRINT_TYPE,
						A.STATUS_BLANGKO,
						A.PRINTED_BY,
						A.PRINTED_DATE,
						A.CHECKED_BY,
						A.CHECKED_DATE,
						A.REVOKED_BY,
						A.REVOKED_DATE,
						A.NO_BLANGKO,
						A.NO_PROP,
						A.NO_KAB,
						A.NO_KEC,
						A.NO_KEL,
						A.FILENAME,
						A.TIPE_DOK FROM (
						SELECT CONCAT(CONCAT('3273-',TO_CHAR(TGL_CETAK,'DDMMYYYY')),CONCAT('-7',LPAD(TO_CHAR(ID), 6, '0'))) SEQN_ID,A.NIK NO_DOKUMEN,A.NIK, B.NAMA_LGKP, B.JENIS_KLMIN, B.NO_KK,
						CONCAT(CONCAT(CONCAT(CONCAT(concat(concat(CONCAT(CONCAT(CONCAT(CONCAT(C.ALAMAT,'<br/><b>RT. </b>'),C.NO_RT),' <b>RW. </b>'),C.NO_RW),' <b>DUSUN. </b>'),C.DUSUN),'<br/><b>KODEPOS. </b>'),C.KODE_POS),'<b>TELP. </b>'),C.TELP) ALAMAT ,
						0 PRINT_TYPE, 0 STATUS_BLANGKO, 'anjungan_kia' PRINTED_BY, TO_DATE(TO_CHAR(A.TGL_CETAK,'DD-MM-YYYY'),'DD-MM-YYYY') PRINTED_DATE, NULL CHECKED_BY,  NULL CHECKED_DATE,  NULL REVOKED_BY, NULL REVOKED_DATE, NULL NO_BLANGKO, B.NO_PROP, B.NO_KAB, B.NO_KEC, B.NO_KEL, NULL FILENAME, NULL TIPE_DOK
						FROM PRINTING_LOG@DBKIA A 
						INNER JOIN BIODATA_WNI B ON A.NIK = B.NIK
						INNER JOIN DATA_KELUARGA C ON C.NO_KK = B.NO_KK
						) A WHERE NOT EXISTS (SELECT 1 FROM T5_SEQN_KIA_PRINT B WHERE A.SEQN_ID = B.SEQN_ID)"));
		        
		    });

		    \DB::connection('db222')->commit();
		} catch (\Exception $e){ 
		    \DB::connection('db222')->rollback();
		    //do something with $e->getMessage();
		}
     
    }


    public function update_req_done_ktp(Request $request)
    {

    	 AndrApi::do_update_req_done_ktp();
		 $output = array(
	                "message_type"=>1,
	                "message"=>"Prosedure Telah Di Eksekusi !"
	        );
	        return $output; 
    }

    public static function do_update_req_done_ktp()
    {

		try {
		    \DB::connection('db222')->transaction(function(){
		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("UPDATE SIAK_REQ_CETAK_KTP@YZDB SET STEP_PROC = 'C', PROC_DATE = SYSDATE, PROC_BY = 'CETAK_KTP' WHERE STEP_PROC = 'A' AND NIK IN (SELECT NIK FROM (SELECT A.NO_KEC,A.NIK,B.CURRENT_STATUS_CODE,TO_CHAR(A.REQ_DATE,'DD-MM-YYYY') AS REQ_DATE,A.REQ_BY, TO_CHAR(C.KTP_DT,'DD-MM-YYYY') AS PRINTED_DATE,C.CREATED_USERNAME AS PRINTED_BY  FROM SIAK_REQ_CETAK_KTP@YZDB A INNER JOIN DEMOGRAPHICS_ALL@DB2 B ON A.NIK = B.NIK LEFT JOIN (SELECT NIK,CASE WHEN  LAST_UPDATE IS NULL AND PERSONALIZED_DATE IS NULL THEN NULL WHEN LAST_UPDATE IS NULL AND PERSONALIZED_DATE IS NOT NULL THEN PERSONALIZED_DATE ELSE  LAST_UPDATE END AS KTP_DT,CREATED_USERNAME FROM (SELECT NIK,CREATED,LAST_UPDATE,PERSONALIZED_DATE,CREATED_USERNAME, RANK() OVER (PARTITION BY NIK ORDER BY  PERSONALIZED_DATE DESC, LAST_UPDATE DESC, CHIP_ID DESC) RNK FROM CARD_MANAGEMENT@DB5CETAK) WHERE RNK = 1) C ON A.NIK = C.NIK  WHERE A.STEP_PROC = 'A' AND (B.CURRENT_STATUS_CODE LIKE '%CARD%' OR B.CURRENT_STATUS_CODE LIKE '%PRINT%') AND (A.REQ_DATE) < C.KTP_DT))"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("DELETE FROM BIODATA_WNI A WHERE EXISTS (SELECT 1 FROM DEMOGRAPHICS_ALL B WHERE A.NIK = B.NIK AND B.CURRENT_STATUS_CODE = 'DUPLICATE_RECORD')"));
		        
		    });

		    \DB::connection('db222')->commit();
		} catch (\Exception $e){ 
		    \DB::connection('db222')->rollback();
		    //do something with $e->getMessage();
		}
     
    }


    public function update_clean_siak(Request $request)
    {

    	 AndrApi::do_update_clean_siak();
		 $output = array(
	                "message_type"=>1,
	                "message"=>"Prosedure Telah Di Eksekusi !"
	        );
	        return $output; 
    }

    public static function do_update_clean_siak()
    {

		try {
		    \DB::connection('db222')->transaction(function(){
		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("ALTER TRIGGER ADT_BIO_WNI DISABLE"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("ALTER TRIGGER TRG_KONS_BIO_WNI DISABLE"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("INSERT INTO BIODATA_WNI_DELETE SELECT NIK, NO_KTP, TMPT_SBL, NO_PASPOR, TGL_AKH_PASPOR, NAMA_LGKP, JENIS_KLMIN, TMPT_LHR, TGL_LHR, AKTA_LHR, NO_AKTA_LHR, GOL_DRH, AGAMA, STAT_KWN, AKTA_KWN, NO_AKTA_KWN, TGL_KWN, AKTA_CRAI, NO_AKTA_CRAI, TGL_CRAI, STAT_HBKEL, KLAIN_FSK, PNYDNG_CCT, PDDK_AKH, JENIS_PKRJN, NIK_IBU, NAMA_LGKP_IBU, NIK_AYAH, NAMA_LGKP_AYAH, NAMA_KET_RT, NAMA_KET_RW, NAMA_PET_REG, NIP_PET_REG, NAMA_PET_ENTRI, NIP_PET_ENTRI, TGL_ENTRI, NO_KK, JENIS_BNTU, NO_PROP, NO_KAB, NO_KEC, NO_KEL, STAT_HIDUP, TGL_UBAH, TGL_CETAK_KTP, TGL_GANTI_KTP, TGL_PJG_KTP, STAT_KTP, ALS_NUMPANG, PFLAG, CFLAG, SYNC_FLAG, GLR_AKADEMIS, GLR_BANGSAWAN, GLR_AGAMA, DESC_KEPERCAYAAN, DESC_PEKERJAAN, IS_PROS_DATANG, FLAG_STATUS, FLAGSINK, 'SIAKOFF' DELETED_BY, SYSDATE DELETED_DATE FROM BIODATA_WNI A WHERE (FLAG_STATUS = 2 AND TRUNC(TGL_UBAH) <> TRUNC(SYSDATE) OR FLAG_STATUS = 1) AND NOT EXISTS (SELECT 1 FROM BIODATA_WNI_DELETE B WHERE A.NIK = B.NIK)"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("DELETE FROM BIODATA_WNI WHERE FLAG_STATUS = 1"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("DELETE FROM BIODATA_WNI WHERE FLAG_STATUS = 2 AND TRUNC(TGL_UBAH) <> TRUNC(SYSDATE)"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("DELETE FROM BIODATA_WNI WHERE NIK LIKE '1050%'"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("DELETE FROM BIODATA_WNI_DELETE A WHERE EXISTS (SELECT 1 FROM BIODATA_WNI B WHERE A.NIK = B.NIK)"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("DELETE FROM BIODATA_WNI A WHERE NOT EXISTS (SELECT 1 FROM DATA_KELUARGA B WHERE A.NO_KK = B.NO_KK)"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("DELETE FROM DATA_KELUARGA A WHERE NOT EXISTS (SELECT 1 FROM BIODATA_WNA B WHERE A.NO_KK = B.NO_KK) AND NOT EXISTS (SELECT 1 FROM BIODATA_WNI B WHERE A.NO_KK = B.NO_KK )"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("DELETE FROM BIODATA_WNI_DELETE WHERE ROWID  IN ( SELECT MAX(ROWID) FROM BIODATA_WNI_DELETE WHERE NIK IN (SELECT NIK FROM   (SELECT COUNT(1) JML, NIK FROM BIODATA_WNI_DELETE GROUP BY NIK) WHERE JML >1) GROUP BY NIK)"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("DELETE FROM DATA_KELUARGA_DELETE WHERE ROWID  IN ( SELECT MAX(ROWID) FROM DATA_KELUARGA_DELETE WHERE NO_KK IN (SELECT NO_KK FROM   (SELECT COUNT(1) JML, NO_KK FROM DATA_KELUARGA_DELETE GROUP BY NO_KK) WHERE JML >1) GROUP BY NO_KK)"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("ALTER TRIGGER ADT_BIO_WNI ENABLE"));
		        
		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("ALTER TRIGGER TRG_KONS_BIO_WNI ENABLE"));
		        
		    });

		    \DB::connection('db222')->commit();
		} catch (\Exception $e){ 
		    \DB::connection('db222')->rollback();
		    //do something with $e->getMessage();
		}
     
    }


    public function update_kepala_keluarga(Request $request)
    {

    	 AndrApi::do_update_kepala_keluarga();
		 $output = array(
	                "message_type"=>1,
	                "message"=>"Prosedure Telah Di Eksekusi !"
	        );
	        return $output; 
    }

    public static function do_update_kepala_keluarga()
    {
    	// ini_set('MAX_EXECUTION_TIME', '-1');

		try {
		    \DB::connection('db222')->transaction(function(){
		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("ALTER TRIGGER ADT_BIO_WNI DISABLE"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("ALTER TRIGGER TRG_KONS_BIO_WNI DISABLE"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("UPDATE DATA_KELUARGA C SET (NIK_KK,NAMA_KEP,NO_PROP, NO_KAB, NO_KEC, NO_KEL) = (SELECT NIK,NAMA_LGKP,NO_PROP, NO_KAB, NO_KEC, NO_KEL FROM(SELECT A.NIK, B.NIK_KK, A.NO_KK, A.NAMA_LGKP,A.NO_PROP, A.NO_KAB, A.NO_KEC, A.NO_KEL  FROM BIODATA_WNI A  INNER JOIN DATA_KELUARGA B ON A.NO_KK =B.NO_KK WHERE A.STAT_HBKEL =1 AND (A.NIK <> B.NIK_KK OR B.NIK_KK IS NULL)) B WHERE C.NO_KK = B.NO_KK) WHERE EXISTS (SELECT 1 FROM(SELECT A.NIK, B.NIK_KK, A.NO_KK  FROM BIODATA_WNI A  INNER JOIN DATA_KELUARGA B ON A.NO_KK =B.NO_KK WHERE A.STAT_HBKEL =1 AND (A.NIK <> B.NIK_KK OR B.NIK_KK IS NULL)) B WHERE C.NO_KK = B.NO_KK)"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("UPDATE BIODATA_WNI SET EKTP_CURRENT_STATUS_CODE = NAMA_KET_RT,NAMA_KET_RT = NULL WHERE NAMA_KET_RT IN (SELECT CODE FROM STATUS_ID)"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("UPDATE BIODATA_WNI SET NAMA_LGKP =  (SUBSTR(NAMA_LGKP, 2, LENGTH(NAMA_LGKP) - 1)) WHERE NAMA_LGKP LIKE ' %'"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("UPDATE BIODATA_WNI SET NAMA_LGKP =  (SUBSTR(NAMA_LGKP, 1, LENGTH(NAMA_LGKP) - 1)) WHERE NAMA_LGKP LIKE '% '"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("UPDATE BIODATA_WNI SET NAMA_LGKP =  REPLACE(NAMA_LGKP,'  ',' ') WHERE NAMA_LGKP LIKE '%  %'"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("UPDATE DATA_KELUARGA A SET CERT_STATUS = NULL WHERE NOT EXISTS (SELECT 1 FROM BSRE_KARTU_KELUARGA B WHERE A.NO_KK = B.NO_DOC) AND A.CERT_STATUS IS NOT NULL"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("UPDATE DATA_KELUARGA SET NO_RT = 1 WHERE NO_RT IS NULL"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("UPDATE DATA_KELUARGA SET NO_RW = 1 WHERE NO_RW IS NULL"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("UPDATE DATA_KELUARGA SET NO_RT = 1 WHERE NO_RT = 0"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("UPDATE DATA_KELUARGA SET NO_RW = 1 WHERE NO_RW = 0"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("UPDATE DATA_KELUARGA SET KODE_POS = 0 WHERE KODE_POS IS NULL"));

		        /*\DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("UPDATE DATA_KELUARGA X
						   SET (NO_KK) = (SELECT NO_KK_AFTER FROM (SELECT NO_KK, CONCAT(3273,CONCAT(LPAD(NO_KEC, 2, '0'),CONCAT(SUBSTR(NO_KK,7,6),CONCAT(9,SUBSTR(NO_KK,14,3))))) NO_KK_AFTER FROM DATA_KELUARGA WHERE NO_KK NOT LIKE '3273%') A  
						   WHERE NOT EXISTS (SELECT  1 FROM DATA_KELUARGA B WHERE A.NO_KK_AFTER = B.NO_KK) AND X.NO_KK = A.NO_KK
						   )
						 WHERE EXISTS (
						    SELECT 1 FROM (SELECT NO_KK, CONCAT(3273,CONCAT(LPAD(NO_KEC, 2, '0'),CONCAT(SUBSTR(NO_KK,7,6),CONCAT(9,SUBSTR(NO_KK,14,3))))) NO_KK_AFTER FROM DATA_KELUARGA WHERE NO_KK NOT LIKE '3273%') A  
						   WHERE NOT EXISTS (SELECT  1 FROM DATA_KELUARGA B WHERE A.NO_KK_AFTER = B.NO_KK) AND X.NO_KK = A.NO_KK)"));

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("UPDATE BIODATA_WNI X
						   SET (NO_KK) = (SELECT NO_KK_AFTER FROM (SELECT NO_KK, NO_KK_AFTER FROM (SELECT NO_KK, CONCAT(3273,CONCAT(LPAD(NO_KEC, 2, '0'),CONCAT(SUBSTR(NO_KK,7,6),CONCAT(9,SUBSTR(NO_KK,14,3))))) NO_KK_AFTER FROM BIODATA_WNI WHERE NO_KK NOT LIKE '3273%') A GROUP BY NO_KK, NO_KK_AFTER ) A  
						   WHERE X.NO_KK = A.NO_KK
						   )
						 WHERE EXISTS (
						    SELECT 1 FROM (SELECT NO_KK, NO_KK_AFTER FROM (SELECT NO_KK, CONCAT(3273,CONCAT(LPAD(NO_KEC, 2, '0'),CONCAT(SUBSTR(NO_KK,7,6),CONCAT(9,SUBSTR(NO_KK,14,3))))) NO_KK_AFTER FROM BIODATA_WNI WHERE NO_KK NOT LIKE '3273%') A GROUP BY NO_KK, NO_KK_AFTER ) A  
						   WHERE  X.NO_KK = A.NO_KK)"));*/

		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("ALTER TRIGGER ADT_BIO_WNI ENABLE"));
		        
		        \DB::connection('db222')->unprepared(\DB::connection('db222')
		        ->raw("ALTER TRIGGER TRG_KONS_BIO_WNI ENABLE"));
		        
		    });

		    \DB::connection('db222')->commit();
		} catch (\Exception $e){ 
		    \DB::connection('db222')->rollback();
		    //do something with $e->getMessage();
		}
     
    }
    
}

