<?php

namespace App\Http\Controllers;

use DB;
use Session;
use DateTime;
use PDF;
use Illuminate\Http\Request;
use App\Http\Controllers\SharedController as Shr;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class BlangkoController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function input_cetak_dinas(Request $request) 
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 101;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$data = array(
		 		"stitle"=>'Kendali Cetak Dinas',
		 		"mtitle"=>'Kendali Cetak Dinas',
		 		"my_url"=>'input_cetak',
		 		"type_tgl"=>'Request',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			return view('cetak_dinas/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}

    public function input_kia_dinas(Request $request) 
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 101;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$data = array(
		 		"stitle"=>'Pengambilan Kia',
		 		"mtitle"=>'Pengambilan Kia',
		 		"my_url"=>'input_pengambilan_kia',
		 		"type_tgl"=>'Request',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			return view('cetak_kia_dinas/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}

    public function input_ludo_dinas(Request $request) 
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 101;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$data = array(
		 		"stitle"=>'Kendali Cetak Dinas',
		 		"mtitle"=>'Kendali Cetak Dinas',
		 		"my_url"=>'input_cetak',
		 		"type_tgl"=>'Request',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			return view('cetak_ludo_dinas/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function cek_cetak_dinas(Request $request) 
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 102;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->nik != null OR $request->nama != null){
			$nik = $request->nik;
			if (substr($nik, 0, 1) === ','){
				$nik = ltrim($nik, ',');
			}
			$nama = $request->nama;
			$r = $this->blk_cek_rekap_cetak_dinas($nik,$nama);
			$data = array(
		 		"stitle"=>'Pencarian Cetak Dinas',
		 		"mtitle"=>'Pencarian Cetak Dinas',
		 		"my_url"=>'cek_cetak_dinas',
		 		"type_tgl"=>'Tanggal Cetak',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			}else{
			$data = array(
		 		"stitle"=>'Pencarian Cetak Dinas',
		 		"mtitle"=>'Pencarian Cetak Dinas',
		 		"my_url"=>'cek_cetak_dinas',
		 		"type_tgl"=>'Tanggal Cetak',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
    		}
			return view('cekcetakdinas/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function rekap_cetak_dinas(Request $request) 
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 103;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->tanggal != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$r = $this->blk_rekap_cetak_dinas($tgl_start,$tgl_end);
			$data = array(
		 		"stitle"=>'Rekap Cetak Dinas',
		 		"mtitle"=>'Rekap Cetak Dinas',
		 		"my_url"=>'rekap_cetak_dinas',
		 		"type_tgl"=>'Tanggal Cetak',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			}else{
			$data = array(
		 		"stitle"=>'Rekap Cetak Dinas',
		 		"mtitle"=>'Rekap Cetak Dinas',
		 		"my_url"=>'rekap_cetak_dinas',
		 		"type_tgl"=>'Tanggal Cetak',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
    		}
			return view('rekapcetakdinas/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function blangko_out(Request $request) 
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 98;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$data = array(
		 		"stitle"=>'Kendali Blangko KTP Keluar',
		 		"mtitle"=>'Kendali Blangko KTP Keluar',
		 		"my_url"=>'blangko_out',
		 		"type_tgl"=>'Request',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			return view('Blangko_out/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function blangko_in(Request $request) 
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 97;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$data = array(
		 		"stitle"=>'Kendali Blangko KTP Masuk',
		 		"mtitle"=>'Kendali Blangko KTP Masuk',
		 		"my_url"=>'blangko_in',
		 		"type_tgl"=>'Request',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			return view('Blangko_in/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function blangko_pemusnahan(Request $request) 
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 295;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$data = array(
		 		"stitle"=>'Pemusnahan Blangko',
		 		"mtitle"=>'Pemusnahan Blangko',
		 		"my_url"=>'blangko_pemusnahan',
		 		"type_tgl"=>'Request',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			return view('Blangko_pemusnahan/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function blangko_gudang(Request $request) 
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 274;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$data = array(
		 		"stitle"=>'Kendali Blangko Gudang',
		 		"mtitle"=>'Kendali Blangko Gudang',
		 		"my_url"=>'blangko_in',
		 		"type_tgl"=>'Request',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			return view('Blangko_gudang/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function blangko_gudang_out(Request $request) 
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 275;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$data = array(
		 		"stitle"=>'Distribusi Blangko Gudang',
		 		"mtitle"=>'Distribusi Blangko Gudang',
		 		"my_url"=>'blangko_in',
		 		"type_tgl"=>'Request',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			return view('blangko_gudang_distribusi/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function blangko_rekap(Request $request) 
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 99;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->tanggal != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$r = $this->blk_cek_rekap_blangko($tgl_start,$tgl_end);
			$data = array(
		 		"stitle"=>'Rekap Blangko KTP Keluar',
		 		"mtitle"=>'Rekap Blangko KTP Keluar',
		 		"my_url"=>'blangko_rekap',
		 		"type_tgl"=>'Request',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			}else{
			$data = array(
		 		"stitle"=>'Rekap Blangko KTP Keluar',
		 		"mtitle"=>'Rekap Blangko KTP Keluar',
		 		"my_url"=>'blangko_rekap',
		 		"type_tgl"=>'Request',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
    		}
			return view('Blangko_rekap/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function blangko_capil_rekap(Request $request) 
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 259;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->tanggal != null){
			$tgl = $request->tanggal;
			$jenis = $request->no_jenis;
			$jenis_lap = $request->no_lap;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$r = $this->blk_cek_rekap_blangko_capil($tgl_start,$tgl_end,$jenis,$jenis_lap);
			$data = array(
		 		"stitle"=>'Rekap Blangko Capil',
		 		"mtitle"=>'Rekap Blangko Capil',
		 		"my_url"=>'blangko_rekap',
		 		"type_tgl"=>'Request',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			}else{
			$data = array(
		 		"stitle"=>'Rekap Blangko Capil',
		 		"mtitle"=>'Rekap Blangko Capil',
		 		"my_url"=>'blangko_rekap',
		 		"type_tgl"=>'Request',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
    		}
			return view('Blangko_capil_rekap/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function blangko_capil_in(Request $request) 
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 258;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$data = array(
		 		"stitle"=>'Kendali Blangko Capil',
		 		"mtitle"=>'Kendali Blangko Capil',
		 		"my_url"=>'blangko_in',
		 		"type_tgl"=>'Request',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			return view('Blangko_capil_in/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}

	public function blangko_capil_out(Request $request) 
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 259;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$data = array(
		 		"stitle"=>'Kendali Blangko Capil Keluar',
		 		"mtitle"=>'Kendali Blangko Capil Keluar',
		 		"my_url"=>'blangko_capil_out',
		 		"type_tgl"=>'Request',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			return view('Blangko_capil_out/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function blangko_kk_out(Request $request) 
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 0;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$data = array(
		 		"stitle"=>'Kendali Blangko KK Keluar',
		 		"mtitle"=>'Kendali Blangko KK Keluar',
		 		"my_url"=>'blangko_out',
		 		"type_tgl"=>'Request',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			return view('Blangko_kk_out/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function blangko_kk_in(Request $request) 
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 0;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$data = array(
		 		"stitle"=>'Kendali Blangko KK Masuk',
		 		"mtitle"=>'Kendali Blangko KK Masuk',
		 		"my_url"=>'blangko_in',
		 		"type_tgl"=>'Request',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			return view('Blangko_kk_in/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function get_hist_distribusi(Request $request)
    {

    	if($request->no_kec != null){
			$no_kec = $request->no_kec;
	        $data = $this->blk_hist_distribusi_keping($no_kec);
	        return $data;
		}else{
			return redirect()->route('logout');
		}
        
    }
	public function get_hist_distribusikec(Request $request)
    {
			$no_kec = $request->no_kec;
	        $data = $this->blk_hist_distribusikec_keping($no_kec);
	        return $data;
    }
	public function get_nik(Request $request) 
	{
		if($request->nik != null){
			$nik = $request->nik;
			$j = $this->blk_get_nik($nik);
			if($j > 0){
				$r = $this->blk_get_data_nik($nik);
				$data["success"] = TRUE;
				$data["is_exists"] = 1;
        		$data["nama"] = $r[0]->nama_lgkp;
        		$data["no_kec"] = $r[0]->no_kec;
        		$data["nama_kec"] = $r[0]->nama_kec;
        		return $data;
			}else{
				$data["success"] = TRUE;
				$data["is_exists"] = 0;
        		$data["nama"] = '';
        		$data["no_kec"] = 0;
        		$data["nama_kec"] = 'LUAR DOMISILI';
        		return $data;
			}
		}else{
			return redirect()->route('logout');
		}
	}
	public function get_nik_kia(Request $request) 
	{
		if($request->nik != null){
			$nik = $request->nik;
			$r = $this->blk_get_nik_kia($nik);
			if(count($r) > 0){
				$data["success"] = TRUE;
				$data["is_exists"] = 1;
        		$data["nik"] = $r[0]->nik;
        		$data["nama"] = $r[0]->nama_lgkp;
        		$data["jenis_klmin"] = $r[0]->jenis_klmin;
        		$data["ttl"] = $r[0]->tmpt_lhr.' / '.$r[0]->tgl_lhr;
        		$data["gol_drh"] = $r[0]->gol_drh;
        		$data["umur"] = $r[0]->umur.' Tahun';
                $data['agama'] = $r[0]->agama;   
                $data['nik_ibu'] = $r[0]->nik_ibu;   
                $data['nama_ibu'] = $r[0]->nama_lgkp_ibu;   
                $data['nik_ayah'] = $r[0]->nik_ayah;   
                $data['nama_ayah'] = $r[0]->nama_lgkp_ayah;   
                $data['no_kk'] = $r[0]->no_kk;   
                $data['provinsi'] = $r[0]->provinsi;   
                $data['kabupaten'] = $r[0]->kabupaten;   
                $data['kecamatan'] = $r[0]->kecamatan;   
                $data['kelurahan'] = $r[0]->kelurahan;   
                $data['alamat'] = $r[0]->alamat;   
                $data['rtrw'] =$r[0]->rt.' / '.$r[0]->rw;;   
                $data['kodepos'] = $r[0]->kode_pos;   
                $data['no_akta_lhr'] = $r[0]->no_akta_lhr;   
                $data['anak_ke'] = $r[0]->anak_ke; 
                $data['tgl_pengajuan'] = $r[0]->created_date; 
                $data['petugas'] = $r[0]->created_by; 
                $data['path'] = ($r[0]->path != '') ? 'http://10.32.73.222:8080/Siak/'.$r[0]->path.'/'.$r[0]->nik.'.jpg' : ''; 

        		$data["no_kec"] = $r[0]->no_kec;
        		$data["nama_kec"] = $r[0]->nama_kec;
        		$data["tgl_cetak"] = $r[0]->tgl_cetak;
        		return $data;
			}else{
				$data["is_exists"] = 0;
        		return $data;
			}
		}else{
			return redirect()->route('logout');
		}
	}
	public function get_idpengajuan_kia(Request $request) 
	{
		if($request->idpengajuan != null){
			$idpengajuan = $request->idpengajuan;
			$r = $this->blk_get_idpengajuan_kia($idpengajuan);
			if(count($r) > 0){
				$data["success"] = TRUE;
				$data["is_exists"] = 1;
        		$data["nik"] = $r[0]->nik;
        		$data["idpengajuan"] = $r[0]->idpengajuan;
        		$data["nama"] = $r[0]->nama_lgkp;
        		$data["jenis_klmin"] = $r[0]->jenis_klmin;
        		$data["ttl"] = $r[0]->tmpt_lhr.' / '.$r[0]->tgl_lhr;
        		$data["gol_drh"] = $r[0]->gol_drh;
        		$data["umur"] = $r[0]->umur.' Tahun';
                $data['agama'] = $r[0]->agama;   
                $data['nik_ibu'] = $r[0]->nik_ibu;   
                $data['nama_ibu'] = $r[0]->nama_lgkp_ibu;   
                $data['nik_ayah'] = $r[0]->nik_ayah;   
                $data['nama_ayah'] = $r[0]->nama_lgkp_ayah;   
                $data['no_kk'] = $r[0]->no_kk;   
                $data['provinsi'] = $r[0]->provinsi;   
                $data['kabupaten'] = $r[0]->kabupaten;   
                $data['kecamatan'] = $r[0]->kecamatan;   
                $data['kelurahan'] = $r[0]->kelurahan;   
                $data['alamat'] = $r[0]->alamat;   
                $data['rtrw'] =$r[0]->rt.' / '.$r[0]->rw;;   
                $data['kodepos'] = $r[0]->kode_pos;   
                $data['no_akta_lhr'] = $r[0]->no_akta_lhr;   
                $data['anak_ke'] = $r[0]->anak_ke; 
                $data['tgl_pengajuan'] = $r[0]->created_date; 
                $data['petugas'] = $r[0]->created_by; 
                $data['path'] = ($r[0]->path != '') ? 'http://10.32.73.222:8080/Siak/'.$r[0]->path.'/'.$r[0]->nik.'.jpg' : ''; 

        		$data["no_kec"] = $r[0]->no_kec;
        		$data["nama_kec"] = $r[0]->nama_kec;
        		$data["tgl_cetak"] = $r[0]->tgl_cetak;
        		return $data;
			}else{
				$data["is_exists"] = 0;
        		return $data;
			}
		}else{
			return redirect()->route('logout');
		}
	}
	public function get_seqn_print_kia(Request $request) 
	{
		if($request->nik != null){
			$nik = $request->nik;
			$r = $this->blk_hist_cetak_kia($nik);

			$file_pdf = "<embed src='".$r[0]->file_pdf."' type='application/pdf'   height='500px' width='100%' id='kia_file' >";
			if(count($r) > 0){
				$html ="";
				$i=0;
				foreach ($r as $r)
        		{
        			$i++;
        			$html .="<tr>
	                            <td class='text-center'><h4>$i</h4></td>
	                            <td class='text-center'> <h4>$r->nik</h4> </td>
	                            <td class='text-center'> <h4>$r->nama_lgkp</h4> </td>
	                            <td class='text-center'> <h4>$r->tgl_cetak</h4> </td>
	                            <td class='text-center'> <h4>$r->printed_by</h4> </td>
	                            ";
	                if ($r->file_pdf != '-'){
	                	$html .="<td class='text-center'> <a target='_blank' href='$r->file_pdf' class='btn btn-info waves-effect waves-light' type='button'><span class='btn-label'><i class='fa fa-download'></i></span>Download</a></td>";
	                }else{
	                	$html .="<td class='text-center'> <button class='btn btn-danger waves-effect waves-light' type='button'><span class='btn-label'><i class='fa fa-times'></i></span>File Tidak Ditemukan</button></td>";
	                }
	              
	               $html .="</tr>";
        		}
				$data["is_exists"] = 1;
				$data["table_cetak"] = $html;
				$data["file_pdf"] = $file_pdf;
        		return $data;
			}else{
				$data["is_exists"] = 0;
        		return $data;
			}
		}else{
			return redirect()->route('logout');
		}
	}
	public function get_seqn_print_kia_id(Request $request) 
	{
		if($request->idpengajuan != null){
			$idpengajuan = $request->idpengajuan;
			$r = $this->blk_hist_cetak_kia_id($idpengajuan);

			$file_pdf = "<embed src='".$r[0]->file_pdf."' type='application/pdf'   height='500px' width='100%' id='kia_file' >";
			if(count($r) > 0){
				$html ="";
				$i=0;
				foreach ($r as $r)
        		{
        			$i++;
        			$html .="<tr>
	                            <td class='text-center'><h4>$i</h4></td>
	                            <td class='text-center'> <h4>$r->nik</h4> </td>
	                            <td class='text-center'> <h4>$r->nama_lgkp</h4> </td>
	                            <td class='text-center'> <h4>$r->tgl_cetak</h4> </td>
	                            <td class='text-center'> <h4>$r->printed_by</h4> </td>
	                            ";
	                if ($r->file_pdf != '-'){
	                	$html .="<td class='text-center'> <a target='_blank' href='$r->file_pdf' class='btn btn-info waves-effect waves-light' type='button'><span class='btn-label'><i class='fa fa-download'></i></span>Download</a></td>";
	                }else{
	                	$html .="<td class='text-center'> <button class='btn btn-danger waves-effect waves-light' type='button'><span class='btn-label'><i class='fa fa-times'></i></span>File Tidak Ditemukan</button></td>";
	                }
	              
	               $html .="</tr>";
        		}
				$data["is_exists"] = 1;
				$data["table_cetak"] = $html;
				$data["file_pdf"] = $file_pdf;
        		return $data;
			}else{
				$data["is_exists"] = 0;
        		return $data;
			}
		}else{
			return redirect()->route('logout');
		}
	}
	public function get_delivery_kia(Request $request) 
	{
		if($request->nik != null){
			$nik = $request->nik;
			$r = $this->blk_hist_delivery_kia($nik);
			if(count($r) > 0){
				$html ="";
				$i=0;
				foreach ($r as $r)
        		{
        			$i++;
        			$html .="<tr>
	                            <td class='text-center'><h4>$i</h4></td>
	                            <td class='text-center'> <h4>$r->nik</h4> </td>
	                            <td class='text-center'> <h4>$r->nama_lgkp</h4> </td>
	                            <td class='text-center'> <h4>$r->tgl_pengambilan</h4> </td>
	                            <td class='text-center'> <h4>$r->location</h4> </td>
	                            <td class='text-center'> <h4>$r->jenis_pengambilan</h4> </td>
	                            <td class='text-center'> <h4>$r->req_by</h4> </td>
	                            <td class='text-center'> <h4>$r->receive_by</h4> </td>
	                            <td class='text-center'> <h4>$r->created_by</h4> </td>
	                            </tr>";

        		}
				$data["is_exists"] = 1;
				$data["table_delivery"] = $html;
        		return $data;
			}else{
				$data["is_exists"] = 0;
        		return $data;
			}
		}else{
			return redirect()->route('logout');
		}
	}
	public function get_delivery_kia_id(Request $request) 
	{
		if($request->idpengajuan != null){
			$idpengajuan = $request->idpengajuan;
			$r = $this->blk_hist_delivery_kia_id($idpengajuan);
			if(count($r) > 0){
				$html ="";
				$i=0;
				foreach ($r as $r)
        		{
        			$i++;
        			$html .="<tr>
	                            <td class='text-center'><h4>$i</h4></td>
	                            <td class='text-center'> <h4>$r->nik</h4> </td>
	                            <td class='text-center'> <h4>$r->nama_lgkp</h4> </td>
	                            <td class='text-center'> <h4>$r->tgl_pengambilan</h4> </td>
	                            <td class='text-center'> <h4>$r->location</h4> </td>
	                            <td class='text-center'> <h4>$r->jenis_pengambilan</h4> </td>
	                            <td class='text-center'> <h4>$r->req_by</h4> </td>
	                            <td class='text-center'> <h4>$r->receive_by</h4> </td>
	                            <td class='text-center'> <h4>$r->created_by</h4> </td>
	                            </tr>";

        		}
				$data["is_exists"] = 1;
				$data["table_delivery"] = $html;
        		return $data;
			}else{
				$data["is_exists"] = 0;
        		return $data;
			}
		}else{
			return redirect()->route('logout');
		}
	}
	public function do_save_cetak(Request $request) 
	{
		if($request->nik != null){
			$nik = $request->nik;
			$nama = str_replace('\'', '',$request->nama);
			$no_kec = $request->no_kec;
			$nama_kec = $request->nama_kec;
			$request_by = $request->request_by;
			$receive = $request->receive;
			$tanggal = $request->tanggal;
			$this->blk_save_cetak($nik,$nama,$no_kec,$nama_kec,$request_by,$receive,$tanggal,$request->session()->get('S_USER_ID'));
			$this->blk_save_cetakdns($nik,$request_by);
			$this->blk_save_cetakdnsld($nik,$request_by);

			$data["success"] = TRUE;
			$data["message"] = "Data Berhasil Di Simpan";
       		return $data;
		}else{
			return redirect()->route('logout');
		}
	}
	public function do_save_kia(Request $request) 
	{
		if($request->nik != null){
			$nik = $request->nik;
			$nama = str_replace('\'', '',$request->nama);
			$no_kec = $request->no_kec;
			$nama_kec = $request->nama_kec;
			$location_id = $request->location_id;
			$pengambilan_id = $request->pengambilan_id;
			$request_by = $request->request_by;
			$receive = $request->receive;
			$tanggal = $request->tanggal;
			$tanggal_cetak = $request->tanggal_cetak;
			$this->blk_save_kia($nik,$nama,$no_kec,$nama_kec,$location_id,$pengambilan_id,$request_by,$receive,$tanggal,$tanggal_cetak,$request->session()->get('S_USER_ID'));

			$data["success"] = TRUE;
			$data["message"] = "Data Berhasil Di Simpan";
       		return $data;
		}else{
			return redirect()->route('logout');
		}
	}
	public function do_save_ludo(Request $request) 
	{

		if($request->jml != null){
			$jml = $request->jml;
			$j = $this->blk_get_ludo();
			if($j > 0){
				$this->blk_update_ludo($jml);
			}else{
				$this->blk_save_ludo($jml);
			}

			$data["success"] = TRUE;
			$data["message"] = "Data Berhasil Di Simpan";
       		return $data;
		}else{
			return redirect()->route('logout');
		}
	}
	
	public function get_data(Request $request) 
	{

		if($request->no_kec != null){
			$no_kec = $request->no_kec;
			$tgl = $request->tanggal;
			$j = $this->blk_get_count_blangko($no_kec,$tgl);
			if($j > 0){
				$r = $this->blk_get_blangko($no_kec,$tgl);
				$data["pengambilan"] = $r[0]->ambil;
				$data["rusak"] = $r[0]->rusak;
        		$data["pengembalian"] = $r[0]->pengembalian;
        		$data["jumlah"] = $j;
        		return $data;
			}else{
				$data["pengambilan"] = 0;
				$data["rusak"] = 0;
        		$data["pengembalian"] = 0;
        		$data["jumlah"] = $j;
        		return $data;
			}
		}else{
			return redirect()->route('logout');
		}
	}

	public function do_save_capil_out(Request $request) 
	{
		if($request->tanggal != null){
			$jenis = ($request->jenis_blangko != null) ? $request->jenis_blangko : 0;
			$tgl = $request->tanggal;
			$rusak = (int)$request->rusak;
			$pengambilan = (int)$request->pengambilan;
			$ket = $request->ket;
			$j = $this->blk_get_count_blangko_capil($jenis,$tgl);
			if($j > 0){
				$this->blk_update_blangko_capil($jenis,$tgl,$pengambilan,$rusak,$ket,$request->session()->get('S_USER_ID'));
				$data["success"] = TRUE;
				$data["is_save"] = 0;
        		$data["message"] = "Data Berhasil Di Update";
        		return $data;
			}else{
				$this->blk_save_blangko_capil($jenis,$tgl,$pengambilan,$rusak,$ket,$request->session()->get('S_USER_ID'));
				$data["success"] = TRUE;
				$data["is_save"] = 1;
        		$data["message"] = "Data Berhasil Di Simpan";
        		return $data;
			}
		}else{
			return redirect()->route('logout');
		}
	}
	public function get_capil_data(Request $request) 
	{

		if($request->tanggal != null){
			$jenis = ($request->jenis_blangko != null) ? $request->jenis_blangko : 0;
			$tgl = $request->tanggal;
			$j = $this->blk_get_count_blangko_capil($jenis,$tgl);
			if($j > 0){
				$r = $this->blk_get_blangko_capil($jenis,$tgl);
				$data["pengambilan"] = $r[0]->jumlah;
				$data["rusak"] = $r[0]->rusak;
        		$data["ket"] = $r[0]->ket;
        		$data["jumlah"] = $j;
        		return $data;
			}else{
				$data["pengambilan"] = 0;
				$data["rusak"] = 0;
        		$data["ket"] = "";
        		$data["jumlah"] = $j;
        		return $data;
			}
		}else{
			return redirect()->route('logout');
		}
	}
	public function get_all_data(Request $request) 
	{
		if($request->tanggal != null){
			$tgl = $request->tanggal;
			$j = $this->blk_get_count_all_blangko($tgl);
			if($j > 0){
				$r = $this->blk_get_all_blangko($tgl);
				$data["pengambilan"] = $r[0]->ambil;
				$data["rusak"] = $r[0]->rusak;
        		$data["pengembalian"] = $r[0]->pengembalian;
        		$data["total"] = $r[0]->total;
        		$data["jumlah"] = $j;
        		return $data;
			}else{
				$data["pengambilan"] = 0;
				$data["rusak"] = 0;
        		$data["pengembalian"] = 0;
        		$data["total"] = 0;
        		$data["jumlah"] = $j;
        		return $data;
			}
		}else{
			return redirect()->route('logout');
		}
	}
	public function get_allin_data(Request $request) 
	{
		if($request->tanggal != null){
				$tgl = $request->tanggal;
				$r = $this->blk_get_allin_blangko();
				$data["masuk"] = $r[0]->masuk;
				$data["keluar"] = $r[0]->keluar;
        		$data["rusak"] = $r[0]->rusak;
        		$data["sisa"] = $r[0]->sisa;
        		return $data;
		}else{
			return redirect()->route('logout');
		}
	}

	public function get_gudang_data(Request $request) 
	{
		if($request->tanggal != null){
				$tgl = $request->tanggal;
				$r = $this->blk_get_gudang_blangko();
				$data["blk_dinas"] = $r[0]->blk_dinas;
				$data["blk_gudang"] = $r[0]->blk_gudang;
        		$data["blk_dafduk"] = $r[0]->blk_dafduk;
        		return $data;
		}else{
			return redirect()->route('logout');
		}
	}

	public function get_dafduk_data(Request $request) 
	{
		if($request->tanggal != null){
				$tgl = $request->tanggal;
				$r = $this->blk_get_last_distribusi();
				$data["data_dafduk"] = $r[0]->data_dafduk;
        		return $data;
		}else{
			return redirect()->route('logout');
		}
	}

	public function get_pemusnahan_data(Request $request) 
	{
		if($request->tanggal != null){
				$tgl = $request->tanggal;
				$r = $this->blk_get_last_pemusnahan();
				$data["data_pemusnahan"] = $r[0]->data_pemusnahan;
        		return $data;
		}else{
			return redirect()->route('logout');
		}
	}
	public function get_allin_capil_data(Request $request) 
	{
		if($request->jenis_blangko != null){
				$jenis = $request->jenis_blangko;
				$r = $this->blk_get_allin_blangko_capil($jenis);
				$data["jenis_blangko"] = $r[0]->jenis_blangko;
				$data["masuk"] = $r[0]->masuk;
				$data["keluar"] = $r[0]->keluar;
        		$data["rusak"] = $r[0]->rusak;
        		$data["sisa"] = $r[0]->sisa;
        		return $data;
		}else{
			return redirect()->route('logout');
		}
	}
	public function get_allin_capil_data_out(Request $request) 
	{
		if($request->tanggal != null){
				$jenis = ($request->jenis_blangko != null) ? $request->jenis_blangko : 0;
				$r = $this->blk_get_allin_blangko_capil($jenis);
				$data["jenis_blangko"] = $r[0]->jenis_blangko;
				$data["masuk"] = $r[0]->masuk;
				$data["keluar"] = $r[0]->keluar;
        		$data["rusak"] = $r[0]->rusak;
        		$data["sisa"] = $r[0]->sisa;
        		return $data;
		}else{
			return redirect()->route('logout');
		}
	}
	public function do_save(Request $request) 
	{
		if($request->no_kec != null){
			$no_kec = (int)$request->no_kec;
			$tgl = $request->tanggal;
			$pengambilan = (int)$request->pengambilan;
			$rusak = (int)$request->rusak;
			$pengembalian = (int)$request->pengembalian;
			$j = $this->blk_get_count_blangko($no_kec,$tgl);
			if($j > 0){
				$this->blk_update_blangko($no_kec,$tgl,$pengambilan,$rusak,$pengembalian);
				$data["success"] = TRUE;
				$data["is_save"] = 0;
        		$data["message"] = "Data Berhasil Di Update";
        		return $data;
			}else{
				$this->blk_save_blangko($no_kec,$tgl,$pengambilan,$rusak,$pengembalian);
				$data["success"] = TRUE;
				$data["is_save"] = 1;
        		$data["message"] = "Data Berhasil Di Simpan";
        		return $data;
			}
		}else{
			return redirect()->route('logout');
		}
	}

	public function do_save_dis(Request $request) 
	{
		if($request->no_kec != null){
			$no_kec = (int)$request->no_kec;
			$tgl = $request->tanggal;
			$penerima = strtoupper($request->penerima);
			$j = $this->blk_get_count_dist_belangko($no_kec,$tgl);
			if($j > 0){
				$data["success"] = FALSE;
				$data["is_save"] = 0;
        		$data["message"] = "Pengambilan Sudah Di Lakukan Hari Ini !";
        		return $data;
			}else{
				$this->blk_save_dist_belangko($no_kec,$tgl,$penerima,$request->ip(),$request->session()->get('S_USER_ID'));
				$this->blk_update_dist_belangko($no_kec,$penerima);
				$this->blk_update_distld_belangko($no_kec,$penerima);
				$data["success"] = TRUE;
				$data["is_save"] = 1;
        		$data["message"] = "Data Berhasil Di Simpan";
        		return $data;
			}
		}else{
			return redirect()->route('logout');
		}
	}
	public function doin_save(Request $request) 
	{
		if($request->tanggal != null){
			$blangko_masuk = (int)$request->blangko_masuk;
			$tgl = $request->tanggal;
			$keterangan = $request->keterangan;
			$user_name = $request->user_name;
			$j = $this->blk_get_count_inblangko($tgl);
			if($j > 0){
				$this->blk_update_inblangko($blangko_masuk,$tgl,$keterangan,$user_name);
				$data["success"] = TRUE;
				$data["is_save"] = 0;
        		$data["message"] = "Data Berhasil Di Update";
        		return $data;
			}else{
				$this->blk_save_inblangko($blangko_masuk,$tgl,$keterangan,$user_name);
				$data["success"] = TRUE;
				$data["is_save"] = 1;
        		$data["message"] = "Data Berhasil Di Simpan";
        		return $data;
			}
		}else{
			return redirect()->route('logout');
		}
	}
	public function doin_distribusi_gudang(Request $request) 
	{
		if($request->tanggal != null){
			$distribusi = (int)$request->distribusi;
			$sisa_gudang = (int)$request->sisa_gudang;
			$tgl = $request->tanggal;
			$keterangan = $request->keterangan;
			$user_name = $request->user_name;
			$j = $this->blk_get_count_distribusi_gudang($tgl);
			if($j > 0){
				$this->blk_update_distribusi_gudang($distribusi,$tgl,$keterangan,$user_name);
				$this->blk_update_inblangko_gudang($sisa_gudang);
				$data["success"] = TRUE;
				$data["is_save"] = 0;
        		$data["message"] = "Data Berhasil Di Update";
        		return $data;
			}else{
				$this->blk_save_distribusi_gudang($distribusi,$tgl,$keterangan,$user_name);
				$this->blk_update_inblangko_gudang($sisa_gudang);
				$data["success"] = TRUE;
				$data["is_save"] = 1;
        		$data["message"] = "Data Berhasil Di Simpan";
        		return $data;
			}
		}else{
			return redirect()->route('logout');
		}
	}
	public function doin_distribusi_pemusnahan(Request $request) 
	{
		if($request->tanggal != null){
			$distribusi = (int)$request->distribusi;
			$tgl = $request->tanggal;
			$keterangan = $request->keterangan;
			$user_name = $request->user_name;
			$j = $this->blk_get_count_pemusnahan($tgl);
			if($j > 0){
				$this->blk_update_pemusnahan($distribusi,$tgl,$keterangan,$user_name);
				$data["success"] = TRUE;
				$data["is_save"] = 0;
        		$data["message"] = "Data Berhasil Di Update";
        		return $data;
			}else{
				$this->blk_save_pemusnahan($distribusi,$tgl,$keterangan,$user_name);
				$data["success"] = TRUE;
				$data["is_save"] = 1;
        		$data["message"] = "Data Berhasil Di Simpan";
        		return $data;
			}
		}else{
			return redirect()->route('logout');
		}
	}
	public function blangko_gudang_rekap(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 276;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->tanggal != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$r = $this->blk_rekap_distribusi($tgl_start,$tgl_end);
			$data = array(
		 		"stitle"=>'Laporan Distribusi Blangko Ektp',
		 		"mtitle"=>'Laporan Distribusi Blangko Ektp',
		 		"my_url"=>'blangko_gudang_rekap',
		 		"type_tgl"=>'Tanggal',
		 		"data"=>$r,
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			}else{
			$data = array(
		 		"stitle"=>'Laporan Distribusi Blangko Ektp',
		 		"mtitle"=>'Laporan Distribusi Blangko Ektp',
		 		"my_url"=>'blangko_gudang_rekap',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
    		}
			return view('Blangko_gudang_rekap/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function doin_save_gudang(Request $request) 
	{
		if($request->blangko_gudang != null){
			$blangko_gudang = (int)$request->blangko_gudang;
				$this->blk_update_inblangko_gudang($blangko_gudang);
				$data["success"] = TRUE;
				$data["is_save"] = 0;
        		$data["message"] = "Data Berhasil Di Update";
        		return $data;
		}else{
			return redirect()->route('logout');
		}
	}
	public function doin_save_capil(Request $request) 
	{
		if($request->tanggal != null){
			$blangko_masuk = (int)$request->blangko_masuk;
			$tgl = $request->tanggal;
			$jenis_blangko = $request->jenis_blangko;
			$keterangan = $request->keterangan;
			$user_name = $request->user_name;
			$j = $this->blk_get_count_inblangko_capil($tgl,$jenis_blangko);
			if($j > 0){
				$this->blk_update_inblangko_capil($blangko_masuk,$tgl,$keterangan,$user_name,$jenis_blangko);
				$data["success"] = TRUE;
				$data["is_save"] = 0;
        		$data["message"] = "Data Berhasil Di Update";
        		return $data;
			}else{
				$this->blk_save_inblangko_capil($blangko_masuk,$tgl,$keterangan,$user_name,$jenis_blangko);
				$data["success"] = TRUE;
				$data["is_save"] = 1;
        		$data["message"] = "Data Berhasil Di Simpan";
        		return $data;
			}
		}else{
			return redirect()->route('logout');
		}
	}

	public function blk_get_count_blangko($no_kec,$tgl){
			$sql = "SELECT COUNT(1) AS JML FROM SIAK_MONEV_BLANGKO WHERE NO_KEC = $no_kec AND TO_CHAR(TANGGAL,'DD-MM-YYYY') = '$tgl'";
			$r = DB::select($sql);
			return $r[0]->jml;
		}

	public function blk_get_count_dist_belangko($no_kec,$tgl){
			$sql = "SELECT COUNT(1) AS JML FROM SIAK_DISTRIBUSI_KEPING WHERE NO_KEC = $no_kec AND TO_CHAR(UPDATE_DT,'DD-MM-YYYY') = '$tgl'";
			$r = DB::select($sql);
			return $r[0]->jml;
		}

	public function blk_get_count_blangko_capil($jns = 0,$tgl){
			$sql = "SELECT COUNT(1) AS JML FROM SIAK_BLANGKO_CAPIL_OUT WHERE JENIS = $jns AND TO_CHAR(CREATED_DT,'DD-MM-YYYY') = '$tgl'";
			$r = DB::select($sql);

			return $r[0]->jml;
		}
		public function blk_get_count_inblangko($tgl){
			$sql = "SELECT COUNT(1) AS JML FROM SIAK_BLANGKO WHERE TO_CHAR(TANGGAL_MASUK,'DD-MM-YYYY') = '$tgl'";
			$r = DB::select($sql);
			return $r[0]->jml;
		}
		public function blk_get_count_inblangko_capil($tgl,$jns){
			$sql = "SELECT COUNT(1) AS JML FROM SIAK_BLANGKO_CAPIL_IN WHERE TO_CHAR(CREATED_DT,'DD-MM-YYYY') = '$tgl' AND JENIS = $jns";
			$r = DB::select($sql);
			return $r[0]->jml;
		}
		public function blk_get_count_all_blangko($tgl){
			$sql = "SELECT COUNT(1) AS JML FROM SIAK_MONEV_BLANGKO WHERE TO_CHAR(TANGGAL,'DD-MM-YYYY') = '$tgl'";
			$r = DB::select($sql);
			return $r[0]->jml;
		}
		public function blk_get_blangko($no_kec,$tgl){
			$sql = "SELECT AMBIL,RUSAK,PENGEMBALIAN FROM SIAK_MONEV_BLANGKO WHERE NO_KEC = $no_kec AND TO_CHAR(TANGGAL,'DD-MM-YYYY') = '$tgl'";
			$r = DB::select($sql);
			return $r;
		}
		public function blk_get_blangko_capil($jns,$tgl){
			$sql = "SELECT JUMLAH,RUSAK,KET FROM SIAK_BLANGKO_CAPIL_OUT WHERE JENIS = $jns AND TO_CHAR(CREATED_DT,'DD-MM-YYYY') = '$tgl'";
			$r = DB::select($sql);
			return $r;
		}
		public function blk_get_all_blangko($tgl){
			$sql = "SELECT SUM(AMBIL) AMBIL, SUM(RUSAK) RUSAK, SUM(PENGEMBALIAN) PENGEMBALIAN, SUM(AMBIL) - (SUM(RUSAK) + SUM(PENGEMBALIAN)) TOTAL FROM SIAK_MONEV_BLANGKO WHERE TO_CHAR(TANGGAL,'DD-MM-YYYY') = '$tgl'";
			$r = DB::select($sql);
			return $r;
		}
		public function blk_get_allin_blangko(){
			$sql = "SELECT 
					(SELECT NVL(SUM(JUMLAH),0) FROM SIAK_BLANGKO WHERE TRUNC(TANGGAL_MASUK,'YYYY') = TRUNC(SYSDATE,'YYYY')) AS MASUK,
					(SELECT NVL(SUM(AMBIL) - (SUM(RUSAK) +SUM(PENGEMBALIAN)),0) AS JUMLAH FROM SIAK_MONEV_BLANGKO WHERE TRUNC(TANGGAL,'YYYY') = TRUNC(SYSDATE,'YYYY')) AS KELUAR,
					(SELECT NVL(SUM(RUSAK),0) AS JUMLAH FROM SIAK_MONEV_BLANGKO WHERE TRUNC(TANGGAL,'YYYY') = TRUNC(SYSDATE,'YYYY')) AS RUSAK,
					(SELECT NVL(SUM(JUMLAH),0) FROM SIAK_BLANGKO ) - (SELECT SUM(AMBIL) - SUM(PENGEMBALIAN) AS JUMLAH FROM SIAK_MONEV_BLANGKO) AS SISA
					FROM DUAL";
			$r = DB::select($sql);
			return $r;
		}

		public function blk_get_gudang_blangko(){
			$sql = "SELECT BLK_DINAS, BLK_GUDANG, (BLK_DINAS- BLK_GUDANG) BLK_DAFDUK FROM (SELECT VAL BLK_DINAS, ( SELECT SYSTEM_VALUE_NUM FROM SIAK_MASTER WHERE SYSTEM_TYPE = 'SET_DAFDUK' AND SYSTEM_CODE = 'KTP_GUDANG' ) BLK_GUDANG FROM SIAK_DASHBOARD WHERE TRUNC(CREATED_DT) = TRUNC(SYSDATE) AND ID = 'GET_BLANGKO_OUT') ";
			$r = DB::select($sql);
			return $r;
		}
		public function blk_get_allin_blangko_capil($jns = 0){
			$sql = "SELECT 
					COALESCE((SELECT JENIS_BLANGKO FROM SIAK_BLANGKO_CAPIL_OPTION WHERE ID = $jns),'-') AS JENIS_BLANGKO,
					(SELECT NVL(SUM(JUMLAH),0) FROM SIAK_BLANGKO_CAPIL_IN WHERE TRUNC(CREATED_DT,'YYYY') = TRUNC(SYSDATE,'YYYY') AND JENIS = $jns) AS MASUK,
					(SELECT NVL(SUM(JUMLAH) - (SUM(RUSAK)),0) AS JUMLAH FROM SIAK_BLANGKO_CAPIL_OUT WHERE TRUNC(CREATED_DT,'YYYY') = TRUNC(SYSDATE,'YYYY') AND JENIS = $jns) AS KELUAR,
					(SELECT NVL(SUM(RUSAK),0) AS JUMLAH FROM SIAK_BLANGKO_CAPIL_OUT WHERE TRUNC(CREATED_DT,'YYYY') = TRUNC(SYSDATE,'YYYY') AND JENIS = $jns) AS RUSAK,
					(SELECT NVL(SUM(JUMLAH),0) FROM SIAK_BLANGKO_CAPIL_IN WHERE JENIS = $jns) - (SELECT NVL(SUM(JUMLAH),0) AS JUMLAH FROM SIAK_BLANGKO_CAPIL_OUT WHERE JENIS = $jns) AS SISA
					FROM DUAL";
			$r = DB::select($sql);
			return $r;
		}

		public function blk_save_blangko($no_kec,$tgl,$pengambilan,$rusak,$pengembalian){
			$sql = "INSERT INTO SIAK_MONEV_BLANGKO (NO_KEC,AMBIL,RUSAK,PENGEMBALIAN,TANGGAL,BLANGKO_ID) VALUES ($no_kec,$pengambilan,$rusak,$pengembalian,SYSDATE,BLANGKO_ID.NEXTVAL)";
			$r = DB::insert($sql);
			return $r;
		}

		public function blk_save_dist_belangko($no_kec,$tgl,$penerima,$ip_address,$username){
			$sql = "INSERT INTO SIAK_DISTRIBUSI_KEPING (ID,NO_KEC,PENERIMA,UPDATE_DT,UPDATE_BY,IP_ADDRESS) VALUES ('distriution-".time()."',$no_kec,'$penerima',SYSDATE,'$username','$ip_address')";
			$r = DB::insert($sql);
			return $r;
		}

		public function blk_update_dist_belangko($no_kec,$petugas){
			$sql = "UPDATE SIAK_REQ_CETAK_KTP A SET DIS_DT = SYSDATE, DIS_STAT = 'Y', DIS_BY = '$petugas' WHERE
					EXISTS (SELECT 1 FROM (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME,LAST_UPDATE,CREATED 
					FROM (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME,LAST_UPDATE,CREATED, RANK() OVER (PARTITION BY NIK ORDER BY  PERSONALIZED_DATE DESC, LAST_UPDATE DESC, CHIP_ID DESC) RNK FROM CARD_MANAGEMENT) 
					WHERE RNK = 1) B 
					WHERE A.NIK = B.NIK AND TRUNC(A.REQ_DATE) <= CASE WHEN B.LAST_UPDATE IS NULL THEN TRUNC(B.PERSONALIZED_DATE) ELSE TRUNC(B.LAST_UPDATE) END) AND TRUNC(A.REQ_DATE) < TRUNC(SYSDATE-1)
					AND A.DIS_STAT = 'N' AND A.NO_KEC = $no_kec";
			$r = DB::update($sql);
			return $r;
		}

		public function blk_update_distld_belangko($no_kec,$petugas){
			$sql = "UPDATE SIAK_REQ_CETAK_KTPLD A SET DIS_DT = SYSDATE, DIS_STAT = 'Y', DIS_BY = '$petugas' WHERE
					EXISTS (SELECT 1 FROM (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME,LAST_UPDATE,CREATED 
					FROM (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME,LAST_UPDATE,CREATED, RANK() OVER (PARTITION BY NIK ORDER BY  PERSONALIZED_DATE DESC, LAST_UPDATE DESC, CHIP_ID DESC) RNK FROM CARD_MANAGEMENT) 
					WHERE RNK = 1) B 
					WHERE A.NIK = B.NIK AND TRUNC(A.REQ_DATE) <= CASE WHEN B.LAST_UPDATE IS NULL THEN TRUNC(B.PERSONALIZED_DATE) ELSE TRUNC(B.LAST_UPDATE) END) AND TRUNC(A.REQ_DATE) < TRUNC(SYSDATE-1)
					AND A.DIS_STAT = 'N' AND A.NO_KEC = $no_kec";
			$r = DB::update($sql);
			return $r;
		}

		public function blk_save_blangko_capil($jenis,$tgl,$pengambilan,$rusak,$ket,$username){
			$sql = "INSERT INTO SIAK_BLANGKO_CAPIL_OUT (JENIS,JUMLAH,RUSAK,KET,CREATED_DT,CREATED_BY) VALUES ($jenis,$pengambilan,$rusak,'$ket',SYSDATE,'$username')";
			$r = DB::insert($sql);
			return $r;
		}
		public function blk_update_blangko($no_kec,$tgl,$pengambilan,$rusak,$pengembalian){
			$sql = "UPDATE SIAK_MONEV_BLANGKO SET AMBIL = $pengambilan, RUSAK =$rusak, PENGEMBALIAN = $pengembalian WHERE NO_KEC = $no_kec AND TO_CHAR(TANGGAL,'DD-MM-YYYY') = '$tgl'";
			$r = DB::update($sql);
			return $r;
		}
		public function blk_update_blangko_capil($jenis,$tgl,$pengambilan,$rusak,$ket,$username){
			$sql = "UPDATE SIAK_BLANGKO_CAPIL_OUT SET JUMLAH = $pengambilan, RUSAK =$rusak, KET = '$ket',CREATED_BY = '$username' WHERE JENIS = $jenis AND TO_CHAR(CREATED_DT,'DD-MM-YYYY') = '$tgl'";
			$r = DB::update($sql);
			return $r;
		}
		public function blk_save_inblangko($blangko_masuk,$tgl,$keterangan = '',$user_name){
			$sql = "INSERT INTO SIAK_BLANGKO (JUMLAH,TANGGAL_MASUK,CREATED_BY,KET) VALUES ($blangko_masuk,SYSDATE,'$user_name','$keterangan')";
			$r = DB::insert($sql);
			return $r;
		}
		public function blk_save_inblangko_capil($blangko_masuk,$tgl,$keterangan = '',$user_name, $jns){
			$sql = "INSERT INTO SIAK_BLANGKO_CAPIL_IN (JUMLAH,CREATED_DT,CREATED_BY,KET,JENIS) VALUES ($blangko_masuk,SYSDATE,'$user_name','$keterangan',$jns)";
			$r = DB::insert($sql);
			return $r;
		}
		public function blk_update_inblangko_gudang($blangko_gudang){
			$sql = "UPDATE SIAK_MASTER SET SYSTEM_VALUE_NUM = $blangko_gudang  WHERE SYSTEM_TYPE = 'SET_DAFDUK' AND SYSTEM_CODE = 'KTP_GUDANG'";
			$r = DB::update($sql);
			return $r;
		}
		public function blk_update_inblangko($blangko_masuk,$tgl,$keterangan = '',$user_name){
			$sql = "UPDATE SIAK_BLANGKO SET JUMLAH = $blangko_masuk, CREATED_BY ='$user_name', KET = '$keterangan' WHERE TO_CHAR(TANGGAL_MASUK,'DD-MM-YYYY') = '$tgl'";
			$r = DB::update($sql);
			return $r;
		}
		public function blk_update_inblangko_capil($blangko_masuk,$tgl,$keterangan = '',$user_name, $jns){
			$sql = "UPDATE SIAK_BLANGKO_CAPIL_IN SET JUMLAH = $blangko_masuk, CREATED_BY ='$user_name', KET = '$keterangan' WHERE TO_CHAR(CREATED_DT,'DD-MM-YYYY') = '$tgl' AND JENIS = $jns";
			$r = DB::update($sql);
			return $r;
		}
		public function blk_cek_rekap_blangko($tgl_start,$tgl_end){
			$sql = "";
			$sql .= "SELECT 
					  NO_WIL
					  , NAMA_WIL
					  , CASE WHEN AMBIL IS NULL THEN 0 ELSE AMBIL END AS AMBIL 
					  , CASE WHEN RUSAK IS NULL THEN 0 ELSE RUSAK END AS RUSAK 
					  , CASE WHEN PENGEMBALIAN IS NULL THEN 0 ELSE PENGEMBALIAN END AS PENGEMBALIAN 
					  , CASE WHEN TOTAL IS NULL THEN 0 ELSE TOTAL END AS TOTAL 
					FROM SIAK_KODE_WIL A LEFT JOIN 
					(SELECT 
					  A.NO_KEC
					  , SUM(A.AMBIL) AS AMBIL
					  , SUM(A.RUSAK) AS RUSAK
					  , SUM(A.PENGEMBALIAN) AS PENGEMBALIAN
					  , SUM(A.AMBIL) - (SUM(A.RUSAK)+SUM(A.PENGEMBALIAN)) AS TOTAL
					FROM SIAK_MONEV_BLANGKO A
					WHERE 1=1
					AND A.TANGGAL >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND A.TANGGAL < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1
					GROUP BY A.NO_KEC ORDER BY A.NO_KEC) B ON A.NO_WIL = B.NO_KEC ORDER BY A.NO_WIL";
			$r = DB::select($sql);
			return $r;
		}
		public function blk_cek_rekap_blangko_capil($tgl_start,$tgl_end,$jenis,$jenis_lap){
			$sql = "";
			if($jenis_lap == 0){
				if($jenis == 0){
				$sql .= "SELECT JENIS_BLANGKO,'BLANGKO MASUK' JENIS, JUMLAH, '-' RUSAK, CASE WHEN KET IS NULL THEN '-' ELSE KET END KET, CREATED_DT, TO_CHAR(CREATED_DT,'DD-MM-YYYY') TANGGAL  FROM SIAK_BLANGKO_CAPIL_IN A INNER JOIN SIAK_BLANGKO_CAPIL_OPTION B ON A.JENIS = B.ID
				UNION ALL
				SELECT JENIS_BLANGKO,'BLANGKO KELUAR' JENIS, JUMLAH, CASE WHEN RUSAK = 0 THEN '-' ELSE TO_CHAR(RUSAK) END RUSAK, CASE WHEN KET IS NULL THEN '-' ELSE KET END KET, CREATED_DT, TO_CHAR(CREATED_DT,'DD-MM-YYYY') TANGGAL  FROM SIAK_BLANGKO_CAPIL_OUT A INNER JOIN SIAK_BLANGKO_CAPIL_OPTION B ON A.JENIS = B.ID
				ORDER BY CREATED_DT DESC";	
				}else{
					$sql .= "SELECT JENIS_BLANGKO,'BLANGKO MASUK' JENIS, JUMLAH, '-' RUSAK, CASE WHEN KET IS NULL THEN '-' ELSE KET END KET, CREATED_DT, TO_CHAR(CREATED_DT,'DD-MM-YYYY') TANGGAL  FROM SIAK_BLANGKO_CAPIL_IN A INNER JOIN SIAK_BLANGKO_CAPIL_OPTION B ON A.JENIS = B.ID WHERE A.JENIS = $jenis
				UNION ALL
				SELECT JENIS_BLANGKO,'BLANGKO KELUAR' JENIS, JUMLAH, CASE WHEN RUSAK = 0 THEN '-' ELSE TO_CHAR(RUSAK) END RUSAK, CASE WHEN KET IS NULL THEN '-' ELSE KET END KET, CREATED_DT, TO_CHAR(CREATED_DT,'DD-MM-YYYY') TANGGAL  FROM SIAK_BLANGKO_CAPIL_OUT A INNER JOIN SIAK_BLANGKO_CAPIL_OPTION B ON A.JENIS = B.ID WHERE A.JENIS = $jenis
				ORDER BY CREATED_DT DESC";	

				}
				
			}else if($jenis_lap == 1){
				if($jenis == 0){
					$sql .= "SELECT JENIS_BLANGKO,'BLANGKO MASUK' JENIS, JUMLAH, '-' RUSAK, CASE WHEN KET IS NULL THEN '-' ELSE KET END KET, CREATED_DT, TO_CHAR(CREATED_DT,'DD-MM-YYYY') TANGGAL  FROM SIAK_BLANGKO_CAPIL_IN A INNER JOIN SIAK_BLANGKO_CAPIL_OPTION B ON A.JENIS = B.ID
					ORDER BY CREATED_DT DESC";	
				}else{
					$sql .= "SELECT JENIS_BLANGKO,'BLANGKO MASUK' JENIS, JUMLAH, '-' RUSAK, CASE WHEN KET IS NULL THEN '-' ELSE KET END KET, CREATED_DT, TO_CHAR(CREATED_DT,'DD-MM-YYYY') TANGGAL  FROM SIAK_BLANGKO_CAPIL_IN A INNER JOIN SIAK_BLANGKO_CAPIL_OPTION B ON A.JENIS = B.ID WHERE A.JENIS = $jenis
					ORDER BY CREATED_DT DESC";	
				}
			}else{
				if($jenis == 0){
					$sql .= "SELECT JENIS_BLANGKO,'BLANGKO KELUAR' JENIS, JUMLAH, CASE WHEN RUSAK = 0 THEN '-' ELSE TO_CHAR(RUSAK) END RUSAK, CASE WHEN KET IS NULL THEN '-' ELSE KET END KET, CREATED_DT, TO_CHAR(CREATED_DT,'DD-MM-YYYY') TANGGAL  FROM SIAK_BLANGKO_CAPIL_OUT A INNER JOIN SIAK_BLANGKO_CAPIL_OPTION B ON A.JENIS = B.ID
					ORDER BY CREATED_DT DESC";	
				}else{
					$sql .= "SELECT JENIS_BLANGKO,'BLANGKO KELUAR' JENIS, JUMLAH, CASE WHEN RUSAK = 0 THEN '-' ELSE TO_CHAR(RUSAK) END RUSAK, CASE WHEN KET IS NULL THEN '-' ELSE KET END KET, CREATED_DT, TO_CHAR(CREATED_DT,'DD-MM-YYYY') TANGGAL  FROM SIAK_BLANGKO_CAPIL_OUT A INNER JOIN SIAK_BLANGKO_CAPIL_OPTION B ON A.JENIS = B.ID WHERE A.JENIS = $jenis
					ORDER BY CREATED_DT DESC";	
				}
			}

			$r = DB::select($sql);
			return $r;
		}
		public function blk_cek_rekap_count_blangko($tgl_start,$tgl_end){
			$sql = "";
			$sql .= "SELECT 
					    CASE WHEN SUM(A.AMBIL) IS NULL THEN 0 ELSE SUM(A.AMBIL) END AS AMBIL
	  					, CASE WHEN SUM(A.RUSAK) IS NULL THEN 0 ELSE SUM(A.RUSAK) END AS RUSAK
	  					, CASE WHEN SUM(A.PENGEMBALIAN) IS NULL THEN 0 ELSE SUM(A.PENGEMBALIAN) END AS PENGEMBALIAN
	  					, CASE WHEN SUM(A.AMBIL) - (SUM(A.RUSAK)+SUM(A.PENGEMBALIAN)) IS NULL THEN 0 ELSE SUM(A.AMBIL) - (SUM(A.RUSAK)+SUM(A.PENGEMBALIAN)) END AS TOTAL
					FROM SIAK_MONEV_BLANGKO A
					WHERE 1=1
					AND A.TANGGAL >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND A.TANGGAL < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1";
			$r = DB::select($sql);
			return $r;
		}
		// BLANGKO KK
		public function blk_get_count_blangko_kk($no_kec,$tgl){
			$sql = "SELECT COUNT(1) AS JML FROM SIAK_MONEV_BLANGKO_KK WHERE NO_KEC = $no_kec AND TO_CHAR(TANGGAL,'DD-MM-YYYY') = '$tgl'";
			$r = DB::select($sql);
			return $r[0]->jml;
		}
		public function blk_get_blangko_kk($no_kec,$tgl){
			$sql = "SELECT AMBIL,RUSAK FROM SIAK_MONEV_BLANGKO_KK WHERE NO_KEC = $no_kec AND TO_CHAR(TANGGAL,'DD-MM-YYYY') = '$tgl'";
			$r = DB::select($sql);
			return $r;
		}
		public function blk_get_count_all_blangko_kk($no_kec,$tgl){
			$sql = "SELECT COUNT(1) AS JML FROM SIAK_MONEV_BLANGKO_KK WHERE TO_CHAR(TANGGAL,'DD-MM-YYYY') = '$tgl' AND NO_KEC = $no_kec";
			$r = DB::select($sql);
			return $r[0]->jml;
		}
		public function blk_get_count_saldo_blangko_kk($no_kec){
			$sql = "SELECT COUNT(1) AS JML FROM SIAK_MONEV_BLANGKO_KK WHERE NO_KEC = $no_kec ";
			$r = DB::select($sql);
			return $r[0]->jml;
		}
		public function blk_get_all_blangko_kk($no_kec,$tgl){
			$sql = "SELECT SUM(AMBIL) AMBIL, SUM(RUSAK) RUSAK, SUM(AMBIL) - SUM(RUSAK) TOTAL FROM SIAK_MONEV_BLANGKO_KK WHERE TO_CHAR(TANGGAL,'DD-MM-YYYY') = '$tgl' AND NO_KEC = $no_kec";
			$r = DB::select($sql);
			return $r;
		}
		public function blk_get_saldo_blangko_kk($no_kec){
			$sql = "SELECT  SUM(AMBIL) - SUM(RUSAK) SALDO FROM SIAK_MONEV_BLANGKO_KK WHERE NO_KEC = $no_kec ";
			$r = DB::select($sql);
			return $r;
		}
		public function blk_get_allin_kk_blangko(){
			$sql = "SELECT 
					(SELECT SUM(JUMLAH) FROM SIAK_BLANGKO_KK) AS MASUK,
					(SELECT SUM(AMBIL) - SUM(RUSAK) AS JUMLAH FROM SIAK_MONEV_BLANGKO_KK ) AS KELUAR,
					(SELECT SUM(RUSAK)AS JUMLAH FROM SIAK_MONEV_BLANGKO_KK) AS RUSAK,
					(SELECT SUM(JUMLAH) FROM SIAK_BLANGKO_KK) - (SELECT SUM(AMBIL) AS JUMLAH FROM SIAK_MONEV_BLANGKO_KK) AS SISA
					FROM DUAL";
			$r = DB::select($sql);
			return $r;
		}
		public function blk_save_blangko_kk($no_kec,$tgl,$pengambilan,$rusak,$user_id){
			$sql = "INSERT INTO SIAK_MONEV_BLANGKO_KK (NO_KEC,AMBIL,RUSAK,PENGEMBALIAN,TANGGAL,BLANGKO_ID,CREATED_BY) VALUES ($no_kec,$pengambilan,$rusak,$pengembalian,SYSDATE,BLANGKO_KK_ID.NEXTVAL,'$user_id')";
			$r = DB::insert($sql);
			return $r;
		}
		public function blk_update_blangko_kk($no_kec,$tgl,$pengambilan,$rusak,$user_id){
			$sql = "UPDATE SIAK_MONEV_BLANGKO_KK SET AMBIL = $pengambilan, RUSAK =$rusak, CREATED_BY = '$user_id' WHERE NO_KEC = $no_kec AND TO_CHAR(TANGGAL,'DD-MM-YYYY') = '$tgl'";
			$r = DB::update($sql);
			return $r;
		}
		public function blk_save_inblangko_kk($blangko_masuk,$tgl,$keterangan = '',$user_name){
			$sql = "INSERT INTO SIAK_BLANGKO (JUMLAH,TANGGAL_MASUK,CREATED_BY,KET) VALUES ($blangko_masuk,SYSDATE,'$user_name','$keterangan')";
			$r = DB::insert($sql);
			return $r;
		}
		public function blk_update_inblangko_kk($blangko_masuk,$tgl,$keterangan = '',$user_name){
			$sql = "UPDATE SIAK_BLANGKO SET JUMLAH = $blangko_masuk, CREATED_BY ='$user_name', KET = '$keterangan' WHERE TO_CHAR(TANGGAL_MASUK,'DD-MM-YYYY') = '$tgl'";
			$r = DB::update($sql);
			return $r;
		}
		// CETAK DINAS
		public function blk_get_nik($nik){
			$sql = "SELECT COUNT(1) AS JML FROM BIODATA_WNI@DB222 WHERE NIK = $nik";
			$r = DB::select($sql);
			return $r[0]->jml;
		}
		public function blk_get_data_nik($nik){
			$sql = "SELECT A.NAMA_LGKP, A.NO_KEC, (SELECT B.NAMA_KEC FROM SETUP_KEC@DB222 B WHERE A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC) AS NAMA_KEC FROM BIODATA_WNI@DB222 A WHERE A.NIK = $nik";
			$r = DB::select($sql);
			return $r;
		}
		public function blk_get_nik_kia($nik){
			$sql = "SELECT 
					A.NIK
					, A.NAMA_LGKP
					, A.NO_KEC
					, F5_GET_NAMA_KECAMATAN(32,73,A.NO_KEC) NAMA_KEC
					, TRUNC(MONTHS_BETWEEN(A.PRINTED_DATE,B.TGL_LHR)/12) UMUR
					, TO_CHAR(A.PRINTED_DATE,'DD-MM-YYYY') TGL_CETAK 
					, B.TMPT_LHR
					, TO_CHAR(B.TGL_LHR,'DD-MM-YYYY') AS TGL_LHR
					, UPPER(F5_GET_REF_WNI(F5_TO_NUMBER(B.JENIS_KLMIN,7), 801)) JENIS_KLMIN
					, UPPER(F5_GET_REF_WNI(B.GOL_DRH, 401)) GOL_DRH
					, UPPER(F5_GET_REF_WNI(F5_TO_NUMBER(B.AGAMA,7), 501)) AGAMA
					, D.ALAMAT
					, LPAD(TO_CHAR(D.NO_RT), 3, '0') AS RT
					, LPAD(TO_CHAR(D.NO_RW), 3, '0') AS RW
					, CASE WHEN D.KODE_POS IS NULL THEN '-' ELSE TO_CHAR(D.KODE_POS) END KODE_POS
					, B.NIK_IBU
					, B.NAMA_LGKP_IBU
					, B.NIK_AYAH
					, B.NAMA_LGKP_AYAH
					, B.NO_KK
					, B.NO_PROP ||' - '|| F5_GET_NAMA_PROVINSI(B.NO_PROP) PROVINSI
					, B.NO_KAB ||' - '|| F5_GET_NAMA_KABUPATEN(B.NO_PROP, B.NO_KAB) KABUPATEN
					, B.NO_KEC ||' - '|| F5_GET_NAMA_KECAMATAN(B.NO_PROP, B.NO_KAB, B.NO_KEC) KECAMATAN
					, B.NO_KEL ||' - '|| F5_GET_NAMA_KELURAHAN(B.NO_PROP, B.NO_KAB, B.NO_KEC, B.NO_KEL) KELURAHAN
					, B.NO_AKTA_LHR
					, C.ANAK_KE
					, TO_CHAR(C.CREATED_DATE,'DD-MM-YYYY') CREATED_DATE
					, C.CREATED_BY
					, E.PATH
					FROM (SELECT SEQN_ID , NO_DOKUMEN, NIK , NAMA_LGKP , JENIS_KLMIN , NO_KK, ALAMAT , PRINT_TYPE , STATUS_BLANGKO , PRINTED_BY , PRINTED_DATE , CHECKED_BY , CHECKED_DATE , REVOKED_BY , REVOKED_DATE , NO_BLANGKO , NO_PROP , NO_KAB , NO_KEC , NO_KEL, FILENAME , TIPE_DOK FROM (SELECT SEQN_ID , NO_DOKUMEN, NIK , NAMA_LGKP , JENIS_KLMIN , NO_KK, ALAMAT , PRINT_TYPE , STATUS_BLANGKO , PRINTED_BY , PRINTED_DATE , CHECKED_BY , CHECKED_DATE , REVOKED_BY , REVOKED_DATE , NO_BLANGKO , NO_PROP , NO_KAB , NO_KEC , NO_KEL, FILENAME , TIPE_DOK, RANK() OVER (PARTITION BY NIK ORDER BY PRINTED_DATE DESC,SEQN_ID DESC) RNK FROM T5_SEQN_KIA_PRINT) WHERE RNK = 1) A 
					LEFT JOIN LTS_KIA C ON A.NIK = C.NIK
					LEFT JOIN BIODATA_WNI B ON A.NIK = B.NIK
					LEFT JOIN DATA_KELUARGA D ON B.NO_KK = D.NO_KK
					LEFT JOIN T5_FOTO E ON B.NIK = E.NIK
					WHERE A.NIK = $nik";
			$r = DB::connection('db222')->select($sql);
			return $r;
		}
		public function blk_get_idpengajuan_kia($idpengajuan){
			$sql = "SELECT 
					A.NIK
					, A.NAMA_LGKP
					, A.NO_KEC
					, F5_GET_NAMA_KECAMATAN(32,73,A.NO_KEC) NAMA_KEC
					, TRUNC(MONTHS_BETWEEN(A.PRINTED_DATE,B.TGL_LHR)/12) UMUR
					, TO_CHAR(A.PRINTED_DATE,'DD-MM-YYYY') TGL_CETAK 
					, B.TMPT_LHR
					, TO_CHAR(B.TGL_LHR,'DD-MM-YYYY') AS TGL_LHR
					, UPPER(F5_GET_REF_WNI(F5_TO_NUMBER(B.JENIS_KLMIN,7), 801)) JENIS_KLMIN
					, UPPER(F5_GET_REF_WNI(B.GOL_DRH, 401)) GOL_DRH
					, UPPER(F5_GET_REF_WNI(F5_TO_NUMBER(B.AGAMA,7), 501)) AGAMA
					, D.ALAMAT
					, LPAD(TO_CHAR(D.NO_RT), 3, '0') AS RT
					, LPAD(TO_CHAR(D.NO_RW), 3, '0') AS RW
					, CASE WHEN D.KODE_POS IS NULL THEN '-' ELSE TO_CHAR(D.KODE_POS) END KODE_POS
					, B.NIK_IBU
					, B.NAMA_LGKP_IBU
					, B.NIK_AYAH
					, B.NAMA_LGKP_AYAH
					, B.NO_KK
					, B.NO_PROP ||' - '|| F5_GET_NAMA_PROVINSI(B.NO_PROP) PROVINSI
					, B.NO_KAB ||' - '|| F5_GET_NAMA_KABUPATEN(B.NO_PROP, B.NO_KAB) KABUPATEN
					, B.NO_KEC ||' - '|| F5_GET_NAMA_KECAMATAN(B.NO_PROP, B.NO_KAB, B.NO_KEC) KECAMATAN
					, B.NO_KEL ||' - '|| F5_GET_NAMA_KELURAHAN(B.NO_PROP, B.NO_KAB, B.NO_KEC, B.NO_KEL) KELURAHAN
					, B.NO_AKTA_LHR
					, C.ANAK_KE
					, TO_CHAR(C.CREATED_DATE,'DD-MM-YYYY') CREATED_DATE
					, C.CREATED_BY
					, E.PATH
					, F.ID IDPENGAJUAN
					FROM (SELECT SEQN_ID , NO_DOKUMEN, NIK , NAMA_LGKP , JENIS_KLMIN , NO_KK, ALAMAT , PRINT_TYPE , STATUS_BLANGKO , PRINTED_BY , PRINTED_DATE , CHECKED_BY , CHECKED_DATE , REVOKED_BY , REVOKED_DATE , NO_BLANGKO , NO_PROP , NO_KAB , NO_KEC , NO_KEL, FILENAME , TIPE_DOK FROM (SELECT SEQN_ID , NO_DOKUMEN, NIK , NAMA_LGKP , JENIS_KLMIN , NO_KK, ALAMAT , PRINT_TYPE , STATUS_BLANGKO , PRINTED_BY , PRINTED_DATE , CHECKED_BY , CHECKED_DATE , REVOKED_BY , REVOKED_DATE , NO_BLANGKO , NO_PROP , NO_KAB , NO_KEC , NO_KEL, FILENAME , TIPE_DOK, RANK() OVER (PARTITION BY NIK ORDER BY PRINTED_DATE DESC,SEQN_ID DESC) RNK FROM T5_SEQN_KIA_PRINT) WHERE RNK = 1) A 
					LEFT JOIN LTS_KIA C ON A.NIK = C.NIK
					LEFT JOIN BIODATA_WNI B ON A.NIK = B.NIK
					LEFT JOIN DATA_KELUARGA D ON B.NO_KK = D.NO_KK
					LEFT JOIN T5_FOTO E ON B.NIK = E.NIK
					LEFT JOIN (SELECT NIK, ID FROM PENGAJUAN_HEADER@WEBDB A UNION ALL SELECT NIK, ID FROM PENGAJUAN_HEADER_DELETE@WEBDB ) F ON A.NIK = F.NIK
					WHERE F.ID = $idpengajuan";
			$r = DB::connection('db222')->select($sql);
			return $r;
		}
		public function blk_hist_cetak_kia($nik){
			$sql = "SELECT NIK, NAMA_LGKP, TGL_CETAK, PRINTED_BY, FILE_PDF FROM (SELECT NIK, NAMA_LGKP, TO_CHAR(PRINTED_DATE,'DD-MM-YYYY') TGL_CETAK,PRINTED_BY, CASE WHEN FILENAME IS NOT NULL THEN 'http://10.32.73.222:8080/Siak/tmpcetak/'||TO_CHAR(PRINTED_DATE,'YYYYMMDD')||'/'||REPLACE(FILENAME,'.pdf','')||'.pdf' ELSE '-' END FILE_PDF, RANK() OVER (PARTITION BY NIK ORDER BY PRINTED_DATE DESC,SEQN_ID DESC) RNK FROM T5_SEQN_KIA_PRINT WHERE NIK = $nik ORDER BY PRINTED_DATE DESC) WHERE RNK = 1";
			$r = DB::connection('db222')->select($sql);
			return $r;

			// http://10.32.73.222:8080/Siak/tmpcetak/20210723/assist_yz_60fa753b8cd81.pdf
		}
		public function blk_hist_cetak_kia_id($idpengajuan){
			$sql = "SELECT A.NIK, A.NAMA_LGKP, A.TGL_CETAK, A.PRINTED_BY, A.FILE_PDF FROM (SELECT A.NIK, A.NAMA_LGKP, TO_CHAR(A.PRINTED_DATE,'DD-MM-YYYY') TGL_CETAK,A.PRINTED_BY
                    , CASE WHEN A.FILENAME IS NOT NULL THEN 'http://10.32.73.222:8080/Siak/tmpcetak/'||TO_CHAR(PRINTED_DATE,'YYYYMMDD')||'/'||REPLACE(A.FILENAME,'.pdf','')||'.pdf' ELSE '-' END FILE_PDF, RANK() OVER (PARTITION BY A.NIK ORDER BY A.PRINTED_DATE DESC,A.SEQN_ID DESC) RNK 
                    FROM T5_SEQN_KIA_PRINT A LEFT JOIN (SELECT NIK, ID FROM PENGAJUAN_HEADER@WEBDB A UNION ALL SELECT NIK, ID FROM PENGAJUAN_HEADER_DELETE@WEBDB ) F ON A.NIK = F.NIK
                    WHERE F.ID = $idpengajuan ORDER BY PRINTED_DATE DESC) A WHERE RNK = 1";
			$r = DB::connection('db222')->select($sql);
			return $r;
		}
		public function blk_hist_delivery_kia($nik){
			$sql = "SELECT NIK, NAMA_LGKP, TO_CHAR(TGL_PENGAMBILAN,'DD-MM-YYYY HH24:MI:SS') TGL_PENGAMBILAN, LOCATION, JENIS_PENGAMBILAN, REQ_BY, RECEIVE_BY, CREATED_BY  FROM SIAK_DELIVERY_KIA WHERE NIK = $nik ORDER BY TGL_PENGAMBILAN DESC";
			$r = DB::select($sql);
			return $r;
		}
		public function blk_hist_delivery_kia_id($idpengajuan){
			$sql = "SELECT A.NIK, A.NAMA_LGKP, TO_CHAR(A.TGL_PENGAMBILAN,'DD-MM-YYYY HH24:MI:SS') TGL_PENGAMBILAN, A.LOCATION, A.JENIS_PENGAMBILAN, A.REQ_BY, A.RECEIVE_BY, A.CREATED_BY  
				FROM SIAK_DELIVERY_KIA A LEFT JOIN (SELECT NIK, ID FROM PENGAJUAN_HEADER@WEBDB A UNION ALL SELECT NIK, ID FROM PENGAJUAN_HEADER_DELETE@WEBDB ) F ON A.NIK = F.NIK
				WHERE F.ID = $idpengajuan ORDER BY TGL_PENGAMBILAN DESC";
			$r = DB::select($sql);
			return $r;
		}
		public function blk_save_cetak($nik,$nama,$no_kec,$nama_kec,$request_by,$receive,$tanggal,$user_name){
			$sql = "INSERT INTO SIAK_CETAK_DINAS (NO,NIK,NAMA_LGKP,NO_KEC,NAMA_KEC,REQ_BY,TGL_CETAK,RECEIVE_BY,CREATED_BY) VALUES (CETAK_DINAS_NO.NEXTVAL,$nik,'$nama',$no_kec,'$nama_kec','$request_by',SYSDATE,'$receive','$user_name')";
			$r = DB::insert($sql);
			return $r;
		}
		public function blk_save_cetakdns($nik,$request_by){
			$sql = "UPDATE SIAK_REQ_CETAK_KTP SET DIS_DT = SYSDATE, DIS_STAT = 'Y', DIS_BY = '$request_by' WHERE NIK = $nik";
			$r = DB::update($sql);
			return $r;
		}
		public function blk_save_cetakdnsld($nik,$request_by){
			$sql = "UPDATE SIAK_REQ_CETAK_KTPLD SET DIS_DT = SYSDATE, DIS_STAT = 'Y', DIS_BY = '$request_by' WHERE NIK = $nik";
			$r = DB::update($sql);
			return $r;
		}
		public function blk_save_kia($nik,$nama,$no_kec,$nama_kec,$location_id,$pengambilan_id,$request_by,$receive,$tanggal,$tanggal_cetak,$user_name){
			
			$sql = "INSERT INTO SIAK_DELIVERY_KIA 
					(NO,NIK,NAMA_LGKP,NO_KEC,NAMA_KEC,LOCATION,JENIS_PENGAMBILAN,REQ_BY,TGL_CETAK,TGL_PENGAMBILAN,RECEIVE_BY,CREATED_BY) 
					VALUES 
					(DELIVERY_KIA_NO.NEXTVAL,$nik,'$nama',$no_kec,'$nama_kec','$location_id','$pengambilan_id','$request_by',TO_DATE('$tanggal_cetak','DD-MM-YYYY'),SYSDATE,'$receive','$user_name')";
			$r = DB::insert($sql);
			return $r;
		}
		public function blk_cek_rekap_cetak_dinas($nik,$nama){
			$sql ="";
			$sql .= " SELECT NO, CASE WHEN NAMA_LGKP IS NULL THEN '-' ELSE NAMA_LGKP END AS NAMA_LGKP, NIK, NO_KEC, NAMA_KEC, REQ_BY, TO_CHAR(TGL_CETAK,'DD-MM-YYYY') AS TGL_CETAK, RECEIVE_BY FROM SIAK_CETAK_DINAS WHERE 1=1";
			if ($nik != ''){
				$sql .= " AND NIK IN ($nik)";	
			}
			if ($nama != ''){
				$sql .= " AND UPPER(NAMA_LGKP) LIKE '".$nama."'";
			}
			$r = DB::select($sql);
			return $r;
		}
		public function blk_cek_count_cetak_dinas($nik,$nama){
			$sql ="";
			$sql .= "SELECT COUNT(1) AS JML FROM SIAK_CETAK_DINAS WHERE 1=1";
			if ($nik != ''){
				$sql .= " AND NIK IN ($nik)";	
			}
			if ($nama != ''){
				$sql .= " AND UPPER(NAMA_LGKP) LIKE '".$nama."'";
			}
			$r = DB::select($sql);
			return $r[0]->jml;
		}
		public function blk_rekap_cetak_dinas($tgl_start,$tgl_end){
			$sql ="";
			$sql .= " SELECT NO, CASE WHEN NAMA_LGKP IS NULL THEN '-' ELSE NAMA_LGKP END AS NAMA_LGKP, NIK, NO_KEC, NAMA_KEC, REQ_BY, TO_CHAR(TGL_CETAK,'DD-MM-YYYY') AS TGL_CETAK, RECEIVE_BY, CREATED_BY FROM SIAK_CETAK_DINAS WHERE 1=1 AND TGL_CETAK >= TO_DATE('$tgl_start','DD-MM-YYYY') AND TGL_CETAK < TO_DATE('$tgl_end','DD-MM-YYYY') +1 ORDER BY NO DESC";
			$r = DB::select($sql);
			return $r;
		}
		public function blk_count_cetak_dinas($tgl_start,$tgl_end){
			$sql ="";
			$sql .= "SELECT COUNT(1) AS JML FROM SIAK_CETAK_DINAS WHERE 1=1 AND TGL_CETAK >= TO_DATE('$tgl_start','DD-MM-YYYY') AND TGL_CETAK < TO_DATE('$tgl_end','DD-MM-YYYY') +1";
			$r = DB::select($sql);
			return $r[0]->jml;
		}
		public function blk_get_ludo(){
			$sql ="";
			$sql .= "SELECT COUNT(1) AS JML FROM SIAK_MONEV_BLANGKO_LUDO WHERE 1=1 AND TRUNC(TANGGAL) = TRUNC(SYSDATE) ";
			$r = DB::select($sql);
			return $r[0]->jml;
		}
		public function blk_save_ludo($jml){
			$sql ="";
			$sql .= "INSERT INTO SIAK_MONEV_BLANGKO_LUDO (AMBIL,RUSAK,PENGEMBALIAN,TANGGAL) VALUES ($jml,0,0,SYSDATE)";
			$r = DB::insert($sql);
			return $r;
		}
		public function blk_update_ludo($jml){
			$sql ="";
			$sql .= "UPDATE SIAK_MONEV_BLANGKO_LUDO SET AMBIL = $jml WHERE TRUNC(TANGGAL) = TRUNC(SYSDATE)";
			$r = DB::update($sql);
			return $r;
		}
		public function get_ludo()
		{
			header('Content-type: application/json');
			$r = DB::select("SELECT CASE WHEN SUM(AMBIL) IS NULL THEN 0 ELSE SUM(AMBIL) END JML FROM siak_monev_blangko_ludo WHERE TRUNC(TANGGAL) =  TRUNC(SYSDATE)");
			$response["jum_ludo"] = $r[0]->jml;
	        return $response;
		}
		public function blk_get_count_distribusi_gudang($tgl){
			$sql = "SELECT COUNT(1) AS JML FROM SIAK_BLANGKO_GUDANG WHERE TO_CHAR(TANGGAL_MASUK,'DD-MM-YYYY') = '$tgl'";
			$r = DB::select($sql);
			return $r[0]->jml;
		}
		public function blk_save_distribusi_gudang($blangko_masuk,$tgl,$keterangan = '',$user_name){
			$sql = "INSERT INTO SIAK_BLANGKO_GUDANG (JUMLAH,TANGGAL_MASUK,CREATED_BY,KET) VALUES ($blangko_masuk,SYSDATE,'$user_name','$keterangan')";
			$r = DB::insert($sql);
			return $r;
		}
		public function blk_update_distribusi_gudang($blangko_masuk,$tgl,$keterangan = '',$user_name){
			$sql = "UPDATE SIAK_BLANGKO_GUDANG SET JUMLAH = $blangko_masuk, CREATED_BY ='$user_name', KET = '$keterangan' WHERE TO_CHAR(TANGGAL_MASUK,'DD-MM-YYYY') = '$tgl'";
			$r = DB::update($sql);
			return $r;
		}
		public function blk_get_last_distribusi(){
			$sql ="";
			$sql .= "SELECT NVL(SUM(JUMLAH),0) DATA_DAFDUK FROM SIAK_BLANGKO_GUDANG WHERE TRUNC(TANGGAL_MASUK) = TRUNC(SYSDATE)";
			$r = DB::select($sql);
			return $r;
		}
		public function blk_get_last_pemusnahan(){
			$sql ="";
			$sql .= "SELECT NVL(SUM(JUMLAH),0) DATA_PEMUSNAHAN FROM SIAK_BLANGKO_INVALID WHERE TRUNC(TANGGAL_MASUK) = TRUNC(SYSDATE)";
			$r = DB::select($sql);
			return $r;
		}
		public function blk_rekap_distribusi($tgl_start,$tgl_end){
			$sql = "";
			$sql .="SELECT TANGGAL_MASUK, JUMLAH, CREATED_BY, CASE WHEN KET IS NULL THEN '-' ELSE KET END KET FROM SIAK_BLANGKO_GUDANG WHERE   TANGGAL_MASUK >= TO_DATE('$tgl_start','DD-MM-YYYY') AND TANGGAL_MASUK < TO_DATE('$tgl_end','DD-MM-YYYY')+1 ORDER BY TANGGAL_MASUK DESC";	
			$r = DB::select($sql);
			return $r;
		}

		public function blk_get_count_pemusnahan($tgl){
			$sql = "SELECT COUNT(1) AS JML FROM SIAK_BLANGKO_INVALID WHERE TO_CHAR(TANGGAL_MASUK,'DD-MM-YYYY') = '$tgl'";
			$r = DB::select($sql);
			return $r[0]->jml;
		}
		public function blk_save_pemusnahan($blangko_masuk,$tgl,$keterangan = '',$user_name){
			$sql = "INSERT INTO SIAK_BLANGKO_INVALID (JUMLAH,TANGGAL_MASUK,CREATED_BY,KET) VALUES ($blangko_masuk,SYSDATE,'$user_name','$keterangan')";
			$r = DB::insert($sql);
			return $r;
		}
		public function blk_update_pemusnahan($blangko_masuk,$tgl,$keterangan = '',$user_name){
			$sql = "UPDATE SIAK_BLANGKO_INVALID SET JUMLAH = $blangko_masuk, CREATED_BY ='$user_name', KET = '$keterangan' WHERE TO_CHAR(TANGGAL_MASUK,'DD-MM-YYYY') = '$tgl'";
			$r = DB::update($sql);
			return $r;
		}

		public function blk_hist_distribusi_keping($no_kec){
			$sql = "";
			$sql .="SELECT NO_KEC, NAMA_KEC, PENERIMA, UPDATE_DT
					FROM  
					( SELECT NO_KEC,F5_GET_NAMA_KECAMATAN(32,73,NO_KEC) NAMA_KEC, PENERIMA, TO_CHAR(UPDATE_DT,'DD-MM-YYY') UPDATE_DT
					  FROM SIAK_DISTRIBUSI_KEPING WHERE NO_KEC = $no_kec
					  ORDER BY UPDATE_DT DESC ) 
					WHERE ROWNUM <= 5";	
			$r = DB::select($sql);
			return $r;
		}

		public function blk_hist_distribusikec_keping($no_kec){
			$sql = "";
			$sql .="SELECT A.NO_KEC, A.NAMA_KEC, CASE WHEN B.TANGGAL IS NULL THEN '-' ELSE TO_CHAR(B.TANGGAL,'DD-MM-YYYY') END UPDATE_DT FROM SETUP_KEC A LEFT JOIN (SELECT NO_KEC, MAX(UPDATE_DT) TANGGAL FROM SIAK_DISTRIBUSI_KEPING GROUP BY NO_KEC) B ON A.NO_KEC = B.NO_KEC 
				WHERE A.NO_PROP = 32 AND A.NO_KAB = 73
				ORDER BY A.NO_KEC";	
			$r = DB::select($sql);
			return $r;
		}

}
