<?php

namespace App\Http\Controllers;

use DB;
use Session;
use DateTime;
use PDF;
use Illuminate\Http\Request;
use App\Http\Controllers\SharedController as Shr;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class GisaController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public function Gisa(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 112;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->no_kec != null && $request->no_kel != null && $request->no_rw != null && $request->no_rt != null ){
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$no_rw = $request->no_rw;
			$no_rt = $request->no_rt;
			$r = $this->gisa_get_kk($no_kec,$no_kel,$no_rw,$no_rt);
			$j = $this->gisa_get_count_kk($no_kec,$no_kel,$no_rw,$no_rt);
			$data = array(
		 		"stitle"=>'#Gisa (Gerakan Indonesia Sadar Administrasi Kependudukan)',
		 		"mtitle"=>'#Gisa (Gerakan Indonesia Sadar Administrasi Kependudukan)',
		 		"my_url"=>'Gisa',
		 		"data"=>$r,
		 		"jumlah"=>$j,
		 		"menu"=>$menu,
		 		"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
    		);
			}else if($request->no_kec != null && $request->no_kel != null && $request->no_rw != null && $request->no_rt != null ){
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$no_rw = $request->no_rw;
			$no_rt = $request->no_rt;
			$r = $this->gisa_get_kk($no_kec,$no_kel,$no_rw,$no_rt);
			$j = $this->gisa_get_count_kk($no_kec,$no_kel,$no_rw,$no_rt);
			$data = array(
		 		"stitle"=>'#Gisa (Gerakan Indonesia Sadar Administrasi Kependudukan)',
		 		"mtitle"=>'#Gisa (Gerakan Indonesia Sadar Administrasi Kependudukan)',
		 		"my_url"=>'Gisa',
		 		"data"=>$r,
		 		"jumlah"=>$j,
		 		"menu"=>$menu,
		 		"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
    		);
			}else{
			$data = array(
		 		"stitle"=>'#Gisa (Gerakan Indonesia Sadar Administrasi Kependudukan)',
		 		"mtitle"=>'#Gisa (Gerakan Indonesia Sadar Administrasi Kependudukan)',
		 		"my_url"=>'Gisa',
		 		"menu"=>$menu,
		 		"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
    		);
    		}
			return view('Gisa/main',$data);
			}else{
			return redirect()->route('logout');
		}
	}
	public function cek_detail(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 112;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			if (!empty($request->no_kk)){
			$no_kk = $request->no_kk;
			$j = $this->gisa_get_count_detail_kk($no_kk);
				if ($j>0){
				$k = $this->gisa_get_detail_kk($no_kk);
				$h = $this->gisa_get_detail_anggota_kk($no_kk);
				$data = array(
		 		"stitle"=>'Detail Kartu Keluarga',
		 		"sno_kk"=> $no_kk,
		 		"menu"=>$menu,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
	 			"user_nik"=>$request->session()->get('S_NIK'),
		 		"data_kk"=>$k,
		 		"data_nik"=>$h,
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
	    		);
				return view('gisa_detail/main',$data);
				}else{
					redirect('/','refresh');
				}
			}else{
				redirect('/','refresh');
			}
			}else{
			return redirect()->route('logout');
		}
	    
	}
	public function do_edit(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 112;
			$is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			if (!empty($request->nik)){
			$nik = $request->nik;
			$j = $this->gisa_get_count_detail_nik($nik);
				if ($j>0){
				$d = $this->gisa_get_detail_nik($nik);
				$data = array(
		 		"stitle"=>'Edit #GISA',
		 		"snik"=> 'Edit #GISA Nik '.$nik,
		 		"nik"=> $nik,
		 		"menu"=>$menu,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
	 			"user_nik"=>$request->session()->get('S_NIK'),
		 		"data_nik"=>$d,
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
	    		);
				return view('gisa_edit/main',$data);
				}else{
					redirect('/','refresh');
				}
			}else{
				redirect('/','refresh');
			}
			}else{
			return redirect()->route('logout');
		}
	    
	}
	public function getdata(Request $request) 
	{
		if($this->input->post('nik') != null){
			$nik = $this->input->post('nik');
			$j = $this->gisa_get_count_detail_nik($nik);
			if($j > 0){
				$r = $this->gisa_get_detail_nik($nik);
				$data["akta_lhr"] = $r[0]->status_akta_lhr;
				$data["no_akta_lhr"] = $r[0]->no_akta_lhr;
        		$data["akta_kwn"] = $r[0]->status_akta_kwn;
        		$data["no_akta_kwn"] = $r[0]->no_akta_kwn;
        		$data["tgl_kwn"] = (!empty($r[0]->tgl_kwn)) ? $r[0]->tgl_kwn : '';
        		$data["akta_crai"] = $r[0]->status_akta_crai;
        		$data["no_akta_crai"] = $r[0]->no_akta_crai;
        		$data["tgl_crai"] = (!empty($r[0]->tgl_crai)) ? $r[0]->tgl_crai : '';
        		$data["jumlah"] = $j;
        		echo json_encode($data);
			}else{
				$data["akta_lhr"] = "";
				$data["no_akta_lhr"] = "";
        		$data["akta_kwn"] = "";
        		$data["no_akta_kwn"] = "";
        		$data["tgl_kwn"] = "";
        		$data["akta_crai"] = "";
        		$data["no_akta_crai"] = "";
        		$data["tgl_crai"] = "";
        		$data["jumlah"] = $j;
        		echo json_encode($data);
			}
		}else{
			redirect('/','refresh');
		}
	}
	public function Aprove_kk(Request $request) 
	{
		
			$result = DB::connection('db222')->statement("EXEC UPDATE_ALL_ROKET");
        	$data["messages"] = "KK Aproved";
        	return $result;
		
	}
	public function dosave(Request $request) 
	{
		if($this->input->post('nik') != null){
			$nik = $this->input->post('nik');
			$akta_lhr = $this->input->post('akta_lhr');
			$no_akta_lhr = (!empty($this->input->post('no_akta_lhr'))) ? $this->input->post('no_akta_lhr') : '-';
			$akta_kwn = (!empty($this->input->post('akta_kwn'))) ? $this->input->post('akta_kwn') : '-';
			$no_akta_kwn = (!empty($this->input->post('no_akta_kwn'))) ? $this->input->post('no_akta_kwn') : '-';
			$akta_crai = (!empty($this->input->post('akta_crai'))) ? $this->input->post('akta_crai') : '-';
			$no_akta_crai = (!empty($this->input->post('no_akta_crai'))) ? $this->input->post('no_akta_crai') : '-';
			$user_name = $this->input->post('user_name');
			$j = $this->gisa_get_count_detail_nik($nik);
			if($j > 0){
				$this->gisa_insert_hist_gisa($nik,$akta_lhr,$no_akta_lhr,$akta_kwn,$no_akta_kwn,$akta_crai,$no_akta_crai,$user_name);
				$this->gisa_update_gisa($nik,$akta_lhr,$no_akta_lhr,$akta_kwn,$no_akta_kwn,$akta_crai,$no_akta_crai);
				$data["success"] = TRUE;
				$data["is_save"] = 0;
        		$data["message"] = "Data Berhasil Di Update";
        		echo json_encode($data);
			}else{
				$data["success"] = TRUE;
				$data["is_save"] = 1;
        		$data["message"] = "Data Tidak Ditemukan";
        		echo json_encode($data);
			}
		}else{
			redirect('/','refresh');
		}
	}
	public function gisa_get_kk($no_kec = 0,$no_kel =0,$no_rw = 0,$no_rt = 0){
			$sql = "SELECT A.NO_KK, A.NAMA_LENGKAP AS NAMA_KEP, A.ALAMAT, A.NO_RW NO_RW,A.NO_RT NO_RT, A.NAMA_KEC, A.NAMA_KEL FROM KAMPUNG_GISA_DKB A WHERE A.NO_KEC = $no_kec AND A.NO_KEL =$no_kel AND A.NO_RW = $no_rw AND A.NO_RT = $no_rt AND A.HUBUNGAN_DLM_KELUARGA = 1 ORDER BY A.NAMA_LENGKAP";
			$r = DB::select($sql);
			return $r;
		}
		public function gisa_get_count_kk($no_kec = 0,$no_kel =0,$no_rw = 0,$no_rt = 0){
			$sql = "SELECT COUNT(1) AS JML FROM KAMPUNG_GISA_DKB A WHERE A.NO_KEC = $no_kec AND A.NO_KEL =$no_kel AND A.NO_RW = $no_rw AND A.NO_RT = $no_rt AND A.HUBUNGAN_DLM_KELUARGA = 1";
			$r = DB::select($sql);
			return $r[0]->jml;
		}
		public function gisa_export_gisa($no_kk = ''){
			$sql = "";
			$sql .= "SELECT A.NO_KK, A.NAMA_LENGKAP AS NAMA_KEP, A.ALAMAT, A.RW NO_RW,A.RT NO_RT, A.NAMA_KEC,A.STATUS_KAWIN AS STAT_KWN, A.NAMA_KEL FROM KAMPUNG_GISA_DKB A WHERE A.HUBUNGAN_DLM_KELUARGA = 1 ";
			if($no_kk != '' || $no_kk != null || !empty($no_kk)){ 
				$sql .= " AND A.NO_KK IN ($no_kk) ";
			} 
			$sql .= " ORDER BY A.NAMA_LENGKAP ";
			$r = DB::select($sql);
			return $r;
		}
		public function gisa_get_count_detail_kk($no_kk){
			$sql = "SELECT COUNT(1) AS JML FROM KAMPUNG_GISA_DKB A WHERE A.NO_KK = $no_kk";
			$r = DB::select($sql);
			return $r[0]->jml;
		}
		public function gisa_get_detail_kk($no_kk){
			$sql = "SELECT 
					  A.NO_KK
					  , A.ALAMAT
					  , A.NO_RT AS NO_RT 
					  , A.NO_RW AS NO_RW
					  , A.NO_KEC 
					  , A.NAMA_KEC 
					  , A.NO_KEL 
					  , A.NAMA_KEL
					  , 73 AS NO_KAB 
					  , 'KOTA BANDUNG' AS NAMA_KAB 
					  , 32 AS NO_PROP 
					  , 'JAWA BARAT' AS NAMA_PROP
					  FROM KAMPUNG_GISA_DKB A WHERE
					 A.NO_KK = $no_kk AND ROWNUM =1";
			$r = DB::select($sql);
			return $r;
		}
		public function gisa_get_detail_anggota_kk($no_kk){
			$sql = "SELECT A.NIK
				  , A.NO_KK
				  , A.NAMA_LENGKAP NAMA_LGKP
          		  , A.JENIS_KELAMIN JENIS_KLMIN
          		  , A.TEMPAT_LAHIR TMPT_LHR
          		  , A.TANGGAL_LAHIR TGL_LHR
          		  , A.UMUR
          		  , A.HUBUNGAN_KELUARGA_DESC STAT_HBKEL
          		  , A.AGAMA
          		  , CASE WHEN A.STATUS_PERKAWINAN = 2 AND A.STATUS_AKTA_KWN = 'TERCATAT' THEN 'KAWIN TERCATAT' WHEN A.STATUS_PERKAWINAN = 2 AND A.STATUS_AKTA_KWN = 'TIDAK TERCATAT' THEN 'KAWIN BELUM TERCATAT' ELSE A.STATUS_PERKAWINAN_DESC END AS STATUS_KAWIN
          		  , A.GOL_DRH
          		  , A.JENIS_PEKERJAAN PEKERJAAN
          		  , A.STATUS_KK
          		  , A.STATUS_KIA
          		  , A.STATUS_KTP
          		  , A.STATUS_REKAM
          		  , A.STATUS_AKTA_LHR
          		  , A.STATUS_AKTA_KWN
            	  , A.STATUS_AKTA_CRAI
          		  , A.NO_AKTA_LHR
          		  , A.NO_AKTA_KWN
          		  , TO_CHAR(A.TGL_KWN,'DD/MM/YYYY') AS TGL_KWN
          		  , A.NO_AKTA_CRAI
          		  , TO_CHAR(A.TGL_CRAI,'DD/MM/YYYY') AS TGL_CRAI
          		  FROM KAMPUNG_GISA_DKB A 
				 WHERE 
				 A.NO_KK = $no_kk
				ORDER BY A.NO_KEC, A.NO_KEL, A.NO_KK, A.HUBUNGAN_DLM_KELUARGA";
			$r = DB::select($sql);
			return $r;
		}
		public function gisa_get_count_detail_nik($nik){
			$sql = "SELECT COUNT(1) AS JML FROM KAMPUNG_GISA_DKB A WHERE A.NIK = $nik";
			$r = DB::select($sql);
			return $r[0]->jml;
		}
		public function gisa_get_detail_nik($nik){
			$sql = "SELECT 
					  A.NIK
					  , A.NO_KK
					  , A.NAMA_LENGKAP NAMA_LGKP
          		      , A.JENIS_KELAMIN JENIS_KLMIN
          		      , A.TEMPAT_LAHIR TMPT_LHR
          		  	  , A.TANGGAL_LAHIR TGL_LHR
          		  	  , A.UMUR
          		  	  , CASE WHEN A.STATUS_PERKAWINAN = 2 AND A.STATUS_AKTA_KWN = 'TERCATAT' THEN 'KAWIN TERCATAT' WHEN A.STATUS_PERKAWINAN = 2 AND A.STATUS_AKTA_KWN = 'TIDAK TERCATAT' THEN 'KAWIN BELUM TERCATAT' ELSE A.STATUS_PERKAWINAN_DESC END AS STATUS_KAWIN
          		  	  , A.STATUS_AKTA_LHR
          		  	  , A.STATUS_AKTA_KWN
            	  	  , A.STATUS_AKTA_CRAI
          		  	  , A.NO_AKTA_LHR
          		  	  , A.NO_AKTA_KWN
          		  	  , TO_CHAR(A.TGL_KWN,'DD/MM/YYYY') AS TGL_KWN
          		  	  , A.NO_AKTA_CRAI
          		  	  , TO_CHAR(A.TGL_CRAI,'DD/MM/YYYY') AS TGL_CRAI
					  FROM KAMPUNG_GISA_DKB A WHERE
					 A.NIK = $nik AND ROWNUM =1";
			$r = DB::select($sql);
			return $r;
		}
		public function gisa_insert_hist_gisa($nik,$akta_lhr,$no_akta_lhr,$akta_kwn,$no_akta_kwn,$akta_crai,$no_akta_crai,$user_name){
			$sql = "INSERT INTO KAMPUNG_GISA_HIST (ID,NIK,STATUS_AKTA_LHR,NO_AKTA_LHR,STATUS_AKTA_KWN,NO_AKTA_KWN,STATUS_AKTA_CRAI,NO_AKTA_CERAI,CREATED_BY,CREATED_DT) VALUES ('$nik-GISA-".time()."',$nik,'$akta_lhr','$no_akta_lhr','$akta_kwn','$no_akta_kwn','$akta_crai','$no_akta_crai','$user_name',SYSDATE)";
			$r = DB::insert($sql);
			return $q;
		}
		Public function gisa_update_gisa($nik,$akta_lhr,$no_akta_lhr,$akta_kwn,$no_akta_kwn,$akta_crai,$no_akta_crai){
			$sql = "UPDATE KAMPUNG_GISA_DKB SET STATUS_AKTA_LHR = '$akta_lhr', NO_AKTA_LHR = '$no_akta_lhr', STATUS_AKTA_KWN = '$akta_kwn', NO_AKTA_KWN = '$no_akta_kwn', STATUS_AKTA_CRAI = '$akta_crai', NO_AKTA_CRAI = '$no_akta_crai' WHERE NIK = $nik";
			$r = DB::update($sql);
			return $q;
		}
}
