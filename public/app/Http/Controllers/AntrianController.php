<?php
namespace App\Http\Controllers;

use DB;
use Session;
use DateTime;
use PDF;
use Illuminate\Http\Request;
use App\Http\Controllers\SharedController as Shr;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class AntrianController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function list_antrian(Request $request)
    {
        Shr::check_login();
        if (Session::get('S_USER_ID') != null)
        {
            $menu_id = 106;
            $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL') , $menu_id);
            if ($is_akses == 0)
            {
                return redirect()->route('404Notfound');
            }
            $menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
            $isakses_kec = Shr::get_give_kec();
            $isakses_kel = Shr::get_give_kel();
            $data = array(
                "stitle" => 'Antrian SMS',
                "mtitle" => 'Antrian SMS',
                "my_url" => 'list_antrian',
                "type_tgl" => 'Tanggal',
                "menu" => $menu,
                "akses_kec" => $isakses_kec,
                "akses_kel" => $isakses_kel,
                "user_id" => $request->session()
                    ->get('S_USER_ID') ,
                "user_nik" => $request->session()
                    ->get('S_NIK') ,
                "user_nama_lgkp" => $request->session()
                    ->get('S_NAMA_LGKP') ,
                "user_nama_dpn" => $request->session()
                    ->get('S_NAMA_DPN') ,
                "user_no_kel" => $request->session()
                    ->get('S_NO_KEL') ,
                "user_no_kec" => $request->session()
                    ->get('S_NO_KEC') ,
                "user_level" => $request->session()
                    ->get('S_USER_LEVEL') ,
            );
            return view('Antrian/main', $data);
        }
        else
        {
            return redirect()->route('logout');
        }
    }

    public function cek_antrian(Request $request)
    {
        Shr::check_login();
        if (Session::get('S_USER_ID') != null)
        {
            $menu_id = 107;
            $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL') , $menu_id);
            if ($is_akses == 0)
            {
                return redirect()->route('404Notfound');
            }
            $menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
            $isakses_kec = Shr::get_give_kec();
            $isakses_kel = Shr::get_give_kel();
            $data = array(
                "stitle" => 'Cek Antrian SMS',
                "mtitle" => 'Cek Antrian SMS',
                "my_url" => 'cek_antrian',
                "type_tgl" => 'Tanggal',
                "menu" => $menu,
                "akses_kec" => $isakses_kec,
                "akses_kel" => $isakses_kel,
                "user_id" => $request->session()
                    ->get('S_USER_ID') ,
                "user_nik" => $request->session()
                    ->get('S_NIK') ,
                "user_nama_lgkp" => $request->session()
                    ->get('S_NAMA_LGKP') ,
                "user_nama_dpn" => $request->session()
                    ->get('S_NAMA_DPN') ,
                "user_no_kel" => $request->session()
                    ->get('S_NO_KEL') ,
                "user_no_kec" => $request->session()
                    ->get('S_NO_KEC') ,
                "user_level" => $request->session()
                    ->get('S_USER_LEVEL') ,
            );
            return view('Antrian_cek/main', $data);
        }
        else
        {
            return redirect()->route('logout');
        }
    }
    public function cek_antrian_log(Request $request)
    {
        Shr::check_login();
        if (Session::get('S_USER_ID') != null)
        {
            $menu_id = 108;
            $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL') , $menu_id);
            if ($is_akses == 0)
            {
                return redirect()->route('404Notfound');
            }
            $menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
            $isakses_kec = Shr::get_give_kec();
            $isakses_kel = Shr::get_give_kel();
            $data = array(
                "stitle" => 'Cek Log Antrian SMS',
                "mtitle" => 'Cek Log Antrian SMS',
                "my_url" => 'cek_antrian_log',
                "type_tgl" => 'Tanggal',
                "menu" => $menu,
                "akses_kec" => $isakses_kec,
                "akses_kel" => $isakses_kel,
                "user_id" => $request->session()
                    ->get('S_USER_ID') ,
                "user_nik" => $request->session()
                    ->get('S_NIK') ,
                "user_nama_lgkp" => $request->session()
                    ->get('S_NAMA_LGKP') ,
                "user_nama_dpn" => $request->session()
                    ->get('S_NAMA_DPN') ,
                "user_no_kel" => $request->session()
                    ->get('S_NO_KEL') ,
                "user_no_kec" => $request->session()
                    ->get('S_NO_KEC') ,
                "user_level" => $request->session()
                    ->get('S_USER_LEVEL') ,
            );
            return view('Antrian_ceklog/main', $data);
        }
        else
        {
            return redirect()->route('logout');
        }
    }
    public function cek_nohp(Request $request)
    {
        Shr::check_login();
        if (Session::get('S_USER_ID') != null)
        {
            $menu_id = 109;
            $is_akses = Shr::cek_is_akses($request->session()
                ->get('S_USER_LEVEL') , $menu_id);
            if ($is_akses == 0)
            {
                redirect('404Notfound', 'refresh');
            }
            $menu = Shr::get_menu($request->session()
                ->get('S_USER_LEVEL'));
            $isakses_kec = Shr::get_give_kec();
            $isakses_kel = Shr::get_give_kel();
            $data = array(
                "stitle" => 'Cek Nomer HP',
                "mtitle" => 'Cek Nomer HP',
                "my_url" => 'cek_nohp',
                "type_tgl" => 'Tanggal',
                "menu" => $menu,
                "akses_kec" => $isakses_kec,
                "akses_kel" => $isakses_kel,
                "user_id" => $request->session()
                    ->get('S_USER_ID') ,
                "user_nik" => $request->session()
                    ->get('S_NIK') ,
                "user_nama_lgkp" => $request->session()
                    ->get('S_NAMA_LGKP') ,
                "user_nama_dpn" => $request->session()
                    ->get('S_NAMA_DPN') ,
                "user_no_kel" => $request->session()
                    ->get('S_NO_KEL') ,
                "user_no_kec" => $request->session()
                    ->get('S_NO_KEC') ,
                "user_level" => $request->session()
                    ->get('S_USER_LEVEL') ,
            );
            return view('Antrian_nohp/main', $data);
        }
        else
        {
            return redirect()->route('logout');
        }
    }
    public function cek_kepakta(Request $request)
    {
        Shr::check_login();
        if (Session::get('S_USER_ID') != null)
        {
            $menu_id = 110;
            $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL') , $menu_id);
            if ($is_akses == 0)
            {
                return redirect()->route('404Notfound');
            }
            $menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
            $isakses_kec = Shr::get_give_kec();
            $isakses_kel = Shr::get_give_kel();
            $data = array(
                "stitle" => 'Cek Nomor Akta Kelahiran Invalid',
                "mtitle" => 'Cek Nomor Akta Kelahiran Invalid',
                "my_url" => 'cek_kepakta',
                "type_tgl" => 'Tanggal',
                "menu" => $menu,
                "akses_kec" => $isakses_kec,
                "akses_kel" => $isakses_kel,
                "user_id" => $request->session()
                    ->get('S_USER_ID') ,
                "user_nik" => $request->session()
                    ->get('S_NIK') ,
                "user_nama_lgkp" => $request->session()
                    ->get('S_NAMA_LGKP') ,
                "user_nama_dpn" => $request->session()
                    ->get('S_NAMA_DPN') ,
                "user_no_kel" => $request->session()
                    ->get('S_NO_KEL') ,
                "user_no_kec" => $request->session()
                    ->get('S_NO_KEC') ,
                "user_level" => $request->session()
                    ->get('S_USER_LEVEL') ,
            );
            return view('Antrian_akta/main', $data);
        }
        else
        {
            return redirect()->route('logout');
        }
    }
    public function insert_akta(Request $request)
    {
        Shr::check_login();
        if (Session::get('S_USER_ID') != null)
        {
            $menu_id = 179;
            $is_akses = Shr::cek_is_akses($request->session()
                ->get('S_USER_LEVEL') , $menu_id);
            if ($is_akses == 0)
            {
                redirect('404Notfound', 'refresh');
            }
            $menu = Shr::get_menu($request->session()
                ->get('S_USER_LEVEL'));
            $isakses_kec = Shr::get_give_kec();
            $isakses_kel = Shr::get_give_kel();
            $data = array(
                "stitle" => 'Insert Akta Kelahiran Smsdb',
                "mtitle" => 'Insert Akta Kelahiran Smsdb',
                "my_url" => 'insert_akta',
                "type_tgl" => 'Tanggal',
                "menu" => $menu,
                "akses_kec" => $isakses_kec,
                "akses_kel" => $isakses_kel,
                "user_id" => $request->session()
                    ->get('S_USER_ID') ,
                "user_nik" => $request->session()
                    ->get('S_NIK') ,
                "user_nama_lgkp" => $request->session()
                    ->get('S_NAMA_LGKP') ,
                "user_nama_dpn" => $request->session()
                    ->get('S_NAMA_DPN') ,
                "user_no_kel" => $request->session()
                    ->get('S_NO_KEL') ,
                "user_no_kec" => $request->session()
                    ->get('S_NO_KEC') ,
                "user_level" => $request->session()
                    ->get('S_USER_LEVEL') ,
            );
            return view('Antrian_akta_insert/main', $data);
        }
        else
        {
            return redirect()->route('logout');
        }
    }
    public function insert_biodata(Request $request)
    {
        Shr::check_login();
        if (Session::get('S_USER_ID') != null)
        {
            $menu_id = 180;
            $is_akses = Shr::cek_is_akses($request->session()
                ->get('S_USER_LEVEL') , $menu_id);
            if ($is_akses == 0)
            {
                redirect('404Notfound', 'refresh');
            }
            $menu = Shr::get_menu($request->session()
                ->get('S_USER_LEVEL'));
            $isakses_kec = Shr::get_give_kec();
            $isakses_kel = Shr::get_give_kel();
            $data = array(
                "stitle" => 'Insert Biodata Smsdb',
                "mtitle" => 'Insert Biodata Smsdb',
                "my_url" => 'insert_biodata',
                "type_tgl" => 'Tanggal',
                "menu" => $menu,
                "akses_kec" => $isakses_kec,
                "akses_kel" => $isakses_kel,
                "user_id" => $request->session()
                    ->get('S_USER_ID') ,
                "user_nik" => $request->session()
                    ->get('S_NIK') ,
                "user_nama_lgkp" => $request->session()
                    ->get('S_NAMA_LGKP') ,
                "user_nama_dpn" => $request->session()
                    ->get('S_NAMA_DPN') ,
                "user_no_kel" => $request->session()
                    ->get('S_NO_KEL') ,
                "user_no_kec" => $request->session()
                    ->get('S_NO_KEC') ,
                "user_level" => $request->session()
                    ->get('S_USER_LEVEL') ,
            );
            return view('Antrian_biodata/main', $data);
        }
        else
        {
            return redirect()->route('logout');
        }
    }
    public function get_loket(Request $request)
    {
        header("Content-Type: application/json", true);
        $r = $this->ant_get_loket();
        return $r;
    }

    public function get_antrian_today(Request $request)
    {
        header("Content-Type: application/json", true);
        $loket = $request->loket;
        $r = $this->ant_get_antrian_today($loket);
        $response["jumlah"] = $r[0]->jml;
        return $response;
    }

    public function get_antrian_tomorrow(Request $request)
    {
        header("Content-Type: application/json", true);
        $loket = $request->loket;
        $r = $this->ant_get_antrian_tomorrow($loket);
        $response["jumlah"] = $r[0]->jml;
        return $response;
    }
    public function get_antrian(Request $request)
    {
        $draw = intval($request->draw);
        $start = intval($request->start);
        $length = intval($request->length);
        $loket = $request->loket;

        $antrian = $this->ant_get_antrian($loket);

        $data = array();
        $i = 0;
        foreach ($antrian as $r)
        {
            $i++;
            $data[] = array(
                $i,
                '<span style="color: #fff">,</span>' . $r->nik,
                $r->nama,
                $r->kode_pelayanan,
                $r->urutan,
                $r->tanggal,
                $r->jam,
                $r->hari,
                $r->telepon
            );
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => count($antrian) ,
            "recordsFiltered" => count($antrian) ,
            "data" => $data
        );
        return $output;
        exit();
    }
    public function get_cek_antrian(Request $request)
    {
        $draw = intval($request->draw);
        $start = intval($request->start);
        $length = intval($request->length);
        $nik = $request->nik;
        $tlp = $request->tlp;

        $antrian = $this->ant_get_cek_antrian($nik, $tlp);

        $data = array();
        foreach ($antrian as $r)
        {
            $data[] = array(
                '<span style="color: #fff">,</span>' . $r->nik,
                $r->nama,
                $r->kode_pelayanan,
                $r->urutan,
                $r->tanggal,
                $r->jam,
                $r->hari,
                $r->telepon,
                '<button type="button" class="btn btn-danger waves-effect waves-light m-r-10" id="btn-delete" onclick="on_delete(' . $r->nik . ');">Hapus Nomer Antrian
                     <i class="mdi  mdi-delete fa-fw"></i></button>'
            );
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => count($antrian) ,
            "recordsFiltered" => count($antrian) ,
            "data" => $data
        );
        return $output;
        exit();
    }
    public function get_cek_antrianakta(Request $request)
    {
        $draw = intval($request->draw);
        $start = intval($request->start);
        $length = intval($request->length);
        if (!empty($request->nik))
        {
            $nik = (!empty($request->nik)) ? $request->nik : '';
            $antrian = $this->ant_get_cek_antrianakta($nik);
            $data = array();
            foreach ($antrian as $r)
            {
                $data[] = array(
                    '<span style="color: #fff">,</span>' . $r->nik,
                    $r->nama_lgkp,
                    $r->tmpt_lhr,
                    $r->tgl_lhr,
                    $r->status_akta,
                    $r->no_akta_lhr,
                    ($r->no_akta_lhr != '-') ? '<button type="button" class="btn btn-danger waves-effect waves-light m-r-10" id="btn-delete" onclick="on_delete(' . $r->nik . ');">Hapus Nomer Akta Invalid
                     <i class="mdi  mdi-delete fa-fw"></i></button>' : '-'
                );
            }
            $output = array(
                "draw" => $draw,
                "recordsTotal" => count($antrian) ,
                "recordsFiltered" => count($antrian) ,
                "data" => $data
            );
            return $output;
            exit();
        }
        else
        {
            $data = array();
            $output = array(
                "draw" => $draw,
                "recordsTotal" => 0,
                "recordsFiltered" => 0,
                "data" => $data
            );
            return $output;
            exit();
        }

    }
    public function get_cek_antrian_biodata(Request $request)
    {
        $draw = intval($request->draw);
        $start = intval($request->start);
        $length = intval($request->length);
        if (!empty($request->nik))
        {
            $nik = (!empty($request->nik)) ? $request->nik : '';
            $antrian = $this->ant_get_cek_antrian_biodata($nik);
            $data = array();
            foreach ($antrian as $r)
            {
                $data[] = array(
                    '<span style="color: #fff">,</span>' . $r->nik,
                    $r->nama_lgkp,
                    $r->tmpt_lhr,
                    $r->tgl_lhr,
                    $r->status_akta,
                    $r->no_akta_lhr,
                    ($r->status_data == 1) ? '<button type="button" class="btn btn-info waves-effect waves-light m-r-10" id="btn-delete" >Biodata Sudah Ada DI Smsdb
                     <i class="mdi  mdi-sync-alert fa-fw"></i></button>' : '<button type="button" class="btn btn-success waves-effect waves-light m-r-10" id="btn-delete" onclick="on_insert(' . $r->nik . ');">Tambah Biodata Wni
                     <i class="mdi  mdi-plus-circle fa-fw"></i></button>',

                );
            }
            $output = array(
                "draw" => $draw,
                "recordsTotal" => count($antrian) ,
                "recordsFiltered" => count($antrian) ,
                "data" => $data
            );
            return $output;
            exit();
        }
        else
        {
            $data = array();
            $output = array(
                "draw" => $draw,
                "recordsTotal" => 0,
                "recordsFiltered" => 0,
                "data" => $data
            );
            return $output;
            exit();
        }

    }
    public function get_cek_antrianlog(Request $request)
    {
        $draw = intval($request->draw);
        $start = intval($request->start);
        $length = intval($request->length);
        $nik = $request->nik;
        $tlp = $request->tlp;

        $antrian = $this->ant_get_cek_antrianlog($nik, $tlp);

        $data = array();
        $i = 0;
        foreach ($antrian as $r)
        {
            $i++;
            $data[] = array(
                $i,
                '<span style="color: #fff">,</span>' . $r->nik,
                $r->nama,
                $r->kode_pelayanan,
                $r->urutan,
                $r->tanggal,
                $r->jam,
                $r->hari,
                $r->telepon
            );
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => count($antrian) ,
            "recordsFiltered" => count($antrian) ,
            "data" => $data
        );
        return $output;
        exit();
    }
    public function get_cek_nohp(Request $request)
    {
        $draw = intval($request->draw);
        $start = intval($request->start);
        $length = intval($request->length);
        $tlp = $request->tlp;

        $antrian = $this->ant_get_cek_nohp($tlp);

        $data = array();
        $i = 0;
        foreach ($antrian as $r)
        {
            $i++;
            $data[] = array(
                $i,
                $r->telepon,
                $r->jumlah_sms,
                $r->status_blokir,
                $r->tanggal_blokir,
                '<button type="button" class="btn btn-danger waves-effect waves-light m-r-10" id="btn-delete" onclick="on_delete(' . $r->telepon . ');">Hapus
                     <i class="mdi  mdi-delete fa-fw"></i></button>'
            );
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => count($antrian) ,
            "recordsFiltered" => count($antrian) ,
            "data" => $data
        );
        return $output;
        exit();
    }
    public function do_delete_phone(Request $request)
    {
        if ($request->tlp != null)
        {
            $tlp = '+' . $request->tlp;
            $antrian = $this->ant_get_cek_nohp_block($tlp);
            if (count($antrian) > 0)
            {
                $j = $this->ant_check_calo($tlp);
                if (count($antrian) > 0)
                {
                    $data["success"] = true;
                    $data["is_save"] = 1;
                    $data["message"] = "Nomor : " . $tlp . " Nomor Telepon Sudah Di Block Permanen Tidak Dapat Dihapus, Contact Admin E-Spasi";
                    return $data;
                }
                else
                {
                    $this->ant_delete_nohp($tlp);
                    $data["success"] = true;
                    $data["is_save"] = 0;
                    $data["message"] = "Nomor : " . $tlp . " Berhasil Di Reset Dari Daftar Blokir";
                    return $data;
                }
            }
            else
            {
                $data["success"] = true;
                $data["is_save"] = 1;
                $data["message"] = "Nomor : " . $tlp . " Tidak Di Temukan Mohon Cek Kembali";
                return $data;
            }
        }
        else
        {
            redirect('/', 'refresh');
        }
    }
    public function do_delete_antrian(Request $request)
    {
        if ($request->nik != null)
        {
            $nik = $request->nik;
            $antrian = $this->ant_get_cek_antrian_invalid($nik);
            if (count($antrian) > 0)
            {
                $this->ant_delete_antrian($nik);
                $data["success"] = true;
                $data["is_save"] = 0;
                $data["message"] = "Antrian Dari Nik : " . $nik . " Berhasil Di Hapus";
                return $data;
            }
            else
            {
                $data["success"] = true;
                $data["is_save"] = 1;
                $data["message"] = "Antrian Dari Nik : " . $nik . " Tidak Dapat Di Hapus";
                return $data;
            }
        }
        else
        {
            redirect('/', 'refresh');
        }
    }
    public function do_delete_akta(Request $request)
    {
        if ($request->nik != null)
        {
            $nik = $request->nik;
            $antrian = $this->ant_get_cek_antrianakta_invalid($nik);
            if (count($antrian) > 0)
            {
                $nik_akta = $antrian[0]->nik;
                $akta_lhr = $antrian[0]->akta_lhr;
                $no_akta_lhr = $antrian[0]->no_akta_lhr;
                $this->ant_delete_akta($nik_akta, $akta_lhr, $no_akta_lhr, $request->session()
                    ->get('S_USER_ID') , $request->ip());
                $data["success"] = true;
                $data["is_save"] = 0;
                $data["message"] = "No Akta Lahir Dari Nik : " . $nik . " Berhasil Di Ubah";
                return $data;
            }
            else
            {
                $data["success"] = true;
                $data["is_save"] = 1;
                $data["message"] = "Mohon Maaf Nik : " . $nik . " Tidak Memiliki Nomor Akta Invalid, Silahkan Cek SIAK";
                return $data;
            }
        }
        else
        {
            redirect('/', 'refresh');
        }
    }
    public function do_insert_biodata(Request $request)
    {
        if ($request->nik != null)
        {
            $nik = $request->nik;
            $antrian = $this->ant_get_cek_insert_biodata($nik);
            if (count($antrian) > 0)
            {
                $nik_bio = $antrian[0]->nik;
                $this->ant_insert_nik_biodata($nik_bio);
                // $this->ant_delete_akta($nik);
                $data["success"] = true;
                $data["is_save"] = 0;
                $data["message"] = "Nik : " . $nik . " Berhasil Di Masukan Ke Smsdb";
                return $data;
            }
            else
            {
                $data["success"] = true;
                $data["is_save"] = 1;
                $data["message"] = "Mohon Maaf Nik : " . $nik . " Sudah Ada Di Smsdb";
                return $data;
            }
        }
        else
        {
            redirect('/', 'refresh');
        }
    }
    public function edit_akta_sms(Request $request)
    {
        if ($request->nik != null)
        {
            $nik = $request->nik;
            $akta_lhr = $request->akta_lhr;
            $no_akta_lhr = $request->no_akta_lhr;
            $antrian = $this->ant_get_cek_antrianakta_not_invalid($nik);
            if (count($antrian) > 0)
            {
                $nik_akta = $antrian[0]->nik;
                $this->ant_update_akta($nik_akta, $akta_lhr, $no_akta_lhr, $request->session()
                    ->get('S_USER_ID') , $request->ip());
                $data["success"] = true;
                $data["is_save"] = 0;
                $data["message"] = "No Akta Lahir Dari Nik : " . $nik . " Berhasil Di Ubah";
                return $data;
            }
            else
            {
                $data["success"] = true;
                $data["is_save"] = 1;
                $data["message"] = "Mohon Maaf Nik : " . $nik . " Tidak Memiliki Nomor Akta Invalid, Silahkan Cek SIAK";
                return $data;
            }
        }
        else
        {
            redirect('/', 'refresh');
        }
    }
    public function get_edit_antrianakta(Request $request)
    {
        $draw = intval($request->draw);
        $start = intval($request->start);
        $length = intval($request->length);
        if (!empty($request->nik))
        {
            $nik = (!empty($request->nik)) ? $request->nik : '';
            $antrian = $this->ant_get_cek_antrianakta($nik);
            $data = array();
            foreach ($antrian as $r)
            {
                $data[] = array(
                    '<span style="color: #fff">,</span>' . $r->nik,
                    $r->nama_lgkp,
                    $r->tmpt_lhr,
                    $r->tgl_lhr,
                    $r->status_akta,
                    $r->no_akta_lhr,
                    ($r->status_akta == 'ADA') ? '<button type="button" class="btn btn-info waves-effect waves-light m-r-10" id="btn-delete" >No Akta Sudah Ada
                     <i class="mdi  mdi-sync-alert fa-fw"></i></button>' : '<button type="button" class="btn btn-success waves-effect waves-light m-r-10" id="btn-delete" onclick="on_update(' . $r->nik . ');">Tambah No Akta Lahirs
                     <i class="mdi  mdi-plus-circle fa-fw"></i></button>'
                );
            }
            $output = array(
                "draw" => $draw,
                "recordsTotal" => count($antrian) ,
                "recordsFiltered" => count($antrian) ,
                "data" => $data
            );
            return $output;
            exit();
        }
        else
        {
            $data = array();
            $output = array(
                "draw" => $draw,
                "recordsTotal" => 0,
                "recordsFiltered" => 0,
                "data" => $data
            );
            return $output;
            exit();
        }

    }
    public function ant_get_loket()
    {
        $sql = "SELECT DISTINCT(KODE_PELAYANAN) KODE_PELAYANAN  FROM SMS_PANGGILAN ORDER BY KODE_PELAYANAN";
        $r = DB::connection('smsdb')->select($sql);
        return $r;
    }
    public function ant_get_antrian($loket)
    {
        $sql = "SELECT NIK, NAMA, KODE_PELAYANAN, URUTAN, TO_CHAR(TGL_PELAYANAN,'DD-MM-YYYY') AS TANGGAL, JAM, HARI, TELEPON FROM ANTRIAN WHERE URUTAN LIKE '$loket%' AND LOKASI = 'DISDUK' AND TRUNC(TGL_PELAYANAN) = TRUNC(SYSDATE) ORDER BY TGL_PELAYANAN ASC";
        $r = DB::connection('smsdb')->select($sql);
        return $r;
    }
    public function ant_get_cek_antrian($nik, $tlp)
    {
        $sql = "SELECT NIK, NAMA, KODE_PELAYANAN, URUTAN, TO_CHAR(TGL_PELAYANAN,'DD-MM-YYYY') AS TANGGAL, JAM, HARI, TELEPON FROM (SELECT A.*, ROW_NUMBER() OVER (ORDER BY TGL_PELAYANAN ASC) AS SEQNUM FROM ANTRIAN A WHERE NIK = '$nik' OR TELEPON = '$tlp') RIL WHERE SEQNUM = 1";
        $r = DB::connection('smsdb')->select($sql);
        return $r;
    }
    public function ant_get_cek_antrianlog($nik, $tlp)
    {
        $sql = "SELECT NIK, NAMA, KODE_PELAYANAN, URUTAN, TO_CHAR(TGL_PELAYANAN,'DD-MM-YYYY') AS TANGGAL, JAM, HARI, TELEPON FROM ANTRIAN_LOG WHERE NIK = '$nik' OR TELEPON = '$tlp' ORDER BY TGL_PELAYANAN DESC";
        $r = DB::connection('smsdb')->select($sql);
        return $r;
    }
    public function ant_get_cek_nohp($tlp)
    {
        $sql = "SELECT TELEPON, JUMLAH_SMS, STATUS_BLOKIR, TO_CHAR(TANGGAL_BLOKIR,'DD-MM-YYYY') TANGGAL_BLOKIR FROM KONTAK WHERE TELEPON = '$tlp'";
        $r = DB::connection('smsdb')->select($sql);
        return $r;
    }
    public function ant_get_cek_antrian_biodata($nik)
    {
        $sql = "SELECT A.NIK, A.NAMA_LGKP, A.TMPT_LHR, TO_CHAR(A.TGL_LHR,'DD-MM-YYYY') TGL_LHR, CASE WHEN A.AKTA_LHR = 2 THEN 'ADA' ELSE 'TIDAK ADA' END AS STATUS_AKTA,CASE WHEN A.AKTA_LHR = 2 THEN A.NO_AKTA_LHR ELSE '-' END AS NO_AKTA_LHR, CASE WHEN B.NIK IS NULL THEN 0 ELSE 1 END STATUS_DATA FROM BIODATA_WNI@DB222 A LEFT JOIN BIODATA_WNI B ON A.NIK = B.NIK WHERE A.NIK = $nik";
        $r = DB::connection('smsdb')->select($sql);
        return $r;
    }
    public function ant_get_cek_antrianakta($nik)
    {
        $sql = "SELECT NIK, NAMA_LGKP, TMPT_LHR, TO_CHAR(TGL_LHR,'DD-MM-YYYY') TGL_LHR, CASE WHEN AKTA_LHR = 2 THEN 'ADA' ELSE 'TIDAK ADA' END AS STATUS_AKTA,CASE WHEN AKTA_LHR = 2 THEN NO_AKTA_LHR ELSE '-' END AS NO_AKTA_LHR FROM BIODATA_WNI WHERE NIK = $nik";
        $r = DB::connection('smsdb')->select($sql);
        return $r;
    }
    public function ant_get_cek_antrian_invalid($nik)
    {
        $sql = "SELECT NIK, NAMA, KODE_PELAYANAN, URUTAN, TO_CHAR(TGL_PELAYANAN,'DD-MM-YYYY') AS TANGGAL, JAM, HARI, TELEPON FROM ANTRIAN A WHERE NIK = '$nik'";
        $r = DB::connection('smsdb')->select($sql);
        return $r;
    }
    public function ant_get_cek_antrianakta_invalid($nik)
    {
        $sql = "SELECT A.NIK, A.AKTA_LHR, A.NO_AKTA_LHR FROM BIODATA_WNI A WHERE A.NIK = $nik AND A.NO_AKTA_LHR IS NOT NULL AND A.AKTA_LHR = 2 AND NOT EXISTS (SELECT 1 FROM CAPIL_LAHIR@DB222 B WHERE TO_CHAR(A.NIK) = TO_CHAR(B.BAYI_NIK)) AND EXISTS (SELECT 1 FROM BIODATA_WNI@DB222 B WHERE A.NIK = B.NIK)";
        $r = DB::connection('smsdb')->select($sql);
        return $r;
    }
    public function ant_get_cek_antrianakta_not_invalid($nik)
    {
        $sql = "SELECT A.NIK, A.AKTA_LHR, A.NO_AKTA_LHR FROM BIODATA_WNI A WHERE A.NIK = $nik  ";
        $r = DB::connection('smsdb')->select($sql);
        return $r;
    }
    public function ant_get_cek_insert_biodata($nik)
    {
        $sql = "SELECT A.NIK FROM BIODATA_WNI@DB222 A LEFT JOIN BIODATA_WNI B ON A.NIK = B.NIK WHERE B.NIK IS NULL AND A.NIK = $nik";
        $r = DB::connection('smsdb')->select($sql);
        return $r;
    }
    public function ant_get_cek_nohp_block($tlp)
    {
        $sql = "SELECT TELEPON, JUMLAH_SMS, STATUS_BLOKIR, TO_CHAR(TANGGAL_BLOKIR,'DD-MM-YYYY') TANGGAL_BLOKIR FROM KONTAK WHERE TELEPON = '$tlp'";
        $r = DB::connection('smsdb')->select($sql);
        return $r;
    }
    public function ant_check_calo($tlp)
    {
        $sql = "SELECT COUNT(1) JML FROM KONTAK WHERE TELEPON = '$tlp' AND TANGGAL_BLOKIR >= TRUNC(SYSDATE+30)";
        $r = DB::connection('smsdb')->select($sql);
        return $r;
    }
    public function ant_delete_nohp($tlp)
    {
        $sql = "DELETE FROM KONTAK WHERE TELEPON = '$tlp' AND TANGGAL_BLOKIR < TRUNC(SYSDATE+30)";
        $r = DB::connection('smsdb')->delete($sql);
        return $r;
    }
    public function ant_insert_nik_biodata($nik)
    {
        $sql = "INSERT INTO BIODATA_WNI SELECT * FROM BIODATA_WNI@DB222 WHERE NIK = $nik";
        $r = DB::connection('smsdb')->insert($sql);
        return $r;
    }
    public function ant_delete_antrian($nik)
    {
        $sql = "DELETE FROM ANTRIAN WHERE NIK = '$nik'";
        $r = DB::connection('smsdb')->delete($sql);
        return $r;
    }
    public function ant_delete_akta($nik_akta, $akta_lhr, $no_akta_lhr, $user_id, $ip_address)
    {
        $sql = "UPDATE BIODATA_WNI SET AKTA_LHR = 1, NO_AKTA_LHR = NULL WHERE NIK = $nik_akta";
        $r = DB::connection('smsdb')->update($sql);
        $sql = "UPDATE BIODATA_WNI SET AKTA_LHR = 1, NO_AKTA_LHR = NULL WHERE NIK = $nik_akta";
        $r = DB::connection('db222')->update($sql);
        $sql = "INSERT INTO SIAK_HIST_AKTA (ID,NIK,AKTA_LHR,NO_AKTA_LHR,UPDATE_DT,UPDATE_BY,IP_ADDRESS) VALUES ('$nik_akta-SMSAKTA-" . time() . "',$nik_akta,$akta_lhr,'$no_akta_lhr',SYSDATE,'$user_id','$ip_address')";
        $r = DB::insert($sql);
        return $r;
    }
    public function ant_update_akta($nik_akta, $akta_lhr, $no_akta_lhr, $user_id, $ip_address)
    {
        $sql = "INSERT INTO SIAK_HIST_AKTA (ID,NIK,AKTA_LHR,NO_AKTA_LHR,UPDATE_DT,UPDATE_BY,IP_ADDRESS) VALUES ('$nik_akta-UPDAKTA-" . time() . "',$nik_akta,$akta_lhr,'$no_akta_lhr',SYSDATE,'$user_id','$ip_address')";
        $r = DB::insert($sql);
        $sql = "UPDATE BIODATA_WNI SET AKTA_LHR = 2, NO_AKTA_LHR = '$no_akta_lhr' WHERE NIK = $nik_akta";
        $r = DB::connection('smsdb')->update($sql);
        $sql = "UPDATE BIODATA_WNI SET AKTA_LHR = 2, NO_AKTA_LHR = '$no_akta_lhr' WHERE NIK = $nik_akta";
        $r = DB::connection('db222')->update($sql);
        return $r;
    }
    public function ant_get_antrian_today($loket)
    {
        $sql = "SELECT COUNT(1) AS JML FROM ANTRIAN WHERE URUTAN LIKE '$loket%' AND TO_CHAR(TGL_PELAYANAN,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') AND LOKASI = 'DISDUK'";
        $r = DB::connection('smsdb')->select($sql);
        return $r;
    }
    public function ant_get_antrian_tomorrow($loket)
    {
        $sql = "SELECT COUNT(1) AS JML FROM ANTRIAN WHERE URUTAN LIKE '$loket%' AND TO_CHAR(TGL_PELAYANAN,'DD/MM/YYYY') = TO_CHAR(SYSDATE+1,'DD/MM/YYYY') AND LOKASI = 'DISDUK'";
        $r = DB::connection('smsdb')->select($sql);
        return $r;
    }
}

