<?php

namespace App\Http\Controllers;

use DB;
use Session;
use DateTime;
use PDF;
use Illuminate\Http\Request;
use App\Http\Controllers\SharedController as Shr;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class SharedController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public static function index(Request $request){
    	   return redirect()->route('logout');
    }

    public static function validatedatetime($date, $format = 'd-m-Y')
    {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) === $date;
    }
    public static function get_menu($level = 0){
        $r = DB::select("SELECT A.MENU_ID,A.PARENT_ID, A.ICON,A.TITLE,A.URL,A.IS_ACTIVE, A.USER_LEVEL, A.MENU_LEVEL FROM VW_SIAK_MENU A  WHERE A.USER_LEVEL = $level AND A.MENU_LEVEL = 1 AND A.ACTIVE_MENU = 1 ORDER BY A.LEVEL_HEAD,A.LEVEL_SUB,A.LEVEL_CONTENT, A.TITLE");
            $i=0;
            foreach($r as $row){
                $r[$i]->sub_menu = Shr::get_sub_menu($row->menu_id,$level);
                $i++;
            }
        return $r;
    }

    public static function get_sub_menu($parent_id = 0,$level = 0){
        $r = DB::select("SELECT A.MENU_ID,A.PARENT_ID, A.ICON,A.TITLE,A.URL,A.IS_ACTIVE, A.USER_LEVEL, A.MENU_LEVEL FROM VW_SIAK_MENU A  WHERE A.USER_LEVEL = $level AND A.PARENT_ID = $parent_id AND A.ACTIVE_MENU = 1 ORDER BY A.LEVEL_HEAD,A.LEVEL_SUB,A.LEVEL_CONTENT, A.TITLE");
            $i=0;
            foreach($r as $row){
                if($row->is_active != 1){
                $r[$i]->sub_menu = Shr::get_sub_menu($row->menu_id,$level);
                }
                $i++;
            }
            return $r;
    }

    public static function check_login(){
        if (Session::get('S_USER_ID') != null) 
        {
         $user_id = Session::get('S_USER_ID');
            $ip_address = Session::get('S_IP_ADDRESS');
            $is_log = DB::select("SELECT COUNT(1) VAL FROM SIAK_SESSION_PLUS WHERE IP_ADDRESS = '$ip_address' AND USER_ID = '$user_id' AND IS_ACTIVE = 1");
            if ($is_log[0]->val == 0)
            {
                if ($user_id != null) 
                {
                    $last_activity = DB::select("SELECT COUNT(1) AS CNT FROM SIAK_SESSION_PLUS WHERE USER_ID = '$user_id'");
                    if($last_activity[0]->cnt > 0)
                    {
                      $sql = DB::update("UPDATE SIAK_SESSION_PLUS SET LAST_ACTIVITY = SYSDATE, IS_ACTIVE = 0  WHERE USER_ID = '$user_id'");
                    }
                }
                if ($user_id != 'adminduk' && $user_id != 'assist_yz'){
                Session::flush();
                return redirect()->route('logout');    
                }else{
                    $sql = DB::update("UPDATE SIAK_SESSION_PLUS SET LAST_ACTIVITY = SYSDATE, IS_ACTIVE = 1, IP_ADDRESS = '$ip_address'  WHERE USER_ID = '$user_id'");
                }
            }  
        } 
        else 
        {   
             redirect()->route('logout');
        }
    }
    public static function get_user_name($user_id){
        $sql = "SELECT NAMA_LGKP FROM SIAK_USER_PLUS WHERE USER_ID = '$user_id'";
        $r = DB::select($sql);
        return $r[0]->nama_lgkp;
    }
    public static function cek_is_akses($level = 0,$menu_id = 0){
        $sql = "SELECT IS_ACTIVE VAL FROM SIAK_AKSES WHERE MENU_ID = $menu_id AND USER_LEVEL = $level";
        $r = DB::select($sql);
        return $r[0]->val;
    }
    public static function get_give_kec(){
        $sql = "SELECT SYSTEM_VALUE_NUM AS VAL FROM SIAK_MASTER WHERE SYSTEM_TYPE = 'INIT_SETING' AND SYSTEM_CODE = 'GIVE_ALL_KEC'";
        $r = DB::select($sql);
        return $r[0]->val;
    }
    public static function get_give_kel(){
        $sql = "SELECT SYSTEM_VALUE_NUM AS VAL FROM SIAK_MASTER WHERE SYSTEM_TYPE = 'INIT_SETING' AND SYSTEM_CODE = 'GIVE_ALL_KEL'";
        $r = DB::select($sql);
        return $r[0]->val;
    }
    public static function get_kecamatan(Request $request)
    {

        header("Content-Type: application/json", true);
        $j = Shr::get_give_kec();
        if ($j > 0){
            $r = Shr::do_get_all_kecamatan(0);
        }else{
            $r = Shr::do_get_all_kecamatan(Session::get('S_NO_KEC'));
        }
        
        return $r;
    }
    public static function get_master_provinsi(Request $request)
    {
        header("Content-Type: application/json", true);
        $no_prop = $request->no_prop;
        $r = Shr::do_get_prop($no_prop);
        return $r;
    }
    public static function get_master_kabupaten(Request $request)
    {
        header("Content-Type: application/json", true);
        $no_prop = $request->no_prop;
        $no_kab = $request->no_kab;
        $r = Shr::do_get_kab($no_prop,$no_kab);
        return $r;
    }
    public static function get_master_kecamatan(Request $request)
    {
        header("Content-Type: application/json", true);
        $no_prop = $request->no_prop;
        $no_kab = $request->no_kab;
        $no_kec = $request->no_kec;
        $r = Shr::do_get_kec($no_prop,$no_kab,$no_kec);
        return $r;
    }
    public static function get_master_kelurahan(Request $request)
    {
        header("Content-Type: application/json", true);
        $no_prop = $request->no_prop;
        $no_kab = $request->no_kab;
        $no_kec = $request->no_kec;
        $no_kel = $request->no_kel;
        $r = Shr::do_get_kel($no_prop,$no_kab,$no_kec,$no_kel);
        return $r;
    }
    public static function get_webdb_provinsi(Request $request)
    {
        header("Content-Type: application/json", true);
        $no_prop = $request->no_prop;
        $r = Shr::do_getweb_prop($no_prop);
        return $r;
    }
    public static function get_webdb_kabupaten(Request $request)
    {
        header("Content-Type: application/json", true);
        $no_prop = $request->no_prop;
        $no_kab = $request->no_kab;
        $r = Shr::do_getweb_kab($no_prop,$no_kab);
        return $r;
    }
    public static function get_webdb_kecamatan(Request $request)
    {
        header("Content-Type: application/json", true);
        $no_prop = $request->no_prop;
        $no_kab = $request->no_kab;
        $no_kec = $request->no_kec;
        $r = Shr::do_getweb_kec($no_prop,$no_kab,$no_kec);
        return $r;
    }
    public static function get_webdb_kelurahan(Request $request)
    {
        header("Content-Type: application/json", true);
        $no_prop = $request->no_prop;
        $no_kab = $request->no_kab;
        $no_kec = $request->no_kec;
        $no_kel = $request->no_kel;
        $r = Shr::do_getweb_kel($no_prop,$no_kab,$no_kec,$no_kel);
        return $r;
    }

    public static function get_one_kecamatan(Request $request)
    {
        header("Content-Type: application/json", true);
        $no_kec = $request->no_kec;
        
        $j = Shr::get_give_kec();
        if ($j > 0){
            $r = Shr::do_get_one_kecamatan(0);
        }else{
            $r = Shr::do_get_one_kecamatan($no_kec);
        }
        return $r;
    }
    public static function gisa_get_one_kecamatan(Request $request)
    {
        header("Content-Type: application/json", true);
        $no_kec = $request->no_kec;
        
        $j = Shr::get_give_kec();
        if ($j > 0){
            $r = Shr::do_gisa_get_one_kecamatan(0);
        }else{
            $r = Shr::do_gisa_get_one_kecamatan($no_kec);
        }
        return $r;
    }
    public static function get_kelurahan(Request $request)
    {
        header("Content-Type: application/json", true);
        $no_kec = $request->no_kec;
        $j = Shr::get_give_kec();
        if ($j > 0){
            $j = Shr::get_give_kel();
            if ($j > 0){
                $r = Shr::do_get_all_kelurahan($no_kec,0);
            }else{
                $r = Shr::do_get_all_kelurahan($no_kec,Session::get('S_NO_KEL'));
            }
        }else{
            $j = Shr::get_give_kel();
            if ($j > 0){
                $r = Shr::do_get_all_kelurahan(Session::get('S_NO_KEC'),0);
            }else{
                $r = Shr::do_get_all_kelurahan(Session::get('S_NO_KEC'),Session::get('S_NO_KEL'));
            }
        }
        
        
        return $r;
        
    }
    public static function gisa_get_kelurahan(Request $request)
    {
        header("Content-Type: application/json", true);
        $no_kec = $request->no_kec;
        $j = Shr::get_give_kec();
        if ($j > 0){
            $j = Shr::get_give_kel();
            if ($j > 0){
                $r = Shr::do_gisa_get_all_kelurahan($no_kec,0);
            }else{
                $r = Shr::do_gisa_get_all_kelurahan($no_kec,Session::get('S_NO_KEL'));
            }
        }else{
            $j = Shr::get_give_kel();
            if ($j > 0){
                $r = Shr::do_gisa_get_all_kelurahan(Session::get('S_NO_KEC'),0);
            }else{
                $r = Shr::do_gisa_get_all_kelurahan(Session::get('S_NO_KEC'),Session::get('S_NO_KEL'));
            }
        }
        
        
        return $r;
        
    }
    public static function get_rw(Request $request)
    {
        header("Content-Type: application/json", true);
        $no_kec = $request->no_kec;
        $no_kel = $request->no_kel;
        $r = Shr::do_get_all_rw($no_kec,$no_kel,Session::get('S_NO_RW'));
        return $r;
        
    }
    public static function gisa_get_rw(Request $request)
    {
        header("Content-Type: application/json", true);
        $no_kec = $request->no_kec;
        $no_kel = $request->no_kel;
        $r = Shr::do_gisa_get_all_rw($no_kec,$no_kel,Session::get('S_NO_RW'));
        return $r;
        
    }
    public static function get_rt(Request $request)
    {
        header("Content-Type: application/json", true);
        $no_kec = $request->no_kec;
        $no_kel = $request->no_kel;
        $no_rw = $request->no_rw;
        $r = Shr::do_get_all_rt($no_kec,$no_kel,$no_rw,Session::get('S_NO_RT'));
        return $r;
        
    }
    public static function gisa_get_rt(Request $request)
    {
        header("Content-Type: application/json", true);
        $no_kec = $request->no_kec;
        $no_kel = $request->no_kel;
        $no_rw = $request->no_rw;
        $r = Shr::do_gisa_get_all_rt($no_kec,$no_kel,$no_rw,Session::get('S_NO_RT'));
        return $r;
        
    }
     public static function get_gisa_kecamatan(Request $request)
    {
        header("Content-Type: application/json", true);
        $no_kec = $request->no_kec;
        
        $j = Shr::get_give_kec();
        if ($j > 0){
            $r = Shr::do_get_gisa_kecamatan(0);
        }else{
            $r = Shr::do_get_gisa_kecamatan($no_kec);
        }
        return $r;
    }
    public static function get_gisa_kelurahan(Request $request)
    {
        header("Content-Type: application/json", true);
        $no_kec = $request->no_kec;
        $j = Shr::get_give_kec();
        if ($j > 0){
            $j = Shr::get_give_kel();
            if ($j > 0){
                $r = Shr::do_get_gisa_kelurahan($no_kec,0);
            }else{
                $r = Shr::do_get_gisa_kelurahan($no_kec,Session::get('S_NO_KEL'));
            }
        }else{
            $j = Shr::get_give_kel();
            if ($j > 0){
                $r = Shr::do_get_gisa_kelurahan(Session::get('S_NO_KEC'),0);
            }else{
                $r = Shr::do_get_gisa_kelurahan(Session::get('S_NO_KEC'),Session::get('S_NO_KEL'));
            }
        }
        
        
        return $r;
        
    }
    public static function get_gisa_rw(Request $request)
    {
        header("Content-Type: application/json", true);
        $no_kec = $request->no_kec;
        $no_kel = $request->no_kel;
        $r = Shr::do_get_gisa_rw($no_kec,$no_kel,Session::get('S_NO_RW'));
        return $r;
        
    }
    public static function get_gisa_rt(Request $request)
    {
        header("Content-Type: application/json", true);
        $no_kec = $request->no_kec;
        $no_kel = $request->no_kel;
        $no_rw = $request->no_rw;
        $r = Shr::do_get_gisa_rt($no_kec,$no_kel,$no_rw,Session::get('S_NO_RT'));
        return $r;
        
    }
    public static function get_will(Request $request)
    {
        header("Content-Type: application/json", true);
        $r = Shr::do_get_all_wil(Session::get('S_NO_KEC'));
        return $r;
    }
    public static function get_capil_blk_opt(Request $request)
    {
        header("Content-Type: application/json", true);
        $r = Shr::do_get_capil_blk_opt(Session::get('S_NO_KEC'));
        return $r;
    }
    public static function get_master_menu(Request $request)
    {
        header("Content-Type: application/json", true);
        $r = Shr::do_get_master_menu();
        return $r;
    }
    public static function get_master_dkb(Request $request)
    {
        header("Content-Type: application/json", true);
        $r = Shr::do_get_master_dkb();
        return $r;
    }
    public static function get_master_usia(Request $request)
    {
        header("Content-Type: application/json", true);
        $r = Shr::do_get_master_usia();
        return $r;
    }
    public static function get_master_cakupan(Request $request)
    {
        header("Content-Type: application/json", true);
        $r = Shr::do_get_master_cakupan();
        return $r;
    }
    public static function get_user_daily(Request $request)
    {
        header("Content-Type: application/json", true);
        $r = Shr::do_get_user_daily();
        return $r;
    }
    public static function get_user_absensi(Request $request)
    {
        header("Content-Type: application/json", true);
        $r = Shr::do_get_user_absensi();
        return $r;
    }
    public static function get_user_cpltte(Request $request)
    {
        header("Content-Type: application/json", true);
        $r = Shr::do_get_user_cpltte();
        return $r;
    }
    public static function get_default_pass(Request $request)
    {
        header("Content-Type: application/json", true);
        $r = Shr::do_get_default_pass();
        echo  json_encode($r);
    }
    public static function get_user_lhr(Request $request)
    {
        $lvl = Shr::do_get_lvl_lhr();
        header("Content-Type: application/json", true);
        $r = Shr::do_get_user_by_lvl($lvl);
        return $r;
    }
    public static function get_user_lhr_mpl(Request $request)
    {
        $lvl = Shr::do_get_lvl_lhr_mpl();
        header("Content-Type: application/json", true);
        $r = Shr::do_get_user_by_lvl($lvl);
        return $r;
    }
    public static function get_user_kmt(Request $request)
    {
        $lvl = Shr::do_get_lvl_kmt();
        header("Content-Type: application/json", true);
        $r = Shr::do_get_user_by_lvl($lvl);
        return $r;
    }
    public static function get_ref_biodata_delete(Request $request)
    {
        header("Content-Type: application/json", true);
        $r = Shr::do_get_ref_biodata_delete();
        return $r;
    }
    public static function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }



    // new

    public static function do_get_all_kecamatan($no_kec = 0){
            $sql = "";
            $sql .= "SELECT NO_KEC, NAMA_KEC FROM SETUP_KEC WHERE NO_PROP = 32 AND NO_KAB =  73 ";
            if ($no_kec != 0){
                $sql .= " AND NO_KEC = $no_kec ";
            }
            $sql .= " ORDER BY NO_KEC";
            $r = DB::select($sql);
            return $r;
        }
        public static function do_get_one_kecamatan($no_kec = 0){
            $sql ="";
            $sql .=" SELECT NO_KEC, NAMA_KEC FROM SETUP_KEC WHERE NO_PROP = 32 AND NO_KAB =  73";
            if($no_kec != 0){
            $sql .=" AND NO_KEC = $no_kec"; 
            }
            $sql .=" ORDER BY NO_KEC";
            $r = DB::select($sql);
            return $r;
        }
        public static function do_gisa_get_one_kecamatan($no_kec = 0){
            $sql ="";
            $sql .="SELECT NO_KEC, NAMA_KEC FROM KAMPUNG_GISA_SETUP_WIL WHERE NO_PROP = 32 AND NO_KAB =  73";
            if($no_kec != 0){
            $sql .=" AND NO_KEC = $no_kec"; 
            }
            $sql .=" GROUP BY NO_KEC,NAMA_KEC ORDER BY NO_KEC";
            $r = DB::select($sql);
            return $r;
        }
        public static function do_get_prop($no_prop = 0){
            $sql ="";
            $sql .=" SELECT NO_PROP, NAMA_PROP FROM SETUP_PROP WHERE 1=1";
            if($no_prop != 0){
            $sql .=" AND NO_PROP = $no_prop";   
            }
            $sql .=" ORDER BY NO_PROP";
            $r = DB::select($sql);
            return $r;
        }
        public static function do_get_kab($no_prop = 0, $no_kab = 0){
            $sql ="";
            $sql .=" SELECT NO_PROP, NO_KAB, NAMA_KAB FROM SETUP_KAB WHERE 1=1";
            if($no_prop != 0){
            $sql .=" AND NO_PROP = $no_prop";   
            }
            if($no_kab != 0){
            $sql .=" AND NO_KAB = $no_kab"; 
            }
            $sql .=" ORDER BY NO_KAB";
            $r = DB::select($sql);
            return $r;
        }
        public static function do_get_kec($no_prop = 0, $no_kab = 0, $no_kec = 0){
            $sql ="";
            $sql .=" SELECT NO_PROP, NO_KAB, NO_KEC, NAMA_KEC FROM SETUP_KEC WHERE 1=1";
            if($no_prop != 0){
            $sql .=" AND NO_PROP = $no_prop";   
            }
            if($no_kab != 0){
            $sql .=" AND NO_KAB = $no_kab"; 
            }
            if($no_kec != 0){
            $sql .=" AND NO_KEC = $no_kec"; 
            }
            $sql .=" ORDER BY NO_KEC";
            $r = DB::select($sql);
            return $r;
        }
        public static function do_get_kel($no_prop = 0, $no_kab = 0, $no_kec = 0, $no_kel = 0){
            $sql ="";
            $sql .=" SELECT NO_PROP, NO_KAB, NO_KEC, NO_KEL, NAMA_KEL FROM SETUP_KEL WHERE 1=1";
            if($no_prop != 0){
            $sql .=" AND NO_PROP = $no_prop";   
            }
            if($no_kab != 0){
            $sql .=" AND NO_KAB = $no_kab"; 
            }
            if($no_kec != 0){
            $sql .=" AND NO_KEC = $no_kec"; 
            }
            if($no_kel != 0){
            $sql .=" AND NO_KEL = $no_kel"; 
            }
            $sql .=" ORDER BY NO_KEL";
            $r = DB::select($sql);
            return $r;
        }
        public static function do_get_user_cpltte(){
            $sql ="";
            $sql .="SELECT DISTINCT(LOWER(REQ_BY)) REQ_BY FROM BSRE_KELAHIRAN ORDER BY REQ_BY";
            $r = DB::connection('db222')->select($sql);
            return $r;
        }

        public static function do_get_all_kelurahan($no_kec=0, $no_kel=0){
            $sql = "";
            $sql .= "SELECT NO_KEL, NAMA_KEL, NO_KEC FROM SETUP_KEL WHERE NO_PROP= 32 AND NO_KAB= 73 ";
            if ($no_kec != 0){
                $sql .= " AND NO_KEC = $no_kec ";
            }
            if ($no_kel != 0){
                $sql .= " AND NO_KEL = $no_kel ";
            }
              $sql .= " ORDER BY NO_KEC,NO_KEL";
            $r = DB::select($sql);
            return $r;
        }

        public static function do_gisa_get_all_kelurahan($no_kec=0, $no_kel=0){
            $sql = "";
            $sql .= "SELECT NO_KEC, NAMA_KEC,NO_KEL, NAMA_KEL FROM KAMPUNG_GISA_SETUP_WIL WHERE NO_PROP= 32 AND NO_KAB= 73 ";
            if ($no_kec != 0){
                $sql .= " AND NO_KEC = $no_kec ";
            }
            if ($no_kel != 0){
                $sql .= " AND NO_KEL = $no_kel ";
            }
              $sql .= " GROUP BY NO_KEC,NAMA_KEC, NO_KEL, NAMA_KEL ORDER BY NO_KEC,NO_KEL";
            $r = DB::select($sql);
            return $r;
        }
        public static function do_get_all_rw($no_kec=0,$no_kel=0){
            $sql = "SELECT NO_KEC, NO_KEL, NO_RW, JML FROM (SELECT NO_KEC, NO_KEL,NO_RW, COUNT(1) JML FROM DATA_KELUARGA WHERE NO_PROP= 32 AND NO_KAB= 73 AND NO_KEC = $no_kec AND NO_KEL = $no_kel AND NO_RW IS NOT NULL GROUP BY NO_KEC, NO_KEL, NO_RW ORDER BY NO_KEC, NO_KEL, NO_RW) WHERE JML > 50 ORDER BY NO_KEC, NO_KEL, NO_RW";
            $r = DB::select($sql);
            return $r;
        }
        public static function do_gisa_get_all_rw($no_kec=0,$no_kel=0){
            $sql = "SELECT NO_KEC, NAMA_KEC,NO_KEL, NAMA_KEL, NO_RW FROM KAMPUNG_GISA_SETUP_WIL WHERE NO_PROP= 32 AND NO_KAB= 73 AND NO_KEC = $no_kec AND NO_KEL = $no_kel AND NO_RW IS NOT NULL GROUP BY NO_KEC,NAMA_KEC, NO_KEL, NAMA_KEL,NO_RW ORDER BY NO_KEC,NO_KEL,NO_RW";
            $r = DB::select($sql);
            return $r;
        }
        public static function do_get_all_rt($no_kec=0,$no_kel=0,$no_rw=0){
            $sql = "SELECT NO_KEC, NO_KEL, NO_RW,NO_RT, JML FROM (SELECT NO_KEC, NO_KEL,NO_RW,NO_RT, COUNT(1) JML FROM DATA_KELUARGA WHERE NO_PROP= 32 AND NO_KAB= 73 AND NO_KEC = $no_kec AND NO_KEL = $no_kel AND NO_RW = $no_rw AND NO_RW IS NOT NULL AND NO_RT IS NOT NULL GROUP BY NO_KEC, NO_KEL, NO_RW, NO_RT ORDER BY NO_KEC, NO_KEL, NO_RW, NO_RT) WHERE JML > 5 ORDER BY NO_KEC, NO_KEL, NO_RW, NO_RT";
            $r = DB::select($sql);
            return $r;
        }
        public static function do_gisa_get_all_rt($no_kec=0,$no_kel=0,$no_rw=0){
            $sql = "SELECT NO_KEC, NAMA_KEC,NO_KEL, NAMA_KEL, NO_RW, NO_RT FROM KAMPUNG_GISA_SETUP_WIL WHERE NO_PROP= 32 AND NO_KAB= 73 AND NO_KEC = $no_kec AND NO_KEL = $no_kel AND NO_RW = $no_rw AND NO_RW IS NOT NULL AND NO_RT IS NOT NULL GROUP BY NO_KEC,NAMA_KEC, NO_KEL, NAMA_KEL,NO_RW, NO_RT ORDER BY NO_KEC,NO_KEL,NO_RW,NO_RT";
            $r = DB::select($sql);
            return $r;
        }
        public static function do_get_nama_kec($no_kec){
            $sql = "SELECT NAMA_KEC FROM SETUP_KEC WHERE NO_PROP= 32 AND NO_KAB= 73 AND NO_KEC = $no_kec";
            $r = DB::select($sql);
            return $r[0]->nama_kec;
        }
        public static function do_get_nama_kel($no_kec, $no_kel){
            $sql = "SELECT NAMA_KEL FROM SETUP_KEL WHERE NO_PROP= 32 AND NO_KAB= 73 AND NO_KEC = $no_kec AND NO_KEL = $no_kel";
            $r = DB::select($sql);
            return $r[0]->nama_kel;
        }
        public static function do_get_all_wil(){
            $sql = "SELECT NO_WIL, NAMA_WIL FROM SIAK_KODE_WIL ORDER BY NO_WIL";
            $r = DB::select($sql);
            return $r;
        }
        public static function do_get_capil_blk_opt(){
            $sql = "SELECT ID, JENIS_BLANGKO FROM SIAK_BLANGKO_CAPIL_OPTION ORDER BY ID";
            $r = DB::select($sql);
            return $r;
        }
        public static function do_get_master_menu(){
            $sql = "SELECT MENU_ID, MENU_DESC FROM SIAK_MASTER_LAPORAN ORDER BY MENU_ID";
            $r = DB::select($sql);
            return $r;
        }
        public static function do_get_master_dkb(){
            $sql = "SELECT VAL, DKB, BIO_NAME, KK_NAME, CUTOFF, LAST_UPDATE FROM SIAK_MENU_DKB ORDER BY VAL DESC";
            $r = DB::select($sql);
            return $r;
        }
        public static function do_get_master_usia(){
            $sql = "SELECT MENU_ID, MENU_DESC FROM SIAK_MASTER_USIA ORDER BY MENU_ID";
            $r = DB::select($sql);
            return $r;
        }
        public static function do_get_master_cakupan(){
            $sql = "SELECT MENU_ID, MENU_DESC FROM SIAK_MASTER_CAKUPAN ORDER BY MENU_ID";
            $r = DB::select($sql);
            return $r;
        }
        public static function do_get_month_date($bln){
            $sql = "SELECT 
                      TO_CHAR(DAY,'DD-MM-YYYY') DAY
                    , TO_CHAR(DAY, 'd') DOW
                FROM (SELECT TRUNC(TO_DATE('$bln','MM-YYYY'), 'MM') + LEVEL - 1 AS DAY FROM DUAL CONNECT BY TRUNC(TRUNC(TO_DATE('$bln','MM-YYYY'), 'MM') + LEVEL - 1, 'MM') = TRUNC(TO_DATE('$bln','MM-YYYY'), 'MM')) ORDER BY DAY";
            $r = DB::select($sql);
            return $r;
        }
        public static function do_get_dkb(){
            $sql = "SELECT SYSTEM_VALUE_TXT AS VAL FROM SIAK_MASTER WHERE SYSTEM_TYPE = 'INIT_SETING' AND SYSTEM_CODE = 'DKB'";
            $r = DB::select($sql);
            return $r[0]->val;
        }
        public static function do_get_dkb_gisa(){
            $sql = "SELECT SYSTEM_VALUE_TXT AS VAL FROM SIAK_MASTER WHERE SYSTEM_TYPE = 'INIT_SETING' AND SYSTEM_CODE = 'DKB_GISA'";
            $r = DB::select($sql);
            return $r[0]->val;
        }
        public static function do_get_dkb_bio(){
            $sql = "SELECT SYSTEM_VALUE_TXT AS VAL FROM SIAK_MASTER WHERE SYSTEM_TYPE = 'INIT_SETING' AND SYSTEM_CODE = 'DKB_BIO'";
            $r = DB::select($sql);
            return $r[0]->val;
        }
        public static function do_get_dkb_keluarga(){
            $sql = "SELECT SYSTEM_VALUE_TXT AS VAL FROM SIAK_MASTER WHERE SYSTEM_TYPE = 'INIT_SETING' AND SYSTEM_CODE = 'DKB_KELUARGA'";
            $r = DB::select($sql);
            return $r[0]->val;
        }
        public static function do_get_cut_off_date(){
            $sql = "SELECT SYSTEM_VALUE_TXT AS VAL FROM SIAK_MASTER WHERE SYSTEM_TYPE = 'INIT_SETING' AND SYSTEM_CODE = 'DKB_CUTOFF'";
            $r = DB::select($sql);
            return $r[0]->val;
        }
        
        public static function do_get_user_daily(){
            $sql = "SELECT DISTINCT(USER_ID) FROM SIAK_DAILY ORDER BY USER_ID";
            $r = DB::select($sql);
            return $r;
        }
        public static function do_get_no_prop(){
            $sql = "SELECT SYSTEM_VALUE_NUM AS VAL FROM SIAK_MASTER WHERE SYSTEM_TYPE = 'INIT_SETING' AND SYSTEM_CODE = 'NO_PROP'";
            $r = DB::select($sql);
            return $r[0]->val;
        }
        public static function do_get_no_kab(){
            $sql = "SELECT SYSTEM_VALUE_NUM AS VAL FROM SIAK_MASTER WHERE SYSTEM_TYPE = 'INIT_SETING' AND SYSTEM_CODE = 'NO_KAB'";
            $r = DB::select($sql);
            return $r[0]->val;
        }
        public static function do_get_nm_prop(){
            $sql = "SELECT SYSTEM_VALUE_TXT AS VAL FROM SIAK_MASTER WHERE SYSTEM_TYPE = 'INIT_SETING' AND SYSTEM_CODE = 'NM_PROP'";
            $r = DB::select($sql);
            return $r[0]->val;
        }
        public static function do_get_nm_kab(){
            $sql = "SELECT SYSTEM_VALUE_TXT AS VAL FROM SIAK_MASTER WHERE SYSTEM_TYPE = 'INIT_SETING' AND SYSTEM_CODE = 'NM_KAB'";
            $r = DB::select($sql);
            return $r[0]->val;
        }
        public static function do_get_siak_dblink(){
            $sql = "SELECT SYSTEM_VALUE_TXT AS VAL FROM SIAK_MASTER WHERE SYSTEM_TYPE = 'INIT_SETING' AND SYSTEM_CODE = 'SIAK_DBLINK'";
            $r = DB::select($sql);
            return $r[0]->val;
        }
        public static function do_get_rekam_dblink(){
            $sql = "SELECT SYSTEM_VALUE_TXT AS VAL FROM SIAK_MASTER WHERE SYSTEM_TYPE = 'INIT_SETING' AND SYSTEM_CODE = 'REKAM_DBLINK'";
            $r = DB::select($sql);
            return $r[0]->val;
        }
        public static function do_get_cetak_dblink(){
            $sql = "SELECT SYSTEM_VALUE_TXT AS VAL FROM SIAK_MASTER WHERE SYSTEM_TYPE = 'INIT_SETING' AND SYSTEM_CODE = 'CETAK_DBLINK'";
            $r = DB::select($sql);
            return $r[0]->val;
        }
        public static function do_get_master_dblink(){
            $sql = "SELECT SYSTEM_VALUE_TXT AS VAL FROM SIAK_MASTER WHERE SYSTEM_TYPE = 'INIT_SETING' AND SYSTEM_CODE = 'MASTER_DBLINK'";
            $r = DB::select($sql);
            return $r[0]->val;
        }
        public static function do_is_valid(){
            $sql = "SELECT SYSTEM_VALUE_TXT AS VAL FROM SIAK_MASTER WHERE SYSTEM_TYPE = 'INIT_SETING' AND SYSTEM_CODE = 'EXP'";
            $r = DB::select($sql);
            return $r[0]->val;
        }
        public static function do_get_default_pass(){
            $sql = "SELECT SYSTEM_VALUE_TXT AS VAL FROM SIAK_MASTER WHERE SYSTEM_TYPE = 'INIT_SETING' AND SYSTEM_CODE = 'DEFAULT_PASS'";
            $r = DB::select($sql);
            return $r[0]->val;
        }
        public static function do_get_lvl_lhr(){
            $sql = "SELECT SYSTEM_VALUE_TXT AS VAL FROM SIAK_MASTER WHERE SYSTEM_TYPE = 'SET_CAPIL' AND SYSTEM_CODE = 'LEVEL_AKTA_LHR'";
            $r = DB::select($sql);
            return $r[0]->val;
        }
        public static function do_get_lvl_kmt(){
            $sql = "SELECT SYSTEM_VALUE_TXT AS VAL FROM SIAK_MASTER WHERE SYSTEM_TYPE = 'SET_CAPIL' AND SYSTEM_CODE = 'LEVEL_AKTA_KMT'";
            $r = DB::select($sql);
            return $r[0]->val;
        }

        public static function do_get_lvl_lhr_mpl(){
            $sql = "SELECT SYSTEM_VALUE_TXT AS VAL FROM SIAK_MASTER WHERE SYSTEM_TYPE = 'SET_CAPIL' AND SYSTEM_CODE = 'LAHIR_MPL'";
            $r = DB::select($sql);
            return $r[0]->val;
        }

        public static function do_get_is_mpl(){
            $sql = "SELECT SYSTEM_VALUE_TXT AS VAL FROM SIAK_MASTER WHERE SYSTEM_TYPE = 'SET_CAPIL' AND SYSTEM_CODE = 'LHR_IS_MPL'";
            $r = DB::select($sql);
            return $r[0]->val;
        }

        public static function do_get_is_dns(){
            $sql = "SELECT SYSTEM_VALUE_TXT AS VAL FROM SIAK_MASTER WHERE SYSTEM_TYPE = 'SET_CAPIL' AND SYSTEM_CODE = 'LHR_IS_DNS'";
            $r = DB::select($sql);
            return $r[0]->val;
        }
     
        public static function do_get_menu_all($level = 0){
            $sql = "SELECT TO_NUMBER(A.MENU_ID) MENU_ID,A.PARENT_ID,A.TITLE,A.ACTIVE_MENU, A.USER_LEVEL, A.MENU_LEVEL FROM VW_SIAK_MENU A  WHERE A.USER_LEVEL = $level AND A.MENU_LEVEL = 1 ORDER BY A.LEVEL_HEAD,A.LEVEL_SUB,A.LEVEL_CONTENT";
            $r = DB::select($sql);
            return $r;
        }
        public static function do_get_sub_menu_all($parent_id = 0,$level = 0){
            $sql = "SELECT TO_NUMBER(A.MENU_ID) MENU_ID,A.PARENT_ID,A.TITLE,A.ACTIVE_MENU, A.USER_LEVEL, A.MENU_LEVEL FROM VW_SIAK_MENU A  WHERE A.USER_LEVEL = $level AND A.PARENT_ID = $parent_id ORDER BY A.LEVEL_HEAD,A.LEVEL_SUB,A.LEVEL_CONTENT";
            $r = DB::select($sql);
            return $r;
        }

        public static function do_get_islogin($ip_address,$user_id){
            $sql = "SELECT COUNT(1) VAL FROM SIAK_SESSION_PLUS WHERE IP_ADDRESS = '$ip_address' AND USER_ID = '$user_id' AND IS_ACTIVE = 1";
            $r = DB::select($sql);
            return $r[0]->val;
        }
         public static function do_stop_activity($user_id){
            $sql = "SELECT COUNT(1) AS CNT FROM SIAK_SESSION_PLUS WHERE USER_ID = '$user_id'";
            $r = DB::select($sql);
             $r = (int) $q->row()->CNT;
             if($r > 0){
                $sql = "UPDATE SIAK_SESSION_PLUS SET LAST_ACTIVITY = SYSDATE, IS_ACTIVE = 0  WHERE USER_ID = '$user_id'";
                $r = DB::update($sql);
             }
             
        }
        public static function do_update_no_prop($val,$user){
            $sql = "UPDATE SIAK_MASTER 
                       SET SYSTEM_VALUE_NUM = $val
                       , CHANGED_BY = '$user'
                       , CHANGED_DT = SYSDATE
                     WHERE SYSTEM_CODE = 'NO_PROP'";
            $r = DB::update($sql);
            return $r;
        }
        public static function do_update_no_kab($val,$user){
            $sql = "UPDATE SIAK_MASTER 
                       SET SYSTEM_VALUE_NUM = $val
                       , CHANGED_BY = '$user'
                       , CHANGED_DT = SYSDATE
                     WHERE SYSTEM_CODE = 'NO_KAB'";
            $r = DB::update($sql);
            return $r;
        }
        public static function do_update_nm_prop($val,$user){
            $sql = "UPDATE SIAK_MASTER 
                       SET SYSTEM_VALUE_TXT = '$val'
                       , CHANGED_BY = '$user'
                       , CHANGED_DT = SYSDATE
                     WHERE SYSTEM_CODE = 'NM_PROP'";
            $r = DB::update($sql);
            return $r;
        }
        public static function do_update_nm_kab($val,$user){
            $sql = "UPDATE SIAK_MASTER 
                       SET SYSTEM_VALUE_TXT = '$val'
                       , CHANGED_BY = '$user'
                       , CHANGED_DT = SYSDATE
                     WHERE SYSTEM_CODE = 'NM_KAB'";
            $r = DB::update($sql);
            return $r;
        }
        public static function do_update_siak_dblink($val,$user){
            $sql = "UPDATE SIAK_MASTER 
                       SET SYSTEM_VALUE_TXT = '$val'
                       , CHANGED_BY = '$user'
                       , CHANGED_DT = SYSDATE
                     WHERE SYSTEM_CODE = 'SIAK_DBLINK'";
            $r = DB::update($sql);
            return $r;
        }
        public static function do_update_cetak_dblink($val,$user){
            $sql = "UPDATE SIAK_MASTER 
                       SET SYSTEM_VALUE_TXT = '$val'
                       , CHANGED_BY = '$user'
                       , CHANGED_DT = SYSDATE
                     WHERE SYSTEM_CODE = 'CETAK_DBLINK'";
            $r = DB::update($sql);
            return $r;
        }
        public static function do_update_rekam_dblink($val,$user){
            $sql = "UPDATE SIAK_MASTER 
                       SET SYSTEM_VALUE_TXT = '$val'
                       , CHANGED_BY = '$user'
                       , CHANGED_DT = SYSDATE
                     WHERE SYSTEM_CODE = 'REKAM_DBLINK'";
            $r = DB::update($sql);
            return $r;
        }
        public static function do_update_master_dblink($val,$user){
            $sql = "UPDATE SIAK_MASTER 
                       SET SYSTEM_VALUE_TXT = '$val'
                       , CHANGED_BY = '$user'
                       , CHANGED_DT = SYSDATE
                     WHERE SYSTEM_CODE = 'MASTER_DBLINK'";
            $r = DB::update($sql);
            return $r;
        }
        public static function do_update_dkb_bio($val,$user){
            $sql = "UPDATE SIAK_MASTER 
                       SET SYSTEM_VALUE_TXT = '$val'
                       , CHANGED_BY = '$user'
                       , CHANGED_DT = SYSDATE
                     WHERE SYSTEM_CODE = 'DKB_BIO'";
            $r = DB::update($sql);
            return $r;
        }
        public static function do_update_dkb_kk($val,$user){
            $sql = "UPDATE SIAK_MASTER 
                       SET SYSTEM_VALUE_TXT = '$val'
                       , CHANGED_BY = '$user'
                       , CHANGED_DT = SYSDATE
                     WHERE SYSTEM_CODE = 'DKB_KELUARGA'";
            $r = DB::update($sql);
            return $r;
        }
        public static function do_update_dkb_tahun($val,$user){
            $sql = "UPDATE SIAK_MASTER 
                       SET SYSTEM_VALUE_TXT = '$val'
                       , CHANGED_BY = '$user'
                       , CHANGED_DT = SYSDATE
                     WHERE SYSTEM_CODE = 'DKB'";
            $r = DB::update($sql);
            return $r;
        }
        public static function do_update_dkb_cutoff($val,$user){
            $sql = "UPDATE SIAK_MASTER 
                       SET SYSTEM_VALUE_TXT = '$val'
                       , CHANGED_BY = '$user'
                       , CHANGED_DT = SYSDATE
                     WHERE SYSTEM_CODE = 'DKB_CUTOFF'";
            $r = DB::update($sql);
            return $r;
        }
        public static function do_update_default_pass($val,$user){
            $sql = "UPDATE SIAK_MASTER 
                       SET SYSTEM_VALUE_TXT = '$val'
                       , CHANGED_BY = '$user'
                       , CHANGED_DT = SYSDATE
                     WHERE SYSTEM_CODE = 'DEFAULT_PASS'";
            $r = DB::update($sql);
            return $r;
        }
        public static function do_update_give_kec($val,$user){
            $sql = "UPDATE SIAK_MASTER 
                       SET SYSTEM_VALUE_NUM = $val
                       , CHANGED_BY = '$user'
                       , CHANGED_DT = SYSDATE
                     WHERE SYSTEM_CODE = 'GIVE_ALL_KEC'";
            $r = DB::update($sql);
            return $r;
        }
        public static function do_update_give_kel($val,$user){
            $sql = "UPDATE SIAK_MASTER 
                       SET SYSTEM_VALUE_NUM = $val
                       , CHANGED_BY = '$user'
                       , CHANGED_DT = SYSDATE
                     WHERE SYSTEM_CODE = 'GIVE_ALL_KEL'";
            $r = DB::update($sql);
            return $r;
        }
        public static function do_get_gisa_kecamatan($no_kec = 0){
            $sql ="";
            $sql .=" SELECT NO_KEC, NAMA_KEC FROM SETUP_KEC WHERE NO_PROP = 32 AND NO_KAB =  73";
            if($no_kec != 0){
            $sql .=" AND NO_KEC = $no_kec"; 
            }
            $sql .=" ORDER BY NO_KEC";
            $r = DB::select($sql);
            return $r;
        }
        public static function do_get_user_by_lvl($lvl = 0){
            $sql ="";
            $sql .="SELECT A.USER_ID, A.NAMA_LGKP, A.NIP, A.TMPT_LHR, A.TGL_LHR, A.JENIS_KLMIN, A.GOLONGAN, A.PANGKAT, A.JABATAN, A.NAMA_KANTOR, A.ALAMAT_KANTOR, A.TELP, A.WILAYAH_CAKUPAN, A.USER_LEVEL,B.LEVEL_NAME, A.USER_PWD, A.STATUS, A.CHECK_IP, A.CREATED_BY, A.CREATED_DATE, A.MODIFIED_BY, A.MODIFIED_DATE, A.NO_PROP, A.NO_KAB, A.NO_KEC, A.NO_KEL, A.KODE_INSTANSI, A.KODE_KONJEN, A.SALAH_PASSWORD, A.TTE_FILE, A.TTE_STATUS, A.TTE_NIP, A.TTE_NAMA_LGKP, A.TTE_JABATAN, A.TTE_TITLE_KK1, A.TTE_TITLE_KK2, A.TTE_TITLE_PINDAH1, A.TTE_TITLE_PINDAH2, F5_GET_NAMA_PROVINSI(A.NO_PROP) NAMA_PROP, F5_GET_NAMA_KABUPATEN(A.NO_PROP,A.NO_KAB) NAMA_KAB, F5_GET_NAMA_KECAMATAN(A.NO_PROP,A.NO_KAB,A.NO_KEC) NAMA_KEC, F5_GET_NAMA_KELURAHAN(A.NO_PROP,A.NO_KAB,A.NO_KEC,A.NO_KEL) NAMA_KEL, CASE WHEN C.USER_ID IS NULL THEN 'OFFLINE' ELSE 'ONLINE' END IS_ONLINE FROM T5_SIAK_USER A INNER JOIN T5_SIAK_LEVEL B ON A.USER_LEVEL = B.USER_LEVEL LEFT JOIN T5_SESSION C ON A.USER_ID = C.USER_ID WHERE A.USER_LEVEL IN ($lvl) ORDER BY A.NAMA_LGKP";
            $r = DB::connection('db222')->select($sql);
            return $r;
        }
        public static function activate_user_siak($user_id)
        {
                $sql = "UPDATE T5_SIAK_USER SET STATUS = 1 WHERE USER_ID = '$user_id'";
                $r = DB::connection('db222')->update($sql);
        }
        public static function deactivate_user_siak($user_id)
        {
                $sql = "UPDATE T5_SIAK_USER SET STATUS = 0 WHERE USER_ID = '$user_id'";
                $r = DB::connection('db222')->update($sql);
                $sql = "DELETE FROM T5_SESSION WHERE USER_ID = '$user_id'";
                $r = DB::connection('db222')->update($sql);
        }
        public static function do_getweb_prop($no_prop = 0){
            $sql ="";
            $sql .=" SELECT NO_PROP, CONCAT(CONCAT(NO_PROP,' - '),NAMA_PROP) NAMA_PROP FROM SETUP_PROP WHERE 1=1";
            if($no_prop != 0){
            $sql .=" AND NO_PROP = $no_prop";   
            }
            $sql .=" ORDER BY NO_PROP";
            $r = DB::select($sql);
            return $r;
        }
        public static function do_getweb_kab($no_prop = 0, $no_kab = 0){
            $sql ="";
            $sql .=" SELECT NO_PROP, NO_KAB, CONCAT(CONCAT(NO_KAB,' - '),NAMA_KAB) NAMA_KAB FROM SETUP_KAB WHERE 1=1";
            if($no_prop != 0){
            $sql .=" AND NO_PROP = $no_prop";   
            }
            if($no_kab != 0){
            $sql .=" AND NO_KAB = $no_kab"; 
            }
            $sql .=" ORDER BY NO_KAB";
            $r = DB::select($sql);
            return $r;
        }
        public static function do_getweb_kec($no_prop = 0, $no_kab = 0, $no_kec = 0){
            $sql ="";
            $sql .=" SELECT NO_PROP, NO_KAB, NO_KEC, CONCAT(CONCAT(NO_KEC,' - '),NAMA_KEC) NAMA_KEC FROM SETUP_KEC WHERE 1=1";
            if($no_prop != 0){
            $sql .=" AND NO_PROP = $no_prop";   
            }
            if($no_kab != 0){
            $sql .=" AND NO_KAB = $no_kab"; 
            }
            if($no_kec != 0){
            $sql .=" AND NO_KEC = $no_kec"; 
            }
            $sql .=" ORDER BY NO_KEC";
            $r = DB::select($sql);
            return $r;
        }
        public static function do_getweb_kel($no_prop = 0, $no_kab = 0, $no_kec = 0, $no_kel = 0){
            $sql ="";
            $sql .=" SELECT NO_PROP, NO_KAB, NO_KEC, NO_KEL, CONCAT(CONCAT(NO_KEL,' - '),NAMA_KEL) NAMA_KEL FROM SETUP_KEL WHERE 1=1";
            if($no_prop != 0){
            $sql .=" AND NO_PROP = $no_prop";   
            }
            if($no_kab != 0){
            $sql .=" AND NO_KAB = $no_kab"; 
            }
            if($no_kec != 0){
            $sql .=" AND NO_KEC = $no_kec"; 
            }
            if($no_kel != 0){
            $sql .=" AND NO_KEL = $no_kel"; 
            }
            $sql .=" ORDER BY NO_KEL";
            $r = DB::select($sql);
            return $r;
        }
        public static function do_get_ref_biodata_delete(){
            $sql ="";
            $sql .=" SELECT REF_DELETE FROM T7_REF_DELETE WHERE REF_TABLE = 'BIODATA WNI' ORDER BY CREATED_DATE";
            $r = DB::connection('db222')->select($sql);
            return $r;
        }
    
}
