<?php

namespace App\Http\Controllers;

use DB;
use Session;
use DateTime;
use PDF;
use Illuminate\Http\Request;
use App\Http\Controllers\SharedController as Shr;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class SearchController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public function uktp(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 90;
		   $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->no_kec != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$nik = rtrim($request->nik, ',');
			$r = $this->scr_cek_req_data_uktp($nik,$tgl_start,$tgl_end,$no_kec,$no_kel);
			// $j = $this->scr_cek_req_count_uktp($nik,$tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'Request Cetak Ktp',
		 		"mtitle"=>'Request Cetak Ktp (Belum Cetak Without Filter)',
		 		"my_url"=>'uktp',
		 		"type_tgl"=>'Request',
		 		"data"=>$r,
		 		// "jumlah"=>$j,
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			}else{
			$data = array(
		 		"stitle"=>'Request Cetak Ktp',
		 		"mtitle"=>'Request Cetak Ktp (Belum Cetak Without Filter)',
		 		"my_url"=>'uktp',
		 		"type_tgl"=>'Request',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
    		}
			return view('Cekreq/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}

	public function dktp(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 91;
		   $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->no_kec != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$nik = rtrim($request->nik, ',');
			$r = $this->scr_cek_req_data_dktp($nik,$tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'Request Cetak Ktp',
		 		"mtitle"=>'Request Cetak Ktp (Sudah Cetak Without Filter)',
		 		"my_url"=>'dktp',
		 		"type_tgl"=>'Request',
		 		"data"=>$r,
		 		// "jumlah"=>$j,
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
    		);
			}else{
			$data = array(
		 		"stitle"=>'Request Cetak Ktp',
		 		"mtitle"=>'Request Cetak Ktp (Sudah Cetak Without Filter)',
		 		"my_url"=>'dktp',
		 		"type_tgl"=>'Request',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
    		);
    		}
			return view('Cekcetak/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}

	public function uktpld(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 90;
		   $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->no_kec != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$nik = rtrim($request->nik, ',');
			$r = $this->scr_cek_req_data_uktpld($nik,$tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'Request Cetak Ktp LD',
		 		"mtitle"=>'Request Cetak Ktp LD',
		 		"my_url"=>'uktp',
		 		"type_tgl"=>'Request',
		 		"data"=>$r,
		 		// "jumlah"=>$j,
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			}else{
			$data = array(
		 		"stitle"=>'Request Cetak Ktp LD',
		 		"mtitle"=>'Request Cetak Ktp LD',
		 		"my_url"=>'uktp',
		 		"type_tgl"=>'Request',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
    		}
			return view('Cekreqld/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}

	public function dktpld(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 91;
		   $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->no_kec != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$nik = rtrim($request->nik, ',');
			$r = $this->scr_cek_req_data_dktp($nik,$tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'Request Cetak Ktp LD',
		 		"mtitle"=>'Request Cetak Ktp LD',
		 		"my_url"=>'dktp',
		 		"type_tgl"=>'Request',
		 		"data"=>$r,
		 		// "jumlah"=>$j,
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
    		);
			}else{
			$data = array(
		 		"stitle"=>'Request Cetak Ktp LD',
		 		"mtitle"=>'Request Cetak Ktp LD',
		 		"my_url"=>'dktp',
		 		"type_tgl"=>'Request',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
    		);
    		}
			return view('Cekcetakld/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function uktp_f(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 87;
		   $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->no_kec != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$nik = rtrim($request->nik, ',');
			$r = $this->scr_cek_req_data_uktp_f($nik,$tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'Request Cetak Ktp',
		 		"mtitle"=>'Request Cetak Ktp (Belum Cetak With Filter)',
		 		"my_url"=>'uktp_f',
		 		"type_tgl"=>'Request',
		 		"data"=>$r,
		 		// "jumlah"=>$j,
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			}else{
			$data = array(
		 		"stitle"=>'Request Cetak Ktp',
		 		"mtitle"=>'Request Cetak Ktp (Belum Cetak With Filter)',
		 		"my_url"=>'uktp_f',
		 		"type_tgl"=>'Request',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
    		}
			return view('Cekreq/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}

	public function dktp_f(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 88;
		   $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->no_kec != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$nik = rtrim($request->nik, ',');
			$r = $this->scr_cek_req_data_dktp_f($nik,$tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'Request Cetak Ktp',
		 		"mtitle"=>'Request Cetak Ktp (Sudah Cetak With Filter)',
		 		"my_url"=>'dktp_f',
		 		"type_tgl"=>'Request',
		 		"data"=>$r,
		 		// "jumlah"=>$j,
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
    		);
			}else{
			$data = array(
		 		"stitle"=>'Request Cetak Ktp',
		 		"mtitle"=>'Request Cetak Ktp (Sudah Cetak With Filter)',
		 		"my_url"=>'dktp_f',
		 		"type_tgl"=>'Request',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
    		);
    		}
			return view('Cekcetak/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}

	public function usuket(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 93;
		   $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->no_kec != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$nik = rtrim($request->nik, ',');
			$r = $this->scr_cek_req_data_usuket($nik,$tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'Request Cetak Suket',
		 		"mtitle"=>'Request Cetak Suket (Belum Cetak)',
		 		"my_url"=>'usuket',
		 		"type_tgl"=>'Request',
		 		"data"=>$r,
		 		// "jumlah"=>$j,
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
    		);
			}else{
			$data = array(
		 		"stitle"=>'Request Cetak Suket',
		 		"mtitle"=>'Request Cetak Suket (Belum Cetak)',
		 		"my_url"=>'usuket',
		 		"type_tgl"=>'Request',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
    		);
    		}
			return view('Cekreq/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}

	public function dsuket(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 94;
		   $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->no_kec != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$nik = rtrim($request->nik, ',');
			$r = $this->scr_cek_req_data_dsuket($nik,$tgl_start,$tgl_end,$no_kec,$no_kel);
			// $j = $this->scr_cek_req_count_dsuket($nik,$tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'Request Cetak Suket',
		 		"mtitle"=>'Request Cetak Suket (Sudah Cetak)',
		 		"my_url"=>'dsuket',
		 		"type_tgl"=>'Request',
		 		"data"=>$r,
		 		// "jumlah"=>$j,
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
    		);
			}else{
			$data = array(
		 		"stitle"=>'Request Cetak Suket',
		 		"mtitle"=>'Request Cetak Suket (Sudah Cetak)',
		 		"my_url"=>'dsuket',
		 		"type_tgl"=>'Request',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
    		);
    		}
			return view('Cekcetak/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}

	
	public function req_bio(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 85;
		   $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->no_kec != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$nik = rtrim($request->nik, ',');
			$r = $this->scr_cek_req_data_bio($nik,$tgl_start,$tgl_end,$no_kec,$no_kel);
			// $j = $this->scr_cek_req_count_bio($nik,$tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'Request Perpindahan Monitoring',
		 		"mtitle"=>'Request Perpindahan Monitoring',
		 		"my_url"=>'req_bio',
		 		"type_tgl"=>'Request',
		 		"data"=>$r,
		 		// "jumlah"=>$j,
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
    		);
			}else{
			$data = array(
		 		"stitle"=>'Request Perpindahan Monitoring',
		 		"mtitle"=>'Request Perpindahan Monitoring',
		 		"my_url"=>'req_bio',
		 		"type_tgl"=>'Request',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
    		);
    		}
			return view('Reqbio/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}

	
	public function req_bio2(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 85;
		   $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			
			$data = array(
		 		"stitle"=>'Request Perpindahan Monitoring',
		 		"mtitle"=>'Request Perpindahan Monitoring',
		 		"my_url"=>'req_bio',
		 		"type_tgl"=>'Request',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
    		);
			return view('Reqbio2/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function delete_req(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 119;
		   $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->nik != null){
			$nik = rtrim($request->nik, ',');
			if (substr($nik, 0, 1) === ','){
				$nik = ltrim($nik, ',');
			}
			$r = $this->scr_del_req($nik,$request->session()->get('S_NO_KEC'));
			$data = array(
		 		"stitle"=>'Delete Pengajuan Bermasalah',
		 		"mtitle"=>'Delete Pengajuan Bermasalah',
		 		"my_url"=>'delete_req',
		 		"type_tgl"=>'Pengajuan',
		 		"data"=>$r,
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
    		);
			}else{
			$data = array(
		 		"stitle"=>'Delete Pengajuan Bermasalah',
		 		"mtitle"=>'Delete Pengajuan Bermasalah',
		 		"my_url"=>'delete_req',
		 		"type_tgl"=>'Pengajuan',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
    		);
    		}
			return view('Deletereq/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function do_delete(Request $request)
	{
		   $menu_id = 119;
		   $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->niks != null){
			$nik = $request->niks;
			if (substr($nik, 0, 1) === ','){
				$nik = ltrim($nik, ',');
			}
				$this->scr_do_delete($nik);
				return redirect()->route('delete_req');
			}else{
				return redirect()->route('delete_req');
    		}    		
	}
	public function req_ektp_perubahan(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 280;
		   $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$data = array(
		 		"stitle"=>'Request Pencetakan Ektp (Perubahan Elemen)',
		 		"mtitle"=>'Request Pencetakan Ektp (Perubahan Elemen)',
		 		"my_url"=>'delete_req',
		 		"type_tgl"=>'Pengajuan',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
    		);
			return view('pengajuan_ektp_perubahan/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function req_ektp_pushbio(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 280;
		   $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$data = array(
		 		"stitle"=>'Request Pengajuan Pindahan',
		 		"mtitle"=>'Request Pengajuan Pindahan',
		 		"my_url"=>'delete_req',
		 		"type_tgl"=>'Pengajuan',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
    		);
			return view('pengajuan_ektp_pushbio/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function req_ektp_rusak(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 279;
		   $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$data = array(
		 		"stitle"=>'Request Pencetakan Ektp (Rusak)',
		 		"mtitle"=>'Request Pencetakan Ektp (Rusak)',
		 		"my_url"=>'delete_req',
		 		"type_tgl"=>'Pengajuan',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
    		);
			return view('pengajuan_ektp_rusak/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function req_ektp_hilang(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 278;
		   $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$data = array(
		 		"stitle"=>'Request Pencetakan Ektp (Hilang)',
		 		"mtitle"=>'Request Pencetakan Ektp (Hilang)',
		 		"my_url"=>'delete_req',
		 		"type_tgl"=>'Pengajuan',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
    		);
			return view('pengajuan_ektp_hilang/main',$data);
		}else{
			return redirect()->route('logout');
			}
	}

	public function req_ektp_detail_hilang(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 281;
		   $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$data = array(
		 		"stitle"=>'Detail Pencetakan Ektp (Hilang)',
		 		"mtitle"=>'Detail Pencetakan Ektp (Hilang)',
		 		"my_url"=>'InputKehilangan',
		 		"type_tgl"=>'Pengajuan',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
    		);
			return view('pengajuan_hilang_detail/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function list_pengajuan(Request $request)
     {
          $draw = intval($request->draw);
          $start = intval($request->start);
          $length = intval($request->length);
          if($request->nik != null){
			$nik = rtrim($request->nik, ',');
			if($request->nik != null){
				$nik = rtrim($request->nik, ',');
				if (substr($nik, 0, 1) === ','){
					$nik = ltrim($nik, ',');
				}
			$rekap_pengajuan = $this->scr_cek_list_pengajuan($nik,$request->session()->get('S_NO_KEC'));
			}
			
          $data = array();
          $i = 0;
          foreach($rekap_pengajuan as $r) {
          		$i ++;
               $data[] = array(
               		$i,
                    $r->no_kec,
                    $r->nik,
                    $r->no_kk,
                    $r->nama_lengkap,
                    $r->current_status_code,
                    $r->tmpt_lhr.',<br>'.$r->tgl_lhr,
                    $r->alamat.'<br><b>RT.</b> '.$r->rt.' <b>RW.</b> '.$r->rw.'<br><b>KDOE POS:</b>'.$r->kode_pos,
                    $r->req_date,
                    $r->req_by,
                    $r->printed_date,
                    $r->printed_by,
                    $r->alasan_pengajuan,
                    $r->status_pengajuan,
                    $r->keterangan_status,
                    $r->nama_kec,
                    $r->nama_kel,
               );
          }

          $output = array(
               "draw" => $draw,
                 "recordsTotal" => count($rekap_pengajuan),
                 "recordsFiltered" => count($rekap_pengajuan),
                 "data" => $data
            );
          return $output;
          exit();
		}else{
			$rekap_pengajuan= [];
			 $data = array();
		      $i = 0;
          $output = array(
               "draw" => $draw,
                 "recordsTotal" => count($rekap_pengajuan),
                 "recordsFiltered" => count($rekap_pengajuan),
                 "data" => $data
            );
          return $output;
          exit();
		}
	}
	public function list_pengajuan_ditolak(Request $request)
     {
          $draw = intval($request->draw);
          $start = intval($request->start);
          $length = intval($request->length);
          if($request->nik != null){
			$nik = rtrim($request->nik, ',');
			if($request->nik != null){
				$nik = rtrim($request->nik, ',');
				if (substr($nik, 0, 1) === ','){
					$nik = ltrim($nik, ',');
				}
			$rekap_pengajuan = $this->scr_tolak_list_pengajuan($nik,$request->session()->get('S_NO_KEC'));
			}
			
          $data = array();
          $i = 0;
          foreach($rekap_pengajuan as $r) {
          		$i ++;
               $data[] = array(
               		$i,
                    $r->no_kec,
                    $r->nik,
                    $r->no_kk,
                    $r->nama_lengkap,
                    $r->tmpt_lhr.',<br>'.$r->tgl_lhr,
                    $r->alamat.'<br><b>RT.</b> '.$r->rt.' <b>RW.</b> '.$r->rw.'<br><b>KDOE POS:</b>'.$r->kode_pos,
                    ($r->current_status_code == 'DATA REKAM KOSONG') ? '<i class="mdi mdi-biohazard text-white" style="float: none; color: red !important;"></i><span style="float: none; color: red !important;"> '.$r->current_status_code : '<i class="mdi mdi-fingerprint text-white" style="float: none; color: green !important;"></i><span style="float: none; color: green !important;"> '.$r->current_status_code,
                   
                    $r->req_date,
                    $r->req_by,
                    $r->printed_date,
                    $r->printed_by,
                    $r->alasan_pengajuan,
                    $r->status_pengajuan,
                    $r->keterangan_status,
                    $r->nama_kec,
                    $r->nama_kel,
               );
          }

          $output = array(
               "draw" => $draw,
                 "recordsTotal" => count($rekap_pengajuan),
                 "recordsFiltered" => count($rekap_pengajuan),
                 "data" => $data
            );
          return $output;
          exit();
		}else{
			$rekap_pengajuan= [];
			 $data = array();
		      $i = 0;
          $output = array(
               "draw" => $draw,
                 "recordsTotal" => count($rekap_pengajuan),
                 "recordsFiltered" => count($rekap_pengajuan),
                 "data" => $data
            );
          return $output;
          exit();
		}
	}
	public function list_hilang(Request $request)
     {
          $draw = intval($request->draw);
          $start = intval($request->start);
          $length = intval($request->length);
          if($request->tanggal != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$rekap_pengajuan = $this->scr_cek_list_hilang($tgl_start,$tgl_end,$request->session()->get('S_NO_KEC'));
			
			
          $data = array();
          $i = 0;
          foreach($rekap_pengajuan as $r) {
          		$i ++;
               $data[] = array(
               		$i,
                    $r->nik,
                    $r->no_kk,
                    $r->nama_lengkap,
                    $r->req_date,
                    $r->req_by,
                    $r->alasan_pengajuan,
                    $r->no_kehilangan,
                    $r->nama_kec,
                    $r->nama_kel,
                     '<button type="button" class="btn btn-info btn-xs" id="btn-lihat" onclick="do_kehilangan('.$r->do_nik.');" style="margin-top: 5px;">Update <i class="mdi mdi-tooltip-edit fa-fw"></i></button>'
                 );
          }

          $output = array(
               "draw" => $draw,
                 "recordsTotal" => count($rekap_pengajuan),
                 "recordsFiltered" => count($rekap_pengajuan),
                 "data" => $data
            );
          return $output;
          exit();
		}else{
			$rekap_pengajuan= [];
			 $data = array();
		      $i = 0;
          $output = array(
               "draw" => $draw,
                 "recordsTotal" => count($rekap_pengajuan),
                 "recordsFiltered" => count($rekap_pengajuan),
                 "data" => $data
            );
          return $output;
          exit();
		}
	}
	public function do_ajukan(Request $request)
	{
		   header("Content-Type: application/json", true);
			if($request->niks != null){
			$nik = $request->niks;
			$pengajuan = $request->reqstatus;
			if (substr($nik, 0, 1) === ','){
				$nik = ltrim($nik, ',');
			}
			if ($request->session()->get('S_NO_KEC') != 0){
				$no_kec = $request->session()->get('S_NO_KEC');
			}else{
				$no_kec = 0;
			}
			if ($pengajuan == 4){
				$this->scr_do_ajukan_pindah($nik,$request->session()->get('S_USER_ID'),$no_kec);
				$data["success"] = TRUE;
				$data["message"] = "Data Berhasil Di Ajukan";
	       		return $data;
			}else if ($pengajuan == 3){
				$this->scr_do_ajukan_hilang($nik,$request->session()->get('S_USER_ID'),$no_kec);
				$data["success"] = TRUE;
				$data["message"] = "Data Berhasil Di Ajukan";
	       		return $data;
			}else if ($pengajuan == 2){
				$this->scr_do_ajukan_rusak($nik,$request->session()->get('S_USER_ID'),$no_kec);
				$data["success"] = TRUE;
				$data["message"] = "Data Berhasil Di Ajukan";
	       		return $data;
			}else{
				$this->scr_do_ajukan_perubahan_data($nik,$request->session()->get('S_USER_ID'),$no_kec);
				$data["success"] = TRUE;
				$data["message"] = "Data Berhasil Di Ajukan";
	       		return $data;
			}
				
			}else{
				$data["success"] = FALSE;
				$data["message"] = "Data Gagal Di Ajukan";
	       		return $data;
    		}    		
	}
	public function do_ajukan_ld(Request $request)
	{
		   header("Content-Type: application/json", true);
			if($request->nik != null){
				if($request->state == 1){
					$nik = $request->nik;
					$nama = $request->nama;
					$no_kec = $request->no_kec;
					$this->scr_do_ajukan_ld($nik,$nama,$no_kec,$request->session()->get('S_USER_ID'));
					$data["success"] = TRUE;
					$data["message"] = "Data Berhasil Di Ajukan";
		       		return $data;
	       		}else if($request->state == 2){
	       			$nik = $request->nik;
					$nama = $request->nama;
					$no_kec = $request->no_kec;
					$this->scr_do_update_ld($nik,$nama,$no_kec,$request->session()->get('S_USER_ID'));
					$data["success"] = TRUE;
					$data["message"] = "Data Berhasil Di Ajukan Ulang";
		       		return $data;
	       		}else if($request->state == 3){
	       			$nik = $request->nik;
					$nama = $request->nama;
					$no_kec = $request->no_kec;
					$this->scr_do_delete_ld($nik,$nama,$no_kec,$request->session()->get('S_USER_ID'));
					$data["success"] = TRUE;
					$data["message"] = "Data Berhasil Di Hapus Dari Pengajuan";
		       		return $data;
	       		}else{

	       		}
		  	}
	}
	public function edit_kehilangan(Request $request)
    {
        if ($request->nik != null)
        {
            $nik = $request->nik;
            $no_kehilangan = $request->no_kehilangan;
            $pengajuan = $this->src_get_cek_pengajuan_hilang($nik);
            if (count($pengajuan) > 0)
            {
                $nik_hilang = $pengajuan[0]->nik;
                $this->src_update_pengajuan_hilang($nik_hilang, $no_kehilangan);
                $data["success"] = true;
                $data["is_save"] = 0;
                $data["message"] = "Nomor Kehilangan Dari Nik : " . $nik . " Berhasil Di Ubah";
                return $data;
            }
            else
            {
                $data["success"] = true;
                $data["is_save"] = 1;
                $data["message"] = "Mohon Maaf Nik : " . $nik . " Tidak Memiliki Pengajuan Ektp Hilang, Silahkan Cek SIAK";
                return $data;
            }
        }
        else
        {
            redirect('/', 'refresh');
        }
    }
		
		
		
		

		public function scr_del_req($nik,$no_kec){
			$sql = "";
			$sql .= "SELECT 
					A.NO_KEC
					,X.NIK
					,X.NAMA_LGKP AS NAMA_LENGKAP, X.ALAMAT, CASE WHEN TO_CHAR(X.KODE_POS) IS NULL THEN '-' ELSE TO_CHAR(X.KODE_POS) END KODE_POS, LPAD(TO_CHAR(X.NO_RT), 3, '0') AS RT, LPAD(TO_CHAR(X.NO_RW), 3, '0') AS RW,CASE WHEN X.CURRENT_STATUS_CODE IS NULL THEN '-' ELSE X.CURRENT_STATUS_CODE END AS CURRENT_STATUS_CODE
					,TO_CHAR(A.REQ_DATE,'YYYY-MM-DD') AS REQ_DATE
					,A.REQ_BY
					, CASE WHEN A.STEP_PROC = 'T' THEN 'DITOLAK' WHEN A.STEP_PROC = 'C' THEN 'DICETAK' ELSE 'PROSSESS' END AS STATUS_PENGAJUAN
					, CASE WHEN A.STEP_PROC = 'T' THEN UPPER(A.PROC_BY) WHEN A.STEP_PROC = 'C' THEN 'SUDAH DICATAK' ELSE '-' END AS KETERANGAN_STATUS
					, TO_CHAR(C.PERSONALIZED_DATE,'YYYY-MM-DD') AS PRINTED_DATE
					, CASE WHEN TO_CHAR(C.LAST_UPDATED_USERNAME) IS NULL AND TO_CHAR(C.CREATED_USERNAME) IS NULL  THEN '-' WHEN TO_CHAR(C.LAST_UPDATED_USERNAME) IS NULL AND TO_CHAR(C.CREATED_USERNAME) IS NOT NULL THEN TO_CHAR(C.CREATED_USERNAME) ELSE TO_CHAR(C.LAST_UPDATED_USERNAME) END AS PRINTED_BY
					, X.NAMA_KEC
					, X.NAMA_KEL
					, X.JENIS_PKRJN
					, X.STAT_KWN
					FROM DEMOGRAPHICS_ALL X INNER JOIN (SELECT ID_CETAK,NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY,STEP_PROC,PROC_BY FROM 
					(SELECT  ID_CETAK, NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY,STEP_PROC,PROC_BY, RANK() OVER (PARTITION BY NIK ORDER BY REQ_DATE DESC, ID_CETAK DESC) RNK FROM SIAK_REQ_CETAK_KTP@YZDB WHERE STEP_PROC <> 'C') WHERE RNK = 1) A ON A.NIK =X.NIK
					 LEFT JOIN (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME FROM (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME, RANK() OVER (PARTITION BY NIK ORDER BY  PERSONALIZED_DATE DESC, LAST_UPDATE DESC, CHIP_ID DESC) RNK FROM CARD_MANAGEMENT) WHERE RNK = 1) C ON A.NIK = C.NIK  WHERE 1=1 AND (TRUNC(A.REQ_DATE)+1 > TRUNC(C.PERSONALIZED_DATE) OR C.PERSONALIZED_DATE IS NULL)";
			if($no_kec != 0){
				$sql .= " AND A.NO_KEC = $no_kec";
			}
			if($nik != ""){
				$sql .= " AND X.NIK IN ($nik)";
			}
			$sql .= "ORDER BY C.PERSONALIZED_DATE";
			$r = DB::connection('db5cetak')->select($sql);
			return $r;
		}
		public function scr_do_delete($nik){
			$sql = "DELETE FROM SIAK_REQ_CETAK_KTP@YZDB WHERE STEP_PROC <> 'C' AND NIK IN ($nik) ";
			$r = DB::connection('db222')->delete($sql);
		}
		public function scr_cek_req_data_uktp($nik,$tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "";
			$sql .= "SELECT 
					A.NO_KEC
					,X.NIK
					,X.NAMA_LGKP AS NAMA_LENGKAP, X.ALAMAT, CASE WHEN TO_CHAR(X.KODE_POS) IS NULL THEN '-' ELSE TO_CHAR(X.KODE_POS) END KODE_POS, LPAD(TO_CHAR(X.NO_RT), 3, '0') AS RT, LPAD(TO_CHAR(X.NO_RW), 3, '0') AS RW,CASE WHEN X.CURRENT_STATUS_CODE IS NULL THEN '-' ELSE X.CURRENT_STATUS_CODE END AS CURRENT_STATUS_CODE
					,TO_CHAR(A.REQ_DATE,'YYYY-MM-DD') AS REQ_DATE
					,A.REQ_BY
					, CASE WHEN A.STEP_PROC = 'T' THEN 'DITOLAK' WHEN A.STEP_PROC = 'C' THEN 'DICETAK' ELSE 'PROSSESS' END AS STATUS_PENGAJUAN
					,CASE WHEN A.ALASAN_PENGAJUAN IS NOT NULL THEN A.ALASAN_PENGAJUAN WHEN A.STEP_PROC = 'T' THEN UPPER(A.PROC_BY) WHEN A.STEP_PROC = 'C' THEN 'SUDAH DICATAK' ELSE '-' END AS KETERANGAN_STATUS
					, TO_CHAR(C.PERSONALIZED_DATE,'YYYY-MM-DD') AS PRINTED_DATE
					, CASE WHEN TO_CHAR(C.LAST_UPDATED_USERNAME) IS NULL AND TO_CHAR(C.CREATED_USERNAME) IS NULL  THEN '-' WHEN TO_CHAR(C.LAST_UPDATED_USERNAME) IS NULL AND TO_CHAR(C.CREATED_USERNAME) IS NOT NULL THEN TO_CHAR(C.CREATED_USERNAME) ELSE TO_CHAR(C.LAST_UPDATED_USERNAME) END AS PRINTED_BY
					, X.NAMA_KEC
					, X.NAMA_KEL
					, X.AGAMA
					, X.JENIS_PKRJN
					, X.STAT_KWN
					FROM DEMOGRAPHICS_ALL X INNER JOIN (SELECT ID_CETAK,NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY,STEP_PROC,PROC_BY,ALASAN_PENGAJUAN FROM 
					(SELECT  ID_CETAK, NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY,STEP_PROC,PROC_BY,ALASAN_PENGAJUAN, RANK() OVER (PARTITION BY NIK ORDER BY REQ_DATE DESC, ID_CETAK DESC) RNK FROM SIAK_REQ_CETAK_KTP@YZDB WHERE STEP_PROC <> 'C') WHERE RNK = 1) A ON A.NIK =X.NIK
					LEFT JOIN (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME FROM (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME, RANK() OVER (PARTITION BY NIK ORDER BY  PERSONALIZED_DATE DESC, LAST_UPDATE DESC, CHIP_ID DESC) RNK FROM CARD_MANAGEMENT) WHERE RNK = 1) C ON A.NIK = C.NIK  WHERE 1=1 AND (TRUNC(A.REQ_DATE) > TRUNC(C.PERSONALIZED_DATE) OR C.PERSONALIZED_DATE IS NULL) ";
			if($no_kec != 0){
				$sql .= " AND A.NO_KEC = $no_kec";
			}
			if($no_kel != 0){
				$sql .= " AND A.NO_KEL = $no_kel";
			} 
			if($nik != ""){
				$sql .= " AND X.NIK IN ($nik)";
			}
			$sql .= " AND A.REQ_DATE >= TO_DATE('$tgl_start','DD-MM-YYYY') AND A.REQ_DATE < TO_DATE('$tgl_end','DD-MM-YYYY') +1 ORDER BY C.PERSONALIZED_DATE";
			$r = DB::connection('db5cetak')->select($sql);
			return $r;
		}
		public function scr_cek_req_data_uktpld($nik,$tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "";
			$sql .= "SELECT 
					A.NO_KEC
					,A.NIK
					,A.NAMA_LENGKAP AS NAMA_LENGKAP, X.ALAMAT, CASE WHEN TO_CHAR(X.KODE_POS) IS NULL THEN '-' ELSE TO_CHAR(X.KODE_POS) END KODE_POS, LPAD(TO_CHAR(X.NO_RT), 3, '0') AS RT, LPAD(TO_CHAR(X.NO_RW), 3, '0') AS RW,CASE WHEN X.CURRENT_STATUS_CODE IS NULL THEN '-' ELSE X.CURRENT_STATUS_CODE END AS CURRENT_STATUS_CODE
					,TO_CHAR(A.REQ_DATE,'YYYY-MM-DD') AS REQ_DATE
					,A.REQ_BY
					, CASE WHEN A.STEP_PROC = 'T' THEN 'DITOLAK' WHEN A.STEP_PROC = 'C' THEN 'DICETAK' ELSE 'PROSSESS' END AS STATUS_PENGAJUAN
					,CASE WHEN A.ALASAN_PENGAJUAN IS NOT NULL THEN A.ALASAN_PENGAJUAN WHEN A.STEP_PROC = 'T' THEN UPPER(A.PROC_BY) WHEN A.STEP_PROC = 'C' THEN 'SUDAH DICATAK' ELSE '-' END AS KETERANGAN_STATUS
					, TO_CHAR(C.PERSONALIZED_DATE,'YYYY-MM-DD') AS PRINTED_DATE
					, CASE WHEN TO_CHAR(C.LAST_UPDATED_USERNAME) IS NULL AND TO_CHAR(C.CREATED_USERNAME) IS NULL  THEN '-' WHEN TO_CHAR(C.LAST_UPDATED_USERNAME) IS NULL AND TO_CHAR(C.CREATED_USERNAME) IS NOT NULL THEN TO_CHAR(C.CREATED_USERNAME) ELSE TO_CHAR(C.LAST_UPDATED_USERNAME) END AS PRINTED_BY
					, X.NAMA_KEC
					, X.NAMA_KEL
					, X.AGAMA
					, X.JENIS_PKRJN
					, X.STAT_KWN
					FROM (SELECT ID_CETAK,NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY,STEP_PROC,PROC_BY,ALASAN_PENGAJUAN FROM (SELECT  ID_CETAK, NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY,STEP_PROC,PROC_BY,ALASAN_PENGAJUAN, RANK() OVER (PARTITION BY NIK ORDER BY REQ_DATE DESC, ID_CETAK DESC) RNK FROM SIAK_REQ_CETAK_KTPLD@YZDB WHERE STEP_PROC <> 'C') WHERE RNK = 1) A LEFT JOIN DEMOGRAPHICS_ALL X  ON A.NIK =X.NIK
					LEFT JOIN (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME FROM (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME, RANK() OVER (PARTITION BY NIK ORDER BY  PERSONALIZED_DATE DESC, LAST_UPDATE DESC, CHIP_ID DESC) RNK FROM CARD_MANAGEMENT) WHERE RNK = 1) C ON A.NIK = C.NIK  WHERE 1=1 AND (TRUNC(A.REQ_DATE) > TRUNC(C.PERSONALIZED_DATE) OR C.PERSONALIZED_DATE IS NULL)  ";
			if($no_kec != 0){
				$sql .= " AND A.NO_KEC = $no_kec";
			}
			if($nik != ""){
				$sql .= " AND A.NIK IN ($nik)";
			}
			$sql .= " AND A.REQ_DATE >= TO_DATE('$tgl_start','DD-MM-YYYY') AND A.REQ_DATE < TO_DATE('$tgl_end','DD-MM-YYYY') +1 ORDER BY C.PERSONALIZED_DATE";
			// echo $sql;
			// die;
			$r = DB::connection('db222')->select($sql);
			return $r;
		}

		public function scr_cek_req_data_dktp($nik,$tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "";
			$sql .= "SELECT X.NO_KEC,X.NIK,X.NAMA_LGKP AS NAMA_LENGKAP, X.ALAMAT,CASE WHEN A.ALASAN_PENGAJUAN IS NOT NULL THEN A.ALASAN_PENGAJUAN WHEN A.STEP_PROC = 'T' THEN UPPER(A.PROC_BY) WHEN A.STEP_PROC = 'C' THEN 'SUDAH DICATAK' ELSE '-' END AS KETERANGAN_STATUS,  CASE WHEN TO_CHAR(X.KODE_POS) IS NULL THEN '-' ELSE TO_CHAR(X.KODE_POS) END KODE_POS, LPAD(TO_CHAR(X.NO_RT), 3, '0') AS RT, LPAD(TO_CHAR(X.NO_RW), 3, '0') AS RW, B.CURRENT_STATUS_CODE,TO_CHAR(A.REQ_DATE,'DD-MM-YYYY') AS REQ_DATE,A.REQ_BY,TO_CHAR(C.PERSONALIZED_DATE,'DD-MM-YYYY') AS PRINTED_DATE,CASE WHEN TO_CHAR(C.LAST_UPDATED_USERNAME) IS NULL AND TO_CHAR(C.CREATED_USERNAME) IS NULL  THEN '-' WHEN TO_CHAR(C.LAST_UPDATED_USERNAME) IS NULL AND TO_CHAR(C.CREATED_USERNAME) IS NOT NULL THEN TO_CHAR(C.CREATED_USERNAME) ELSE TO_CHAR(C.LAST_UPDATED_USERNAME) END AS PRINTED_BY, X.NAMA_KEC, X.NAMA_KEL, X.AGAMA, X.JENIS_PKRJN, X.STAT_KWN FROM DEMOGRAPHICS_ALL X INNER JOIN (SELECT ID_CETAK,NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY,STEP_PROC,PROC_BY,ALASAN_PENGAJUAN FROM (SELECT  ID_CETAK, NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY,STEP_PROC,PROC_BY,ALASAN_PENGAJUAN, RANK() OVER (PARTITION BY NIK ORDER BY REQ_DATE DESC, ID_CETAK DESC) RNK FROM SIAK_REQ_CETAK_KTP@YZDB ) WHERE RNK = 1) A ON A.NIK = X.NIK LEFT JOIN DEMOGRAPHICS_ALL B ON A.NIK = B.NIK LEFT JOIN (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME FROM (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME, RANK() OVER (PARTITION BY NIK ORDER BY  PERSONALIZED_DATE DESC, LAST_UPDATE DESC, CHIP_ID DESC) RNK FROM CARD_MANAGEMENT) WHERE RNK = 1) C ON A.NIK = C.NIK  WHERE 1=1  AND (A.REQ_DATE < C.PERSONALIZED_DATE) ";
			if($no_kec != 0){
				$sql .= " AND X.NO_KEC = $no_kec";
			}
			if($no_kel != 0){
				$sql .= " AND X.NO_KEL = $no_kel";
			} 
			if($nik != ""){
				$sql .= " AND X.NIK IN ($nik)";
			}
			$sql .= " AND A.REQ_DATE >= TO_DATE('$tgl_start','DD-MM-YYYY') AND A.REQ_DATE < TO_DATE('$tgl_end','DD-MM-YYYY') +1";
			$r = DB::connection('db5cetak')->select($sql);
			return $r;
		}

		public function scr_cek_req_data_uktp_f($nik,$tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "";
			$sql .= "SELECT A.NO_KEC,X.NIK,X.NAMA_LGKP AS NAMA_LENGKAP, X.ALAMAT, CASE WHEN TO_CHAR(X.KODE_POS) IS NULL THEN '-' ELSE TO_CHAR(X.KODE_POS) END KODE_POS, LPAD(TO_CHAR(X.NO_RT), 3, '0') AS RT, LPAD(TO_CHAR(X.NO_RW), 3, '0') AS RW, CASE WHEN B.CURRENT_STATUS_CODE IS NULL THEN '-' ELSE B.CURRENT_STATUS_CODE END AS CURRENT_STATUS_CODE,TO_CHAR(A.REQ_DATE,'YYYY-MM-DD') AS REQ_DATE,A.REQ_BY, TO_CHAR(C.PERSONALIZED_DATE,'YYYY-MM-DD') AS PRINTED_DATE,CASE WHEN TO_CHAR(C.LAST_UPDATED_USERNAME) IS NULL AND TO_CHAR(C.CREATED_USERNAME) IS NULL  THEN '-' WHEN TO_CHAR(C.LAST_UPDATED_USERNAME) IS NULL AND TO_CHAR(C.CREATED_USERNAME) IS NOT NULL THEN TO_CHAR(C.CREATED_USERNAME) ELSE TO_CHAR(C.LAST_UPDATED_USERNAME) END AS PRINTED_BY, X.NAMA_KEC, X.NAMA_KEL, X.AGAMA, X.JENIS_PKRJN, X.STAT_KWN FROM DEMOGRAPHICS_ALL@DB5CETAK X INNER JOIN (SELECT ID_CETAK,NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY FROM (SELECT  ID_CETAK, NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY, RANK() OVER (PARTITION BY NIK ORDER BY REQ_DATE DESC, ID_CETAK DESC) RNK FROM SIAK_REQ_CETAK_KTP@YZDB ) WHERE RNK = 1) A ON A.NIK = X.NIK LEFT JOIN DEMOGRAPHICS_ALL@DB5CETAK B ON A.NIK = B.NIK LEFT JOIN (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME FROM (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME, RANK() OVER (PARTITION BY NIK ORDER BY  PERSONALIZED_DATE DESC, LAST_UPDATE DESC, CHIP_ID DESC) RNK FROM CARD_MANAGEMENT@DB5CETAK) WHERE RNK = 1) C ON A.NIK = C.NIK  WHERE 1=1 AND (B.CURRENT_STATUS_CODE LIKE '%CARD%' OR B.CURRENT_STATUS_CODE LIKE '%PRINT%') AND (TRUNC(A.REQ_DATE) > TRUNC(C.PERSONALIZED_DATE) OR C.PERSONALIZED_DATE IS NULL)";
			if($no_kec != 0){
				$sql .= " AND A.NO_KEC = $no_kec";
			}
			if($no_kel != 0){
				$sql .= " AND A.NO_KEL = $no_kel";
			} 
			if($nik != ""){
				$sql .= " AND X.NIK IN ($nik)";
			}
			$sql .= " AND A.REQ_DATE >= TO_DATE('$tgl_start','DD-MM-YYYY') AND A.REQ_DATE < TO_DATE('$tgl_end','DD-MM-YYYY') +1";
		
			$r = DB::connection('db5cetak')->select($sql);
			return $r;
		}

		public function scr_cek_req_data_dktp_f($nik,$tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "";
			$sql .= "SELECT X.NO_KEC,X.NIK,X.NAMA_LGKP AS NAMA_LENGKAP, X.ALAMAT,CASE WHEN A.ALASAN_PENGAJUAN IS NOT NULL THEN A.ALASAN_PENGAJUAN WHEN A.STEP_PROC = 'T' THEN UPPER(A.PROC_BY) WHEN A.STEP_PROC = 'C' THEN 'SUDAH DICATAK' ELSE '-' END AS KETERANGAN_STATUS, CASE WHEN TO_CHAR(X.KODE_POS) IS NULL THEN '-' ELSE TO_CHAR(X.KODE_POS) END KODE_POS, LPAD(TO_CHAR(X.NO_RT), 3, '0') AS RT, LPAD(TO_CHAR(X.NO_RW), 3, '0') AS RW, B.CURRENT_STATUS_CODE,TO_CHAR(A.REQ_DATE,'DD-MM-YYYY') AS REQ_DATE,A.REQ_BY,TO_CHAR(C.PERSONALIZED_DATE,'DD-MM-YYYY') AS PRINTED_DATE,CASE WHEN TO_CHAR(C.LAST_UPDATED_USERNAME) IS NULL AND TO_CHAR(C.CREATED_USERNAME) IS NULL  THEN '-' WHEN TO_CHAR(C.LAST_UPDATED_USERNAME) IS NULL AND TO_CHAR(C.CREATED_USERNAME) IS NOT NULL THEN TO_CHAR(C.CREATED_USERNAME) ELSE TO_CHAR(C.LAST_UPDATED_USERNAME) END AS PRINTED_BY, X.NAMA_KEC, X.NAMA_KEL, X.AGAMA, X.JENIS_PKRJN, X.STAT_KWN FROM DEMOGRAPHICS_ALL@DB5CETAK X INNER JOIN (SELECT ID_CETAK,NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY,STEP_PROC,PROC_BY,ALASAN_PENGAJUAN FROM (SELECT  ID_CETAK, NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY,STEP_PROC,PROC_BY,ALASAN_PENGAJUAN, RANK() OVER (PARTITION BY NIK ORDER BY REQ_DATE DESC, ID_CETAK DESC) RNK FROM SIAK_REQ_CETAK_KTP@YZDB ) WHERE RNK = 1) A ON A.NIK = X.NIK LEFT JOIN DEMOGRAPHICS_ALL@DB5CETAK B ON A.NIK = B.NIK LEFT JOIN (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME FROM (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME, RANK() OVER (PARTITION BY NIK ORDER BY  PERSONALIZED_DATE DESC, LAST_UPDATE DESC, CHIP_ID DESC) RNK FROM CARD_MANAGEMENT@DB5CETAK) WHERE RNK = 1) C ON A.NIK = C.NIK  WHERE 1=1  AND (B.CURRENT_STATUS_CODE LIKE '%CARD%' OR B.CURRENT_STATUS_CODE LIKE '%PRINT%') AND (A.REQ_DATE < C.PERSONALIZED_DATE)";
			if($no_kec != 0){
				$sql .= " AND X.NO_KEC = $no_kec";
			}
			if($no_kel != 0){
				$sql .= " AND X.NO_KEL = $no_kel";
			} 
			if($nik != ""){
				$sql .= " AND X.NIK IN ($nik)";
			}
			$sql .= " AND A.REQ_DATE >= TO_DATE('$tgl_start','DD-MM-YYYY') AND A.REQ_DATE < TO_DATE('$tgl_end','DD-MM-YYYY') +1";
			$r = DB::connection('db5cetak')->select($sql);
			return $r;
		}

		public function scr_cek_req_data_usuket($nik,$tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "";
			$sql .= "SELECT X.NO_KEC,X.NIK,X.NAMA_LGKP AS NAMA_LENGKAP, X.ALAMAT,X.AGAMA, CASE WHEN TO_CHAR(X.KODE_POS) IS NULL THEN '-' ELSE TO_CHAR(X.KODE_POS) END KODE_POS, LPAD(TO_CHAR(X.NO_RT), 3, '0') AS RT, LPAD(TO_CHAR(X.NO_RW), 3, '0') AS RW, B.CURRENT_STATUS_CODE,TO_CHAR(A.REQ_DATE,'DD-MM-YYYY') AS REQ_DATE,A.REQ_BY, TO_CHAR(C.PRINTED_DATE,'DD-MM-YYYY') AS PRINTED_DATE,C.PRINTED_BY, B.NAMA_KEC, B.NAMA_KEL, X.JENIS_PKRJN, X.STAT_KWN FROM DEMOGRAPHICS_ALL@DB5CETAK X INNER JOIN (SELECT ID_CETAK,NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY FROM (SELECT  ID_CETAK, NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY, RANK() OVER (PARTITION BY NIK ORDER BY REQ_DATE DESC, ID_CETAK DESC) RNK FROM SIAK_REQ_CETAK_KTP@YZDB) WHERE RNK = 1) A ON A.NIK = X.NIK INNER JOIN DEMOGRAPHICS_ALL@DB5CETAK B ON A.NIK = B.NIK LEFT JOIN (SELECT NIK, PRINTED_DATE,PRINTED_BY FROM (SELECT NIK,PRINTED_DATE,PRINTED_BY, RANK() OVER (PARTITION BY NIK ORDER BY PRINTED_DATE DESC) RNK FROM T7_HIST_SUKET@DB222) WHERE RNK = 1) C ON A.NIK = C.NIK  WHERE 1=1 AND (B.CURRENT_STATUS_CODE NOT LIKE '%CARD%' AND B.CURRENT_STATUS_CODE NOT LIKE '%PRINT%') AND (A.REQ_DATE > C.PRINTED_DATE OR C.PRINTED_DATE IS NULL)";
			if($no_kec != 0){
				$sql .= " AND X.NO_KEC = $no_kec";
			}
			if($no_kel != 0){
				$sql .= " AND X.NO_KEL = $no_kel";
			} 
			if($nik != ""){
				$sql .= " AND X.NIK IN ($nik)";
			}
			$sql .= " AND A.REQ_DATE >= TO_DATE('$tgl_start','DD-MM-YYYY') AND A.REQ_DATE < TO_DATE('$tgl_end','DD-MM-YYYY') +1";

			$r = DB::connection('db5cetak')->select($sql);
			return $r;
		}

		

		public function scr_cek_req_data_dsuket($nik,$tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "";
			$sql .= "SELECT X.NO_KEC,X.NIK,X.NAMA_LGKP AS NAMA_LENGKAP, X.ALAMAT,X.AGAMA, CASE WHEN TO_CHAR(X.KODE_POS) IS NULL THEN '-' ELSE TO_CHAR(X.KODE_POS) END KODE_POS, LPAD(TO_CHAR(X.NO_RT), 3, '0') AS RT, LPAD(TO_CHAR(X.NO_RW), 3, '0') AS RW, B.CURRENT_STATUS_CODE,TO_CHAR(A.REQ_DATE,'DD-MM-YYYY') AS REQ_DATE,A.REQ_BY, TO_CHAR(C.PRINTED_DATE,'DD-MM-YYYY') AS PRINTED_DATE,C.PRINTED_BY, B.NAMA_KEC, B.NAMA_KEL, X.JENIS_PKRJN, X.STAT_KWN FROM DEMOGRAPHICS_ALL@DB5CETAK X INNER JOIN (SELECT ID_CETAK,NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY FROM (SELECT  ID_CETAK, NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY, RANK() OVER (PARTITION BY NIK ORDER BY REQ_DATE DESC, ID_CETAK DESC) RNK FROM SIAK_REQ_CETAK_KTP@YZDB ) WHERE RNK = 1) A ON A.NIK = X.NIK INNER JOIN DEMOGRAPHICS_ALL@DB5CETAK B ON A.NIK = B.NIK LEFT JOIN (SELECT NIK, PRINTED_DATE,PRINTED_BY FROM (SELECT NIK,PRINTED_DATE,PRINTED_BY, RANK() OVER (PARTITION BY NIK ORDER BY PRINTED_DATE DESC) RNK FROM T7_HIST_SUKET@DB222) WHERE RNK = 1) C ON A.NIK = C.NIK  WHERE 1=1 AND (B.CURRENT_STATUS_CODE NOT LIKE '%CARD%' AND B.CURRENT_STATUS_CODE NOT LIKE '%PRINT%') AND (A.REQ_DATE < C.PRINTED_DATE)";
			if($no_kec != 0){
				$sql .= " AND X.NO_KEC = $no_kec";
			}
			if($no_kel != 0){
				$sql .= " AND X.NO_KEL = $no_kel";
			} 
			if($nik != ""){
				$sql .= " AND X.NIK IN ($nik)";
			}
			$sql .= " AND A.REQ_DATE >= TO_DATE('$tgl_start','DD-MM-YYYY') AND A.REQ_DATE < TO_DATE('$tgl_end','DD-MM-YYYY') +1";
			$r = DB::connection('db5cetak')->select($sql);
			return $r;
		}

		
		
		
		public function scr_cek_req_data_bio($nik,$tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "";
			$sql .= "SELECT A.NO_KEC,A.NIK,A.NAMA_LENGKAP, B.ALAMAT ALAMAT, B.KODE_POS  KODE_POS, B.NO_RT  RT,B.NO_RW  RW,  CASE WHEN B.CURRENT_STATUS_CODE IS NULL THEN 'PINDAHAN' ELSE B.CURRENT_STATUS_CODE END AS CURRENT_STATUS_CODE, TO_CHAR(A.REQ_DATE,'DD-MM-YYYY') AS REQ_DATE,A.REQ_BY, TO_CHAR(C.PERSONALIZED_DATE,'YYYY-MM-DD') AS PRINTED_DATE,CASE WHEN TO_CHAR(C.LAST_UPDATED_USERNAME) IS NULL AND TO_CHAR(C.CREATED_USERNAME) IS NULL  THEN '-' WHEN TO_CHAR(C.LAST_UPDATED_USERNAME) IS NULL AND TO_CHAR(C.CREATED_USERNAME) IS NOT NULL THEN TO_CHAR(C.CREATED_USERNAME) ELSE TO_CHAR(C.LAST_UPDATED_USERNAME) END AS PRINTED_BY FROM (SELECT ID_CETAK,NO_KEC, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY,STEP_PROC FROM (SELECT  ID_CETAK, NO_KEC, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY,STEP_PROC, RANK() OVER (PARTITION BY NIK ORDER BY REQ_DATE DESC, ID_CETAK DESC) RNK FROM SIAK_REQ_CETAK_KTPLD@YZDB) WHERE RNK = 1) A  LEFT JOIN DEMOGRAPHICS_ALL B ON A.NIK = B.NIK LEFT JOIN DEMOGRAPHICS D ON A.NIK = D.NIK LEFT JOIN (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME FROM (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME, RANK() OVER (PARTITION BY NIK ORDER BY  PERSONALIZED_DATE DESC, LAST_UPDATE DESC, CHIP_ID DESC) RNK FROM CARD_MANAGEMENT@DB5CETAK) WHERE RNK = 1) C ON A.NIK = C.NIK  WHERE 1=1 AND (B.CURRENT_STATUS_CODE LIKE '%CARD%' OR B.CURRENT_STATUS_CODE LIKE '%PRINT%' OR B.CURRENT_STATUS_CODE IS NULL) AND (TRUNC(A.REQ_DATE) > TRUNC(C.PERSONALIZED_DATE) OR C.PERSONALIZED_DATE IS NULL) AND STEP_PROC = 'A'";
			if($no_kec != 0){
				$sql .= " AND A.NO_KEC = $no_kec";
			}
			if($no_kel != 0){
				$sql .= " AND A.NO_KEL = $no_kel";
			} 
			if($nik != ""){
				$sql .= " AND A.NIK IN ($nik)";
			}
			$sql .= " AND A.REQ_DATE >= TO_DATE('$tgl_start','DD-MM-YYYY') AND A.REQ_DATE < TO_DATE('$tgl_end','DD-MM-YYYY') +1";			
			$r = DB::connection('db5cetak')->select($sql);
			return $r;
		}

		

		public function scr_cek_list_pengajuan($nik,$no_kec){
			$sql = "";
			$sql .= "SELECT 
					CASE WHEN A.NIK IS NOT NULL THEN A.NO_KEC ELSE B.NO_KEC END NO_KEC
					, B.NIK
					, CONCAT(',',B.NIK) NIK
					, CONCAT(',',B.NO_KK) NO_KK
					, B.NAMA_LGKP AS NAMA_LENGKAP
					, B.TMPT_LHR
					, TO_CHAR(B.TGL_LHR,'DD-MM-YYYY') TGL_LHR
                    , B.ALAMAT
                    , CASE WHEN TO_CHAR(B.KODE_POS) IS NULL THEN '-' ELSE TO_CHAR(B.KODE_POS) END KODE_POS
                    , LPAD(TO_CHAR(B.NO_RT), 3, '0') AS RT
                    , LPAD(TO_CHAR(B.NO_RW), 3, '0') AS RW
                    , CASE WHEN B.CURRENT_STATUS_CODE IS NULL THEN 'DATA REKAM KOSONG' ELSE B.CURRENT_STATUS_CODE END AS CURRENT_STATUS_CODE
					, CASE WHEN TO_CHAR(A.REQ_DATE,'YYYY-MM-DD') IS NULL THEN '-' ELSE TO_CHAR(A.REQ_DATE,'YYYY-MM-DD')  END AS REQ_DATE
					, CASE WHEN A.REQ_BY IS NULL THEN '-' ELSE A.REQ_BY END REQ_BY
                    , CASE WHEN A.ALASAN_PENGAJUAN IS NULL THEN '-' ELSE A.ALASAN_PENGAJUAN END ALASAN_PENGAJUAN
					, CASE WHEN A.STEP_PROC = 'T' THEN 'DITOLAK' WHEN A.STEP_PROC = 'C' THEN 'DICETAK' WHEN A.STEP_PROC IS NULL THEN '-' ELSE 'PROSSESS' END AS STATUS_PENGAJUAN
					, CASE WHEN A.ALASAN_PENGAJUAN IS NOT NULL THEN A.ALASAN_PENGAJUAN WHEN A.STEP_PROC = 'T' THEN UPPER(A.PROC_BY) WHEN A.STEP_PROC = 'C' THEN 'SUDAH DICATAK' ELSE '-' END AS KETERANGAN_STATUS
					, CASE WHEN TO_CHAR(C.LAST_UPDATE,'DD-MM-YYYY') IS NULL AND TO_CHAR(C.PERSONALIZED_DATE,'DD-MM-YYYY') IS NULL THEN '-' WHEN TO_CHAR(C.LAST_UPDATE,'DD-MM-YYYY') IS NULL THEN TO_CHAR(C.PERSONALIZED_DATE,'DD-MM-YYYY') ELSE TO_CHAR(C.LAST_UPDATE,'DD-MM-YYYY') END AS PRINTED_DATE
					, CASE WHEN TO_CHAR(C.LAST_UPDATED_USERNAME) IS NULL AND TO_CHAR(C.CREATED_USERNAME) IS NULL  THEN '-' WHEN TO_CHAR(C.LAST_UPDATED_USERNAME) IS NULL AND TO_CHAR(C.CREATED_USERNAME) IS NOT NULL THEN TO_CHAR(C.CREATED_USERNAME) ELSE TO_CHAR(C.LAST_UPDATED_USERNAME) END AS PRINTED_BY
					, B.NAMA_KEC
					, B.NAMA_KEL
					, B.AGAMA
					, B.JENIS_PKRJN
					, B.STAT_KWN
					FROM  DEMOGRAPHICS_ALL@DB5CETAK B  
                    LEFT JOIN (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME,LAST_UPDATE FROM 
                    (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME,LAST_UPDATE, RANK() OVER (PARTITION BY NIK ORDER BY  PERSONALIZED_DATE DESC, LAST_UPDATE DESC, CHIP_ID DESC) RNK FROM 
                    CARD_MANAGEMENT@DB5CETAK) WHERE RNK = 1) C ON B.NIK = C.NIK  
                    LEFT JOIN (SELECT ID_CETAK,NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY,STEP_PROC,PROC_BY,ALASAN_PENGAJUAN FROM 
					(SELECT  ID_CETAK, NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY,STEP_PROC,PROC_BY,ALASAN_PENGAJUAN, RANK() OVER (PARTITION BY NIK ORDER BY REQ_DATE DESC,ID_CETAK DESC) RNK FROM 
                    SIAK_REQ_CETAK_KTP@YZDB) WHERE RNK = 1) A ON A.NIK =B.NIK 
                    WHERE 1=1 
                    AND B.CURRENT_STATUS_CODE NOT IN ('UNREGISTERED' ,'BIO_CAPTURED' ,'ENROLL_FAILURE_AT_REGIONAL' ,'PACKET_RETRY' ,'RECEIVED_AT_CENTRAL' ,'PROCESSING' ,'SENT_FOR_ENROLLMENT' ,'ENROLL_FAILURE_AT_CENTRAL' ,'MISSING_BIOMETRIC_EXCEPTION' ,'INVALID_PACKET' ,'SEARCH_FAILURE_AT_CENTRAL' ,'ADJUDICATE_RECORD' ,'ADJUDICATE_IN_PROCESS' ,'SENT_FOR_DEDUP' ,'DUPLICATE_RECORD' ,'PRINT_READY_RECORD') AND B.CURRENT_STATUS_CODE IS NOT NULL";
		
			if($nik != ""){
				$sql .= " AND B.NIK IN ($nik)";
			}
				$sql .= " ORDER BY B.NO_KEC, B.NO_KEL, B.NO_KK, B.NO_RW, B.NO_RT, B.NAMA_LGKP";
			$r = DB::connection('db222')->select($sql);
			return $r;
		}
		public function scr_tolak_list_pengajuan($nik,$no_kec){
			$sql = "";
			$sql .= "SELECT 
					CASE WHEN A.NIK IS NOT NULL THEN A.NO_KEC ELSE B.NO_KEC END NO_KEC
					, B.NIK
					, CONCAT(',',B.NIK) NIK
					, CONCAT(',',B.NO_KK) NO_KK
					, B.NAMA_LGKP AS NAMA_LENGKAP
					, B.TMPT_LHR
					, TO_CHAR(B.TGL_LHR,'DD-MM-YYYY') TGL_LHR
                    , B.ALAMAT
                    , CASE WHEN TO_CHAR(B.KODE_POS) IS NULL THEN '-' ELSE TO_CHAR(B.KODE_POS) END KODE_POS
                    , LPAD(TO_CHAR(B.NO_RT), 3, '0') AS RT
                    , LPAD(TO_CHAR(B.NO_RW), 3, '0') AS RW
                    , CASE WHEN B.CURRENT_STATUS_CODE IS NULL THEN 'DATA REKAM KOSONG' ELSE B.CURRENT_STATUS_CODE END AS CURRENT_STATUS_CODE
					, CASE WHEN TO_CHAR(A.REQ_DATE,'YYYY-MM-DD') IS NULL THEN '-' ELSE TO_CHAR(A.REQ_DATE,'YYYY-MM-DD')  END AS REQ_DATE
					, CASE WHEN A.REQ_BY IS NULL THEN '-' ELSE A.REQ_BY END REQ_BY
                    , CASE WHEN A.ALASAN_PENGAJUAN IS NULL THEN '-' ELSE A.ALASAN_PENGAJUAN END ALASAN_PENGAJUAN
					, CASE WHEN A.STEP_PROC = 'T' THEN 'DITOLAK' WHEN A.STEP_PROC = 'C' THEN 'DICETAK' WHEN A.STEP_PROC IS NULL THEN '-' ELSE 'PROSSESS' END AS STATUS_PENGAJUAN
					, CASE WHEN A.ALASAN_PENGAJUAN IS NOT NULL THEN A.ALASAN_PENGAJUAN WHEN A.STEP_PROC = 'T' THEN UPPER(A.PROC_BY) WHEN A.STEP_PROC = 'C' THEN 'SUDAH DICATAK' ELSE '-' END AS KETERANGAN_STATUS
					, CASE WHEN TO_CHAR(C.LAST_UPDATE,'DD-MM-YYYY') IS NULL AND TO_CHAR(C.PERSONALIZED_DATE,'DD-MM-YYYY') IS NULL THEN '-' WHEN TO_CHAR(C.LAST_UPDATE,'DD-MM-YYYY') IS NULL THEN TO_CHAR(C.PERSONALIZED_DATE,'DD-MM-YYYY') ELSE TO_CHAR(C.LAST_UPDATE,'DD-MM-YYYY') END AS PRINTED_DATE
					, CASE WHEN TO_CHAR(C.LAST_UPDATED_USERNAME) IS NULL AND TO_CHAR(C.CREATED_USERNAME) IS NULL  THEN '-' WHEN TO_CHAR(C.LAST_UPDATED_USERNAME) IS NULL AND TO_CHAR(C.CREATED_USERNAME) IS NOT NULL THEN TO_CHAR(C.CREATED_USERNAME) ELSE TO_CHAR(C.LAST_UPDATED_USERNAME) END AS PRINTED_BY
					, B.NAMA_KEC
					, B.NAMA_KEL
					, B.AGAMA
					, B.JENIS_PKRJN
					, B.STAT_KWN
					FROM  DEMOGRAPHICS_ALL@DB5CETAK B  
                    LEFT JOIN (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME,LAST_UPDATE FROM 
                    (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME,LAST_UPDATE, RANK() OVER (PARTITION BY NIK ORDER BY  PERSONALIZED_DATE DESC, LAST_UPDATE DESC, CHIP_ID DESC) RNK FROM 
                    CARD_MANAGEMENT@DB5CETAK) WHERE RNK = 1) C ON B.NIK = C.NIK  
                    LEFT JOIN (SELECT ID_CETAK,NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY,STEP_PROC,PROC_BY,ALASAN_PENGAJUAN FROM 
					(SELECT  ID_CETAK, NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY,STEP_PROC,PROC_BY,ALASAN_PENGAJUAN, RANK() OVER (PARTITION BY NIK ORDER BY  REQ_DATE DESC,ID_CETAK DESC) RNK FROM 
                    SIAK_REQ_CETAK_KTP@YZDB WHERE STEP_PROC <> 'C') WHERE RNK = 1) A ON A.NIK =B.NIK
                    LEFT JOIN SIAK_KTP_BLOKIR@YZDB E ON B.NIK = E.NIK
                    WHERE 1=1  AND ((B.CURRENT_STATUS_CODE IN ('UNREGISTERED' ,'BIO_CAPTURED' ,'ENROLL_FAILURE_AT_REGIONAL' ,'PACKET_RETRY' ,'RECEIVED_AT_CENTRAL' ,'PROCESSING' ,'SENT_FOR_ENROLLMENT' ,'ENROLL_FAILURE_AT_CENTRAL' ,'MISSING_BIOMETRIC_EXCEPTION' ,'INVALID_PACKET' ,'SEARCH_FAILURE_AT_CENTRAL' ,'ADJUDICATE_RECORD' ,'ADJUDICATE_IN_PROCESS' ,'SENT_FOR_DEDUP' ,'DUPLICATE_RECORD' ,'PRINT_READY_RECORD') OR B.CURRENT_STATUS_CODE IS NULL) OR (E.NIK IS NOT NULL AND E.STATUS = 1))";
		
			if($nik != ""){
				$sql .= " AND B.NIK IN ($nik)";
			}
				$sql .= " ORDER BY B.NO_KEC, B.NO_KEL, B.NO_KK, B.NO_RW, B.NO_RT, B.NAMA_LGKP";
			$r = DB::connection('db222')->select($sql);
			return $r;
		}

		public function scr_cek_list_pindahan($nik,$no_kec){
			$sql = "";
			$sql .= "SELECT 
					CASE WHEN A.NIK IS NULL THEN B.NO_KEC ELSE A.NO_KEC END NO_KEC
					, B.NIK
					, CONCAT(',',B.NIK) NIK
					, CONCAT(',',B.NO_KK) NO_KK
					, B.NAMA_LGKP AS NAMA_LENGKAP
					, B.TMPT_LHR
					, TO_CHAR(B.TGL_LHR,'DD-MM-YYYY') TGL_LHR
                    , B.ALAMAT
                    , CASE WHEN TO_CHAR(B.KODE_POS) IS NULL THEN '-' ELSE TO_CHAR(B.KODE_POS) END KODE_POS
                    , LPAD(TO_CHAR(B.NO_RT), 3, '0') AS RT
                    , LPAD(TO_CHAR(B.NO_RW), 3, '0') AS RW
                    , CASE WHEN B.CURRENT_STATUS_CODE IS NULL THEN 'DATA REKAM KOSONG' ELSE B.CURRENT_STATUS_CODE END AS CURRENT_STATUS_CODE
					, CASE WHEN TO_CHAR(A.REQ_DATE,'YYYY-MM-DD') IS NULL THEN '-' ELSE TO_CHAR(A.REQ_DATE,'YYYY-MM-DD')  END AS REQ_DATE
					, CASE WHEN A.REQ_BY IS NULL THEN '-' ELSE A.REQ_BY END REQ_BY
                    , CASE WHEN A.ALASAN_PENGAJUAN IS NULL THEN '-' ELSE A.ALASAN_PENGAJUAN END ALASAN_PENGAJUAN
					, CASE WHEN A.STEP_PROC = 'T' THEN 'DITOLAK' WHEN A.STEP_PROC = 'C' THEN 'DICETAK' WHEN A.STEP_PROC IS NULL THEN '-' ELSE 'PROSSESS' END AS STATUS_PENGAJUAN
					, CASE WHEN A.ALASAN_PENGAJUAN IS NOT NULL THEN A.ALASAN_PENGAJUAN WHEN A.STEP_PROC = 'T' THEN UPPER(A.PROC_BY) WHEN A.STEP_PROC = 'C' THEN 'SUDAH DICATAK' ELSE '-' END AS KETERANGAN_STATUS
					, CASE WHEN TO_CHAR(C.LAST_UPDATE,'DD-MM-YYYY') IS NULL AND TO_CHAR(C.PERSONALIZED_DATE,'DD-MM-YYYY') IS NULL THEN '-' WHEN TO_CHAR(C.LAST_UPDATE,'DD-MM-YYYY') IS NULL THEN TO_CHAR(C.PERSONALIZED_DATE,'DD-MM-YYYY') ELSE TO_CHAR(C.LAST_UPDATE,'DD-MM-YYYY') END AS PRINTED_DATE
					, CASE WHEN TO_CHAR(C.LAST_UPDATED_USERNAME) IS NULL AND TO_CHAR(C.CREATED_USERNAME) IS NULL  THEN '-' WHEN TO_CHAR(C.LAST_UPDATED_USERNAME) IS NULL AND TO_CHAR(C.CREATED_USERNAME) IS NOT NULL THEN TO_CHAR(C.CREATED_USERNAME) ELSE TO_CHAR(C.LAST_UPDATED_USERNAME) END AS PRINTED_BY
					, B.NAMA_KEC
					, B.NAMA_KEL
					, B.AGAMA
					, B.JENIS_PKRJN
					, B.STAT_KWN
					FROM  DEMOGRAPHICS@DB5CETAK B  
                    LEFT JOIN (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME,LAST_UPDATE FROM 
                    (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME,LAST_UPDATE, RANK() OVER (PARTITION BY NIK ORDER BY  PERSONALIZED_DATE DESC, LAST_UPDATE DESC, CHIP_ID DESC) RNK FROM 
                    CARD_MANAGEMENT@DB5CETAK) WHERE RNK = 1) C ON B.NIK = C.NIK  
                    LEFT JOIN (SELECT ID_CETAK,NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY,STEP_PROC,PROC_BY,ALASAN_PENGAJUAN FROM 
					(SELECT  ID_CETAK, NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY,STEP_PROC,PROC_BY,ALASAN_PENGAJUAN, RANK() OVER (PARTITION BY NIK ORDER BY REQ_DATE DESC,ID_CETAK DESC) RNK FROM 
                    SIAK_REQ_CETAK_KTP@YZDB) WHERE RNK = 1) A ON A.NIK =B.NIK LEFT JOIN SIAK_KTP_BLOKIR@YZDB E ON B.NIK = E.NIK 
                    WHERE 1=1 
                    AND (E.NIK IS NULL OR E.STATUS = 0)  
                    AND B.CURRENT_STATUS_CODE NOT IN ('UNREGISTERED' ,'BIO_CAPTURED' ,'ENROLL_FAILURE_AT_REGIONAL' ,'PACKET_RETRY' ,'RECEIVED_AT_CENTRAL' ,'PROCESSING' ,'SENT_FOR_ENROLLMENT' ,'ENROLL_FAILURE_AT_CENTRAL' ,'MISSING_BIOMETRIC_EXCEPTION' ,'INVALID_PACKET' ,'SEARCH_FAILURE_AT_CENTRAL' ,'ADJUDICATE_RECORD' ,'ADJUDICATE_IN_PROCESS' ,'SENT_FOR_DEDUP' ,'DUPLICATE_RECORD' ,'PRINT_READY_RECORD') AND B.CURRENT_STATUS_CODE IS NOT NULL";
			
			if($nik != ""){
				$sql .= " AND B.NIK IN ($nik)";
			}
				$sql .= " ORDER BY B.NO_KEC, B.NO_KEL, B.NO_KK, B.NO_RW, B.NO_RT, B.NAMA_LGKP";
			$r = DB::connection('db222')->select($sql);
			return $r;
		}

		public function scr_insert_nik_pindah($nik){
			$sql = "INSERT INTO DEMOGRAPHICS@DB2OLD SELECT NIK
					, NAMA_LGKP
					, TMPT_LHR
					, NAMA_PROP
					, NAMA_KAB
					, NAMA_KEC
					, NAMA_KEL
					, NO_PROP
					, NO_KAB
					, NO_KEC
					, NO_KEL
					, AGAMA
					, JENIS_KLMIN
					, GOL_DRH
					, STAT_KWN
					, TGL_LHR
					, IS_DIRTY
					, CURRENT_STATUS_CODE
					, CREATED
					, CREATED_USERNAME
					, NO_KTP
					, NO_PASPOR
					, NO_RT
					, NO_RW
					, PNYDNG_CCT
					, KLAIN_FSK
					, JENIS_PKRJN
					, KEBANGSAAN
					, GELAR
					, NO_AKTA_LHR
					, NO_AKTA_KWN
					, NO_AKTA_CRAI
					, NAMA_PET_REG
					, NAMA_LGKP_IBU
					, NIK_IBU
					, NAMA_LGKP_AYAH
					, NIK_AYAH
					, NO_KK
					, STAT_KTP
					, NAMA_KET_RT
					, NAMA_KET_RW
					, STAT_HBKEL
					, KET_AGAMA
					, TMPT_SBL
					, TGL_PJG_KTP
					, TGL_AKH_PASPOR
					, TGL_KWN
					, TGL_CRAI
					, TGL_ENTRI
					, TGL_UBAH
					, TGL_CETAK_KTP
					, TGL_GANTI_KTP
					, GLR_AGAMA
					, GLR_BANGSAWAN
					, GLR_AKADEMIS
					, EXCEPTION_CODE
					, LAST_UPDATED
					, LAST_UPDATED_USERNAME
					, UPLOAD_LOCATION
					, REGION_ID
					, LOCAL_ID
					, AKTA_LHR
					, AKTA_KWN
					, AKTA_CRAI
					, PDDK_AKH
					, STAT_HIDUP
					, FLAG_STATUS
					, NAMA_KEP
					, ALAMAT
					, DUSUN
					, KODE_POS
					, TELP
					, KWRNGRN
					, DOK_IMGR
					, TGL_DTBIT
					, TGL_AKH_DOK
					, SIAK_FLAG
					, COUNT_KTP
					, NO_DOK
					, NID_VK FROM DEMOGRAPHICS A WHERE NOT EXISTS (SELECT 1 FROM DEMOGRAPHICS@DB2OLD B WHERE A.NIK = B.NIK) AND A.NIK IN ($nik)";
			$r = DB::connection('db5cetak')->insert($sql);
			return $r;
		}

		public function scr_insert_nik_pindah_all($nik){
			$sql = "INSERT INTO DEMOGRAPHICS_ALL@DB2OLD SELECT  NIK, NAMA_LGKP, TMPT_LHR, NAMA_PROP, NAMA_KAB, NAMA_KEC, NAMA_KEL, NO_PROP, NO_KAB, NO_KEC, NO_KEL, AGAMA, JENIS_KLMIN, GOL_DRH, STAT_KWN, TGL_LHR, IS_DIRTY, CURRENT_STATUS_CODE, CREATED, CREATED_USERNAME, REGION_ID, LOCAL_ID, LAST_UPDATED, NO_RT, NO_RW, PNYDNG_CCT, KLAIN_FSK, JENIS_PKRJN, NAMA_LGKP_IBU, NIK_IBU, NAMA_LGKP_AYAH, NIK_AYAH, NO_KK, STAT_KTP, STAT_HBKEL, TGL_PJG_KTP, TGL_AKH_PASPOR, TGL_UBAH, TGL_CETAK_KTP, TGL_GANTI_KTP, GLR_AGAMA, GLR_BANGSAWAN, GLR_AKADEMIS, LAST_UPDATED_USERNAME, UPLOAD_LOCATION, AKTA_LHR, AKTA_KWN, AKTA_CRAI, PDDK_AKH, STAT_HIDUP, FLAG_STATUS, NAMA_KEP, ALAMAT, DUSUN, KODE_POS, TELP, NO_KTP, NO_PASPOR, KEBANGSAAN, GELAR, NO_AKTA_LHR, NO_AKTA_KWN, NO_AKTA_CRAI, NAMA_PET_REG, NAMA_KET_RT, NAMA_KET_RW, KET_AGAMA, TMPT_SBL, TGL_KWN, TGL_CRAI, TGL_ENTRI, EXCEPTION_CODE,NULL REGIONAL_FLAG,NULL CUTOFF, KWRNGRN, DOK_IMGR, TGL_DTBIT, TGL_AKH_DOK, SIAK_FLAG, COUNT_KTP, NO_DOK,NULL NID_VK FROM DEMOGRAPHICS@DB2OLD A WHERE NOT EXISTS (SELECT 1 FROM DEMOGRAPHICS_ALL@DB2OLD B WHERE A.NIK = B.NIK) AND A.NIK IN ($nik)";
			$r = DB::connection('db5cetak')->insert($sql);
			return $r;
		}

		public function scr_insert_nik_pindah_all5($nik){
			$sql = "INSERT INTO DEMOGRAPHICS_ALL@DB2OLD SELECT  NIK, NAMA_LGKP, TMPT_LHR, NAMA_PROP, NAMA_KAB, NAMA_KEC, NAMA_KEL, NO_PROP, NO_KAB, NO_KEC, NO_KEL, AGAMA, JENIS_KLMIN, GOL_DRH, STAT_KWN, TGL_LHR, IS_DIRTY, CURRENT_STATUS_CODE, CREATED, CREATED_USERNAME, REGION_ID, LOCAL_ID, LAST_UPDATED, NO_RT, NO_RW, PNYDNG_CCT, KLAIN_FSK, JENIS_PKRJN, NAMA_LGKP_IBU, NIK_IBU, NAMA_LGKP_AYAH, NIK_AYAH, NO_KK, STAT_KTP, STAT_HBKEL, TGL_PJG_KTP, TGL_AKH_PASPOR, TGL_UBAH, TGL_CETAK_KTP, TGL_GANTI_KTP, GLR_AGAMA, GLR_BANGSAWAN, GLR_AKADEMIS, LAST_UPDATED_USERNAME, UPLOAD_LOCATION, AKTA_LHR, AKTA_KWN, AKTA_CRAI, PDDK_AKH, STAT_HIDUP, FLAG_STATUS, NAMA_KEP, ALAMAT, DUSUN, KODE_POS, TELP, NO_KTP, NO_PASPOR, KEBANGSAAN, GELAR, NO_AKTA_LHR, NO_AKTA_KWN, NO_AKTA_CRAI, NAMA_PET_REG, NAMA_KET_RT, NAMA_KET_RW, KET_AGAMA, TMPT_SBL, TGL_KWN, TGL_CRAI, TGL_ENTRI, EXCEPTION_CODE,NULL REGIONAL_FLAG,NULL CUTOFF, KWRNGRN, DOK_IMGR, TGL_DTBIT, TGL_AKH_DOK, SIAK_FLAG, COUNT_KTP, NO_DOK,NULL NID_VK FROM DEMOGRAPHICS@DB5CETAK A WHERE NOT EXISTS (SELECT 1 FROM DEMOGRAPHICS_ALL@DB2OLD B WHERE A.NIK = B.NIK) AND A.NIK IN ($nik)";
			$r = DB::connection('db5cetak')->insert($sql);
			return $r;
		}

		public function scr_insert_nik_all($nik){
			$sql = "INSERT INTO DEMOGRAPHICS_ALL@DB5CETAK SELECT NIK, NAMA_LGKP, TMPT_LHR, NAMA_PROP, NAMA_KAB, NAMA_KEC, NAMA_KEL, NO_PROP, NO_KAB, NO_KEC, NO_KEL, AGAMA, JENIS_KLMIN, GOL_DRH, STAT_KWN, TGL_LHR, IS_DIRTY, CURRENT_STATUS_CODE, CREATED, CREATED_USERNAME, REGION_ID, LOCAL_ID, LAST_UPDATED, NO_RT, NO_RW, PNYDNG_CCT, KLAIN_FSK, JENIS_PKRJN, NAMA_LGKP_IBU, NIK_IBU, NAMA_LGKP_AYAH, NIK_AYAH, NO_KK, STAT_KTP, STAT_HBKEL, TGL_PJG_KTP, TGL_AKH_PASPOR, TGL_UBAH, TGL_CETAK_KTP, TGL_GANTI_KTP, GLR_AGAMA, GLR_BANGSAWAN, GLR_AKADEMIS, LAST_UPDATED_USERNAME, UPLOAD_LOCATION, AKTA_LHR, AKTA_KWN, AKTA_CRAI, PDDK_AKH, STAT_HIDUP, FLAG_STATUS, NAMA_KEP, ALAMAT, DUSUN, KODE_POS, TELP, NO_KTP, NO_PASPOR, KEBANGSAAN, GELAR, NO_AKTA_LHR, NO_AKTA_KWN, NO_AKTA_CRAI, NAMA_PET_REG, NAMA_KET_RT, NAMA_KET_RW, KET_AGAMA, TMPT_SBL, TGL_KWN, TGL_CRAI, TGL_ENTRI, EXCEPTION_CODE, REGIONAL_FLAG, CUTOFF, KWRNGRN, DOK_IMGR, TGL_DTBIT, TGL_AKH_DOK, SIAK_FLAG, COUNT_KTP, NO_DOK, NID_VK FROM DEMOGRAPHICS_ALL A WHERE NOT EXISTS (SELECT 1 FROM DEMOGRAPHICS_ALL@DB5CETAK B WHERE A.NIK = B.NIK) AND A.NIK IN ($nik)";
			$r = DB::connection('db5cetak')->insert($sql);
			return $r;
		}

		public function scr_insert_nik_all5($nik){
			$sql = "INSERT INTO DEMOGRAPHICS_ALL SELECT NIK, NAMA_LGKP, TMPT_LHR, NAMA_PROP, NAMA_KAB, NAMA_KEC, NAMA_KEL, NO_PROP, NO_KAB, NO_KEC, NO_KEL, AGAMA, JENIS_KLMIN, GOL_DRH, STAT_KWN, TGL_LHR, IS_DIRTY, CURRENT_STATUS_CODE, CREATED, CREATED_USERNAME, REGION_ID, LOCAL_ID, LAST_UPDATED, NO_RT, NO_RW, PNYDNG_CCT, KLAIN_FSK, JENIS_PKRJN, NAMA_LGKP_IBU, NIK_IBU, NAMA_LGKP_AYAH, NIK_AYAH, NO_KK, STAT_KTP, STAT_HBKEL, TGL_PJG_KTP, TGL_AKH_PASPOR, TGL_UBAH, TGL_CETAK_KTP, TGL_GANTI_KTP, GLR_AGAMA, GLR_BANGSAWAN, GLR_AKADEMIS, LAST_UPDATED_USERNAME, UPLOAD_LOCATION, AKTA_LHR, AKTA_KWN, AKTA_CRAI, PDDK_AKH, STAT_HIDUP, FLAG_STATUS, NAMA_KEP, ALAMAT, DUSUN, KODE_POS, TELP, NO_KTP, NO_PASPOR, KEBANGSAAN, GELAR, NO_AKTA_LHR, NO_AKTA_KWN, NO_AKTA_CRAI, NAMA_PET_REG, NAMA_KET_RT, NAMA_KET_RW, KET_AGAMA, TMPT_SBL, TGL_KWN, TGL_CRAI, TGL_ENTRI, EXCEPTION_CODE, REGIONAL_FLAG, CUTOFF, KWRNGRN, DOK_IMGR, TGL_DTBIT, TGL_AKH_DOK, SIAK_FLAG, COUNT_KTP, NO_DOK, NID_VK FROM DEMOGRAPHICS_ALL@DB5CETAK A WHERE NOT EXISTS (SELECT 1 FROM DEMOGRAPHICS_ALL B WHERE A.NIK = B.NIK) AND A.NIK IN ($nik)";
			$r = DB::connection('db5cetak')->insert($sql);
			return $r;
		}
		
		public function scr_do_ajukan_pindah($nik,$user_id,$no_kec){
			if($no_kec == 0){
				$sql = "INSERT INTO SIAK_REQ_CETAK_KTP ( NIK ,NO_KK ,NAMA_LENGKAP ,TGL_LAHIR ,TMP_LAHIR ,NO_PROP ,NO_KAB ,NO_KEC ,NO_KEL ,REQ_DATE ,REQ_BY ,PROC_DATE ,PROC_BY ,STEP_PROC ,SMS_COUNT ,ALAMAT ,NO_RT ,NO_RW ,DUSUN ,TELP ,KODE_POS ,JENIS_KLMIN ,ID_CETAK ,SMS_PHONE, ALASAN_PENGAJUAN) 
					SELECT B.NIK, B.NO_KK, B.NAMA_LGKP NAMA_LENGKAP, B.TGL_LHR TGL_LAHIR, B.TMPT_LHR TMP_LAHIR, B.NO_PROP, B.NO_KAB, B.NO_KEC, B.NO_KEL, SYSDATE REQ_DATE, '$user_id' REQ_BY, NULL PROC_DATE , NULL PROC_BY, 'A' STEP_PROC, 0 SMS_COUNT, B.ALAMAT, B.NO_RT, B.NO_RW, B.DUSUN,  NULL TELP, B.KODE_POS, CASE WHEN B.JENIS_KLMIN = 'LAKI-LAKI' THEN 1 ELSE 0 END JENIS_KLMIN, CONCAT(CONCAT(CONCAT(TRUNC(DBMS_RANDOM.VALUE(1000000000,9999999999)),'-'),TRUNC(DBMS_RANDOM.VALUE(1000000000,9999999999))),B.NIK) ID_CETAK, NULL SMS_PHONE, 'KEDATANGAN LUAR KOTA' ALASAN_PENGAJUAN FROM DEMOGRAPHICS_ALL B  
					WHERE NIK IN ($nik) AND NOT EXISTS (SELECT 1 FROM SIAK_REQ_CETAK_KTP D WHERE B.NIK = D.NIK AND D.STEP_PROC = 'A')";
			}else{
				$sql = "INSERT INTO SIAK_REQ_CETAK_KTP ( NIK ,NO_KK ,NAMA_LENGKAP ,TGL_LAHIR ,TMP_LAHIR ,NO_PROP ,NO_KAB ,NO_KEC ,NO_KEL ,REQ_DATE ,REQ_BY ,PROC_DATE ,PROC_BY ,STEP_PROC ,SMS_COUNT ,ALAMAT ,NO_RT ,NO_RW ,DUSUN ,TELP ,KODE_POS ,JENIS_KLMIN ,ID_CETAK ,SMS_PHONE, ALASAN_PENGAJUAN) 
					SELECT B.NIK, B.NO_KK, B.NAMA_LGKP NAMA_LENGKAP, B.TGL_LHR TGL_LAHIR, B.TMPT_LHR TMP_LAHIR, B.NO_PROP, B.NO_KAB, $no_kec NO_KEC, B.NO_KEL, SYSDATE REQ_DATE, '$user_id' REQ_BY, NULL PROC_DATE , NULL PROC_BY, 'A' STEP_PROC, 0 SMS_COUNT, B.ALAMAT, B.NO_RT, B.NO_RW, B.DUSUN,  NULL TELP, B.KODE_POS, CASE WHEN B.JENIS_KLMIN = 'LAKI-LAKI' THEN 1 ELSE 0 END JENIS_KLMIN, CONCAT(CONCAT(CONCAT(TRUNC(DBMS_RANDOM.VALUE(1000000000,9999999999)),'-'),TRUNC(DBMS_RANDOM.VALUE(1000000000,9999999999))),B.NIK) ID_CETAK, NULL SMS_PHONE, 'KEDATANGAN LUAR KOTA' ALASAN_PENGAJUAN FROM DEMOGRAPHICS_ALL B  
					WHERE NIK IN ($nik) AND NOT EXISTS (SELECT 1 FROM SIAK_REQ_CETAK_KTP D WHERE B.NIK = D.NIK AND D.STEP_PROC = 'A')";
			}
			
			$r = DB::insert($sql);
		}
		public function scr_do_ajukan_perubahan_data($nik,$user_id,$no_kec){
			if($no_kec == 0){
				$sql = "INSERT INTO SIAK_REQ_CETAK_KTP ( NIK ,NO_KK ,NAMA_LENGKAP ,TGL_LAHIR ,TMP_LAHIR ,NO_PROP ,NO_KAB ,NO_KEC ,NO_KEL ,REQ_DATE ,REQ_BY ,PROC_DATE ,PROC_BY ,STEP_PROC ,SMS_COUNT ,ALAMAT ,NO_RT ,NO_RW ,DUSUN ,TELP ,KODE_POS ,JENIS_KLMIN ,ID_CETAK ,SMS_PHONE, ALASAN_PENGAJUAN) 
					SELECT B.NIK, B.NO_KK, B.NAMA_LGKP NAMA_LENGKAP, B.TGL_LHR TGL_LAHIR, B.TMPT_LHR TMP_LAHIR, B.NO_PROP, B.NO_KAB, B.NO_KEC, B.NO_KEL, SYSDATE REQ_DATE, '$user_id' REQ_BY, NULL PROC_DATE , NULL PROC_BY, 'A' STEP_PROC, 0 SMS_COUNT, B.ALAMAT, B.NO_RT, B.NO_RW, B.DUSUN,  NULL TELP, B.KODE_POS, CASE WHEN B.JENIS_KLMIN = 'LAKI-LAKI' THEN 1 ELSE 0 END JENIS_KLMIN, CONCAT(CONCAT(CONCAT(TRUNC(DBMS_RANDOM.VALUE(1000000000,9999999999)),'-'),TRUNC(DBMS_RANDOM.VALUE(1000000000,9999999999))),B.NIK) ID_CETAK, NULL SMS_PHONE, 'PERUBAHAN ELEMEN' ALASAN_PENGAJUAN FROM DEMOGRAPHICS_ALL B 
					WHERE NIK IN ($nik) AND NOT EXISTS (SELECT 1 FROM SIAK_REQ_CETAK_KTP D WHERE B.NIK = D.NIK AND D.STEP_PROC = 'A')";
			}else{
				$sql = "INSERT INTO SIAK_REQ_CETAK_KTP ( NIK ,NO_KK ,NAMA_LENGKAP ,TGL_LAHIR ,TMP_LAHIR ,NO_PROP ,NO_KAB ,NO_KEC ,NO_KEL ,REQ_DATE ,REQ_BY ,PROC_DATE ,PROC_BY ,STEP_PROC ,SMS_COUNT ,ALAMAT ,NO_RT ,NO_RW ,DUSUN ,TELP ,KODE_POS ,JENIS_KLMIN ,ID_CETAK ,SMS_PHONE, ALASAN_PENGAJUAN) 
					SELECT B.NIK, B.NO_KK, B.NAMA_LGKP NAMA_LENGKAP, B.TGL_LHR TGL_LAHIR, B.TMPT_LHR TMP_LAHIR, B.NO_PROP, B.NO_KAB, $no_kec NO_KEC, B.NO_KEL, SYSDATE REQ_DATE, '$user_id' REQ_BY, NULL PROC_DATE , NULL PROC_BY, 'A' STEP_PROC, 0 SMS_COUNT, B.ALAMAT, B.NO_RT, B.NO_RW, B.DUSUN,  NULL TELP, B.KODE_POS, CASE WHEN B.JENIS_KLMIN = 'LAKI-LAKI' THEN 1 ELSE 0 END JENIS_KLMIN, CONCAT(CONCAT(CONCAT(TRUNC(DBMS_RANDOM.VALUE(1000000000,9999999999)),'-'),TRUNC(DBMS_RANDOM.VALUE(1000000000,9999999999))),B.NIK) ID_CETAK, NULL SMS_PHONE, 'PERUBAHAN ELEMEN' ALASAN_PENGAJUAN FROM DEMOGRAPHICS_ALL B 
					WHERE NIK IN ($nik) AND NOT EXISTS (SELECT 1 FROM SIAK_REQ_CETAK_KTP D WHERE B.NIK = D.NIK AND D.STEP_PROC = 'A')";
			}
			// echo $sql;
			// die;
			$r = DB::insert($sql);
		}
		public function scr_do_ajukan_rusak($nik,$user_id,$no_kec){
			if($no_kec == 0){
				$sql = "INSERT INTO SIAK_REQ_CETAK_KTP ( NIK ,NO_KK ,NAMA_LENGKAP ,TGL_LAHIR ,TMP_LAHIR ,NO_PROP ,NO_KAB ,NO_KEC ,NO_KEL ,REQ_DATE ,REQ_BY ,PROC_DATE ,PROC_BY ,STEP_PROC ,SMS_COUNT ,ALAMAT ,NO_RT ,NO_RW ,DUSUN ,TELP ,KODE_POS ,JENIS_KLMIN ,ID_CETAK ,SMS_PHONE, ALASAN_PENGAJUAN) 
			SELECT B.NIK, B.NO_KK, B.NAMA_LGKP NAMA_LENGKAP, B.TGL_LHR TGL_LAHIR, B.TMPT_LHR TMP_LAHIR, B.NO_PROP, B.NO_KAB, B.NO_KEC, B.NO_KEL, SYSDATE REQ_DATE, '$user_id' REQ_BY, NULL PROC_DATE , NULL PROC_BY, 'A' STEP_PROC, 0 SMS_COUNT, B.ALAMAT, B.NO_RT, B.NO_RW, B.DUSUN,  NULL TELP, B.KODE_POS, CASE WHEN B.JENIS_KLMIN = 'LAKI-LAKI' THEN 1 ELSE 0 END JENIS_KLMIN, CONCAT(CONCAT(CONCAT(TRUNC(DBMS_RANDOM.VALUE(1000000000,9999999999)),'-'),TRUNC(DBMS_RANDOM.VALUE(1000000000,9999999999))),B.NIK) ID_CETAK, NULL SMS_PHONE, 'RUSAK' ALASAN_PENGAJUAN FROM DEMOGRAPHICS_ALL B 
					WHERE NIK IN ($nik) AND NOT EXISTS (SELECT 1 FROM SIAK_REQ_CETAK_KTP D WHERE B.NIK = D.NIK AND D.STEP_PROC = 'A')";
			}else{
				$sql = "INSERT INTO SIAK_REQ_CETAK_KTP ( NIK ,NO_KK ,NAMA_LENGKAP ,TGL_LAHIR ,TMP_LAHIR ,NO_PROP ,NO_KAB ,NO_KEC ,NO_KEL ,REQ_DATE ,REQ_BY ,PROC_DATE ,PROC_BY ,STEP_PROC ,SMS_COUNT ,ALAMAT ,NO_RT ,NO_RW ,DUSUN ,TELP ,KODE_POS ,JENIS_KLMIN ,ID_CETAK ,SMS_PHONE, ALASAN_PENGAJUAN) 
			SELECT B.NIK, B.NO_KK, B.NAMA_LGKP NAMA_LENGKAP, B.TGL_LHR TGL_LAHIR, B.TMPT_LHR TMP_LAHIR, B.NO_PROP, B.NO_KAB, $no_kec NO_KEC, B.NO_KEL, SYSDATE REQ_DATE, '$user_id' REQ_BY, NULL PROC_DATE , NULL PROC_BY, 'A' STEP_PROC, 0 SMS_COUNT, B.ALAMAT, B.NO_RT, B.NO_RW, B.DUSUN,  NULL TELP, B.KODE_POS, CASE WHEN B.JENIS_KLMIN = 'LAKI-LAKI' THEN 1 ELSE 0 END JENIS_KLMIN, CONCAT(CONCAT(CONCAT(TRUNC(DBMS_RANDOM.VALUE(1000000000,9999999999)),'-'),TRUNC(DBMS_RANDOM.VALUE(1000000000,9999999999))),B.NIK) ID_CETAK, NULL SMS_PHONE, 'RUSAK' ALASAN_PENGAJUAN FROM DEMOGRAPHICS_ALL B 
					WHERE NIK IN ($nik) AND NOT EXISTS (SELECT 1 FROM SIAK_REQ_CETAK_KTP D WHERE B.NIK = D.NIK AND D.STEP_PROC = 'A')";
			}
			
			$r = DB::insert($sql);
		}
		public function scr_do_ajukan_hilang($nik,$user_id,$no_kec){
			if($no_kec == 0){
				$sql = "INSERT INTO SIAK_REQ_CETAK_KTP ( NIK ,NO_KK ,NAMA_LENGKAP ,TGL_LAHIR ,TMP_LAHIR ,NO_PROP ,NO_KAB ,NO_KEC ,NO_KEL ,REQ_DATE ,REQ_BY ,PROC_DATE ,PROC_BY ,STEP_PROC ,SMS_COUNT ,ALAMAT ,NO_RT ,NO_RW ,DUSUN ,TELP ,KODE_POS ,JENIS_KLMIN ,ID_CETAK ,SMS_PHONE, ALASAN_PENGAJUAN) 
					SELECT B.NIK, B.NO_KK, B.NAMA_LGKP NAMA_LENGKAP, B.TGL_LHR TGL_LAHIR, B.TMPT_LHR TMP_LAHIR, B.NO_PROP, B.NO_KAB, B.NO_KEC, B.NO_KEL, SYSDATE REQ_DATE, '$user_id' REQ_BY, NULL PROC_DATE , NULL PROC_BY, 'A' STEP_PROC, 0 SMS_COUNT, B.ALAMAT, B.NO_RT, B.NO_RW, B.DUSUN,  NULL TELP, B.KODE_POS, CASE WHEN B.JENIS_KLMIN = 'LAKI-LAKI' THEN 1 ELSE 0 END JENIS_KLMIN, CONCAT(CONCAT(CONCAT(TRUNC(DBMS_RANDOM.VALUE(1000000000,9999999999)),'-'),TRUNC(DBMS_RANDOM.VALUE(1000000000,9999999999))),B.NIK) ID_CETAK, NULL SMS_PHONE, 'HILANG' ALASAN_PENGAJUAN FROM DEMOGRAPHICS_ALL B  
					WHERE NIK IN ($nik) AND NOT EXISTS (SELECT 1 FROM SIAK_REQ_CETAK_KTP D WHERE B.NIK = D.NIK AND D.STEP_PROC = 'A')";
			}else{
				$sql = "INSERT INTO SIAK_REQ_CETAK_KTP ( NIK ,NO_KK ,NAMA_LENGKAP ,TGL_LAHIR ,TMP_LAHIR ,NO_PROP ,NO_KAB ,NO_KEC ,NO_KEL ,REQ_DATE ,REQ_BY ,PROC_DATE ,PROC_BY ,STEP_PROC ,SMS_COUNT ,ALAMAT ,NO_RT ,NO_RW ,DUSUN ,TELP ,KODE_POS ,JENIS_KLMIN ,ID_CETAK ,SMS_PHONE, ALASAN_PENGAJUAN) 
					SELECT B.NIK, B.NO_KK, B.NAMA_LGKP NAMA_LENGKAP, B.TGL_LHR TGL_LAHIR, B.TMPT_LHR TMP_LAHIR, B.NO_PROP, B.NO_KAB, $no_kec NO_KEC, B.NO_KEL, SYSDATE REQ_DATE, '$user_id' REQ_BY, NULL PROC_DATE , NULL PROC_BY, 'A' STEP_PROC, 0 SMS_COUNT, B.ALAMAT, B.NO_RT, B.NO_RW, B.DUSUN,  NULL TELP, B.KODE_POS, CASE WHEN B.JENIS_KLMIN = 'LAKI-LAKI' THEN 1 ELSE 0 END JENIS_KLMIN, CONCAT(CONCAT(CONCAT(TRUNC(DBMS_RANDOM.VALUE(1000000000,9999999999)),'-'),TRUNC(DBMS_RANDOM.VALUE(1000000000,9999999999))),B.NIK) ID_CETAK, NULL SMS_PHONE, 'HILANG' ALASAN_PENGAJUAN FROM DEMOGRAPHICS_ALL B  
					WHERE NIK IN ($nik) AND NOT EXISTS (SELECT 1 FROM SIAK_REQ_CETAK_KTP D WHERE B.NIK = D.NIK AND D.STEP_PROC = 'A')";
			}
			
			$r = DB::insert($sql);
		}
		public function scr_do_ajukan_ld($nik,$nama,$no_kec,$user_id){
			$sql = "INSERT INTO SIAK_REQ_CETAK_KTPLD 
								( NIK
								, NAMA_LENGKAP
								, NO_KEC
								, REQ_DATE
								, REQ_BY
								, PROC_DATE
								, PROC_BY
								, STEP_PROC
								, ID_CETAK)  
								SELECT $nik
								, '$nama'
								, $no_kec
								, SYSDATE
								, '$user_id'
								, NULL
								, NULL
								, 'A'
								, CONCAT(CONCAT(CONCAT(TRUNC(DBMS_RANDOM.VALUE(1000000000,9999999999)),'-'),TRUNC(DBMS_RANDOM.VALUE(1000000000,9999999999))),$nik)
								FROM DUAL WHERE NOT EXISTS (SELECT 1 FROM SIAK_REQ_CETAK_KTPLD D WHERE D.NIK = $nik AND D.STEP_PROC = 'A')";
			$r = DB::insert($sql);
		}
		public function scr_do_update_ld($nik,$nama,$no_kec,$user_id){
			$sql = "UPDATE SIAK_REQ_CETAK_KTPLD 
								SET REQ_DATE = SYSDATE
								, REQ_BY = '$user_id'
								, STEP_PROC = 'A'
								 WHERE NIK = '$nik'";
			$r = DB::update($sql);
		}
		public function scr_do_delete_ld($nik,$nama,$no_kec,$user_id){
			$sql = "DELETE SIAK_REQ_CETAK_KTPLD WHERE NIK = '$nik'";
			$r = DB::delete($sql);
		}
		public function scr_cek_list_hilang($tgl_start,$tgl_end,$no_kec){
			$sql = "";
			$sql .= "SELECT 
					CASE WHEN A.NIK IS NULL THEN X.NO_KEC ELSE A.NO_KEC END NO_KEC
					, X.NIK DO_NIK
					, CONCAT(',',X.NIK) NIK
					, CONCAT(',',X.NO_KK) NO_KK
					, X.NAMA_LGKP AS NAMA_LENGKAP
					, X.TMPT_LHR
					, TO_CHAR(X.TGL_LHR,'DD-MM-YYYY') TGL_LHR
                    , X.ALAMAT
                    , CASE WHEN TO_CHAR(X.KODE_POS) IS NULL THEN '-' ELSE TO_CHAR(X.KODE_POS) END KODE_POS
                    , LPAD(TO_CHAR(X.NO_RT), 3, '0') AS RT
                    , LPAD(TO_CHAR(X.NO_RW), 3, '0') AS RW
					, CASE WHEN TO_CHAR(A.REQ_DATE,'YYYY-MM-DD') IS NULL THEN '-' ELSE TO_CHAR(A.REQ_DATE,'YYYY-MM-DD')  END AS REQ_DATE
					, CASE WHEN A.REQ_BY IS NULL THEN '-' ELSE A.REQ_BY END REQ_BY
                    , CASE WHEN A.ALASAN_PENGAJUAN IS NULL THEN '-' ELSE A.ALASAN_PENGAJUAN END ALASAN_PENGAJUAN
                    , CASE WHEN A.NO_KEHILANGAN IS NULL THEN '-' ELSE A.NO_KEHILANGAN END NO_KEHILANGAN
					, F5_GET_NAMA_KECAMATAN(X.NO_PROP,X.NO_KAB,X.NO_KEC) NAMA_KEC
					, F5_GET_NAMA_KELURAHAN(X.NO_PROP,X.NO_KAB,X.NO_KEC,X.NO_KEL) NAMA_KEL
					FROM DEMOGRAPHICS_ALL X
                    LEFT JOIN (SELECT ID_CETAK,NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY,STEP_PROC,PROC_BY,ALASAN_PENGAJUAN,NO_KEHILANGAN FROM 
					(SELECT  ID_CETAK, NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY,STEP_PROC,PROC_BY,ALASAN_PENGAJUAN,NO_KEHILANGAN, RANK() OVER (PARTITION BY NIK ORDER BY REQ_DATE DESC,ID_CETAK DESC) RNK FROM 
                    SIAK_REQ_CETAK_KTP@YZDB WHERE STEP_PROC <> 'C') WHERE RNK = 1) A ON A.NIK =X.NIK
                    WHERE 1=1 AND A.ALASAN_PENGAJUAN = 'HILANG' ";
			if($no_kec != 0){
				$sql .= " AND X.NO_KEC = $no_kec";
			}
			$sql .= " AND A.REQ_DATE >= TO_DATE('$tgl_start','DD-MM-YYYY') AND A.REQ_DATE < TO_DATE('$tgl_end','DD-MM-YYYY') +1";			
			$r = DB::connection('db222')->select($sql);
			return $r;
		}
	
	public function src_get_cek_pengajuan_hilang($nik)
    {
        $sql = "SELECT A.NIK FROM SIAK_REQ_CETAK_KTP A WHERE A.NIK = $nik AND ALASAN_PENGAJUAN = 'HILANG' ";
        $r = DB::select($sql);
        return $r;
    }

    public function src_update_pengajuan_hilang($nik_hilang, $no_kehilangan)
    {
        $sql = "UPDATE SIAK_REQ_CETAK_KTP SET KEHILANGAN = 1, NO_KEHILANGAN = upper('$no_kehilangan') WHERE NIK = $nik_hilang AND ALASAN_PENGAJUAN = 'HILANG'";
        $r = DB::update($sql);
        return $r;
    }

    // Siak Terpusat
    public function req_ektp_ld(Request $request) 
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 101;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$data = array(
		 		"stitle"=>'Pengajuan KTP Pindahan',
		 		"mtitle"=>'Pengajuan KTP Pindahan',
		 		"my_url"=>'input_cetak',
		 		"type_tgl"=>'Request',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			return view('pengajuan_ld/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function get_nik_ld(Request $request) 
	{
		if($request->nik != null){
			$nik = $request->nik;
			$r = $this->blk_get_nik_ld($nik);
			if(count($r) > 0){
				$data["success"] = TRUE;
				$data["is_exists"] = 1;
				$data["is_ulang"] = 0;
        		$data["nik"] = $r[0]->nik;
        		$data["nama"] = $r[0]->nama_lgkp; 
        		$data["no_kec"] = $r[0]->no_kec;
        		$data["nama_kec"] = $r[0]->nama_kec;
        		return $data;
			}else{
				$r2 = $this->blk_get_nik_pengajuan_ld($nik);
				if(count($r2) > 0){
					$data["success"] = TRUE;
					$data["is_exists"] = 1;
					$data["is_ulang"] = 1;
	        		$data["nik"] = $r2[0]->nik;
	        		$data["req_date"] = $r2[0]->req_date;
	        		$data["nama"] = $r2[0]->nama_lgkp; 
	        		$data["no_kec"] = $r2[0]->no_kec;
	        		$data["nama_kec"] = $r2[0]->nama_kec;
	        		return $data;
				}else{
					$data["is_exists"] = 0;
	        		return $data;
				}
			}
		}else{
			return redirect()->route('logout');
		}
	}
	public function blk_get_nik_ld($nik){
			$sql = "SELECT 
					NIK
					, NAMA_LGKP
					, NO_KEC
					, NAMA_KEC
					FROM DEMOGRAPHICS_ALL@DB5CETAK
					WHERE NIK = $nik";
			$r = DB::connection('db222')->select($sql);
			return $r;
		}


	public function blk_get_nik_pengajuan_ld($nik){
			$sql = "SELECT 
					NIK
					, NAMA_LENGKAP NAMA_LGKP
					, NO_KEC
					, F5_GET_NAMA_KECAMATAN(32,73,NO_KEC) NAMA_KEC
					, TO_CHAR(REQ_DATE,'DD-MM-YYYY')  REQ_DATE
					FROM SIAK_REQ_CETAK_KTPLD
					WHERE NIK = $nik";
			$r = DB::select($sql);
			return $r;
		}

}
