<?php

namespace App\Http\Controllers;

use DB;
use Session;
use DateTime;
use Excel;
use PDF;
use App\Biodatawni;
use App\Exports\BiodatawniExport;
use Illuminate\Http\Request;
use App\Http\Controllers\SharedController as Shr;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class NikController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

     public function Nik_duplicate(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 77;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->nik != null || $request->no_kec != null){
			if($request->nik != null){
			$nik = $request->nik;
			if (substr($nik, 0, 1) === ','){
				$nik = ltrim($nik, ',');
			}
				$d = $this->cek_data_duplicate($nik);
				$r = [];
			}else{
				$no_kec = $request->no_kec;
				$no_kel = $request->no_kel;
				$d = [];
				$r = $this->prr_get_list_duplicate($no_kec,$no_kel);
			}
			
			
			$data = array(
		 		"stitle"=>'Ktp-el Cek Duplicate Monitoring',
		 		"mtitle"=>'Ktp-el Cek Duplicate Monitoring',
		 		"my_url"=>'Duplicate',
		 		"type_tgl"=>'Rekam',
		 		"data"=>$r,
		 		"data_duplicate"=>$d,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			}else{
			$data = array(
		 		"stitle"=>'Ktp-el Cek Duplicate Monitoring',
		 		"mtitle"=>'Ktp-el Cek Duplicate Monitoring',
		 		"my_url"=>'Duplicate',
		 		"type_tgl"=>'Rekam',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
    		}
			return view('Ceklistduplicate/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function Nik_cetak(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
		  	$menu_id = 75;
	        $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
		  if($request->nik != null || $request->no_kk != null || $request->nama_lgkp != null || $request->tgl_lhr != null){
			$nik = $request->nik;
				if (substr($nik, 0, 1) === ','){
					$nik = ltrim($nik, ',');
				}
	      if (substr($nik, -1, 1) === ','){
	        $nik = rtrim($nik, ',');
	      }
				$no_kk = $request->no_kk;
				if (substr($no_kk, 0, 1) === ','){
					$no_kk = ltrim($no_kk, ',');
				}
	      if (substr($no_kk, -1, 1) === ','){
	        $no_kk = rtrim($no_kk, ',');
	      }
	      
				$nama_lgkp = $request->nama_lgkp;
		        $tgl_lhr = $request->tgl_lhr;
		        $no_kec = $request->no_kec;
		        $no_kel = $request->no_kel;
				$r = $this->scr_cek_data_rekam($nik,$no_kk,$nama_lgkp,$tgl_lhr,$no_kec,$no_kel);
				$data = array(
			 		"stitle"=>'Ktp-el Cek Status',
			 		"mtitle"=>'Ktp-el Cek Status',
			 		"my_url"=>'Ktpel',
			 		"type_tgl"=>'Cetak',
			 		"data"=>$r,
			 		"jumlah"=>count($r),
			 		"menu"=>$menu,
	            	"akses_kec"=>$isakses_kec,
	            	"akses_kel"=>$isakses_kel,
			 		"user_id"=>$request->session()->get('S_USER_ID'),
			 		"user_nik"=>$request->session()->get('S_NIK'),
			 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
			 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
			 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
			 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
			 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
			 		"user_level"=>$request->session()->get('S_USER_LEVEL')
	    		);
				}else{
				$data = array(
			 		"stitle"=>'Ktp-el Cek Status',
			 		"mtitle"=>'Ktp-el Cek Status',
			 		"my_url"=>'Ktpel',
			 		"type_tgl"=>'Cetak',
			 		"menu"=>$menu,
	            	"akses_kec"=>$isakses_kec,
	            	"akses_kel"=>$isakses_kel,
			 		"user_id"=>$request->session()->get('S_USER_ID'),
			 		"user_nik"=>$request->session()->get('S_NIK'),
			 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
			 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
			 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
			 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
			 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
			 		"user_level"=>$request->session()->get('S_USER_LEVEL')
	    		);
    		}
			return view('ceknik/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function Nikwna_cetak(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
		  	$menu_id = 282;
	        $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
		  if($request->nik != null || $request->no_kk != null || $request->nama_lgkp != null || $request->tgl_lhr != null){
			$nik = $request->nik;
				if (substr($nik, 0, 1) === ','){
					$nik = ltrim($nik, ',');
				}
	      if (substr($nik, -1, 1) === ','){
	        $nik = rtrim($nik, ',');
	      }
				$no_kk = $request->no_kk;
				if (substr($no_kk, 0, 1) === ','){
					$no_kk = ltrim($no_kk, ',');
				}
	      if (substr($no_kk, -1, 1) === ','){
	        $no_kk = rtrim($no_kk, ',');
	      }
	      
				$nama_lgkp = $request->nama_lgkp;
		        $tgl_lhr = $request->tgl_lhr;
		        $no_kec = $request->no_kec;
		        $no_kel = $request->no_kel;
				// $r = $this->scr_cek_data_rekam_wna($nik,$no_kk,$nama_lgkp,$tgl_lhr,$no_kec,$no_kel);
				$data = array(
			 		"stitle"=>'Ktp-el Cek Status (WNA)',
			 		"mtitle"=>'Ktp-el Cek Status (WNA)',
			 		"my_url"=>'KtpelWna',
			 		"type_tgl"=>'Cetak',
			 		"data"=>$r,
			 		"jumlah"=>count($r),
			 		"menu"=>$menu,
	            	"akses_kec"=>$isakses_kec,
	            	"akses_kel"=>$isakses_kel,
			 		"user_id"=>$request->session()->get('S_USER_ID'),
			 		"user_nik"=>$request->session()->get('S_NIK'),
			 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
			 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
			 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
			 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
			 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
			 		"user_level"=>$request->session()->get('S_USER_LEVEL')
	    		);
				}else{
				$data = array(
			 		"stitle"=>'Ktp-el Cek Status (WNA)',
			 		"mtitle"=>'Ktp-el Cek Status (WNA)',
			 		"my_url"=>'KtpelWna',
			 		"type_tgl"=>'Cetak',
			 		"menu"=>$menu,
	            	"akses_kec"=>$isakses_kec,
	            	"akses_kel"=>$isakses_kel,
			 		"user_id"=>$request->session()->get('S_USER_ID'),
			 		"user_nik"=>$request->session()->get('S_NIK'),
			 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
			 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
			 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
			 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
			 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
			 		"user_level"=>$request->session()->get('S_USER_LEVEL')
	    		);
    		}
			return view('ceknik/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}

	
	public function Nik_kia(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 80;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->no_kec != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$r = $this->prr_get_list_kia($tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'List Kia',
		 		"mtitle"=>'List Kia '.$tgl,
		 		"my_url"=>'Kia',
		 		"type_tgl"=>'Tanggal Cetak',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			}else{
			$data = array(
		 		"stitle"=>'List Kia',
		 		"mtitle"=>'List Kia',
		 		"my_url"=>'Kia',
		 		"type_tgl"=>'Tanggal Cetak',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
    		}
			return view('Ceknik_kia/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}

	
	public function Nik_skts(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 270;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->no_kec != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$r = $this->prr_get_list_skts($tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'List Skts',
		 		"mtitle"=>'List Skts '.$tgl,
		 		"my_url"=>'Skts',
		 		"type_tgl"=>'Tanggal Verivikasi',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			}else{
			$data = array(
		 		"stitle"=>'List Skts',
		 		"mtitle"=>'List Skts',
		 		"my_url"=>'Skts',
		 		"type_tgl"=>'Tanggal Verivikasi',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
    		}
			return view('Ceknik_skts/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function do_push(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
		  $menu_id = 75;
	        $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->nik != null){
			$nik = $request->nik;
			if (substr($nik, 0, 1) === ','){
				$nik = ltrim($nik, ',');
			}
			$this->scr_push_demogh221($nik);
			$this->scr_push_demogh2($nik);
			$this->scr_push_demoghall($nik);
			$r = $this->scr_cek_data_rekam($nik);
			$data = array(
		 		"stitle"=>'Re-Push Status Ktp-el',
		 		"mtitle"=>'Re-Push Status Ktp-el',
		 		"my_url"=>'Push',
		 		"type_tgl"=>'Cetak',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
            	"akses_kec"=>$isakses_kec,
            	"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			}else{
			$data = array(
		 		"stitle"=>'Re-Push Status Ktp-el',
		 		"mtitle"=>'Re-Push Status Ktp-el',
		 		"my_url"=>'Push',
		 		"type_tgl"=>'Cetak',
		 		"menu"=>$menu,
            	"akses_kec"=>$isakses_kec,
            	"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
    		}
			return view('ceknik_dopush/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function Nik_history(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 76;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->nik != null){
			$nik = $request->nik;
			if (substr($nik, 0, 1) === ','){
				$nik = ltrim($nik, ',');
			}
			
			$r = $this->scr_cek_data_history($nik,$request->session()->get('S_NO_KEC'));
			$data = array(
		 		"stitle"=>'Ktp-el Cek History Cetak',
		 		"mtitle"=>'Ktp-el Cek History Cetak',
		 		"my_url"=>'History',
		 		"type_tgl"=>'Cetak',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			}else{
			$data = array(
		 		"stitle"=>'Ktp-el Cek History Cetak',
		 		"mtitle"=>'Ktp-el Cek History Cetak',
		 		"my_url"=>'History',
		 		"type_tgl"=>'Cetak',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
    		}
			return view('cekhistory/main',$data);
		}else{
			return redirect()->route('logout');
		}
		
	}
	public function Nik_kk(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 129;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->no_kk != null){
			$no_kk = $request->no_kk;
			if (substr($no_kk, 0, 1) === ','){
				$no_kk = ltrim($no_kk, ',');
			}
	        if (substr($no_kk, -1, 1) === ','){
	            $no_kk = rtrim($no_kk, ',');
	        }
			$r = $this->scr_cek_kk($no_kk,$request->session()->get('S_NO_KEC'));
			$data = array(
		 		"stitle"=>'Cek Status KK',
		 		"mtitle"=>'Cek Status KK',
		 		"my_url"=>'KK',
		 		"type_tgl"=>'Cetak',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			}else{
			$data = array(
		 		"stitle"=>'Cek Status KK',
		 		"mtitle"=>'Cek Status KK',
		 		"my_url"=>'KK',
		 		"type_tgl"=>'Cetak',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
    		}
			return view('cekkk/main',$data);
		}else{
			return redirect()->route('logout');
		}
		
	}
	public function kkdo_push(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 129;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->no_kk != null){
			$no_kk = $request->no_kk;
			if (substr($no_kk, 0, 1) === ','){
				$no_kk = ltrim($no_kk, ',');
			}
		      $this->scr_push_data_kel($no_kk);
		      $this->scr_push_data_bsre($no_kk);
		      $r = $this->scr_cek_kk($no_kk);
			$data = array(
		 		"stitle"=>'Repush Status KK',
		 		"mtitle"=>'Repush Status KK',
		 		"my_url"=>'KK',
		 		"type_tgl"=>'Cetak',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			}else{
			$data = array(
		 		"stitle"=>'Repush Status KK',
		 		"mtitle"=>'Repush Status KK',
		 		"my_url"=>'KK',
		 		"type_tgl"=>'Cetak',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
    		}
			return view('cekkk_dopush/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function Nik_kkroket(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 131;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->no_kec != null){
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$r = $this->prr_get_list_kkroket($no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'List KK Roket',
		 		"mtitle"=>'List KK Roket',
		 		"my_url"=>'KKRoket',
		 		"type_tgl"=>'Tanggal Pengajuan',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			}else{
			$data = array(
		 		"stitle"=>'List KK Roket',
		 		"mtitle"=>'List KK Roket',
		 		"my_url"=>'KKRoket',
		 		"type_tgl"=>'Tanggal Pengajuan',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
    		}
			return view('Cekkkroket/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function Nik_prr(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 78;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->no_kec != null){
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$r = $this->prr_get_list_prr($no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'List PRR',
		 		"mtitle"=>'List PRR',
		 		"my_url"=>'Prr',
		 		"type_tgl"=>'Tanggal Rekam',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			}else{
			$data = array(
		 		"stitle"=>'List PRR',
		 		"mtitle"=>'List PRR',
		 		"my_url"=>'Prr',
		 		"type_tgl"=>'Tanggal Rekam',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
    		}
			return view('Cekprr/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function Nik_kia_balita(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 316;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->no_kec != null){
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$jumnik = $request->jumnik;
			$r = $this->prr_get_list_kia_balita($no_kec,$no_kel,$jumnik);
			$data = array(
		 		"stitle"=>'List Kia Balita',
		 		"mtitle"=>'List Kia Balita',
		 		"my_url"=>'KiaBalita',
		 		"type_tgl"=>'Tanggal',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"is_colnotwil"=>1,
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			}else{
			$data = array(
		 		"stitle"=>'List Kia Balita',
		 		"mtitle"=>'List Kia Balita',
		 		"my_url"=>'KiaBalita',
		 		"type_tgl"=>'Tanggal',
		 		"is_colnotwil"=>1,
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
    		}
			return view('Cekkiabalita/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function Nik_kia_pengajuan(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 326;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->no_kec != null){
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$r = $this->prr_get_list_kia_pengajuan($tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'List Kia Pengajuan',
		 		"mtitle"=>'List Kia Pengajuan',
		 		"my_url"=>'KiaPengajuan',
		 		"type_tgl"=>'Tanggal',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"is_colnotwil"=>1,
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			}else{
			$data = array(
		 		"stitle"=>'List Kia Pengajuan',
		 		"mtitle"=>'List Kia Pengajuan',
		 		"my_url"=>'KiaPengajuan',
		 		"type_tgl"=>'Tanggal',
		 		"is_colnotwil"=>1,
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
    		}
			return view('Cekkiapengajuan/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function Nik_srekam(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 80;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->no_kec != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$nik = $request->nik;
			$no_kk = $request->no_kk;
			$r = $this->prr_get_list_perekaman($tgl_start,$tgl_end,$no_kec,$no_kel,$nik,$no_kk);
			$data = array(
		 		"stitle"=>'List Perekaman',
		 		"mtitle"=>'List Perekaman '.$tgl,
		 		"my_url"=>'Rekam',
		 		"type_tgl"=>'Tanggal Rekam',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			}else{
			$data = array(
		 		"stitle"=>'List Perekaman',
		 		"mtitle"=>'List Perekaman',
		 		"my_url"=>'Rekam',
		 		"type_tgl"=>'Tanggal Rekam',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
    		}
			return view('Ceksperekaman/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function Cetak_kk(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 177;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->no_kec != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$r = $this->prr_get_list_cetak_kk($tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'List Cetak KK',
		 		"mtitle"=>'List Cetak KK '.$tgl,
		 		"my_url"=>'Cetak_kk',
		 		"type_tgl"=>'Tanggal Cetak KK',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			}else{
			$data = array(
		 		"stitle"=>'List Cetak KK',
		 		"mtitle"=>'List Cetak KK',
		 		"my_url"=>'Cetak_kk',
		 		"type_tgl"=>'Tanggal Cetak KK',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
    		}
			return view('Cekscetakkk/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function Nik_baru(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 114;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->no_kec != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			// $r = $this->prr_get_list_nik_baru($tgl_start,$tgl_end,$no_kec,$no_kel);
			$r2 = $this->prr_get_group_nik_baru($tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'List Nik Baru',
		 		"mtitle"=>'List Nik Baru '.$tgl,
		 		"my_url"=>'Nik_baru',
		 		"type_tgl"=>'Tanggal Nik Baru',
		 		// "data"=>$r,
		 		"data2"=>$r2,
		 		// "jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			}else{
			$data = array(
		 		"stitle"=>'List Nik Baru',
		 		"mtitle"=>'List Nik Baru',
		 		"my_url"=>'Nik_baru',
		 		"type_tgl"=>'Tanggal Nik Baru',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
    		}
			return view('Ceksnikbaru/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function Nik_scetak(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 81;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->no_kec != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$user = $request->user;
			$r = $this->prr_get_list_sudah_cetak($tgl_start,$tgl_end,$no_kec,$no_kel,$user);
			$data = array(
		 		"stitle"=>'List Sudah Cetak '.$tgl,
		 		"mtitle"=>'List Sudah Cetak '.$tgl,
		 		"my_url"=>'Cetak',
		 		"type_tgl"=>'Tanggal Cetak',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			}else{
			$data = array(
		 		"stitle"=>'List Sudah Cetak',
		 		"mtitle"=>'List Sudah Cetak',
		 		"my_url"=>'Cetak',
		 		"type_tgl"=>'Tanggal Cetak',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
    		}
			return view('Cekscetak/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function Nik_list_sfe(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 83;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->no_kec != null){
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$r = $this->prr_get_list_sfe($no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'List Sent For Enrollment',
		 		"mtitle"=>'List Sent For Enrollment',
		 		"my_url"=>'Sfe',
		 		"type_tgl"=>'Tanggal Rekam',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			}else{
			$data = array(
		 		"stitle"=>'List Sent For Enrollment',
		 		"mtitle"=>'List Sent For Enrollment',
		 		"my_url"=>'Sfe',
		 		"type_tgl"=>'Tanggal Rekam',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
    		}
			return view('Ceklistsfe/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function Nik_list_cetak_suket(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 82;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->no_kec != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$nik = rtrim($request->nik, ',');
			$r = $this->prr_get_list_cetak_suket($tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'List Cetak Suket',
		 		"mtitle"=>'List Cetak Suket',
		 		"my_url"=>'ListCetakSuket',
		 		"type_tgl"=>'Tanggal Cetak',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			}else{
			$data = array(
		 		"stitle"=>'List Cetak Suket',
		 		"mtitle"=>'List Cetak Suket',
		 		"my_url"=>'ListCetakSuket',
		 		"type_tgl"=>'Tanggal Cetak',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
    		}
			return view('Cekcetaksuket/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function Nik_list_failure(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 74;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->no_kec != null){
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$r = $this->prr_get_list_failure($no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'List Failure',
		 		"mtitle"=>'List Failure',
		 		"my_url"=>'Failure',
		 		"type_tgl"=>'Tanggal Rekam',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			}else{
			$data = array(
		 		"stitle"=>'List Failure',
		 		"mtitle"=>'List Failure',
		 		"my_url"=>'Failure',
		 		"type_tgl"=>'Tanggal Rekam',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
    		}
			return view('Ceklistfailure/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function Nik_belum_rekam(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 121;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->no_kec != null){
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$r = $this->prr_get_list_belum_rekam($no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'Target Perekaman',
		 		"mtitle"=>'Target Perekaman',
		 		"my_url"=>'NotRecord',
		 		"type_tgl"=>'Tanggal Lahir',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			}else{
			$data = array(
		 		"stitle"=>'Target Perekaman',
		 		"mtitle"=>'Target Perekaman',
		 		"my_url"=>'NotRecord',
		 		"type_tgl"=>'Tanggal Lahir',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
    		}
			return view('Cekbelumrekam/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function Nik_prsuket(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 79;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->no_kec != null){
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$r = $this->prr_get_list_pr_suket($no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'List PR Suket',
		 		"mtitle"=>'List PR Suket',
		 		"my_url"=>'Suket',
		 		"type_tgl"=>'Tanggal Rekam',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			}else{
			$data = array(
		 		"stitle"=>'List PR Suket',
		 		"mtitle"=>'List PR Suket',
		 		"my_url"=>'Suket',
		 		"type_tgl"=>'Tanggal Rekam',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
    		}
			return view('Cekprsuket/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function Delete_suket(Request $request)
	{
		header('Content-type: application/json');
		$no_dokumen = $request->no_dokumen;
		$this->del_suket($no_dokumen);
		$output = array(
			"message_type"=>1,
			"message"=>"Suket Berhasil Dihapus."
		);
		return $output;
	}
	public function Nik_pemula(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 118;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->tanggal != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$r = $this->prr_nik_pemula($tgl_start,$tgl_end);
			$data = array(
		 		"stitle"=>'Nik Pemula',
		 		"mtitle"=>'Nik Pemula',
		 		"my_url"=>'Beginner',
		 		"type_tgl"=>'Tanggal',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			}else{
			$data = array(
		 		"stitle"=>'Nik Pemula',
		 		"mtitle"=>'Nik Pemula',
		 		"my_url"=>'Beginner',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
    		}
			return view('Nikpemula/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function Fcl_prr(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 71;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->no_kec != null){
			$no_kec = $request->no_kec;
			$no_kel = 0;
			$r = $this->prr_get_list_prr_fcl($no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'List PRR Gerai',
		 		"mtitle"=>'List PRR Gerai',
		 		"my_url"=>'FclPrr',
		 		"type_tgl"=>'Tanggal Rekam',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			}else{
			$data = array(
		 		"stitle"=>'List PRR Gerai',
		 		"mtitle"=>'List PRR Gerai',
		 		"my_url"=>'FclPrr',
		 		"type_tgl"=>'Tanggal Rekam',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
    		}
			return view('Fclprr/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function Fcl_scetak(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 73;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->tanggal != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $request->no_kec;
			$no_kel = 0;
			$r = $this->prr_get_list_sudah_cetak_fcl($tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'List Sudah Cetak Gerai',
		 		"mtitle"=>'List Sudah Cetak Gerai'.$tgl,
		 		"my_url"=>'FclCetak',
		 		"type_tgl"=>'Tanggal Cetak',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			}else{
			$data = array(
		 		"stitle"=>'List Sudah Cetak Gerai',
		 		"mtitle"=>'List Sudah Cetak Gerai',
		 		"my_url"=>'FclCetak',
		 		"type_tgl"=>'Tanggal Cetak',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
    		}
			return view('fclscetak/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function Fcl_srekam(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 71;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->tanggal != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $request->no_kec;
			$no_kel = 0;
			$r = $this->prr_get_list_perekaman_fcl($tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'List Perekaman Gerai',
		 		"mtitle"=>'List Perekaman Gerai'.$tgl,
		 		"my_url"=>'FclRekam',
		 		"type_tgl"=>'Tanggal Rekam',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			}else{
			$data = array(
		 		"stitle"=>'List Perekaman Gerai',
		 		"mtitle"=>'List Perekaman Gerai',
		 		"my_url"=>'FclRekam',
		 		"type_tgl"=>'Tanggal Rekam',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
    		}
			return view('fclsperekaman/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function input_lintas_batas(Request $request) 
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 251;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$data = array(
		 		"stitle"=>'Input Lintas Batas',
		 		"mtitle"=>'Input Lintas Batas',
		 		"my_url"=>'InputLintasBatas',
		 		"type_tgl"=>'Request',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			return view('lintas_batas/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function cek_lintas_batas(Request $request) 
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 252;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->nik != null OR $request->nama != null){
			$nik = $request->nik;
			if (substr($nik, 0, 1) === ','){
				$nik = ltrim($nik, ',');
			}
			$nama = $request->nama;
			$r = $this->plb_cek_rekap_lintas_batas($nik,$nama);
			$data = array(
		 		"stitle"=>'Pencarian Lintas Batas',
		 		"mtitle"=>'Pencarian Lintas Batas',
		 		"my_url"=>'CekLintasBatas',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			}else{
			$data = array(
		 		"stitle"=>'Pencarian Lintas Batas',
		 		"mtitle"=>'Pencarian Lintas Batas',
		 		"my_url"=>'CekLintasBatas',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
    		}
			return view('lintas_batas_cek/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function cek_lintas_batas_lihat(Request $request, $id) 
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 252;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
		    if($id != null){
		    $nik = $id;
		    $data = $this->plb_cek_rekap_lintas_batas($nik,'');
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$data = array(
		 		"stitle"=>'Pencarian Lintas Batas',
		 		"mtitle"=>'Pencarian Lintas Batas',
		 		"my_url"=>'CekLintasBatas',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"data"=>$data,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
    		);
			return view('lintas_batas_lihat/main',$data);
			}else{
				return redirect()->route('logout');
			}
			}else{
			return redirect()->route('logout');
		}
	}
	public function cek_lintas_batas_edit(Request $request, $id) 
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 252;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
		    if($id != null){
		    $nik = $id;
		    $data = $this->plb_cek_rekap_lintas_batas($nik,'');
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$data = array(
		 		"stitle"=>'Pencarian Lintas Batas',
		 		"mtitle"=>'Pencarian Lintas Batas',
		 		"my_url"=>'CekLintasBatas',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"data"=>$data,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
    		);
			return view('lintas_batas_edit/main',$data);
			}else{
				return redirect()->route('logout');
			}
			}else{
			return redirect()->route('logout');
		}
	}
	public function plb_get_nik(Request $request) 
	{
		if($request->nik != null){
			$nik = $request->nik;
			$j = $this->do_plb_get_nik($nik);
			if($j > 0){
				$r = $this->do_plb_get_data_nik($nik);
				$data["success"] = TRUE;
				$data["is_exists"] = 1;
        		$data["nama"] = $r[0]->nama_lgkp;
        		$data["tmpt_lhr"] = $r[0]->tmpt_lhr;
        		$data["tgl_lhr"] = $r[0]->tgl_lhr;
        		$data["umur"] = $r[0]->umur;
        		$data["jenis_klmin"] = $r[0]->jenis_klmin;
        		$data["jenis_pkrjn"] = $r[0]->pekerjaan;
        		$data["alamat"] = $r[0]->alamat;
        		$data["rt"] = $r[0]->rt;
        		$data["rw"] = $r[0]->rw;
        		$data["no_prop"] = $r[0]->no_prop;
        		$data["nama_prop"] = $r[0]->nama_prop;
        		$data["no_kab"] = $r[0]->no_kab;
        		$data["nama_kab"] = $r[0]->nama_kab;
        		$data["no_kec"] = $r[0]->no_kec;
        		$data["nama_kec"] = $r[0]->nama_kec;
        		$data["no_kel"] = $r[0]->no_kel;
        		$data["nama_kel"] = $r[0]->nama_kel;
        		return $data;
			}else{
				$data["success"] = TRUE;
				$data["is_exists"] = 0;
        		return $data;
			}
		}else{
			return redirect()->route('logout');
		}
	}
	public function plb_do_save(Request $request) 
	{
		if($request->plb != null){
			$plb = $request->plb;
			$nik = $request->nik;
			$nama = str_replace('\'', '',$request->nama);
			$umur = $request->umur;
			$tgl_lhr = $request->tgl_lhr;
			$tmpt_lhr = $request->tmpt_lhr;
			$jenis_klmin = $request->jenis_klmin;
			$jenis_pkrjn = $request->jenis_pkrjn;
			$no_prop = $request->no_prop;
			$no_kab = $request->no_kab;
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$alamat = $request->alamat;
			$rt = $request->rt;
			$rw = $request->rw;
			$j = $this->do_plb_get_data($nik);
			if($j > 0){
			$data["success"] = FALSE;
			$data["message"] = "Nik Sudah Terdaftar PLB !";
       		return $data;
			}else{
			$this->plb_save($plb,$nik,$nama,$umur,$tgl_lhr,$tmpt_lhr,$jenis_klmin,$jenis_pkrjn,$no_prop,$no_kab,$no_kec,$no_kel,$alamat,$rt,$rw,$request->session()->get('S_USER_ID'));
			$data["success"] = TRUE;
			$data["message"] = "Data Berhasil Di Simpan";
       		return $data;
       		}
		}else{
			return redirect()->route('logout');
		}
	}
	public function plb_do_edit(Request $request) 
	{
		if($request->plb != null){
			$plb = $request->plb;
			$nik = $request->nik;
			
			$this->plb_edit($plb,$nik,$request->session()->get('S_USER_ID'));
			$data["success"] = TRUE;
			$data["message"] = "Data Berhasil Di Ubah";
       		return $data;
		}else{
			return redirect()->route('logout');
		}
	}
	public function plb_do_delete(Request $request) 
	{
		if($request->nik != null){
			$nik = $request->nik;
			$this->plb_delete($nik);
			$data["success"] = TRUE;
			$data["message"] = "Data Berhasil Di Hapus";
       		return $data;
		}else{
			return redirect()->route('logout');
		}
	}
	public function input_petugas_khusus(Request $request) 
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 253;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$data = array(
		 		"stitle"=>'Input Petugas Khusus',
		 		"mtitle"=>'Input Petugas Khusus',
		 		"my_url"=>'InputLintasBatas',
		 		"type_tgl"=>'Request',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			return view('petugas_khusus/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function cek_petugas_khusus(Request $request) 
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 254;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->nik != null OR $request->nama != null){
			$nik = $request->nik;
			if (substr($nik, 0, 1) === ','){
				$nik = ltrim($nik, ',');
			}
			$nama = $request->nama;
			$r = $this->ph_cek_rekap_petugas_khusus($nik,$nama);
			$data = array(
		 		"stitle"=>'Pencarian Petugas Khusus',
		 		"mtitle"=>'Pencarian Petugas Khusus',
		 		"my_url"=>'CekPetugasKhusus',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			}else{
			$data = array(
		 		"stitle"=>'Pencarian Petugas Khusus',
		 		"mtitle"=>'Pencarian Petugas Khusus',
		 		"my_url"=>'CekPetugasKhusus',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
    		}
			return view('petugas_khusus_cek/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function cek_petugas_khusus_lihat(Request $request, $id) 
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 254;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
		    if($id != null){
		    $nik = $id;
		    $data = $this->ph_cek_rekap_petugas_khusus($nik,'');
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$data = array(
		 		"stitle"=>'Pencarian Petugas Khusus',
		 		"mtitle"=>'Pencarian Petugas Khusus',
		 		"my_url"=>'CekPetugasKhusus',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"data"=>$data,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
    		);
			return view('petugas_khusus_lihat/main',$data);
			}else{
				return redirect()->route('logout');
			}
			}else{
			return redirect()->route('logout');
		}
	}
	public function cek_petugas_khusus_edit(Request $request, $id) 
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 254;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
		    if($id != null){
		    $nik = $id;
		    $data = $this->ph_cek_rekap_petugas_khusus($nik,'');
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$data = array(
		 		"stitle"=>'Pencarian Petugas Khusus',
		 		"mtitle"=>'Pencarian Petugas Khusus',
		 		"my_url"=>'CekPetugasKhusus',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"data"=>$data,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
    		);
			return view('petugas_khusus_edit/main',$data);
			}else{
				return redirect()->route('logout');
			}
			}else{
			return redirect()->route('logout');
		}
	}
	public function ph_get_nik(Request $request) 
	{
		if($request->nik != null){
			$nik = $request->nik;
			$j = $this->do_ph_get_nik($nik);
			if($j > 0){
				$r = $this->do_ph_get_data_nik($nik);
				$data["success"] = TRUE;
				$data["is_exists"] = 1;
        		$data["nama"] = $r[0]->nama_lgkp;
        		$data["tmpt_lhr"] = $r[0]->tmpt_lhr;
        		$data["tgl_lhr"] = $r[0]->tgl_lhr;
        		$data["umur"] = $r[0]->umur;
        		$data["jenis_klmin"] = $r[0]->jenis_klmin;
        		$data["jenis_pkrjn"] = $r[0]->pekerjaan;
        		$data["alamat"] = $r[0]->alamat;
        		$data["rt"] = $r[0]->rt;
        		$data["rw"] = $r[0]->rw;
        		$data["no_prop"] = $r[0]->no_prop;
        		$data["nama_prop"] = $r[0]->nama_prop;
        		$data["no_kab"] = $r[0]->no_kab;
        		$data["nama_kab"] = $r[0]->nama_kab;
        		$data["no_kec"] = $r[0]->no_kec;
        		$data["nama_kec"] = $r[0]->nama_kec;
        		$data["no_kel"] = $r[0]->no_kel;
        		$data["nama_kel"] = $r[0]->nama_kel;
        		return $data;
			}else{
				$data["success"] = TRUE;
				$data["is_exists"] = 0;
        		return $data;
			}
		}else{
			return redirect()->route('logout');
		}
	}
	public function ph_do_save(Request $request) 
	{
		if($request->ph != null){
			$ph = $request->ph;
			$nik = $request->nik;
			$nama = str_replace('\'', '',$request->nama);
			$umur = $request->umur;
			$tgl_lhr = $request->tgl_lhr;
			$tmpt_lhr = $request->tmpt_lhr;
			$jenis_klmin = $request->jenis_klmin;
			$jenis_pkrjn = $request->jenis_pkrjn;
			$no_prop = $request->no_prop;
			$no_kab = $request->no_kab;
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$alamat = $request->alamat;
			$rt = $request->rt;
			$rw = $request->rw;
			$j = $this->do_ph_get_data($nik);
			if($j > 0){
			$data["success"] = FALSE;
			$data["message"] = "Nik Sudah Terdaftar ph !";
       		return $data;
			}else{
			$this->ph_save($ph,$nik,$nama,$umur,$tgl_lhr,$tmpt_lhr,$jenis_klmin,$jenis_pkrjn,$no_prop,$no_kab,$no_kec,$no_kel,$alamat,$rt,$rw,$request->session()->get('S_USER_ID'));
			$data["success"] = TRUE;
			$data["message"] = "Data Berhasil Di Simpan";
       		return $data;
       		}
		}else{
			return redirect()->route('logout');
		}
	}
	public function ph_do_edit(Request $request) 
	{
		if($request->ph != null){
			$ph = $request->ph;
			$nik = $request->nik;
			
			$this->ph_edit($ph,$nik,$request->session()->get('S_USER_ID'));
			$data["success"] = TRUE;
			$data["message"] = "Data Berhasil Di Ubah";
       		return $data;
		}else{
			return redirect()->route('logout');
		}
	}
	public function ph_do_delete(Request $request) 
	{
		if($request->nik != null){
			$nik = $request->nik;
			$this->ph_delete($nik);
			$data["success"] = TRUE;
			$data["message"] = "Data Berhasil Di Hapus";
       		return $data;
		}else{
			return redirect()->route('logout');
		}
	}

	public function nik_export(Request $request) {
		if($request->niks != null){

			$nik = $request->niks;
			if (substr($nik, 0, 1) === ','){
				$nik = ltrim($nik, ',');
			}
		$data_rekam =  $this->scr_export_data_nik($nik);
		$export = new BiodatawniExport($data_rekam);
    	 return Excel::download($export, 'Biodatawni.xlsx');
		 } 
        else 
        {
           redirect('/','refresh');
        }

    }
    
	public function nik_pdf(Request $request) {

		if($request->niks != null){
			$nik = $request->niks;
			if (substr($nik, 0, 1) === ','){
				$nik = ltrim($nik, ',');
			}
        $fileName = strtoupper($request->session()->get('S_USER_ID')).'_DATA_REKAM_'.time().'.xlsx';  
		$data_rekam =  $this->scr_export_data_rekam($nik);
		
        $html = "";
        $i=0;
        foreach ($data_rekam as $val) 
        {
        $i++;
        $html .= '
			<!-- EXAMPLE OF CSS STYLE -->
			<style>

			    table.first {
			    	padding: 0px;    
			    	margin: 0px;    
			        font-family: helvetica;
			        font-size: 8pt;

			        
			    }
			</style>
			<table class="first" cellpadding="0" cellspacing="5" border="0" ';
			if($i>1){$html .='style="page-break-before: always;"';}
			$html .='>
			 <tr>
			  <td width="50" align="left">Nomor</td>
			  <td width="20" align="center">:</td>
			  <td width="140" align="left">'.time().'-Disdukcapil</td>
			  <td width="80" align="center"></td>
			  <td width="60" align="center"></td>
			  <td width="20" align="center"></td>
			  <td width="220" align="left">Bandung,'.date('d-m-Y').'</td>
			 </tr>
			 <tr>
			  <td width="50" align="left">Lampiran</td>
			  <td width="20" align="center">:</td>
			  <td width="140" align="left">-</td>
			  <td width="80" align="center"></td>
			  <td width="60" align="center"></td>
			  <td width="20" align="center"></td>
			  <td width="220" align="left">Kepada</td>
			 </tr>
			 <tr>
			  <td width="50" align="left">Perihal</td>
			  <td width="20" align="center">:</td>
			  <td width="210" align="left">Resi Pengajuan Ktp-El</td>
			  <td width="10" align="center"></td>
			  <td width="60" align="right">Yth.</td>
			  <td width="20" align="center">:</td>
			  <td width="220" align="left"><B>'.$val->nama_lgkp.'</B></td>
			 </tr>
			</table>
			<br>
			<br>
			<br>
			<table class="first" cellpadding="0" cellspacing="5" border="0">
			 <tr>
			  <td colspan = "7" align="center"><h2>RESI PENGAJUAN PENCETAKAN E-KTP</h2></td>
			 </tr>
			 <tr>
			  <td colspan = "7" align="center"><br></td>
			 </tr>
			 <tr>
			  <td width="120" align="left">NIK</td>
			  <td width="20" align="center">:</td>
			  <td width="210" align="left"><B>'.$val->nik.'</B></td>
			  <td width="80" align="center"></td>
			  <td width="60" align="center" colspan ="3"></td>
			  
			 </tr>
			 <tr>
			  <td width="120" align="left">Nama Lengkap</td>
			  <td width="20" align="center">:</td>
			  <td width="210" align="left"><B>'.$val->nama_lgkp.'</B></td>
			  <td width="80" align="center"></td>
			  <td width="60" align="center"></td>
			  <td width="20" align="center"></td>
			  <td width="50" align="left"></td>
			 </tr>
			 <tr>
			  <td width="120" align="left">Tempat/Tanggal Lahir</td>
			  <td width="20" align="center">:</td>
			  <td width="210" align="left"><B>'.$val->tmpt_lhr.', '.$val->tgl_lhr.'</B></td>
			  <td width="80" align="center"></td>
			  <td width="60" align="center"></td>
			  <td width="20" align="center"></td>
			  <td width="50" align="left"></td>
			 </tr>
			 <tr>
			  <td width="120" align="left">Jenis Kelamin</td>
			  <td width="20" align="center">:</td>
			  <td width="210" align="left"><B>'.$val->jenis_klmin.'</B></td>
			  <td width="80" align="center"></td>
			  <td width="60" align="center"></td>
			  <td width="20" align="center"></td>
			  <td width="50" align="left"></td>
			 </tr>
			 <tr>
			  <td width="120" align="left">Alamat</td>
			  <td width="20" align="center">:</td>
			  <td width="210" align="left"><B>'.$val->alamat.'</B></td>
			  <td width="80" align="center"></td>
			  <td width="60" align="center"></td>
			  <td width="20" align="center"></td>
			  <td width="50" align="left"></td>
			 </tr>
			 <tr>
			  <td width="120" align="left">&#160;&#160;&#160;&#160;&#160;&#160;RT/RW</td>
			  <td width="20" align="center">:</td>
			  <td width="210" align="left"><B>'.$val->rt.'/'.$val->rw.'</B></td>
			  <td width="80" align="center"></td>
			  <td width="60" align="center"></td>
			  <td width="20" align="center"></td>
			  <td width="50" align="left"></td>
			 </tr>
			 <tr>
			  <td width="120" align="left">&#160;&#160;&#160;&#160;&#160;&#160;Kelurahan</td>
			  <td width="20" align="center">:</td>
			  <td width="210" align="left"><B>'.$val->nama_kel.'</B></td>
			  <td width="80" align="center"></td>
			  <td width="60" align="center"></td>
			  <td width="20" align="center"></td>
			  <td width="50" align="left"></td>
			 </tr>
			 <tr>
			  <td width="120" align="left">&#160;&#160;&#160;&#160;&#160;&#160;Kecamatan</td>
			  <td width="20" align="center">:</td>
			  <td width="210" align="left"><B>'.$val->nama_kec.'</B></td>
			  <td width="80" align="center"></td>
			  <td width="60" align="center"></td>
			  <td width="20" align="center"></td>
			  <td width="50" align="left"></td>
			 </tr>
			 <tr>
			  <td width="120" align="left">Status E-Ktp</td>
			  <td width="20" align="center">:</td>
			  <td width="210" align="left"><B>'.$val->current_status_code.'</B></td>
			  <td width="80" align="center"></td>
			  <td width="60" align="center"></td>
			  <td width="20" align="center"></td>
			  <td width="50" align="left"></td>
			 </tr>
			 <tr>
			  <td width="120" align="left">Tanggal Pengajuan E-Ktp</td>
			  <td width="20" align="center">:</td>
			  <td width="210" align="left"><B>'.$val->req_date.'</B></td>
			  <td width="80" align="center"></td>
			  <td width="60" align="center"></td>
			  <td width="20" align="center"></td>
			  <td width="50" align="left"></td>
			 </tr>
			 <tr>
			  <td width="120" align="left">Diajukan E-Ktp Oleh</td>
			  <td width="20" align="center">:</td>
			  <td width="210" align="left"><B>'.$val->req_bX.'</B></td>
			  <td width="80" align="center"></td>
			  <td width="60" align="center"></td>
			  <td width="20" align="center"></td>
			  <td width="50" align="left"></td>
			 </tr>
			</table>
			<br>
			<br>
			<br>
			<table class="first" cellpadding="0" cellspacing="5" border="0">
			 <tr>
			  <td colspan = "7" align="left"><p>Keterangan Pengajuan Pencetakan E-ktp Ini Dikeluarkan Oleh '.$request->session()->get('S_NAMA_LGKP').' Selaku Operator Dinas Kependudukan Dan Pencatatan Sipil Kota Bandung Di-'.$request->session()->get('S_NAMA_KANTOR').', Melalui Aplikasi Monitoring Dinas Kependudukan Dan Pencatatan Sipil Kota Bandung</p><br><p>Resi Ini Kami Buat Sebagai Bukti Bahwa Telah Mengajukan Pencetakan KTP-el, Dan <b>Tidak Dapat</b> Dipergunakan Untuk Kepentingan Pemilu, Pemilukada, Pilkades, Perbankan, Imigrasi, Kepolisian, Asuransi, BPJS, Pernikahan, dan lain-lain. Kepada yang berkepentingan agar menjadi maklum.</p></td>
			 </tr>
			</table>
			';
		
}
        return  PDF::loadHTML($html)->setPaper('a4', 'portraits')
          ->setWarnings(false)
          ->stream(strtoupper($request->session()->get('S_USER_ID')).'_DATA_REKAM_'.time().'.pdf');
         } 
        else 
        {
           redirect('/','refresh');
        }
    }

    public function nik_pdf_ppdb(Request $request) {

		if($request->niks != null){
			$nik = $request->niks;
			if (substr($nik, 0, 1) === ','){
				$nik = ltrim($nik, ',');
			}
        $fileName = strtoupper($request->session()->get('S_USER_ID')).'_DATA_REKAM_'.time().'.xlsx';  
		$data_rekam =  $this->scr_export_data_ppdb($nik);
		$image_cop = url('/').'/assets/plugins/images/pemkot.png';
		$image_ttd = url('/').'/assets/plugins/images/ttd_kadis_ttg.png';
		$css_pdf = url('/').'/assets/plugins/bower_components/cetak/css/style.css';
        $html = "";
        $i=0;
        foreach ($data_rekam as $val) 
        {
        $i++;
        $urut = $this->scr_get_urut_ppdb($val->no_kec,$val->no_kel);
        $no_surat = '3273'.$val->no_kec.''.$val->no_kel.'/SURKET/03/'.date('dmY').'/'.$urut.'';
        $this->scr_hist_data_ppdb($val->nik,$request->session()->get('S_USER_ID'),$no_surat,$val->no_kec,$val->no_kel,$val->tgl_datang);
        $html .= '
			<!-- EXAMPLE OF CSS STYLE -->
			<style>
			</style>
			<link href="'.$css_pdf.'" rel="stylesheet" type="text/css" />
			<table width="95%" class="tengah" ';
			if($i>1){$html .='style="page-break-before: always;"';}
			$html .='>
				<tr>
					<td height="20" colspan="2"></td>
				</tr>
				<tr>
					<td width="1%" rowspan="6" class="tengah vtengah">
				
				
				<img src="'.$image_cop.'" width="100" height="100" />  
				
					</td>
			</tr>
				<tr>
					<td class="font18"><strong>PEMERINTAH KOTA BANDUNG</strong></td>
				</tr>
				<tr>
					<td class="font18"><strong>DINAS KEPENDUDUKAN DAN PENCATATAN SIPIL KOTA BANDUNG</strong></td>
				</tr>
				<tr>
					<td class="font14">Jalan Ambon No.1 B Telp. (022) 4209891 Faksimil : (022) 4218695</td>
				</tr>
				<tr>
					<td class="font14">Website : disdukcapil.bandung.go.id</td>
				</tr>
				<tr>
					<td class="font14">BANDUNG - 40117</td>
				</tr>
		 </table>
		<table width="95%">
					<tr>
				<td height="4"></td>
			</tr>
			<tr>
				<td bgcolor="#333333" height="1"></td>
			</tr>
			<tr>
				<td height="1"></td>
			</tr>
			<tr>
				<td bgcolor="#333333" height="1"></td>
			</tr>
			<tr>
				<td height="15"></td>
			</tr>
			 </table>
			 <table width="90%">
					<tr>
						<td class="font15 tengah" colspan="3"><u><strong>SURAT KETERANGAN</strong></u></td>
					</tr>
					<tr>
					<td width="27%"></td>
					<td width="73%" class="font13" colspan="2" style="line-height:5px; margin-top:8px!important;"><strong>Nomor : '.$no_surat.'</strong></td>
					</tr>
					<tr>
						<td height="22px"></td>
					</tr>
					<tr>
						<td class="font13" colspan="3">Yang bertanda tangan di bawah ini :</td>
					</tr>
					<tr>
						<td height="10px"></td>
					</tr>
			 </table>
			<table width="90%" class="atas">
					<tr>
					 <td width="20%">Nama Lengkap</td>
					 <td width="1%">:</td>
					 <td width="79%">H. TATANG MUHTAR, S.Sos., M.Si.</td>
					</tr>
					<tr>
						<td>NIP</td>
						<td>:</td>
					<td>196806021989031004</td>
					</tr>
					<tr>
						<td>Jabatan</td>
						<td>:</td>
					<td>Kepala Dinas Kependudukan dan Pencatatan Sipil Kota Bandung</td>
					</tr>
					<tr>
						<td height="15px"></td>
					</tr>
			 </table>
			 <table  width="90%">
					<tr>
						<td>Menerangkan bahwa :</td>
					</tr>
					<tr>
						<td height="10px"></td>
					</tr>
			 </table>
			 <table width="90%">
					<tr>
						<td width="20%">NIK</td>
						<td width="1%">:</td>
						<td width="60%"> '.$val->nik.'</td>
						<td width="19%" rowspan="7" valign="top" class="tengah"> 
								<div style="position: absolute; left: 648px; top: 318px; z-index: -1;">
								</div>
						</td>
					</tr>
					<tr>
						<td>Nama Lengkap</td>
						<td>:</td>
						<td> 
							'.$val->nama_lgkp.'
						</td>
						
					</tr>
					<tr>
						<td>Tempat/Tanggal Lahir</td>
						<td>:</td>
						<td>'.$val->tgl_lhr.'</td>
				</tr>
					<tr>
						<td>Jenis Kelamin</td>
						<td>:</td>
						<td>'.$val->jenis_klmin.'</td>
				</tr>
				<tr>
						<td>Golongan Darah</td>
						<td>:</td>
						<td>'.$val->gol_drh.'</td>
				</tr>
					<tr>
						<td>Alamat</td>
						<td>:</td>
						<td>'.$val->alamat.'</td>
				</tr>
					<tr>
						<td>&#160;&#160;&#160;&#160;&#160;&#160;RT/RW</td>
						<td>:</td>
						<td>'.$val->rt.'/'.$val->rw.'</td>
				</tr>
					<tr>
						<td>&#160;&#160;&#160;&#160;&#160;&#160;Kelurahan/Desa</td>
						<td>:</td>
						<td>'.$val->nama_kel.'</td>
				</tr>
					<tr>
						<td>&#160;&#160;&#160;&#160;&#160;&#160;Kecamatan</td>
						<td>:</td>
						<td>'.$val->nama_kec.'</td>
						<td rowspan="5">
							<div style="position: absolute; left: 652px; top: 438px; z-index: -5;">
								
							</div>
						</td>
				</tr>	
				<tr>
						<td>Agama</td>
						<td>:</td>
						<td>'.$val->agama.'</td>
				</tr>
					<tr>
						<td>Status Perkawinan</td>
						<td>:</td>
						<td>'.$val->stat_kwn.'
						</td>
				</tr>
					<tr>
						<td>Pekerjaan</td>
						<td>:</td>
						<td>'.$val->pekerjaan.'</td>
				</tr>
					<tr>
						<td>Kewarganegaraan</td>
						<td>:</td>
						<td>INDONESIA </td>
				</tr>
			<tr>
				<td height="22px"></td>
			</tr>
			 </table> 
		<table  width="90%">
			<tr>
				<td style="text-align:justify">
					Penduduk tersebut di atas benar-benar <b>TERCATAT</b> dalam Database Kependudukan Kota Bandung pada alamat tersebut sejak tanggal '.$val->tgl_datang.' 
				</td>
			</tr>
				<tr>
					<td colspan="9" height="5"></td>
			</tr>
			<tr>
				<td style="text-align:justify">
					Demikian Surat Keterangan ini dibuat khusus untuk kepentingan Penerimaan Peserta Didik Baru (PPDB) Tahun Ajaran 2021/2022.
				</td>
			</tr>
		</table>
		<table width="90%" class="tengah">
			<tr>
					<td colspan="9" height="33"></td>
			</tr>
			<tr>
			  <td width="45%"></td>
			  <td width="15%"></td>
			  <td width="38%">Bandung, '.date('d-m-Y').'</td>
			  <td width="2%"></td>
			</tr>
			<tr>
				<td height="7px"></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td  style="line-height:10px; margin-top:5px!important;">KEPALA DINAS</td>
				<td></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td  style="line-height:6px; margin-top:5px!important;">KEPENDUDUKAN DAN PENCATATAN SIPIL</td>
				<td></td>
			</tr>
			<tr>
				<td style="line-height:6px; margin-top:5px!important;"></td>
				<td></td>
				<td  style="line-height:6px; margin-top:5px!important;">KOTA BANDUNG,</td>
				<td></td>
			</tr>
			<tr>
				<td height="65px"></td>
				<td></td>
				<td><img src="'.$image_ttd.'" width="200" height="100" /></td>
				<td></td>        
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td><u>H. TATANG MUHTAR, S.Sos., M.Si.</u></td>
				<td></td>
			</tr>
			<tr>
				<td style="line-height:5px; margin-top:5px!important;"></td>
				<td></td>
				<td style="line-height:5px; margin-top:5px!important;">NIP. 196806021989031004</td>
				<td></td>
			</tr>
		</table>
			';
		
}
        return  PDF::loadHTML($html)->setPaper('a4', 'portraits')
          ->setWarnings(false)
          ->stream(strtoupper($request->session()->get('S_USER_ID')).'_DATA_REKAM_'.time().'.pdf');
         } 
        else 
        {
           redirect('/','refresh');
        }
    }

    public function scr_export_data_ppdb($nik = ''){
			$sql = "";
			$sql .= "SELECT B.NIK,B.NO_KK, B.NAMA_LGKP, B.TMPT_LHR,TO_CHAR(B.TGL_LHR,'DD-MM-YYYY') AS TGL_LHR,CASE WHEN B.JENIS_KLMIN = 1 THEN 'LAKI-LAKI' ELSE 'PEREMPUAN' END AS JENIS_KLMIN, D.ALAMAT,  LPAD(TO_CHAR(D.NO_RT), 3, '0') AS RT,LPAD(TO_CHAR(D.NO_RW), 3, '0') AS RW, UPPER(F5_GET_REF_WNI(B.GOL_DRH, 401)) GOL_DRH, F5_GET_NAMA_KECAMATAN(B.NO_PROP,B.NO_KAB,B.NO_KEC) AS NAMA_KEC, F5_GET_NAMA_KELURAHAN(B.NO_PROP,B.NO_KAB,B.NO_KEC,B.NO_KEL) AS NAMA_KEL, UPPER(F5_GET_REF_WNI(F5_TO_NUMBER(B.AGAMA,7), 501)) AGAMA, UPPER(F5_GET_REF_WNI(B.JENIS_PKRJN, 201)) PEKERJAAN, UPPER(F5_GET_REF_WNI(B.STAT_KWN, 601)) STAT_KWN, UPPER(F5_GET_REF_WNI(B.JENIS_PKRJN, 201)) JENIS_PKRJN, UPPER(F5_GET_REF_WNI(B.STAT_KWN, 601)) STAT_KWN, CASE WHEN E.CREATED_DATE IS NULL THEN TO_CHAR(D.TGL_INSERTION,'DD-MM-YYYY') ELSE TO_CHAR(E.CREATED_DATE,'DD-MM-YYYY') END TGL_DATANG 
				, B.NO_KEC
				, B.NO_KEL
			FROM BIODATA_WNI B LEFT JOIN DATA_KELUARGA D ON B.NO_KK = D.NO_KK LEFT JOIN (SELECT NIK, MAX(CREATED_DATE) CREATED_DATE FROM PINDAH_DETAIL GROUP BY NIK) E ON B.NIK = E.NIK WHERE 1=1 
				AND (B.FLAG_STATUS = '0' OR (B.FLAG_STATUS = '2' AND B.FLAG_PINDAH IN(1,2,3))) ";
			if($nik != '' || $nik != null || !empty($nik)){ 
				$sql .= " AND B.NIK IN ($nik) ";
			} 
			$sql .= " ORDER BY B.NO_KEC, B.NO_KEL, B.NO_KK, D.NO_RW, D.NO_RT, B.STAT_HBKEL ";
			$r = DB::connection('db222')->select($sql);
			return $r;
		}
		public function scr_get_urut_ppdb($no_kec,$no_kel){
			$sql = "";
			$sql .= "SELECT LPAD(COUNT(1) +1,4,0) URUT FROM SIAK_HIST_PPDB WHERE NO_KEC = $no_kec AND NO_KEL = $no_kel AND TRUNC(CREATED_DATE) = TRUNC(SYSDATE)";
			$r = DB::select($sql);
			return $r[0]->urut;
		}
		public function scr_hist_data_ppdb($nik,$user_id,$no_surat,$no_kec,$no_kel){
			
			$sql = "INSERT INTO SIAK_HIST_PPDB (ID ,NIK ,CREATED_BY ,CREATED_DATE,NO_KEC, NO_KEL) VALUES ('$no_surat',$nik,'$user_id',SYSDATE,$no_kec,$no_kel)";
			$r = DB::insert($sql);
		}



	public function cek_data_duplicate($NIK){
			$sql = "SELECT  A.NIK
					, A.NO_PROP
					, A.NAMA_PROP
					, A.NO_KAB
					, A.NAMA_KAB
					, A.NO_KEC
					, A.NAMA_KEC
					, A.NO_KEL
					, A.NAMA_KEL
					, A.NAMA_LGKP
					, A.ALAMAT
					, A.CURRENT_STATUS_CODE
					, A.TGL_REKAM
					, A.NIK_SINGLE
					, A.NO_PROP_SINGLE
					, A.NAMA_PROP_SINGLE
					, A.NO_KAB_SINGLE
					, A.NAMA_KAB_SINGLE
					, A.NO_KEC_SINGLE
					, A.NAMA_KEC_SINGLE
					, A.NO_KEL_SINGLE
					, A.NAMA_KEL_SINGLE
					, A.NAMA_LGKP_SINGLE
					, A.ALAMAT_SINGLE
					, A.CURRENT_STATUS_CODE_SINGLE
					, A.TGL_REKAM_SINGLE FROM (SELECT  A.NIK
					, A.NO_PROP
					, F5_GET_NAMA_PROVINSI(A.NO_PROP) NAMA_PROP
					, A.NO_KAB
					, F5_GET_NAMA_KABUPATEN(A.NO_PROP,A.NO_KAB) NAMA_KAB
					, A.NO_KEC
					, F5_GET_NAMA_KECAMATAN(A.NO_PROP,A.NO_KAB, A.NO_KEC) NAMA_KEC
					, '0' NO_KEL
					, '-' NAMA_KEL
					, A.NAMA_LGKP
					, '-' ALAMAT
					, A.CURRENT_STATUS_CODE
					, TO_CHAR(A.TGL_REKAM,'DD-MM-YYYY') TGL_REKAM
					, TO_CHAR(A.NIK_SINGLE) NIK_SINGLE
					, TO_CHAR(A.NO_PROP_SINGLE) NO_PROP_SINGLE
					, (SELECT C.NAMA_PROP FROM SETUP_PROP C WHERE A.NO_PROP_SINGLE = C.NO_PROP) NAMA_PROP_SINGLE
					, TO_CHAR(A.NO_KAB_SINGLE) NO_KAB_SINGLE
					, (SELECT C.NAMA_KAB FROM SETUP_KAB C WHERE A.NO_PROP_SINGLE = C.NO_PROP AND A.NO_KAB_SINGLE = C.NO_KAB) NAMA_KAB_SINGLE
					, TO_CHAR(A.NO_KEC_SINGLE) NO_KEC_SINGLE
					, (SELECT C.NAMA_KEC FROM SETUP_KEC C WHERE A.NO_PROP_SINGLE = C.NO_PROP AND A.NO_KAB_SINGLE = C.NO_KAB AND A.NO_KEC_SINGLE = C.NO_KEC) NAMA_KEC_SINGLE
					, '0' NO_KEL_SINGLE
					, '-' NAMA_KEL_SINGLE
					, A.NAMA_LGKP_SINGLE
					, '-' ALAMAT_SINGLE
					, A.CURRENT_STATUS_CODE_SINGLE
					, TO_CHAR(A.TGL_REKAM_SINGLE,'DD-MM-YYYY') TGL_REKAM_SINGLE
          			FROM SIAK_DUPLICATE_FULL A
                UNION ALL
                SELECT  B.NIK
					, B.NO_PROP
					, B.NAMA_PROP
					, B.NO_KAB
					, B.NAMA_KAB
					, B.NO_KEC
					, B.NAMA_KEC
					, B.NO_KEL
					, B.NAMA_KEL
					, B.NAMA_LGKP
					, B.ALAMAT
					, B.CURRENT_STATUS_CODE
					, B.TGL_REKAM
					, B.NIK_SINGLE
					, B.NO_PROP_SINGLE
					, B.NAMA_PROP_SINGLE
					, B.NO_KAB_SINGLE
					, B.NAMA_KAB_SINGLE
					, B.NO_KEC_SINGLE
					, B.NAMA_KEC_SINGLE
					, B.NO_KEL_SINGLE
					, B.NAMA_KEL_SINGLE
					, B.NAMA_LGKP_SINGLE
					, B.ALAMAT_SINGLE
					, B.CURRENT_STATUS_CODE_SINGLE
					, B.TGL_REKAM_SINGLE FROM (SELECT  B.NIK
					, B.NO_PROP
					, F5_GET_NAMA_PROVINSI(B.NO_PROP) NAMA_PROP
					, B.NO_KAB
					,F5_GET_NAMA_KABUPATEN(B.NO_PROP,B.NO_KAB) NAMA_KAB
					, B.NO_KEC
					, B.NAMA_KEC
					, '0' NO_KEL
					, '-' NAMA_KEL
					, B.NAMA_LGKP
					, '-' ALAMAT
					, B.CURRENT_STATUS_CODE
					, TO_CHAR(B.CREATED,'DD-MM-YYYY') TGL_REKAM
					, TO_CHAR(C.DUPLICATE_NIK) NIK_SINGLE
					, '0' NO_PROP_SINGLE
					, '-' NAMA_PROP_SINGLE
					, '0' NO_KAB_SINGLE
					, '-' NAMA_KAB_SINGLE
					, '0' NO_KEC_SINGLE
					, '-' NAMA_KEC_SINGLE
					, '0' NO_KEL_SINGLE
					, '-' NAMA_KEL_SINGLE
					, '-' NAMA_LGKP_SINGLE
					, '-' ALAMAT_SINGLE
					, '-' CURRENT_STATUS_CODE_SINGLE
					, '-' TGL_REKAM_SINGLE 
          FROM DEMOGRAPHICS@DB5 B INNER JOIN  DUPLICATE_RESULTS@DB5 C ON B.NIK = C.NIK WHERE 
          (B.NIK IN ($NIK)) AND NOT EXISTS (SELECT 1 FROM SIAK_DUPLICATE_FULL A WHERE A.NIK = B.NIK)) B
                ) A
                WHERE A.NIK IN ($NIK) OR A.NIK_SINGLE IN ($NIK)";
			$r = DB::select($sql);
			return $r;
		}
		public function scr_cek_data_rekam($nik = '',$no_kk = '',$nama_lgkp = '',$tgl_lhr = '',$no_kec = 0,$no_kel = 0){
			$sql = "";
			$sql .= "SELECT 
            A.NIK
            , A.NO_KK
            , A.NAMA_LGKP
            , A.TMPT_LHR
            , TO_CHAR(A.TGL_LHR,'DD-MM-YYYY') AS TGL_LHR
            , JENIS_KLMIN
            , A.ALAMAT
            , LPAD(TO_CHAR(A.NO_RT), 3, '0') AS RT
            , LPAD(TO_CHAR(A.NO_RW), 3, '0') AS RW
            , A.KODE_POS
            ,(SELECT COUNT(1) AS JML FROM CARD_MANAGEMENT WHERE NIK = A.NIK ) AS JUMLAH_CETAK
            , A.NAMA_KEC
            , A.NAMA_KEL
            , A.AGAMA
            , A.JENIS_PKRJN
            , A.STAT_KWN
            , CASE WHEN A.CURRENT_STATUS_CODE IS NULL THEN '-' ELSE A.CURRENT_STATUS_CODE END AS CURRENT_STATUS_CODE
            , CASE WHEN F.DIS_DT IS NULL THEN TO_CHAR(H.DIS_DT,'DD-MM-YYYY') WHEN H.DIS_DT > F.DIS_DT THEN TO_CHAR(H.DIS_DT,'DD-MM-YYYY') ELSE TO_CHAR(F.DIS_DT,'DD-MM-YYYY') END DIS_DT
            , CASE WHEN F.DIS_BY IS NULL THEN H.DIS_BY WHEN H.DIS_DT > F.DIS_DT THEN H.DIS_BY ELSE F.DIS_BY END DIS_BY
            , CASE WHEN  TO_CHAR(G.LAST_UPDATE,'DD-MM-YYYY') IS NULL AND TO_CHAR(G.PERSONALIZED_DATE,'DD-MM-YYYY') IS NULL THEN '-' WHEN TO_CHAR(G.LAST_UPDATE,'DD-MM-YYYY') IS NULL AND TO_CHAR(G.PERSONALIZED_DATE,'DD-MM-YYYY') IS NOT NULL THEN TO_CHAR(G.PERSONALIZED_DATE,'DD-MM-YYYY') ELSE  TO_CHAR(G.LAST_UPDATE,'DD-MM-YYYY') END AS KTP_DT
            , CASE WHEN TO_CHAR(G.LAST_UPDATED_USERNAME) IS NULL AND TO_CHAR(G.CREATED_USERNAME) IS NULL  THEN '-' WHEN TO_CHAR(G.LAST_UPDATED_USERNAME) IS NULL AND TO_CHAR(G.CREATED_USERNAME) IS NOT NULL THEN TO_CHAR(G.CREATED_USERNAME) ELSE TO_CHAR(G.LAST_UPDATED_USERNAME) END AS KTP_BY
            , E.PATH
            , CASE WHEN TO_CHAR(H.REQ_DATE,'DD-MM-YYYY') IS NULL THEN '-' ELSE TO_CHAR(H.REQ_DATE,'DD-MM-YYYY') END REQ_DATE
            , CASE WHEN H.REQ_BY IS NULL THEN '-' ELSE H.REQ_BY END REQ_BY
            , CASE WHEN H.ALASAN_PENGAJUAN IS NULL THEN '-' ELSE H.ALASAN_PENGAJUAN END PROC_BY
            , CASE WHEN H.STEP_PROC IS NULL THEN '-' WHEN H.STEP_PROC = 'T' THEN 'Pengajuan Ditolak' WHEN H.STEP_PROC = 'A' THEN 'Masih Dalam Proses' WHEN H.STEP_PROC = 'C' THEN 'Sudah Tercetak' ELSE  H.STEP_PROC END STEP_PROC 
            FROM DEMOGRAPHICS_ALL A   
            LEFT JOIN T5_FOTO@DB222 E ON A.NIK = E.NIK LEFT JOIN (SELECT NIK, DIS_BY, DIS_DT FROM (SELECT NIK, REQ_BY DIS_BY, TGL_CETAK DIS_DT, RANK() OVER (PARTITION BY NIK ORDER BY TGL_CETAK DESC,NO DESC) RNK FROM SIAK_CETAK_DINAS@YZDB) WHERE RNK =1) F ON A.NIK = F.NIK 
            LEFT JOIN (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME,LAST_UPDATE,CREATED FROM (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME,LAST_UPDATE,CREATED, RANK() OVER (PARTITION BY NIK ORDER BY  PERSONALIZED_DATE DESC, LAST_UPDATE DESC, CHIP_ID DESC) RNK FROM CARD_MANAGEMENT) WHERE RNK = 1) G ON G.NIK =A.NIK LEFT JOIN (SELECT NIK,REQ_DATE,REQ_BY,PROC_DATE,PROC_BY,STEP_PROC,ALASAN_PENGAJUAN,DIS_BY,DIS_DT FROM (SELECT NIK,REQ_DATE,REQ_BY,PROC_DATE,PROC_BY,STEP_PROC,ALASAN_PENGAJUAN,DIS_BY,DIS_DT, RANK() OVER (PARTITION BY NIK ORDER BY REQ_DATE DESC,ID_CETAK DESC) RNK FROM SIAK_REQ_CETAK_KTP@YZDB) WHERE RNK = 1) H ON H.NIK =A.NIK WHERE 1=1 
              ";
			if($nik != '' || $nik != null || !empty($nik)){ 
				if(strlen($nik) <= 17){
				$sql .= " AND A.NIK = $nik ";	
				}else{
				$sql .= " AND A.NIK IN ($nik) ";	
				}
				
			} 
			if($no_kk != '' || $no_kk != null || !empty($no_kk)){ 
				if(strlen($no_kk) <= 17){
				$sql .= " AND A.NO_KK = $no_kk ";
				}else{
				$sql .= " AND A.NO_KK IN ($no_kk) ";
				}
			}
			if($nama_lgkp != '' || $nama_lgkp != null || !empty($nama_lgkp)){ 
				$sql .= " AND A.NAMA_LGKP LIKE UPPER('$nama_lgkp') ";
			}
			if($tgl_lhr != '' || $tgl_lhr != null || !empty($tgl_lhr)){ 
				$sql .= " AND TO_CHAR(A.TGL_LHR,'DD-MM-YYYY') = '$tgl_lhr' ";
			} 
			$sql .= " ORDER BY A.NO_KK, A.STAT_HBKEL ";
			// echo $sql;
			// die;
			$r = DB::connection('db5cetak')->select($sql);
			return $r;

		}

		public function scr_export_data_rekam($nik = ''){
			$sql = "";
			$sql .= " SELECT A.NIK,A.NO_KK, A.NAMA_LGKP, A.TMPT_LHR,TO_CHAR(A.TGL_LHR,'DD-MM-YYYY') AS TGL_LHR,A.JENIS_KLMIN, A.ALAMAT,  LPAD(TO_CHAR(A.NO_RT), 3, '0') AS RT,LPAD(TO_CHAR(A.NO_RW), 3, '0') AS RW,(SELECT COUNT(1) AS JML FROM CARD_MANAGEMENT WHERE NIK = A.NIK ) AS JUMLAH_CETAK, A.NAMA_KEC, A.NAMA_KEL, A.AGAMA, A.JENIS_PKRJN, A.STAT_KWN, CASE WHEN A.CURRENT_STATUS_CODE IS NULL THEN '-' ELSE A.CURRENT_STATUS_CODE END AS CURRENT_STATUS_CODE, CASE WHEN F.PRINTED_BY IS NULL THEN '-' ELSE F.PRINTED_BY END AS SUKET_BY ,CASE WHEN TO_CHAR(F.PRINTED_DATE,'DD-MM-YYYY') IS NULL THEN '-' ELSE TO_CHAR(F.PRINTED_DATE,'DD-MM-YYYY') END AS SUKET_DT,CASE WHEN  TO_CHAR(G.LAST_UPDATE,'DD-MM-YYYY') IS NULL AND TO_CHAR(G.PERSONALIZED_DATE,'DD-MM-YYYY') IS NULL THEN '-' WHEN TO_CHAR(G.LAST_UPDATE,'DD-MM-YYYY') IS NULL AND TO_CHAR(G.PERSONALIZED_DATE,'DD-MM-YYYY') IS NOT NULL THEN TO_CHAR(G.PERSONALIZED_DATE,'DD-MM-YYYY') ELSE  TO_CHAR(G.LAST_UPDATE,'DD-MM-YYYY') END AS KTP_DT, CASE WHEN TO_CHAR(G.LAST_UPDATED_USERNAME) IS NULL AND TO_CHAR(G.CREATED_USERNAME) IS NULL  THEN '-' WHEN TO_CHAR(G.LAST_UPDATED_USERNAME) IS NULL AND TO_CHAR(G.CREATED_USERNAME) IS NOT NULL THEN TO_CHAR(G.CREATED_USERNAME) ELSE TO_CHAR(G.LAST_UPDATED_USERNAME) END AS KTP_BY, E.PATH, CASE WHEN TO_CHAR(H.REQ_DATE,'DD-MM-YYYY') IS NULL THEN '-' ELSE TO_CHAR(H.REQ_DATE,'DD-MM-YYYY') END REQ_DATE, CASE WHEN H.REQ_BY IS NULL THEN '-' ELSE H.REQ_BY END REQ_BY,CASE WHEN H.PROC_BY IS NULL THEN '-' ELSE H.PROC_BY END PROC_BY, CASE WHEN H.STEP_PROC IS NULL THEN '-' WHEN H.STEP_PROC = 'T' THEN 'Pengajuan Ditolak' WHEN H.STEP_PROC = 'A' THEN 'Masih Dalam Proses' WHEN H.STEP_PROC = 'C' THEN 'Sudah Tercetak' ELSE  H.STEP_PROC END STEP_PROC 
 				FROM DEMOGRAPHICS_ALL A LEFT JOIN T5_FOTO@DB222 E ON A.NIK = E.NIK LEFT JOIN (SELECT NIK, PRINTED_DATE,PRINTED_BY,NO_DOKUMEN  FROM (SELECT NIK,PRINTED_DATE,PRINTED_BY,NO_DOKUMEN, RANK() OVER (PARTITION BY NIK ORDER BY PRINTED_DATE DESC,NO_DOKUMEN DESC) RNK FROM T7_HIST_SUKET@DB222)  WHERE RNK = 1) F ON F.NIK = A.NIK LEFT JOIN (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME,LAST_UPDATE,CREATED FROM (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME,LAST_UPDATE,CREATED, RANK() OVER (PARTITION BY NIK ORDER BY  PERSONALIZED_DATE DESC, LAST_UPDATE DESC, CHIP_ID DESC) RNK FROM CARD_MANAGEMENT) WHERE RNK = 1) G ON G.NIK =A.NIK LEFT JOIN (SELECT NIK,REQ_DATE,REQ_BY,PROC_DATE,PROC_BY,STEP_PROC FROM (SELECT NIK,REQ_DATE,REQ_BY,PROC_DATE,PROC_BY,STEP_PROC, RANK() OVER (PARTITION BY NIK ORDER BY REQ_DATE DESC,ID_CETAK DESC) RNK FROM SIAK_REQ_CETAK_KTP@YZDB) WHERE RNK = 1) H ON H.NIK =A.NIK WHERE 1=1  ";
			if($nik != '' || $nik != null || !empty($nik)){ 
				$sql .= " AND A.NIK IN ($nik) ";
			} 
			$sql .= " ORDER BY A.NO_KEC, A.NO_KEL, A.NO_KK, A.NO_RW, A.NO_RT, A.STAT_HBKEL ";
			$r = DB::connection('db5cetak')->select($sql);
			return $r;
		}

		public function scr_cek_data_history($nik = '',$no_kec= 0){
			$sql = "";
			$sql .= "SELECT A.NIK, A.NO_KK, A.NAMA_LGKP, B.CHIP_ID, TO_CHAR(B.PERSONALIZED_DATE,'DD-MM-YYYY') AS KTP_DT, B.CREATED_USERNAME AS KTP_BY
			, CASE WHEN TO_CHAR(B.LAST_UPDATE,'DD-MM-YYYY') IS NULL THEN '-' ELSE TO_CHAR(B.LAST_UPDATE,'DD-MM-YYYY') END AS UPDATED_DT
			, CASE WHEN B.LAST_UPDATED_USERNAME IS NULL THEN NULL ELSE B.LAST_UPDATED_USERNAME END AS UPDATED_BY
			, A.NAMA_KEC,A.NAMA_KEL FROM DEMOGRAPHICS_ALL A INNER JOIN CARD_MANAGEMENT B ON A.NIK = B.NIK WHERE 1=1 AND (A.TGL_LHR<=ADD_MONTHS(SYSDATE,-12*17) OR A.STAT_KWN<>'BELUM KAWIN') ";
			if($no_kec != 0 ){ 
				$sql .= " AND A.NO_KEC = $no_kec ";
			} 
			$sql .= " AND A.NIK IN ($nik) ORDER BY B.PERSONALIZED_DATE DESC";
			$r = DB::connection('db5cetak')->select($sql);
			return $r;
		}
		public function scr_push_demogh221($NIK){
			$sql = "UPDATE DEMOGRAPHICS A SET A.CURRENT_STATUS_CODE = 'BIO_CAPTURED' WHERE A.CURRENT_STATUS_CODE IN (SELECT CODE FROM STATUS_ID WHERE ID_STATUS IN (3,4,5,6,7,8,9,10,11,12)) AND A.NIK IN ($NIK) ";
			$r = DB::connection('db221')->update($sql);
			return $r;
		}
		public function scr_push_demogh2($NIK){
			$sql = "UPDATE DEMOGRAPHICS A SET A.CURRENT_STATUS_CODE = 'BIO_CAPTURED' WHERE A.CURRENT_STATUS_CODE IN (SELECT CODE FROM STATUS_ID WHERE ID_STATUS IN (3,4,5,6,7,8,9,10,11,12)) AND A.NIK IN ($NIK) ";
			$r = DB::connection('db5cetak')->update($sql);
			return $r;
		}
		public function scr_push_demoghall($NIK){
			$sql = "UPDATE DEMOGRAPHICS_ALL A SET A.CURRENT_STATUS_CODE = 'BIO_CAPTURED' WHERE A.CURRENT_STATUS_CODE IN (SELECT CODE FROM STATUS_ID WHERE ID_STATUS IN (3,4,5,6,7,8,9,10,11,12)) AND A.NIK IN ($NIK) ";
			$r = DB::connection('db5cetak')->update($sql);
			return $r;
		}


		public function scr_del_req($nik,$no_kec){
			$sql = "";
			$sql .= "SELECT 
					X.NO_KEC
					,X.NIK
					,X.NAMA_LGKP AS NAMA_LENGKAP, X.ALAMAT, CASE WHEN TO_CHAR(X.KODE_POS) IS NULL THEN '-' ELSE TO_CHAR(X.KODE_POS) END KODE_POS, LPAD(TO_CHAR(X.NO_RT), 3, '0') AS RT, LPAD(TO_CHAR(X.NO_RW), 3, '0') AS RW,CASE WHEN X.CURRENT_STATUS_CODE IS NULL THEN '-' ELSE X.CURRENT_STATUS_CODE END AS CURRENT_STATUS_CODE
					,TO_CHAR(A.REQ_DATE,'YYYY-MM-DD') AS REQ_DATE
					,A.REQ_BY
					, CASE WHEN A.STEP_PROC = 'T' THEN 'DITOLAK' WHEN A.STEP_PROC = 'C' THEN 'DICETAK' ELSE 'PROSSESS' END AS STATUS_PENGAJUAN
					, CASE WHEN A.STEP_PROC = 'T' THEN UPPER(A.PROC_BY) WHEN A.STEP_PROC = 'C' THEN 'SUDAH DICATAK' ELSE '-' END AS KETERANGAN_STATUS
					, TO_CHAR(C.PERSONALIZED_DATE,'YYYY-MM-DD') AS PRINTED_DATE
					, CASE WHEN TO_CHAR(C.LAST_UPDATED_USERNAME) IS NULL AND TO_CHAR(C.CREATED_USERNAME) IS NULL  THEN '-' WHEN TO_CHAR(C.LAST_UPDATED_USERNAME) IS NULL AND TO_CHAR(C.CREATED_USERNAME) IS NOT NULL THEN TO_CHAR(C.CREATED_USERNAME) ELSE TO_CHAR(C.LAST_UPDATED_USERNAME) END AS PRINTED_BY
					, X.NAMA_KEC
					, X.NAMA_KEL
					, X.JENIS_PKRJN
					, X.STAT_KWN
					FROM DEMOGRAPHICS_ALL X INNER JOIN (SELECT ID_CETAK,NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY,STEP_PROC,PROC_BY FROM 
					(SELECT  ID_CETAK, NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY,STEP_PROC,PROC_BY, RANK() OVER (PARTITION BY NIK ORDER BY REQ_DATE DESC, ID_CETAK DESC) RNK FROM SIAK_REQ_CETAK_KTP@YZDB WHERE STEP_PROC <> 'C') WHERE RNK = 1) A ON A.NIK =X.NIK
					 LEFT JOIN (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME FROM (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME, RANK() OVER (PARTITION BY NIK ORDER BY  PERSONALIZED_DATE DESC, LAST_UPDATE DESC, CHIP_ID DESC) RNK FROM CARD_MANAGEMENT) WHERE RNK = 1) C ON A.NIK = C.NIK  WHERE 1=1 AND (TO_DATE(A.REQ_DATE+1,'DD-MM-YYYY') > TO_DATE(C.PERSONALIZED_DATE,'DD-MM-YYYY') OR C.PERSONALIZED_DATE IS NULL)";
			if($no_kec != 0){
				$sql .= " AND X.NO_KEC = $no_kec";
			}
			if($nik != ""){
				$sql .= " AND X.NIK IN ($nik)";
			}
			$sql .= "ORDER BY X.PERSONALIZED_DATE";
			$r = DB::connection('db5cetak')->select($sql);
			return $r;
		}
		public function scr_do_delete($nik){
			$sql = "DELETE FROM SIAK_REQ_CETAK_KTP@YZDB WHERE STEP_PROC <> 'C' AND NIK IN ($nik) ";
			$r = DB::connection('db222')->delete($sql);
		}
		public function scr_cek_req_data_uktp($nik,$tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "";
			$sql .= "SELECT 
					X.NO_KEC
					,X.NIK
					,X.NAMA_LGKP AS NAMA_LENGKAP, X.ALAMAT, CASE WHEN TO_CHAR(X.KODE_POS) IS NULL THEN '-' ELSE TO_CHAR(X.KODE_POS) END KODE_POS, LPAD(TO_CHAR(X.NO_RT), 3, '0') AS RT, LPAD(TO_CHAR(X.NO_RW), 3, '0') AS RW,CASE WHEN X.CURRENT_STATUS_CODE IS NULL THEN '-' ELSE X.CURRENT_STATUS_CODE END AS CURRENT_STATUS_CODE
					,TO_CHAR(A.REQ_DATE,'YYYY-MM-DD') AS REQ_DATE
					,A.REQ_BY
					, CASE WHEN A.STEP_PROC = 'T' THEN 'DITOLAK' WHEN A.STEP_PROC = 'C' THEN 'DICETAK' ELSE 'PROSSESS' END AS STATUS_PENGAJUAN
					, CASE WHEN A.STEP_PROC = 'T' THEN UPPER(A.PROC_BY) WHEN A.STEP_PROC = 'C' THEN 'SUDAH DICATAK' ELSE '-' END AS KETERANGAN_STATUS
					, TO_CHAR(C.PERSONALIZED_DATE,'YYYY-MM-DD') AS PRINTED_DATE
					, CASE WHEN TO_CHAR(C.LAST_UPDATED_USERNAME) IS NULL AND TO_CHAR(C.CREATED_USERNAME) IS NULL  THEN '-' WHEN TO_CHAR(C.LAST_UPDATED_USERNAME) IS NULL AND TO_CHAR(C.CREATED_USERNAME) IS NOT NULL THEN TO_CHAR(C.CREATED_USERNAME) ELSE TO_CHAR(C.LAST_UPDATED_USERNAME) END AS PRINTED_BY
					, X.NAMA_KEC
					, X.NAMA_KEL
					, X.AGAMA
					, X.JENIS_PKRJN
					, X.STAT_KWN
					FROM DEMOGRAPHICS_ALL X INNER JOIN (SELECT ID_CETAK,NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY,STEP_PROC,PROC_BY FROM 
					(SELECT  ID_CETAK, NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY,STEP_PROC,PROC_BY, RANK() OVER (PARTITION BY NIK ORDER BY REQ_DATE DESC, ID_CETAK DESC) RNK FROM SIAK_REQ_CETAK_KTP@YZDB WHERE STEP_PROC <> 'C') WHERE RNK = 1) A ON A.NIK =X.NIK
					LEFT JOIN (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME FROM (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME, RANK() OVER (PARTITION BY NIK ORDER BY  PERSONALIZED_DATE DESC, LAST_UPDATE DESC, CHIP_ID DESC) RNK FROM CARD_MANAGEMENT) WHERE RNK = 1) C ON A.NIK = C.NIK  WHERE 1=1 AND (TO_DATE(A.REQ_DATE,'DD-MM-YYYY') > TO_DATE(C.PERSONALIZED_DATE,'DD-MM-YYYY') OR C.PERSONALIZED_DATE IS NULL) ";
			if($no_kec != 0){
				$sql .= " AND X.NO_KEC = $no_kec";
			}
			if($no_kel != 0){
				$sql .= " AND X.NO_KEL = $no_kel";
			} 
			if($nik != ""){
				$sql .= " AND X.NIK IN ($nik)";
			}
			$sql .= " AND A.REQ_DATE >= TO_DATE('$tgl_start','DD-MM-YYYY') AND A.REQ_DATE < TO_DATE('$tgl_end','DD-MM-YYYY') +1 ORDER BY C.PERSONALIZED_DATE";
			$r = DB::connection('db5cetak')->select($sql);
			return $r;
		}

		public function scr_cek_req_data_dktp($nik,$tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "";
			$sql .= "SELECT X.NO_KEC,X.NIK,X.NAMA_LGKP AS NAMA_LENGKAP, X.ALAMAT, CASE WHEN TO_CHAR(X.KODE_POS) IS NULL THEN '-' ELSE TO_CHAR(X.KODE_POS) END KODE_POS, LPAD(TO_CHAR(X.NO_RT), 3, '0') AS RT, LPAD(TO_CHAR(X.NO_RW), 3, '0') AS RW, B.CURRENT_STATUS_CODE,TO_CHAR(A.REQ_DATE,'DD-MM-YYYY') AS REQ_DATE,A.REQ_BY, TO_CHAR(C.PERSONALIZED_DATE,'DD-MM-YYYY') AS PRINTED_DATE,TO_CHAR(C.PERSONALIZED_DATE,'DD-MM-YYYY') AS PRINTED_DATE,CASE WHEN TO_CHAR(C.LAST_UPDATED_USERNAME) IS NULL AND TO_CHAR(C.CREATED_USERNAME) IS NULL  THEN '-' WHEN TO_CHAR(C.LAST_UPDATED_USERNAME) IS NULL AND TO_CHAR(C.CREATED_USERNAME) IS NOT NULL THEN TO_CHAR(C.CREATED_USERNAME) ELSE TO_CHAR(C.LAST_UPDATED_USERNAME) END AS PRINTED_BY, X.NAMA_KEC, X.NAMA_KEL, X.AGAMA, X.JENIS_PKRJN, X.STAT_KWN FROM DEMOGRAPHICS_ALL X INNER JOIN (SELECT ID_CETAK,NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY FROM (SELECT  ID_CETAK, NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY, RANK() OVER (PARTITION BY NIK ORDER BY REQ_DATE DESC, ID_CETAK DESC) RNK FROM SIAK_REQ_CETAK_KTP@YZDB  WHERE STEP_PROC = 'A') WHERE RNK = 1) A ON A.NIK = X.NIK LEFT JOIN DEMOGRAPHICS_ALL B ON A.NIK = B.NIK LEFT JOIN (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME FROM (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME, RANK() OVER (PARTITION BY NIK ORDER BY  PERSONALIZED_DATE DESC, LAST_UPDATE DESC, CHIP_ID DESC) RNK FROM CARD_MANAGEMENT) WHERE RNK = 1) C ON A.NIK = C.NIK  WHERE 1=1  AND (A.REQ_DATE < C.PERSONALIZED_DATE) ";
			if($no_kec != 0){
				$sql .= " AND X.NO_KEC = $no_kec";
			}
			if($no_kel != 0){
				$sql .= " AND X.NO_KEL = $no_kel";
			} 
			if($nik != ""){
				$sql .= " AND X.NIK IN ($nik)";
			}
			$sql .= " AND A.REQ_DATE >= TO_DATE('$tgl_start','DD-MM-YYYY') AND A.REQ_DATE < TO_DATE('$tgl_end','DD-MM-YYYY') +1";
	
			$r = DB::connection('db5cetak')->select($sql);
			return $r;
		}

		public function scr_cek_req_data_uktp_f($nik,$tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "";
			$sql .= "SELECT X.NO_KEC,X.NIK,X.NAMA_LGKP AS NAMA_LENGKAP, X.ALAMAT, CASE WHEN TO_CHAR(X.KODE_POS) IS NULL THEN '-' ELSE TO_CHAR(X.KODE_POS) END KODE_POS, LPAD(TO_CHAR(X.NO_RT), 3, '0') AS RT, LPAD(TO_CHAR(X.NO_RW), 3, '0') AS RW, CASE WHEN B.CURRENT_STATUS_CODE IS NULL THEN '-' ELSE B.CURRENT_STATUS_CODE END AS CURRENT_STATUS_CODE,TO_CHAR(A.REQ_DATE,'YYYY-MM-DD') AS REQ_DATE,A.REQ_BY, TO_CHAR(C.PERSONALIZED_DATE,'YYYY-MM-DD') AS PRINTED_DATE,CASE WHEN TO_CHAR(C.LAST_UPDATED_USERNAME) IS NULL AND TO_CHAR(C.CREATED_USERNAME) IS NULL  THEN '-' WHEN TO_CHAR(C.LAST_UPDATED_USERNAME) IS NULL AND TO_CHAR(C.CREATED_USERNAME) IS NOT NULL THEN TO_CHAR(C.CREATED_USERNAME) ELSE TO_CHAR(C.LAST_UPDATED_USERNAME) END AS PRINTED_BY, X.NAMA_KEC, X.NAMA_KEL, X.AGAMA, X.JENIS_PKRJN, X.STAT_KWN FROM DEMOGRAPHICS_ALL@DB5CETAK X INNER JOIN (SELECT ID_CETAK,NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY FROM (SELECT  ID_CETAK, NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY, RANK() OVER (PARTITION BY NIK ORDER BY REQ_DATE DESC, ID_CETAK DESC) RNK FROM SIAK_REQ_CETAK_KTP@YZDB  WHERE STEP_PROC = 'A') WHERE RNK = 1) A ON A.NIK = X.NIK LEFT JOIN DEMOGRAPHICS_ALL@DB5CETAK B ON A.NIK = B.NIK LEFT JOIN (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME FROM (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME, RANK() OVER (PARTITION BY NIK ORDER BY  PERSONALIZED_DATE DESC, LAST_UPDATE DESC, CHIP_ID DESC) RNK FROM CARD_MANAGEMENT@DB5CETAK) WHERE RNK = 1) C ON A.NIK = C.NIK  WHERE 1=1 AND (B.CURRENT_STATUS_CODE LIKE '%CARD%' OR B.CURRENT_STATUS_CODE LIKE '%PRINT%') AND (TO_DATE(A.REQ_DATE,'DD-MM-YYYY') > TO_DATE(C.PERSONALIZED_DATE,'DD-MM-YYYY') OR C.PERSONALIZED_DATE IS NULL)";
			if($no_kec != 0){
				$sql .= " AND X.NO_KEC = $no_kec";
			}
			if($no_kel != 0){
				$sql .= " AND X.NO_KEL = $no_kel";
			} 
			if($nik != ""){
				$sql .= " AND X.NIK IN ($nik)";
			}
			$sql .= " AND A.REQ_DATE >= TO_DATE('$tgl_start','DD-MM-YYYY') AND A.REQ_DATE < TO_DATE('$tgl_end','DD-MM-YYYY') +1";
		
			$r = DB::connection('db5cetak')->select($sql);
			return $r;
		}

		public function scr_cek_req_data_dktp_f($nik,$tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "";
			$sql .= "SELECT X.NO_KEC,X.NIK,X.NAMA_LGKP AS NAMA_LENGKAP, X.ALAMAT, CASE WHEN TO_CHAR(X.KODE_POS) IS NULL THEN '-' ELSE TO_CHAR(X.KODE_POS) END KODE_POS, LPAD(TO_CHAR(X.NO_RT), 3, '0') AS RT, LPAD(TO_CHAR(X.NO_RW), 3, '0') AS RW, B.CURRENT_STATUS_CODE,TO_CHAR(A.REQ_DATE,'DD-MM-YYYY') AS REQ_DATE,A.REQ_BY, TO_CHAR(C.PERSONALIZED_DATE,'DD-MM-YYYY') AS PRINTED_DATE,TO_CHAR(C.PERSONALIZED_DATE,'DD-MM-YYYY') AS PRINTED_DATE,CASE WHEN TO_CHAR(C.LAST_UPDATED_USERNAME) IS NULL AND TO_CHAR(C.CREATED_USERNAME) IS NULL  THEN '-' WHEN TO_CHAR(C.LAST_UPDATED_USERNAME) IS NULL AND TO_CHAR(C.CREATED_USERNAME) IS NOT NULL THEN TO_CHAR(C.CREATED_USERNAME) ELSE TO_CHAR(C.LAST_UPDATED_USERNAME) END AS PRINTED_BY, X.NAMA_KEC, X.NAMA_KEL, X.AGAMA, X.JENIS_PKRJN, X.STAT_KWN FROM DEMOGRAPHICS_ALL@DB5CETAK X INNER JOIN (SELECT ID_CETAK,NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY FROM (SELECT  ID_CETAK, NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY, RANK() OVER (PARTITION BY NIK ORDER BY REQ_DATE DESC, ID_CETAK DESC) RNK FROM SIAK_REQ_CETAK_KTP@YZDB  WHERE STEP_PROC = 'A') WHERE RNK = 1) A ON A.NIK = X.NIK LEFT JOIN DEMOGRAPHICS_ALL@DB5CETAK B ON A.NIK = B.NIK LEFT JOIN (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME FROM (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME, RANK() OVER (PARTITION BY NIK ORDER BY  PERSONALIZED_DATE DESC, LAST_UPDATE DESC, CHIP_ID DESC) RNK FROM CARD_MANAGEMENT@DB5CETAK) WHERE RNK = 1) C ON A.NIK = C.NIK  WHERE 1=1  AND (B.CURRENT_STATUS_CODE LIKE '%CARD%' OR B.CURRENT_STATUS_CODE LIKE '%PRINT%') AND (A.REQ_DATE < C.PERSONALIZED_DATE)";
			if($no_kec != 0){
				$sql .= " AND X.NO_KEC = $no_kec";
			}
			if($no_kel != 0){
				$sql .= " AND X.NO_KEL = $no_kel";
			} 
			if($nik != ""){
				$sql .= " AND X.NIK IN ($nik)";
			}
			$sql .= " AND A.REQ_DATE >= TO_DATE('$tgl_start','DD-MM-YYYY') AND A.REQ_DATE < TO_DATE('$tgl_end','DD-MM-YYYY') +1";
			$r = DB::connection('db5cetak')->select($sql);
			return $r;
		}

		public function scr_cek_req_data_usuket($nik,$tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "";
			$sql .= "SELECT X.NO_KEC,X.NIK,X.NAMA_LGKP AS NAMA_LENGKAP, X.ALAMAT,X.AGAMA, CASE WHEN TO_CHAR(X.KODE_POS) IS NULL THEN '-' ELSE TO_CHAR(X.KODE_POS) END KODE_POS, LPAD(TO_CHAR(X.NO_RT), 3, '0') AS RT, LPAD(TO_CHAR(X.NO_RW), 3, '0') AS RW, B.CURRENT_STATUS_CODE,TO_CHAR(A.REQ_DATE,'DD-MM-YYYY') AS REQ_DATE,A.REQ_BY, TO_CHAR(C.PRINTED_DATE,'DD-MM-YYYY') AS PRINTED_DATE,C.PRINTED_BY, B.NAMA_KEC, B.NAMA_KEL, X.JENIS_PKRJN, X.STAT_KWN FROM DEMOGRAPHICS_ALL@DB5CETAK X INNER JOIN (SELECT ID_CETAK,NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY FROM (SELECT  ID_CETAK, NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY, RANK() OVER (PARTITION BY NIK ORDER BY REQ_DATE DESC, ID_CETAK DESC) RNK FROM SIAK_REQ_CETAK_KTP@YZDB WHERE STEP_PROC = 'A') WHERE RNK = 1) A ON A.NIK = X.NIK INNER JOIN DEMOGRAPHICS_ALL@DB5CETAK B ON A.NIK = B.NIK LEFT JOIN (SELECT NIK, PRINTED_DATE,PRINTED_BY FROM (SELECT NIK,PRINTED_DATE,PRINTED_BY, RANK() OVER (PARTITION BY NIK ORDER BY PRINTED_DATE DESC) RNK FROM T7_HIST_SUKET@DB222) WHERE RNK = 1) C ON A.NIK = C.NIK  WHERE 1=1 AND (B.CURRENT_STATUS_CODE NOT LIKE '%CARD%' AND B.CURRENT_STATUS_CODE NOT LIKE '%PRINT%') AND (A.REQ_DATE > C.PRINTED_DATE OR C.PRINTED_DATE IS NULL)";
			if($no_kec != 0){
				$sql .= " AND X.NO_KEC = $no_kec";
			}
			if($no_kel != 0){
				$sql .= " AND X.NO_KEL = $no_kel";
			} 
			if($nik != ""){
				$sql .= " AND X.NIK IN ($nik)";
			}
			$sql .= " AND A.REQ_DATE >= TO_DATE('$tgl_start','DD-MM-YYYY') AND A.REQ_DATE < TO_DATE('$tgl_end','DD-MM-YYYY') +1";

			$r = DB::connection('db5cetak')->select($sql);
			return $r;
		}

		public function scr_cek_req_data_dsuket($nik,$tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "";
			$sql .= "SELECT X.NO_KEC,X.NIK,X.NAMA_LGKP AS NAMA_LENGKAP, X.ALAMAT,X.AGAMA, CASE WHEN TO_CHAR(X.KODE_POS) IS NULL THEN '-' ELSE TO_CHAR(X.KODE_POS) END KODE_POS, LPAD(TO_CHAR(X.NO_RT), 3, '0') AS RT, LPAD(TO_CHAR(X.NO_RW), 3, '0') AS RW, B.CURRENT_STATUS_CODE,TO_CHAR(A.REQ_DATE,'DD-MM-YYYY') AS REQ_DATE,A.REQ_BY, TO_CHAR(C.PRINTED_DATE,'DD-MM-YYYY') AS PRINTED_DATE,C.PRINTED_BY, B.NAMA_KEC, B.NAMA_KEL, X.JENIS_PKRJN, X.STAT_KWN FROM DEMOGRAPHICS_ALL@DB5CETAK X INNER JOIN (SELECT ID_CETAK,NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY FROM (SELECT  ID_CETAK, NO_KEC,NO_KEL, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY, RANK() OVER (PARTITION BY NIK ORDER BY REQ_DATE DESC, ID_CETAK DESC) RNK FROM SIAK_REQ_CETAK_KTP@YZDB  WHERE STEP_PROC = 'A') WHERE RNK = 1) A ON A.NIK = X.NIK INNER JOIN DEMOGRAPHICS_ALL@DB5CETAK B ON A.NIK = B.NIK LEFT JOIN (SELECT NIK, PRINTED_DATE,PRINTED_BY FROM (SELECT NIK,PRINTED_DATE,PRINTED_BY, RANK() OVER (PARTITION BY NIK ORDER BY PRINTED_DATE DESC) RNK FROM T7_HIST_SUKET@DB222) WHERE RNK = 1) C ON A.NIK = C.NIK  WHERE 1=1 AND (B.CURRENT_STATUS_CODE NOT LIKE '%CARD%' AND B.CURRENT_STATUS_CODE NOT LIKE '%PRINT%') AND (A.REQ_DATE < C.PRINTED_DATE)";
			if($no_kec != 0){
				$sql .= " AND X.NO_KEC = $no_kec";
			}
			if($no_kel != 0){
				$sql .= " AND X.NO_KEL = $no_kel";
			} 
			if($nik != ""){
				$sql .= " AND X.NIK IN ($nik)";
			}
			$sql .= " AND A.REQ_DATE >= TO_DATE('$tgl_start','DD-MM-YYYY') AND A.REQ_DATE < TO_DATE('$tgl_end','DD-MM-YYYY') +1";
			$r = DB::connection('db5cetak')->select($sql);
			return $r;
		}

		

		
		public function scr_cek_req_data_bio($nik,$tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "";
			$sql .= "SELECT X.NO_KEC,X.NIK,X.NAMA_LGKP AS NAMA_LENGKAP, X.ALAMAT, CASE WHEN TO_CHAR(X.KODE_POS) IS NULL THEN '-' ELSE TO_CHAR(X.KODE_POS) END KODE_POS, LPAD(TO_CHAR(X.NO_RT), 3, '0') AS RT, LPAD(TO_CHAR(X.NO_RW), 3, '0') AS RW,  CASE WHEN B.CURRENT_STATUS_CODE IS NULL THEN 'REQ BIO' ELSE B.CURRENT_STATUS_CODE END AS CURRENT_STATUS_CODE,TO_CHAR(A.REQ_DATE,'DD-MM-YYYY') AS REQ_DATE,A.REQ_BY, TO_CHAR(C.CREATED,'DD-MM-YYYY') AS PRINTED_DATE,C.CREATED_USERNAME AS PRINTED_BY FROM DEMOGRAPHICS_ALL X INNER JOIN (SELECT ID_CETAK,NO_KEC, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY FROM (SELECT  ID_CETAK, NO_KEC, NIK,NAMA_LENGKAP,REQ_DATE,REQ_BY, RANK() OVER (PARTITION BY NIK ORDER BY REQ_DATE DESC, ID_CETAK DESC) RNK FROM SIAK_REQ_CETAK_KTP@YZDB WHERE STEP_PROC = 'A') WHERE RNK = 1) A ON A.NIK = X.NIK LEFT JOIN DEMOGRAPHICS_ALL B ON A.NIK = B.NIK LEFT JOIN DEMOGRAPHICS D ON A.NIK = D.NIK LEFT JOIN (SELECT NIK,CREATED,CREATED_USERNAME FROM (SELECT NIK,CREATED,CREATED_USERNAME, RANK() OVER (PARTITION BY NIK ORDER BY CREATED DESC) RNK FROM CARD_MANAGEMENT) WHERE RNK = 1) C ON A.NIK = C.NIK  WHERE 1=1 AND ((B.CURRENT_STATUS_CODE IS NULL) AND (D.CURRENT_STATUS_CODE IS NULL))";
			if($no_kec != 0){
				$sql .= " AND X.NO_KEC = $no_kec";
			}
			if($no_kel != 0){
				$sql .= " AND X.NO_KEL = $no_kel";
			} 
			if($nik != ""){
				$sql .= " AND X.NIK IN ($nik)";
			}
			$sql .= " AND A.REQ_DATE >= TO_DATE('$tgl_start','DD-MM-YYYY') AND A.REQ_DATE < TO_DATE('$tgl_end','DD-MM-YYYY') +1";			
			$r = DB::connection('db5cetak')->select($sql);
			return $r;
		}
		public function prr_get_list_prr($no_kec,$no_kel){
			$sql = "SELECT 
					TO_CHAR(A.NIK) AS NIK
					, A.NAMA_LGKP
					, A.TMPT_LHR, TO_CHAR(A.TGL_LHR,'DD-MM-YYYY') AS TGL_LHR
					, TRUNC(MONTHS_BETWEEN(SYSDATE,A.TGL_LHR)/12) UMUR
					, CASE WHEN A.ALAMAT IS NULL THEN '-' ELSE A.ALAMAT END AS ALAMAT
					, A.NO_KEC,  A.NAMA_KEC
					, A.NO_KEL,  A.NAMA_KEL
					, CASE WHEN A.NO_RW IS NULL THEN 0 ELSE A.NO_RW END AS NO_RW 
					, CASE WHEN A.NO_RT IS NULL THEN 0 ELSE A.NO_RT END AS NO_RT 
					, CASE WHEN TO_CHAR(A.KODE_POS) IS NULL THEN '-' ELSE TO_CHAR(A.KODE_POS) END KODE_POS
					, A.AGAMA
					, A.STAT_KWN STATUS_KAWIN
					, A.JENIS_PKRJN PEKERJAAN
					, A.GOL_DRH
					, TO_CHAR(A.CREATED,'YYYY-MM-DD') AS TGL_REKAM
					, A.CURRENT_STATUS_CODE AS STATUS_KTP
					, A.CREATED_USERNAME AS PETUGAS_REKAM
					FROM DEMOGRAPHICS@DB5 A 
					LEFT JOIN CARD_MANAGEMENT D ON A.NIK = D.NIK
					WHERE A.CURRENT_STATUS_CODE = 'PRINT_READY_RECORD' AND D.NIK IS NULL
					AND (A.TGL_LHR<=ADD_MONTHS(SYSDATE,-12*17) OR A.STAT_KWN<>'BELUM KAWIN') 
					 ";
			if($no_kec != 0){
				$sql .= " AND A.NO_KEC = $no_kec";
			}
			if($no_kel != 0){
				$sql .= " AND A.NO_KEL = $no_kel";
			} 
			$sql .=" ORDER BY A.CREATED, A.NO_KEL, A.NO_RW, A.NO_RT";
			$r = DB::connection('db5cetak')->select($sql);
			return $r;
		}

		public function prr_get_list_perekaman($tgl_start,$tgl_end,$no_kec,$no_kel,$nik,$no_kk){
		
        	$sql = "SELECT 
				TO_CHAR(A.NIK) AS NIK
				, A.NAMA_LGKP
				, A.TMPT_LHR, TO_CHAR(A.TGL_LHR,'DD-MM-YYYY') AS TGL_LHR
				, CASE WHEN A.ALAMAT IS NULL THEN '-' ELSE A.ALAMAT END AS ALAMAT
				, A.NO_KEC, A.NAMA_KEC
				, A.NO_KEL, A.NAMA_KEL
				, CASE WHEN A.NO_RW IS NULL THEN 0 ELSE A.NO_RW END AS NO_RW 
				, CASE WHEN A.NO_RT IS NULL THEN 0 ELSE A.NO_RT END AS NO_RT 
				, CASE WHEN TO_CHAR(A.KODE_POS) IS NULL THEN '-' ELSE TO_CHAR(A.KODE_POS) END KODE_POS
				, A.AGAMA
				, A.STAT_KWN STATUS_KAWIN
				, A.JENIS_PKRJN PEKERJAAN
				, TO_CHAR(A.CREATED,'YYYY-MM-DD') AS TGL_REKAM
	 			, TO_CHAR(A.CREATED,'HH24:MM') AS JAM_REKAM
				, A.CURRENT_STATUS_CODE AS STATUS_KTP
				, A.CREATED_USERNAME AS PETUGAS_REKAM
				FROM DEMOGRAPHICS@DB5 A 
				INNER JOIN FACES@DB5 X ON A.NIK = X.NIK
				WHERE 1=1
				 
	            ";
            if($nik != ''){
				$sql .= " AND A.NIK = $nik";
			}
            else if($no_kk != ''){
				$sql .= " AND A.NO_KK = $no_kk";
			}else{
				$sql .= " AND A.CREATED >= TO_DATE('$tgl_start','DD-MM-YYYY') AND A.CREATED < TO_DATE('$tgl_end','DD-MM-YYYY') +1";
			}
			
			if($no_kec != 0){
				$sql .= " AND A.NO_KEC = $no_kec";
			}
			if($no_kel != 0){
				$sql .= " AND A.NO_KEL = $no_kel";
			} 
			$sql .=" ORDER BY A.CREATED";
			$r = DB::connection('db5cetak')->select($sql);
			return $r;
		}
		public function prr_get_list_sudah_cetak($tgl_start,$tgl_end,$no_kec,$no_kel,$user){
			$sql = "SELECT 
					TO_CHAR(B.NIK) AS NIK
					, B.NAMA_LGKP
					, B.TMPT_LHR, TO_CHAR(B.TGL_LHR,'DD-MM-YYYY') AS TGL_LHR
					, CASE WHEN B.ALAMAT IS NULL THEN '-' ELSE B.ALAMAT END AS ALAMAT
					, B.NO_KEC, B.NAMA_KEC
					, B.NO_KEL, B.NAMA_KEL
					, CASE WHEN B.NO_RW IS NULL THEN 0 ELSE B.NO_RW END AS NO_RW 
					, CASE WHEN B.NO_RT IS NULL THEN 0 ELSE B.NO_RT END AS NO_RT 
					, CASE WHEN TO_CHAR(B.KODE_POS) IS NULL THEN '-' ELSE TO_CHAR(B.KODE_POS) END KODE_POS
					, B.AGAMA
					, B.STAT_KWN STATUS_KAWIN
					, B.JENIS_PKRJN PEKERJAAN
					, TO_CHAR(A.PERSONALIZED_DATE,'DD-MM-YYYY') AS TGL_CETAK
					, CASE WHEN TO_CHAR(A.LAST_UPDATED_USERNAME) IS NULL AND TO_CHAR(A.CREATED_USERNAME) IS NULL  THEN '-' WHEN TO_CHAR(A.LAST_UPDATED_USERNAME) IS NULL AND TO_CHAR(A.CREATED_USERNAME) IS NOT NULL THEN TO_CHAR(A.CREATED_USERNAME) ELSE TO_CHAR(A.LAST_UPDATED_USERNAME) END AS PETUGAS_CETAK
					FROM CARD_MANAGEMENT A 
					INNER JOIN DEMOGRAPHICS B ON A.NIK = B.NIK
					LEFT JOIN SIAK_CETAK_GOIB@YZDB D ON  CASE WHEN UPPER(A.LAST_UPDATED_USERNAME) IS NULL THEN UPPER(A.CREATED_USERNAME) ELSE UPPER(A.LAST_UPDATED_USERNAME) END =  UPPER(D.USER_BCARD) AND D.IS_ACTIVE = 1
					WHERE 1=1 AND D.USER_BCARD IS NULL
					AND (B.TGL_LHR<=ADD_MONTHS(SYSDATE,-12*17) OR B.STAT_KWN<>'BELUM KAWIN') 
					  AND CASE WHEN A.LAST_UPDATE IS NULL THEN A.PERSONALIZED_DATE ELSE A.LAST_UPDATE END >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND CASE WHEN A.LAST_UPDATE IS NULL THEN A.PERSONALIZED_DATE ELSE A.LAST_UPDATE END < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1
					    
                    ";
			if($no_kec != 0){
				$sql .= " AND B.NO_KEC = $no_kec";
			}
			if($no_kel != 0){
				$sql .= " AND B.NO_KEL = $no_kel";
			} 
			if($user != ''){
				$sql .= " AND CASE WHEN TO_CHAR(A.LAST_UPDATED_USERNAME) IS NULL AND TO_CHAR(A.CREATED_USERNAME) IS NULL  THEN '-' WHEN TO_CHAR(A.LAST_UPDATED_USERNAME) IS NULL AND TO_CHAR(A.CREATED_USERNAME) IS NOT NULL THEN TO_CHAR(A.CREATED_USERNAME) ELSE TO_CHAR(A.LAST_UPDATED_USERNAME) END = '$user'";
			} 
			$sql .=" ORDER BY A.PERSONALIZED_DATE DESC, B.NO_KEL, B.NO_RW, B.NO_RT";
			$r = DB::connection('db5cetak')->select($sql);
			return $r;
		}
		public function prr_get_list_sfe($no_kec,$no_kel){
			$sql = "SELECT 
					TO_CHAR(B.NIK) AS NIK
					, B.NAMA_LGKP
					, B.TMPT_LHR, TO_CHAR(B.TGL_LHR,'DD-MM-YYYY') AS TGL_LHR
					, CASE WHEN C.ALAMAT IS NULL THEN '-' ELSE C.ALAMAT END AS ALAMAT
					, B.NO_KEC, B.NAMA_KEC
					, B.NO_KEL, B.NAMA_KEL
					, CASE WHEN C.NO_RW IS NULL THEN 0 ELSE C.NO_RW END AS NO_RW 
					, CASE WHEN C.NO_RT IS NULL THEN 0 ELSE C.NO_RT END AS NO_RT 
					, CASE WHEN TO_CHAR(C.KODE_POS) IS NULL THEN '-' ELSE TO_CHAR(C.KODE_POS) END KODE_POS
					, B.AGAMA
					, B.STAT_KWN STATUS_KAWIN
					, B.JENIS_PKRJN PEKERJAAN
					, TO_CHAR(A.CREATED,'YYYY-MM-DD') AS TGL_REKAM
					, A.CURRENT_STATUS_CODE AS STATUS_KTP
					FROM DEMOGRAPHICS@DB5 A 
					INNER JOIN DEMOGRAPHICS_ALL@DB5CETAK B ON A.NIK = B.NIK
					WHERE A.CURRENT_STATUS_CODE = 'SENT_FOR_ENROLLMENT'
					AND (B.TGL_LHR<=ADD_MONTHS(SYSDATE,-12*17) OR B.STAT_KWN<>'BELUM REKAM') 
					 
					 ";
			if($no_kec != 0){
				$sql .= " AND B.NO_KEC = $no_kec";
			}
			if($no_kel != 0){
				$sql .= " AND B.NO_KEL = $no_kel";
			} 
			$sql .=" ORDER BY A.CREATED, B.NO_KEL, C.NO_RW, C.NO_RT";
			$r = DB::select($sql);
			return $r;
		}

		public function prr_get_list_duplicate($no_kec,$no_kel){
			$sql = "SELECT 
					TO_CHAR(A.NIK) AS NIK
                    , CASE WHEN D.NIK_SINGLE IS NULL THEN '-' ELSE D.NIK_SINGLE END NIK_SINGLE
					, A.NAMA_LGKP
					, A.TMPT_LHR, TO_CHAR(A.TGL_LHR,'DD-MM-YYYY') AS TGL_LHR
					, CASE WHEN A.ALAMAT IS NULL THEN '-' ELSE A.ALAMAT END AS ALAMAT
					, A.NO_KEC, A.NAMA_KEC
					, A.NO_KEL, A.NAMA_KEL
					, CASE WHEN A.NO_RW IS NULL THEN 0 ELSE A.NO_RW END AS NO_RW 
					, CASE WHEN A.NO_RT IS NULL THEN 0 ELSE A.NO_RT END AS NO_RT 
					, CASE WHEN TO_CHAR(A.KODE_POS) IS NULL THEN '-' ELSE TO_CHAR(A.KODE_POS) END KODE_POS
					, A.AGAMA AGAMA
					, A.STAT_KWN STATUS_KAWIN
					, A.JENIS_PKRJN PEKERJAAN
					, TO_CHAR(A.CREATED,'YYYY-MM-DD') AS TGL_REKAM
					, A.CURRENT_STATUS_CODE AS STATUS_KTP
					FROM DEMOGRAPHICS@DB5 A 
                    LEFT JOIN (SELECT  A.NIK
					, A.NIK_SINGLE
                    FROM 
                    (SELECT  A.NIK
					, TO_CHAR(A.NIK_SINGLE) NIK_SINGLE
          			FROM SIAK_DUPLICATE_FULL A
                        UNION ALL
                        SELECT  B.NIK
                            , B.NIK_SINGLE
                            FROM (
                            SELECT  C.NIK
                            , TO_CHAR(C.DUPLICATE_NIK) NIK_SINGLE
                   FROM DUPLICATE_RESULTS@DB5 C WHERE 
                   NOT EXISTS (SELECT 1 FROM SIAK_DUPLICATE_FULL A WHERE A.NIK = C.NIK)) B
                        ) A) D ON A.NIK = D.NIK
					WHERE A.CURRENT_STATUS_CODE = 'DUPLICATE_RECORD' 
					AND (A.TGL_LHR<=ADD_MONTHS(SYSDATE,-12*17) OR A.STAT_KWN<>'BELUM KAWIN') 
					 ";
			if($no_kec != 0){
				$sql .= " AND A.NO_KEC = $no_kec";
			}
			if($no_kel != 0){
				$sql .= " AND A.NO_KEL = $no_kel";
			} 
			$sql .=" ORDER BY A.CREATED, A.NO_KEL, A.NO_RW, A.NO_RT";
			$r = DB::select($sql);
			return $r;
		}
		public function prr_get_list_failure($no_kec,$no_kel){
			$sql = "SELECT 
					TO_CHAR(B.NIK) AS NIK
					, B.NAMA_LGKP
					, B.TMPT_LHR, TO_CHAR(B.TGL_LHR,'DD-MM-YYYY') AS TGL_LHR
					, CASE WHEN C.ALAMAT IS NULL THEN '-' ELSE C.ALAMAT END AS ALAMAT
					, B.NO_KEC, B.NAMA_KEC
					, B.NO_KEL, B.NAMA_KEL
					, CASE WHEN C.NO_RW IS NULL THEN 0 ELSE C.NO_RW END AS NO_RW 
					, CASE WHEN C.NO_RT IS NULL THEN 0 ELSE C.NO_RT END AS NO_RT 
					, CASE WHEN TO_CHAR(C.KODE_POS) IS NULL THEN '-' ELSE TO_CHAR(C.KODE_POS) END KODE_POS
					, B.AGAMA
					, B.STAT_KWN STATUS_KAWIN
					, B.JENIS_PKRJN PEKERJAAN
					, TO_CHAR(A.CREATED,'YYYY-MM-DD') AS TGL_REKAM
					, A.CURRENT_STATUS_CODE AS STATUS_KTP
					FROM DEMOGRAPHICS@DB5 A 
					INNER JOIN DEMOGRAPHICS_ALL@DB5CETAK B ON A.NIK = B.NIK
					WHERE A.CURRENT_STATUS_CODE NOT LIKE '%CARD%' AND A.CURRENT_STATUS_CODE NOT LIKE '%PRINT%' AND  A.CURRENT_STATUS_CODE NOT LIKE '%DUPLICATE%' AND A.CURRENT_STATUS_CODE NOT LIKE 'SENT_FOR_ENROLLMENT'
					AND (B.TGL_LHR<=ADD_MONTHS(SYSDATE,-12*17) OR B.STAT_KWN<>'BELUM REKAM') 
					 
					 ";
			if($no_kec != 0){
				$sql .= " AND B.NO_KEC = $no_kec";
			}
			if($no_kel != 0){
				$sql .= " AND B.NO_KEL = $no_kel";
			} 
			$sql .=" ORDER BY A.CREATED, B.NO_KEL, C.NO_RW, C.NO_RT";
			$r = DB::select($sql);
			return $r;
		}
		
}
