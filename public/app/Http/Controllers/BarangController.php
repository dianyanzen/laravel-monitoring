<?php

namespace App\Http\Controllers;

use DB;
use Session;
use DateTime;
use PDF;
use Illuminate\Http\Request;
use App\Http\Controllers\SharedController as Shr;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class BarangController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function master_barang(Request $request) 
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 167;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$data = array(
		 		"stitle"=>'Master Barang',
		 		"mtitle"=>'Master Barang',
		 		"my_url"=>'JenisBarang',
		 		"type_tgl"=>'Request',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
    		);
			return view('Master_barang/main',$data);
			}else{
			return redirect()->route('logout');
		}
	}
	public function master_aset(Request $request) 
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 181;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$data = array(
		 		"stitle"=>'Master Aset',
		 		"mtitle"=>'Master Aset',
		 		"my_url"=>'MasterAset',
		 		"type_tgl"=>'Request',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
    		);
			return view('Master_aset/main',$data);
			}else{
			return redirect()->route('logout');
		}
	}
	public function Master_aset_in(Request $request) 
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 152;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$data = array(
		 		"stitle"=>'Input Aset',
		 		"mtitle"=>'Input Aset',
		 		"my_url"=>'InputAset',
		 		"type_tgl"=>'Request',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
    		);
			return view('Master_aset_in/main',$data);
			}else{
			return redirect()->route('logout');
		}
	}
	public function Status_barang(Request $request) 
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 150;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$data = array(
		 		"stitle"=>'Status Barang',
		 		"mtitle"=>'Status Barang',
		 		"my_url"=>'JenisBarang',
		 		"type_tgl"=>'Request',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
    		);
			return view('Master_barang_status/main',$data);
			}else{
			return redirect()->route('logout');
		}
	}
	public function Laporan_barang(Request $request) 
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 150;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$data = array(
		 		"stitle"=>'Laporan Barang',
		 		"mtitle"=>'Laporan Barang',
		 		"my_url"=>'JenisBarang',
		 		"type_tgl"=>'Request',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
    		);
			return view('Master_barang_laporan/main',$data);
			}else{
			return redirect()->route('logout');
		}
	}
	public function edit_master_barang(Request $request, $id) 
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 167;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$barang_id = $id;
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
		    $databarang = $this->brg_get_data_barang($barang_id);

			$data = array(
		 		"stitle"=>'Master Barang',
		 		"mtitle"=>'Master Barang',
		 		"my_url"=>'JenisBarang',
		 		"type_tgl"=>'Request',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"data"=>$databarang,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
    		);
			return view('Master_barang_edit/main',$data);
			}else{
			return redirect()->route('logout');
		}
	}
	public function edit_master_barang_in(Request $request, $id) 
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 167;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$barang_id = $id;
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
		    $databarang = $this->brg_get_data_barang_in($barang_id);
			$data = array(
		 		"stitle"=>'Barang Masuk',
		 		"mtitle"=>'Barang Masuk',
		 		"my_url"=>'JenisBarang',
		 		"type_tgl"=>'Request',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"data"=>$databarang,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
    		);
			return view('Master_barang_edit_in/main',$data);
			}else{
			return redirect()->route('logout');
		}
	}
	public function edit_jenis_barang(Request $request, $id) 
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 167;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$barang_id = $id;
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
		    $databarang = $this->brg_get_data_barang($barang_id);
		/*    print_r($databarang);
		    echo $databarang[0]->tanggal;
		    die;*/
			$data = array(
		 		"stitle"=>'Barang Masuk',
		 		"mtitle"=>'Barang Masuk',
		 		"my_url"=>'JenisBarang',
		 		"type_tgl"=>'Request',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"data"=>$databarang,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
    		);
			return view('Master_barang_edit/main',$data);
			}else{
			return redirect()->route('logout');
		}
	}
	public function edit_master_barang_out(Request $request, $id) 
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 167;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$barang_id = $id;
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
		    $databarang = $this->brg_get_data_barang_out($barang_id);
			$data = array(
		 		"stitle"=>'Barang Keluar',
		 		"mtitle"=>'Barang Keluar',
		 		"my_url"=>'JenisBarang',
		 		"type_tgl"=>'Request',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"data"=>$databarang,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
    		);
			return view('Master_barang_edit_out/main',$data);
			}else{
			return redirect()->route('logout');
		}
	}
	public function get_master_barang(Request $request)
     {
          $draw = intval($request->draw);
          $start = intval($request->start);
          $length = intval($request->length);

          $user_data = $this->brg_get_master_barang();

          $data = array();
          $i = 0;
          foreach($user_data as $r) {
          		$i ++;
               $data[] = array(
               		$i,
               		$r->jenis_barang,
                    $r->satuan,
                    'Rp.'.number_format($r->harga,2),
                    '<button type="button" class="btn btn-primary btn-xs" id="btn-edit" onclick="edit('.$r->barang_id.');" style="margin-top: 5px;">Edit <i class="mdi  mdi-tooltip-edit fa-fw"></i></button> &nbsp <button type="button" class="btn btn-danger btn-xs" id="btn-delete" onclick="hapus('.$r->barang_id.');" style="margin-top: 5px;">Delete <i class="mdi mdi-delete-forever fa-fw"></i></button>'
               );
          }

          $output = array(
               "draw" => $draw,
                 "recordsTotal" => count($user_data),
                 "recordsFiltered" => count($user_data),
                 "data" => $data
            );
          return $output;
          exit();
	}
	public function get_master_aset(Request $request)
     {
          $draw = intval($request->draw);
          $start = intval($request->start);
          $length = intval($request->length);
          $kategori = $request->kategori;
          $user_data = $this->brg_get_master_aset($kategori);

          $data = array();
          $i = 0;
          foreach($user_data as $r) {
          		$i ++;
               $data[] = array(
               		$i,
                    $r->descrip,
                    '<button type="button" class="btn btn-primary btn-xs" id="btn-edit" onclick="edit(\''.$r->no.'\',\''.$r->sect.'\');" style="margin-top: 5px;">Edit <i class="mdi  mdi-tooltip-edit fa-fw"></i></button> &nbsp <button type="button" class="btn btn-danger btn-xs" id="btn-delete" onclick="hapus(\''.$r->no.'\',\''.$r->sect.'\');" style="margin-top: 5px;">Delete <i class="mdi mdi-delete-forever fa-fw"></i></button>'
               );
          }

          $output = array(
               "draw" => $draw,
                 "recordsTotal" => count($user_data),
                 "recordsFiltered" => count($user_data),
                 "data" => $data
            );
          return $output;
          exit();
	}
	public function get_master_barang_laporan(Request $request)
     {
          $draw = intval($request->draw);
          $start = intval($request->start);
          $length = intval($request->length);
          $barang = $request->barang;
          $tgl = $request->tanggal;
		  $tgl_start = substr($tgl, 0, 10);
		  $tgl_end = substr($tgl,13, 10);
          $user_data = $this->brg_get_master_barang_laporan($tgl_start,$tgl_end,$barang);

          $data = array();
          $i = 0;
          foreach($user_data as $r) {
          		$i ++;
               $data[] = array(
               		$i,
               		$r->jenis_barang,
               		$r->ket,
                    $r->jumlah,
                    'Rp.'.number_format($r->harga),
                    $r->tanggal,
                    $r->received,
               );
          }

          $output = array(
               "draw" => $draw,
                 "recordsTotal" => count($user_data),
                 "recordsFiltered" => count($user_data),
                 "data" => $data
            );
          return $output;
          exit();
	}
	public function get_master_barang_status(Request $request)
     {
          $draw = intval($request->draw);
          $start = intval($request->start);
          $length = intval($request->length);

          $user_data = $this->brg_get_master_barang_status();

          $data = array();
          $i = 0;
          foreach($user_data as $r) {
          		$i ++;
               $data[] = array(
               		$i,
               		$r->jenis_barang,
                    $r->satuan,
                    'Rp.'.number_format($r->harga),
                    $r->jml_masuk,
                    $r->jml_keluar,
                    $r->saldo,
                    'Rp.'.number_format($r->harga_aset)
                    
               );
          }

          $output = array(
               "draw" => $draw,
                 "recordsTotal" => count($user_data),
                 "recordsFiltered" => count($user_data),
                 "data" => $data
            );
          return $output;
          exit();
	}
	public function get_master_barang_in(Request $request)
     {
          $draw = intval($request->draw);
          $start = intval($request->start);
          $length = intval($request->length);

          $user_data = $this->brg_get_master_barang_in();

          $data = array();
          $i = 0;
          foreach($user_data as $r) {
          		$i ++;
               $data[] = array(
               		$i,
               		$r->jenis_barang,
                    $r->jumlah,
                    'Rp.'.number_format($r->harga),
                    $r->tanggal,
                    $r->received,
                    '<button type="button" class="btn btn-primary btn-xs" id="btn-edit" onclick="edit('.$r->seq_id.');" style="margin-top: 5px;">Edit <i class="mdi  mdi-tooltip-edit fa-fw"></i></button> &nbsp <button type="button" class="btn btn-danger btn-xs" id="btn-delete" onclick="hapus('.$r->seq_id.');" style="margin-top: 5px;">Delete <i class="mdi mdi-delete-forever fa-fw"></i></button>'
               );
          }

          $output = array(
               "draw" => $draw,
                 "recordsTotal" => count($user_data),
                 "recordsFiltered" => count($user_data),
                 "data" => $data
            );
          return $output;
          exit();
	}
	public function get_master_barang_out(Request $request)
     {
          $draw = intval($request->draw);
          $start = intval($request->start);
          $length = intval($request->length);

          $user_data = $this->brg_get_master_barang_out();

          $data = array();
          $i = 0;
          foreach($user_data as $r) {
          		$i ++;
               $data[] = array(
               		$i,
               		$r->jenis_barang,
                    $r->jumlah,
                    'Rp.'.number_format($r->harga),
                    $r->tanggal,
                    $r->received,
                    '<button type="button" class="btn btn-primary btn-xs" id="btn-edit" onclick="edit('.$r->seq_id.');" style="margin-top: 5px;">Edit <i class="mdi  mdi-tooltip-edit fa-fw"></i></button> &nbsp <button type="button" class="btn btn-danger btn-xs" id="btn-delete" onclick="hapus('.$r->seq_id.');" style="margin-top: 5px;">Delete <i class="mdi mdi-delete-forever fa-fw"></i></button>'
               );
          }

          $output = array(
               "draw" => $draw,
                 "recordsTotal" => count($user_data),
                 "recordsFiltered" => count($user_data),
                 "data" => $data
            );
          return $output;
          exit();
	}
	public function insert_master_aset(Request $request) 
	{
		if($request->kd_kategori != null){
			$kd_kategori = $request->kd_kategori;
			$value_data = strtoupper($request->value_data);
			$j = $this->brg_max_aset($kd_kategori);
			$this->brg_save_aset($j,$kd_kategori,$value_data);

			$data["success"] = TRUE;
			$data["message"] = "Data Berhasil Di Simpan";
       		return $data;
		}else{
			return redirect()->route('logout');
		}
	}
	public function get_modal_data(Request $request) 
	{
		if($request->aset_no != null){
			$aset_no = $request->aset_no;
			$aset_sect = $request->aset_sect;
			$r = $this->brg_aset_select_data($aset_no,$aset_sect);

			$data["success"] = TRUE;
			$data["message"] = "Data Berhasil Di Simpan";
			$data["value_data"] = $r;
       		return $data;
		}else{
			return redirect()->route('logout');
		}
	}
	public function insert_master_barang(Request $request) 
	{
		if($request->jenis_barang != null){
			$jenis_barang = $request->jenis_barang;
			$satuan = $request->satuan;
			$harga_satuan = $request->harga_satuan;
			$keterangan = $request->keterangan;
			$j = $this->brg_max_barang();
			$this->brg_save_barang($j,$jenis_barang,$satuan,$harga_satuan,$keterangan,Session::get('S_USER_ID'));

			$data["success"] = TRUE;
			$data["message"] = "Data Berhasil Di Simpan";
       		return $data;
		}else{
			return redirect()->route('logout');
		}
	}
	public function insert_master_in_barang(Request $request) 
	{
		if($request->kd_brg != null){
			$kd_brg = $request->kd_brg;
			$received = $request->received;
			$jumlah = $request->jumlah;
			$keterangan = $request->keterangan;
			$j = $this->brg_max_barang_in();
			$this->brg_save_barang_in($j,$kd_brg,$received,$jumlah,$keterangan,Session::get('S_USER_ID'));

			$data["success"] = TRUE;
			$data["message"] = "Data Berhasil Di Simpan";
       		return $data;
		}else{
			return redirect()->route('logout');
		}
	}
	public function insert_master_out_barang(Request $request) 
	{
		if($request->kd_brg != null){
			$kd_brg = $request->kd_brg;
			$received = $request->received;
			$jumlah = $request->jumlah;
			$keterangan = $request->keterangan;
			$j = $this->brg_max_barang_out();
			$this->brg_save_barang_out($j,$kd_brg,$received,$jumlah,$keterangan,Session::get('S_USER_ID'));

			$data["success"] = TRUE;
			$data["message"] = "Data Berhasil Di Simpan";
       		return $data;
		}else{
			return redirect()->route('logout');
		}
	}
	public function do_edit_master_aset(Request $request) 
	{
		if($request->mdl_no_data != null){
			$mdl_no_data = $request->mdl_no_data;
			$mdl_value_data = strtoupper($request->mdl_value_data);
			$mdl_sect_data = $request->mdl_sect_data;
			$this->brg_edit_aset($mdl_no_data,$mdl_value_data,$mdl_sect_data);

			$data["success"] = TRUE;
			$data["message"] = "Data Berhasil Di Ubah";
       		return $data;
		}else{
			return redirect()->route('logout');
		}
	}
	public function do_edit_master_barang(Request $request) 
	{
		if($request->kode_barang != null){
			$kode_barang = $request->kode_barang;
			$jenis_barang = $request->jenis_barang;
			$satuan = $request->satuan;
			$harga_satuan = $request->harga_satuan;
			$keterangan = $request->keterangan;
			$this->brg_edit_barang($kode_barang,$jenis_barang,$satuan,$harga_satuan,$keterangan,Session::get('S_USER_ID'));

			$data["success"] = TRUE;
			$data["message"] = "Data Berhasil Di Ubah";
       		return $data;
		}else{
			return redirect()->route('logout');
		}
	}
	public function do_edit_master_barang_in(Request $request) 
	{
		if($request->kode_barang != null){
			$kode_barang = $request->kode_barang;
			$kd_brg = $request->kd_brg;
			$received = $request->received;
			$jumlah = $request->jumlah;
			$keterangan = $request->keterangan;
			$this->brg_edit_barang_in($kode_barang,$kd_brg,$received,$jumlah,$keterangan,Session::get('S_USER_ID'));

			$data["success"] = TRUE;
			$data["message"] = "Data Berhasil Di Ubah";
       		return $data;
		}else{
			return redirect()->route('logout');
		}
	}
	public function do_edit_master_barang_out(Request $request) 
	{
		if($request->kode_barang != null){
			$kode_barang = $request->kode_barang;
			$kd_brg = $request->kd_brg;
			$received = $request->received;
			$jumlah = $request->jumlah;
			$keterangan = $request->keterangan;
			$this->brg_edit_barang_out($kode_barang,$kd_brg,$received,$jumlah,$keterangan,Session::get('S_USER_ID'));

			$data["success"] = TRUE;
			$data["message"] = "Data Berhasil Di Ubah";
       		return $data;
		}else{
			return redirect()->route('logout');
		}
	}
	public function delete_master_barang(Request $request) 
	{
		if($request->barang != null){
			$barang = $request->barang;
			$this->brg_delete_master_barang($barang);
			$data["success"] = TRUE;
			$data["message"] = "Data Berhasil Di Hapus";
       		return $data;
		}else{
			return redirect()->route('logout');
		}
	}
	public function delete_master_aset(Request $request) 
	{
		if($request->aset_no != null){
			$aset_no = $request->aset_no;
			$aset_sect = $request->aset_sect;
			$this->brg_delete_master_aset($aset_no,$aset_sect);
			$data["success"] = TRUE;
			$data["message"] = "Data Berhasil Di Hapus";
       		return $data;
		}else{
			return redirect()->route('logout');
		}
	}
	public function delete_master_in_barang(Request $request) 
	{
		if($request->barang != null){
			$barang = $request->barang;
			$this->brg_delete_master_in_barang($barang);
			$data["success"] = TRUE;
			$data["message"] = "Data Berhasil Di Hapus";
       		return $data;
		}else{
			return redirect()->route('logout');
		}
	}
	public function delete_master_out_barang(Request $request) 
	{
		if($request->barang != null){
			$barang = $request->barang;
			$this->brg_delete_master_out_barang($barang);
			$data["success"] = TRUE;
			$data["message"] = "Data Berhasil Di Hapus";
       		return $data;
		}else{
			return redirect()->route('logout');
		}
	}
	public function get_barang(Request $request)
	{
		header("Content-Type: application/json", true);
		$r = $this->brg_get_master_barang();
		return $r;
	}
	public function master_barang_in(Request $request) 
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 149;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$data = array(
		 		"stitle"=>'Input Barang Masuk',
		 		"mtitle"=>'Input Barang Masuk',
		 		"my_url"=>'JenisBarang',
		 		"type_tgl"=>'Request',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
    		);
			return view('master_barang_in/main',$data);
			}else{
			return redirect()->route('logout');
		}
	}
	public function master_barang_out(Request $request) 
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 165;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$data = array(
		 		"stitle"=>'Input Barang Keluar',
		 		"mtitle"=>'Input Barang Keluar',
		 		"my_url"=>'JenisBarang',
		 		"type_tgl"=>'Request',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
    		);
			return view('master_barang_out/main',$data);
			}else{
			return redirect()->route('logout');
		}
	}
	public function brg_max_barang(){
            $sql = "SELECT CASE WHEN MAX(BARANG_ID) +1 IS NULL THEN 1 ELSE MAX(BARANG_ID) +1 END JML FROM SIAK_MASTER_BARANG";
             $r = DB::select($sql);
           	return $r[0]->jml;
        }
        public function brg_max_aset($kategori){
            $sql = "SELECT CASE WHEN MAX(NO) +1 IS NULL THEN 1 ELSE MAX(NO) +1 END JML FROM SIAK_MASTER_ASET WHERE SECT = $kategori";
             $r = DB::select($sql);
            return $r[0]->jml;
        }
        public function brg_max_barang_in(){
            $sql = "SELECT CASE WHEN MAX(SEQ_ID) +1 IS NULL THEN 1 ELSE MAX(SEQ_ID) +1 END JML FROM SIAK_BARANG_MASUK";
             $r = DB::select($sql);
            return $r[0]->jml;
        }
        public function brg_max_barang_out(){
            $sql = "SELECT CASE WHEN MAX(SEQ_ID) +1 IS NULL THEN 1 ELSE MAX(SEQ_ID) +1 END JML FROM SIAK_BARANG_KELUAR";
             $r = DB::select($sql);
            return $r[0]->jml;
        }
        public function brg_get_master_barang(){
            $sql = "SELECT BARANG_ID, JENIS_BARANG, SATUAN, HARGA, KETERANGAN FROM SIAK_MASTER_BARANG ORDER BY BARANG_ID";
             $r = DB::select($sql);
            return $r;
        }
        public function brg_get_master_aset($kategori){
            $sql = "SELECT NO, DESCRIP, SECT FROM SIAK_MASTER_ASET WHERE SECT = $kategori ORDER BY NO";
             $r = DB::select($sql);
            return $r;
        }
        public function brg_get_master_barang_status(){
            $sql = "SELECT 
                        A.BARANG_ID
                        , A.JENIS_BARANG
                        , A.SATUAN
                        , A.HARGA
                        , CONCAT((CASE WHEN B.JML IS NULL THEN 0 ELSE B.JML END),CONCAT(' ',A.SATUAN)) JML_MASUK
                        , CONCAT((CASE WHEN C.JML IS NULL THEN 0 ELSE C.JML END),CONCAT(' ',A.SATUAN)) JML_KELUAR 
                        , CONCAT((CASE WHEN B.JML IS NULL THEN 0 ELSE B.JML END - CASE WHEN C.JML IS NULL THEN 0 ELSE C.JML END),CONCAT(' ',A.SATUAN)) SALDO
                        , ((CASE WHEN B.JML IS NULL THEN 0 ELSE B.JML END - CASE WHEN C.JML IS NULL THEN 0 ELSE C.JML END) * A.HARGA) HARGA_ASET
                    FROM SIAK_MASTER_BARANG A 
                    LEFT JOIN (SELECT KODE_BARANG, SUM(JUMLAH) JML FROM SIAK_BARANG_MASUK GROUP BY KODE_BARANG) B ON A.BARANG_ID = B.KODE_BARANG
                    LEFT JOIN (SELECT KODE_BARANG, SUM(JUMLAH) JML FROM SIAK_BARANG_KELUAR GROUP BY KODE_BARANG) C ON A.BARANG_ID = C.KODE_BARANG
                    ORDER BY BARANG_ID";
             $r = DB::select($sql);
            return $r;
        }
        public function brg_get_master_barang_laporan($tgl_start,$tgl_end,$barang){
            $sql = "";
            $sql .= "SELECT SEQ_ID, JENIS_BARANG, KODE_BARANG,KET, CONCAT(JUMLAH,CONCAT(' ',SATUAN)) JUMLAH, HARGA, SATUAN,TO_CHAR(TANGGAL,'DD-MM-YYYY') TANGGAL, RECEIVED  FROM 
                    (SELECT A.SEQ_ID,B.JENIS_BARANG,'BARANG MASUK' KET,A.KODE_BARANG, A.JUMLAH, B.SATUAN, (A.JUMLAH * B.HARGA) HARGA, A.CREATED_DT TANGGAL, A.RECEIVED 
                    FROM SIAK_BARANG_MASUK A LEFT JOIN SIAK_MASTER_BARANG B ON A.KODE_BARANG = B.BARANG_ID 
                    UNION ALL
                    SELECT A.SEQ_ID,B.JENIS_BARANG,'BARANG KELUAR' KET,A.KODE_BARANG, A.JUMLAH, B.SATUAN, (A.JUMLAH * B.HARGA) HARGA, A.CREATED_DT TANGGAL, A.RECEIVED 
                    FROM SIAK_BARANG_KELUAR A LEFT JOIN SIAK_MASTER_BARANG B ON A.KODE_BARANG = B.BARANG_ID) 
                    WHERE 1=1 AND TANGGAL >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND TANGGAL < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1 ";
            if ($barang != 0){
                $sql .= "AND KODE_BARANG = $barang";
            }
            $sql .= "ORDER BY TANGGAL";
             $r = DB::select($sql);
            return $r;
        }
        public function brg_get_master_barang_in(){
            $sql = "SELECT A.SEQ_ID,B.JENIS_BARANG, CONCAT(A.JUMLAH,CONCAT(' ',B.SATUAN)) JUMLAH, (A.JUMLAH * B.HARGA) HARGA, TO_CHAR(A.CREATED_DT,'DD-MM-YYYY') TANGGAL, A.RECEIVED FROM SIAK_BARANG_MASUK A LEFT JOIN SIAK_MASTER_BARANG B ON A.KODE_BARANG = B.BARANG_ID ORDER BY A.CREATED_DT DESC";
             $r = DB::select($sql);
            return $r;
        }
        public function brg_get_master_barang_out(){
            $sql = "SELECT A.SEQ_ID,B.JENIS_BARANG, CONCAT(A.JUMLAH,CONCAT(' ',B.SATUAN)) JUMLAH, (A.JUMLAH * B.HARGA) HARGA, TO_CHAR(A.CREATED_DT,'DD-MM-YYYY') TANGGAL, A.RECEIVED FROM SIAK_BARANG_KELUAR A LEFT JOIN SIAK_MASTER_BARANG B ON A.KODE_BARANG = B.BARANG_ID ORDER BY A.CREATED_DT DESC";
             $r = DB::select($sql);
            return $r;
        }
        public function brg_get_data_barang($barang_id){
            $sql = "SELECT BARANG_ID, JENIS_BARANG, SATUAN, HARGA, KETERANGAN, TO_CHAR(CREATED_DT,'DD-MM-YYYY') TANGGAL FROM SIAK_MASTER_BARANG WHERE BARANG_ID = $barang_id";
             $r = DB::select($sql);
      		return $r;
        }
        public function brg_aset_select_data($aset_no,$aset_sect){
            $sql = "SELECT DESCRIP FROM SIAK_MASTER_ASET WHERE NO = $aset_no AND SECT = $aset_sect";
             $r = DB::select($sql);
            return $r[0]->descrip;
        }
        public function brg_get_data_barang_in($barang_id){
            $sql = "SELECT SEQ_ID, KODE_BARANG, JUMLAH, RECEIVED, KETERANGAN, TO_CHAR(CREATED_DT,'DD-MM-YYYY') TANGGAL FROM SIAK_BARANG_MASUK WHERE SEQ_ID = $barang_id";
             $r = DB::select($sql);
            return $r;
        }     
        public function brg_get_data_barang_out($barang_id){
            $sql = "SELECT SEQ_ID, KODE_BARANG, JUMLAH, RECEIVED, KETERANGAN, TO_CHAR(CREATED_DT,'DD-MM-YYYY') TANGGAL FROM SIAK_BARANG_KELUAR WHERE SEQ_ID = $barang_id";
             $r = DB::select($sql);
            return $r;
        }
        public function brg_save_barang($barang_id,$jenis_barang,$satuan,$harga_satuan,$keterangan,$user_id){
          $sql = "INSERT INTO SIAK_MASTER_BARANG (BARANG_ID ,JENIS_BARANG ,SATUAN ,HARGA ,KETERANGAN ,CREATED_BY ,CREATED_DT) VALUES ($barang_id,'$jenis_barang','$satuan',$harga_satuan,'$keterangan','$user_id',SYSDATE)";
             $r = DB::insert($sql);
            return $r;
        }
        public function brg_save_aset($barang_id,$sect,$descrip){
          $sql = "INSERT INTO SIAK_MASTER_ASET (NO ,DESCRIP ,SECT) VALUES ($barang_id,'$descrip',$sect)";
             $r = DB::insert($sql);
            return $r;
        }
        public function brg_save_barang_in($barang_id,$kd_brg,$received,$jumlah,$keterangan,$user_id){
          $sql = "INSERT INTO SIAK_BARANG_MASUK (SEQ_ID ,KODE_BARANG ,RECEIVED ,JUMLAH ,KETERANGAN ,CREATED_BY ,CREATED_DT) VALUES ($barang_id,$kd_brg,'$received',$jumlah,'$keterangan','$user_id',SYSDATE)";
             $r = DB::insert($sql);
            return $r;
        }
        public function brg_save_barang_out($barang_id,$kd_brg,$received,$jumlah,$keterangan,$user_id){
          $sql = "INSERT INTO SIAK_BARANG_KELUAR (SEQ_ID ,KODE_BARANG ,RECEIVED ,JUMLAH ,KETERANGAN ,CREATED_BY ,CREATED_DT) VALUES ($barang_id,$kd_brg,'$received',$jumlah,'$keterangan','$user_id',SYSDATE)";
             $r = DB::insert($sql);
            return $r;
        }
        public function brg_edit_aset($mdl_no_data,$mdl_value_data,$mdl_sect_data){
          $sql = "UPDATE SIAK_MASTER_ASET 
                       SET 
                       DESCRIP = '$mdl_value_data'
                     WHERE NO = $mdl_no_data AND SECT = $mdl_sect_data";

             $r = DB::update($sql);
            return $r;
        }
        public function brg_edit_barang($barang_id,$jenis_barang,$satuan,$harga_satuan,$keterangan,$user_id){
          $sql = "UPDATE SIAK_MASTER_BARANG 
                       SET 
                       JENIS_BARANG = '$jenis_barang'
                       , SATUAN = '$satuan'
                       , HARGA = $harga_satuan
                       , KETERANGAN = '$keterangan' 
                       , CHANGED_BY = '$user_id' 
                       , CHANGED_DT = SYSDATE 
                     WHERE BARANG_ID = $barang_id";
             $r = DB::update($sql);
            return $r;
        }
        public function brg_edit_barang_in($kode_barang,$kd_brg,$received,$jumlah,$keterangan,$user_id){
          $sql = "UPDATE SIAK_BARANG_MASUK 
                       SET 
                       KODE_BARANG = '$kd_brg'
                       , RECEIVED = '$received'
                       , JUMLAH = $jumlah
                       , KETERANGAN = '$keterangan' 
                       , CREATED_BY = '$user_id' 
                       , CREATED_DT = SYSDATE 
                     WHERE SEQ_ID = $kode_barang";
             $r = DB::update($sql);
            return $r;
        }
        public function brg_edit_barang_out($kode_barang,$kd_brg,$received,$jumlah,$keterangan,$user_id){
          $sql = "UPDATE SIAK_BARANG_KELUAR
                       SET 
                       KODE_BARANG = '$kd_brg'
                       , RECEIVED = '$received'
                       , JUMLAH = $jumlah
                       , KETERANGAN = '$keterangan' 
                       , CREATED_BY = '$user_id' 
                       , CREATED_DT = SYSDATE 
                     WHERE SEQ_ID = $kode_barang";
             $r = DB::update($sql);
            return $r;
        }
        public function brg_delete_master_barang($barang_id){
          $sql = "DELETE FROM SIAK_MASTER_BARANG
                     WHERE BARANG_ID = $barang_id";
             $r = DB::delete($sql);
            $sql = "DELETE FROM SIAK_BARANG_MASUK
                     WHERE KODE_BARANG = $barang_id";
             $r = DB::delete($sql);
            $sql = "DELETE FROM SIAK_BARANG_KELUAR
                     WHERE KODE_BARANG = $barang_id";
             $r = DB::delete($sql);
            return $r;
        }

        public function brg_delete_master_in_barang($barang_id){
          $sql = "DELETE FROM SIAK_BARANG_MASUK
                     WHERE SEQ_ID = $barang_id";
             $r = DB::delete($sql);
            return $r;
        }

        public function brg_delete_master_out_barang($barang_id){
          $sql = "DELETE FROM SIAK_BARANG_KELUAR
                     WHERE SEQ_ID = $barang_id";
             $r = DB::delete($sql);
            return $r;
        }

        public function brg_delete_master_aset($barang_id,$sect){
          $sql = "DELETE FROM SIAK_MASTER_ASET
                     WHERE NO = $barang_id AND SECT = $sect";
             $r = DB::delete($sql);
            return $r;
        }
}
