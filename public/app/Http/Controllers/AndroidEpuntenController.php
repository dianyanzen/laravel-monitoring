<?php

namespace App\Http\Controllers;

use App\Pengajuanskts;
use App\Pengajuanwna;
use App\Pengajuandatang;
use App\Notification;
use App\HistPrint;

use Auth;
use DB;
use Session;
use DateTime;
use PDF;
use Validator;
use App\User;
use Jenssegers\Date\Date;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\cache;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Str;
use App\Http\Controllers\SharedController as Shr;
use App\Http\Controllers\AndroidApiController as AndrApi;
use Illuminate\Support\Facades\Log;
use Spatie\Browsershot\Browsershot;
use GuzzleHttp\Client;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class AndroidEpuntenController extends Controller
{

  public $successStatus = 200;


  public function total_user()
  {

    $sql = "SELECT 
             COUNT(1) JUM
            , SUM(CASE WHEN JENIS_KLMIN = 1 THEN 1 ELSE 0 END) LK
            , SUM(CASE WHEN JENIS_KLMIN = 2 THEN 1 ELSE 0 END) PR
            , SUM(CASE WHEN PROC_STAT = 4 THEN 1 ELSE 0 END) AKTIF
            FROM PENGAJUANSKTS";
    $query=DB::connection('webpunten')->select($sql); 
    return response()->json([
      'total' => $query[0]->jum,
      'lk' => $query[0]->lk,
      'pr' => $query[0]->pr,
      'aktv' => $query[0]->aktif
    ]);
  }

    public function cek_berkas(Request $request)
  {
    $nik = $request->nik;
    $sql = "SELECT SUM(c) as JUMLAH
            FROM (
            SELECT COUNT(1) AS c FROM PENGAJUANDATANG where NIK = $nik
            UNION ALL
            SELECT COUNT(1) FROM PENGAJUANSKTS where NIK = $nik
            UNION ALL
            SELECT COUNT(1) FROM PENGAJUANWNA where NIK = $nik
            )";
    $query=DB::connection('webpunten')
    ->select($sql);
    if($query[0]->jumlah > 0){
      $sql = "SELECT nik, nama_lgkp, proc_stat, stat_berkas,null dok_imgr, jenis_layanan, UPDATED_AT, CREATED_AT FROM PENGAJUANDATANG a where a.NIK = $nik
              union all
              SELECT nik, nama_lgkp, proc_stat, stat_berkas,null dok_imgr, jenis_layanan, UPDATED_AT, CREATED_AT FROM PENGAJUANSKTS b where b.NIK = $nik
              union all
              SELECT nik, nama_lgkp, proc_stat, stat_berkas,dok_imgr dok_imgr, jenis_layanan, UPDATED_AT, CREATED_AT FROM PENGAJUANWNA c where c.NIK = $nik 
              ORDER BY CREATED_AT DESC";
      $query=DB::connection('webpunten')->select($sql);
      return response()->json([
        $query
      ]);
    }else{
      return response()->json([
        'status' => 'error',
        'data' => 'NIK Tidak Ada'
      ]);
    }
  }

  public function cek_notif(Request $request)
  {
    $nik = $request->nik;
    $sql = "SELECT COUNT(1) JUMLAH FROM NOTIFICATION WHERE NIK = $nik";
    $query=DB::select($sql);
    if($query[0]->jumlah > 0){
      $sql = "SELECT
      ID
      ,TYPE
      ,PENGAJUAN_ID
      ,JENIS_PENGAJUAN
      ,NIK
      ,FROM_USER_ID
      ,TO_USER_ID
      ,MESSAGE
      ,SEEN_AT
      ,CREATED_AT
      ,UPDATED_AT
      ,DELETED_AT
      ,FLAG_READ
      FROM NOTIFICATION WHERE NIK = $nik ORDER BY CREATED_AT DESC";
      $query=DB::select($sql);
      return response()->json([
        $query
      ]);
    }else{
      return response()->json([
        'status' => 'error',
        'data' => 'NIK Tidak Ada'
      ]);
    }
  }
  public function read_notif(Request $request)
  {
    $id=$request->id;
    $query=DB::select("select count(1) jumlah from NOTIFICATION where ID = ".$id."");

    if($query[0]->jumlah > 0){
      $query=DB::update("UPDATE NOTIFICATION SET FLAG_READ = 1 WHERE ID = ".$id."");
      return response()->json([
        'status' => 'success',
        'data' => 'NOTIFIKASI SUDAH DI BACA'
      ]);
    }else{
      return response()->json([
        'status' => 'error',
        'data' => 'NOTIFIKASI TIDAK ADA'
      ]);
    }
  }
  public function update_pengajuan(Request $request)
  {
    $stat_pngjuan=$request->stat_pngjuan;
    $id_pengajuan=$request->id_pengajuan;
    $jenis_layanan=$request->jenis_layanan;
    if ($jenis_layanan == 1){
      DB::connection('webpunten')->table('pengajuan')
      ->where('id', $id_pengajuan)
      ->update(['proc_stat' => $stat_pngjuan]);
      return response()->json([
        'status' => 'success',
        'data' => 'Pengajuan Berhasil Diupdate'
      ]);
    }else if ($jenis_layanan == 3){
      DB::connection('webpunten')->table('pengajuanwna')
      ->where('id', $id_pengajuan)
      ->update(['proc_stat' => $stat_pngjuan]);
      return response()->json([
        'status' => 'success',
        'data' => 'Pengajuan Berhasil Diupdate'
      ]);
    }

  }

public function hapus_notif(Request $request)
  {
    $id= $request->id;
    $query=DB::delete("DELETE FROM NOTIFICATION WHERE ID = $id");
      return response()->json([
        'status' => '200',
        'data' => 'Berhasil'
      ]);
  }

public function store2(Request $request)
  {
    $nik= $request->nik;
    $jenis_layanan=$request->jenis_layanan;
    $arrayPengajuan = json_decode($request->biodata, true);
    usort($arrayPengajuan, function($a, $b) {
    return $b['no'] <=> $a['no'];
    });
    
    $arrayPengajuan2 = json_decode($request->biodata, true);
    usort($arrayPengajuan, function($a, $b) {
    return $a['stat_hbkel'] <=> $b['stat_hbkel'];
    });


    $is_done = 0;
    if (empty($nik)){
      return response()->json([
          'status' => 'error',
          'data' => 'Nik Tidak Boleh Kosong !'
        ]);
    }
    

    $query=DB::connection('webpunten')
    ->select("SELECT SUM(c) as JUMLAH
            FROM (
            SELECT COUNT(1) AS c FROM PENGAJUANDATANG where NIK = $nik and proc_stat <> 4
            UNION ALL
            SELECT COUNT(1) FROM PENGAJUANSKTS where NIK = $nik and proc_stat <> 4
            UNION ALL
            SELECT COUNT(1) FROM PENGAJUANWNA where NIK = $nik and proc_stat <> 4)");
    if($query[0]->jumlah == 1){
      $is_done = 1;
      return response()->json([
        'status' => 'error',
        'data' => 'NIK sudah pernah mengajukan sebelumnya'
      ]);
    }

    if($is_done == 1){
      return response()->json([
        'status' => 'error',
        'data' => 'NIK sudah pernah mengajukan sebelumnya'
      ]);
    }else{
      if ($jenis_layanan == 1){
        $destinationPath=$this->pengajuan_path().'/pengajuan/skts/'.$nik.'/ori/';
        $destinationPathThumb=$this->pengajuan_path().'/pengajuan/skts/'.$nik.'/thumb/';
        $imgName = 'pengajuan/skts/'.$nik.'/ori/';
        $imgThumbName = 'pengajuan/skts/'.$nik.'/thumb/';
      }else if($jenis_layanan == 2){
        $destinationPath=$this->pengajuan_path().'/pengajuan/datang/'.$nik.'/ori/';
        $destinationPathThumb=$this->pengajuan_path().'/pengajuan/datang/'.$nik.'/thumb/';
        $imgName = 'pengajuan/datang/'.$nik.'/ori/';
        $imgThumbName = 'pengajuan/datang/'.$nik.'/thumb/';
      }else if($jenis_layanan == 3){
        $destinationPath=$this->pengajuan_path().'/pengajuan/wna/'.$nik.'/ori/';
        $destinationPathThumb=$this->pengajuan_path().'/pengajuan/wna/'.$nik.'/thumb/';
        $imgName = 'pengajuan/wna/'.$nik.'/ori/';
        $imgThumbName = 'pengajuan/wna/'.$nik.'/thumb/';
      }else{
        return response()->json([
          'status' => 'error',
          'data' => 'Jenis Pengajuan Salah Mohon Cek Kemabali'
        ]);
      }


      $picname = "imgupload_thumb_1.jpg";
      if (!file_exists($destinationPath)) {
        mkdir($destinationPath, 0777, true);
      }
      if (!file_exists($destinationPathThumb)) {
        mkdir($destinationPathThumb, 0777, true);
      }

      if ($jenis_layanan == 1){
        $pengajuan = new Pengajuanskts;
      }else if($jenis_layanan == 2){
        $pengajuan = new Pengajuandatang;
      }else if($jenis_layanan == 3){
        $pengajuan = new Pengajuanwna;
      }else{
        return response()->json([
          'status' => 'error',
          'data' => 'Jenis Pengajuan Salah Mohon Cek Kemabali'
        ]);
      }




        for($i=0; $i < count($arrayPengajuan); $i++) {
          // echo $arrayPengajuan[$i]['nik'];
          // die;
          $pengajuanDetail = new Pengajuandatang;
          $pengajuanDetail->no_kk = $request->no_kk;
          $pengajuanDetail->proc_stat = 1;
          $pengajuanDetail->nik = $arrayPengajuan[$i]['nik'];
          $pengajuanDetail->nama_lgkp = $arrayPengajuan[$i]['nama_lgkp'];
          $pengajuanDetail->jenis_klmin = $arrayPengajuan[$i]['jenis_klmin'];
          $pengajuanDetail->tmpt_lhr = $arrayPengajuan[$i]['tmpt_lhr'];
          $pengajuanDetail->tgl_lhr = $arrayPengajuan[$i]['tgl_lhr'];
          $pengajuanDetail->agama = $arrayPengajuan[$i]['agama'];
          $pengajuanDetail->stat_kwn = $arrayPengajuan[$i]['stat_kwn'];
          $pengajuanDetail->gol_drh = $arrayPengajuan[$i]['gol_drh'];
          $pengajuanDetail->pendidikan = $arrayPengajuan[$i]['pendidikan'];
          $pengajuanDetail->pekerjaan = $arrayPengajuan[$i]['pekerjaan'];
          $pengajuanDetail->prop = $request->prop;
          $pengajuanDetail->kab = $request->kab;
          $pengajuanDetail->src_prov = $request->src_prov;
          $pengajuanDetail->src_kab =$request->src_kab;
          $pengajuanDetail->src_kec = $request->src_kec;
          $pengajuanDetail->src_kel = $request->src_kel;
          $pengajuanDetail->src_alamat = $request->src_alamat;
          $pengajuanDetail->src_rt = $request->src_rt;
          $pengajuanDetail->src_rw = $request->src_rw;
          $pengajuanDetail->kec = $request->kec;
          $pengajuanDetail->kel = $request->kel;
          $pengajuanDetail->alamat = $request->alamat;
          $pengajuanDetail->no_rw = $request->no_rw;
          $pengajuanDetail->no_rt = $request->no_rt;

          $pengajuanDetail->stat_hbkel = $arrayPengajuan[$i]['stat_hbkel'];
          $pengajuanDetail->telepon = $arrayPengajuan[$i]['telepon'];
          $pengajuanDetail->email = $arrayPengajuan[$i]['email'];
          $pengajuanDetail->jenis_layanan = $request->jenis_layanan = 2;
          // $pengajuanDetail->alasan_pindah = $arrayPengajuan[$i]['alasan_pindah'];
          // // $pengajuanDetail->jangka_waktu = $arrayPengajuan[$i]['jangka_waktu'];
          // // $pengajuanDetail->jum_anggota = $arrayPengajuan[$i]['jum_anggota'];
          // // $pengajuanDetail->tgl_datang = $arrayPengajuan[$i]['tgl_datang'];
          $pengajuanDetail->akta_lhr=strtoupper($arrayPengajuan[$i]['akta_lhr']);
          $pengajuanDetail->no_akta_lhr=strtoupper($arrayPengajuan[$i]['no_akta_lhr']);

          $pengajuanDetail->no_skpwni=strtoupper($request->skpwni);
          $pengajuanDetail->status = $request->status;
          $pengajuanDetail->jenis_layanan = $request->jenis_layanan;
          $pengajuanDetail->akta_lhr = $arrayPengajuan[$i]['akta_lhr'];
          $pengajuanDetail->no_akta_lhr = $arrayPengajuan[$i]['no_akta_lhr'];
          $pengajuanDetail->no_skpwni = $request->no_skpwni;
          $pengajuanDetail->tgl_berlaku= Carbon::now()->addMonths(6);
          //echo $pengajuanDetail;
          $pengajuanDetail->alasan_pindah=strtoupper($request->alasan_pindah);
          $pengajuanDetail->jum_anggota=strtoupper($request->jum_anggota);
          $pengajuanDetail->save();
        }


if ($jenis_layanan == 1){
        $destinationPath=$this->pengajuan_path().'/pengajuan/skts/'.$nik.'/ori/';
        $destinationPathThumb=$this->pengajuan_path().'/pengajuan/skts/'.$nik.'/thumb/';
        $imgName = 'pengajuan/skts/'.$nik.'/ori/';
        $imgThumbName = 'pengajuan/skts/'.$nik.'/thumb/';
      }else if($jenis_layanan == 2){
        $destinationPath=$this->pengajuan_path().'/pengajuan/datang/'.$nik.'/ori/';
        $destinationPathThumb=$this->pengajuan_path().'/pengajuan/datang/'.$nik.'/thumb/';
        $imgName = 'pengajuan/datang/'.$nik.'/ori/';
        $imgThumbName = 'pengajuan/datang/'.$nik.'/thumb/';
      }else if($jenis_layanan == 3){
        $destinationPath=$this->pengajuan_path().'/pengajuan/wna/'.$nik.'/ori/';
        $destinationPathThumb=$this->pengajuan_path().'/pengajuan/wna/'.$nik.'/thumb/';
        $imgName = 'pengajuan/wna/'.$nik.'/ori/';
        $imgThumbName = 'pengajuan/wna/'.$nik.'/thumb/';
      }else{
        return response()->json([
          'status' => 'error',
          'data' => 'Jenis Pengajuan Salah Mohon Cek Kemabali'
        ]);
      }


        if (!file_exists($destinationPath)) {
          mkdir($destinationPath, 0777, true);
        }
        if (!file_exists($destinationPathThumb)) {
          mkdir($destinationPathThumb, 0777, true);
        }
       

        $biodata1 = "";
        $biodata2 = "";
        $biodata3 = "";
        $biodata4 = "";
        $biodata5 = "";
        for($i=0; $i < count($arrayPengajuan2); $i++) {
          $no = $i + 1 + count($arrayPengajuan2);
          $tglLhr = '';
          if($arrayPengajuan2[$i]['tgl_lhr']){
            $tglLhr = Carbon::createFromFormat('Y-m-d H:i:s', $arrayPengajuan2[$i]['tgl_lhr'])->format('d-m-Y');
          }

          // $tglAkhPaspor = '';
          // if($arrayPengajuan2[$i]['tgl_akh_paspor']){
          //   $tglAkhPaspor = Carbon::createFromFormat('Y-m-d H:i:s', $arrayPengajuan2[$i]['tgl_akh_paspor'])->format('d-m-Y');
          // }

          $tglKwn =$arrayPengajuan2[$i]['tgl_kwn'];
          // if($arrayPengajuan2[$i]['tgl_kwn']){
          //   $tglKwn = Carbon::createFromFormat('Y-m-d H:i:s', $arrayPengajuan2[$i]['tgl_kwn'])->format('d-m-Y');
          // }

          $tglCrai= $arrayPengajuan2[$i]['tgl_cerai'];
          // if($arrayPengajuan2[$i]['tgl_cerai']){
          //   $tglCrai = Carbon::createFromFormat('Y-m-d H:i:s', $arrayPengajuan2[$i]['tgl_cerai'])->format('d-m-Y');
          // }

          // $tglTrbtItap= '';
          // if($arrayPengajuan2[$i]['tgl_trbt_itas_itap']){
          //   $tglTrbtItap = Carbon::createFromFormat('Y-m-d H:i:s', $arrayPengajuan2[$i]['tgl_trbt_itas_itap'])->format('d-m-Y');
          // }
          // $tglAkhItap= '';
          // if($arrayPengajuan2[$i]['tgl_akh_itas_itap']){
          //   $tglAkhItap = Carbon::createFromFormat('Y-m-d H:i:s', $arrayPengajuan2[$i]['tgl_akh_itas_itap'])->format('d-m-Y');
          // }
          // $tglDtg= '';
          // if($arrayPengajuan2[$i]['tgl_dtg_pertma']){
          //   $tglDtg = Carbon::createFromFormat('Y-m-d H:i:s', $arrayPengajuan2[$i]['tgl_dtg_pertma'])->format('d-m-Y');
          // }
          $biodata1 = $biodata1.
          '<tr>
          <td width="5%" class="center">'.$no.'</td>
          <td>'.$arrayPengajuan2[$i]['nik'].'</td>
          <td>'.$arrayPengajuan2[$i]['nama_lgkp'].'</td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          </tr>';
          $biodata2 = $biodata2.
          '<tr>
          <td width="5%" class="center">'.$no.'</td>
          <td></td>
          <td></td>
          <td>'.$this->get_detail_wni(801, $arrayPengajuan2[$i]['jenis_klmin']).'</td>
          <td>'.$arrayPengajuan2[$i]['tmpt_lhr'].'</td>
          <td>'.$tglLhr.'</td>
          <td></td>
          <td></td>
          <td>'.$arrayPengajuan2[$i]['no_akta_lhr'].'</td>
          </tr>';
          $biodata3 = $biodata3.
          '<tr>
          <td width="5%" class="center">'.$no.'</td>
          <td>'.$this->get_detail_wni(401, $arrayPengajuan2[$i]['gol_drh']).'</td>
          <td>'.$this->get_detail_wni(501, $arrayPengajuan2[$i]['agama']).'</td>
          <td></td>
          <td>'.$this->get_detail_wni(601, $arrayPengajuan2[$i]['stat_kwn']).'</td>
          <td>'.$arrayPengajuan2[$i]['no_akta_kwn'].'</td>
          <td>'.$arrayPengajuan2[$i]['tgl_kwn'].'</td>
          </tr>';
          $biodata4 = $biodata4.
          '<tr>
          <td width="5%" class="center">'.$no.'</td>
          <td>'.$arrayPengajuan2[$i]['no_akta_crai'].'</td>
          <td>'.$arrayPengajuan2[$i]['tgl_cerai'].'</td>
          <td>'.$this->get_detail_wni(301, $arrayPengajuan2[$i]['stat_hbkel']).'</td>
          <td></td>
          <td></td>
          <td>'.$this->get_detail_wni(101, $arrayPengajuan2[$i]['pendidikan']).'</td>
          <td>'.$this->get_detail_wni(201, $arrayPengajuan2[$i]['pekerjaan']).'</td>
          <td></td>
          <td></td>
          </tr>';
          $biodata5 = $biodata5.
          '<tr>
          <td width="5%" class="center">'.$no.'</td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td>'.$arrayPengajuan2[$i]['nik_ibu'].'</td>
          <td>'.$arrayPengajuan2[$i]['nama_lgkp_ibu'].'</td>
          <td>'.$arrayPengajuan2[$i]['nik_ayah'].'</td>
          <td>'.$arrayPengajuan2[$i]['nama_lgkp_ayah'].'</td>
          </tr>';
        }
        $sisaRow = 10 - (count($arrayPengajuan2) + count($arrayPengajuan2));
        $rowKosong1 = "";
        $rowKosong2 = "";
        $rowKosong3 = "";
        $rowKosong4 = "";
        $rowKosong5 = "";
        for($i=0; $i < $sisaRow; $i++) {
          $no = count($arrayPengajuan2) + count($arrayPengajuan2) + $i + 1;
          $rowKosong1 = $rowKosong1.
          '<tr>
          <td width="5%" class="center">'.$no.'</td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          </tr>';
          $rowKosong2 = $rowKosong2.
          '<tr>
          <td width="5%" class="center">'.$no.'</td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          </tr>';
          $rowKosong3 = $rowKosong3.
          '<tr>
          <td width="5%" class="center">'.$no.'</td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          </tr>';
          $rowKosong4 = $rowKosong4.
          '<tr>
          <td width="5%" class="center">'.$no.'</td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          </tr>';
          $rowKosong5 = $rowKosong5.
          '<tr>
          <td width="5%" class="center">'.$no.'</td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          </tr>';
        }
        $html = '<!DOCTYPE html>
        <html>
        <head>
        <style>
        @media all {
          .page-break { display: none; }
        }
        @media print {
          .page-break { display: block; page-break-before: always; }
        }
        *{ font-family: Arial, Helvetica, sans-serif; font-size:12px; }
        .my-container { position: relative; overflow: hidden; border: 1px solid; left: 0;
        right: 0;
        top: 0;
        bottom: 0;}
        .my-container:before {
          content: " ";
          display: block;
          position: absolute;
          width: 100%;
          height: 100%;
          z-index: 1;
          opacity: 0.1;
          background-image: url(\'https://disdukcapil.bandung.go.id/api/public/assets/logo-pemkot.svg\');
          background-repeat: no-repeat;
          background-position: center;
          background-size: 350px;
          border: 5px solid red;
        }
        .main-topic { display: flex; }
        .left-picture > img{ display: block; }
        hr.solid { border-top: 3px solid; }
        .center {text-align: center;}
        #page-border {
          position: absolute;
          overflow: auto;
          left: 0;
          right: 0;
          top: 0;
          bottom: 0;
          border: 5px solid red;
        }
        </style>
        </head>
        <body>
        <div class="my-container">
        <table width="100%">
        <tbody>
        <tr>
        <td></td>
        <td style="text-align: center; border: 1px solid;" width="20%">F-1.01</td>
        </tr>
        <tr>
        <td colspan="3"></td>
        </tr>
        </tbody>
        </table>
        <strong style="font-size: 20px">FORMULIR BIODATA KELUARGA<br></strong>
        <table width="100%">
        <tbody>
        <tr>
        <td width="50%">
        <table width="100%">
        <tbody>
        <tr>
        <td>
        <tr>
        <td width="30%">NO KK</td>
        <td>:</td>
        <td>'.$request->no_kk.'</td>
        </tr>
        <tr>
        <td>Nama Kepala Keluarga</td>
        <td>:</td>
        <td>'.$request->nama_lgkp.'</td>
        </tr>
        <tr>
        <td>NIK</td>
        <td>:</td>
        <td>'.$request->nik.'</td>
        </tr>
        </td>
        <td></td>
        </tr>
        </tbody>
        </table>
        </td>
        <td width="50%">
        <table width="100%">
        <tbody>
        <tr>
        <td>
        <tr>
        <td>Alamat</td>
        <td>:</td>
        <td>'.$request->alamat.' Kode Pos: </td>
        </tr>
        <tr>
        <td>Telepon</td>
        <td>:</td>
        <td>'.$request->telepon.'</td>
        </tr>
        <tr>
        <td>Email</td>
        <td>:</td>
        <td>'.$request->email.'</td>
        </tr>
        </td>
        </tr>
        </tbody>
        </table>
        </td>
        </tr>
        </tbody>
        </table>
        <strong>DATA ANGGOTA KELUARGA</strong>
        <table width="100%" style="border-collapse: collapse;">
        <tbody>
        <tr style="background-color: darkgray;">
        <th class="border" width="1%">No.</th>
        <th class="border" width="8%">NIK</th>
        <th class="border">Nama Lengkap</th>
        <th class="border" colspan="2">Gelar</th>
        <th class="border">Nomor Paspor</th>
        <th class="border" width="6%">Tanggal Berakhir Paspor</th>
        <th class="border">Nama Sponsor</th>
        </tr>
        '.$biodata1.'
        '.$rowKosong1.'
        </tbody>
        </table>
        <table width="100%" style="border-collapse: collapse;">
        <tbody>
        <tr style="background-color: darkgray;">
        <th class="border" width="1%">No.</th>
        <th class="border">Tipe Sponsor</th>
        <th class="border">Alamat Sponsor</th>
        <th class="border" width="8%">Jenis Kelamin</th>
        <th class="border">Tempat Lahir</th>
        <th class="border" width="6%">Tanggal Lahir</th>
        <th class="border">Kewarganegaraan</th>
        <th class="border">No. SK Penetapan WNI</th>
        <th class="border" width="13%">Nomor Akta Kelahiran</th>
        </tr>
        '.$biodata2.'
        '.$rowKosong2.'
        </tbody>
        </table>
        </div>
        <div class="page-break"></div>
        <br>
        <table width="100%" style="border-collapse: collapse;">
        <tbody>
        <tr style="background-color: darkgray;">
        <th class="border" width="1%">No.</th>
        <th class="border" width="7%">Gol. Darah</th>
        <th class="border">Agama</th>
        <th class="border">Nama Organisasi<br>Kepercayaan terhadap Tuhan YME</th>
        <th class="border">Status Perkawinan</th>
        <th class="border">Nomor Akta Perkawinan</th>
        <th class="border" width="6%">Tanggal Perkawinan</th>
        </tr>
        '.$biodata3.'
        '.$rowKosong3.'
        </tbody>
        </table>
        <table width="100%" style="border-collapse: collapse;">
        <tbody>
        <tr style="background-color: darkgray;">
        <th class="border" width="1%">No.</th>
        <th class="border">Nomor Akta Cerai</th>
        <th class="border" width="6%">Tanggal Perceraian</th>
        <th class="border">Status Hubungan Dalam Keluarga</th>
        <th class="border">Kelainan Fisik</th>
        <th class="border">Penyandang Cacat</th>
        <th class="border">Pendidikan Terakhir</th>
        <th class="border">Jenis Pekerjaan</th>
        <th class="border">Nomor ITAS/ITAP</th>
        <th class="border">Tempat Terbit ITAS/ITAP</th>
        '.$biodata4.'
        '.$rowKosong4.'
        </tbody>
        </table>
        <table width="100%" style="border-collapse: collapse;">
        <tbody>
        <tr style="background-color: darkgray;">
        <th class="border" width="1%">No.</th>
        <th class="border" width="6%">Tanggal Terbit ITAS/ITAP</th>
        <th class="border" width="6%">Tanggal Akhir ITAS/ITAP</th>
        <th class="border">Tempat Datang Pertama</th>
        <th class="border" width="6%">Tanggal Kedatangan Pertama</th>
        <th class="border" width="5%">NIK Ibu</th>
        <th class="border">Nama Ibu</th>
        <th class="border" width="5%">NIK Ayah</th>
        <th class="border">Nama Ayah</th>
        </tr>
        '.$biodata5.'
        '.$rowKosong5.'
        </tbody>
        </table>
        <div style="text-align: left;"><strong style="font-size: 10px" >FORMULIR ELEKTRONIK INI ADALAH DOKUMEN YANG DIPERSAMAKAN DENGAN FORMULIR NON ELEKTRONIK DAN MERUPAKAN BUKTI PENGAJUAN YANG SAH DARI PEMOHON TANPA DIBUBUHI TANDA TANGAN.</strong></div>
        <table width="100%">
        <tbody>
        <tr>
        <td></td>
        <td style="text-align: center;" width="40%"><br><br><br>Bandung, '.Carbon::now()->format('d-m-Y').'<br>
        Pelapor<br><br>('.$request->nama_lgkp.')
        </td>
        </tr>
        <tr>
        <td colspan="3"></td>
        </tr>
        </tbody>
        </table>
        </br></br></br></br></br></br></br>
        </body>
        </html>
        ';
        $tgl = Carbon::now()->format('d-m-Y-His');
        $url = $destinationPath.'formulir-'.$tgl.'.pdf';
        Browsershot::html($html)
        ->fullPage()
        ->landscape()
        ->showBackground()
        ->waitUntilNetworkIdle()
        ->format('A4')
        ->save($url);

      
        
        $pengajuanDetail->img_15=$imgName."formulir-".$tgl.".pdf";



        if($request->hasFile('imgupload1')){
        $photo=$request->file('imgupload1')->getClientOriginalName();
        $extension = $request->file('imgupload1')->getClientOriginalExtension();
        $filename = "imgupload1.".$extension;
        $request->file('imgupload1')->move($destinationPath,$filename);
        $pengajuanDetail->img_1=$imgName.$filename;

        $img1 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_1.jpg";
        $img1->save($destinationPathThumb.$filename);
        $pengajuanDetail->img_thumb_1=$imgThumbName.$filename;
      }

      /////////////////
      if($request->hasFile('imgupload2')){
        $photo=$request->file('imgupload2')->getClientOriginalName();
        $extension = $request->file('imgupload2')->getClientOriginalExtension();
        $filename = "imgupload2.".$extension;
        $request->file('imgupload2')->move($destinationPath,$filename);
        $pengajuanDetail->img_2=$imgName.$filename;

        $img2 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_2.jpg";
        $img2->save($destinationPathThumb.$filename);
        $pengajuanDetail->img_thumb_2=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload3')){
        $photo=$request->file('imgupload3')->getClientOriginalName();
        $extension = $request->file('imgupload3')->getClientOriginalExtension();
        $filename = "imgupload3.".$extension;
        $request->file('imgupload3')->move($destinationPath,$filename);
        $pengajuanDetail->img_3=$imgName.$filename;

        $img3 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_3.jpg";
        $img3->save($destinationPathThumb.$filename);
        $pengajuanDetail->img_thumb_3=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload4')){
        $photo=$request->file('imgupload4')->getClientOriginalName();
        $extension = $request->file('imgupload4')->getClientOriginalExtension();
        $filename = "imgupload4.".$extension;
        $request->file('imgupload4')->move($destinationPath,$filename);
        $pengajuanDetail->img_4=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_4.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuanDetail->img_thumb_4=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload5')){
        $photo=$request->file('imgupload5')->getClientOriginalName();
        $extension = $request->file('imgupload5')->getClientOriginalExtension();
        $filename = "imgupload5.".$extension;
        $request->file('imgupload5')->move($destinationPath,$filename);
        $pengajuanDetail->img_5=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_5.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuanDetail->img_thumb_5=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload6')){
        $photo=$request->file('imgupload6')->getClientOriginalName();
        $extension = $request->file('imgupload6')->getClientOriginalExtension();
        $filename = "imgupload6.".$extension;
        $request->file('imgupload6')->move($destinationPath,$filename);
        $pengajuanDetail->img_6=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_6.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuanDetail->img_thumb_6=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload7')){
        $photo=$request->file('imgupload7')->getClientOriginalName();
        $extension = $request->file('imgupload7')->getClientOriginalExtension();
        $filename = "imgupload7.".$extension;
        $request->file('imgupload7')->move($destinationPath,$filename);
        $pengajuanDetail->img_7=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_7.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuanDetail->img_thumb_7=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload8')){
        $photo=$request->file('imgupload8')->getClientOriginalName();
        $extension = $request->file('imgupload8')->getClientOriginalExtension();
        $filename = "imgupload8.".$extension;
        $request->file('imgupload8')->move($destinationPath,$filename);
        $pengajuanDetail->img_8=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_8.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuanDetail->img_thumb_8=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload9')){
        $photo=$request->file('imgupload9')->getClientOriginalName();
        $extension = $request->file('imgupload9')->getClientOriginalExtension();
        $filename = "imgupload9.".$extension;
        $request->file('imgupload9')->move($destinationPath,$filename);
        $pengajuanDetail->img_9=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_9.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuanDetail->img_thumb_9=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload10')){
        $photo=$request->file('imgupload10')->getClientOriginalName();
        $extension = $request->file('imgupload10')->getClientOriginalExtension();
        $filename = "imgupload10.".$extension;
        $request->file('imgupload10')->move($destinationPath,$filename);
        $pengajuanDetail->img_10=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_10.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuanDetail->img_thumb_10=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload11')){
        $photo=$request->file('imgupload11')->getClientOriginalName();
        $extension = $request->file('imgupload11')->getClientOriginalExtension();
        $filename = "imgupload11.".$extension;
        $request->file('imgupload11')->move($destinationPath,$filename);
        $pengajuanDetail->img_11=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_11.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuanDetail->img_thumb_11=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload12')){
        $photo=$request->file('imgupload12')->getClientOriginalName();
        $extension = $request->file('imgupload12')->getClientOriginalExtension();
        $filename = "imgupload12.".$extension;
        $request->file('imgupload12')->move($destinationPath,$filename);
        $pengajuanDetail->img_12=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_12.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuanDetail->img_thumb_12=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload13')){
        $photo=$request->file('imgupload13')->getClientOriginalName();
        $extension = $request->file('imgupload13')->getClientOriginalExtension();
        $filename = "imgupload13.".$extension;
        $request->file('imgupload13')->move($destinationPath,$filename);
        $pengajuanDetail->img_13=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_13.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuanDetail->img_thumb_13=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload14')){
        $photo=$request->file('imgupload14')->getClientOriginalName();
        $extension = $request->file('imgupload14')->getClientOriginalExtension();
        $filename = "imgupload14.".$extension;
        $request->file('imgupload14')->move($destinationPath,$filename);
        $pengajuanDetail->img_14=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_14.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuanDetail->img_thumb_14=$imgThumbName.$filename;
      }


      $pengajuanDetail->save();
 

      $msg = 'Pengajuan anda masih dalam tahap menunggu validasi';
      $ntf = 'Pemberitahuan';
      $jenis = 'PENGAJUAN';
      $pengajuan_id = null;
      $from_user_id = 'DISDUKCAPIL KOTA BANDUNG';
      AndrApi::sendnotification($jenis,$pengajuan_id,$jenis_layanan,$nik,$msg,$ntf,$from_user_id);
      return response()->json([
        'status' => 'success',
        'data' => 'Pendaftaran berhasil'
      ]);
    }
  }


  public function store3(Request $request)
  {
    $nik= $request->nik;
    $jumlah= $request->jum_anggota;
    $jenis_layanan=$request->jenis_layanan;
    if ($jumlah > 0) {
    $arrayPengajuan = json_decode($request->biodata, true);
    usort($arrayPengajuan, function($a, $b) {
    return $b['no'] <=> $a['no'];
    });
    
    $arrayPengajuan2 = json_decode($request->biodata, true);
    usort($arrayPengajuan, function($a, $b) {
    return $a['stat_hbkel'] <=> $b['stat_hbkel'];
});
  }


    $is_done = 0;
    if (empty($nik)){
      return response()->json([
          'status' => 'error',
          'data' => 'Nik Tidak Boleh Kosong !'
        ]);
    }
    

    $query=DB::connection('webpunten')
    ->select("SELECT SUM(c) as JUMLAH
            FROM (
            SELECT COUNT(1) AS c FROM PENGAJUANDATANG where NIK = $nik and proc_stat <> 4
            UNION ALL
            SELECT COUNT(1) FROM PENGAJUANSKTS where NIK = $nik and proc_stat <> 4
            UNION ALL
            SELECT COUNT(1) FROM PENGAJUANWNA where NIK = $nik and proc_stat <> 4)");
    if($query[0]->jumlah == 1){
      $is_done = 1;
      return response()->json([
        'status' => 'error',
        'data' => 'NIK sudah pernah mengajukan sebelumnya'
      ]);
    }

    if($is_done == 1){
      return response()->json([
        'status' => 'error',
        'data' => 'NIK sudah pernah mengajukan sebelumnya'
      ]);
    }else{
      if ($jenis_layanan == 1){
        $destinationPath=$this->pengajuan_path().'/pengajuan/skts/'.$nik.'/ori/';
        $destinationPathThumb=$this->pengajuan_path().'/pengajuan/skts/'.$nik.'/thumb/';
        $imgName = 'pengajuan/skts/'.$nik.'/ori/';
        $imgThumbName = 'pengajuan/skts/'.$nik.'/thumb/';
      }else if($jenis_layanan == 2){
        $destinationPath=$this->pengajuan_path().'/pengajuan/datang/'.$nik.'/ori/';
        $destinationPathThumb=$this->pengajuan_path().'/pengajuan/datang/'.$nik.'/thumb/';
        $imgName = 'pengajuan/datang/'.$nik.'/ori/';
        $imgThumbName = 'pengajuan/datang/'.$nik.'/thumb/';
      }else if($jenis_layanan == 3){
        $destinationPath=$this->pengajuan_path().'/pengajuan/wna/'.$nik.'/ori/';
        $destinationPathThumb=$this->pengajuan_path().'/pengajuan/wna/'.$nik.'/thumb/';
        $imgName = 'pengajuan/wna/'.$nik.'/ori/';
        $imgThumbName = 'pengajuan/wna/'.$nik.'/thumb/';
      }else{
        return response()->json([
          'status' => 'error',
          'data' => 'Jenis Pengajuan Salah Mohon Cek Kemabali'
        ]);
      }


      $picname = "imgupload_thumb_1.jpg";
      if (!file_exists($destinationPath)) {
        mkdir($destinationPath, 0777, true);
      }
      if (!file_exists($destinationPathThumb)) {
        mkdir($destinationPathThumb, 0777, true);
      }

      if ($jenis_layanan == 1){
        $pengajuan = new Pengajuanskts;
      }else if($jenis_layanan == 2){
        $pengajuan = new Pengajuandatang;
      }else if($jenis_layanan == 3){
        $pengajuan = new Pengajuanwna;
      }else{
        return response()->json([
          'status' => 'error',
          'data' => 'Jenis Pengajuan Salah Mohon Cek Kemabali'
        ]);
      }


      // $pengajuan->proc_stat=1;
      // $pengajuan->nik=$nik;
      // $pengajuan->email=$request->email;
      // $pengajuan->telepon=$request->telepon;
      // $pengajuan->nama_lgkp=strtoupper($request->nama_lgkp);
      // $pengajuan->jenis_klmin=strtoupper($request->jenis_klmin);
      // $pengajuan->tmpt_lhr=strtoupper($request->tmpt_lhr);
      // $pengajuan->tgl_lhr=strtoupper($request->tgl_lhr);
      // $pengajuan->agama=strtoupper($request->agama);
      // $pengajuan->stat_kwn=strtoupper($request->stat_kwn);
      // $pengajuan->gol_drh=strtoupper($request->gol_drh);
      // $pengajuan->pendidikan=strtoupper($request->pendidikan);
      // $pengajuan->pekerjaan=strtoupper($request->pekerjaan);
      // $pengajuan->src_prov=strtoupper($request->src_prov);
      // $pengajuan->src_kab=strtoupper($request->src_kab);
      // $pengajuan->src_kec=strtoupper($request->src_kec);
      // $pengajuan->src_kel=strtoupper($request->src_kel);
      // $pengajuan->src_alamat=strtoupper($request->src_alamat);
      // $pengajuan->src_rt=strtoupper($request->src_rt);
      // $pengajuan->src_rw=strtoupper($request->src_rw);
      // $pengajuan->prop=strtoupper($request->prop);
      // $pengajuan->kab=strtoupper($request->kab);
      // $pengajuan->kec=strtoupper($request->kec);
      // $pengajuan->kel=strtoupper($request->kel);
      // $pengajuan->alamat=strtoupper($request->alamat);
      // $pengajuan->no_rw=strtoupper($request->no_rw);
      // $pengajuan->no_rt=strtoupper($request->no_rt);
      // $pengajuan->no_kk=strtoupper($request->no_kk);
      // $pengajuan->stat_hbkel=strtoupper($request->stat_hbkel);
      // $pengajuan->alasan_pindah=strtoupper($request->alasan_pindah);
      // $pengajuan->jangka_waktu=strtoupper($request->jangka_waktu);
      // $pengajuan->jum_anggota=strtoupper($request->jum_anggota);
      // $pengajuan->tgl_datang=strtoupper($request->tgl_datang);
      // $pengajuan->status=strtoupper($request->status);
      // $pengajuan->jenis_layanan=strtoupper($request->jenis_layanan);
      // if ($request->jenis_layanan == 2){
      //   $pengajuan->akta_lhr=strtoupper($request->akta_lhr);
      //   $pengajuan->no_akta_lhr=strtoupper($request->no_akta_lhr);
      //   $pengajuan->no_skpwni=strtoupper($request->no_skpwni);
      // }
      // if ($request->jenis_layanan == 3){
      //   $pengajuan->kwrngrn=strtoupper($request->kwrngrn);
      //   $pengajuan->nama_fam=strtoupper($request->nama_fam);
      //   $pengajuan->tipe_spsor=strtoupper($request->tipe_spsor);
      //   $pengajuan->nama_spsor=strtoupper($request->nama_spsor);
      //   $pengajuan->no_paspor=strtoupper($request->no_paspor);
      //   $pengajuan->tgl_paspor=strtoupper($request->tgl_paspor);
      //   $pengajuan->tgl_akh_paspor=strtoupper($request->tgl_akh_paspor);
      //   $pengajuan->no_dok=strtoupper($request->no_doc);
      //   $pengajuan->dok_imgr=strtoupper($request->doc_imigrasi);
      //   $pengajuan->tgl_doc_imigrasi=strtoupper($request->tgl_doc_imigrasi);
      //   $pengajuan->tgl_stay=strtoupper($request->tgl_stay);
      //   $pengajuan->tgl_permit=strtoupper($request->tgl_permit);
      //   $pengajuan->tgl_akh_doc_imgr=strtoupper($request->tgl_akh_doc_imgr);
      // }

      // $pengajuan->tgl_berlaku= Carbon::now()->addMonths(6);


      if ($jumlah > 0 && $jumlah != null) {

        for($i=0; $i < count($arrayPengajuan); $i++) {
          // echo $arrayPengajuan[$i]['nik'];
          // die;
      $pengajuan->proc_stat=1;
      $pengajuan->nik=$nik;
      $pengajuan->email=$request->email;
      $pengajuan->telepon=$request->telepon;
      $pengajuan->nama_lgkp=strtoupper($request->nama_lgkp);
      $pengajuan->jenis_klmin=strtoupper($request->jenis_klmin);
      $pengajuan->tmpt_lhr=strtoupper($request->tmpt_lhr);
      $pengajuan->tgl_lhr=strtoupper($request->tgl_lhr);
      $pengajuan->agama=strtoupper($request->agama);
      $pengajuan->stat_kwn=strtoupper($request->stat_kwn);
      $pengajuan->gol_drh=strtoupper($request->gol_drh);
      $pengajuan->pendidikan=strtoupper($request->pendidikan);
      $pengajuan->pekerjaan=strtoupper($request->pekerjaan);
      $pengajuan->src_prov=strtoupper($request->src_prov);
      $pengajuan->src_kab=strtoupper($request->src_kab);
      $pengajuan->src_kec=strtoupper($request->src_kec);
      $pengajuan->src_kel=strtoupper($request->src_kel);
      $pengajuan->src_alamat=strtoupper($request->src_alamat);
      $pengajuan->src_rt=strtoupper($request->src_rt);
      $pengajuan->src_rw=strtoupper($request->src_rw);
      $pengajuan->prop=strtoupper($request->prop);
      $pengajuan->kab=strtoupper($request->kab);
      $pengajuan->kec=strtoupper($request->kec);
      $pengajuan->kel=strtoupper($request->kel);
      $pengajuan->alamat=strtoupper($request->alamat);
      $pengajuan->no_rw=strtoupper($request->no_rw);
      $pengajuan->no_rt=strtoupper($request->no_rt);
      $pengajuan->no_kk=strtoupper($request->no_kk);
      $pengajuan->stat_hbkel=strtoupper($request->stat_hbkel);
      $pengajuan->alasan_pindah=strtoupper($request->alasan_pindah);
      $pengajuan->jangka_waktu=strtoupper($request->jangka_waktu);
      $pengajuan->jum_anggota=strtoupper($request->jum_anggota);
      $pengajuan->tgl_datang=strtoupper($request->tgl_datang);
      $pengajuan->status=strtoupper($request->status);
      $pengajuan->jenis_layanan=strtoupper($request->jenis_layanan);
      if ($request->jenis_layanan == 3){
        $pengajuan->kwrngrn=strtoupper($request->kwrngrn);
        $pengajuan->nama_fam=strtoupper($request->nama_fam);
        $pengajuan->tipe_spsor=strtoupper($request->tipe_spsor);
        $pengajuan->nama_spsor=strtoupper($request->nama_spsor);
        $pengajuan->no_paspor=strtoupper($request->no_paspor);
        $pengajuan->tgl_paspor=strtoupper($request->tgl_paspor);
        $pengajuan->tgl_akh_paspor=strtoupper($request->tgl_akh_paspor);
        $pengajuan->no_dok=strtoupper($request->no_dok);
        $pengajuan->dok_imgr=strtoupper($request->doc_imigrasi);
        $pengajuan->tgl_doc_imigrasi=strtoupper($request->tgl_doc_imigrasi);
        $pengajuan->tgl_stay=strtoupper($request->tgl_stay);
        $pengajuan->tgl_permit=strtoupper($request->tgl_permit);
        $pengajuan->tgl_akh_doc_imgr=strtoupper($request->tgl_akh_doc_imgr);
      }

      $pengajuan->tgl_berlaku= Carbon::now()->addMonths(6);


          $pengajuanDetail = new Pengajuanwna;
          $pengajuanDetail->no_kk = $request->no_kk;
          $pengajuanDetail->proc_stat = 1;
          $pengajuanDetail->nik = $arrayPengajuan[$i]['nik'];
          $pengajuanDetail->nama_lgkp = $arrayPengajuan[$i]['nama_lgkp'];
          $pengajuanDetail->nama_fam = $arrayPengajuan[$i]['namafam'];
          $pengajuanDetail->jenis_klmin = $arrayPengajuan[$i]['jenis_klmin'];
          $pengajuanDetail->tmpt_lhr = $arrayPengajuan[$i]['tmpt_lhr'];
          $pengajuanDetail->tgl_lhr = $arrayPengajuan[$i]['tgl_lhr'];


          $pengajuanDetail->stat_hbkel = $arrayPengajuan[$i]['stat_hbkel'];
          $pengajuanDetail->telepon = $arrayPengajuan[$i]['telepon'];
          $pengajuanDetail->email = $arrayPengajuan[$i]['email'];
          $pengajuanDetail->jenis_layanan = $request->jenis_layanan;

          $pengajuanDetail->status = $request->status;
          $pengajuanDetail->jenis_layanan = $request->jenis_layanan;

          $pengajuanDetail->tgl_berlaku= Carbon::now()->addMonths(6);


          $pengajuanDetail->save();
        } } else {

      $pengajuan->proc_stat=1;
      $pengajuan->nik=$nik;
      $pengajuan->email=$request->email;
      $pengajuan->telepon=$request->telepon;
      $pengajuan->nama_lgkp=strtoupper($request->nama_lgkp);
      $pengajuan->jenis_klmin=strtoupper($request->jenis_klmin);
      $pengajuan->tmpt_lhr=strtoupper($request->tmpt_lhr);
      $pengajuan->tgl_lhr=strtoupper($request->tgl_lhr);
      $pengajuan->agama=strtoupper($request->agama);
      $pengajuan->stat_kwn=strtoupper($request->stat_kwn);
      $pengajuan->gol_drh=strtoupper($request->gol_drh);
      $pengajuan->pendidikan=strtoupper($request->pendidikan);
      $pengajuan->pekerjaan=strtoupper($request->pekerjaan);
      $pengajuan->src_prov=strtoupper($request->src_prov);
      $pengajuan->src_kab=strtoupper($request->src_kab);
      $pengajuan->src_kec=strtoupper($request->src_kec);
      $pengajuan->src_kel=strtoupper($request->src_kel);
      $pengajuan->src_alamat=strtoupper($request->src_alamat);
      $pengajuan->src_rt=strtoupper($request->src_rt);
      $pengajuan->src_rw=strtoupper($request->src_rw);
      $pengajuan->prop=strtoupper($request->prop);
      $pengajuan->kab=strtoupper($request->kab);
      $pengajuan->kec=strtoupper($request->kec);
      $pengajuan->kel=strtoupper($request->kel);
      $pengajuan->alamat=strtoupper($request->alamat);
      $pengajuan->no_rw=strtoupper($request->no_rw);
      $pengajuan->no_rt=strtoupper($request->no_rt);
      $pengajuan->no_kk=strtoupper($request->no_kk);
      $pengajuan->stat_hbkel=strtoupper($request->stat_hbkel);
      $pengajuan->alasan_pindah=strtoupper($request->alasan_pindah);
      $pengajuan->jangka_waktu=strtoupper($request->jangka_waktu);
      $pengajuan->jum_anggota=strtoupper($request->jum_anggota);
      $pengajuan->tgl_datang=strtoupper($request->tgl_datang);
      $pengajuan->status=strtoupper($request->status);
      $pengajuan->jenis_layanan=strtoupper($request->jenis_layanan);
      if ($request->jenis_layanan == 2){
        $pengajuan->akta_lhr=strtoupper($request->akta_lhr);
        $pengajuan->no_akta_lhr=strtoupper($request->no_akta_lhr);
        $pengajuan->no_skpwni=strtoupper($request->no_skpwni);
      }
      if ($request->jenis_layanan == 3){
        $pengajuan->kwrngrn=strtoupper($request->kwrngrn);
        $pengajuan->nama_fam=strtoupper($request->nama_fam);
        $pengajuan->tipe_spsor=strtoupper($request->tipe_spsor);
        $pengajuan->nama_spsor=strtoupper($request->nama_spsor);
        $pengajuan->no_paspor=strtoupper($request->no_paspor);
        $pengajuan->tgl_paspor=strtoupper($request->tgl_paspor);
        $pengajuan->tgl_akh_paspor=strtoupper($request->tgl_akh_paspor);
        $pengajuan->dok_imgr=strtoupper($request->doc_imigrasi);
        $pengajuan->tgl_doc_imigrasi=strtoupper($request->tgl_doc_imigrasi);
        $pengajuan->tgl_stay=strtoupper($request->tgl_stay);
        $pengajuan->tgl_permit=strtoupper($request->tgl_permit);
        $pengajuan->tgl_akh_doc_imgr=strtoupper($request->tgl_akh_doc_imgr);
        $pengajuan->no_dok=strtoupper($request->no_dok);
      }

      $pengajuan->tgl_berlaku= Carbon::now()->addMonths(6);

        }
 
           if (!file_exists($destinationPath)) {
          mkdir($destinationPath, 0777, true);
        }
        if (!file_exists($destinationPathThumb)) {
          mkdir($destinationPathThumb, 0777, true);
        }

        $biodata5 = "";
        if ($jumlah > 0 && $jumlah != null) {
        for($i=0; $i < count($arrayPengajuan); $i++) {
          $no = $i + 1;
          $tglLhr = '';
          if($arrayPengajuan[$i]['tgl_lhr']){
            $tglLhr = Carbon::createFromFormat('Y-m-d H:i:s', $arrayPengajuan[$i]['tgl_lhr'])->format('d-m-Y');
          }

          

          $biodata5 = $biodata5.
          '<tr>
          <td width="5%" class="center">'.$no.'</td>
          <td>'.$arrayPengajuan[$i]['nik'].'</td>
          <td>'.$arrayPengajuan[$i]['nama_lgkp'].'&nbsp;'.$arrayPengajuan[$i]['namafam'].'</td>
          <td>'.$this->get_detail_wna(801,$arrayPengajuan[$i]['jenis_klmin']).'</td>
          <td>'.$arrayPengajuan[$i]['tmpt_lhr'].'</td>
          <td>'.$tglLhr.'</td>
          <td>'.$this->get_detail_wna(301,$arrayPengajuan[$i]['stat_hbkel']).'</td>
          </tr>';
        }} else {$biodata5 = "";}
        
        $rowKosong5 = "";

        $html = '<!DOCTYPE html>
        <html>
        <head>
        <style>
        @media all {
          .page-break { display: none; }
        }
        @media print {
          .page-break { display: block; page-break-before: always; }
        }
        *{ font-family: Arial, Helvetica, sans-serif; font-size:15px; }
        .my-container { position: relative; overflow: hidden; border: 1px solid; left: 0;
        right: 0;
        top: 0;
        bottom: 0;}
        .my-container:before {
          content: " ";
          display: block;
          position: absolute;
          width: 100%;
          height: 100%;
          z-index: 1;
          opacity: 0.1;
          background-image: url(\'https://disdukcapil.bandung.go.id/api/public/assets/logo-pemkot.svg\');
          background-repeat: no-repeat;
          background-position: center;
          background-size: 350px;
          border: 5px solid red;
        }
        .main-topic { display: flex; }
        .left-picture > img{ display: block; }
        hr.solid { border-top: 3px solid; }
        .center {text-align: center;}
        #page-border {
          position: absolute;
          overflow: auto;
          left: 0;
          right: 0;
          top: 0;
          bottom: 0;
          border: 5px solid red;
        }
        </style>
        </head>
        <body>
        <div class="my-container">
        <table width="60%">
        <tbody>
        <tr>
        <td width="30%"><strong style="font-size: 15px" >PROVINSI/PROVINCE</strong></td>
        <td><strong style="font-size: 15px" >:</strong></td>
        <td><strong style="font-size: 15px" >JAWA BARAT</strong></td>
        </tr>
        <tr>
        <td width="30%"><strong style="font-size: 15px" >KOTA/CITY</strong></td>
        <td><strong style="font-size: 15px" >:</strong></td>
        <td><strong style="font-size: 15px" >BANDUNG</strong></td>
        </tr>
        <tr>
        </tr>
        </tbody>
        </table>
        </br>
        <div style="text-align: center;"><strong style="font-size: 20px" ><u style="font-size: 20px" >FORMULIR PENDAFTARAN ORANG ASING TINGGAL TERBATAS/TETAP</u></br>REGISTRATION FORM FOR LIMITED STAY/PERMANENT<br></strong></div>
        </br>
        <table width="100%">
        <tbody>
        <tr>
        <td width="90%">
        <table width="100%">
        <tbody>
        <tr>
        <td>
        <tr>
        <tr>
        <td width="40%">Yang bertanda tangan di bawah ini :</td>
        <tr>
        <td>
        <tr>
        <td width="30%">1. <u>NIK *)</u></br>&nbsp;&nbsp;&nbsp;&nbsp;Number of Population Identity</td>
        <td>:</td>
        <td>'.$request->nik.'</td>
        </tr>
        <tr>
        <td width="30%">2. <u>Nama Lengkap Pemohon</u></br>&nbsp;&nbsp;&nbsp;&nbsp;Full Name</td>
        <td>:</td>
        <td>'.$request->nama_lgkp.'&nbsp;'.$request->nama_fam.'</td>
        </tr>
        <tr>
        <td width="30%">3. <u>Jenis Kelamin</u></br>&nbsp;&nbsp;&nbsp;&nbsp;Sex</td>
        <td>:</td>
        <td>'.$this->get_detail_wna(801,$request->jenis_klmin).'</td>
        </tr>
        <tr>
        <td width="30%">4. <u>Tempat Lahir</u></br>&nbsp;&nbsp;&nbsp;&nbsp;Place Of Birth</td>
        <td>:</td>
        <td>'.$request->tmpt_lhr.'</td>
        </tr>
        <tr>
        <td width="30%">5. <u>Tanggal Lahir</u></br>&nbsp;&nbsp;&nbsp;&nbsp;Date Of Birth</td>
        <td>:</td>
        <td>'.Carbon::createFromFormat('Y-m-d H:i:s', $request->tgl_lhr)->format('d-m-Y').'</td>
        </tr>
        <tr>
        <td width="30%">6. <u>Agama</u></br>&nbsp;&nbsp;&nbsp;&nbsp;Religion</td>
        <td>:</td>
        <td>'.$this->get_detail_wna(901, $request->agama).'</td>
        </tr>
        <tr>
        <td width="30%">7. <u>Alamat</u></br>&nbsp;&nbsp;&nbsp;&nbsp;Address</td>
        <td>:</td>
        <td>'.$request->alamat.'&nbsp;,'.$request->kel_desc.'&nbsp;,'.$request->kec_desc.'</td>
        </tr>
        <tr>
        <td width="30%">8. <u>Kewarganegaraan</u></br>&nbsp;&nbsp;&nbsp;&nbsp;Nationality</td>
        <td>:</td>
        <td>'.$this->get_detail_negara($request->kwrngrn).'</td>
        </tr>
        <tr>
        <td width="30%">9. <u>Status Perkawinan</u></br>&nbsp;&nbsp;&nbsp;&nbsp;Marital Status</td>
        <td>:</td>
        <td>'.$this->get_detail_wna(701,$request->stat_kwn).'</td>
        </tr>
        <tr>
        <td width="30%">10. <u>Pendidikan Terakhir</u></br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Last Education</td>
        <td>:</td>
        <td>'.$this->get_detail_wna(101,$request->pendidikan).'</td>
        </tr>
        <tr>
        <td width="30%">11. <u>Bidang Pekerjaan</u></br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Occupation</td>
        <td>:</td>
        <td>'.$this->get_detail_wna(201,$request->pekerjaan).'</td>
        </tr>
        <tr>
        <td width="30%">12. <u>Nomor Passport</u></br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Number of Passport</td>
        <td>:</td>
        <td>'.$request->no_paspor.'</td>
        </tr>
        <tr>
        <td width="30%">13. <u>Tanggal Paspor</u></br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Issued Date of Passport</td>
        <td>:</td>
        <td>'.Carbon::createFromFormat('Y-m-d H:i:s', $request->tgl_paspor)->format('d-m-Y').'</td>
        </tr>
        <tr>
        <tr>
        <td width="30%">14. <u>Tanggal Berakhir Paspor</u></br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Expired Date of Passport</td>
        <td>:</td>
        <td>'.Carbon::createFromFormat('Y-m-d H:i:s', $request->tgl_akh_paspor)->format('d-m-Y').'</td>
        </tr>     
        <tr>
        <td width="30%">15. <u>Tipe Dokumen Imigrasi</u></br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Type of Document</td>
        <td>:</td>
        <td>'.$this->get_detail_wna(501,$request->doc_imigrasi).'</td>
        </tr>
        <tr>
        <td width="30%">16. <u>Nomor Dokumen Imigrasi</u></br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;KITAS/KITAP Number</td>
        <td>:</td>
        <td>'.$request->no_dok.'</td>
        </tr>
        <tr>
        <td width="30%">17. <u>Tanggal dikeluarkan KITAS/KITAP</u></br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Issued Date of KITAS/KITAP</td>
        <td>:</td>
        <td>'.Carbon::createFromFormat('Y-m-d H:i:s', $request->tgl_doc_imigrasi)->format('d-m-Y').'</td>
        </tr>
        <tr>
        <td width="30%">18. <u>Diizinkan tinggal sampai tanggal</u></br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Permitted to stay in indonesia until</td>
        <td>:</td>
        <td>'.Carbon::createFromFormat('Y-m-d H:i:s', $request->tgl_akh_doc_imgr)->format('d-m-Y').'</td>
        </tr> 
        </td>
        <td></td>
        </tr>
        </tbody>
        </table>
        </td>
        
        <table width="100%" style="border-collapse: collapse;">
        <tbody>
        <tr style="background-color: darkgray;">
        <th class="border" width="1%">No.</th>
        <th class="border" width="8%">NIK</th>
        <th class="border">Name</th>
        <th class="border" width="8%">Sex</th>
        <th class="border">Place of birth</th>
        <th class="border" width="12%">Date of Birth</th>
        <th class="border">Relationship</th>
        </tr>
        '.$biodata5.'
        '.$rowKosong5.'
        </tbody>
        </table>
        
        <td width="50%">

        </td>
        </tr>
        </tbody>
        </table>
        <div style="text-align: left;"><strong style="font-size: 10px" >THIS ELECTRONIC FORM IS A DOCUMENT THAT IS THE SAME WITH A NON-ELECTRONIC FORM AND IS A LEGAL PROOF OF THE APPLICANT WITHOUT SIGNATURE</strong></div>
        </br></br>
        <table width="100%">
        <tbody>
        <tr>
        <td></td>
        <td style="text-align: center;" width="40%"><br><br><br>Bandung, '.Carbon::now()->format('d-m-Y').'<br>
        Pelapor<br><br>('.$request->nama_lgkp.')
        </td>
        </tr>
        <tr>
        <td colspan="3"></td>
        </tr>
        </tbody>
        </table>
        </body>
        </html>
        ';
        $tgl = Carbon::now()->format('d-m-Y-His');
        $url = $destinationPath.'formulir-'.$tgl.'.pdf';
        Browsershot::html($html)
        ->fullPage()
        ->showBackground()
        ->waitUntilNetworkIdle()
        ->format('A4')
        ->save($url);

      
        
        $pengajuan->img_15=$imgName."formulir-".$tgl.".pdf";

      



      if($request->hasFile('imgupload1')){
        $photo=$request->file('imgupload1')->getClientOriginalName();
        $extension = $request->file('imgupload1')->getClientOriginalExtension();
        $filename = "imgupload1.".$extension;
        $request->file('imgupload1')->move($destinationPath,$filename);
        $pengajuan->img_1=$imgName.$filename;

        $img1 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_1.jpg";
        $img1->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_1=$imgThumbName.$filename;
      }

      /////////////////
      if($request->hasFile('imgupload2')){
        $photo=$request->file('imgupload2')->getClientOriginalName();
        $extension = $request->file('imgupload2')->getClientOriginalExtension();
        $filename = "imgupload2.".$extension;
        $request->file('imgupload2')->move($destinationPath,$filename);
        $pengajuan->img_2=$imgName.$filename;

        $img2 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_2.jpg";
        $img2->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_2=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload3')){
        $photo=$request->file('imgupload3')->getClientOriginalName();
        $extension = $request->file('imgupload3')->getClientOriginalExtension();
        $filename = "imgupload3.".$extension;
        $request->file('imgupload3')->move($destinationPath,$filename);
        $pengajuan->img_3=$imgName.$filename;

        $img3 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_3.jpg";
        $img3->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_3=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload4')){
        $photo=$request->file('imgupload4')->getClientOriginalName();
        $extension = $request->file('imgupload4')->getClientOriginalExtension();
        $filename = "imgupload4.".$extension;
        $request->file('imgupload4')->move($destinationPath,$filename);
        $pengajuan->img_4=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_4.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_4=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload5')){
        $photo=$request->file('imgupload5')->getClientOriginalName();
        $extension = $request->file('imgupload5')->getClientOriginalExtension();
        $filename = "imgupload5.".$extension;
        $request->file('imgupload5')->move($destinationPath,$filename);
        $pengajuan->img_5=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_5.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_5=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload6')){
        $photo=$request->file('imgupload6')->getClientOriginalName();
        $extension = $request->file('imgupload6')->getClientOriginalExtension();
        $filename = "imgupload6.".$extension;
        $request->file('imgupload6')->move($destinationPath,$filename);
        $pengajuan->img_6=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_6.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_6=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload7')){
        $photo=$request->file('imgupload7')->getClientOriginalName();
        $extension = $request->file('imgupload7')->getClientOriginalExtension();
        $filename = "imgupload7.".$extension;
        $request->file('imgupload7')->move($destinationPath,$filename);
        $pengajuan->img_7=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_7.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_7=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload8')){
        $photo=$request->file('imgupload8')->getClientOriginalName();
        $extension = $request->file('imgupload8')->getClientOriginalExtension();
        $filename = "imgupload8.".$extension;
        $request->file('imgupload8')->move($destinationPath,$filename);
        $pengajuan->img_8=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_8.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_8=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload9')){
        $photo=$request->file('imgupload9')->getClientOriginalName();
        $extension = $request->file('imgupload9')->getClientOriginalExtension();
        $filename = "imgupload9.".$extension;
        $request->file('imgupload9')->move($destinationPath,$filename);
        $pengajuan->img_9=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_9.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_9=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload10')){
        $photo=$request->file('imgupload10')->getClientOriginalName();
        $extension = $request->file('imgupload10')->getClientOriginalExtension();
        $filename = "imgupload10.".$extension;
        $request->file('imgupload10')->move($destinationPath,$filename);
        $pengajuan->img_10=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_10.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_10=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload11')){
        $photo=$request->file('imgupload11')->getClientOriginalName();
        $extension = $request->file('imgupload11')->getClientOriginalExtension();
        $filename = "imgupload11.".$extension;
        $request->file('imgupload11')->move($destinationPath,$filename);
        $pengajuan->img_11=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_11.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_11=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload12')){
        $photo=$request->file('imgupload12')->getClientOriginalName();
        $extension = $request->file('imgupload12')->getClientOriginalExtension();
        $filename = "imgupload12.".$extension;
        $request->file('imgupload12')->move($destinationPath,$filename);
        $pengajuan->img_12=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_12.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_12=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload13')){
        $photo=$request->file('imgupload13')->getClientOriginalName();
        $extension = $request->file('imgupload13')->getClientOriginalExtension();
        $filename = "imgupload13.".$extension;
        $request->file('imgupload13')->move($destinationPath,$filename);
        $pengajuan->img_13=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_13.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_13=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload14')){
        $photo=$request->file('imgupload14')->getClientOriginalName();
        $extension = $request->file('imgupload14')->getClientOriginalExtension();
        $filename = "imgupload14.".$extension;
        $request->file('imgupload14')->move($destinationPath,$filename);
        $pengajuan->img_14=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_14.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_14=$imgThumbName.$filename;
      }
      /////////////////
      // if($request->hasFile('imgupload15')){
      //   $photo=$request->file('imgupload15')->getClientOriginalName();
      //   $extension = $request->file('imgupload15')->getClientOriginalExtension();
      //   $filename = "imgupload15.".$extension;
      //   $request->file('imgupload15')->move($destinationPath,$filename);
      //   $pengajuan->img_15=$imgName.$filename;

      //   $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
      //   $filename = "imgupload_thumb_15.jpg";
      //   $img4->save($destinationPathThumb.$filename);
      //   $pengajuan->img_thumb_15=$imgThumbName.$filename;
      // }

      $pengajuan->save();



      /////////////////
 

      $msg = 'Pengajuan anda masih dalam tahap menunggu validasi';
      $ntf = 'Pemberitahuan';
      $jenis = 'PENGAJUAN';
      $pengajuan_id = null;
      $from_user_id = 'DISDUKCAPIL KOTA BANDUNG';
      AndrApi::sendnotification($jenis,$pengajuan_id,$jenis_layanan,$nik,$msg,$ntf,$from_user_id);
      return response()->json([
        'status' => 'success',
        'data' => 'Pendaftaran berhasil'
      ]);
    }
  }


  public function store(Request $request)
  {
    $nik= $request->nik;
    $jenis_layanan=$request->jenis_layanan;
    $is_done = 0;
    if (empty($nik)){
      return response()->json([
          'status' => 'error',
          'data' => 'Nik Tidak Boleh Kosong !'
        ]);
    }
    

    $query=DB::connection('webpunten')
    ->select("SELECT SUM(c) as JUMLAH
            FROM (
            SELECT COUNT(1) AS c FROM PENGAJUANDATANG where NIK = $nik and proc_stat <> 4
            UNION ALL
            SELECT COUNT(1) FROM PENGAJUANSKTS where NIK = $nik and proc_stat <> 4
            UNION ALL
            SELECT COUNT(1) FROM PENGAJUANWNA where NIK = $nik and proc_stat <> 4)");
    if($query[0]->jumlah == 1){
      $is_done = 1;
      return response()->json([
        'status' => 'error',
        'data' => 'NIK sudah pernah mengajukan sebelumnya'
      ]);
    }

    // $query=DB::connection('webpunten')
    // ->select("SELECT COUNT(1) JUMLAH FROM PENGAJUANDATANG WHERE NIK = ".$nik." AND JENIS_LAYANAN = 2 AND PROC_STAT <> 4");
    // if($query[0]->jumlah == 1){
    //   $is_done = 1;
    //   return response()->json([
    //     'status' => 'error',
    //     'data' => 'NIK sudah pernah mengajukan sebelumnya'
    //   ]);
    // }

    // $query=DB::connection('webpunten')
    // ->select("SELECT COUNT(1) JUMLAH FROM PENGAJUANWNA WHERE NIK = ".$nik." AND JENIS_LAYANAN = 3 AND PROC_STAT <> 4");
    // if($query[0]->jumlah == 1){
    //   $is_done = 1;
    //   return response()->json([
    //     'status' => 'error',
    //     'data' => 'NIK sudah pernah mengajukan sebelumnya'
    //   ]);
    // }

    if($is_done == 1){
      return response()->json([
        'status' => 'error',
        'data' => 'NIK sudah pernah mengajukan sebelumnya'
      ]);
    }else{
      if ($jenis_layanan == 1){
        $destinationPath=$this->pengajuan_path().'/pengajuan/skts/'.$nik.'/ori/';
        $destinationPathThumb=$this->pengajuan_path().'/pengajuan/skts/'.$nik.'/thumb/';
        $imgName = 'pengajuan/skts/'.$nik.'/ori/';
        $imgThumbName = 'pengajuan/skts/'.$nik.'/thumb/';
      }else if($jenis_layanan == 2){
        $destinationPath=$this->pengajuan_path().'/pengajuan/datang/'.$nik.'/ori/';
        $destinationPathThumb=$this->pengajuan_path().'/pengajuan/datang/'.$nik.'/thumb/';
        $imgName = 'pengajuan/datang/'.$nik.'/ori/';
        $imgThumbName = 'pengajuan/datang/'.$nik.'/thumb/';
      }else if($jenis_layanan == 3){
        $destinationPath=$this->pengajuan_path().'/pengajuan/wna/'.$nik.'/ori/';
        $destinationPathThumb=$this->pengajuan_path().'/pengajuan/wna/'.$nik.'/thumb/';
        $imgName = 'pengajuan/wna/'.$nik.'/ori/';
        $imgThumbName = 'pengajuan/wna/'.$nik.'/thumb/';
      }else{
        return response()->json([
          'status' => 'error',
          'data' => 'Jenis Pengajuan Salah Mohon Cek Kemabali'
        ]);
      }


      $picname = "imgupload_thumb_1.jpg";
      if (!file_exists($destinationPath)) {
        mkdir($destinationPath, 0777, true);
      }
      if (!file_exists($destinationPathThumb)) {
        mkdir($destinationPathThumb, 0777, true);
      }

      if ($jenis_layanan == 1){
        $pengajuan = new Pengajuanskts;
      }else if($jenis_layanan == 2){
        $pengajuan = new Pengajuandatang;
      }else if($jenis_layanan == 3){
        $pengajuan = new Pengajuanwna;
      }else{
        return response()->json([
          'status' => 'error',
          'data' => 'Jenis Pengajuan Salah Mohon Cek Kemabali'
        ]);
      }

      $pengajuan->proc_stat=1;
      $pengajuan->nik=$nik;
      $pengajuan->email=$request->email;
      $pengajuan->telepon=$request->telepon;
      $pengajuan->nama_lgkp=strtoupper($request->nama_lgkp);
      $pengajuan->jenis_klmin=strtoupper($request->jenis_klmin);
      $pengajuan->tmpt_lhr=strtoupper($request->tmpt_lhr);
      $pengajuan->tgl_lhr=strtoupper($request->tgl_lhr);
      $pengajuan->agama=strtoupper($request->agama);
      $pengajuan->stat_kwn=strtoupper($request->stat_kwn);
      $pengajuan->gol_drh=strtoupper($request->gol_drh);
      $pengajuan->pendidikan=strtoupper($request->pendidikan);
      $pengajuan->pekerjaan=strtoupper($request->pekerjaan);
      $pengajuan->src_prov=strtoupper($request->src_prov);
      $pengajuan->src_kab=strtoupper($request->src_kab);
      $pengajuan->src_kec=strtoupper($request->src_kec);
      $pengajuan->src_kel=strtoupper($request->src_kel);
      $pengajuan->src_alamat=strtoupper($request->src_alamat);
      $pengajuan->src_rt=strtoupper($request->src_rt);
      $pengajuan->src_rw=strtoupper($request->src_rw);
      $pengajuan->prop=strtoupper($request->prop);
      $pengajuan->kab=strtoupper($request->kab);
      $pengajuan->kec=strtoupper($request->kec);
      $pengajuan->kel=strtoupper($request->kel);
      $pengajuan->alamat=strtoupper($request->alamat);
      $pengajuan->no_rw=strtoupper($request->no_rw);
      $pengajuan->no_rt=strtoupper($request->no_rt);
      $pengajuan->no_kk=strtoupper($request->no_kk);
      $pengajuan->stat_hbkel=strtoupper($request->stat_hbkel);
      $pengajuan->alasan_pindah=strtoupper($request->alasan_pindah);
      $pengajuan->jangka_waktu=strtoupper($request->jangka_waktu);
      $pengajuan->jum_anggota=strtoupper($request->jum_anggota);
      $pengajuan->tgl_datang=strtoupper($request->tgl_datang);
      $pengajuan->status=strtoupper($request->status);
      $pengajuan->jenis_layanan=strtoupper($request->jenis_layanan);
      if ($request->jenis_layanan == 2){
        $pengajuan->akta_lhr=strtoupper($request->akta_lhr);
        $pengajuan->no_akta_lhr=strtoupper($request->no_akta_lhr);
        $pengajuan->no_skpwni=strtoupper($request->no_skpwni);
      }
      if ($request->jenis_layanan == 3){
        $pengajuan->kwrngrn=strtoupper($request->kwrngrn);
        $pengajuan->nama_fam=strtoupper($request->nama_fam);
        $pengajuan->tipe_spsor=strtoupper($request->tipe_spsor);
        $pengajuan->nama_spsor=strtoupper($request->nama_spsor);
        $pengajuan->no_paspor=strtoupper($request->no_paspor);
        $pengajuan->tgl_paspor=strtoupper($request->tgl_paspor);
        $pengajuan->tgl_akh_paspor=strtoupper($request->tgl_akh_paspor);
        $pengajuan->no_dok=strtoupper($request->no_doc);
        $pengajuan->dok_imgr=strtoupper($request->doc_imigrasi);
        $pengajuan->tgl_doc_imigrasi=strtoupper($request->tgl_doc_imigrasi);
        $pengajuan->tgl_stay=strtoupper($request->tgl_stay);
        $pengajuan->tgl_permit=strtoupper($request->tgl_permit);
        $pengajuan->tgl_akh_doc_imgr=strtoupper($request->tgl_akh_doc_imgr);
      }

      $pengajuan->tgl_berlaku= Carbon::now()->addMonths(6);


      if($jenis_layanan == 1) {

        $destinationPath=$this->pengajuan_path().'/pengajuan/skts/'.$nik.'/ori/';
        $destinationPathThumb=$this->pengajuan_path().'/pengajuan/skts/'.$nik.'/thumb/';
        $imgName = 'pengajuan/skts/'.$nik.'/ori/';
        $imgThumbName = 'pengajuan/skts/'.$nik.'/thumb/';



        if (!file_exists($destinationPath)) {
          mkdir($destinationPath, 0777, true);
        }
        if (!file_exists($destinationPathThumb)) {
          mkdir($destinationPathThumb, 0777, true);
        }

        $html = '<!DOCTYPE html>
        <html>
        <head>
        <style>
        @media all {
          .page-break { display: none; }
        }
        @media print {
          .page-break { display: block; page-break-before: always; }
        }
        *{ font-family: Arial, Helvetica, sans-serif; font-size:17px; }
        .my-container { position: relative; overflow: hidden; border: 1px solid; left: 0;
        right: 0;
        top: 0;
        bottom: 0;}
        .my-container:before {
          content: " ";
          display: block;
          position: absolute;
          width: 100%;
          height: 100%;
          z-index: 1;
          opacity: 0.1;
          background-image: url(\'https://disdukcapil.bandung.go.id/api/public/assets/logo-pemkot.svg\');
          background-repeat: no-repeat;
          background-position: center;
          background-size: 350px;
          border: 5px solid red;
        }
        .main-topic { display: flex; }
        .left-picture > img{ display: block; }
        hr.solid { border-top: 3px solid; }
        .center {text-align: center;}
        #page-border {
          position: absolute;
          overflow: auto;
          left: 0;
          right: 0;
          top: 0;
          bottom: 0;
          border: 5px solid red;
        }
        </style>
        </head>
        <body>
        <div class="my-container">
        <table width="100%">
        <tbody>
        <tr>
        <td></td>
        <td style="text-align: center; border: 1px solid;" width="20%">F.4 - 01</td>
        </tr>
        <tr>
        <td colspan="3"></td>
        </tr>
        </tbody>
        </table>
        <div style="text-align: center;"><strong style="font-size: 25px" >FORMULIR</br>PENDATAAN PENDUDUK NON PERMANEN<br></strong></div>
        </br>
          </br></br></br></br>
        <table width="100%">
        <tbody>
        <tr>
        <td width="70%">
        <table width="100%">
        <tbody>
        <tr>
        <td>
        <tr>
        <tr>
        <td width="40%"><strong>I. IDENTITAS LOKASI</strong></td>
        <tr>
        <td>
        <tr>
        <td width="30%">Nama Provinsi</td>
        <td>:</td>
        <td>Jawa Barat</td>
        </tr>
        <tr>
        <td>Nama Kabupaten/Kota</td>
        <td>:</td>
        <td>Kota Bandung</td>
        </tr>
        <tr>
        <td>Nama Kecamatan</td>
        <td>:</td>
        <td>'.$request->kel_desc.'</td>
        </tr>
        <tr>
        <td>Nama Kelurahan</td>
        <td>:</td>
        <td>'.$request->kec_desc.'</td>
        </tr>
        <tr>
        <td>
        <tr>
        <tr>
        <td>
        <tr>
        <tr>
        <td>
        <tr>
        <td width="30%"><strong>II. IDENTITAS PENDUDUK</strong></td>
        <tr>
        <td>
        <tr>
        <td width="30%">NIK</td>
        <td>:</td>
        <td>'.$request->nik.'</td>
        </tr>
        <tr>
        <td>Nama Lengkap</td>
        <td>:</td>
        <td>'.$request->nama_lgkp.'</td>
        </tr>
        <tr>
        <td>Tempat dan Tanggal Lahir</td>
        <td>:</td>
        <td>'.$request->tmpt_lhr.','.Carbon::createFromFormat('Y-m-d H:i:s', $request->tgl_lhr)->format('d-m-Y').'</td>
        </tr>
        <tr>
        <td>Provinsi Daerah Asal</td>
        <td>:</td>
        <td>'.$request->src_prov_desc.'</td>
        </tr>
        <tr>
        <td>Kabupaten/Kota Daerah Asal</td>
        <td>:</td>
        <td>'.$request->src_kab_desc.'</td>
        </tr>
        <tr>
        <td>Kecamatan Daerah Asal</td>
        <td>:</td>
        <td>'.$request->src_kec_desc.'</td>
        </tr>
        <tr>
        <td>Kelurahan Daerah Asal</td>
        <td>:</td>
        <td>'.$request->src_kel_desc.'</td>
        </tr>
        <tr>
        <td>Alamat Daerah Asal</td>
        <td>:</td>
        <td>'.$request->src_alamat.'</td>
        </tr>
        <tr>
        <td width="30%">Jenis Kelamin</td>
        <td>:</td>
        <td>'.$this->get_detail_wni(801,$request->jenis_klmin).'</td>
        </tr>
        <tr>
        <td>Pendidikan</td>
        <td>:</td>
        <td>'.$this->get_detail_wni(101, $request->pendidikan).'</td>
        </tr>
        <tr>
        <td>Pekerjaan</td>
        <td>:</td>
        <td>'.$this->get_detail_wni(201, $request->pekerjaan).'</td>
        </tr>
        <tr>
        <td>Tanggal Kedatangan</td>
        <td>:</td>
        <td>'.Carbon::createFromFormat('Y-m-d H:i:s', $request->tgl_datang)->format('d-m-Y').'</td>
        </tr>          
        <tr>
        <td width="30%">Alasan Domisili Sementara</td>
        <td>:</td>
        <td>'.$this->get_detail_wni(901, $request->alasan_pindah).'</td>
        </tr>
        <tr>
        <td>Alamat Tinggal Sementara</td>
        <td>:</td>
        <td>'.$request->alamat.'</td>
        </tr>
        <tr>
        <td>Tinggal Sementara Sampai</td>
        <td>:</td>
        <td>'.Carbon::createFromFormat('Y-m-d H:i:s', $request->jangka_waktu)->format('d-m-Y').'</td>
        </tr>
        </td>
        <td></td>
        </tr>
        </tbody>
        </table>
        </td>
        <td width="50%">

        </td>
        </tr>
        </tbody>
        </table>
        <div style="text-align: left;"><strong style="font-size: 10px" >FORMULIR ELEKTRONIK INI ADALAH DOKUMEN YANG DIPERSAMAKAN DENGAN FORMULIR NON ELEKTRONIK DAN MERUPAKAN BUKTI PENGAJUAN YANG SAH DARI PEMOHON TANPA DIBUBUHI TANDA TANGAN.</strong></div>
        </br></br></br></br></br></br></br>
        <table width="100%">
        <tbody>
        <tr>
        <td></td>
        <td style="text-align: center;" width="40%"><br><br><br>Bandung, '.Carbon::now()->format('d-m-Y').'<br>
        Pelapor<br><br>('.$request->nama_lgkp.')
        </td>
        </tr>
        <tr>
        <td colspan="3"></td>
        </tr>
        </tbody>
        </table>
        </body>
        </html>
        ';
        $tgl = Carbon::now()->format('d-m-Y-His');
        $url = $destinationPath.'formulir-'.$tgl.'.pdf';
        Browsershot::html($html)
        ->fullPage()
        ->showBackground()
        ->waitUntilNetworkIdle()
        ->format('A4')
        ->save($url);

      
        
        $pengajuan->img_15=$imgName."formulir-".$tgl.".pdf";

      }

      /////////////////
      if($request->hasFile('imgupload1')){
        $photo=$request->file('imgupload1')->getClientOriginalName();
        $extension = $request->file('imgupload1')->getClientOriginalExtension();
        $filename = "imgupload1.".$extension;
        $request->file('imgupload1')->move($destinationPath,$filename);
        $pengajuan->img_1=$imgName.$filename;

        $img1 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_1.jpg";
        $img1->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_1=$imgThumbName.$filename;
      }

      /////////////////
      if($request->hasFile('imgupload2')){
        $photo=$request->file('imgupload2')->getClientOriginalName();
        $extension = $request->file('imgupload2')->getClientOriginalExtension();
        $filename = "imgupload2.".$extension;
        $request->file('imgupload2')->move($destinationPath,$filename);
        $pengajuan->img_2=$imgName.$filename;

        $img2 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_2.jpg";
        $img2->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_2=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload3')){
        $photo=$request->file('imgupload3')->getClientOriginalName();
        $extension = $request->file('imgupload3')->getClientOriginalExtension();
        $filename = "imgupload3.".$extension;
        $request->file('imgupload3')->move($destinationPath,$filename);
        $pengajuan->img_3=$imgName.$filename;

        $img3 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_3.jpg";
        $img3->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_3=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload4')){
        $photo=$request->file('imgupload4')->getClientOriginalName();
        $extension = $request->file('imgupload4')->getClientOriginalExtension();
        $filename = "imgupload4.".$extension;
        $request->file('imgupload4')->move($destinationPath,$filename);
        $pengajuan->img_4=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_4.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_4=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload5')){
        $photo=$request->file('imgupload5')->getClientOriginalName();
        $extension = $request->file('imgupload5')->getClientOriginalExtension();
        $filename = "imgupload5.".$extension;
        $request->file('imgupload5')->move($destinationPath,$filename);
        $pengajuan->img_5=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_5.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_5=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload6')){
        $photo=$request->file('imgupload6')->getClientOriginalName();
        $extension = $request->file('imgupload6')->getClientOriginalExtension();
        $filename = "imgupload6.".$extension;
        $request->file('imgupload6')->move($destinationPath,$filename);
        $pengajuan->img_6=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_6.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_6=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload7')){
        $photo=$request->file('imgupload7')->getClientOriginalName();
        $extension = $request->file('imgupload7')->getClientOriginalExtension();
        $filename = "imgupload7.".$extension;
        $request->file('imgupload7')->move($destinationPath,$filename);
        $pengajuan->img_7=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_7.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_7=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload8')){
        $photo=$request->file('imgupload8')->getClientOriginalName();
        $extension = $request->file('imgupload8')->getClientOriginalExtension();
        $filename = "imgupload8.".$extension;
        $request->file('imgupload8')->move($destinationPath,$filename);
        $pengajuan->img_8=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_8.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_8=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload9')){
        $photo=$request->file('imgupload9')->getClientOriginalName();
        $extension = $request->file('imgupload9')->getClientOriginalExtension();
        $filename = "imgupload9.".$extension;
        $request->file('imgupload9')->move($destinationPath,$filename);
        $pengajuan->img_9=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_9.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_9=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload10')){
        $photo=$request->file('imgupload10')->getClientOriginalName();
        $extension = $request->file('imgupload10')->getClientOriginalExtension();
        $filename = "imgupload10.".$extension;
        $request->file('imgupload10')->move($destinationPath,$filename);
        $pengajuan->img_10=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_10.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_10=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload11')){
        $photo=$request->file('imgupload11')->getClientOriginalName();
        $extension = $request->file('imgupload11')->getClientOriginalExtension();
        $filename = "imgupload11.".$extension;
        $request->file('imgupload11')->move($destinationPath,$filename);
        $pengajuan->img_11=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_11.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_11=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload12')){
        $photo=$request->file('imgupload12')->getClientOriginalName();
        $extension = $request->file('imgupload12')->getClientOriginalExtension();
        $filename = "imgupload12.".$extension;
        $request->file('imgupload12')->move($destinationPath,$filename);
        $pengajuan->img_12=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_12.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_12=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload13')){
        $photo=$request->file('imgupload13')->getClientOriginalName();
        $extension = $request->file('imgupload13')->getClientOriginalExtension();
        $filename = "imgupload13.".$extension;
        $request->file('imgupload13')->move($destinationPath,$filename);
        $pengajuan->img_13=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_13.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_13=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload14')){
        $photo=$request->file('imgupload14')->getClientOriginalName();
        $extension = $request->file('imgupload14')->getClientOriginalExtension();
        $filename = "imgupload14.".$extension;
        $request->file('imgupload14')->move($destinationPath,$filename);
        $pengajuan->img_14=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_14.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_14=$imgThumbName.$filename;
      }
      /////////////////
      // if($request->hasFile('imgupload15')){
      //   $photo=$request->file('imgupload15')->getClientOriginalName();
      //   $extension = $request->file('imgupload15')->getClientOriginalExtension();
      //   $filename = "imgupload15.".$extension;
      //   $request->file('imgupload15')->move($destinationPath,$filename);
      //   $pengajuan->img_15=$imgName.$filename;

      //   $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
      //   $filename = "imgupload_thumb_15.jpg";
      //   $img4->save($destinationPathThumb.$filename);
      //   $pengajuan->img_thumb_15=$imgThumbName.$filename;
      // }

      $pengajuan->save();

      $msg = 'Pengajuan anda masih dalam tahap menunggu validasi';
      $ntf = 'Pemberitahuan';
      $jenis = 'PENGAJUAN';
      $pengajuan_id = null;
      $from_user_id = 'DISDUKCAPIL KOTA BANDUNG';
      AndrApi::sendnotification($jenis,$pengajuan_id,$jenis_layanan,$nik,$msg,$ntf,$from_user_id);
      return response()->json([
        'status' => 'success',
        'data' => 'Pendaftaran berhasil'
      ]);
    }
  }




  public function store_admin(Request $request)
  {
    $nik= $request->nik;
    $jenis_layanan=$request->jenis_layanan;
    $is_done = 0;
    if (empty($nik)){
      return response()->json([
          'status' => 'error',
          'data' => 'Nik Tidak Boleh Kosong !'
        ]);
    }
    

    $query=DB::connection('webpunten')
    ->select("SELECT SUM(c) as JUMLAH
            FROM (
            SELECT COUNT(1) AS c FROM PENGAJUANDATANG where NIK = $nik and proc_stat <> 4
            UNION ALL
            SELECT COUNT(1) FROM PENGAJUANSKTS where NIK = $nik and proc_stat <> 4
            UNION ALL
            SELECT COUNT(1) FROM PENGAJUANWNA where NIK = $nik and proc_stat <> 4)");
    if($query[0]->jumlah == 1){
      $is_done = 1;
      return response()->json([
        'status' => 'error',
        'data' => 'NIK sudah pernah mengajukan sebelumnya'
      ]);
    }

    if($is_done == 1){
      return response()->json([
        'status' => 'error',
        'data' => 'NIK sudah pernah mengajukan sebelumnya'
      ]);
    }else{
      if ($jenis_layanan == 1){
        $destinationPath=$this->pengajuan_path().'/pengajuan/skts/'.$nik.'/ori/';
        $destinationPathThumb=$this->pengajuan_path().'/pengajuan/skts/'.$nik.'/thumb/';
        $imgName = 'pengajuan/skts/'.$nik.'/ori/';
        $imgThumbName = 'pengajuan/skts/'.$nik.'/thumb/';
      }else if($jenis_layanan == 2){
        $destinationPath=$this->pengajuan_path().'/pengajuan/datang/'.$nik.'/ori/';
        $destinationPathThumb=$this->pengajuan_path().'/pengajuan/datang/'.$nik.'/thumb/';
        $imgName = 'pengajuan/datang/'.$nik.'/ori/';
        $imgThumbName = 'pengajuan/datang/'.$nik.'/thumb/';
      }else if($jenis_layanan == 3){
        $destinationPath=$this->pengajuan_path().'/pengajuan/wna/'.$nik.'/ori/';
        $destinationPathThumb=$this->pengajuan_path().'/pengajuan/wna/'.$nik.'/thumb/';
        $imgName = 'pengajuan/wna/'.$nik.'/ori/';
        $imgThumbName = 'pengajuan/wna/'.$nik.'/thumb/';
      }else{
        return response()->json([
          'status' => 'error',
          'data' => 'Jenis Pengajuan Salah Mohon Cek Kemabali'
        ]);
      }


      $picname = "imgupload_thumb_1.jpg";
      if (!file_exists($destinationPath)) {
        mkdir($destinationPath, 0777, true);
      }
      if (!file_exists($destinationPathThumb)) {
        mkdir($destinationPathThumb, 0777, true);
      }

      if ($jenis_layanan == 1){
        $pengajuan = new Pengajuanskts;
      }else if($jenis_layanan == 2){
        $pengajuan = new Pengajuandatang;
      }else if($jenis_layanan == 3){
        $pengajuan = new Pengajuanwna;
      }else{
        return response()->json([
          'status' => 'error',
          'data' => 'Jenis Pengajuan Salah Mohon Cek Kemabali'
        ]);
      }

      $pengajuan->proc_stat=1;
      $pengajuan->nik=$nik;
      $pengajuan->email=$request->email;
      $pengajuan->telepon=$request->telepon;
      $pengajuan->nama_lgkp=strtoupper($request->nama_lgkp);
      $pengajuan->jenis_klmin=strtoupper($request->jenis_klmin);
      $pengajuan->tmpt_lhr=strtoupper($request->tmpt_lhr);
      $pengajuan->tgl_lhr=strtoupper($request->tgl_lhr);
      $pengajuan->agama=strtoupper($request->agama);
      $pengajuan->stat_kwn=strtoupper($request->stat_kwn);
      $pengajuan->gol_drh=strtoupper($request->gol_drh);
      $pengajuan->pendidikan=strtoupper($request->pendidikan);
      $pengajuan->pekerjaan=strtoupper($request->pekerjaan);
      $pengajuan->src_prov=strtoupper($request->src_prov);
      $pengajuan->src_kab=strtoupper($request->src_kab);
      $pengajuan->src_kec=strtoupper($request->src_kec);
      $pengajuan->src_kel=strtoupper($request->src_kel);
      $pengajuan->src_alamat=strtoupper($request->src_alamat);
      $pengajuan->src_rt=strtoupper($request->src_rt);
      $pengajuan->src_rw=strtoupper($request->src_rw);
      $pengajuan->prop=strtoupper($request->prop);
      $pengajuan->kab=strtoupper($request->kab);
      $pengajuan->kec=strtoupper($request->kec);
      $pengajuan->kel=strtoupper($request->kel);
      $pengajuan->alamat=strtoupper($request->alamat);
      $pengajuan->no_rw=strtoupper($request->no_rw);
      $pengajuan->no_rt=strtoupper($request->no_rt);
      $pengajuan->no_kk=strtoupper($request->no_kk);
      $pengajuan->stat_hbkel=strtoupper($request->stat_hbkel);
      $pengajuan->alasan_pindah=strtoupper($request->alasan_pindah);
      $pengajuan->jangka_waktu=Carbon::now()->addYears(1);
      $pengajuan->jum_anggota=strtoupper($request->jum_anggota);
      $pengajuan->tgl_datang=strtoupper($request->tgl_datang);
      $pengajuan->status=strtoupper($request->status);
      $pengajuan->jenis_layanan=strtoupper($request->jenis_layanan);
      if ($request->jenis_layanan == 2){
        $pengajuan->akta_lhr=strtoupper($request->akta_lhr);
        $pengajuan->no_akta_lhr=strtoupper($request->no_akta_lhr);
        $pengajuan->no_skpwni=strtoupper($request->no_skpwni);
      }
      if ($request->jenis_layanan == 3){
        $pengajuan->kwrngrn=strtoupper($request->kwrngrn);
        $pengajuan->nama_fam=strtoupper($request->nama_fam);
        $pengajuan->tipe_spsor=strtoupper($request->tipe_spsor);
        $pengajuan->nama_spsor=strtoupper($request->nama_spsor);
        $pengajuan->no_paspor=strtoupper($request->no_paspor);
        $pengajuan->tgl_paspor=strtoupper($request->tgl_paspor);
        $pengajuan->tgl_akh_paspor=strtoupper($request->tgl_akh_paspor);
        $pengajuan->no_dok=strtoupper($request->no_doc);
        $pengajuan->dok_imgr=strtoupper($request->doc_imigrasi);
        $pengajuan->tgl_doc_imigrasi=strtoupper($request->tgl_doc_imigrasi);
        $pengajuan->tgl_stay=strtoupper($request->tgl_stay);
        $pengajuan->tgl_permit=strtoupper($request->tgl_permit);
        $pengajuan->tgl_akh_doc_imgr=strtoupper($request->tgl_akh_doc_imgr);
      }

      $pengajuan->tgl_berlaku= Carbon::now()->addMonths(6);


      if($jenis_layanan == 1) {

        $destinationPath=$this->pengajuan_path().'/pengajuan/skts/'.$nik.'/ori/';
        $destinationPathThumb=$this->pengajuan_path().'/pengajuan/skts/'.$nik.'/thumb/';
        $imgName = 'pengajuan/skts/'.$nik.'/ori/';
        $imgThumbName = 'pengajuan/skts/'.$nik.'/thumb/';



        if (!file_exists($destinationPath)) {
          mkdir($destinationPath, 0777, true);
        }
        if (!file_exists($destinationPathThumb)) {
          mkdir($destinationPathThumb, 0777, true);
        }

        $html = '<!DOCTYPE html>
        <html>
        <head>
        <style>
        @media all {
          .page-break { display: none; }
        }
        @media print {
          .page-break { display: block; page-break-before: always; }
        }
        *{ font-family: Arial, Helvetica, sans-serif; font-size:17px; }
        .my-container { position: relative; overflow: hidden; border: 1px solid; left: 0;
        right: 0;
        top: 0;
        bottom: 0;}
        .my-container:before {
          content: " ";
          display: block;
          position: absolute;
          width: 100%;
          height: 100%;
          z-index: 1;
          opacity: 0.1;
          background-image: url(\'https://disdukcapil.bandung.go.id/api/public/assets/logo-pemkot.svg\');
          background-repeat: no-repeat;
          background-position: center;
          background-size: 350px;
          border: 5px solid red;
        }
        .main-topic { display: flex; }
        .left-picture > img{ display: block; }
        hr.solid { border-top: 3px solid; }
        .center {text-align: center;}
        #page-border {
          position: absolute;
          overflow: auto;
          left: 0;
          right: 0;
          top: 0;
          bottom: 0;
          border: 5px solid red;
        }
        </style>
        </head>
        <body>
        <div class="my-container">
        <table width="100%">
        <tbody>
        <tr>
        <td></td>
        <td style="text-align: center; border: 1px solid;" width="20%">F.4 - 01</td>
        </tr>
        <tr>
        <td colspan="3"></td>
        </tr>
        </tbody>
        </table>
        <div style="text-align: center;"><strong style="font-size: 25px" >FORMULIR</br>PENDATAAN PENDUDUK NON PERMANEN<br></strong></div>
        </br>
          </br></br></br></br>
        <table width="100%">
        <tbody>
        <tr>
        <td width="70%">
        <table width="100%">
        <tbody>
        <tr>
        <td>
        <tr>
        <tr>
        <td width="40%"><strong>I. IDENTITAS LOKASI</strong></td>
        <tr>
        <td>
        <tr>
        <td width="30%">Nama Provinsi</td>
        <td>:</td>
        <td>Jawa Barat</td>
        </tr>
        <tr>
        <td>Nama Kabupaten/Kota</td>
        <td>:</td>
        <td>Kota Bandung</td>
        </tr>
        <tr>
        <td>Nama Kecamatan</td>
        <td>:</td>
        <td>'.$request->kel_desc.'</td>
        </tr>
        <tr>
        <td>Nama Kelurahan</td>
        <td>:</td>
        <td>'.$request->kec_desc.'</td>
        </tr>
        <tr>
        <td>
        <tr>
        <tr>
        <td>
        <tr>
        <tr>
        <td>
        <tr>
        <td width="30%"><strong>II. IDENTITAS PENDUDUK</strong></td>
        <tr>
        <td>
        <tr>
        <td width="30%">NIK</td>
        <td>:</td>
        <td>'.$request->nik.'</td>
        </tr>
        <tr>
        <td>Nama Lengkap</td>
        <td>:</td>
        <td>'.$request->nama_lgkp.'</td>
        </tr>
        <tr>
        <td>Tempat dan Tanggal Lahir</td>
        <td>:</td>
        <td>'.$request->tmpt_lhr.','.$request->tgl_lhr.'</td>
        </tr>
        <tr>
        <td>Provinsi Daerah Asal</td>
        <td>:</td>
        <td>'.$request->src_prov_desc.'</td>
        </tr>
        <tr>
        <td>Kabupaten/Kota Daerah Asal</td>
        <td>:</td>
        <td>'.$request->src_kab_desc.'</td>
        </tr>
        <tr>
        <td>Kecamatan Daerah Asal</td>
        <td>:</td>
        <td>'.$request->src_kec_desc.'</td>
        </tr>
        <tr>
        <td>Kelurahan Daerah Asal</td>
        <td>:</td>
        <td>'.$request->src_kel_desc.'</td>
        </tr>
        <tr>
        <td>Alamat Daerah Asal</td>
        <td>:</td>
        <td>'.$request->src_alamat.'</td>
        </tr>
        <tr>
        <td width="30%">Jenis Kelamin</td>
        <td>:</td>
        <td>'.$this->get_detail_wni(801,$request->jenis_klmin).'</td>
        </tr>
        <tr>
        <td>Pendidikan</td>
        <td>:</td>
        <td>'.$this->get_detail_wni(101, $request->pendidikan).'</td>
        </tr>
        <tr>
        <td>Pekerjaan</td>
        <td>:</td>
        <td>'.$this->get_detail_wni(201, $request->pekerjaan).'</td>
        </tr>
        <tr>
        <td>Tanggal Kedatangan</td>
        <td>:</td>
        <td>'.$request->tgl_datang.'</td>
        </tr>          
        <tr>
        <td width="30%">Alasan Domisili Sementara</td>
        <td>:</td>
        <td>'.$this->get_detail_wni(901, $request->alasan_pindah).'</td>
        </tr>
        <tr>
        <td>Alamat Tinggal Sementara</td>
        <td>:</td>
        <td>'.$request->alamat.'</td>
        </tr>
        <tr>
        <td>Tinggal Sementara Sampai</td>
        <td>:</td>
        <td>'.Carbon::now()->addYears(1).'</td>
        </tr>
        </td>
        <td></td>
        </tr>
        </tbody>
        </table>
        </td>
        <td width="50%">

        </td>
        </tr>
        </tbody>
        </table>
        <div style="text-align: left;"><strong style="font-size: 10px" >FORMULIR ELEKTRONIK INI ADALAH DOKUMEN YANG DIPERSAMAKAN DENGAN FORMULIR NON ELEKTRONIK DAN MERUPAKAN BUKTI PENGAJUAN YANG SAH DARI PEMOHON TANPA DIBUBUHI TANDA TANGAN.</strong></div>
        </br></br></br></br></br></br></br>
        <table width="100%">
        <tbody>
        <tr>
        <td></td>
        <td style="text-align: center;" width="40%"><br><br><br>Bandung, '.Carbon::now()->format('d-m-Y').'<br>
        Pelapor<br><br>('.$request->nama_lgkp.')
        </td>
        </tr>
        <tr>
        <td colspan="3"></td>
        </tr>
        </tbody>
        </table>
        </body>
        </html>
        ';
        $tgl = Carbon::now()->format('d-m-Y-His');
        $url = $destinationPath.'formulir-'.$tgl.'.pdf';
        Browsershot::html($html)
        ->fullPage()
        ->showBackground()
        ->waitUntilNetworkIdle()
        ->format('A4')
        ->save($url);

      
        
        $pengajuan->img_15=$imgName."formulir-".$tgl.".pdf";

      }

      /////////////////
      if($request->hasFile('imgupload1')){
        $photo=$request->file('imgupload1')->getClientOriginalName();
        $extension = $request->file('imgupload1')->getClientOriginalExtension();
        $filename = "imgupload1.".$extension;
        $request->file('imgupload1')->move($destinationPath,$filename);
        $pengajuan->img_1=$imgName.$filename;

        $img1 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_1.jpg";
        $img1->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_1=$imgThumbName.$filename;
      }

      /////////////////
      if($request->hasFile('imgupload2')){
        $photo=$request->file('imgupload2')->getClientOriginalName();
        $extension = $request->file('imgupload2')->getClientOriginalExtension();
        $filename = "imgupload2.".$extension;
        $request->file('imgupload2')->move($destinationPath,$filename);
        $pengajuan->img_2=$imgName.$filename;

        $img2 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_2.jpg";
        $img2->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_2=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload3')){
        $photo=$request->file('imgupload3')->getClientOriginalName();
        $extension = $request->file('imgupload3')->getClientOriginalExtension();
        $filename = "imgupload3.".$extension;
        $request->file('imgupload3')->move($destinationPath,$filename);
        $pengajuan->img_3=$imgName.$filename;

        $img3 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_3.jpg";
        $img3->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_3=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload4')){
        $photo=$request->file('imgupload4')->getClientOriginalName();
        $extension = $request->file('imgupload4')->getClientOriginalExtension();
        $filename = "imgupload4.".$extension;
        $request->file('imgupload4')->move($destinationPath,$filename);
        $pengajuan->img_4=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_4.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_4=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload5')){
        $photo=$request->file('imgupload5')->getClientOriginalName();
        $extension = $request->file('imgupload5')->getClientOriginalExtension();
        $filename = "imgupload5.".$extension;
        $request->file('imgupload5')->move($destinationPath,$filename);
        $pengajuan->img_5=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_5.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_5=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload6')){
        $photo=$request->file('imgupload6')->getClientOriginalName();
        $extension = $request->file('imgupload6')->getClientOriginalExtension();
        $filename = "imgupload6.".$extension;
        $request->file('imgupload6')->move($destinationPath,$filename);
        $pengajuan->img_6=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_6.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_6=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload7')){
        $photo=$request->file('imgupload7')->getClientOriginalName();
        $extension = $request->file('imgupload7')->getClientOriginalExtension();
        $filename = "imgupload7.".$extension;
        $request->file('imgupload7')->move($destinationPath,$filename);
        $pengajuan->img_7=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_7.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_7=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload8')){
        $photo=$request->file('imgupload8')->getClientOriginalName();
        $extension = $request->file('imgupload8')->getClientOriginalExtension();
        $filename = "imgupload8.".$extension;
        $request->file('imgupload8')->move($destinationPath,$filename);
        $pengajuan->img_8=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_8.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_8=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload9')){
        $photo=$request->file('imgupload9')->getClientOriginalName();
        $extension = $request->file('imgupload9')->getClientOriginalExtension();
        $filename = "imgupload9.".$extension;
        $request->file('imgupload9')->move($destinationPath,$filename);
        $pengajuan->img_9=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_9.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_9=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload10')){
        $photo=$request->file('imgupload10')->getClientOriginalName();
        $extension = $request->file('imgupload10')->getClientOriginalExtension();
        $filename = "imgupload10.".$extension;
        $request->file('imgupload10')->move($destinationPath,$filename);
        $pengajuan->img_10=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_10.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_10=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload11')){
        $photo=$request->file('imgupload11')->getClientOriginalName();
        $extension = $request->file('imgupload11')->getClientOriginalExtension();
        $filename = "imgupload11.".$extension;
        $request->file('imgupload11')->move($destinationPath,$filename);
        $pengajuan->img_11=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_11.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_11=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload12')){
        $photo=$request->file('imgupload12')->getClientOriginalName();
        $extension = $request->file('imgupload12')->getClientOriginalExtension();
        $filename = "imgupload12.".$extension;
        $request->file('imgupload12')->move($destinationPath,$filename);
        $pengajuan->img_12=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_12.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_12=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload13')){
        $photo=$request->file('imgupload13')->getClientOriginalName();
        $extension = $request->file('imgupload13')->getClientOriginalExtension();
        $filename = "imgupload13.".$extension;
        $request->file('imgupload13')->move($destinationPath,$filename);
        $pengajuan->img_13=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_13.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_13=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload14')){
        $photo=$request->file('imgupload14')->getClientOriginalName();
        $extension = $request->file('imgupload14')->getClientOriginalExtension();
        $filename = "imgupload14.".$extension;
        $request->file('imgupload14')->move($destinationPath,$filename);
        $pengajuan->img_14=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_14.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_14=$imgThumbName.$filename;
      }
      /////////////////
      // if($request->hasFile('imgupload15')){
      //   $photo=$request->file('imgupload15')->getClientOriginalName();
      //   $extension = $request->file('imgupload15')->getClientOriginalExtension();
      //   $filename = "imgupload15.".$extension;
      //   $request->file('imgupload15')->move($destinationPath,$filename);
      //   $pengajuan->img_15=$imgName.$filename;

      //   $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
      //   $filename = "imgupload_thumb_15.jpg";
      //   $img4->save($destinationPathThumb.$filename);
      //   $pengajuan->img_thumb_15=$imgThumbName.$filename;
      // }

      $pengajuan->save();

      $msg = 'Pengajuan anda masih dalam tahap menunggu validasi';
      $ntf = 'Pemberitahuan';
      $jenis = 'PENGAJUAN';
      $pengajuan_id = null;
      $from_user_id = 'DISDUKCAPIL KOTA BANDUNG';
      AndrApi::sendnotification($jenis,$pengajuan_id,$jenis_layanan,$nik,$msg,$ntf,$from_user_id);
      return response()->json([
        'status' => 'success',
        'data' => 'Pendaftaran berhasil'
      ]);
    }
  }

  public function store_array(Request $request) {
    // // echo 1;
    // $arrayPengajuan = $request->biodata;
    // print_r($arrayPengajuan);
    // // print_r($request->biodata[0]['nik']);
    // // echo $request->biodata[0]->nik;
    // die;

    try {
      // DB::transaction(function() use ($request) {
      // $arrayPengajuan = $request->biodata;
      $arrayPengajuan = json_decode($request->biodata, true);

      // $nik= $request->biodata[0]['nik'];
      $nik = '6789456123321654';
      $jenis_layanan=$request->jenis_layanan;
      // $jenis_layanan = '2';
      $is_done = 0;

      $query=DB::connection('webpunten')
      ->select("SELECT COUNT(1) JUMLAH FROM PENGAJUANSKTS WHERE NIK = ".$nik." AND JENIS_LAYANAN = 1 ");
      if($query[0]->jumlah == 1){
        $is_done = 1;
        return response()->json([
          'status' => 'error',
          'data' => 'NIK sudah pernah mengajukan sebelumnya'
        ]);
      }

      $query=DB::connection('webpunten')
      ->select("SELECT COUNT(1) JUMLAH FROM PENGAJUANDATANG WHERE NIK = ".$nik." AND JENIS_LAYANAN = 2 ");
      if($query[0]->jumlah == 1){
        $is_done = 1;
        return response()->json([
          'status' => 'error',
          'data' => 'NIK sudah pernah mengajukan sebelumnya'
        ]);
      }

      $query=DB::connection('webpunten')
      ->select("SELECT COUNT(1) JUMLAH FROM PENGAJUANWNA WHERE NIK = ".$nik." AND JENIS_LAYANAN = 3 ");
      if($query[0]->jumlah == 1){
        $is_done = 1;
        return response()->json([
          'status' => 'error',
          'data' => 'NIK sudah pernah mengajukan sebelumnya'
        ]);
      }

      if($is_done == 1){
        return response()->json([
          'status' => 'error',
          'data' => 'NIK sudah pernah mengajukan sebelumnya'
        ]);
      }else{
        if ($jenis_layanan == 1){
          $destinationPath=$this->pengajuan_path().'/pengajuan/skts/'.$nik.'/ori/';
          $destinationPathThumb=$this->pengajuan_path().'/pengajuan/skts/'.$nik.'/thumb/';
          $imgName = 'pengajuan/skts/'.$nik.'/ori/';
          $imgThumbName = 'pengajuan/skts/'.$nik.'/thumb/';
        }else if($jenis_layanan == 2){
          $destinationPath=$this->pengajuan_path().'/pengajuan/datang/'.$nik.'/ori/';
          $destinationPathThumb=$this->pengajuan_path().'/pengajuan/datang/'.$nik.'/thumb/';
          $imgName = 'pengajuan/datang/'.$nik.'/ori/';
          $imgThumbName = 'pengajuan/datang/'.$nik.'/thumb/';
        }else if($jenis_layanan == 3){
          $destinationPath=$this->pengajuan_path().'/pengajuan/wna/'.$nik.'/ori/';
          $destinationPathThumb=$this->pengajuan_path().'/pengajuan/wna/'.$nik.'/thumb/';
          $imgName = 'pengajuan/wna/'.$nik.'/ori/';
          $imgThumbName = 'pengajuan/wna/'.$nik.'/thumb/';
        }else{
          return response()->json([
            'status' => 'error',
            'data' => 'Jenis Pengajuan Salah Mohon Cek Kemabali'
          ]);
        }
        // $picname = "imgupload_thumb_1.jpg";
        // if (!file_exists($destinationPath)) {
        //   mkdir($destinationPath, 0777, true);
        // }
        // if (!file_exists($destinationPathThumb)) {
        //   mkdir($destinationPathThumb, 0777, true);
        // }





        // return count($arrayPengajuan);
        for($i=0; $i < count($arrayPengajuan); $i++) {
          // echo $arrayPengajuan[$i]['nik'];
          // die;
          $pengajuanDetail = new Pengajuandatang;
          $pengajuanDetail->no_kk = $request->no_kk;
          $pengajuanDetail->proc_stat = 1;
          $pengajuanDetail->nik = $arrayPengajuan[$i]['nik'];
          $pengajuanDetail->nama_lgkp = $arrayPengajuan[$i]['nama_lgkp'];
          $pengajuanDetail->jenis_klmin = $arrayPengajuan[$i]['jenis_klmin'];
          $pengajuanDetail->tmpt_lhr = $arrayPengajuan[$i]['tmpt_lhr'];
          $pengajuanDetail->tgl_lhr = $arrayPengajuan[$i]['tgl_lhr'];
          $pengajuanDetail->agama = $arrayPengajuan[$i]['agama'];
          $pengajuanDetail->stat_kwn = $arrayPengajuan[$i]['stat_kwn'];
          $pengajuanDetail->gol_drh = $arrayPengajuan[$i]['gol_drh'];
          $pengajuanDetail->pendidikan = $arrayPengajuan[$i]['pendidikan'];
          $pengajuanDetail->pekerjaan = $arrayPengajuan[$i]['pekerjaan'];
          $pengajuanDetail->prop = $request->prop;
          $pengajuanDetail->kab = $request->kab;
          $pengajuanDetail->src_prov = $request->src_prov;
          $pengajuanDetail->src_kab =$request->src_kab;
          $pengajuanDetail->src_kec = $request->src_kec;
          $pengajuanDetail->src_kel = $request->src_kel;
          $pengajuanDetail->src_alamat = $request->src_alamat;
          $pengajuanDetail->src_rt = $request->src_rt;
          $pengajuanDetail->src_rw = $request->src_rw;
          $pengajuanDetail->kec = $request->kec;
          $pengajuanDetail->kel = $request->kel;
          $pengajuanDetail->alamat = $request->alamat;
          $pengajuanDetail->no_rw = $request->no_rw;
          $pengajuanDetail->no_rt = $request->no_rt;
          $pengajuanDetail->stat_hbkel = $arrayPengajuan[$i]['stat_hbkel'];
          // $pengajuanDetail->alasan_pindah = $arrayPengajuan[$i]['alasan_pindah'];
          // // $pengajuanDetail->jangka_waktu = $arrayPengajuan[$i]['jangka_waktu'];
          // // $pengajuanDetail->jum_anggota = $arrayPengajuan[$i]['jum_anggota'];
          // // $pengajuanDetail->tgl_datang = $arrayPengajuan[$i]['tgl_datang'];
          $pengajuanDetail->status = $request->status;
          $pengajuanDetail->jenis_layanan = $request->jenis_layanan;
          $pengajuanDetail->akta_lhr = $arrayPengajuan[$i]['akta_lhr'];
          $pengajuanDetail->no_akta_lhr = $arrayPengajuan[$i]['no_akta_lhr'];
          $pengajuanDetail->no_skpwni = $request->no_skpwni;
          $pengajuanDetail->tgl_berlaku= Carbon::now()->addMonths(6);
          //echo $pengajuanDetail;
          $pengajuanDetail->save();
        }


        // if ($jenis_layanan == 1){
        //   $pengajuan = new Pengajuanskts;
        // }else if($jenis_layanan == 2){
        //   $pengajuan = new Pengajuandatang;
        // }else if($jenis_layanan == 3){
        //   $pengajuan = new Pengajuanwna;
        // }else{
        //   return response()->json([
        //     'status' => 'error',
        //     'data' => 'Jenis Pengajuan Salah Mohon Cek Kemabali'
        //   ]);
        // }
        // if($request->hasFile('imgupload1')){
        //   $photo=$request->file('imgupload1')->getClientOriginalName();
        //   $extension = $request->file('imgupload1')->getClientOriginalExtension();
        //   $filename = "imgupload1.".$extension;
        //   $request->file('imgupload1')->move($destinationPath,$filename);
        //   $pengajuan->img_1=$imgName.$filename;
        //
        //   $img1 = Image::make($destinationPath.$filename)->resize(500, 500);
        //   $filename = "imgupload_thumb_1.jpg";
        //   $img1->save($destinationPathThumb.$filename);
        //   $pengajuan->img_thumb_1=$imgThumbName.$filename;
        // }
        //
        // /////////////////
        // if($request->hasFile('imgupload2')){
        //   $photo=$request->file('imgupload2')->getClientOriginalName();
        //   $extension = $request->file('imgupload2')->getClientOriginalExtension();
        //   $filename = "imgupload2.".$extension;
        //   $request->file('imgupload2')->move($destinationPath,$filename);
        //   $pengajuan->img_2=$imgName.$filename;
        //
        //   $img2 = Image::make($destinationPath.$filename)->resize(500, 500);
        //   $filename = "imgupload_thumb_2.jpg";
        //   $img2->save($destinationPathThumb.$filename);
        //   $pengajuan->img_thumb_2=$imgThumbName.$filename;
        // }
        // /////////////////
        // if($request->hasFile('imgupload3')){
        //   $photo=$request->file('imgupload3')->getClientOriginalName();
        //   $extension = $request->file('imgupload3')->getClientOriginalExtension();
        //   $filename = "imgupload3.".$extension;
        //   $request->file('imgupload3')->move($destinationPath,$filename);
        //   $pengajuan->img_3=$imgName.$filename;
        //
        //   $img3 = Image::make($destinationPath.$filename)->resize(500, 500);
        //   $filename = "imgupload_thumb_3.jpg";
        //   $img3->save($destinationPathThumb.$filename);
        //   $pengajuan->img_thumb_3=$imgThumbName.$filename;
        // }
        // /////////////////
        // if($request->hasFile('imgupload4')){
        //   $photo=$request->file('imgupload4')->getClientOriginalName();
        //   $extension = $request->file('imgupload4')->getClientOriginalExtension();
        //   $filename = "imgupload4.".$extension;
        //   $request->file('imgupload4')->move($destinationPath,$filename);
        //   $pengajuan->img_4=$imgName.$filename;
        //
        //   $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        //   $filename = "imgupload_thumb_4.jpg";
        //   $img4->save($destinationPathThumb.$filename);
        //   $pengajuan->img_thumb_4=$imgThumbName.$filename;
        // }
        // /////////////////
        // if($request->hasFile('imgupload5')){
        //   $photo=$request->file('imgupload5')->getClientOriginalName();
        //   $extension = $request->file('imgupload5')->getClientOriginalExtension();
        //   $filename = "imgupload5.".$extension;
        //   $request->file('imgupload5')->move($destinationPath,$filename);
        //   $pengajuan->img_5=$imgName.$filename;
        //
        //   $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        //   $filename = "imgupload_thumb_5.jpg";
        //   $img4->save($destinationPathThumb.$filename);
        //   $pengajuan->img_thumb_5=$imgThumbName.$filename;
        // }
        // /////////////////
        // if($request->hasFile('imgupload6')){
        //   $photo=$request->file('imgupload6')->getClientOriginalName();
        //   $extension = $request->file('imgupload6')->getClientOriginalExtension();
        //   $filename = "imgupload6.".$extension;
        //   $request->file('imgupload6')->move($destinationPath,$filename);
        //   $pengajuan->img_6=$imgName.$filename;
        //
        //   $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        //   $filename = "imgupload_thumb_6.jpg";
        //   $img4->save($destinationPathThumb.$filename);
        //   $pengajuan->img_thumb_6=$imgThumbName.$filename;
        // }
        // /////////////////
        // if($request->hasFile('imgupload7')){
        //   $photo=$request->file('imgupload7')->getClientOriginalName();
        //   $extension = $request->file('imgupload7')->getClientOriginalExtension();
        //   $filename = "imgupload7.".$extension;
        //   $request->file('imgupload7')->move($destinationPath,$filename);
        //   $pengajuan->img_7=$imgName.$filename;
        //
        //   $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        //   $filename = "imgupload_thumb_7.jpg";
        //   $img4->save($destinationPathThumb.$filename);
        //   $pengajuan->img_thumb_7=$imgThumbName.$filename;
        // }
        // /////////////////
        // if($request->hasFile('imgupload8')){
        //   $photo=$request->file('imgupload8')->getClientOriginalName();
        //   $extension = $request->file('imgupload8')->getClientOriginalExtension();
        //   $filename = "imgupload8.".$extension;
        //   $request->file('imgupload8')->move($destinationPath,$filename);
        //   $pengajuan->img_8=$imgName.$filename;
        //
        //   $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        //   $filename = "imgupload_thumb_8.jpg";
        //   $img4->save($destinationPathThumb.$filename);
        //   $pengajuan->img_thumb_8=$imgThumbName.$filename;
        // }
        // /////////////////
        // if($request->hasFile('imgupload9')){
        //   $photo=$request->file('imgupload9')->getClientOriginalName();
        //   $extension = $request->file('imgupload9')->getClientOriginalExtension();
        //   $filename = "imgupload9.".$extension;
        //   $request->file('imgupload9')->move($destinationPath,$filename);
        //   $pengajuan->img_9=$imgName.$filename;
        //
        //   $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        //   $filename = "imgupload_thumb_9.jpg";
        //   $img4->save($destinationPathThumb.$filename);
        //   $pengajuan->img_thumb_9=$imgThumbName.$filename;
        // }
        // /////////////////
        // if($request->hasFile('imgupload10')){
        //   $photo=$request->file('imgupload10')->getClientOriginalName();
        //   $extension = $request->file('imgupload10')->getClientOriginalExtension();
        //   $filename = "imgupload10.".$extension;
        //   $request->file('imgupload10')->move($destinationPath,$filename);
        //   $pengajuan->img_10=$imgName.$filename;
        //
        //   $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        //   $filename = "imgupload_thumb_10.jpg";
        //   $img4->save($destinationPathThumb.$filename);
        //   $pengajuan->img_thumb_10=$imgThumbName.$filename;
        // }
        // /////////////////
        // if($request->hasFile('imgupload11')){
        //   $photo=$request->file('imgupload11')->getClientOriginalName();
        //   $extension = $request->file('imgupload11')->getClientOriginalExtension();
        //   $filename = "imgupload11.".$extension;
        //   $request->file('imgupload11')->move($destinationPath,$filename);
        //   $pengajuan->img_11=$imgName.$filename;
        //
        //   $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        //   $filename = "imgupload_thumb_11.jpg";
        //   $img4->save($destinationPathThumb.$filename);
        //   $pengajuan->img_thumb_11=$imgThumbName.$filename;
        // }
        // /////////////////
        // if($request->hasFile('imgupload12')){
        //   $photo=$request->file('imgupload12')->getClientOriginalName();
        //   $extension = $request->file('imgupload12')->getClientOriginalExtension();
        //   $filename = "imgupload12.".$extension;
        //   $request->file('imgupload12')->move($destinationPath,$filename);
        //   $pengajuan->img_12=$imgName.$filename;
        //
        //   $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        //   $filename = "imgupload_thumb_12.jpg";
        //   $img4->save($destinationPathThumb.$filename);
        //   $pengajuan->img_thumb_12=$imgThumbName.$filename;
        // }
        // /////////////////
        // if($request->hasFile('imgupload13')){
        //   $photo=$request->file('imgupload13')->getClientOriginalName();
        //   $extension = $request->file('imgupload13')->getClientOriginalExtension();
        //   $filename = "imgupload13.".$extension;
        //   $request->file('imgupload13')->move($destinationPath,$filename);
        //   $pengajuan->img_13=$imgName.$filename;
        //
        //   $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        //   $filename = "imgupload_thumb_13.jpg";
        //   $img4->save($destinationPathThumb.$filename);
        //   $pengajuan->img_thumb_13=$imgThumbName.$filename;
        // }
        // /////////////////
        // if($request->hasFile('imgupload14')){
        //   $photo=$request->file('imgupload14')->getClientOriginalName();
        //   $extension = $request->file('imgupload14')->getClientOriginalExtension();
        //   $filename = "imgupload14.".$extension;
        //   $request->file('imgupload14')->move($destinationPath,$filename);
        //   $pengajuan->img_14=$imgName.$filename;
        //
        //   $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        //   $filename = "imgupload_thumb_14.jpg";
        //   $img4->save($destinationPathThumb.$filename);
        //   $pengajuan->img_thumb_14=$imgThumbName.$filename;
        // }
        // /////////////////
        // if($request->hasFile('imgupload15')){
        //   $photo=$request->file('imgupload15')->getClientOriginalName();
        //   $extension = $request->file('imgupload15')->getClientOriginalExtension();
        //   $filename = "imgupload15.".$extension;
        //   $request->file('imgupload15')->move($destinationPath,$filename);
        //   $pengajuan->img_15=$imgName.$filename;
        //
        //   $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        //   $filename = "imgupload_thumb_15.jpg";
        //   $img4->save($destinationPathThumb.$filename);
        //   $pengajuan->img_thumb_15=$imgThumbName.$filename;
        // }
        //
        // $pengajuan->save();
      $msg = 'Pengajuan anda masih dalam tahap menunggu validasi';
      $ntf = 'Pemberitahuan';
      $jenis = 'PENGAJUAN';
      $pengajuan_id = null;
      $from_user_id = 'DISDUKCAPIL KOTA BANDUNG';
      AndrApi::sendnotification($jenis,$pengajuan_id,$jenis_layanan,$nik,$msg,$ntf,$from_user_id);
        
        return response()->json([
          'status' => 'success',
          'data' => 'Pendaftaran berhasil'
        ]);
      }
      return response()->json(
        [
          'success' => [
            'status' => 200,
            'message' => 'success',
          ]
        ], 200
      );
      // });
      //   $pengajuanDetail->save();
      //   return response()->json(
      //     [
      //       'success' => [
      //         'status' => 200,
      //         'message' => 'success',
      //       ]
      //     ], 200
      //   );
      // });




    } catch (\Exception $e) {
      $message = $e->getMessage()." at ".$e->getFile()." line ".$e->getLine();
      Log::error($message);
      return response()->json(
        [
          'status' => 500,
          'message' => $e->getMessage(),
        ], 500
      );
    }
  }

  public function re_store(Request $request)
  {

    $timeUpdate = Carbon::now()->format('YmdHis');
    $nik= $request->nik;
    $jenis_layanan=$request->jenis_layanan;
      if ($jenis_layanan == 1){
        $query=DB::connection('webpunten')
        ->select("SELECT COUNT(1) JUMLAH FROM PENGAJUANSKTS WHERE NIK = ".$nik." AND PROC_STAT = 2 AND STAT_BERKAS IS not NULL");
      }else if ($jenis_layanan == 2){
         $query=DB::connection('webpunten')
         ->select("SELECT COUNT(1) JUMLAH FROM PENGAJUANDATANG WHERE NIK = ".$nik." AND PROC_STAT = 2 AND STAT_BERKAS IS not NULL");
      }else{
        $query=DB::connection('webpunten')
        ->select("SELECT COUNT(1) JUMLAH FROM PENGAJUANWNA WHERE NIK = ".$nik." AND PROC_STAT = 2 AND STAT_BERKAS IS not NULL");
      }
      if($query[0]->jumlah == 0){
      return response()->json([
        'status' => 'error',
        'data' => 'NIK belum pernah mengajukan sebelumnya, Atau Sudah Melakukan Perbaikan. Silahkan tunggu proses verifikasi kembali oleh verifikator.'
      ]);
    }else{
      if ($jenis_layanan == 1){
        $destinationPath=$this->pengajuan_path().'/pengajuan/skts/'.$nik.'/ori/';
        $destinationPathThumb=$this->pengajuan_path().'/pengajuan/skts/'.$nik.'/thumb/';
        $imgName = 'pengajuan/skts/'.$nik.'/ori/';
        $imgThumbName = 'pengajuan/skts/'.$nik.'/thumb/';
      }else if($jenis_layanan == 2){
        $destinationPath=$this->pengajuan_path().'/pengajuan/datang/'.$nik.'/ori/';
        $destinationPathThumb=$this->pengajuan_path().'/pengajuan/datang/'.$nik.'/thumb/';
        $imgName = 'pengajuan/datang/'.$nik.'/ori/';
        $imgThumbName = 'pengajuan/datang/'.$nik.'/thumb/';
      }else if($jenis_layanan == 3){
        $destinationPath=$this->pengajuan_path().'/pengajuan/wna/'.$nik.'/ori/';
        $destinationPathThumb=$this->pengajuan_path().'/pengajuan/wna/'.$nik.'/thumb/';
        $imgName = 'pengajuan/wna/'.$nik.'/ori/';
        $imgThumbName = 'pengajuan/wna/'.$nik.'/thumb/';
      }else{
        return response()->json([
          'status' => 'error',
          'data' => 'Jenis Pengajuan Salah Mohon Cek Kemabali'
        ]);
      }


      $picname = "imgupload_thumb_1.jpg";
      if (!file_exists($destinationPath)) {
        mkdir($destinationPath, 0777, true);
      }
      if (!file_exists($destinationPathThumb)) {
        mkdir($destinationPathThumb, 0777, true);
      }

      if ($jenis_layanan == 1){
        $pengajuan = Pengajuanskts::where('nik',$nik)->firstOrFail();
      }else if($jenis_layanan == 2){
        $pengajuan = Pengajuandatang::where('nik',$nik)->firstOrFail();
      }else if($jenis_layanan == 3){
        $pengajuan = Pengajuanwna::where('nik',$nik)->firstOrFail();
      }else{
        return response()->json([
          'status' => 'error',
          'data' => 'Jenis Pengajuan Salah Mohon Cek Kemabali'
        ]);
      }

      $pengajuan->proc_stat=2;
      $pengajuan->stat_berkas=null;
      $pengajuan->nik=$nik;
      $pengajuan->nama_lgkp=strtoupper($request->nama_lgkp);
      $pengajuan->jenis_klmin=strtoupper($request->jenis_klmin);
      $pengajuan->tmpt_lhr=strtoupper($request->tmpt_lhr);
      $pengajuan->tgl_lhr=strtoupper($request->tgl_lhr);
      $pengajuan->agama=strtoupper($request->agama);
      $pengajuan->stat_kwn=strtoupper($request->stat_kwn);
      $pengajuan->gol_drh=strtoupper($request->gol_drh);
      $pengajuan->pendidikan=strtoupper($request->pendidikan);
      $pengajuan->pekerjaan=strtoupper($request->pekerjaan);
      $pengajuan->src_prov=strtoupper($request->src_prov);
      $pengajuan->src_kab=strtoupper($request->src_kab);
      $pengajuan->src_kec=strtoupper($request->src_kec);
      $pengajuan->src_kel=strtoupper($request->src_kel);
      $pengajuan->src_alamat=strtoupper($request->src_alamat);
      $pengajuan->src_rt=strtoupper($request->src_rt);
      $pengajuan->src_rw=strtoupper($request->src_rw);
      $pengajuan->kec=strtoupper($request->kec);
      $pengajuan->kel=strtoupper($request->kel);
      $pengajuan->alamat=strtoupper($request->alamat);
      $pengajuan->no_rw=strtoupper($request->no_rw);
      $pengajuan->no_rt=strtoupper($request->no_rt);
      $pengajuan->no_kk=strtoupper($request->no_kk);
      $pengajuan->stat_hbkel=strtoupper($request->stat_hbkel);
      $pengajuan->alasan_pindah=strtoupper($request->alasan_pindah);
      $pengajuan->jangka_waktu=strtoupper($request->jangka_waktu);
      $pengajuan->jum_anggota=strtoupper($request->jum_anggota);
      $pengajuan->tgl_datang=strtoupper($request->tgl_datang);
      $pengajuan->status=strtoupper($request->status);
      $pengajuan->jenis_layanan=strtoupper($request->jenis_layanan);
      if ($request->jenis_layanan == 2){
        $pengajuan->akta_lhr=strtoupper($request->akta_lhr);
        $pengajuan->no_akta_lhr=strtoupper($request->no_akta_lhr);
        $pengajuan->no_skpwni=strtoupper($request->no_skpwni);
      }
      if ($request->jenis_layanan == 3){
        $pengajuan->kwrngrn=strtoupper($request->kwrngrn);
        $pengajuan->nama_fam=strtoupper($request->nama_fam);
        $pengajuan->tipe_spsor=strtoupper($request->tipe_spsor);
        $pengajuan->nama_spsor=strtoupper($request->nama_spsor);
        $pengajuan->no_paspor=strtoupper($request->no_paspor);
        $pengajuan->tgl_paspor=strtoupper($request->tgl_paspor);
        $pengajuan->tgl_akh_paspor=strtoupper($request->tgl_akh_paspor);
        $pengajuan->no_dok=strtoupper($request->no_doc);
        $pengajuan->dok_imgr=strtoupper($request->doc_imigrasi);
        $pengajuan->tgl_doc_imigrasi=strtoupper($request->tgl_doc_imigrasi);
        $pengajuan->tgl_stay=strtoupper($request->tgl_stay);
        $pengajuan->tgl_permit=strtoupper($request->tgl_permit);
      }

      $pengajuan->tgl_berlaku= Carbon::now()->addMonths(6);

      /////////////////
      if($request->hasFile('imgupload1')){
        $photo=$request->file('imgupload1')->getClientOriginalName();
        $extension = $request->file('imgupload1')->getClientOriginalExtension();
        $filename = "imgupload1-".$timeUpdate.".".$extension;
        $request->file('imgupload1')->move($destinationPath,$filename);
        $pengajuan->img_1=$imgName.$filename;

        $img1 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_1-".$timeUpdate.".".$extension;
        $img1->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_1=$imgThumbName.$filename;
      }

      /////////////////
      if($request->hasFile('imgupload2')){
        $photo=$request->file('imgupload2')->getClientOriginalName();
        $extension = $request->file('imgupload2')->getClientOriginalExtension();
        $filename = "imgupload2-".$timeUpdate.".".$extension;
        $request->file('imgupload2')->move($destinationPath,$filename);
        $pengajuan->img_2=$imgName.$filename;

        $img2 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_2-".$timeUpdate.".".$extension;
        $img2->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_2=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload3')){
        $photo=$request->file('imgupload3')->getClientOriginalName();
        $extension = $request->file('imgupload3')->getClientOriginalExtension();
        $filename = "imgupload3-".$timeUpdate.".".$extension;
        $request->file('imgupload3')->move($destinationPath,$filename);
        $pengajuan->img_3=$imgName.$filename;

        $img3 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_3-".$timeUpdate.".".$extension;
        $img3->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_3=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload4')){
        $photo=$request->file('imgupload4')->getClientOriginalName();
        $extension = $request->file('imgupload4')->getClientOriginalExtension();
        $filename = "imgupload4-".$timeUpdate.".".$extension;
        $request->file('imgupload4')->move($destinationPath,$filename);
        $pengajuan->img_4=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_4-".$timeUpdate.".".$extension;
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_4=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload5')){
        $photo=$request->file('imgupload5')->getClientOriginalName();
        $extension = $request->file('imgupload5')->getClientOriginalExtension();
        $filename = "imgupload5-".$timeUpdate.".".$extension;
        $request->file('imgupload5')->move($destinationPath,$filename);
        $pengajuan->img_5=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_5-".$timeUpdate.".".$extension;
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_5=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload6')){
        $photo=$request->file('imgupload6')->getClientOriginalName();
        $extension = $request->file('imgupload6')->getClientOriginalExtension();
        $filename = "imgupload6-".$timeUpdate.".".$extension;
        $request->file('imgupload6')->move($destinationPath,$filename);
        $pengajuan->img_6=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_6-".$timeUpdate.".".$extension;
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_6=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload7')){
        $photo=$request->file('imgupload7')->getClientOriginalName();
        $extension = $request->file('imgupload7')->getClientOriginalExtension();
        $filename = "imgupload7-".$timeUpdate.".".$extension;
        $request->file('imgupload7')->move($destinationPath,$filename);
        $pengajuan->img_7=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_7-".$timeUpdate.".".$extension;
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_7=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload8')){
        $photo=$request->file('imgupload8')->getClientOriginalName();
        $extension = $request->file('imgupload8')->getClientOriginalExtension();
        $filename = "imgupload8-".$timeUpdate.".".$extension;
        $request->file('imgupload8')->move($destinationPath,$filename);
        $pengajuan->img_8=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_8-".$timeUpdate.".".$extension;
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_8=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload9')){
        $photo=$request->file('imgupload9')->getClientOriginalName();
        $extension = $request->file('imgupload9')->getClientOriginalExtension();
        $filename = "imgupload9-".$timeUpdate.".".$extension;
        $request->file('imgupload9')->move($destinationPath,$filename);
        $pengajuan->img_9=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_9-".$timeUpdate.".".$extension;
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_9=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload10')){
        $photo=$request->file('imgupload10')->getClientOriginalName();
        $extension = $request->file('imgupload10')->getClientOriginalExtension();
        $filename = "imgupload10-".$timeUpdate.".".$extension;
        $request->file('imgupload10')->move($destinationPath,$filename);
        $pengajuan->img_10=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_10-".$timeUpdate.".".$extension;
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_10=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload11')){
        $photo=$request->file('imgupload11')->getClientOriginalName();
        $extension = $request->file('imgupload11')->getClientOriginalExtension();
        $filename = "imgupload11-".$timeUpdate.".".$extension;
        $request->file('imgupload11')->move($destinationPath,$filename);
        $pengajuan->img_11=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_11-".$timeUpdate.".".$extension;
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_11=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload12')){
        $photo=$request->file('imgupload12')->getClientOriginalName();
        $extension = $request->file('imgupload12')->getClientOriginalExtension();
        $filename = "imgupload12-".$timeUpdate.".".$extension;
        $request->file('imgupload12')->move($destinationPath,$filename);
        $pengajuan->img_12=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_12-".$timeUpdate.".".$extension;
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_12=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload13')){
        $photo=$request->file('imgupload13')->getClientOriginalName();
        $extension = $request->file('imgupload13')->getClientOriginalExtension();
        $filename = "imgupload13-".$timeUpdate.".".$extension;
        $request->file('imgupload13')->move($destinationPath,$filename);
        $pengajuan->img_13=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_13-".$timeUpdate.".".$extension;
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_13=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload14')){
        $photo=$request->file('imgupload14')->getClientOriginalName();
        $extension = $request->file('imgupload14')->getClientOriginalExtension();
        $filename = "imgupload14-".$timeUpdate.".".$extension;
        $request->file('imgupload14')->move($destinationPath,$filename);
        $pengajuan->img_14=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_14-".$timeUpdate.".".$extension;
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_14=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload15')){
        $photo=$request->file('imgupload15')->getClientOriginalName();
        $extension = $request->file('imgupload15')->getClientOriginalExtension();
        $filename = "imgupload15-".$timeUpdate.".".$extension;
        $request->file('imgupload15')->move($destinationPath,$filename);
        $pengajuan->img_15=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_15-".$timeUpdate.".".$extension;
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_15=$imgThumbName.$filename;
      }

      $pengajuan->update();
      return response()->json([
        'status' => 'success',
        'data' => 'Pendaftaran berhasil'
      ]);
    }
  }

  public function re_edit(Request $request)
  {

    $timeUpdate = Carbon::now()->format('YmdHis');
    $nik= $request->nik;
    $jenis_layanan=$request->jenis_layanan;
      if ($jenis_layanan == 1){
        $query=DB::connection('webpunten')
        ->select("SELECT COUNT(1) JUMLAH FROM PENGAJUANSKTS WHERE NIK = ".$nik."");
      }else if ($jenis_layanan == 2){
         $query=DB::connection('webpunten')
         ->select("SELECT COUNT(1) JUMLAH FROM PENGAJUANDATANG WHERE NIK = ".$nik."");
      }else{
        $query=DB::connection('webpunten')
        ->select("SELECT COUNT(1) JUMLAH FROM PENGAJUANWNA WHERE NIK = ".$nik."");
      }
      if($query[0]->jumlah == 0){
      return response()->json([
        'status' => 'error',
        'data' => 'NIK belum pernah mengajukan sebelumnya, Atau Sudah Melakukan Perbaikan. Silahkan tunggu proses verifikasi kembali oleh verifikator.'
      ]);
    }else{
      if ($jenis_layanan == 1){
        $destinationPath=$this->pengajuan_path().'/pengajuan/skts/'.$nik.'/ori/';
        $destinationPathThumb=$this->pengajuan_path().'/pengajuan/skts/'.$nik.'/thumb/';
        $imgName = 'pengajuan/skts/'.$nik.'/ori/';
        $imgThumbName = 'pengajuan/skts/'.$nik.'/thumb/';
      }else if($jenis_layanan == 2){
        $destinationPath=$this->pengajuan_path().'/pengajuan/datang/'.$nik.'/ori/';
        $destinationPathThumb=$this->pengajuan_path().'/pengajuan/datang/'.$nik.'/thumb/';
        $imgName = 'pengajuan/datang/'.$nik.'/ori/';
        $imgThumbName = 'pengajuan/datang/'.$nik.'/thumb/';
      }else if($jenis_layanan == 3){
        $destinationPath=$this->pengajuan_path().'/pengajuan/wna/'.$nik.'/ori/';
        $destinationPathThumb=$this->pengajuan_path().'/pengajuan/wna/'.$nik.'/thumb/';
        $imgName = 'pengajuan/wna/'.$nik.'/ori/';
        $imgThumbName = 'pengajuan/wna/'.$nik.'/thumb/';
      }else{
        return response()->json([
          'status' => 'error',
          'data' => 'Jenis Pengajuan Salah Mohon Cek Kemabali'
        ]);
      }


      $picname = "imgupload_thumb_1.jpg";
      if (!file_exists($destinationPath)) {
        mkdir($destinationPath, 0777, true);
      }
      if (!file_exists($destinationPathThumb)) {
        mkdir($destinationPathThumb, 0777, true);
      }

      if ($jenis_layanan == 1){
        $pengajuan = Pengajuanskts::where('nik',$nik)->firstOrFail();
      }else if($jenis_layanan == 2){
        $pengajuan = Pengajuandatang::where('nik',$nik)->firstOrFail();
      }else if($jenis_layanan == 3){
        $pengajuan = Pengajuanwna::where('nik',$nik)->firstOrFail();
      }else{
        return response()->json([
          'status' => 'error',
          'data' => 'Jenis Pengajuan Salah Mohon Cek Kemabali'
        ]);
      }

      $pengajuan->proc_stat=2;
      $pengajuan->stat_berkas=null;
      $pengajuan->nik=$nik;
      $pengajuan->nama_lgkp=$request->nama_lgkp;
      $pengajuan->jenis_klmin=$request->jenis_klmin;
      $pengajuan->tmpt_lhr=$request->tmpt_lhr;
      $pengajuan->tgl_lhr=$request->tgl_lhr;
      $pengajuan->agama=$request->agama;
      $pengajuan->stat_kwn=$request->stat_kwn;
      $pengajuan->gol_drh=$request->gol_drh;
      $pengajuan->pendidikan=$request->pendidikan;
      $pengajuan->pekerjaan=$request->pekerjaan;
      $pengajuan->src_prov=$request->src_prov;
      $pengajuan->src_kab=$request->src_kab;
      $pengajuan->src_kec=$request->src_kec;
      $pengajuan->src_kel=$request->src_kel;
      $pengajuan->src_alamat=$request->src_alamat;
      $pengajuan->src_rt=$request->src_rt;
      $pengajuan->src_rw=$request->src_rw;
      $pengajuan->prop=$request->prop;
      $pengajuan->kab=$request->kab;
      $pengajuan->kec=$request->kec;
      $pengajuan->kel=$request->kel;
      $pengajuan->alamat=$request->alamat;
      $pengajuan->no_rw=$request->no_rw;
      $pengajuan->no_rt=$request->no_rt;
      $pengajuan->no_kk=$request->no_kk;
      $pengajuan->stat_hbkel=$request->stat_hbkel;
      $pengajuan->alasan_pindah=$request->alasan_pindah;
      $pengajuan->jangka_waktu=$request->jangka_waktu;
      $pengajuan->jum_anggota=$request->jum_anggota;
      $pengajuan->tgl_datang=$request->tgl_datang;
      $pengajuan->status=$request->status;
      $pengajuan->jenis_layanan=$request->jenis_layanan;
      $pengajuan->verified_by=$request->verified_by;
      $pengajuan->verified_at=Carbon::now();
      if ($request->jenis_layanan == 2){
        $pengajuan->akta_lhr=$request->akta_lhr;
        $pengajuan->no_akta_lhr=$request->no_akta_lhr;
        $pengajuan->no_skpwni=$request->no_skpwni;
      }
      if ($request->jenis_layanan == 3){
        $pengajuan->kwrngrn=$request->kwrngrn;
        $pengajuan->nama_fam=$request->nama_fam;
        $pengajuan->tipe_spsor=$request->tipe_spsor;
        $pengajuan->nama_spsor=$request->nama_spsor;
        $pengajuan->no_paspor=$request->no_paspor;
        $pengajuan->tgl_paspor=$request->tgl_paspor;
        $pengajuan->tgl_akh_paspor=$request->tgl_akh_paspor;
        $pengajuan->no_dok=$request->no_doc;
        $pengajuan->dok_imgr=$request->doc_imigrasi;
        $pengajuan->tgl_doc_imigrasi=$request->tgl_doc_imigrasi;
        $pengajuan->tgl_stay=$request->tgl_stay;
        $pengajuan->tgl_permit=$request->tgl_permit;
      }

      $pengajuan->tgl_berlaku= Carbon::now()->addMonths(6);

      /////////////////
      if($request->hasFile('imgupload1')){
        $photo=$request->file('imgupload1')->getClientOriginalName();
        $extension = $request->file('imgupload1')->getClientOriginalExtension();
        $filename = "imgupload1-".$timeUpdate.".".$extension;
        $request->file('imgupload1')->move($destinationPath,$filename);
        $pengajuan->img_1=$imgName.$filename;

        $img1 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_1-".$timeUpdate.".".$extension;
        $img1->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_1=$imgThumbName.$filename;
      }

      /////////////////
      if($request->hasFile('imgupload2')){
        $photo=$request->file('imgupload2')->getClientOriginalName();
        $extension = $request->file('imgupload2')->getClientOriginalExtension();
        $filename = "imgupload2-".$timeUpdate.".".$extension;
        $request->file('imgupload2')->move($destinationPath,$filename);
        $pengajuan->img_2=$imgName.$filename;

        $img2 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_2-".$timeUpdate.".".$extension;
        $img2->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_2=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload3')){
        $photo=$request->file('imgupload3')->getClientOriginalName();
        $extension = $request->file('imgupload3')->getClientOriginalExtension();
        $filename = "imgupload3-".$timeUpdate.".".$extension;
        $request->file('imgupload3')->move($destinationPath,$filename);
        $pengajuan->img_3=$imgName.$filename;

        $img3 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_3-".$timeUpdate.".".$extension;
        $img3->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_3=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload4')){
        $photo=$request->file('imgupload4')->getClientOriginalName();
        $extension = $request->file('imgupload4')->getClientOriginalExtension();
        $filename = "imgupload4-".$timeUpdate.".".$extension;
        $request->file('imgupload4')->move($destinationPath,$filename);
        $pengajuan->img_4=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_4-".$timeUpdate.".".$extension;
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_4=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload5')){
        $photo=$request->file('imgupload5')->getClientOriginalName();
        $extension = $request->file('imgupload5')->getClientOriginalExtension();
        $filename = "imgupload5-".$timeUpdate.".".$extension;
        $request->file('imgupload5')->move($destinationPath,$filename);
        $pengajuan->img_5=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_5-".$timeUpdate.".".$extension;
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_5=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload6')){
        $photo=$request->file('imgupload6')->getClientOriginalName();
        $extension = $request->file('imgupload6')->getClientOriginalExtension();
        $filename = "imgupload6-".$timeUpdate.".".$extension;
        $request->file('imgupload6')->move($destinationPath,$filename);
        $pengajuan->img_6=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_6-".$timeUpdate.".".$extension;
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_6=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload7')){
        $photo=$request->file('imgupload7')->getClientOriginalName();
        $extension = $request->file('imgupload7')->getClientOriginalExtension();
        $filename = "imgupload7-".$timeUpdate.".".$extension;
        $request->file('imgupload7')->move($destinationPath,$filename);
        $pengajuan->img_7=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_7-".$timeUpdate.".".$extension;
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_7=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload8')){
        $photo=$request->file('imgupload8')->getClientOriginalName();
        $extension = $request->file('imgupload8')->getClientOriginalExtension();
        $filename = "imgupload8-".$timeUpdate.".".$extension;
        $request->file('imgupload8')->move($destinationPath,$filename);
        $pengajuan->img_8=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_8-".$timeUpdate.".".$extension;
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_8=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload9')){
        $photo=$request->file('imgupload9')->getClientOriginalName();
        $extension = $request->file('imgupload9')->getClientOriginalExtension();
        $filename = "imgupload9-".$timeUpdate.".".$extension;
        $request->file('imgupload9')->move($destinationPath,$filename);
        $pengajuan->img_9=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_9-".$timeUpdate.".".$extension;
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_9=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload10')){
        $photo=$request->file('imgupload10')->getClientOriginalName();
        $extension = $request->file('imgupload10')->getClientOriginalExtension();
        $filename = "imgupload10-".$timeUpdate.".".$extension;
        $request->file('imgupload10')->move($destinationPath,$filename);
        $pengajuan->img_10=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_10-".$timeUpdate.".".$extension;
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_10=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload11')){
        $photo=$request->file('imgupload11')->getClientOriginalName();
        $extension = $request->file('imgupload11')->getClientOriginalExtension();
        $filename = "imgupload11-".$timeUpdate.".".$extension;
        $request->file('imgupload11')->move($destinationPath,$filename);
        $pengajuan->img_11=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_11-".$timeUpdate.".".$extension;
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_11=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload12')){
        $photo=$request->file('imgupload12')->getClientOriginalName();
        $extension = $request->file('imgupload12')->getClientOriginalExtension();
        $filename = "imgupload12-".$timeUpdate.".".$extension;
        $request->file('imgupload12')->move($destinationPath,$filename);
        $pengajuan->img_12=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_12-".$timeUpdate.".".$extension;
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_12=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload13')){
        $photo=$request->file('imgupload13')->getClientOriginalName();
        $extension = $request->file('imgupload13')->getClientOriginalExtension();
        $filename = "imgupload13-".$timeUpdate.".".$extension;
        $request->file('imgupload13')->move($destinationPath,$filename);
        $pengajuan->img_13=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_13-".$timeUpdate.".".$extension;
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_13=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload14')){
        $photo=$request->file('imgupload14')->getClientOriginalName();
        $extension = $request->file('imgupload14')->getClientOriginalExtension();
        $filename = "imgupload14-".$timeUpdate.".".$extension;
        $request->file('imgupload14')->move($destinationPath,$filename);
        $pengajuan->img_14=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_14-".$timeUpdate.".".$extension;
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_14=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload15')){
        $photo=$request->file('imgupload15')->getClientOriginalName();
        $extension = $request->file('imgupload15')->getClientOriginalExtension();
        $filename = "imgupload15-".$timeUpdate.".".$extension;
        $request->file('imgupload15')->move($destinationPath,$filename);
        $pengajuan->img_15=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_15-".$timeUpdate.".".$extension;
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_15=$imgThumbName.$filename;
      }

      $pengajuan->update();
      $msg = 'Pengajuan anda anda telah lolos tahap verifikasi dan sedang di proses oleh operator';
      $ntf = 'Pemberitahuan';
      $jenis = 'PENGAJUAN';
      $pengajuan_id = null;
      $from_user_id = 'DISDUKCAPIL KOTA BANDUNG';
      AndrApi::sendnotification($jenis,$pengajuan_id,$jenis_layanan,$nik,$msg,$ntf,$from_user_id);
      return response()->json([
        'status' => 'success',
        'data' => 'Pendaftaran berhasil'
      ]);
    }
  }

  public function re_edit_admin(Request $request)
  {

    $timeUpdate = Carbon::now()->format('YmdHis');
    $nik= $request->nik;
    $jenis_layanan=$request->jenis_layanan;
      if ($jenis_layanan == 1){
        $query=DB::connection('webpunten')
        ->select("SELECT COUNT(1) JUMLAH FROM PENGAJUANSKTS WHERE NIK = ".$nik."");
      }else if ($jenis_layanan == 2){
         $query=DB::connection('webpunten')
         ->select("SELECT COUNT(1) JUMLAH FROM PENGAJUANDATANG WHERE NIK = ".$nik."");
      }else{
        $query=DB::connection('webpunten')
        ->select("SELECT COUNT(1) JUMLAH FROM PENGAJUANWNA WHERE NIK = ".$nik."");
      }
      if($query[0]->jumlah == 0){
      return response()->json([
        'status' => 'error',
        'data' => 'NIK belum pernah mengajukan sebelumnya, Atau Sudah Melakukan Perbaikan. Silahkan tunggu proses verifikasi kembali oleh verifikator.'
      ]);
    }else{
      if ($jenis_layanan == 1){
        $destinationPath=$this->pengajuan_path().'/pengajuan/skts/'.$nik.'/ori/';
        $destinationPathThumb=$this->pengajuan_path().'/pengajuan/skts/'.$nik.'/thumb/';
        $imgName = 'pengajuan/skts/'.$nik.'/ori/';
        $imgThumbName = 'pengajuan/skts/'.$nik.'/thumb/';
      }else if($jenis_layanan == 2){
        $destinationPath=$this->pengajuan_path().'/pengajuan/datang/'.$nik.'/ori/';
        $destinationPathThumb=$this->pengajuan_path().'/pengajuan/datang/'.$nik.'/thumb/';
        $imgName = 'pengajuan/datang/'.$nik.'/ori/';
        $imgThumbName = 'pengajuan/datang/'.$nik.'/thumb/';
      }else if($jenis_layanan == 3){
        $destinationPath=$this->pengajuan_path().'/pengajuan/wna/'.$nik.'/ori/';
        $destinationPathThumb=$this->pengajuan_path().'/pengajuan/wna/'.$nik.'/thumb/';
        $imgName = 'pengajuan/wna/'.$nik.'/ori/';
        $imgThumbName = 'pengajuan/wna/'.$nik.'/thumb/';
      }else{
        return response()->json([
          'status' => 'error',
          'data' => 'Jenis Pengajuan Salah Mohon Cek Kemabali'
        ]);
      }


      $picname = "imgupload_thumb_1.jpg";
      if (!file_exists($destinationPath)) {
        mkdir($destinationPath, 0777, true);
      }
      if (!file_exists($destinationPathThumb)) {
        mkdir($destinationPathThumb, 0777, true);
      }

      if ($jenis_layanan == 1){
        $pengajuan = Pengajuanskts::where('nik',$nik)->firstOrFail();
      }else if($jenis_layanan == 2){
        $pengajuan = Pengajuandatang::where('nik',$nik)->firstOrFail();
      }else if($jenis_layanan == 3){
        $pengajuan = Pengajuanwna::where('nik',$nik)->firstOrFail();
      }else{
        return response()->json([
          'status' => 'error',
          'data' => 'Jenis Pengajuan Salah Mohon Cek Kemabali'
        ]);
      }

      $pengajuan->proc_stat=3;
      $pengajuan->stat_berkas=null;
      $pengajuan->nik=$nik;
      $pengajuan->nama_lgkp=$request->nama_lgkp;
      $pengajuan->jenis_klmin=$request->jenis_klmin;
      $pengajuan->tmpt_lhr=$request->tmpt_lhr;
      $pengajuan->tgl_lhr=$request->tgl_lhr;
      $pengajuan->agama=$request->agama;
      $pengajuan->stat_kwn=$request->stat_kwn;
      $pengajuan->gol_drh=$request->gol_drh;
      $pengajuan->pendidikan=$request->pendidikan;
      $pengajuan->pekerjaan=$request->pekerjaan;
      $pengajuan->src_prov=$request->src_prov;
      $pengajuan->src_kab=$request->src_kab;
      $pengajuan->src_kec=$request->src_kec;
      $pengajuan->src_kel=$request->src_kel;
      $pengajuan->src_alamat=$request->src_alamat;
      $pengajuan->src_rt=$request->src_rt;
      $pengajuan->src_rw=$request->src_rw;
      $pengajuan->prop=$request->prop;
      $pengajuan->kab=$request->kab;
      $pengajuan->kec=$request->kec;
      $pengajuan->kel=$request->kel;
      $pengajuan->alamat=$request->alamat;
      $pengajuan->no_rw=$request->no_rw;
      $pengajuan->no_rt=$request->no_rt;
      $pengajuan->no_kk=$request->no_kk;
      $pengajuan->stat_hbkel=$request->stat_hbkel;
      $pengajuan->alasan_pindah=$request->alasan_pindah;
      $pengajuan->jangka_waktu=$request->jangka_waktu;
      $pengajuan->jum_anggota=$request->jum_anggota;
      $pengajuan->tgl_datang=$request->tgl_datang;
      $pengajuan->status=$request->status;
      $pengajuan->jenis_layanan=$request->jenis_layanan;
      $pengajuan->verified_by=$request->verified_by;
      $pengajuan->verified_at=Carbon::now();
      if ($request->jenis_layanan == 2){
        $pengajuan->akta_lhr=$request->akta_lhr;
        $pengajuan->no_akta_lhr=$request->no_akta_lhr;
        $pengajuan->no_skpwni=$request->no_skpwni;
      }
      if ($request->jenis_layanan == 3){
        $pengajuan->kwrngrn=$request->kwrngrn;
        $pengajuan->nama_fam=$request->nama_fam;
        $pengajuan->tipe_spsor=$request->tipe_spsor;
        $pengajuan->nama_spsor=$request->nama_spsor;
        $pengajuan->no_paspor=$request->no_paspor;
        $pengajuan->tgl_paspor=$request->tgl_paspor;
        $pengajuan->tgl_akh_paspor=$request->tgl_akh_paspor;
        $pengajuan->no_dok=$request->no_doc;
        $pengajuan->dok_imgr=$request->doc_imigrasi;
        $pengajuan->tgl_doc_imigrasi=$request->tgl_doc_imigrasi;
        $pengajuan->tgl_stay=$request->tgl_stay;
        $pengajuan->tgl_permit=$request->tgl_permit;
      }

      $pengajuan->tgl_berlaku= Carbon::now()->addMonths(6);

      /////////////////
      if($request->hasFile('imgupload1')){
        $photo=$request->file('imgupload1')->getClientOriginalName();
        $extension = $request->file('imgupload1')->getClientOriginalExtension();
        $filename = "imgupload1-".$timeUpdate.".".$extension;
        $request->file('imgupload1')->move($destinationPath,$filename);
        $pengajuan->img_1=$imgName.$filename;

        $img1 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_1-".$timeUpdate.".".$extension;
        $img1->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_1=$imgThumbName.$filename;
      }

      /////////////////
      if($request->hasFile('imgupload2')){
        $photo=$request->file('imgupload2')->getClientOriginalName();
        $extension = $request->file('imgupload2')->getClientOriginalExtension();
        $filename = "imgupload2-".$timeUpdate.".".$extension;
        $request->file('imgupload2')->move($destinationPath,$filename);
        $pengajuan->img_2=$imgName.$filename;

        $img2 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_2-".$timeUpdate.".".$extension;
        $img2->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_2=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload3')){
        $photo=$request->file('imgupload3')->getClientOriginalName();
        $extension = $request->file('imgupload3')->getClientOriginalExtension();
        $filename = "imgupload3-".$timeUpdate.".".$extension;
        $request->file('imgupload3')->move($destinationPath,$filename);
        $pengajuan->img_3=$imgName.$filename;

        $img3 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_3-".$timeUpdate.".".$extension;
        $img3->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_3=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload4')){
        $photo=$request->file('imgupload4')->getClientOriginalName();
        $extension = $request->file('imgupload4')->getClientOriginalExtension();
        $filename = "imgupload4-".$timeUpdate.".".$extension;
        $request->file('imgupload4')->move($destinationPath,$filename);
        $pengajuan->img_4=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_4-".$timeUpdate.".".$extension;
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_4=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload5')){
        $photo=$request->file('imgupload5')->getClientOriginalName();
        $extension = $request->file('imgupload5')->getClientOriginalExtension();
        $filename = "imgupload5-".$timeUpdate.".".$extension;
        $request->file('imgupload5')->move($destinationPath,$filename);
        $pengajuan->img_5=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_5-".$timeUpdate.".".$extension;
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_5=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload6')){
        $photo=$request->file('imgupload6')->getClientOriginalName();
        $extension = $request->file('imgupload6')->getClientOriginalExtension();
        $filename = "imgupload6-".$timeUpdate.".".$extension;
        $request->file('imgupload6')->move($destinationPath,$filename);
        $pengajuan->img_6=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_6-".$timeUpdate.".".$extension;
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_6=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload7')){
        $photo=$request->file('imgupload7')->getClientOriginalName();
        $extension = $request->file('imgupload7')->getClientOriginalExtension();
        $filename = "imgupload7-".$timeUpdate.".".$extension;
        $request->file('imgupload7')->move($destinationPath,$filename);
        $pengajuan->img_7=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_7-".$timeUpdate.".".$extension;
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_7=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload8')){
        $photo=$request->file('imgupload8')->getClientOriginalName();
        $extension = $request->file('imgupload8')->getClientOriginalExtension();
        $filename = "imgupload8-".$timeUpdate.".".$extension;
        $request->file('imgupload8')->move($destinationPath,$filename);
        $pengajuan->img_8=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_8-".$timeUpdate.".".$extension;
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_8=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload9')){
        $photo=$request->file('imgupload9')->getClientOriginalName();
        $extension = $request->file('imgupload9')->getClientOriginalExtension();
        $filename = "imgupload9-".$timeUpdate.".".$extension;
        $request->file('imgupload9')->move($destinationPath,$filename);
        $pengajuan->img_9=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_9-".$timeUpdate.".".$extension;
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_9=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload10')){
        $photo=$request->file('imgupload10')->getClientOriginalName();
        $extension = $request->file('imgupload10')->getClientOriginalExtension();
        $filename = "imgupload10-".$timeUpdate.".".$extension;
        $request->file('imgupload10')->move($destinationPath,$filename);
        $pengajuan->img_10=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_10-".$timeUpdate.".".$extension;
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_10=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload11')){
        $photo=$request->file('imgupload11')->getClientOriginalName();
        $extension = $request->file('imgupload11')->getClientOriginalExtension();
        $filename = "imgupload11-".$timeUpdate.".".$extension;
        $request->file('imgupload11')->move($destinationPath,$filename);
        $pengajuan->img_11=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_11-".$timeUpdate.".".$extension;
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_11=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload12')){
        $photo=$request->file('imgupload12')->getClientOriginalName();
        $extension = $request->file('imgupload12')->getClientOriginalExtension();
        $filename = "imgupload12-".$timeUpdate.".".$extension;
        $request->file('imgupload12')->move($destinationPath,$filename);
        $pengajuan->img_12=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_12-".$timeUpdate.".".$extension;
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_12=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload13')){
        $photo=$request->file('imgupload13')->getClientOriginalName();
        $extension = $request->file('imgupload13')->getClientOriginalExtension();
        $filename = "imgupload13-".$timeUpdate.".".$extension;
        $request->file('imgupload13')->move($destinationPath,$filename);
        $pengajuan->img_13=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_13-".$timeUpdate.".".$extension;
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_13=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload14')){
        $photo=$request->file('imgupload14')->getClientOriginalName();
        $extension = $request->file('imgupload14')->getClientOriginalExtension();
        $filename = "imgupload14-".$timeUpdate.".".$extension;
        $request->file('imgupload14')->move($destinationPath,$filename);
        $pengajuan->img_14=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_14-".$timeUpdate.".".$extension;
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_14=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload15')){
        $photo=$request->file('imgupload15')->getClientOriginalName();
        $extension = $request->file('imgupload15')->getClientOriginalExtension();
        $filename = "imgupload15-".$timeUpdate.".".$extension;
        $request->file('imgupload15')->move($destinationPath,$filename);
        $pengajuan->img_15=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_15-".$timeUpdate.".".$extension;
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_15=$imgThumbName.$filename;
      }

      $pengajuan->update();
      $msg = 'Pengajuan anda anda telah lolos tahap verifikasi dan sedang di proses oleh operator';
      $ntf = 'Pemberitahuan';
      $jenis = 'PENGAJUAN';
      $pengajuan_id = null;
      $from_user_id = 'DISDUKCAPIL KOTA BANDUNG';
      AndrApi::sendnotification($jenis,$pengajuan_id,$jenis_layanan,$nik,$msg,$ntf,$from_user_id);
      return response()->json([
        'status' => 'success',
        'data' => 'Pendaftaran berhasil'
      ]);
    }
  }


  public function store_capil(Request $request)
  {
    $nik= $request->nik;
    $jenis_layanan=$request->jenis_layanan;
    if ($jenis_layanan == 1){
      $query=DB::connection('webpunten')
      ->select("SELECT COUNT(1) JUMLAH FROM PENGAJUANLAHIR WHERE NIK = ".$nik." AND JENIS_LAYANAN = 1 AND PROC_STAT = 1");
    }else if ($jenis_layanan == 2){
      $query=DB::connection('webpunten')
      ->select("SELECT COUNT(1) JUMLAH FROM PENGAJUANMATI WHERE NIK = ".$nik." AND JENIS_LAYANAN = 2 AND PROC_STAT = 1");
    }

    if($query[0]->jumlah > 0){
      return response()->json([
        'status' => 'error',
        'data' => 'NIK sudah pernah mengajukan sebelumnya'
      ]);
    }else{
      if ($jenis_layanan == 1){
        $destinationPath=$this->pengajuan_path().'/pengajuan/lahir/'.$nik.'/ori/';
        $destinationPathThumb=$this->pengajuan_path().'/pengajuan/lahir/'.$nik.'/thumb/';
        $imgName = 'pengajuan/lahir/'.$nik.'/ori/';
        $imgThumbName = 'pengajuan/lahir/'.$nik.'/thumb/';
      }else if($jenis_layanan == 2){
        $destinationPath=$this->pengajuan_path().'/pengajuan/mati/'.$nik.'/ori/';
        $destinationPathThumb=$this->pengajuan_path().'/pengajuan/mati/'.$nik.'/thumb/';
        $imgName = 'pengajuan/mati/'.$nik.'/ori/';
        $imgThumbName = 'pengajuan/mati/'.$nik.'/thumb/';
      }else{
        return response()->json([
          'status' => 'error',
          'data' => 'Jenis Pengajuan Salah Mohon Cek Kemabali'
        ]);
      }


      $picname = "imgupload_thumb_1.jpg";
      if (!file_exists($destinationPath)) {
        mkdir($destinationPath, 0777, true);
      }
      if (!file_exists($destinationPathThumb)) {
        mkdir($destinationPathThumb, 0777, true);
      }

      if ($jenis_layanan == 1){
        $pengajuan = new Pengajuanlahir;
      }else if($jenis_layanan == 2){
        $pengajuan = new Pengajuanmati;
      }else{
        return response()->json([
          'status' => 'error',
          'data' => 'Jenis Pengajuan Salah Mohon Cek Kemabali'
        ]);
      }

      $pengajuan->proc_stat=1;
      $pengajuan->nik=$nik;
      $pengajuan->nama_lgkp=$request->nama_lgkp;
      $pengajuan->jenis_klmin=$request->jenis_klmin;
      $pengajuan->tmpt_lhr=$request->tmpt_lhr;
      $pengajuan->tgl_lhr=$request->tgl_lhr;
      $pengajuan->agama=$request->agama;
      $pengajuan->stat_kwn=$request->stat_kwn;
      $pengajuan->gol_drh=$request->gol_drh;
      $pengajuan->pendidikan=$request->pendidikan;
      $pengajuan->pekerjaan=$request->pekerjaan;
      $pengajuan->src_prov=$request->src_prov;
      $pengajuan->src_kab=$request->src_kab;
      $pengajuan->src_kec=$request->src_kec;
      $pengajuan->src_kel=$request->src_kel;
      $pengajuan->src_alamat=$request->src_alamat;
      $pengajuan->src_rt=$request->src_rt;
      $pengajuan->src_rw=$request->src_rw;
      $pengajuan->kec=$request->kec;
      $pengajuan->kel=$request->kel;
      $pengajuan->alamat=$request->alamat;
      $pengajuan->no_rw=$request->no_rw;
      $pengajuan->no_rt=$request->no_rt;
      $pengajuan->status=$request->status;
      $pengajuan->jenis_layanan=$request->jenis_layanan;
      if ($request->jenis_layanan == 3){
        $pengajuan->kwrngrn=$request->kwrngrn;
        $pengajuan->nama_fam=$request->nama_fam;
        $pengajuan->tipe_spsor=$request->tipe_spsor;
        $pengajuan->nama_spsor=$request->nama_spsor;
        $pengajuan->no_paspor=$request->no_paspor;
        $pengajuan->tgl_paspor=$request->tgl_paspor;
        $pengajuan->tgl_akh_paspor=$request->tgl_akh_paspor;
        $pengajuan->no_dok=$request->no_doc;
        $pengajuan->dok_imgr=$request->doc_imigrasi;
        $pengajuan->tgl_doc_imigrasi=$request->tgl_doc_imigrasi;
        $pengajuan->tgl_stay=$request->tgl_stay;
        $pengajuan->tgl_permit=$request->tgl_permit;
      }

      $pengajuan->tgl_berlaku= Carbon::now()->addMonths(6);

      /////////////////
      if($request->hasFile('imgupload1')){
        $photo=$request->file('imgupload1')->getClientOriginalName();
        $extension = $request->file('imgupload1')->getClientOriginalExtension();
        $filename = "imgupload1.".$extension;
        $request->file('imgupload1')->move($destinationPath,$filename);
        $pengajuan->img_1=$imgName.$filename;

        $img1 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_1.jpg";
        $img1->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_1=$imgThumbName.$filename;
      }

      /////////////////
      if($request->hasFile('imgupload2')){
        $photo=$request->file('imgupload2')->getClientOriginalName();
        $extension = $request->file('imgupload2')->getClientOriginalExtension();
        $filename = "imgupload2.".$extension;
        $request->file('imgupload2')->move($destinationPath,$filename);
        $pengajuan->img_2=$imgName.$filename;

        $img2 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_2.jpg";
        $img2->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_2=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload3')){
        $photo=$request->file('imgupload3')->getClientOriginalName();
        $extension = $request->file('imgupload3')->getClientOriginalExtension();
        $filename = "imgupload3.".$extension;
        $request->file('imgupload3')->move($destinationPath,$filename);
        $pengajuan->img_3=$imgName.$filename;

        $img3 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_3.jpg";
        $img3->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_3=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload4')){
        $photo=$request->file('imgupload4')->getClientOriginalName();
        $extension = $request->file('imgupload4')->getClientOriginalExtension();
        $filename = "imgupload4.".$extension;
        $request->file('imgupload4')->move($destinationPath,$filename);
        $pengajuan->img_4=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_4.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_4=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload5')){
        $photo=$request->file('imgupload5')->getClientOriginalName();
        $extension = $request->file('imgupload5')->getClientOriginalExtension();
        $filename = "imgupload5.".$extension;
        $request->file('imgupload5')->move($destinationPath,$filename);
        $pengajuan->img_5=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_5.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_5=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload6')){
        $photo=$request->file('imgupload6')->getClientOriginalName();
        $extension = $request->file('imgupload6')->getClientOriginalExtension();
        $filename = "imgupload6.".$extension;
        $request->file('imgupload6')->move($destinationPath,$filename);
        $pengajuan->img_6=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_6.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_6=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload7')){
        $photo=$request->file('imgupload7')->getClientOriginalName();
        $extension = $request->file('imgupload7')->getClientOriginalExtension();
        $filename = "imgupload7.".$extension;
        $request->file('imgupload7')->move($destinationPath,$filename);
        $pengajuan->img_7=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_7.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_7=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload8')){
        $photo=$request->file('imgupload8')->getClientOriginalName();
        $extension = $request->file('imgupload8')->getClientOriginalExtension();
        $filename = "imgupload8.".$extension;
        $request->file('imgupload8')->move($destinationPath,$filename);
        $pengajuan->img_8=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_8.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_8=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload9')){
        $photo=$request->file('imgupload9')->getClientOriginalName();
        $extension = $request->file('imgupload9')->getClientOriginalExtension();
        $filename = "imgupload9.".$extension;
        $request->file('imgupload9')->move($destinationPath,$filename);
        $pengajuan->img_9=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_9.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_9=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload10')){
        $photo=$request->file('imgupload10')->getClientOriginalName();
        $extension = $request->file('imgupload10')->getClientOriginalExtension();
        $filename = "imgupload10.".$extension;
        $request->file('imgupload10')->move($destinationPath,$filename);
        $pengajuan->img_10=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_10.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_10=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload11')){
        $photo=$request->file('imgupload11')->getClientOriginalName();
        $extension = $request->file('imgupload11')->getClientOriginalExtension();
        $filename = "imgupload11.".$extension;
        $request->file('imgupload11')->move($destinationPath,$filename);
        $pengajuan->img_11=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_11.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_11=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload12')){
        $photo=$request->file('imgupload12')->getClientOriginalName();
        $extension = $request->file('imgupload12')->getClientOriginalExtension();
        $filename = "imgupload12.".$extension;
        $request->file('imgupload12')->move($destinationPath,$filename);
        $pengajuan->img_12=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_12.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_12=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload13')){
        $photo=$request->file('imgupload13')->getClientOriginalName();
        $extension = $request->file('imgupload13')->getClientOriginalExtension();
        $filename = "imgupload13.".$extension;
        $request->file('imgupload13')->move($destinationPath,$filename);
        $pengajuan->img_13=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_13.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_13=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload14')){
        $photo=$request->file('imgupload14')->getClientOriginalName();
        $extension = $request->file('imgupload14')->getClientOriginalExtension();
        $filename = "imgupload14.".$extension;
        $request->file('imgupload14')->move($destinationPath,$filename);
        $pengajuan->img_14=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_14.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_14=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('imgupload15')){
        $photo=$request->file('imgupload15')->getClientOriginalName();
        $extension = $request->file('imgupload15')->getClientOriginalExtension();
        $filename = "imgupload15.".$extension;
        $request->file('imgupload15')->move($destinationPath,$filename);
        $pengajuan->img_15=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "imgupload_thumb_15.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_15=$imgThumbName.$filename;
      }

      $pengajuan->save();
      return response()->json([
        'status' => 'success',
        'data' => 'Pendaftaran berhasil'
      ]);
    }
  }

  public function restore(Request $request)
  {
    $nik= $request->nik;
    $jenis_layanan=$request->jenis_layanan;
    if ($jenis_layanan == 1){
      $query=DB::connection('webpunten')
      ->select("SELECT COUNT(1) JUMLAH FROM PENGAJUANSKTS WHERE NIK = ".$nik." AND JENIS_LAYANAN = 1");
    }else if ($jenis_layanan == 2){
      $query=DB::connection('webpunten')
      ->select("SELECT COUNT(1) JUMLAH FROM PENGAJUANDATANG WHERE NIK = ".$nik." AND JENIS_LAYANAN = 2");
    }else{
      $query=DB::connection('webpunten')
      ->select("SELECT COUNT(1) JUMLAH FROM PENGAJUANWNA WHERE NIK = ".$nik." AND JENIS_LAYANAN = 3");
    }

    if($query[0]->jumlah == 0){
      return response()->json([
        'status' => 'error',
        'data' => 'NIK belum pernah mengajukan sebelumnya'
      ]);
    }else{
      if ($jenis_layanan == 1){
        $destinationPath=$this->pengajuan_path().'/pengajuan/skts/'.$nik.'/ori/';
        $destinationPathThumb=$this->pengajuan_path().'/pengajuan/skts/'.$nik.'/thumb/';
        $imgName = 'pengajuan/skts/'.$nik.'/ori/';
        $imgThumbName = 'pengajuan/skts/'.$nik.'/thumb/';
      }else if($jenis_layanan == 2){
        $destinationPath=$this->pengajuan_path().'/pengajuan/datang/'.$nik.'/ori/';
        $destinationPathThumb=$this->pengajuan_path().'/pengajuan/datang/'.$nik.'/thumb/';
        $imgName = 'pengajuan/datang/'.$nik.'/ori/';
        $imgThumbName = 'pengajuan/datang/'.$nik.'/thumb/';
      }else if($jenis_layanan == 3){
        $destinationPath=$this->pengajuan_path().'/pengajuan/wna/'.$nik.'/ori/';
        $destinationPathThumb=$this->pengajuan_path().'/pengajuan/wna/'.$nik.'/thumb/';
        $imgName = 'pengajuan/wna/'.$nik.'/ori/';
        $imgThumbName = 'pengajuan/wna/'.$nik.'/thumb/';
      }else{
        return response()->json([
          'status' => 'error',
          'data' => 'Jenis Pengajuan Salah Mohon Cek Kemabali'
        ]);
      }


      $picname = "re_imgupload_thumb_1.jpg";
      if (!file_exists($destinationPath)) {
        mkdir($destinationPath, 0777, true);
      }
      if (!file_exists($destinationPathThumb)) {
        mkdir($destinationPathThumb, 0777, true);
      }

      if ($jenis_layanan == 1){
        $pengajuan = new Pengajuanskts;
      }else if($jenis_layanan == 2){
        $pengajuan = new Pengajuandatang;
      }else if($jenis_layanan == 3){
        $pengajuan = new Pengajuanwna;
      }else{
        return response()->json([
          'status' => 'error',
          'data' => 'Jenis Pengajuan Salah Mohon Cek Kemabali'
        ]);
      }

      $pengajuan->proc_stat=2;
      $pengajuan->stat_berkas=null;
      $pengajuan->nik=$nik;
      $pengajuan->nama_lgkp=$request->nama_lgkp;
      $pengajuan->jenis_klmin=$request->jenis_klmin;
      $pengajuan->tmpt_lhr=$request->tmpt_lhr;
      $pengajuan->tgl_lhr=$request->tgl_lhr;
      $pengajuan->agama=$request->agama;
      $pengajuan->stat_kwn=$request->stat_kwn;
      $pengajuan->gol_drh=$request->gol_drh;
      $pengajuan->pendidikan=$request->pendidikan;
      $pengajuan->pekerjaan=$request->pekerjaan;
      $pengajuan->src_prov=$request->src_prov;
      $pengajuan->src_kab=$request->src_kab;
      $pengajuan->src_kec=$request->src_kec;
      $pengajuan->src_kel=$request->src_kel;
      $pengajuan->src_alamat=$request->src_alamat;
      $pengajuan->src_rt=$request->src_rt;
      $pengajuan->src_rw=$request->src_rw;
      $pengajuan->kec=$request->kec;
      $pengajuan->kel=$request->kel;
      $pengajuan->alamat=$request->alamat;
      $pengajuan->no_rw=$request->no_rw;
      $pengajuan->no_rt=$request->no_rt;
      $pengajuan->no_kk=$request->no_kk;
      $pengajuan->stat_hbkel=$request->stat_hbkel;
      $pengajuan->alasan_pindah=$request->alasan_pindah;
      $pengajuan->jangka_waktu=$request->jangka_waktu;
      $pengajuan->jum_anggota=$request->jum_anggota;
      $pengajuan->tgl_datang=$request->tgl_datang;
      $pengajuan->status=$request->status;
      $pengajuan->jenis_layanan=$request->jenis_layanan;
      if ($request->jenis_layanan == 2){
        $pengajuan->akta_lhr=$request->akta_lhr;
        $pengajuan->no_akta_lhr=$request->no_akta_lhr;
      }
      if ($request->jenis_layanan == 3){
        $pengajuan->kwrngrn=$request->kwrngrn;
        $pengajuan->nama_fam=$request->nama_fam;
        $pengajuan->tipe_spsor=$request->tipe_spsor;
        $pengajuan->nama_spsor=$request->nama_spsor;
        $pengajuan->no_paspor=$request->no_paspor;
        $pengajuan->tgl_paspor=$request->tgl_paspor;
        $pengajuan->tgl_akh_paspor=$request->tgl_akh_paspor;
        $pengajuan->no_dok=$request->no_dok;
        $pengajuan->dok_imgr=$request->doc_imigrasi;
        $pengajuan->tgl_doc_imigrasi=$request->tgl_doc_imigrasi;
        $pengajuan->tgl_stay=$request->tgl_stay;
        $pengajuan->tgl_permit=$request->tgl_permit;
      }

      $pengajuan->tgl_berlaku= Carbon::now()->addMonths(6);

      /////////////////
      if($request->hasFile('re_imgupload1')){
        $photo=$request->file('re_imgupload1')->getClientOriginalName();
        $extension = $request->file('re_imgupload1')->getClientOriginalExtension();
        $filename = "re_imgupload1.".$extension;
        $request->file('re_imgupload1')->move($destinationPath,$filename);
        $pengajuan->img_1=$imgName.$filename;

        $img1 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "re_imgupload_thumb_1.jpg";
        $img1->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_1=$imgThumbName.$filename;
      }

      /////////////////
      if($request->hasFile('re_imgupload2')){
        $photo=$request->file('re_imgupload2')->getClientOriginalName();
        $extension = $request->file('re_imgupload2')->getClientOriginalExtension();
        $filename = "re_imgupload2.".$extension;
        $request->file('re_imgupload2')->move($destinationPath,$filename);
        $pengajuan->img_2=$imgName.$filename;

        $img2 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "re_imgupload_thumb_2.jpg";
        $img2->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_2=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('re_imgupload3')){
        $photo=$request->file('re_imgupload3')->getClientOriginalName();
        $extension = $request->file('re_imgupload3')->getClientOriginalExtension();
        $filename = "re_imgupload3.".$extension;
        $request->file('re_imgupload3')->move($destinationPath,$filename);
        $pengajuan->img_3=$imgName.$filename;

        $img3 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "re_imgupload_thumb_3.jpg";
        $img3->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_3=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('re_imgupload4')){
        $photo=$request->file('re_imgupload4')->getClientOriginalName();
        $extension = $request->file('re_imgupload4')->getClientOriginalExtension();
        $filename = "re_imgupload4.".$extension;
        $request->file('re_imgupload4')->move($destinationPath,$filename);
        $pengajuan->img_4=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "re_imgupload_thumb_4.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_4=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('re_imgupload5')){
        $photo=$request->file('re_imgupload5')->getClientOriginalName();
        $extension = $request->file('re_imgupload5')->getClientOriginalExtension();
        $filename = "re_imgupload5.".$extension;
        $request->file('re_imgupload5')->move($destinationPath,$filename);
        $pengajuan->img_5=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "re_imgupload_thumb_5.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_5=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('re_imgupload6')){
        $photo=$request->file('re_imgupload6')->getClientOriginalName();
        $extension = $request->file('re_imgupload6')->getClientOriginalExtension();
        $filename = "re_imgupload6.".$extension;
        $request->file('re_imgupload6')->move($destinationPath,$filename);
        $pengajuan->img_6=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "re_imgupload_thumb_6.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_6=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('re_imgupload7')){
        $photo=$request->file('re_imgupload7')->getClientOriginalName();
        $extension = $request->file('re_imgupload7')->getClientOriginalExtension();
        $filename = "re_imgupload7.".$extension;
        $request->file('re_imgupload7')->move($destinationPath,$filename);
        $pengajuan->img_7=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "re_imgupload_thumb_7.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_7=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('re_imgupload8')){
        $photo=$request->file('re_imgupload8')->getClientOriginalName();
        $extension = $request->file('re_imgupload8')->getClientOriginalExtension();
        $filename = "re_imgupload8.".$extension;
        $request->file('re_imgupload8')->move($destinationPath,$filename);
        $pengajuan->img_8=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "re_imgupload_thumb_8.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_8=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('re_imgupload9')){
        $photo=$request->file('re_imgupload9')->getClientOriginalName();
        $extension = $request->file('re_imgupload9')->getClientOriginalExtension();
        $filename = "re_imgupload9.".$extension;
        $request->file('re_imgupload9')->move($destinationPath,$filename);
        $pengajuan->img_9=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "re_imgupload_thumb_9.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_9=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('re_imgupload10')){
        $photo=$request->file('re_imgupload10')->getClientOriginalName();
        $extension = $request->file('re_imgupload10')->getClientOriginalExtension();
        $filename = "re_imgupload10.".$extension;
        $request->file('re_imgupload10')->move($destinationPath,$filename);
        $pengajuan->img_10=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "re_imgupload_thumb_10.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_10=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('re_imgupload11')){
        $photo=$request->file('re_imgupload11')->getClientOriginalName();
        $extension = $request->file('re_imgupload11')->getClientOriginalExtension();
        $filename = "re_imgupload11.".$extension;
        $request->file('re_imgupload11')->move($destinationPath,$filename);
        $pengajuan->img_11=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "re_imgupload_thumb_11.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_11=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('re_imgupload12')){
        $photo=$request->file('re_imgupload12')->getClientOriginalName();
        $extension = $request->file('re_imgupload12')->getClientOriginalExtension();
        $filename = "re_imgupload12.".$extension;
        $request->file('re_imgupload12')->move($destinationPath,$filename);
        $pengajuan->img_12=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "re_imgupload_thumb_12.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_12=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('re_imgupload13')){
        $photo=$request->file('re_imgupload13')->getClientOriginalName();
        $extension = $request->file('re_imgupload13')->getClientOriginalExtension();
        $filename = "re_imgupload13.".$extension;
        $request->file('re_imgupload13')->move($destinationPath,$filename);
        $pengajuan->img_13=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "re_imgupload_thumb_13.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_13=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('re_imgupload14')){
        $photo=$request->file('re_imgupload14')->getClientOriginalName();
        $extension = $request->file('re_imgupload14')->getClientOriginalExtension();
        $filename = "re_imgupload14.".$extension;
        $request->file('re_imgupload14')->move($destinationPath,$filename);
        $pengajuan->img_14=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "re_imgupload_thumb_14.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_14=$imgThumbName.$filename;
      }
      /////////////////
      if($request->hasFile('re_imgupload15')){
        $photo=$request->file('re_imgupload15')->getClientOriginalName();
        $extension = $request->file('re_imgupload15')->getClientOriginalExtension();
        $filename = "re_imgupload15.".$extension;
        $request->file('re_imgupload15')->move($destinationPath,$filename);
        $pengajuan->img_15=$imgName.$filename;

        $img4 = Image::make($destinationPath.$filename)->resize(500, 500);
        $filename = "re_imgupload_thumb_15.jpg";
        $img4->save($destinationPathThumb.$filename);
        $pengajuan->img_thumb_15=$imgThumbName.$filename;
      }
      $pengajuan->update();
      return response()->json([
        'status' => 'success',
        'data' => 'Pendaftaran berhasil'
      ]);
    }
  }

  public function cek_pengajuan(Request $request)
  {
    $nik = $request->nik;
    $jenis_layanan = 0;

    $query1=DB::connection('webpunten')
    ->select("SELECT COUNT(1) JUMLAH FROM PENGAJUANSKTS WHERE NIK = ".$nik."");
    if($query1[0]->jumlah > 0){
      $jenis_layanan = 1;
    }

    $query2=DB::connection('webpunten')
    ->select("SELECT COUNT(1) JUMLAH FROM PENGAJUANDATANG WHERE NIK = ".$nik." ");
    if($query2[0]->jumlah > 0){
      $jenis_layanan = 2;
    }

    $query3=DB::connection('webpunten')
    ->select("SELECT COUNT(1) JUMLAH FROM PENGAJUANWNA WHERE NIK = ".$nik." ");
    if($query3[0]->jumlah > 0){
      $jenis_layanan = 3;
    }

    if($jenis_layanan <> 0){
      if($jenis_layanan == 1){
        $query=DB::connection('webpunten')
        ->select("SELECT  ID ,NIK
          ,IMG_1 ,IMG_THUMB_1 ,IMG_2 ,IMG_THUMB_2 ,IMG_3 ,IMG_THUMB_3 ,IMG_4 ,IMG_THUMB_4 ,IMG_5 ,IMG_THUMB_5
          ,IMG_6 ,IMG_THUMB_6 ,IMG_7 ,IMG_THUMB_7 ,IMG_8 ,IMG_THUMB_8 ,IMG_9 ,IMG_THUMB_9 ,IMG_10 ,IMG_THUMB_10
          ,IMG_11 ,IMG_THUMB_11 ,IMG_12 ,IMG_THUMB_12 ,IMG_13 ,IMG_THUMB_13 ,IMG_14 ,IMG_THUMB_14 ,IMG_15 ,IMG_THUMB_15
          ,PROC_STAT ,STAT_BERKAS ,VERIFIED_AT ,VERIFIED_BY ,CREATED_AT ,UPDATED_AT ,DELETED_AT ,DELETED_BY ,NO_SKTS ,NAMA_LGKP ,TMPT_LHR
          ,TO_CHAR(TGL_LHR ,'DD-MM-YYYY') TGL_LHR
          ,JENIS_KLMIN ,AGAMA ,STAT_KWN ,GOL_DRH ,PENDIDIKAN ,PEKERJAAN
          , SRC_PROV ,SRC_KAB ,SRC_KEC ,SRC_KEL
          , F5_GET_NAMA_PROVINSI(SRC_PROV) SRC_NAMA_PROV
          , F5_GET_NAMA_KABUPATEN(SRC_PROV,SRC_KAB) SRC_NAMA_KAB
          , F5_GET_NAMA_KECAMATAN(SRC_PROV,SRC_KAB ,SRC_KEC) SRC_NAMA_KEC
          , F5_GET_NAMA_KELURAHAN(SRC_PROV,SRC_KAB ,SRC_KEC ,SRC_KEL ) SRC_NAMA_KEL
          , SRC_ALAMAT ,SRC_RT ,SRC_RW ,KEC ,KEL
          , F5_GET_NAMA_KECAMATAN(32,73,KEC) NAMA_KEC
          , F5_GET_NAMA_KELURAHAN(32,73,KEC ,KEL ) NAMA_KEL
          , UPPER(F5_GET_REF_WNI(F5_TO_NUMBER(JENIS_KLMIN,7), 801)) JENIS_KLMIN_DESC
          , UPPER(F5_GET_REF_WNI(F5_TO_NUMBER(AGAMA,7), 501)) AGAMA_DESC
          , UPPER(F5_GET_REF_WNI(PEKERJAAN, 201)) PEKERJAAN_DESC
          , UPPER(F5_GET_REF_WNI(PENDIDIKAN, 101)) PENDIDIKAN_DESC
          , UPPER(F5_GET_REF_WNI(STAT_KWN, 601)) STAT_KWN_DESC
          , UPPER(F5_GET_REF_WNI(GOL_DRH, 401)) GOL_DRH_DESC
          ,ALAMAT ,NO_RT ,NO_RW ,STATUS,TELEPON,EMAIL
          , STAT_HBKEL, UPPER(F5_GET_REF_WNI(STAT_HBKEL, 301)) STAT_HBKEL_DESC, TO_CHAR(JANGKA_WAKTU ,'DD-MM-YYYY') JANGKA_WAKTU, JUM_ANGGOTA, TO_CHAR(TGL_DATANG,'DD-MM-YYYY') TGL_DATANG, ALASAN_PINDAH, UPPER(F5_GET_REF_WNI(ALASAN_PINDAH, 851)) ALASAN_PINDAH_DESC
          ,TO_CHAR(TGL_BERLAKU,'DD-MM-YYYY') TGL_BERLAKU  ,JENIS_LAYANAN,
          CASE WHEN JENIS_LAYANAN = 1 THEN 'SKTS' WHEN JENIS_LAYANAN = 2 THEN 'PINDAH DATANG' ELSE 'WNA' END  JENIS_LAYANAN_DESC
          FROM PENGAJUANSKTS WHERE NIK = ".$nik."");
        }else if($jenis_layanan == 2){
          $query=DB::connection('webpunten')
          ->select("SELECT ID ,NIK
            ,IMG_1 ,IMG_THUMB_1 ,IMG_2 ,IMG_THUMB_2 ,IMG_3 ,IMG_THUMB_3 ,IMG_4 ,IMG_THUMB_4 ,IMG_5 ,IMG_THUMB_5
            ,IMG_6 ,IMG_THUMB_6 ,IMG_7 ,IMG_THUMB_7 ,IMG_8 ,IMG_THUMB_8 ,IMG_9 ,IMG_THUMB_9 ,IMG_10 ,IMG_THUMB_10
            ,IMG_11 ,IMG_THUMB_11 ,IMG_12 ,IMG_THUMB_12 ,IMG_13 ,IMG_THUMB_13 ,IMG_14 ,IMG_THUMB_14 ,IMG_15 ,IMG_THUMB_15
            ,PROC_STAT ,STAT_BERKAS ,VERIFIED_AT ,VERIFIED_BY ,CREATED_AT ,UPDATED_AT ,DELETED_AT ,DELETED_BY ,NO_DATANG ,NAMA_LGKP ,TMPT_LHR
            ,TO_CHAR(TGL_LHR ,'DD-MM-YYYY') TGL_LHR
            ,JENIS_KLMIN ,AGAMA ,STAT_KWN ,GOL_DRH ,PENDIDIKAN ,PEKERJAAN
            , SRC_PROV ,SRC_KAB ,SRC_KEC ,SRC_KEL
            , F5_GET_NAMA_PROVINSI(SRC_PROV) SRC_NAMA_PROV
            , F5_GET_NAMA_KABUPATEN(SRC_PROV,SRC_KAB) SRC_NAMA_KAB
            , F5_GET_NAMA_KECAMATAN(SRC_PROV,SRC_KAB ,SRC_KEC) SRC_NAMA_KEC
            , F5_GET_NAMA_KELURAHAN(SRC_PROV,SRC_KAB ,SRC_KEC ,SRC_KEL ) SRC_NAMA_KEL
            , SRC_ALAMAT ,SRC_RT ,SRC_RW ,KEC ,KEL
            , F5_GET_NAMA_KECAMATAN(32,73,KEC) NAMA_KEC
            , F5_GET_NAMA_KELURAHAN(32,73,KEC ,KEL ) NAMA_KEL
            , UPPER(F5_GET_REF_WNI(F5_TO_NUMBER(JENIS_KLMIN,7), 801)) JENIS_KLMIN_DESC
            , UPPER(F5_GET_REF_WNI(F5_TO_NUMBER(AGAMA,7), 501)) AGAMA_DESC
            , UPPER(F5_GET_REF_WNI(PEKERJAAN, 201)) PEKERJAAN_DESC
            , UPPER(F5_GET_REF_WNI(PENDIDIKAN, 101)) PENDIDIKAN_DESC
            , UPPER(F5_GET_REF_WNI(STAT_KWN, 601)) STAT_KWN_DESC
            , UPPER(F5_GET_REF_WNI(GOL_DRH, 401)) GOL_DRH_DESC
            ,ALAMAT ,NO_RT ,NO_RW ,STATUS
            , STAT_HBKEL, UPPER(F5_GET_REF_WNI(STAT_HBKEL, 301)) STAT_HBKEL_DESC, TO_CHAR(JANGKA_WAKTU ,'DD-MM-YYYY') JANGKA_WAKTU, JUM_ANGGOTA, TO_CHAR(TGL_DATANG,'DD-MM-YYYY') TGL_DATANG, ALASAN_PINDAH, UPPER(F5_GET_REF_WNI(ALASAN_PINDAH, 851)) ALASAN_PINDAH_DESC
            ,TO_CHAR(TGL_BERLAKU,'DD-MM-YYYY') TGL_BERLAKU  ,JENIS_LAYANAN,AKTA_LHR,NO_AKTA_LHR,NO_SKPWNI,TELEPON,EMAIL
            ,CASE WHEN JENIS_LAYANAN = 1 THEN 'SKTS' WHEN JENIS_LAYANAN = 2 THEN 'PINDAH DATANG' ELSE 'WNA' END  JENIS_LAYANAN_DESC
            FROM PENGAJUANDATANG WHERE NIK = ".$nik."");
          }else{
            $query=DB::connection('webpunten')
            ->select("SELECT ID ,NIK
              ,IMG_1 ,IMG_THUMB_1 ,IMG_2 ,IMG_THUMB_2 ,IMG_3 ,IMG_THUMB_3 ,IMG_4 ,IMG_THUMB_4 ,IMG_5 ,IMG_THUMB_5
              ,IMG_6 ,IMG_THUMB_6 ,IMG_7 ,IMG_THUMB_7 ,IMG_8 ,IMG_THUMB_8 ,IMG_9 ,IMG_THUMB_9 ,IMG_10 ,IMG_THUMB_10
              ,IMG_11 ,IMG_THUMB_11 ,IMG_12 ,IMG_THUMB_12 ,IMG_13 ,IMG_THUMB_13 ,IMG_14 ,IMG_THUMB_14 ,IMG_15 ,IMG_THUMB_15
              ,PROC_STAT ,STAT_BERKAS ,VERIFIED_AT ,VERIFIED_BY ,CREATED_AT ,UPDATED_AT ,DELETED_AT ,DELETED_BY ,NO_WNA,NAMA_LGKP, NAMA_FAM,TMPT_LHR
              ,TO_CHAR(TGL_LHR ,'DD-MM-YYYY') TGL_LHR
              ,JENIS_KLMIN ,AGAMA ,STAT_KWN ,GOL_DRH ,PENDIDIKAN ,PEKERJAAN
              , SRC_PROV ,SRC_KAB ,SRC_KEC ,SRC_KEL
              , F5_GET_NAMA_PROVINSI(SRC_PROV) SRC_NAMA_PROV
              , F5_GET_NAMA_KABUPATEN(SRC_PROV,SRC_KAB) SRC_NAMA_KAB
              , F5_GET_NAMA_KECAMATAN(SRC_PROV,SRC_KAB ,SRC_KEC) SRC_NAMA_KEC
              , F5_GET_NAMA_KELURAHAN(SRC_PROV,SRC_KAB ,SRC_KEC ,SRC_KEL ) SRC_NAMA_KEL
              , SRC_ALAMAT ,SRC_RT ,SRC_RW ,KEC ,KEL
              , F5_GET_NAMA_KECAMATAN(32,73,KEC) NAMA_KEC
              , F5_GET_NAMA_KELURAHAN(32,73,KEC ,KEL ) NAMA_KEL
              , UPPER(F5_GET_REF_WNA(F5_TO_NUMBER(JENIS_KLMIN,7), 801)) JENIS_KLMIN_DESC
              , UPPER(F5_GET_REF_WNA(F5_TO_NUMBER(AGAMA,7), 901)) AGAMA_DESC
              , UPPER(F5_GET_REF_WNA(PEKERJAAN, 201)) PEKERJAAN_DESC
              , UPPER(F5_GET_REF_WNA(PENDIDIKAN, 101)) PENDIDIKAN_DESC
              , UPPER(F5_GET_REF_WNA(STAT_KWN, 701)) STAT_KWN_DESC
              , UPPER(F5_GET_REF_WNA(GOL_DRH, 401)) GOL_DRH_DESC
              , UPPER(F5_GET_REF_NEGARA(KWRNGRN)) KWRNGRN_DESC
              , KWRNGRN KWRNGRN
              ,ALAMAT ,NO_RT ,NO_RW ,STATUS,TELEPON,EMAIL
              , STAT_HBKEL, UPPER(F5_GET_REF_WNA(STAT_HBKEL, 301)) STAT_HBKEL_DESC, TO_CHAR(JANGKA_WAKTU ,'DD-MM-YYYY') JANGKA_WAKTU, JUM_ANGGOTA, TO_CHAR(TGL_DATANG,'DD-MM-YYYY') TGL_DATANG, ALASAN_PINDAH, UPPER(F5_GET_REF_WNA(ALASAN_PINDAH, 601)) ALASAN_PINDAH_DESC
              ,NO_PASPOR
              ,TO_CHAR(TGL_PASPOR,'DD-MM-YYYY') TGL_PASPOR
              ,TO_CHAR(TGL_AKH_PASPOR,'DD-MM-YYYY') TGL_AKH_PASPOR
              ,NAMA_SPSOR
              ,TIPE_SPSOR
              ,DOK_IMGR
              ,NO_DOK
              , UPPER(F5_GET_REF_WNA(DOK_IMGR, 501)) DOK_IMGR_DESC
              ,TMPT_DTBIT
              ,TO_CHAR(TGL_DTBIT,'DD-MM-YYYY') TGL_DTBIT
              ,TO_CHAR(TGL_AKH_DOC_IMGR,'DD-MM-YYYY') TGL_AKH_DOC_IMGR
              ,TO_CHAR(TGL_STAY,'DD-MM-YYYY') TGL_STAY
              ,TO_CHAR(TGL_PICKUP,'DD-MM-YYYY') TGL_PICKUP
              ,TO_CHAR(TGL_DOC_IMIGRASI,'DD-MM-YYYY') TGL_DOC_IMIGRASI
              ,TO_CHAR(TGL_BERLAKU,'DD-MM-YYYY') TGL_BERLAKU  ,JENIS_LAYANAN,
              'WNA' JENIS_LAYANAN_DESC
              FROM PENGAJUANWNA WHERE NIK = ".$nik."");
            }
            return response()->json([
              $query[0]
            ]);
          }else{
            return response()->json([
              'status' => 'error',
              'data' => 'NIK Tidak Ada'
            ]);
          }
        }

        public function cek_pengajuan_kk(Request $request)
        {
          $no_kk = $request->no_kk;
          $jenis_layanan = 0;

          $query1=DB::connection('webpunten')
          ->select("SELECT COUNT(1) JUMLAH FROM PENGAJUANSKTS WHERE NO_KK = ".$no_kk."");
          if($query1[0]->jumlah > 0){
            $jenis_layanan = 1;
          }

          $query2=DB::connection('webpunten')
          ->select("SELECT COUNT(1) JUMLAH FROM PENGAJUANDATANG WHERE NO_KK = ".$no_kk."");
          if($query2[0]->jumlah > 0){
            $jenis_layanan = 2;
          }

          $query3=DB::connection('webpunten')
          ->select("SELECT COUNT(1) JUMLAH FROM PENGAJUANWNA WHERE NO_KK = ".$no_kk."");
          if($query3[0]->jumlah > 0){
            $jenis_layanan = 3;
          }

          if($jenis_layanan <> 0){
            if($jenis_layanan == 1){
              $query=DB::connection('webpunten')
              ->select("SELECT  ID ,NIK
                ,IMG_1 ,IMG_THUMB_1 ,IMG_2 ,IMG_THUMB_2 ,IMG_3 ,IMG_THUMB_3 ,IMG_4 ,IMG_THUMB_4 ,IMG_5 ,IMG_THUMB_5
                ,IMG_6 ,IMG_THUMB_6 ,IMG_7 ,IMG_THUMB_7 ,IMG_8 ,IMG_THUMB_8 ,IMG_9 ,IMG_THUMB_9 ,IMG_10 ,IMG_THUMB_10
                ,IMG_11 ,IMG_THUMB_11 ,IMG_12 ,IMG_THUMB_12 ,IMG_13 ,IMG_THUMB_13 ,IMG_14 ,IMG_THUMB_14 ,IMG_15 ,IMG_THUMB_15
                ,PROC_STAT ,STAT_BERKAS ,VERIFIED_AT ,VERIFIED_BY ,CREATED_AT ,UPDATED_AT ,DELETED_AT ,DELETED_BY ,NO_SKTS ,NAMA_LGKP ,TMPT_LHR
                ,TO_CHAR(TGL_LHR ,'DD-MM-YYYY') TGL_LHR
                ,JENIS_KLMIN ,AGAMA ,STAT_KWN ,GOL_DRH ,PENDIDIKAN ,PEKERJAAN
                , SRC_PROV ,SRC_KAB ,SRC_KEC ,SRC_KEL
                , F5_GET_NAMA_PROVINSI(SRC_PROV) SRC_NAMA_PROV
                , F5_GET_NAMA_KABUPATEN(SRC_PROV,SRC_KAB) SRC_NAMA_KAB
                , F5_GET_NAMA_KECAMATAN(SRC_PROV,SRC_KAB ,SRC_KEC) SRC_NAMA_KEC
                , F5_GET_NAMA_KELURAHAN(SRC_PROV,SRC_KAB ,SRC_KEC ,SRC_KEL ) SRC_NAMA_KEL
                , SRC_ALAMAT ,SRC_RT ,SRC_RW ,KEC ,KEL
                , F5_GET_NAMA_KECAMATAN(32,73,KEC) NAMA_KEC
                , F5_GET_NAMA_KELURAHAN(32,73,KEC ,KEL ) NAMA_KEL
                , UPPER(F5_GET_REF_WNI(F5_TO_NUMBER(JENIS_KLMIN,7), 801)) JENIS_KLMIN_DESC
                , UPPER(F5_GET_REF_WNI(F5_TO_NUMBER(AGAMA,7), 501)) AGAMA_DESC
                , UPPER(F5_GET_REF_WNI(PEKERJAAN, 201)) PEKERJAAN_DESC
                , UPPER(F5_GET_REF_WNI(PENDIDIKAN, 101)) PENDIDIKAN_DESC
                , UPPER(F5_GET_REF_WNI(STAT_KWN, 601)) STAT_KWN_DESC
                , UPPER(F5_GET_REF_WNI(GOL_DRH, 401)) GOL_DRH_DESC
                ,ALAMAT ,NO_RT ,NO_RW ,STATUS ,JUM_ANGGOTA
                , STAT_HBKEL, UPPER(F5_GET_REF_WNI(STAT_HBKEL, 301)) STAT_HBKEL_DESC, JANGKA_WAKTU, JUM_ANGGOTA, TO_CHAR(TGL_DATANG,'DD-MM-YYYY') TGL_DATANG, ALASAN_PINDAH, UPPER(F5_GET_REF_WNI(ALASAN_PINDAH, 851)) ALASAN_PINDAH_DESC
                ,TO_CHAR(TGL_BERLAKU,'DD-MM-YYYY') TGL_BERLAKU  ,JENIS_LAYANAN,
                CASE WHEN JENIS_LAYANAN = 1 THEN 'SKTS' WHEN JENIS_LAYANAN = 2 THEN 'PINDAH DATANG' ELSE 'WNA' END  JENIS_LAYANAN_DESC
                FROM PENGAJUANSKTS WHERE NO_KK = ".$no_kk." ORDER BY STAT_HBKEL, TGL_LHR DESC");
              }else if($jenis_layanan == 2){
                $query=DB::connection('webpunten')
                ->select("SELECT ID ,NIK
                  ,IMG_1 ,IMG_THUMB_1 ,IMG_2 ,IMG_THUMB_2 ,IMG_3 ,IMG_THUMB_3 ,IMG_4 ,IMG_THUMB_4 ,IMG_5 ,IMG_THUMB_5
                  ,IMG_6 ,IMG_THUMB_6 ,IMG_7 ,IMG_THUMB_7 ,IMG_8 ,IMG_THUMB_8 ,IMG_9 ,IMG_THUMB_9 ,IMG_10 ,IMG_THUMB_10
                  ,IMG_11 ,IMG_THUMB_11 ,IMG_12 ,IMG_THUMB_12 ,IMG_13 ,IMG_THUMB_13 ,IMG_14 ,IMG_THUMB_14 ,IMG_15 ,IMG_THUMB_15
                  ,PROC_STAT ,STAT_BERKAS ,VERIFIED_AT ,VERIFIED_BY ,CREATED_AT ,UPDATED_AT ,DELETED_AT ,DELETED_BY ,NO_DATANG ,NAMA_LGKP ,TMPT_LHR
                  ,TO_CHAR(TGL_LHR ,'DD-MM-YYYY') TGL_LHR
                  ,JENIS_KLMIN ,AGAMA ,STAT_KWN ,GOL_DRH ,PENDIDIKAN ,PEKERJAAN
                  , SRC_PROV ,SRC_KAB ,SRC_KEC ,SRC_KEL
                  , F5_GET_NAMA_PROVINSI(SRC_PROV) SRC_NAMA_PROV
                  , F5_GET_NAMA_KABUPATEN(SRC_PROV,SRC_KAB) SRC_NAMA_KAB
                  , F5_GET_NAMA_KECAMATAN(SRC_PROV,SRC_KAB ,SRC_KEC) SRC_NAMA_KEC
                  , F5_GET_NAMA_KELURAHAN(SRC_PROV,SRC_KAB ,SRC_KEC ,SRC_KEL ) SRC_NAMA_KEL
                  , SRC_ALAMAT ,SRC_RT ,SRC_RW ,KEC ,KEL
                  , F5_GET_NAMA_KECAMATAN(32,73,KEC) NAMA_KEC
                  , F5_GET_NAMA_KELURAHAN(32,73,KEC ,KEL ) NAMA_KEL
                  , UPPER(F5_GET_REF_WNI(F5_TO_NUMBER(JENIS_KLMIN,7), 801)) JENIS_KLMIN_DESC
                  , UPPER(F5_GET_REF_WNI(F5_TO_NUMBER(AGAMA,7), 501)) AGAMA_DESC
                  , UPPER(F5_GET_REF_WNI(PEKERJAAN, 201)) PEKERJAAN_DESC
                  , UPPER(F5_GET_REF_WNI(PENDIDIKAN, 101)) PENDIDIKAN_DESC
                  , UPPER(F5_GET_REF_WNI(STAT_KWN, 601)) STAT_KWN_DESC
                  , UPPER(F5_GET_REF_WNI(GOL_DRH, 401)) GOL_DRH_DESC
                  ,ALAMAT ,NO_RT ,NO_RW ,STATUS ,JUM_ANGGOTA
                  , STAT_HBKEL, UPPER(F5_GET_REF_WNI(STAT_HBKEL, 301)) STAT_HBKEL_DESC, JANGKA_WAKTU, JUM_ANGGOTA, TO_CHAR(TGL_DATANG,'DD-MM-YYYY') TGL_DATANG, ALASAN_PINDAH, UPPER(F5_GET_REF_WNI(ALASAN_PINDAH, 851)) ALASAN_PINDAH_DESC
                  ,TO_CHAR(TGL_BERLAKU,'DD-MM-YYYY') TGL_BERLAKU  ,JENIS_LAYANAN,
                  CASE WHEN JENIS_LAYANAN = 1 THEN 'SKTS' WHEN JENIS_LAYANAN = 2 THEN 'PINDAH DATANG' ELSE 'WNA' END  JENIS_LAYANAN_DESC
                  FROM PENGAJUANDATANG WHERE NO_KK = ".$no_kk." ORDER BY STAT_HBKEL, TGL_LHR DESC");
                }else{
                  $query=DB::connection('webpunten')
                  ->select("SELECT ID ,NIK
                    ,IMG_1 ,IMG_THUMB_1 ,IMG_2 ,IMG_THUMB_2 ,IMG_3 ,IMG_THUMB_3 ,IMG_4 ,IMG_THUMB_4 ,IMG_5 ,IMG_THUMB_5
                    ,IMG_6 ,IMG_THUMB_6 ,IMG_7 ,IMG_THUMB_7 ,IMG_8 ,IMG_THUMB_8 ,IMG_9 ,IMG_THUMB_9 ,IMG_10 ,IMG_THUMB_10
                    ,IMG_11 ,IMG_THUMB_11 ,IMG_12 ,IMG_THUMB_12 ,IMG_13 ,IMG_THUMB_13 ,IMG_14 ,IMG_THUMB_14 ,IMG_15 ,IMG_THUMB_15
                    ,PROC_STAT ,STAT_BERKAS ,VERIFIED_AT ,VERIFIED_BY ,CREATED_AT ,UPDATED_AT ,DELETED_AT ,DELETED_BY ,NO_WNA
                    , NAMA_LGKP, NAMA_FAM ,TMPT_LHR
                    ,TO_CHAR(TGL_LHR ,'DD-MM-YYYY') TGL_LHR
                    ,JENIS_KLMIN ,AGAMA ,STAT_KWN ,GOL_DRH ,PENDIDIKAN ,PEKERJAAN
                    , SRC_PROV ,SRC_KAB ,SRC_KEC ,SRC_KEL
                    , F5_GET_NAMA_PROVINSI(SRC_PROV) SRC_NAMA_PROV
                    , F5_GET_NAMA_KABUPATEN(SRC_PROV,SRC_KAB) SRC_NAMA_KAB
                    , F5_GET_NAMA_KECAMATAN(SRC_PROV,SRC_KAB ,SRC_KEC) SRC_NAMA_KEC
                    , F5_GET_NAMA_KELURAHAN(SRC_PROV,SRC_KAB ,SRC_KEC ,SRC_KEL ) SRC_NAMA_KEL
                    , SRC_ALAMAT ,SRC_RT ,SRC_RW ,KEC ,KEL
                    , F5_GET_NAMA_KECAMATAN(32,73,KEC) NAMA_KEC
                    , F5_GET_NAMA_KELURAHAN(32,73,KEC ,KEL ) NAMA_KEL
                    , UPPER(F5_GET_REF_WNA(F5_TO_NUMBER(JENIS_KLMIN,7), 801)) JENIS_KLMIN_DESC
                    , UPPER(F5_GET_REF_WNA(F5_TO_NUMBER(AGAMA,7), 901)) AGAMA_DESC
                    , UPPER(F5_GET_REF_WNA(PEKERJAAN, 201)) PEKERJAAN_DESC
                    , UPPER(F5_GET_REF_WNA(PENDIDIKAN, 101)) PENDIDIKAN_DESC
                    , UPPER(F5_GET_REF_WNA(STAT_KWN, 701)) STAT_KWN_DESC
                    , UPPER(F5_GET_REF_WNA(GOL_DRH, 401)) GOL_DRH_DESC
                    , UPPER(F5_GET_REF_NEGARA(KWRNGRN)) KWRNGRN_DESC
                    , KWRNGRN KWRNGRN
                    ,ALAMAT ,NO_RT ,NO_RW ,STATUS
                    , STAT_HBKEL, UPPER(F5_GET_REF_WNA(STAT_HBKEL, 301)) STAT_HBKEL_DESC, JANGKA_WAKTU, JUM_ANGGOTA, TO_CHAR(TGL_DATANG,'DD-MM-YYYY') TGL_DATANG, ALASAN_PINDAH, UPPER(F5_GET_REF_WNA(ALASAN_PINDAH, 601)) ALASAN_PINDAH_DESC
                    ,NO_PASPOR
                    ,TO_CHAR(TGL_PASPOR,'DD-MM-YYYY') TGL_PASPOR
                    ,TO_CHAR(TGL_AKH_PASPOR,'DD-MM-YYYY') TGL_AKH_PASPOR
                    ,NAMA_SPSOR
                    ,TIPE_SPSOR
                    ,UPPER(F5_GET_REF_WNA(TIPE_SPSOR, 601)) TIPE_SPSOR_DESC
                    ,DOK_IMGR
                    , UPPER(F5_GET_REF_WNA(DOK_IMGR, 501)) DOK_IMGR_DESC
                    ,NO_DOK
                    ,TMPT_DTBIT
                    ,TO_CHAR(TGL_DTBIT,'DD-MM-YYYY') TGL_DTBIT
                    ,TO_CHAR(TGL_AKH_DOK,'DD-MM-YYYY') TGL_AKH_DOK
                    ,TO_CHAR(TGL_AKH_DOC_IMGR,'DD-MM-YYYY') TGL_AKH_DOC_IMGR
                    ,TO_CHAR(TGL_STAY,'DD-MM-YYYY') TGL_STAY
                    ,TO_CHAR(TGL_PICKUP,'DD-MM-YYYY') TGL_PICKUP
                    ,TO_CHAR(TGL_DOC_IMIGRASI,'DD-MM-YYYY') TGL_DOC_IMIGRASI
                    ,TO_CHAR(TGL_BERLAKU,'DD-MM-YYYY') TGL_BERLAKU  ,JENIS_LAYANAN,
                    'WNA' JENIS_LAYANAN_DESC
                    FROM PENGAJUANWNA WHERE NO_KK = ".$no_kk." ORDER BY STAT_HBKEL, TGL_LHR DESC");
                  }
                  return response()->json([
                    $query
                  ]);
                }else{
                  return response()->json([
                    'status' => 'error',
                    'data' => 'NOKK Tidak Ada'
                  ]);
                }
              }
              public function get_kk_epunten(Request $request){
                $nik = $request->nik;
                $query=DB::connection('db222')
                ->select("SELECT SUBSTR(URL_DOKUMEN,20) URLONE, SUBSTR(URL_DOKUMEN,11,8) URLSECOND FROM BIODATA_WNI A INNER JOIN DATA_KELUARGA B ON A.NO_KK = B.NO_KK INNER JOIN
                (
                  SELECT
                  NO_DOC
                  , CERT_STATUS
                  , REQ_DATE
                  , REQ_BY
                  , SEQN_ID
                  , URL_DOKUMEN
                  , PEJABAT_PROCCESS_BY
                  , PEJABAT_PROCESS_DATE
                  FROM
                  (
                    SELECT
                    NO_DOC
                    , CERT_STATUS
                    , REQ_DATE
                    , REQ_BY
                    , SEQN_ID
                    , URL_DOKUMEN
                    , PEJABAT_PROCCESS_BY
                    , PEJABAT_PROCESS_DATE
                    , RANK() OVER (PARTITION BY NO_DOC ORDER BY REQ_DATE DESC,SEQN_ID DESC) RNK FROM BSRE_KARTU_KELUARGA)
                    WHERE RNK = 1) C ON B.NO_KK = C.NO_DOC WHERE A.NIK = ".$nik."");
                    if (count($query) > 0){
                      $filePath = 'http://10.32.73.222:8080/Siak/cetak/main/view_pdf/'.$query[0]->urlone.'/'.$query[0]->urlsecond.'/cetak_kartu_keluarga.pdf';
                      $fileContent = file_get_contents($filePath);
                      $response = response($fileContent, 200, [
                        'Content-Type' => 'application/pdf',
                        'Content-Disposition' => 'attachment; filename=kartu_keluarga_'.date("Ymd").'_'.$nik.'.pdf',
                      ]);
                      // return $filePath;
                      return $response;
                    }else{
                      return response()->json([
                        'status' => 'error',
                        'data' => 'KK Tidak Ditemukan'
                      ]);
                    }
                  }

                  public static function pengajuan_path(){
                    return str_replace('\public', '', base_path());
                  }



    public function print(Request $request){
      $key1 = array_search(1, array_column($request->listCetak, 'posisi'));
      $key2 = array_search(2, array_column($request->listCetak, 'posisi'));
      $key3 = array_search(3, array_column($request->listCetak, 'posisi'));
      $key4 = array_search(4, array_column($request->listCetak, 'posisi'));
      $key5 = array_search(5, array_column($request->listCetak, 'posisi'));
      $key6 = array_search(6, array_column($request->listCetak, 'posisi'));
      $key7 = array_search(7, array_column($request->listCetak, 'posisi'));
      $key8 = array_search(8, array_column($request->listCetak, 'posisi'));
      $data1 = 0;
      $data2 = 0;
      $data3 = 0;
      $data4 = 0;
      $data5 = 0;
      $data6 = 0;
      $data7 = 0;
      $data8 = 0;
      $table1 = '';
      $table2 = '';
      $table3 = '';
      $table4 = '';
      $table5 = '';
      $table6 = '';
      $table7 = '';
      $table8 = '';
      $show1 = '';
      $show2 = '';
      $show3 = '';
      $show4 = '';
      $show5 = '';
      $show6 = '';
      $show7 = '';
      $show8 = '';
      if($key1 !== false){
        $data1 = strval($key1);
        $table1 = 'show';
        $show1 = '';
      } else {
        $table1 = 'hide';
        $show1 = 'display: none;';
      }
      if($key2 !== false){
        $data2 = $key2;
        $table2 = 'show';
        $show2 = '';
      } else {
        $table2 = 'hide';
        $show2 = 'display: none;';
      }
      if($key3 !== false){
        $data3 = $key3;
        $table3 = 'show';
        $show3 = '';
      } else {
        $table3 = 'hide';
        $show3 = 'display: none;';
      }
      if($key4 !== false){
        $data4 = $key4;
        $table4 = 'show';
        $show4 = '';
      } else {
        $table4 = 'hide';
        $show4 = 'display: none;';
      }
      if($key5 !== false){
        $data5 = $key5;
        $table5 = 'show';
        $show5 = '';
      } else {
        $table5 = 'hide';
        $show5 = 'display: none;';
      }
      if($key6 !== false){
        $data6 = $key6;
        $table6 = 'show';
        $show6 = '';
      } else {
        $table6 = 'hide';
        $show6 = 'display: none;';
      }
      if($key7 !== false){
        $data7 = strval($key7);
        $table7 = 'show';
        $show7 = '';
      } else {
        $table7 = 'hide';
        $show7 = 'display: none;';
      }
      if($key8 !== false){
        $data8 = strval($key8);
        $table8 = 'show';
        $show8 = '';
      } else {
        $table8 = 'hide';
        $show8 = 'display: none;';
      }
      $destinationPath='print/';
      $ttdkadis=url('/assets/upload/ttd.png');
      $foto=url('/');
      $namakadis='H. TATANG MUHTAR, S.Sos., M.Si.';
      $nipkadis='196806021989031004';

      if (!file_exists($destinationPath)) {
        mkdir($destinationPath, 0777, true);
      }
      $tgl_berlaku = Carbon::now()->addMonths(12)->format('d-m-Y');
      for($i=0; $i < count($request->listCetak); $i++) {
        $nik = $request->listCetak[$i]['nik'];

        $sql= DB::connection('webpunten')->update("update pengajuanskts set proc_stat = 4, tgl_berlaku = to_date('".$tgl_berlaku."','DD-MM-YYYY') where nik = ".$nik." ");
        $user = DB::select("SELECT  ID ,NIK ,NO_KK ,TELEPON ,NAME ,EMAIL ,PASSWORD ,PIC ,API_TOKEN ,ANDROID_TOKEN ,REMEMBER_TOKEN ,VERIFICATION_CODE ,IS_VERIFIED ,NO_PROP ,NO_KAB ,NO_KEC ,NO_KEL ,CREATED_AT ,UPDATED_AT ,DELETED_AT FROM USERS A WHERE EXISTS (SELECT 1 FROM PENGAJUANSKTS@DBEPUNTEN B WHERE A.NO_KK = B.NO_KK AND  NIK =".$nik.")");
        if (!empty($user[0]->telepon)){
          $telepon=$user[0]->telepon;
          $email=$user[0]->email;
          $android_token=$user[0]->android_token;
          $nama_lgkp=$user[0]->name;
          $to_user_id=$user[0]->id;
        }else{
          $telepon=$request->listCetak[$i]['telepon'];
          $email=$request->listCetak[$i]['email'];
          $android_token='';
          $nama_lgkp=$request->listCetak[$i]['nama_lgkp'];
          $to_user_id=$request->listCetak[$i]['nik'];
        }
        
        $now = Carbon::now();
        $query=DB::connection('webpunten')->select("select * from (select tgl_pengambilan,jam,urutan from pengambilan order by tgl_pengambilan desc, tgl_pengambilan)where rownum = 1");
        if($query){
          $tglPengambilan = Date::parse($query[0]->tgl_pengambilan)->format('Y-m-d H:i');
          $tglPengambilan = Date::parse($tglPengambilan)->addMinutes(10);
          $jam= Date::parse($tglPengambilan)->format('H:i');
          $urutan=$query[0]->urutan;
          $hari = $this->hariId(Date::parse($tglPengambilan)->format('l'));
          if($urutan>=50){
            $urutan=1;
            $tglPengambilan = $tglPengambilan->addDays(1);
            $jam='08:00';
            $hari = $this->hariId(Date::parse($tglPengambilan)->format('l'));
          }
          elseif (($urutan < 50) && ($tglPengambilan->format('Y-m-d') <= Carbon::now()->format('Y-m-d'))) {
            $urutan=1;
            $tglPengambilan = $tglPengambilan->addDays(1);
            $jam='08:00';
            $hari = $this->hariId(Date::parse($tglPengambilan)->format('l'));
          }
          else{
            $urutan=$urutan+1;
          }
          if( (Carbon::createFromFormat('H:i', $jam) > Carbon::createFromFormat('H:i', '11:30')) && (Carbon::createFromFormat('H:i', $jam) < Carbon::createFromFormat('H:i', '13:00')) && ($hari == "JUMAT") ) {
            $jam='13:00';
          }
          elseif(Carbon::createFromFormat('H:i', $jam) > Carbon::createFromFormat('H:i', '15:00')){
            $urutan=1;
            $tglPengambilan = $tglPengambilan->addDays(1);
            $jam='08:00';
            $hari = $this->hariId(Date::parse($tglPengambilan)->format('l'));
          }
          else if ((Carbon::createFromFormat('H:i', $jam) > Carbon::createFromFormat('H:i', '12:00') ) && (Carbon::createFromFormat('H:i', $jam) < Carbon::createFromFormat('H:i', '13:00') )) {
            $jam='13:00';
          }
        }
        if(!$query){
          $urutan=1;
          $jam='08:00';
          $tglPengambilan = $now->addDays(1);
          $hari = $this->hariId(Date::parse($tglPengambilan)->format('l'));
        }
        if($tglPengambilan <= $now){
          $tglPengambilan = $now->addDays(1);
          $hari = $this->hariId(Date::parse($tglPengambilan)->format('l'));
        }
        if ($hari=='SABTU'){
          $tglPengambilan = $tglPengambilan->addDays(2);
        }
        if ($hari=='MINGGU'){
          $tglPengambilan = $tglPengambilan->addDays(1);
        }
        $tglPengambilan=Date::parse($tglPengambilan)->format('Y-m-d');
        $query=DB::connection('webpunten')->select("select * from HARI_LIBUR where TANGGAL_LIBUR = TO_DATE('".$tglPengambilan."','yyyy-MM-dd')");
        if($query){
          $jumlah=$query[0]->jumlah_libur;
          //$tglPengambilan = $tglPengambilan->addDays($jumlah);
          // $tglPengambilan = Carbon::createFromFormat('Y-m-d', $tglPengambilan)->addDays($jumlah);
          $tglPengambilan = Carbon::createFromFormat('Y-m-d', $tglPengambilan)->addDays($jumlah);
          $tglPengambilan = Date::parse($tglPengambilan)->format('Y-m-d');
        }
        //$tglPengambilan=Date::parse($tglPengambilan)->format('Y-m-d');
        $hari = $this->hariId(Date::parse($tglPengambilan)->format('l'));
        
        $is_jadwal = DB::connection('webpunten')->select("SELECT COUNT(1) JML FROM PENGAMBILAN WHERE TGL_PENGAMBILAN > SYSDATE AND NIK = ".$nik." ");
        if($is_jadwal[0]->jml == 0){
          if (strpos($nama_lgkp, '\'') !== false) {
          $query=DB::connection('webpunten')->insert("insert into pengambilan (nik,nama_lgkp,tgl_pengambilan,urutan,hari,jam,jenis_dok) select ".$nik.",q'[".$nama_lgkp."]',TO_DATE('".$tglPengambilan." ".$jam."','yyyy-MM-dd hh24:mi'),".$urutan.",'".$hari."','".$jam."','SKTS' from dual where not exists(select 1 from pengambilan where tgl_pengambilan > sysdate and nik = ".$nik.")");
        } else {
          $query=DB::connection('webpunten')->insert("insert into pengambilan (nik,nama_lgkp,tgl_pengambilan,urutan,hari,jam,jenis_dok) select ".$nik.",'".$nama_lgkp."',TO_DATE('".$tglPengambilan." ".$jam."','yyyy-MM-dd hh24:mi'),".$urutan.",'".$hari."','".$jam."','SKTS' from dual where not exists(select 1 from pengambilan where tgl_pengambilan > sysdate and nik = ".$nik.")");
        }
          $subjek = 'Pengajuan Pembuatan SKTS';
          $tglPengambilan=Date::parse($tglPengambilan)->format('d-m-Y');
          $notif = "Kartu SKTS Anda dapat diambil pada :
Hari/Tanggal : ".$hari.", ".$tglPengambilan."
Jam : ".$jam."
Urutan : ".$urutan."
Loket : C
* Dengan membawa berkas yang telah di upload.";
            $ntf = 'pemberitahuan';
            $from_user_id = 'DISDUKCAPIL KOTA BANDUNG';
            AndrApi::sendnotification(null,1,null,$nik,$notif,$ntf,$from_user_id);
            $sql = "INSERT INTO MESSAGES (ID, FROM_ID ,DELETED_FROM ,TO_ID ,DELETED_TO ,TEXT ,READ ,CREATED_AT ,UPDATED_AT ,OPR_READ )
                VALUES (MESSAGES_ID_SEQ.nextval,'$request->nik',NULL,'$nik',NULL,'$notif',0,SYSDATE,SYSDATE,NULL)";
    
          if ($telepon != null){
            $pesan = "Jadwal pengambilan SKTS\n".$hari.", ".$tglPengambilan."\nJam : ".$jam."\nUrutan : ".$urutan."\nLoket : C\n* Bawa semua berkas asli untuk verifikasi data jika melakukan pengajuan online.";
            $query=DB::connection('webpunten')->insert("Insert into OUTBOX@SMSDISDUK
            (TELEPON, ISI_PESAN, TANGGAL_SMS, SERVER)
            Values
            ('".$telepon."', '".$pesan."', TO_DATE('".date('m/d/Y h:i:s')."', 'MM/DD/YYYY HH24:MI:SS'), '1')");
            $this->pushData("by-me", $telepon, $pesan);
            $this->kirimSMS($telepon, $pesan);
          }
        }
        

          
          }
          $kodePrinter =  $request->printer;
          if($kodePrinter == 1){
            // @page { margin:15mm 13mm 0mm 14mm; }
            // @page { margin:15mm 9mm 0mm 10mm; }
            //$pageMargin = "@page { margin:16mm 13mm 0mm 12mm; }";
            $pageMargin = "@page { margin:0mm 0mm 0mm 0mm; }";
          }else{
            $pageMargin = "@page { margin:0mm 0mm 0mm 0mm; }";
          }
          // $font=url('/public/Nimbus-Sans-Light.ttf');
          $html = '
          <!DOCTYPE html>
          <html>
          <head>
          <style>
          '.$pageMargin.'
          *{ font-family: Arial, Helvetica, sans-serif; }
          table
          .hide{color:#FFFFFF;}
          .show{color:#000000;}
          .center{text-align:center;}
          table { }
          table tr td{ }
          table.box {font-size:9px;width:90mm;}
          td.separator{width:9px;text-align:center;}
          td.caption{font-size:9px;width:52px;text-align:left;white-space: nowrap;}
          td.foto{font-size:4px;width:5px;text-align:center;}
          td.isi{width:180px; text-align:left;}
          td {
            padding: 1 !important; margin:0 !important;
          }
          tr {
            padding: 1 !important; margin: 0 !important;
          }
          </style>
          </head>
          <body>
          <div class="my-container">
          <table width="100%" >
          <tbody>
          <tr>
          <td style="width:90mm;height:64mm;">
          <table class="box '.$table8.'" border="0"   cellspacing="0" cellpadding="0" width="100%">
          <tbody>
          <tr>
          <td class="caption">Nomor SKTS</td>
          <td class="separator">:</td>
          <td class="isi" style="white-space:nowrap;">'.$request->listCetak[$data8]['no_skts'].'</td>
          <td style="width:1%; text-align: left; vertical-align: top; '.$show8.'" rowspan="13"><img src="'.$foto.'/'.$request->listCetak[$data8]['pic'].'" width="80px" height="95px" /></td>
          </tr>
          <tr>
          <td class="caption">NIK</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data8]['nik'].'</td>
          </tr>
          <tr>
          <td class="caption">Nama</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data8]['nama_lgkp'].'</td>
          </tr>
          <tr>
          <td class="caption">Jenis Kelamin</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data8]['jenis_klmin'].'</td>
          </tr>
          <tr>
          <td class="caption">Tempat/Tgl Lahir</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data8]['tmpt_lhr'].' , '.Carbon::parse($request->listCetak[$data8]['tgl_lhr'])->format('d-m-Y').'</td>
          </tr>

          <tr>
          <td class="caption">Agama</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data8]['agama'].'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Gol. Darah : '.$request->listCetak[$data8]['gol_drh'].'</td>
          </tr>

          <tr>
          <td class="caption">Pekerjaan</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data8]['pekerjaan'].'</td>
          </tr>
          <tr>
          <td class="caption">Status</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data8]['stat_kwn'].'</td>
          </tr>
          <tr>
          <td class="caption">Alamat Asal</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data8]['src_alamat'].', '.$request->listCetak[$data8]['nama_kec'].', '.$request->listCetak[$data8]['nama_kab'].'</td>
          </tr>
          <tr>
          <td class="caption">Alamat Sementara</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data8]['alamat'].'</td>
          </tr>
          <tr>
          <td class="caption">&nbsp; &nbsp;RT/RW</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data8]['no_rt'].'/'.$request->listCetak[$data8]['no_rw'].'</td>
          </tr>
          <tr>
          <td class="caption">&nbsp; &nbsp;Kelurahan</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data8]['kel'].'</td>
          </tr>
          <tr>
          <td class="caption">&nbsp; &nbsp;Kecamatan</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data8]['kec'].'</td>
          </tr>
          <tr>
          <td class="caption">Berlaku s/d&nbsp;</td>
          <td class="separator">:</td>
          <td class="isi">'.$tgl_berlaku.'</td>
          </tr>
          <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          </tr>
          <tr>
          <td colspan="4">
          <table width="100%">
          <tr>
          <td style="width:1%; white-space:nowrap; text-align: center; vertical-align: top; font-size:6px;" colspan="3">&nbsp;Pemohon</td>
          <td style="text-align: center; font-size:5px; width:1%; white-space:nowrap;">&nbsp;Bandung, '.Carbon::now()->format('d-m-Y').'<br />KEPALA DINAS KEPENDUDUKAN<br />DAN PENCATATAN SIPIL</td>
          </tr>
          <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td style="width:1%; white-space:nowrap;text-align: center; vertical-align: bottom; '.$show8.'"><img style="margin: -30px 0px -20px 0px;" src="'.$ttdkadis.'" width="60px" height=60px" /></td>
          </tr>
          <tr>
          <td style="text-align: center; font-size:6px; width:1%; white-space:nowrap;" colspan="3">'.$request->listCetak[$data8]['nama_lgkp'].'<br />NIK. '.$request->listCetak[$data8]['nik'].'</td>
          <td style="text-align: center; font-size:5px; width:1%; white-space:nowrap;">&nbsp;'.$namakadis.'<br />NIP. '.$nipkadis.'</td>
          </tr>
          </table>
          </td>
          </tr>
          </tbody>
          </table>
          </td>
          <td style="width:90mm;height:64mm;">
          <table class="box '.$table7.'" border="0"   cellspacing="0" cellpadding="0" width="100%">
          <tbody>
          <tr>
          <td class="caption">Nomor SKTS</td>
          <td class="separator">:</td>
          <td class="isi" style="white-space:nowrap;">'.$request->listCetak[$data7]['no_skts'].'</td>
          <td style="width:1%; text-align: left; vertical-align: top; '.$show7.'" rowspan="13"><img src="'.$foto.'/'.$request->listCetak[$data7]['pic'].'" width="80px" height="95px" /></td>
          </tr>
          <tr>
          <td class="caption">NIK</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data7]['nik'].'</td>
          </tr>
          <tr>
          <td class="caption">Nama</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data7]['nama_lgkp'].'</td>
          </tr>
          <tr>
          <td class="caption">Jenis Kelamin</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data7]['jenis_klmin'].'</td>
          </tr>
          <tr>
          <td class="caption">Tempat/Tgl Lahir</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data7]['tmpt_lhr'].' , '.Carbon::parse($request->listCetak[$data7]['tgl_lhr'])->format('d-m-Y').'</td>
          </tr>

          <tr>
          <td class="caption">Agama</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data7]['agama'].'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Gol. Darah : '.$request->listCetak[$data7]['gol_drh'].'</td>
          </tr>

          <tr>
          <td class="caption">Pekerjaan</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data7]['pekerjaan'].'</td>
          </tr>
          <tr>
          <td class="caption">Status</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data7]['stat_kwn'].'</td>
          </tr>
          <tr>
          <td class="caption">Alamat Asal</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data7]['src_alamat'].', '.$request->listCetak[$data7]['nama_kec'].', '.$request->listCetak[$data7]['nama_kab'].'</td>
          </tr>
          <tr>
          <td class="caption">Alamat Sementara</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data7]['alamat'].'</td>
          </tr>
          <tr>
          <td class="caption">&nbsp; &nbsp;RT/RW</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data7]['no_rt'].'/'.$request->listCetak[$data7]['no_rw'].'</td>
          </tr>
          <tr>
          <td class="caption">&nbsp; &nbsp;Kelurahan</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data7]['kel'].'</td>
          </tr>
          <tr>
          <td class="caption">&nbsp; &nbsp;Kecamatan</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data7]['kec'].'</td>
          </tr>
          <tr>
          <td class="caption">Berlaku s/d&nbsp;</td>
          <td class="separator">:</td>
          <td class="isi">'.$tgl_berlaku.'</td>
          </tr>
          <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          </tr>
          <tr>
          <td colspan="4">
          <table width="100%">
          <tr>
          <td style="width:1%; white-space:nowrap; text-align: center; vertical-align: top; font-size:6px;" colspan="3">&nbsp;Pemohon</td>
          <td style="text-align: center; font-size:5px; width:1%; white-space:nowrap;">&nbsp;Bandung, '.Carbon::now()->format('d-m-Y').'<br />KEPALA DINAS KEPENDUDUKAN<br />DAN PENCATATAN SIPIL</td>
          </tr>
          <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td style="width:1%; white-space:nowrap;text-align: center; vertical-align: bottom; '.$show7.'"><img style="margin: -30px 0px -20px 0px;" src="'.$ttdkadis.'" width="60px" height=60px" /></td>
          </tr>
          <tr>
          <td style="text-align: center; font-size:6px; width:1%; white-space:nowrap;" colspan="3">'.$request->listCetak[$data7]['nama_lgkp'].'<br />NIK. '.$request->listCetak[$data7]['nik'].'</td>
          <td style="text-align: center; font-size:5px; width:1%; white-space:nowrap;">&nbsp;'.$namakadis.'<br />NIP. '.$nipkadis.'</td>
          </tr>
          </table>
          </td>
          </tr>
          </tbody>
          </table>
          </td>
          </tr>
          <tr>

          <td style="width:90mm;height:64mm;">
          <table class="box '.$table6.'" border="0"   cellspacing="0" cellpadding="0" width="100%">
          <tbody>
          <tr>
          <td class="caption">Nomor SKTS</td>
          <td class="separator">:</td>
          <td class="isi" style="white-space:nowrap;">'.$request->listCetak[$data6]['no_skts'].'</td>
          <td style="width:1%; text-align: left; vertical-align: top; '.$show6.'" rowspan="13"><img src="'.$foto.'/'.$request->listCetak[$data6]['pic'].'" width="80px" height="95px" /></td>
          </tr>
          <tr>
          <td class="caption">NIK</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data6]['nik'].'</td>
          </tr>
          <tr>
          <td class="caption">Nama</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data6]['nama_lgkp'].'</td>
          </tr>
          <tr>
          <td class="caption">Jenis Kelamin</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data6]['jenis_klmin'].'</td>
          </tr>
          <tr>
          <td class="caption">Tempat/Tgl Lahir</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data6]['tmpt_lhr'].' , '.Carbon::parse($request->listCetak[$data6]['tgl_lhr'])->format('d-m-Y').'</td>
          </tr>

          <tr>
          <td class="caption">Agama</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data6]['agama'].'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Gol. Darah : '.$request->listCetak[$data6]['gol_drh'].'</td>
          </tr>

          <tr>
          <td class="caption">Pekerjaan</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data6]['pekerjaan'].'</td>
          </tr>
          <tr>
          <td class="caption">Status</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data6]['stat_kwn'].'</td>
          </tr>
          <tr>
          <td class="caption">Alamat Asal</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data6]['src_alamat'].', '.$request->listCetak[$data6]['nama_kec'].', '.$request->listCetak[$data6]['nama_kab'].'</td>
          </tr>
          <tr>
          <td class="caption">Alamat Sementara</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data6]['alamat'].'</td>
          </tr>
          <tr>
          <td class="caption">&nbsp; &nbsp;RT/RW</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data6]['no_rt'].'/'.$request->listCetak[$data6]['no_rw'].'</td>
          </tr>
          <tr>
          <td class="caption">&nbsp; &nbsp;Kelurahan</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data6]['kel'].'</td>
          </tr>
          <tr>
          <td class="caption">&nbsp; &nbsp;Kecamatan</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data6]['kec'].'</td>
          </tr>
          <tr>
          <td class="caption">Berlaku s/d&nbsp;</td>
          <td class="separator">:</td>
          <td class="isi">'.$tgl_berlaku.'</td>
          </tr>
          <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          </tr>
          <tr>
          <td colspan="4">
          <table width="100%">
          <tr>
          <td style="width:1%; white-space:nowrap; text-align: center; vertical-align: top; font-size:6px;" colspan="3">&nbsp;Pemohon</td>
          <td style="text-align: center; font-size:5px; width:1%; white-space:nowrap;">&nbsp;Bandung, '.Carbon::now()->format('d-m-Y').'<br />KEPALA DINAS KEPENDUDUKAN<br />DAN PENCATATAN SIPIL</td>
          </tr>
          <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td style="width:1%; white-space:nowrap;text-align: center; vertical-align: bottom; '.$show6.'"><img style="margin: -30px 0px -20px 0px;" src="'.$ttdkadis.'" width="60px" height=60px" /></td>
          </tr>
          <tr>
          <td style="text-align: center; font-size:6px; width:1%; white-space:nowrap;" colspan="3">'.$request->listCetak[$data6]['nama_lgkp'].'<br />NIK. '.$request->listCetak[$data6]['nik'].'</td>
          <td style="text-align: center; font-size:5px; width:1%; white-space:nowrap;">&nbsp;'.$namakadis.'<br />NIP. '.$nipkadis.'</td>
          </tr>
          </table>
          </td>
          </tr>
          </tbody>
          </table>
          </td>
          <td style="width:90mm;height:64mm;">
          <table class="box '.$table5.'" border="0"   cellspacing="0" cellpadding="0" width="100%">
          <tbody>
          <tr>
          <td class="caption">Nomor SKTS</td>
          <td class="separator">:</td>
          <td class="isi" style="white-space:nowrap;">'.$request->listCetak[$data5]['no_skts'].'</td>
          <td style="width:1%; text-align: left; vertical-align: top; '.$show5.'" rowspan="13"><img src="'.$foto.'/'.$request->listCetak[$data5]['pic'].'" width="80px" height="95px" /></td>
          </tr>
          <tr>
          <td class="caption">NIK</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data5]['nik'].'</td>
          </tr>
          <tr>
          <td class="caption">Nama</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data5]['nama_lgkp'].'</td>
          </tr>
          <tr>
          <td class="caption">Jenis Kelamin</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data5]['jenis_klmin'].'</td>
          </tr>
          <tr>
          <td class="caption">Tempat/Tgl Lahir</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data5]['tmpt_lhr'].' , '.Carbon::parse($request->listCetak[$data5]['tgl_lhr'])->format('d-m-Y').'</td>
          </tr>

          <tr>
          <td class="caption">Agama</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data5]['agama'].'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Gol. Darah : '.$request->listCetak[$data5]['gol_drh'].'</td>
          </tr>

          <tr>
          <td class="caption">Pekerjaan</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data5]['pekerjaan'].'</td>
          </tr>
          <tr>
          <td class="caption">Status</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data5]['stat_kwn'].'</td>
          </tr>
          <tr>
          <td class="caption">Alamat Asal</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data5]['src_alamat'].', '.$request->listCetak[$data5]['nama_kec'].', '.$request->listCetak[$data5]['nama_kab'].'</td>
          </tr>
          <tr>
          <td class="caption">Alamat Sementara</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data5]['alamat'].'</td>
          </tr>
          <tr>
          <td class="caption">&nbsp; &nbsp;RT/RW</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data5]['no_rt'].'/'.$request->listCetak[$data5]['no_rw'].'</td>
          </tr>
          <tr>
          <td class="caption">&nbsp; &nbsp;Kelurahan</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data5]['kel'].'</td>
          </tr>
          <tr>
          <td class="caption">&nbsp; &nbsp;Kecamatan</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data5]['kec'].'</td>
          </tr>
          <tr>
          <td class="caption">Berlaku s/d&nbsp;</td>
          <td class="separator">:</td>
          <td class="isi">'.$tgl_berlaku.'</td>
          </tr>
          <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          </tr>
          <tr>
          <td colspan="4">
          <table width="100%">
          <tr>
          <td style="width:1%; white-space:nowrap; text-align: center; vertical-align: top; font-size:6px;" colspan="3">&nbsp;Pemohon</td>
          <td style="text-align: center; font-size:5px; width:1%; white-space:nowrap;">&nbsp;Bandung, '.Carbon::now()->format('d-m-Y').'<br />KEPALA DINAS KEPENDUDUKAN<br />DAN PENCATATAN SIPIL</td>
          </tr>
          <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td style="width:1%; white-space:nowrap;text-align: center; vertical-align: bottom; '.$show5.'"><img style="margin: -30px 0px -20px 0px;" src="'.$ttdkadis.'" width="60px" height=60px" /></td>
          </tr>
          <tr>
          <td style="text-align: center; font-size:6px; width:1%; white-space:nowrap;" colspan="3">'.$request->listCetak[$data5]['nama_lgkp'].'<br />NIK. '.$request->listCetak[$data5]['nik'].'</td>
          <td style="text-align: center; font-size:5px; width:1%; white-space:nowrap;">&nbsp;'.$namakadis.'<br />NIP. '.$nipkadis.'</td>
          </tr>
          </table>
          </td>
          </tr>
          </tbody>
          </table>
          </td>
          </tr>
          <tr>

          <td style="width:90mm;height:68mm;">
          <table class="box '.$table4.'" border="0"   cellspacing="0" cellpadding="0" width="100%">
          <tbody>
          <tr>
          <td class="caption">Nomor SKTS</td>
          <td class="separator">:</td>
          <td class="isi" style="white-space:nowrap;">'.$request->listCetak[$data4]['no_skts'].'</td>
          <td style="width:1%; text-align: left; vertical-align: top; '.$show4.'" rowspan="13"><img src="'.$foto.'/'.$request->listCetak[$data4]['pic'].'" width="80px" height="95px" /></td>
          </tr>
          <tr>
          <td class="caption">NIK</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data4]['nik'].'</td>
          </tr>
          <tr>
          <td class="caption">Nama</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data4]['nama_lgkp'].'</td>
          </tr>
          <tr>
          <td class="caption">Jenis Kelamin</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data4]['jenis_klmin'].'</td>
          </tr>
          <tr>
          <td class="caption">Tempat/Tgl Lahir</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data4]['tmpt_lhr'].' , '.Carbon::parse($request->listCetak[$data4]['tgl_lhr'])->format('d-m-Y').'</td>
          </tr>

          <tr>
          <td class="caption">Agama</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data4]['agama'].'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Gol. Darah : '.$request->listCetak[$data4]['gol_drh'].'</td>
          </tr>

          <tr>
          <td class="caption">Pekerjaan</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data4]['pekerjaan'].'</td>
          </tr>
          <tr>
          <td class="caption">Status</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data4]['stat_kwn'].'</td>
          </tr>
          <tr>
          <td class="caption">Alamat Asal</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data4]['src_alamat'].', '.$request->listCetak[$data4]['nama_kec'].', '.$request->listCetak[$data4]['nama_kab'].'</td>
          </tr>
          <tr>
          <td class="caption">Alamat Sementara</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data4]['alamat'].'</td>
          </tr>
          <tr>
          <td class="caption">&nbsp; &nbsp;RT/RW</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data4]['no_rt'].'/'.$request->listCetak[$data4]['no_rw'].'</td>
          </tr>
          <tr>
          <td class="caption">&nbsp; &nbsp;Kelurahan</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data4]['kel'].'</td>
          </tr>
          <tr>
          <td class="caption">&nbsp; &nbsp;Kecamatan</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data4]['kec'].'</td>
          </tr>
          <tr>
          <td class="caption">Berlaku s/d&nbsp;</td>
          <td class="separator">:</td>
          <td class="isi">'.$tgl_berlaku.'</td>
          </tr>
          <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          </tr>
          <tr>
          <td colspan="4">
          <table width="100%">
          <tr>
          <td style="width:1%; white-space:nowrap; text-align: center; vertical-align: top; font-size:6px;" colspan="3">&nbsp;Pemohon</td>
          <td style="text-align: center; font-size:5px; width:1%; white-space:nowrap;">&nbsp;Bandung, '.Carbon::now()->format('d-m-Y').'<br />KEPALA DINAS KEPENDUDUKAN<br />DAN PENCATATAN SIPIL</td>
          </tr>
          <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td style="width:1%; white-space:nowrap;text-align: center; vertical-align: bottom; '.$show4.'"><img style="margin: -30px 0px -20px 0px;" src="'.$ttdkadis.'" width="60px" height=60px" /></td>
          </tr>
          <tr>
          <td style="text-align: center; font-size:6px; width:1%; white-space:nowrap;" colspan="3">'.$request->listCetak[$data4]['nama_lgkp'].'<br />NIK. '.$request->listCetak[$data4]['nik'].'</td>
          <td style="text-align: center; font-size:5px; width:1%; white-space:nowrap;">&nbsp;'.$namakadis.'<br />NIP. '.$nipkadis.'</td>
          </tr>
          </table>
          </td>
          </tr>
          </tbody>
          </table>
          </td>
          <td style="width:90mm;height:68mm;">
          <table class="box '.$table3.'" border="0"   cellspacing="0" cellpadding="0" width="100%">
          <tbody>
          <tr>
          <td class="caption">Nomor SKTS</td>
          <td class="separator">:</td>
          <td class="isi" style="white-space:nowrap;">'.$request->listCetak[$data3]['no_skts'].'</td>
          <td style="width:1%; text-align: left; vertical-align: top; '.$show3.'" rowspan="13"><img src="'.$foto.'/'.$request->listCetak[$data3]['pic'].'" width="80px" height="95px" /></td>
          </tr>
          <tr>
          <td class="caption">NIK</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data3]['nik'].'</td>
          </tr>
          <tr>
          <td class="caption">Nama</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data3]['nama_lgkp'].'</td>
          </tr>
          <tr>
          <td class="caption">Jenis Kelamin</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data3]['jenis_klmin'].'</td>
          </tr>
          <tr>
          <td class="caption">Tempat/Tgl Lahir</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data3]['tmpt_lhr'].' , '.Carbon::parse($request->listCetak[$data3]['tgl_lhr'])->format('d-m-Y').'</td>
          </tr>

          <tr>
          <td class="caption">Agama</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data3]['agama'].'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Gol. Darah : '.$request->listCetak[$data3]['gol_drh'].'</td>
          </tr>

          <tr>
          <td class="caption">Pekerjaan</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data3]['pekerjaan'].'</td>
          </tr>
          <tr>
          <td class="caption">Status</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data3]['stat_kwn'].'</td>
          </tr>
          <tr>
          <td class="caption">Alamat Asal</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data3]['src_alamat'].', '.$request->listCetak[$data3]['nama_kec'].', '.$request->listCetak[$data3]['nama_kab'].'</td>
          </tr>
          <tr>
          <td class="caption">Alamat Sementara</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data3]['alamat'].'</td>
          </tr>
          <tr>
          <td class="caption">&nbsp; &nbsp;RT/RW</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data3]['no_rt'].'/'.$request->listCetak[$data3]['no_rw'].'</td>
          </tr>
          <tr>
          <td class="caption">&nbsp; &nbsp;Kelurahan</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data3]['kel'].'</td>
          </tr>
          <tr>
          <td class="caption">&nbsp; &nbsp;Kecamatan</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data3]['kec'].'</td>
          </tr>
          <tr>
          <td class="caption">Berlaku s/d&nbsp;</td>
          <td class="separator">:</td>
          <td class="isi">'.$tgl_berlaku.'</td>
          </tr>
          <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          </tr>
          <tr>
          <td colspan="4">
          <table width="100%">
          <tr>
          <td style="width:1%; white-space:nowrap; text-align: center; vertical-align: top; font-size:6px;" colspan="3">&nbsp;Pemohon</td>
          <td style="text-align: center; font-size:5px; width:1%; white-space:nowrap;">&nbsp;Bandung, '.Carbon::now()->format('d-m-Y').'<br />KEPALA DINAS KEPENDUDUKAN<br />DAN PENCATATAN SIPIL</td>
          </tr>
          <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td style="width:1%; white-space:nowrap;text-align: center; vertical-align: bottom; '.$show3.'"><img style="margin: -30px 0px -20px 0px;" src="'.$ttdkadis.'" width="60px" height=60px" /></td>
          </tr>
          <tr>
          <td style="text-align: center; font-size:6px; width:1%; white-space:nowrap;" colspan="3">'.$request->listCetak[$data3]['nama_lgkp'].'<br />NIK. '.$request->listCetak[$data3]['nik'].'</td>
          <td style="text-align: center; font-size:5px; width:1%; white-space:nowrap;">&nbsp;'.$namakadis.'<br />NIP. '.$nipkadis.'</td>
          </tr>
          </table>
          </td>
          </tr>
          </tbody>
          </table>
          </td>
          </tr>
          <tr>
          
          <td style="width:90mm;height:64mm;">
          <table class="box '.$table2.'" border="0"   cellspacing="0" cellpadding="0" width="100%">
          <tbody>
          <tr>
          <td class="caption">Nomor SKTS</td>
          <td class="separator">:</td>
          <td class="isi" style="white-space:nowrap;">'.$request->listCetak[$data2]['no_skts'].'</td>
          <td style="width:1%; text-align: left; vertical-align: top; '.$show2.'" rowspan="13"><img src="'.$foto.'/'.$request->listCetak[$data2]['pic'].'" width="80px" height="95px" /></td>
          </tr>
          <tr>
          <td class="caption">NIK</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data2]['nik'].'</td>
          </tr>
          <tr>
          <td class="caption">Nama</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data2]['nama_lgkp'].'</td>
          </tr>
          <tr>
          <td class="caption">Jenis Kelamin</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data2]['jenis_klmin'].'</td>
          </tr>
          <tr>
          <td class="caption">Tempat/Tgl Lahir</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data2]['tmpt_lhr'].' , '.Carbon::parse($request->listCetak[$data2]['tgl_lhr'])->format('d-m-Y').'</td>
          </tr>

          <tr>
          <td class="caption">Agama</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data2]['agama'].'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Gol. Darah : '.$request->listCetak[$data2]['gol_drh'].'</td>
          </tr>

          <tr>
          <td class="caption">Pekerjaan</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data2]['pekerjaan'].'</td>
          </tr>
          <tr>
          <td class="caption">Status</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data2]['stat_kwn'].'</td>
          </tr>
          <tr>
          <td class="caption">Alamat Asal</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data2]['src_alamat'].', '.$request->listCetak[$data2]['nama_kec'].', '.$request->listCetak[$data2]['nama_kab'].'</td>
          </tr>
          <tr>
          <td class="caption">Alamat Sementara</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data2]['alamat'].'</td>
          </tr>
          <tr>
          <td class="caption">&nbsp; &nbsp;RT/RW</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data2]['no_rt'].'/'.$request->listCetak[$data2]['no_rw'].'</td>
          </tr>
          <tr>
          <td class="caption">&nbsp; &nbsp;Kelurahan</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data2]['kel'].'</td>
          </tr>
          <tr>
          <td class="caption">&nbsp; &nbsp;Kecamatan</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data2]['kec'].'</td>
          </tr>
          <tr>
          <td class="caption">Berlaku s/d&nbsp;</td>
          <td class="separator">:</td>
          <td class="isi">'.$tgl_berlaku.'</td>
          </tr>
          <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          </tr>
          <tr>
          <td colspan="4">
          <table width="100%">
          <tr>
          <td style="width:1%; white-space:nowrap; text-align: center; vertical-align: top; font-size:6px;" colspan="3">&nbsp;Pemohon</td>
          <td style="text-align: center; font-size:5px; width:1%; white-space:nowrap;">&nbsp;Bandung, '.Carbon::now()->format('d-m-Y').'<br />KEPALA DINAS KEPENDUDUKAN<br />DAN PENCATATAN SIPIL</td>
          </tr>
          <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td style="width:1%; white-space:nowrap;text-align: center; vertical-align: bottom; '.$show2.'"><img style="margin: -30px 0px -20px 0px;" src="'.$ttdkadis.'" width="60px" height=60px" /></td>
          </tr>
          <tr>
          <td style="text-align: center; font-size:6px; width:1%; white-space:nowrap;" colspan="3">'.$request->listCetak[$data2]['nama_lgkp'].'<br />NIK. '.$request->listCetak[$data2]['nik'].'</td>
          <td style="text-align: center; font-size:5px; width:1%; white-space:nowrap;">&nbsp;'.$namakadis.'<br />NIP. '.$nipkadis.'</td>
          </tr>
          </table>
          </td>
          </tr>
          </tbody>
          </table>
          </td>
          <td style="width:90mm;height:64mm;">
          <table class="box '.$table1.'" border="0"   cellspacing="0" cellpadding="0" width="100%">
          <tbody>
          <tr>
          <td class="caption">Nomor SKTS</td>
          <td class="separator">:</td>
          <td class="isi" style="white-space:nowrap;">'.$request->listCetak[$data1]['no_skts'].'</td>
          <td style="width:1%; text-align: left; vertical-align: top; '.$show1.'" rowspan="13"><img src="'.$foto.'/'.$request->listCetak[$data1]['pic'].'" width="80px" height="95px" /></td>
          </tr>
          <tr>
          <td class="caption">NIK</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data1]['nik'].'</td>
          </tr>
          <tr>
          <td class="caption">Nama</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data1]['nama_lgkp'].'</td>
          </tr>
          <tr>
          <td class="caption">Jenis Kelamin</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data1]['jenis_klmin'].'</td>
          </tr>
          <tr>
          <td class="caption">Tempat/Tgl Lahir</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data1]['tmpt_lhr'].' , '.Carbon::parse($request->listCetak[$data1]['tgl_lhr'])->format('d-m-Y').'</td>
          </tr>

          <tr>
          <td class="caption">Agama</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data1]['agama'].'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Gol. Darah : '.$request->listCetak[$data1]['gol_drh'].'</td>
          </tr>

          <tr>
          <td class="caption">Pekerjaan</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data1]['pekerjaan'].'</td>
          </tr>
          <tr>
          <td class="caption">Status</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data1]['stat_kwn'].'</td>
          </tr>
          <tr>
          <td class="caption">Alamat Asal</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data1]['src_alamat'].', '.$request->listCetak[$data1]['nama_kec'].', '.$request->listCetak[$data1]['nama_kab'].'</td>
          </tr>
          <tr>
          <td class="caption">Alamat Sementara</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data1]['alamat'].'</td>
          </tr>
          <tr>
          <td class="caption">&nbsp; &nbsp;RT/RW</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data1]['no_rt'].'/'.$request->listCetak[$data1]['no_rw'].'</td>
          </tr>
          <tr>
          <td class="caption">&nbsp; &nbsp;Kelurahan</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data1]['kel'].'</td>
          </tr>
          <tr>
          <td class="caption">&nbsp; &nbsp;Kecamatan</td>
          <td class="separator">:</td>
          <td class="isi">'.$request->listCetak[$data1]['kec'].'</td>
          </tr>
          <tr>
          <td class="caption">Berlaku s/d&nbsp;</td>
          <td class="separator">:</td>
          <td class="isi">'.$tgl_berlaku.'</td>
          </tr>
          <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          </tr>
          <tr>
          <td colspan="4">
          <table width="100%">
          <tr>
          <td style="width:1%; white-space:nowrap; text-align: center; vertical-align: top; font-size:6px;" colspan="3">&nbsp;Pemohon</td>
          <td style="text-align: center; font-size:5px; width:1%; white-space:nowrap;">&nbsp;Bandung, '.Carbon::now()->format('d-m-Y').'<br />KEPALA DINAS KEPENDUDUKAN<br />DAN PENCATATAN SIPIL</td>
          </tr>
          <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td style="width:1%; white-space:nowrap;text-align: center; vertical-align: bottom; '.$show1.'"><img style="margin: -30px 0px -20px 0px;" src="'.$ttdkadis.'" width="60px" height=60px" /></td>
          </tr>
          <tr>
          <td style="text-align: center; font-size:6px; width:1%; white-space:nowrap;" colspan="3">'.$request->listCetak[$data1]['nama_lgkp'].'<br />NIK. '.$request->listCetak[$data1]['nik'].'</td>
          <td style="text-align: center; font-size:5px; width:1%; white-space:nowrap;">&nbsp;'.$namakadis.'<br />NIP. '.$nipkadis.'</td>
          </tr>
          </table>
          </td>
          </tr>
          </tbody>
          </table>
          </td>
          </tr>
          </tbody>
          </table>
          </div>
          </body>
          </html>';
          $user = $request->nik;
          $namaFile = Carbon::now()->format('d-m-Y-His');
          $url = $destinationPath.$user.'-'.$namaFile.'.pdf';
          //$customPaper = array(0,0,507.4016,737.008);
          Browsershot::html($html)
          ->fullPage()
          ->margins(0, 0, 0, 0)
          ->showBackground()
          ->waitUntilNetworkIdle()
          ->paperSize(179,260)
          ->save($url);


          //$customPaper = array(0,0,507.4016,737.008);
          //PDF::loadHTML($html)->setPaper($customPaper, 'portraits')



          // PDF::loadHTML($html)->setPaper([179,260,179,260], 'portraits')
          //->setWarnings(false)
          //->save($url);
          $size  = count($request->listCetak);
          $tgl = Carbon::now();
          for ($i = 0; $i < $size; $i++){
            $print = new HistPrint;
            $print->nik = $request->listCetak[$i]['nik'];
            $print->nama_lgkp = $request->listCetak[$i]['nama_lgkp'];
            $print->tgl_cetak = $tgl;
            $print->printed_by = $request->nik;
            $print->save();
          }
          return $url;
        }

        

        public function hariId($hari)
      {
        if($hari == 'Sunday') {
          return "MINGGU";
        } else if($hari == 'Monday') {
          return "SENIN";
        }
        else if($hari == 'Tuesday') {
          return "SELASA";
        }
        else if($hari == 'Wednesday') {
          return "RABU";
        }
        else if($hari == 'Thursday') {
          return "KAMIS";
        }
        else if($hari == 'Friday') {
          return "JUMAT";
        }
        else if($hari == 'Saturday') {
          return "SABTU";
        } else {
          return $hari;
        }
      }

      public function pushData($jenis, $telepon, $pesan)
        {
          $client = new Client();
          $socketUrl = env("SOCKET_URL", null);
          if($socketUrl != null) {
            $result = $client->request('POST', $socketUrl, [
              'body' => json_encode([
                'jenis' => $jenis,
                'telepon' => $telepon,
                'isi_pesan' => $pesan,
                'tanggal' => date('d-M-Y h:i:s'),
                'waktu' => date('Ymdhis'),
              ], JSON_PRESERVE_ZERO_FRACTION),
              'headers' => ['Content-Type' => 'application/json']
            ]);
          }
        }

        public function kirimSMS($telepon, $pesan)
        {
          $client = new Client();
          $socketUrl = env("E_SPASI_URL", null);
          if($socketUrl != null) {
            $result = $client->request('POST', $socketUrl, [
              'body' => json_encode([
                'telepon' => $telepon,
                'pesan' => $pesan
              ], JSON_PRESERVE_ZERO_FRACTION),
              'headers' => ['Content-Type' => 'application/json']
            ]);
          }
        }
        public function epunten_test(){
          echo $this->get_detail_wni(301, $stathbkle);
        }

        public function get_detail_wni($sect, $no){
          $sql = "SELECT DESCRIP FROM REF_SIAK_WNI WHERE SECT = $sect AND NO =$no";
          $r =DB::connection('webpunten')->select($sql);
          return $r[0]->descrip;;
        }

        public function get_detail_wna($sect, $no){
          $sql = "SELECT DESCRIP FROM REF_SIAK_WNA WHERE SECT = $sect AND NO =$no";
          $r =DB::connection('webpunten')->select($sql);
          return $r[0]->descrip;;
        }
         public function get_detail_negara($sect){
          $sql = "SELECT DESCRIP FROM REF_NEGARA WHERE KODE_NUM = $sect";
          $r =DB::connection('webpunten')->select($sql);
          return $r[0]->descrip;;
        }
    }


