<?php

namespace App\Http\Controllers;

use DB;
use Session;
use DateTime;
use PDF;

use Illuminate\Http\Request;
use App\Http\Controllers\SharedController as Shr;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class MasterController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


    public function mst_salaman(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 174;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$r = $this->get_master_salaman();	
			
			$data = array(
		 		"stitle"=>'Master Pengajuan Layanan Online',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
       			"data"=>$r,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			return view('master_salaman/main',$data);
		}else{
			return redirect()->route('logout');
		}
		
	}


    public function mst_edatang(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 327;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$r = $this->get_master_edatang();	
			
			$data = array(
		 		"stitle"=>'Master Pengajuan Epunten Kedatangan',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
       			"data"=>$r,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			return view('master_edatang/main',$data);
		}else{
			return redirect()->route('logout');
		}
		
	}
    public function tte_kk(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
		    $menu_id = 173;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$r = $this->get_master_tte();	
			$data = array(
		 		"stitle"=>'Master Pengajuan TTE Kartu Keluarga',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
       			"data"=>$r,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			return view('master_tte/main',$data);
		}else{
			return redirect()->route('logout');
		}

	}
    public function tte_detail(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
		    $menu_id = 173;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$r = $this->get_master_tte_detail();	
			$data = array(
		 		"stitle"=>'Master Pengajuan TTE',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
       			"data"=>$r,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			return view('master_tte_detail/main',$data);
		}else{
			return redirect()->route('logout');
		}

	}

    public function tte_kelahiran(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 174;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$r = $this->get_master_tte_kelahiran();	
			
			$data = array(
		 		"stitle"=>'Master Pengajuan TTE Kelahiran',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
       			"data"=>$r,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			return view('master_tte_nonwil/main',$data);
		}else{
			return redirect()->route('logout');
		}
		
	}
	public function tte_kematian(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 174;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$r = $this->get_master_tte_kematian();	
			
			$data = array(
		 		"stitle"=>'Master Pengajuan TTE Kematian',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
       			"data"=>$r,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			return view('master_tte_nonwil/main',$data);
		}else{
			return redirect()->route('logout');
		}
		
	}
	public function tte_perpindahan(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 174;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$r = $this->get_master_tte_perpindahan();	
			
			$data = array(
		 		"stitle"=>'Master Pengajuan TTE Perpindahan',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
       			"data"=>$r,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			return view('master_tte_nonwil/main',$data);
		}else{
			return redirect()->route('logout');
		}
		
	}
	public function tte_perkawinan(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 265;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$r = $this->get_master_tte_perkawinan();	
			
			$data = array(
		 		"stitle"=>'Master Pengajuan TTE Perkawinan',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
       			"data"=>$r,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			return view('master_tte_nonwil/main',$data);
		}else{
			return redirect()->route('logout');
		}
		
	}
	public function tte_perceraian(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 266;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$r = $this->get_master_tte_perceraian();	
			
			$data = array(
		 		"stitle"=>'Master Pengajuan TTE Perceraian',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
       			"data"=>$r,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			return view('master_tte_nonwil/main',$data);
		}else{
			return redirect()->route('logout');
		}
		
	}
	public function tte_pengesahananak(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 267;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$r = $this->get_master_tte_pengesahananak();	
			
			$data = array(
		 		"stitle"=>'Master Pengajuan TTE Pengesahan Anak',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
       			"data"=>$r,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			return view('master_tte_nonwil/main',$data);
		}else{
			return redirect()->route('logout');
		}
		
	}
	public function tte_pengakuananak(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 268;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$r = $this->get_master_tte_pengakuananak();	
			
			$data = array(
		 		"stitle"=>'Master Pengajuan TTE Pengakuan Anak',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
       			"data"=>$r,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			return view('master_tte_nonwil/main',$data);
		}else{
			return redirect()->route('logout');
		}
		
	}
	public function master_activity(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 11;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$r = $this->get_master_activity();	
			$r2 = $this->get_master_activity_dinas();
			
			$data = array(
		 		"stitle"=>'Master Activity Page',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"data"=>$r,
		 		"data2"=>$r2,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			return view('master_activity/main',$data);
		}else{
			return redirect()->route('logout');
		}
		
	}
	public function master_list(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 12;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$r = $this->get_master_list();	
			
			$data = array(
		 		"stitle"=>'Master List Page',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"data"=>$r,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			return view('master_list/main',$data);
		}else{
			return redirect()->route('logout');
		}
		
	}
	public function master_list_download(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {

		    $menu_id = 12;
			$is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$r = $this->get_master_list();	
			$data = array(
		 		"stitle"=>'Master List Download',
		 		"stabletitle"=>'Master List Download',
		 		"back_title"=>'Master List Page',
		 		"backurl"=>'List',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"data"=>$r,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			return view('master_list_download/main',$data);
		}else{
			return redirect()->route('logout');
		}

	}
	public function master_list_suket(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 12;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$r = $this->get_master_list_suket();	
			
			$data = array(
		 		"stitle"=>'Master List Suket Page',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"data"=>$r,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			return view('master_list_suket/main',$data);
		}else{
			return redirect()->route('logout');
		}
		
	}
	public function master_request(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 13;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$r = $this->get_master_pengajuan();	
			
			$data = array(
		 		"stitle"=>'Master Pengajuan Page',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"data"=>$r,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			return view('master_pengajuan/main',$data);
		}else{
			return redirect()->route('logout');
		}
		
	}
	public function master_absensi(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 14;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$r = $this->get_master_absensi();	
			$r2 = [];
			
			$data = array(
		 		"stitle"=>'Master Absensi Page',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"data"=>$r,
		 		"data2"=>$r2,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			return view('master_absensi/main',$data);
		}else{
			return redirect()->route('logout');
		}
		
	}
	public function master_contact(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 122;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$r = $this->get_contact_kec();	
			$data = array(
		 		"stitle"=>'Contact Page',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"theuser"=>$r,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			return view('Contact/main',$data);
		}else{
			return redirect()->route('logout');
		}
		
	}
	public function Activity_setting(Request $request)
	{
		
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$r = $this->act_get_user($request->session()->get('S_USER_ID'));
			$data = array(
		 		"stitle"=>'Account Setting',
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"data"=>$r,
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_kantor"=>$request->session()->get('S_NAMA_KANTOR'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			return view('Activity_setting/main',$data);
		}else{
			return redirect()->route('logout');
		}
		
	}
	public function Activity_do_save(Request $request){
		header('Content-type: application/json');
		$user_name = $request->user_name;
		$old_password = md5($request->old_password);
		$new_password = md5($request->new_password);
		$r = $this->lgn_check_user($user_name, $old_password);
		if (count($r) > 0) {
			$r = $r[0];
			if ($r->user_pwd == $old_password ) {
				$r = $this->lgn_change_pwd($user_name, $new_password);
				$output = array(
		    			"message_type"=>1,
		    			"message"=>"Your Password Has Been Changed Successfully! Thank you."
		    	);
			}else{
				$output = array(
		    			"message_type"=>0,
		    			"message"=>"Old Password Incorect For User $user_name Please Try Again"
		    		);
			}
		}else{
				$output = array(
	    			"message_type"=>0,
	    			"message"=>"Sorry User $user_name Not Found, Please Contact Admin For Registration"
	    		);
		}
		return $output;
	}
	public function activity(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 136;
			$is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$data = array(
		 		"stitle"=>'My Activity Page',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_nama_kantor"=>$request->session()->get('S_NAMA_KANTOR'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			return view('Activity/main',$data);
		}else{
			return redirect()->route('logout');
		}
		
	}
	public function activity_rekap(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 137;
			$is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$data = array(
		 		"stitle"=>'Rekap My Activity Siak ',
		 		"mtitle"=>'Rekap My Activity Siak ',
		 		"my_url"=>'UserLevel',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			return view('Rekap_siak/main',$data);
		}else{
			return redirect()->route('logout');
		}
		
	}
	public function absensi_daily(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 48;
			$is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->user_id != null){
			$user_id = $request->user_id;
			$r = $this->act_cek_daily_absensi($user_id);
			$j = $this->act_cek_count_daily_absensi($user_id);
			$data = array(
		 		"stitle"=>$request->user_id.' Rekap Daily Absensi',
		 		"mtitle"=>$request->user_id.' Rekap Daily Absensi',
		 		"my_url"=>'absensi_daily',
		 		"type_tgl"=>'Absensi',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"data"=>$r,
		 		"jumlah"=>$j,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			}else{
			$data = array(
		 		"stitle"=>'Rekap Daily Absensi',
		 		"mtitle"=>'Rekap Daily Absensi',
		 		"my_url"=>'absensi_daily',
		 		"type_tgl"=>'Absensi',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
    		}
			return view('absensi_daily/main',$data);
		}else{
			return redirect()->route('logout');
		}
		
	}
	public function activity_daily(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 47;
			$is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->user_id != null){
			$user_id = $request->user_id;
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$user_id = $request->user_id;
			$r = $this->act_cek_daily_activity($tgl_start,$tgl_end,$user_id);
			$j = $this->act_cek_count_daily_activity($tgl_start,$tgl_end,$user_id);
			$data = array(
		 		"stitle"=>$request->user_id.' Rekap Daily Report',
		 		"mtitle"=>$request->user_id.' Rekap Daily Report',
		 		"my_url"=>'activity_daily',
		 		"type_tgl"=>'Report',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"data"=>$r,
		 		"jumlah"=>$j,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			}else{
			$data = array(
		 		"stitle"=>'Rekap Daily Report',
		 		"mtitle"=>'Rekap Daily Report',
		 		"my_url"=>'activity_daily',
		 		"type_tgl"=>'Report',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
    		}
			return view('activity_daily/main',$data);
		}else{
			return redirect()->route('logout');
		}
		
	}
	public function kk_detail(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 158;
			$is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$data = array(
		 		"stitle"=>'Daftar Pencetakan Kartu Keluarga ',
		 		"mtitle"=>'Daftar Pencetakan Kartu Keluarga ',
		 		"my_url"=>'UserLevel',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			return view('Rekap_detail_kk/main',$data);
		}else{
			return redirect()->route('logout');
		}
		
	}
	public function kk_bulanan(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 159;
			$is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->no_kec != null){
	          $no_kec = $request->no_kec;
	          $no_kel = 0;
	          $tgl = $request->tanggal;
	          $val = $this->get_pivot_kel_str($no_kec,$no_kel);
	          $j = $this->get_jum_kel($no_kec,$no_kel);
	          $r = $this->rekap_detail_kk_bulan($request->session()->get('S_USER_ID'),$tgl,$no_kec,$no_kel,$val);

			$data = array(
		 		"stitle"=>'Detail Cetak KK User '.$request->session()->get('S_NAMA_LGKP').' Bulan '.$tgl ,
		 		"mtitle"=>'Detail Cetak KK User '.$request->session()->get('S_NAMA_LGKP').' Bulan '.$tgl,
		 		"my_url"=>'Bulanan',
		 		"type_tgl"=>'Tanggal Rekam',
		 		"data"=>$r,
		 		"nama_wil"=>$j,
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			}else{
			$data = array(
		 		"stitle"=>'Detail KK Bulanan',
		 		"mtitle"=>'Detail KK Bulanan',
		 		"my_url"=>'Bulanan',
		 		"type_tgl"=>'Tanggal Rekam',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
    		}
			return view('Rekap_bulan_kk/main',$data);
		}else{
			return redirect()->route('logout');
		}
		
	}
	public function kk_Rekap(Request $request) 
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 160;
			$is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$data = array(
		 		"stitle"=>'Daftar Pencetakan Kartu Keluarga ',
		 		"mtitle"=>'Daftar Pencetakan Kartu Keluarga ',
		 		"my_url"=>'UserLevel',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			return view('Rekap_kk/main',$data);
		}else{
			return redirect()->route('logout');
		}

	}
	public function activitykedatangan(Request $request) 
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 182;
			$is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$data = array(
		 		"stitle"=>'Rekap Kedatangan Siak ',
		 		"mtitle"=>'Rekap Kedatangan Siak ',
		 		"my_url"=>'kedatangan',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			return view('Rekap_siak_kedatangan/main',$data);
		}else{
			return redirect()->route('logout');
		}
		
	}
	public function activityPerpindahan(Request $request) 
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 182;
			$is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$data = array(
		 		"stitle"=>'Rekap Perpindahan Siak ',
		 		"mtitle"=>'Rekap Perpindahan Siak ',
		 		"my_url"=>'Perpindahan',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			return view('Rekap_siak_perpindahan/main',$data);
		}else{
			return redirect()->route('logout');
		}
		
	}

	public function laporankedatangan(Request $request) 
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 329;
			$is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$data = array(
		 		"stitle"=>'Laporan Pindah Datang ',
		 		"mtitle"=>'MONITORING LAYANAN PERPINDAHAN PENDUDUK ',
		 		// "mtitle"=>'SISTEM LAPORAN PERPINDAHAN PENDUDUK (SILADUDUK) ',
		 		"my_url"=>'LaporanKedatangan',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			return view('Rekap_laporan_kedatangan/main',$data);
		}else{
			return redirect()->route('logout');
		}
		
	}
	public function laporanperpindahan(Request $request) 
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 330;
			$is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$data = array(
		 		"stitle"=>'Laporan Pindah Keluar ',
		 		// "mtitle"=>'SISTEM LAPORAN PERPINDAHAN PENDUDUK (SILADUDUK) ',
		 		"mtitle"=>'MONITORING LAYANAN PERPINDAHAN PENDUDUK ',
		 		"my_url"=>'LaporanPerpindahan',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			return view('Rekap_laporan_perpindahan/main',$data);
		}else{
			return redirect()->route('logout');
		}
		
	}
	
	public function kk_pdf_detail(Request $request) {
		if($request->pdf_tanggal != null){
			$pdf_userid = Session::get('S_USER_ID');
			$tgl = $request->pdf_tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $request->pdf_no_kec;
			$no_kel = $request->pdf_no_kel;
			$fileName = strtoupper(Session::get('S_USER_ID')).'_activity_data_'.time().'.xlsx';  
			$activity_data =  $this->rekap_detail_kk_pdf($pdf_userid,$tgl_start,$tgl_end,$no_kec,$no_kel);
			$atasan =  $this->get_atasan($pdf_userid);
			// set document information
			PDF::SetCreator('Dian Yanzen');
			PDF::SetAuthor('Dian Yanzen');
			PDF::SetTitle('PDF Monitoring');
			PDF::SetSubject('YZ PDF');
			PDF::SetKeywords('YZ, PDF, Monitoring, DisdukCapil, Kota Bandung');
			// set auto page breaks
			PDF::SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
			// set image scale factor
			PDF::setImageScale(PDF_IMAGE_SCALE_RATIO);
			// ---------------------------------------------------------
			// set font
			// set default header data
			PDF::SetMargins(PDF_MARGIN_LEFT, 45, PDF_MARGIN_RIGHT);
			PDF::SetHeaderMargin(PDF_MARGIN_HEADER);
			PDF::SetFooterMargin(PDF_MARGIN_FOOTER);
			// set font
			PDF::SetFont('times', '', 12);
			PDF::AddPage();
			// add a page
			$html = '
			<!-- EXAMPLE OF CSS STYLE -->
			<style>
			  table.first {
			    padding: 0px;
			    margin: 0px;
			    font-family: helvetica;
			    font-size: 7pt;
			  }
			</style>
			<table class="first" cellpadding="0" cellspacing="5" border="0">
			  <tr>
			    <td width="50" align="left">Nama
			    </td>
			    <td width="20" align="center">:
			    </td>
			    <td width="140" align="left"><b>'.Session::get('S_NAMA_LGKP').'</b>
			    </td>
			    <td width="80" align="center">
			    </td>
			    <td width="60" align="center">
			    </td>
			    <td width="20" align="center">
			    </td>
			    <td width="220" align="left">
			    </td>
			  </tr>
			  <tr>
			    <td width="50" align="left">Nik
			    </td>
			    <td width="20" align="center">:
			    </td>
			    <td width="140" align="left">'.Session::get('S_NIK').'
			    </td>
			    <td width="80" align="center">
			    </td>
			    <td width="60" align="center">
			    </td>
			    <td width="20" align="center">
			    </td>
			    <td width="220" align="left">
			    </td>
			  </tr>
			  <tr>
			    <td width="50" align="left">Perihal
			    </td>
			    <td width="20" align="center">:
			    </td>
			    <td width="210" align="left">Daftar Pencetakan Kartu Keluarga
			    </td>
			    <td width="10" align="center">
			    </td>
			    <td width="60" align="right">
			    </td>
			    <td width="20" align="center">
			    </td>
			    <td width="220" align="left">
			    </td>
			  </tr>
			  <tr>
			    <td width="50" align="left">Tanggal
			    </td>
			    <td width="20" align="center">:
			    </td>
			    <td width="210" align="left">'.$tgl.'
			    </td>
			    <td width="10" align="center">
			    </td>
			    <td width="60" align="right">
			    </td>
			    <td width="20" align="center">
			    </td>
			    <td width="220" align="left">
			    </td>
			  </tr>
			</table>
			<br>
			<br>
			<table id="demo-foo-row-toggler" class="first" data-page-size="100" cellpadding="10">
			  <thead>
			    <tr >
			      <th width="10%" style="border: 2px solid #e4e7ea; C text-align: center;">No
			      </th>
			      <th width="17%" style="border: 2px solid #e4e7ea;text-align: center;">Nomor KK
			      </th>
			      <th width="20%" style="border: 2px solid #e4e7ea;text-align: center;">Nama Kepala Keluarga
			      </th>
			      <th width="15%" style="border: 2px solid #e4e7ea;text-align: center;">Tanggal Pengajuan
			      </th>
			      <th width="15%" style="border: 2px solid #e4e7ea;text-align: center;">Tanggal Cetak
			      </th>
			      <th width="18%" style="border: 2px solid #e4e7ea;text-align: center;">Keterangan
			      </th>
			    </tr>
			  </thead>
			  <tbody id="my_data" style="border: 1px solid #e4e7ea;">
			    ';
			    $style = array('border' => 0, 'vpadding' => 'auto', 'hpadding' => 'auto', 'fgcolor' => array(0, 0, 0), 'bgcolor' => false, 'module_width' => 1, 'module_height' => 1);
			    $i = 0;
			    foreach ($activity_data as $val) 
			    {
			    // new style
			    $i++;
			    // QRCODE,H : QR-CODE Best error correction
			    // print a line using Cell()
			    // PDF::Cell(0, 12, strtoupper(Session::get('S_USER_ID')).'_activity_data_'.time(), 0, 1, 'C');
			    $html .= '
			    <tr style="border: 1px solid #e4e7ea;">
			      <td width="10%" valign="center" style="border: 1px solid #e4e7ea; text-align: center;">'.$i.'
			      </td>
			      <td width="17%" valign="center" style="border: 1px solid #e4e7ea; text-align: center;">'.$val->no_kk.'
			      </td>
			      <td width="20%" valign="center" style="border: 1px solid #e4e7ea; text-align: left;">'.$val->nama_kep.'
			      </td>
			      <td width="15%" valign="center" style="border: 1px solid #e4e7ea; text-align: left;">'.$val->tgl_req.'
			      </td>
			      <td width="15%" valign="center" style="border: 1px solid #e4e7ea; text-align: left;">'.$val->tgl_cetak.'
			      </td>
			      <td width="18%" valign="center" style="border: 1px solid #e4e7ea; text-align: center;">
			      </td>
			    </tr>
			    ';
			    
			    }
			    $html .= '
			  </tbody>
			</table>
			<table class="first" cellpadding="0" cellspacing="5" border="0">
			  <tr>
			    <td width="45%">
			    </td>	
			    <td width="10%">
			    </td>											
			    <td width="45%" align="center">
			    </td>
			  </tr>
			  <tr>
			    <td height="80" valign="top" align="center">
			    </td>
			    <td height="80" valign="top" align="center">
			    </td>
			    <td height="80" valign="top" align="center">Petugas KK :
			    </td>
			  </tr>        
			  <tr>
			    <td align="center">
			    </td>
			    <td>
			    </td>
			    <td align="center">
			      <u>'.Session::get('S_NAMA_LGKP').'
			      </u>
			    </td>
			  </tr>
			  <tr style="line-height:7px">
			    <td align="center">
			    </td>
			    <td>
			    </td>
			    <td align="center">NIK. '.Session::get('S_NIK').'
			    </td>
			  </tr>
			</table>
			';
			PDF::writeHTML($html, true, false, true, false, '');
			//Close and output PDF document
			PDF::Output(strtoupper(Session::get('S_USER_ID')).'_activity_data_'.time().'.pdf', 'I');
			} 
			else 
			{
			return redirect()->route('logout');
			}
		

		/*PDF::SetTitle('Hello World');

		PDF::AddPage();

		PDF::Write(0, 'Hello World');

		PDF::Output('hello_world.pdf');*/
	}
	

	public function myactivity_data_first(Request $request){
		header('Content-type: application/json');
		$user_id = $request->user_id;
		$is_online = $this->is_online($user_id);
		$clock_in = $this->get_clockin($user_id);
		$clock_out = $this->get_clockout($user_id);
		$response["clock_in"] = $clock_in;
		$response["clock_out"] = $clock_out;
		$response["user_siak"] = $is_online[0]->user_siak;
		$response["user_id"] = $is_online[0]->user_id;
		$response["monev_last"] = $is_online[0]->monev_last;
		$response["monev_ip"] = $is_online[0]->monev_ip;
		$response["siak_last"] = $is_online[0]->siak_last;
		$response["siak_ip"] = $is_online[0]->siak_ip;
        return $response;
	}
	public function myactivity_data_second(Request $request){
		header('Content-type: application/json');
		$user_id = $request->user_id;
			$js = $this->get_count_activity($user_id);
			$jbc = $this->get_count_bcard($user_id);
			$jbe = $this->get_count_benroll($user_id);
			$response["jml_siak"] = $js;
			$response["jml_bcard"] = $jbc;
			$response["jml_benroll"] = $jbe;
        return $response;
	}
	public function myactivity_data_third(Request $request){
		header('Content-type: application/json');
		$user_id = $request->user_id;
		$output = $this->get_all($user_id);
        return $output;	
	}
	public function myactivity_data_four(Request $request){
		header('Content-type: application/json');
		$user_id = $request->user_id;
		$output = $this->get_recap_activity($user_id);
        return $output;	
	}
	public function myactivity_siak_data(Request $request){
		header('Content-type: application/json');
		$user_id = $request->user_id;
		$output = $this->get_all_activity($user_id);
        return $output;	
	}
	public function myactivity_bcard_data(Request $request){
		header('Content-type: application/json');
		$user_id = $request->user_id;
		$output = $this->get_all_bcard($user_id);
        return $output;	
	}
	public function myactivity_benroll_data(Request $request){
		header('Content-type: application/json');
		$user_id = $request->user_id;
		$output = $this->get_all_benroll($user_id);
        return $output;	
	}
	public function myactivity_get_rekap_data(Request $request)
     {
          $draw = intval($request->draw);
          $start = intval($request->start);
          $length = intval($request->length);
          $user_id = $request->user_id;
          $tgl = $request->tanggal;
		  $tgl_start = substr($tgl, 0, 10);
		  $tgl_end = substr($tgl,13, 10);
          $rekap_siak = $this->get_recap_activity_data($user_id,$tgl_start,$tgl_end);
          $data = array();
          $i = 0;
          foreach($rekap_siak as $r) {
          		$i ++;
               $data[] = array(
               		$i,
                    $r->user_id,
                    $r->nama_lgkp,
                    $tgl,
                    $r->p1,
                    $r->jml
                   
               );
          }

          $output = array(
               "draw" => $draw,
                 "recordsTotal" => count($rekap_siak),
                 "recordsFiltered" => count($rekap_siak),
                 "data" => $data
            );
          return $output;
          exit();
		}
	public function myactivity_get_rekap_detail_kk(Request $request)
     {
     	  $draw = intval($request->draw);
          $start = intval($request->start);
          $length = intval($request->length);
          $user_id = $request->user_id;
          $no_kec = $request->no_kec;
          $no_kel = $request->no_kel;
          $tgl = $request->tanggal;
		  $tgl_start = substr($tgl, 0, 10);
		  $tgl_end = substr($tgl,13, 10);
          $rekap_siak = $this->rekap_detail_kk($user_id,$tgl_start,$tgl_end,$no_kec,$no_kel);
          $data = array();
          $i = 0;
          foreach($rekap_siak as $r) {
          		$i ++;
               $data[] = array(
               		$i,
                    $r->no_kk,
                    $r->nama_kep,
                    $r->tgl_req,
                    $r->tgl_cetak,
                    ''
                   
               );
          }

          $output = array(
               "draw" => $draw,
                 "recordsTotal" => count($rekap_siak),
                 "recordsFiltered" => count($rekap_siak),
                 "data" => $data
            );
          return $output;
          exit();
		}

	public function myactivity_get_rekap_kk_data(Request $request)
     {
          $draw = intval($request->draw);
          $start = intval($request->start);
          $length = intval($request->length);
          $user_id = $request->user_id;
          $no_kec = $request->no_kec;
          $tgl = $request->tanggal;
		  $tgl_start = substr($tgl, 0, 10);
		  $tgl_end = substr($tgl,13, 10);
          $rekap_siak = $this->rekap_kk($user_id,$tgl_start,$tgl_end,$no_kec);
          $data = array();
          $i = 0;
          foreach($rekap_siak as $r) {
          		$i ++;
               $data[] = array(
               		$i,
                    $r->nama_wil,
                    $tgl,
                    $r->jml
                   
               );
          }

          $output = array(
               "draw" => $draw,
                 "recordsTotal" => count($rekap_siak),
                 "recordsFiltered" => count($rekap_siak),
                 "data" => $data
            );
          return $output;
          exit();
		}

	public function myactivity_get_rekap_kedatangan(Request $request)
     {
          $draw = intval($request->draw);
          $start = intval($request->start);
          $length = intval($request->length);
          $no_kec = $request->no_kec;
          $no_kel = $request->no_kel;
          $tgl = $request->tanggal;
		  $tgl_start = substr($tgl, 0, 10);
		  $tgl_end = substr($tgl,13, 10);
          $rekap_siak = $this->rekap_kedatangan($tgl_start,$tgl_end,$no_kec,$no_kel);
          $data = array();
          $i = 0;
          foreach($rekap_siak as $r) {
          		$i ++;
               $data[] = array(
               		$i,
                    $r->kec,
                    $r->kel,
                    $r->rw,
                    $r->rt,
                    $r->alamat,
                    $r->no_datang,
                    $r->no_pindah,
                    $r->prop_asal,
                    $r->kab_asal,
                    $r->kec_asal,
                    $r->kel_asal,
                    '\''.$r->no_kk,
                    '\''.$r->nik_pemohon,
                    $r->nama_pemohon,
                    $r->tgl,
                    $r->jam,
                   
               );
          }

          $output = array(
               "draw" => $draw,
                 "recordsTotal" => count($rekap_siak),
                 "recordsFiltered" => count($rekap_siak),
                 "data" => $data
            );
          return $output;
          exit();
		}

	public function myactivity_get_rekap_perpindahan(Request $request)
     {
          $draw = intval($request->draw);
          $start = intval($request->start);
          $length = intval($request->length);
          $no_kec = $request->no_kec;
          $no_kel = $request->no_kel;
          $tgl = $request->tanggal;
		  $tgl_start = substr($tgl, 0, 10);
		  $tgl_end = substr($tgl,13, 10);
          $rekap_siak = $this->rekap_perpindahan($tgl_start,$tgl_end,$no_kec,$no_kel);
          $data = array();
          $i = 0;
          foreach($rekap_siak as $r) {
          		$i ++;
               $data[] = array(
               		$i,
                    $r->kec,
                    $r->kel,
                    '\''.$r->no_kk,
                    '\''.$r->nik_pemohon,
                    $r->nama_pemohon,
                    $r->rw,
                    $r->rt,
                    $r->alamat,
                    $r->no_pindah,
                    $r->prop_asal,
                    $r->kab_asal,
                    $r->kec_asal,
                    $r->kel_asal,
                    $r->tgl,
                    $r->jam,
                   
               );
          }

          $output = array(
               "draw" => $draw,
                 "recordsTotal" => count($rekap_siak),
                 "recordsFiltered" => count($rekap_siak),
                 "data" => $data
            );
          return $output;
          exit();
		}

	public function mylaporan_get_rekap_kedatangan(Request $request)
     {
          $draw = intval($request->draw);
          $start = intval($request->start);
          $length = intval($request->length);
          $no_kec = $request->no_kec;
          $no_kel = $request->no_kel;
          $tgl = $request->tanggal;
		  $tgl_start = substr($tgl, 0, 10);
		  $tgl_end = substr($tgl,13, 10);
          $rekap_siak = $this->laporan_rekap_kedatangan($tgl_start,$tgl_end,$no_kec,$no_kel);
          $data = array();
          $i = 0;
          foreach($rekap_siak as $r) {
          		$i ++;
               $data[] = array(
               		$i,
                    $r->nama_pemohon,
                    $r->alamat,
                    $r->rt,
                    $r->rw,
                    $r->kec,
                    $r->kel,
                    $r->daerah_asal,
                   
               );
          }

          $output = array(
               "draw" => $draw,
                 "recordsTotal" => count($rekap_siak),
                 "recordsFiltered" => count($rekap_siak),
                 "data" => $data
            );
          return $output;
          exit();
		}

	public function mylaporan_get_rekap_perpindahan(Request $request)
     {
          $draw = intval($request->draw);
          $start = intval($request->start);
          $length = intval($request->length);
          $no_kec = $request->no_kec;
          $no_kel = $request->no_kel;
          $tgl = $request->tanggal;
		  $tgl_start = substr($tgl, 0, 10);
		  $tgl_end = substr($tgl,13, 10);
          $rekap_siak = $this->laporan_rekap_perpindahan($tgl_start,$tgl_end,$no_kec,$no_kel);
          $data = array();
          $i = 0;
          foreach($rekap_siak as $r) {
          		$i ++;
               $data[] = array(
               		$i,
               		$r->nama_pemohon,
                    $r->alamat,
                    $r->rt,
                    $r->rw,
                    $r->kec,
                    $r->kel,
                    $r->daerah_tujuan,
                   
               );
          }

          $output = array(
               "draw" => $draw,
                 "recordsTotal" => count($rekap_siak),
                 "recordsFiltered" => count($rekap_siak),
                 "data" => $data
            );
          return $output;
          exit();
		}

	public function get_master_salaman(){
			$sql = "SELECT ID, LAYANAN, BARU_TDY, UPLOAD_ULANG_TDY,BTL_TDY, BARU, UPLOAD_ULANG, BTL, TERVERIFIKASI, TERPUBLISH, JML FROM VW_SIAK_SALAMAN ORDER BY ID";
			$r = DB::connection('webdb')->select($sql);
            return $r;
	}

	public function get_master_edatang(){
			$sql = "SELECT USER_ID, NAMA_LGKP, NAMA_KANTOR, NIK, BARU_TDY, UPLOAD_ULANG_TDY,BTL_TDY, BARU, UPLOAD_ULANG, BTL, TERVERIFIKASI, TERPUBLISH, JML FROM VW_SIAK_EPUNTEN_DATANG";
			$r = DB::select($sql);
            return $r;
	}

	public function get_master_tte(){
			$sql = "SELECT USER_ID, NAMA_LGKP, NAMA_KANTOR, NIK, REQUEST_N, APROVED_N, THE_REST_N, REQUEST_A, APROVED_A, THE_REST_A,APROVED_ORANGE,APROVED_BLUE,APROVED_GREEN,APROVED_RED FROM VW_SIAK_TTE";
			$r = DB::select($sql);
            return $r;
	}

	public function get_master_tte_detail(){
			$sql = 'SELECT LAYANAN, "REQ_TTE" REQUEST_N, "APROVED TTE" APROVED_N, "PENDING TTE" THE_REST_N, "BELUM VERIVIKASI" aproved_red, "PEROSES PENERBITAN" aproved_orange, "BELUM DIPUBLISH (ROKET)" aproved_blue, "SUDAH DIPUBLISH" aproved_green FROM VW_MASTER_TTE';
			$r = DB::select($sql);
            return $r;
	}

	public function get_master_tte_kelahiran(){
			$sql = "SELECT  NAMA_LGKP,  REQUEST_N, APROVED_N, THE_REST_N,APROVED_ORANGE,APROVED_BLUE,APROVED_GREEN,APROVED_RED FROM VW_SIAK_TTE_AKTA";
			$r = DB::select($sql);
            return $r;
	}

	public function get_master_tte_kematian(){
			$sql = "SELECT  NAMA_LGKP,  REQUEST_N, APROVED_N, THE_REST_N,APROVED_ORANGE,APROVED_BLUE,APROVED_GREEN,APROVED_RED FROM VW_SIAK_TTE_KEMATIAN";
			$r = DB::select($sql);
            return $r;
	}

	public function get_master_tte_perpindahan(){
			$sql = "SELECT  NAMA_LGKP,  REQUEST_N, APROVED_N, THE_REST_N,APROVED_ORANGE,APROVED_BLUE,APROVED_GREEN,APROVED_RED FROM VW_SIAK_TTE_PERPINDAHAN";
			$r = DB::select($sql);
            return $r;
	}

	public function get_master_tte_perkawinan(){
			$sql = "SELECT  NAMA_LGKP,  REQUEST_N, APROVED_N, THE_REST_N,APROVED_ORANGE,APROVED_BLUE,APROVED_GREEN,APROVED_RED FROM VW_SIAK_TTE_PERKAWINAN";
			$r = DB::select($sql);
            return $r;
	}

	public function get_master_tte_perceraian(){
			$sql = "SELECT  NAMA_LGKP,  REQUEST_N, APROVED_N, THE_REST_N,APROVED_ORANGE,APROVED_BLUE,APROVED_GREEN,APROVED_RED FROM VW_SIAK_TTE_PERCERAIAN";
			$r = DB::select($sql);
            return $r;
	}

	public function get_master_tte_pengesahananak(){
			$sql = "SELECT  NAMA_LGKP,  REQUEST_N, APROVED_N, THE_REST_N,APROVED_ORANGE,APROVED_BLUE,APROVED_GREEN,APROVED_RED FROM VW_SIAK_TTE_PENGESAHAN_ANAK";
			$r = DB::select($sql);
            return $r;
	}

	public function get_master_tte_pengakuananak(){
			$sql = "SELECT  NAMA_LGKP,  REQUEST_N, APROVED_N, THE_REST_N,APROVED_ORANGE,APROVED_BLUE,APROVED_GREEN,APROVED_RED FROM VW_SIAK_TTE_PENGAKUAN_ANAK";
			$r = DB::select($sql);
            return $r;
	}
	public function get_master_activity(){
			$nowdate = date("d/m/Y");
			$sql = "SELECT A.USER_ID,A.NAMA_LGKP, A.NAMA_KANTOR, A.NIK
                    , CASE WHEN B.PEREKAMAN IS NULL THEN 0 ELSE B.PEREKAMAN END AS PEREKAMAN  
                    , CASE WHEN C.PENCETAKAN IS NULL THEN 0 ELSE C.PENCETAKAN END AS PENCETAKAN  
                    , CASE WHEN D.SIAK IS NULL THEN 0 ELSE D.SIAK END AS SIAK
                    FROM SIAK_USER_PLUS@YZDB A 
                    LEFT JOIN (SELECT e.user_id, COUNT(1) AS PEREKAMAN FROM DEMOGRAPHICS@DB221 A INNER JOIN  SIAK_USER_PLUS@YZDB E ON A.CREATED_USERNAME = E.USER_BENROL WHERE E.USER_LEVEL =1 AND TO_CHAR(CREATED,'DD/MM/YYYY') = '$nowdate' group by e.user_id) B 
                    ON A.USER_ID = B.USER_ID
                    LEFT JOIN (SELECT E.USER_ID, COUNT(1) AS PENCETAKAN FROM CARD_MANAGEMENT@DB5CETAK A INNER JOIN  SIAK_USER_PLUS@YZDB E ON CASE WHEN UPPER(A.LAST_UPDATED_USERNAME) IS NULL THEN UPPER(A.CREATED_USERNAME) ELSE UPPER(A.LAST_UPDATED_USERNAME) END = UPPER(E.USER_BCARD)    WHERE E.USER_LEVEL =1  AND CASE WHEN TO_CHAR(A.LAST_UPDATE,'DD/MM/YYYY') IS NULL THEN TO_CHAR(A.PERSONALIZED_DATE,'DD/MM/YYYY') ELSE TO_CHAR(A.LAST_UPDATE,'DD/MM/YYYY') END =  '$nowdate' GROUP BY E.USER_ID) C
                    ON A.USER_ID = C.USER_ID
                    LEFT JOIN (SELECT E.USER_ID, COUNT(1) AS SIAK FROM T5_HIST_ACTIVITY A INNER JOIN T5_HIST_ACTIVITY_REF B ON A.ACTIVITY_TYPE = B.ACTIVITY_ID INNER JOIN T5_HIST_ACTIVITY_REF C ON A.ACTIVITY_MOD = C.ACTIVITY_ID INNER JOIN SIAK_USER_PLUS@YZDB E ON A.USER_ID = E.USER_SIAK WHERE E.USER_LEVEL =1 AND ACTIVITY_DATE >= TO_DATE('$nowdate','DD/MM/yyyy') AND ACTIVITY_DATE < TO_DATE('$nowdate','DD/MM/yyyy')+1 GROUP BY E.USER_ID) D
                    ON A.USER_ID = D.USER_ID
                    WHERE A.USER_LEVEL = 1 AND A.IS_SHOW = 1 ORDER BY TO_NUMBER(A.NO_KEC)";
			$r = DB::connection('db222')->select($sql);
            return $r;
		}
		public function get_master_activity_dinas(){
			$nowdate = date("d/m/Y");
			$sql = "SELECT A.USER_ID,A.NAMA_LGKP, A.NAMA_KANTOR, A.NIK
					, CASE WHEN B.PEREKAMAN IS NULL THEN 0 ELSE B.PEREKAMAN END AS PEREKAMAN  
					, CASE WHEN C.PENCETAKAN IS NULL THEN 0 ELSE C.PENCETAKAN END AS PENCETAKAN  
					, CASE WHEN D.SIAK IS NULL THEN 0 ELSE D.SIAK END AS SIAK
					FROM SIAK_USER_PLUS@YZDB A 
					LEFT JOIN (SELECT e.user_id, COUNT(1) AS PEREKAMAN FROM DEMOGRAPHICS@DB221 A INNER JOIN  SIAK_USER_PLUS@YZDB E ON A.CREATED_USERNAME = E.USER_BENROL WHERE E.USER_LEVEL = 3 AND E.IS_MONITORING = 1 AND A.CREATED >= TO_DATE('$nowdate','DD/MM/yyyy') AND A.CREATED < TO_DATE('$nowdate','DD/MM/yyyy')+1 group by e.user_id) B 
					ON A.USER_ID = B.USER_ID
					LEFT JOIN (SELECT E.USER_ID, COUNT(1) AS PENCETAKAN FROM CARD_MANAGEMENT@DB5CETAK A INNER JOIN  SIAK_USER_PLUS@YZDB E ON CASE WHEN UPPER(A.LAST_UPDATED_USERNAME) IS NULL THEN UPPER(A.CREATED_USERNAME) ELSE UPPER(A.LAST_UPDATED_USERNAME) END = UPPER(E.USER_BCARD)    WHERE E.USER_LEVEL = 3 AND E.IS_MONITORING = 1  AND CASE WHEN TO_CHAR(A.LAST_UPDATE,'DD/MM/YYYY') IS NULL THEN TO_CHAR(A.PERSONALIZED_DATE,'DD/MM/YYYY') ELSE TO_CHAR(A.LAST_UPDATE,'DD/MM/YYYY') END =  TO_CHAR(SYSDATE,'DD/MM/YYYY') GROUP BY E.USER_ID) C
					ON A.USER_ID = C.USER_ID
					LEFT JOIN (SELECT E.USER_ID, COUNT(1) AS SIAK FROM T5_HIST_ACTIVITY A INNER JOIN T5_HIST_ACTIVITY_REF B ON A.ACTIVITY_TYPE = B.ACTIVITY_ID INNER JOIN T5_HIST_ACTIVITY_REF C ON A.ACTIVITY_MOD = C.ACTIVITY_ID INNER JOIN SIAK_USER_PLUS@YZDB E ON A.USER_ID = E.USER_SIAK WHERE E.USER_LEVEL = 3 AND E.IS_MONITORING = 1 AND A.ACTIVITY_DATE >= TO_DATE('$nowdate','DD/MM/yyyy') AND A.ACTIVITY_DATE < TO_DATE('$nowdate','DD/MM/yyyy')+1 GROUP BY E.USER_ID) D
					ON A.USER_ID = D.USER_ID
					WHERE A.USER_LEVEL = 3 AND A.IS_MONITORING = 1 ORDER BY TO_NUMBER(A.NO_KEC)";
			$r = DB::connection('db222')->select($sql);
            return $r;
		}
		public function get_master_list(){
			$sql = "SELECT USER_ID, NAMA_LGKP, NAMA_KANTOR, NIK, PENGAJUAN_S,PINDAHAN_S, PRR_S FROM VW_SIAK_LIST";
			$r = DB::select($sql);
            return $r;
		}
		public function get_master_list_suket(){
			$sql = "SELECT USER_ID, NAMA_LGKP, NAMA_KANTOR, NIK, SUKET_S FROM VW_SIAK_LIST_SUKET";
			$r = DB::select($sql);
            return $r;
		}
		public function get_master_pengajuan(){
			$sql = "SELECT USER_ID ,NAMA_LGKP ,NAMA_KANTOR ,NIK ,PENGAJUAN_B ,PENGAJUAN_N ,PENGAJUAN_Y FROM VW_SIAK_MASTER_PENGAJUAN";
			$r = DB::select($sql);
            return $r;
		}
		public function get_master_absensi(){
			$sql = "SELECT USER_ID ,NAMA_LGKP ,NAMA_KANTOR ,NIK ,JAM_MASUK ,JAM_KELUAR FROM VW_SIAK_MASTER_ABSENSI";
			$r = DB::select($sql);
            return $r;
		}
		public function get_contact_kec(){
			$sql = "SELECT NIK,NAMA_LGKP,NAMA_KANTOR,TO_CHAR(TGL_LHR,'DD-MM-YYYY') AS TGL_LHR, TMPT_LHR,TELP, ALAMAT_RUMAH FROM SIAK_USER_PLUS  ORDER BY USER_LEVEL, TO_NUMBER(NO_KEC), NAMA_LGKP";
			$r = DB::select($sql);
            return $r;
		}

		public function is_online($user_id){
			$sql = "SELECT A.USER_ID,CASE WHEN A.USER_SIAK IS NULL THEN '-' ELSE A.USER_SIAK END AS USER_SIAK,CASE WHEN TO_CHAR(B.LAST_ACTIVITY,'DD-MM-YYYY HH24:MI:SS') IS NULL THEN '-' ELSE TO_CHAR(B.LAST_ACTIVITY,'DD-MM-YYYY HH24:MI:SS') END AS MONEV_LAST, CASE WHEN B.IP_ADDRESS IS NULL THEN '-' ELSE B.IP_ADDRESS END AS MONEV_IP,CASE WHEN C.LAST_ACTIVITY IS NULL THEN '-' ELSE TO_CHAR(TO_DATE('1970-01-01','YYYY-MM-DD') + NUMTODSINTERVAL(TO_CHAR(C.LAST_ACTIVITY),'SECOND') + 7/24,'DD-MM-YYYY HH24:MI:SS') END AS SIAK_LAST,CASE WHEN C.IP_ADDRESS IS NULL THEN '-' ELSE C.IP_ADDRESS END AS  SIAK_IP FROM SIAK_USER_PLUS A LEFT JOIN SIAK_SESSION_PLUS B ON A.USER_ID = B.USER_ID LEFT JOIN T5_SESSION@DB222 C ON A.USER_SIAK = C.USER_ID WHERE A.USER_ID = '$user_id'  ORDER BY C.LAST_ACTIVITY DESC NULLS LAST";
			$r = DB::select($sql);
            return $r;
		}
		public function get_clockin($user_id){
			$nowdate = date("d/m/Y");
			$sql = "SELECT A.USER_ID, CASE WHEN JAM_MASUK IS NULL THEN '-' ELSE JAM_MASUK END  AS JML FROM SIAK_USER_PLUS A LEFT JOIN  SIAK_ABSENSI B ON A.USER_ID = B.USER_ID AND TO_CHAR(B.TANGGAL,'DD/MM/YYYY') = '$nowdate' WHERE A.USER_ID = '$user_id'";
			$r = DB::select($sql);
            return $r[0]->jml;
		}
		public function get_clockout($user_id){
			$nowdate = date("d/m/Y");
			$sql = "SELECT A.USER_ID, CASE WHEN JAM_KELUAR IS NULL THEN '-' ELSE JAM_KELUAR END  AS JML FROM SIAK_USER_PLUS A LEFT JOIN  SIAK_ABSENSI B ON A.USER_ID = B.USER_ID AND TO_CHAR(B.TANGGAL,'DD/MM/YYYY') = '$nowdate' WHERE A.USER_ID = '$user_id'";
			$r = DB::select($sql);
            return $r[0]->jml;
		}

		public function get_count_activity($user_id){
			$sql = "SELECT E.USER_ID,COUNT(1) AS JML FROM T5_HIST_ACTIVITY@DB222 A INNER JOIN T5_HIST_ACTIVITY_REF@DB222 B ON A.ACTIVITY_TYPE = B.ACTIVITY_ID INNER JOIN T5_HIST_ACTIVITY_REF@DB222 C ON A.ACTIVITY_MOD = C.ACTIVITY_ID INNER JOIN SIAK_USER_PLUS E ON A.USER_ID = E.USER_SIAK WHERE E.USER_ID = '$user_id' AND TRUNC(ACTIVITY_DATE) = TRUNC(SYSDATE) GROUP BY E.USER_ID ORDER BY E.USER_ID";
			$r = DB::select($sql);
			if (!empty($r)){
				return $r[0]->jml;
			}else{
				return 0;
			}
		}
		public function get_count_bcard($user_id){
			$sql = "SELECT E.USER_ID, COUNT(1) AS JML FROM CARD_MANAGEMENT@DB5CETAK A INNER JOIN  SIAK_USER_PLUS E ON CASE WHEN UPPER(A.LAST_UPDATED_USERNAME) IS NULL THEN UPPER(A.CREATED_USERNAME) ELSE UPPER(A.LAST_UPDATED_USERNAME) END = UPPER(E.USER_BCARD)  WHERE E.USER_ID = '$user_id'  AND CASE WHEN TO_CHAR(LAST_UPDATE,'DD/MM/YYYY') IS NULL THEN TO_CHAR(PERSONALIZED_DATE,'DD/MM/YYYY') ELSE TO_CHAR(LAST_UPDATE,'DD/MM/YYYY') END =  TO_CHAR(SYSDATE,'DD/MM/YYYY') GROUP BY E.USER_ID ORDER BY E.USER_ID DESC";
			$r = DB::select($sql);
			if (!empty($r)){
				return $r[0]->jml;
			}else{
				return 0;
			}
			
		}
		public function get_count_benroll($user_id){
			$sql = "SELECT E.USER_ID,COUNT(1) AS JML FROM DEMOGRAPHICS@DB221 A INNER JOIN  SIAK_USER_PLUS E ON A.CREATED_USERNAME = E.USER_BENROL WHERE E.USER_ID = '$user_id' AND TO_CHAR(CREATED,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') GROUP BY E.USER_ID ORDER BY E.USER_ID DESC";
			$r = DB::select($sql);
			if (!empty($r)){
				return $r[0]->jml;
			}else{
				return 0;
			}
		}
		public function get_all($user_id){
			$sql = "SELECT A.USER_ID, CASE WHEN B.JML IS NULL THEN 0 ELSE B.JML END ALL_SIAK, CASE WHEN C.JML IS NULL THEN 0 ELSE C.JML END ALL_REKAM,  CASE WHEN D.JML IS NULL THEN 0 ELSE D.JML END ALL_CETAK FROM SIAK_USER_PLUS A 
          LEFT JOIN
          (SELECT E.USER_ID, COUNT(1) AS JML FROM T5_HIST_ACTIVITY@DB222 A INNER JOIN T5_HIST_ACTIVITY_REF@DB222 B ON A.ACTIVITY_TYPE = B.ACTIVITY_ID INNER JOIN T5_HIST_ACTIVITY_REF@DB222 C ON A.ACTIVITY_MOD = C.ACTIVITY_ID INNER JOIN SIAK_USER_PLUS E ON A.USER_ID = E.USER_SIAK WHERE TO_CHAR(ACTIVITY_DATE,'MM/YYYY') = TO_CHAR(SYSDATE,'MM/YYYY') GROUP BY E.USER_ID) B ON A.USER_ID = B.USER_ID
          LEFT JOIN
          (SELECT E.USER_ID, COUNT(1) AS JML FROM DEMOGRAPHICS@DB221 A INNER JOIN  SIAK_USER_PLUS E ON A.CREATED_USERNAME = E.USER_BENROL WHERE TO_CHAR(CREATED,'MM/YYYY') = TO_CHAR(SYSDATE,'MM/YYYY') GROUP BY E.USER_ID) C ON A.USER_ID = C.USER_ID
          LEFT JOIN 
          (SELECT E.USER_ID, COUNT(1) AS JML FROM CARD_MANAGEMENT@DB5CETAK A INNER JOIN  SIAK_USER_PLUS E ON CASE WHEN UPPER(A.LAST_UPDATED_USERNAME) IS NULL THEN UPPER(A.CREATED_USERNAME) ELSE UPPER(A.LAST_UPDATED_USERNAME) END = UPPER(E.USER_BCARD)  WHERE CASE WHEN TO_CHAR(LAST_UPDATE,'MM/YYYY') IS NULL THEN TO_CHAR(PERSONALIZED_DATE,'MM/YYYY') ELSE TO_CHAR(LAST_UPDATE,'MM/YYYY') END =  TO_CHAR(SYSDATE,'MM/YYYY') GROUP BY E.USER_ID) D ON A.USER_ID = D.USER_ID WHERE A.USER_ID = '$user_id'  ORDER BY A.USER_ID";
			$r = DB::select($sql);
            return $r;
		}
		public function get_recap_activity($user_id){
			$sql = "SELECT COUNT(1) AS JML ,A.USER_ID, CONCAT(B.ACTIVITY_NAME,CONCAT(' ',C.ACTIVITY_NAME)) AS P1 FROM T5_HIST_ACTIVITY@DB222 A INNER JOIN T5_HIST_ACTIVITY_REF@DB222 B ON A.ACTIVITY_TYPE = B.ACTIVITY_ID INNER JOIN T5_HIST_ACTIVITY_REF@DB222 C ON A.ACTIVITY_MOD = C.ACTIVITY_ID INNER JOIN SIAK_USER_PLUS X ON A.USER_ID = X.USER_SIAK WHERE 
				TRUNC(ACTIVITY_DATE) = TRUNC(SYSDATE) AND X.USER_ID = '$user_id'
				GROUP BY CONCAT(B.ACTIVITY_NAME,CONCAT(' ',C.ACTIVITY_NAME)),A.USER_ID ORDER BY A.USER_ID,CONCAT(B.ACTIVITY_NAME,CONCAT(' ',C.ACTIVITY_NAME))";
			$r = DB::select($sql);
            return $r;
		}
		public function get_all_activity($user_id){
			$sql = "SELECT A.USER_ID, E.NIK, E.NAMA_LGKP, TO_CHAR(A.ACTIVITY_DATE, 'HH24:MI')AS ACTIVITY_DATE, B.ACTIVITY_NAME AS P1,C.ACTIVITY_NAME AS P2, A.ACTIVITY_DESC AS P3 FROM T5_HIST_ACTIVITY@DB222 A INNER JOIN T5_HIST_ACTIVITY_REF@DB222 B ON A.ACTIVITY_TYPE = B.ACTIVITY_ID INNER JOIN T5_HIST_ACTIVITY_REF@DB222 C ON A.ACTIVITY_MOD = C.ACTIVITY_ID INNER JOIN SIAK_USER_PLUS E ON A.USER_ID = E.USER_SIAK WHERE (E.USER_LEVEL =1 OR E.USER_LEVEL =3) AND TRUNC(ACTIVITY_DATE) = TRUNC(SYSDATE) AND E.USER_ID = '$user_id' ORDER BY E.USER_ID,A.ACTIVITY_DATE DESC";
			$r = DB::select($sql);
            return $r;
		}
		public function get_all_bcard($user_id){
			$sql = "SELECT E.USER_ID AS MONITORING_USER, CASE WHEN A.LAST_UPDATED_USERNAME IS NULL THEN A.CREATED_USERNAME ELSE A.LAST_UPDATED_USERNAME END AS USER_ID, E.NIK, E.NAMA_LGKP, TO_CHAR(A.PERSONALIZED_DATE, 'HH24:MI')AS ACTIVITY_DATE, 'PENCETAKAN' AS P1,'E-KTP' AS P2, CONCAT(CONCAT(CONCAT('NIK : ',A.NIK),'<br />'),(SELECT CONCAT(CONCAT(CONCAT('NIK : ',X.NAMA_LGKP),'<br />'),CONCAT('E-KTP STATUS CODE : ',X.CURRENT_STATUS_CODE)) FROM DEMOGRAPHICS@DB2 X WHERE A.NIK = X.NIK)) AS P3 FROM CARD_MANAGEMENT@DB5CETAK A INNER JOIN  SIAK_USER_PLUS E ON CASE WHEN UPPER(A.LAST_UPDATED_USERNAME) IS NULL THEN UPPER(A.CREATED_USERNAME) ELSE UPPER(A.LAST_UPDATED_USERNAME) END = UPPER(E.USER_BCARD)    WHERE E.USER_ID = '$user_id'  AND CASE WHEN TO_CHAR(LAST_UPDATE,'DD/MM/YYYY') IS NULL THEN TO_CHAR(PERSONALIZED_DATE,'DD/MM/YYYY') ELSE TO_CHAR(LAST_UPDATE,'DD/MM/YYYY') END =  TO_CHAR(SYSDATE,'DD/MM/YYYY')  ORDER BY A.PERSONALIZED_DATE DESC";
			$r = DB::select($sql);
            return $r;
		}
		public function get_all_benroll($user_id){
			$sql = "SELECT E.USER_ID AS MONITORING_USER,A.CREATED_USERNAME AS USER_ID, E.NIK, E.NAMA_LGKP, TO_CHAR(A.CREATED, 'HH24:MI')AS ACTIVITY_DATE, 'PEREKAMAN' AS P1,'E-KTP' AS P2, CONCAT(CONCAT(CONCAT('NIK : ',A.NIK),'<br />'),CONCAT(CONCAT(CONCAT('NAMA : ',A.NAMA_LGKP),'<br />'),CONCAT('E-KTP STATUS CODE : ',A.CURRENT_STATUS_CODE))) AS P3 FROM DEMOGRAPHICS@DB221 A INNER JOIN  SIAK_USER_PLUS E ON A.CREATED_USERNAME = E.USER_BENROL WHERE E.USER_ID = '$user_id'  AND TO_CHAR(CREATED,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') ORDER BY A.CREATED DESC";
			$r = DB::select($sql);
            return $r;
		}
		public function get_recap_activity_data($user_id,$tgl_start,$tgl_end){
			$sql = "SELECT COUNT(1) AS JML ,X.USER_ID,X.NAMA_LGKP, CONCAT(B.ACTIVITY_NAME,CONCAT(' ',C.ACTIVITY_NAME)) AS P1 FROM T5_HIST_ACTIVITY@DB222 A INNER JOIN T5_HIST_ACTIVITY_REF@DB222 B ON A.ACTIVITY_TYPE = B.ACTIVITY_ID INNER JOIN T5_HIST_ACTIVITY_REF@DB222 C ON A.ACTIVITY_MOD = C.ACTIVITY_ID INNER JOIN SIAK_USER_PLUS X ON A.USER_ID = X.USER_SIAK WHERE 
				ACTIVITY_DATE >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND ACTIVITY_DATE < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1
				AND X.USER_ID = '$user_id'
				GROUP BY CONCAT(B.ACTIVITY_NAME,CONCAT(' ',C.ACTIVITY_NAME)),X.USER_ID,X.NAMA_LGKP ORDER BY X.USER_ID,CONCAT(B.ACTIVITY_NAME,CONCAT(' ',C.ACTIVITY_NAME))";
			$r = DB::select($sql);
            return $r;
		}
		public function rekap_detail_kk($user_id,$tgl_start,$tgl_end, $no_kec = 0, $no_kel = 0){
			$sql = "";
			$sql .= "SELECT A.NO_KK, A.NAMA_KEP, B.USER_ID, TO_CHAR(A.PRINTED_DATE,'DD-MM-YYYY') TGL_CETAK,CASE WHEN C.REQ_DATE IS NULL THEN '-' ELSE TO_CHAR(C.REQ_DATE,'DD-MM-YYYY') END TGL_REQ, A.NO_KEC, A.NO_KEL FROM T5_SEQN_PRINT_KK A INNER JOIN SIAK_USER_PLUS B ON A.PRINTED_BY = B.USER_SIAK LEFT JOIN 
				 (
						    SELECT 
						    NO_DOC
						    , CERT_STATUS
						    , REQ_DATE
						    , REQ_BY
						    , SEQN_ID
						    , URL_DOKUMEN
						    , PEJABAT_PROCCESS_BY
						    , PEJABAT_PROCESS_DATE  
						  FROM 
						  (
						    SELECT 
						    NO_DOC
						    , CERT_STATUS
						    , REQ_DATE
						    , REQ_BY
						    , SEQN_ID
						    , URL_DOKUMEN
						    , PEJABAT_PROCCESS_BY
						    , PEJABAT_PROCESS_DATE 
						    , RANK() OVER (PARTITION BY NO_DOC ORDER BY REQ_DATE DESC,SEQN_ID DESC) RNK FROM BSRE_KARTU_KELUARGA) 
						    WHERE RNK = 1) C ON A.NO_KK = C.NO_DOC 
				WHERE B.USER_ID = '$user_id' AND A.PRINTED_DATE >= TO_DATE('$tgl_start','DD-MM-YYYY') AND A.PRINTED_DATE < TO_DATE('$tgl_end','DD-MM-YYYY')+1";
			if($no_kec != 0 ){ 
				$sql .= " AND A.NO_KEC = $no_kec ";
			} 
			if($no_kel != 0 ){ 
				$sql .= " AND A.NO_KEL = $no_kel ";
			} 
			$sql .= "ORDER BY A.PRINTED_DATE DESC";
			$r = DB::select($sql);
            return $r;
		}
		public function rekap_detail_kk_pdf($user_id,$tgl_start,$tgl_end, $no_kec = 0, $no_kel = 0){
			$sql = "";
			$sql .= "SELECT A.NO_KK, A.NAMA_KEP, B.USER_ID, TO_CHAR(A.PRINTED_DATE,'DD-MM-YYYY') TGL_CETAK,CASE WHEN C.REQ_DATE IS NULL THEN '-' ELSE TO_CHAR(C.REQ_DATE,'DD-MM-YYYY') END TGL_REQ, A.NO_KEC, A.NO_KEL FROM T5_SEQN_PRINT_KK A INNER JOIN SIAK_USER_PLUS B ON A.PRINTED_BY = B.USER_SIAK LEFT JOIN 
				 (
						    SELECT 
						    NO_DOC
						    , CERT_STATUS
						    , REQ_DATE
						    , REQ_BY
						    , SEQN_ID
						    , URL_DOKUMEN
						    , PEJABAT_PROCCESS_BY
						    , PEJABAT_PROCESS_DATE  
						  FROM 
						  (
						    SELECT 
						    NO_DOC
						    , CERT_STATUS
						    , REQ_DATE
						    , REQ_BY
						    , SEQN_ID
						    , URL_DOKUMEN
						    , PEJABAT_PROCCESS_BY
						    , PEJABAT_PROCESS_DATE 
						    , RANK() OVER (PARTITION BY NO_DOC ORDER BY REQ_DATE DESC,SEQN_ID DESC) RNK FROM BSRE_KARTU_KELUARGA) 
						    WHERE RNK = 1) C ON A.NO_KK = C.NO_DOC 
				WHERE B.USER_ID = '$user_id' AND A.PRINTED_DATE >= TO_DATE('$tgl_start','DD-MM-YYYY') AND A.PRINTED_DATE < TO_DATE('$tgl_end','DD-MM-YYYY')+1";
			if($no_kec != 0 ){ 
				$sql .= " AND A.NO_KEC = $no_kec ";
			} 
			if($no_kel != 0 ){ 
				$sql .= " AND A.NO_KEL = $no_kel ";
			} 
			$sql .= "ORDER BY A.PRINTED_DATE DESC";
			$r = DB::select($sql);
			return $r;
		}
		public function get_atasan($user_id){
			$sql = "SELECT 
					  A.USER_ID
					  , A.NAMA_LGKP
					  , CASE WHEN C.NAMA IS NULL THEN '' ELSE C.NAMA END NAMA_SATU
					  , CASE WHEN C.NIP IS NULL THEN '' ELSE TO_CHAR(C.NIP) END NIP_SATU
					  , CASE WHEN C.JABATAN IS NULL THEN '' ELSE C.JABATAN END JABATAN_SATU 
					  , CASE WHEN D.NAMA IS NULL THEN '' ELSE D.NAMA END NAMA_DUA
					  , CASE WHEN D.NIP IS NULL THEN '' ELSE TO_CHAR(D.NIP) END NIP_DUA
					  , CASE WHEN D.JABATAN IS NULL THEN '' ELSE D.JABATAN END JABATAN_DUA
					FROM SIAK_USER_PLUS A 
					LEFT JOIN SIAK_ATASAN B ON A.USER_ID = B.USER_ID 
					LEFT JOIN SIAK_PEJABAT C ON B.ATASAN_SATU= C.PEJABAT_ID
					LEFT JOIN SIAK_PEJABAT D ON B.ATASAN_DUA= D.PEJABAT_ID WHERE A.USER_ID = '$user_id'";
			$r = DB::select($sql);
			return $r[0];
		}
		public function get_pivot_kel_str($no_kec = 0){
			if ($no_kec !=0){
				$sql = "SELECT TO_CHAR(WM_CONCAT(CONCAT(CONCAT(CONCAT(CONCAT('''',NAMA_KEL),''''),' AS C'),ROWNUM))) VAL FROM (SELECT NO_PROP, NO_KAB, NO_KEC, NO_KEL, NAMA_KEL FROM SETUP_KEL ORDER BY NO_KEL) WHERE NO_PROP=32 AND NO_KAB = 73 AND NO_KEC =$no_kec GROUP BY NO_KEC";
			}else{
				$sql = "SELECT TO_CHAR(WM_CONCAT(CONCAT(CONCAT(CONCAT(CONCAT('''',NAMA_KEC),''''),' AS C'),ROWNUM))) VAL FROM (SELECT NO_PROP, NO_KAB, NO_KEC, NAMA_KEC FROM SETUP_KEC ORDER BY NO_KEC) WHERE NO_PROP=32 AND NO_KAB = 73  GROUP BY NO_KAB";
			}
			
			$r = DB::select($sql);
			return $r[0]->val;
		}
		public function get_jum_kel($no_kec = 0){
			if ($no_kec !=0){
			$sql = "SELECT NAMA_KEL NAMA_WIL FROM SETUP_KEL WHERE NO_PROP=32 AND NO_KAB = 73 AND NO_KEC =$no_kec ORDER BY NO_KEL";
			}else{
				$sql = "SELECT NAMA_KEC NAMA_WIL FROM SETUP_KEC WHERE NO_PROP=32 AND NO_KAB = 73 ORDER BY NO_KEC";
			}
			$r = DB::select($sql);
			return $r;
		}
		public function rekap_detail_kk_bulan($user_id,$bln, $no_kec = 0, $no_kel = 0,$sql_kel = ''){
			if ($no_kec !=0){
			$sql = "SELECT * FROM
					(
					   SELECT TO_CHAR(A.PRINTED_DATE,'DD-MM-YYYY') PRINTED_DATE,A.NO_KK, F5_GET_NAMA_KELURAHAN(A.NO_PROP,A.NO_KAB,A.NO_KEC,A.NO_KEL) NAMA_KEL
					  	FROM T5_SEQN_PRINT_KK A INNER JOIN SIAK_USER_PLUS B ON A.PRINTED_BY = B.USER_SIAK WHERE TO_CHAR(A.PRINTED_DATE,'MM-YYYY') = '$bln' AND A.NO_KEC = $no_kec AND B.USER_ID ='$user_id'
					)
					PIVOT (COUNT(NO_KK) FOR (NAMA_KEL) IN ($sql_kel))
					ORDER BY PRINTED_DATE";
			}else{
			$sql = "SELECT * FROM
					(
					   SELECT TO_CHAR(A.PRINTED_DATE,'DD-MM-YYYY') PRINTED_DATE,A.NO_KK, F5_GET_NAMA_KECAMATAN(A.NO_PROP,A.NO_KAB,A.NO_KEC) NAMA_KEC
					  	FROM T5_SEQN_PRINT_KK A INNER JOIN SIAK_USER_PLUS B ON A.PRINTED_BY = B.USER_SIAK WHERE TO_CHAR(A.PRINTED_DATE,'MM-YYYY') = '$bln' AND B.USER_ID ='$user_id'
					)
					PIVOT (COUNT(NO_KK) FOR (NAMA_KEC) IN ($sql_kel))
					ORDER BY PRINTED_DATE";
			}

			$r = DB::select($sql);
			return $r;
		}
		public function rekap_kk($user_id,$tgl_start,$tgl_end, $no_kec = 0){
			if ($no_kec !=0){
			$sql = "SELECT 
					  A.NO_KEL NO_WIL
					  , F5_GET_NAMA_KELURAHAN(A.NO_PROP,A.NO_KAB,A.NO_KEC,A.NO_KEL) NAMA_WIL
					  , COUNT(1) JML 
					  FROM T5_SEQN_PRINT_KK A 
					  INNER JOIN SIAK_USER_PLUS B ON A.PRINTED_BY = B.USER_SIAK 
					  WHERE B.USER_ID = '$user_id' AND A.NO_KEC = $no_kec 
					  AND A.PRINTED_DATE >= TO_DATE('$tgl_start','DD-MM-YYYY') AND A.PRINTED_DATE < TO_DATE('$tgl_end','DD-MM-YYYY')+1
					  GROUP BY A.NO_PROP, A.NO_KAB, A.NO_KEC, A.NO_KEL ORDER BY A.NO_PROP, A.NO_KAB, A.NO_KEC, A.NO_KEL";
			}else{
			$sql = "SELECT 
					  A.NO_KEC NO_WIL
					  , F5_GET_NAMA_KECAMATAN(A.NO_PROP,A.NO_KAB,A.NO_KEC) NAMA_WIL
					  , COUNT(1) JML 
					  FROM T5_SEQN_PRINT_KK A 
					  INNER JOIN SIAK_USER_PLUS B ON A.PRINTED_BY = B.USER_SIAK 
					  WHERE B.USER_ID = '$user_id' 
					  AND A.PRINTED_DATE >= TO_DATE('$tgl_start','DD-MM-YYYY') AND A.PRINTED_DATE < TO_DATE('$tgl_end','DD-MM-YYYY')+1
					  GROUP BY A.NO_PROP, A.NO_KAB, A.NO_KEC ORDER BY A.NO_PROP, A.NO_KAB, A.NO_KEC";
			}

			$r = DB::select($sql);
			return $r;
		}
		public function rekap_kedatangan($tgl_start,$tgl_end, $no_kec = 0, $no_kel = 0){
			$sql = "";
			$sql .= "SELECT 
					    F5_GET_NAMA_KECAMATAN(A.NO_PROP,A.NO_KAB, A.NO_KEC) KEC
					    , F5_GET_NAMA_KELURAHAN(A.NO_PROP,A.NO_KAB, A.NO_KEC, A.NO_KEL) KEL
					    , CASE WHEN A.MK_RW IS NULL AND A.NK_RW IS NULL THEN 0 WHEN A.MK_RW IS NULL THEN A.NK_RW ELSE A.MK_RW END RW
                        , CASE WHEN A.MK_RT IS NULL AND A.NK_RT IS NULL THEN 0 WHEN A.MK_RT IS NULL THEN A.NK_RT ELSE A.MK_RT END RT
                        , CASE WHEN A.MK_ALAMAT IS NULL AND A.NK_ALAMAT IS NULL THEN '-' WHEN A.MK_ALAMAT IS NULL THEN A.NK_ALAMAT ELSE A.MK_ALAMAT END ALAMAT 
					    , A.NO_DATANG
					    , A.NO_PINDAH
					    , F5_GET_NAMA_PROVINSI(A.SRC_PROP) PROP_ASAL
					    , F5_GET_NAMA_KABUPATEN(A.SRC_PROP,A.SRC_KAB) KAB_ASAL
					    , F5_GET_NAMA_KECAMATAN(A.SRC_PROP,A.SRC_KAB, A.SRC_KEC) KEC_ASAL
					    , F5_GET_NAMA_KELURAHAN(A.SRC_PROP,A.SRC_KAB, A.SRC_KEC, A.SRC_KEL) KEL_ASAL
					    , TO_CHAR(A.NO_KK) NO_KK
					    , TO_CHAR(B.NIK) NIK_PEMOHON
					    , B.NAMA_LENGKAP NAMA_PEMOHON
					    , TO_CHAR(A.CREATED_DATE,'DD-MM-YYYY') TGL
					    , TO_CHAR(A.CREATED_DATE,'HH24:MM:SS') JAM
					FROM DATANG_HEADER A INNER JOIN DATANG_DETAIL B ON A.NO_DATANG = B.NO_DATANG WHERE 
					A.CREATED_DATE >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND A.CREATED_DATE < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1 
					AND KLASIFIKASI_PINDAH >= 4 AND A.NO_PROP = 32 AND A.NO_KAB = 73";
			if($no_kec != 0 ){ 
				$sql .= " AND A.NO_KEC = $no_kec ";
			} 
			if($no_kel != 0 ){ 
				$sql .= " AND A.NO_KEL = $no_kel ";
			} 
			$sql .= " ORDER BY A.CREATED_DATE DESC";
			$r = DB::connection('db222')->select($sql);
			return $r;
		}
		public function rekap_perpindahan($tgl_start,$tgl_end, $no_kec = 0, $no_kel = 0){
			$sql = "";
			$sql .= "SELECT 
					    F5_GET_NAMA_KECAMATAN(A.FROM_NO_PROP,A.FROM_NO_KAB, A.FROM_NO_KEC) KEC
					    , F5_GET_NAMA_KELURAHAN(A.FROM_NO_PROP,A.FROM_NO_KAB, A.FROM_NO_KEC, A.FROM_NO_KEL) KEL
					    , A.RW RW
                        , A.RT RT
                        , A.ALAMAT ALAMAT
					    , A.NO_PINDAH
					    , F5_GET_NAMA_PROVINSI(A.DEST_NO_PROP) PROP_ASAL
					    , F5_GET_NAMA_KABUPATEN(A.DEST_NO_PROP,A.DEST_NO_KAB) KAB_ASAL
					    , F5_GET_NAMA_KECAMATAN(A.DEST_NO_PROP,A.DEST_NO_KAB, A.DEST_NO_KEC) KEC_ASAL
					    , F5_GET_NAMA_KELURAHAN(A.DEST_NO_PROP,A.DEST_NO_KAB, A.DEST_NO_KEC, A.DEST_NO_KEL) KEL_ASAL
					    , TO_CHAR(A.NO_KK) NO_KK
					    , TO_CHAR(B.NIK) NIK_PEMOHON
					    , B.NAMA_LENGKAP NAMA_PEMOHON
					    , TO_CHAR(A.CREATED_DATE,'DD-MM-YYYY') TGL
					    , TO_CHAR(A.CREATED_DATE,'HH24:MM:SS') JAM
					FROM PINDAH_HEADER A INNER JOIN PINDAH_DETAIL B ON A.NO_PINDAH = B.NO_PINDAH WHERE 
					A.CREATED_DATE >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND A.CREATED_DATE < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1 
					AND KLASIFIKASI_PINDAH >= 4 AND A.FROM_NO_PROP = 32 AND A.FROM_NO_KAB = 73";
			if($no_kec != 0 ){ 
				$sql .= " AND A.FROM_NO_KEC = $no_kec ";
			} 
			if($no_kel != 0 ){ 
				$sql .= " AND A.FROM_NO_KEL = $no_kel ";
			} 
			$sql .= " ORDER BY A.CREATED_DATE DESC";
			$r = DB::connection('db222')->select($sql);
			return $r;
		}
		public function laporan_rekap_kedatangan($tgl_start,$tgl_end, $no_kec = 0, $no_kel = 0){
			$sql = "";
			$sql .= "SELECT 
					    F5_GET_NAMA_KECAMATAN(A.NO_PROP,A.NO_KAB, A.NO_KEC) KEC
					    , F5_GET_NAMA_KELURAHAN(A.NO_PROP,A.NO_KAB, A.NO_KEC, A.NO_KEL) KEL
					    , CASE WHEN A.MK_RW IS NULL AND A.NK_RW IS NULL THEN 0 WHEN A.MK_RW IS NULL THEN A.NK_RW ELSE A.MK_RW END RW
                        , CASE WHEN A.MK_RT IS NULL AND A.NK_RT IS NULL THEN 0 WHEN A.MK_RT IS NULL THEN A.NK_RT ELSE A.MK_RT END RT
                        , CASE WHEN A.MK_ALAMAT IS NULL AND A.NK_ALAMAT IS NULL THEN '-' WHEN A.MK_ALAMAT IS NULL THEN A.NK_ALAMAT ELSE A.MK_ALAMAT END ALAMAT 
					    , A.NO_DATANG
					    , A.NO_PINDAH
					    ,  CASE WHEN KLASIFIKASI_PINDAH >= 4 THEN F5_GET_NAMA_KABUPATEN(A.SRC_PROP,A.SRC_KAB) ELSE 'KEC. '||F5_GET_NAMA_KECAMATAN(A.SRC_PROP,A.SRC_KAB, A.SRC_KEC) END DAERAH_ASAL
					    , TO_CHAR(A.NO_KK) NO_KK
					    , TO_CHAR(B.NIK) NIK_PEMOHON
					    , B.NAMA_LENGKAP NAMA_PEMOHON
					    , TO_CHAR(A.CREATED_DATE,'DD-MM-YYYY') TGL
					    , TO_CHAR(A.CREATED_DATE,'HH24:MM:SS') JAM
					FROM DATANG_HEADER A INNER JOIN DATANG_DETAIL B ON A.NO_DATANG = B.NO_DATANG WHERE 
					A.CREATED_DATE >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND A.CREATED_DATE < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1 AND A.NO_PROP = 32 AND A.NO_KAB = 73";
			if($no_kec != 0 ){ 
				$sql .= " AND A.NO_KEC = $no_kec ";
			} 
			if($no_kel != 0 ){ 
				$sql .= " AND A.NO_KEL = $no_kel ";
			} 
			$sql .= " ORDER BY A.CREATED_DATE DESC";
			$r = DB::connection('db222')->select($sql);
			return $r;
		}
		public function laporan_rekap_perpindahan($tgl_start,$tgl_end, $no_kec = 0, $no_kel = 0){
			$sql = "";
			$sql .= "SELECT 
					    F5_GET_NAMA_KECAMATAN(A.FROM_NO_PROP,A.FROM_NO_KAB, A.FROM_NO_KEC) KEC
					    , F5_GET_NAMA_KELURAHAN(A.FROM_NO_PROP,A.FROM_NO_KAB, A.FROM_NO_KEC, A.FROM_NO_KEL) KEL
					    , A.RW RW
                        , A.RT RT
                        , A.ALAMAT ALAMAT
					    , A.NO_PINDAH
					    , CASE WHEN KLASIFIKASI_PINDAH >= 4 THEN F5_GET_NAMA_KABUPATEN(A.DEST_NO_PROP,A.DEST_NO_KAB) ELSE 'KEC. '||F5_GET_NAMA_KECAMATAN(A.DEST_NO_PROP,A.DEST_NO_KAB, A.DEST_NO_KEC) END DAERAH_TUJUAN
					    , TO_CHAR(A.NO_KK) NO_KK
					    , TO_CHAR(B.NIK) NIK_PEMOHON
					    , B.NAMA_LENGKAP NAMA_PEMOHON
					    , TO_CHAR(A.CREATED_DATE,'DD-MM-YYYY') TGL
					    , TO_CHAR(A.CREATED_DATE,'HH24:MM:SS') JAM
					FROM PINDAH_HEADER A INNER JOIN PINDAH_DETAIL B ON A.NO_PINDAH = B.NO_PINDAH WHERE 
					A.CREATED_DATE >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND A.CREATED_DATE < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1 AND A.FROM_NO_PROP = 32 AND A.FROM_NO_KAB = 73";
			if($no_kec != 0 ){ 
				$sql .= " AND A.FROM_NO_KEC = $no_kec ";
			} 
			if($no_kel != 0 ){ 
				$sql .= " AND A.FROM_NO_KEL = $no_kel ";
			} 
			$sql .= " ORDER BY A.CREATED_DATE DESC";
			$r = DB::connection('db222')->select($sql);
			return $r;
		}
		public function act_get_user($user_id){
			$sql = "SELECT USER_ID ,USER_PWD ,NAMA_LGKP ,NIK ,TMPT_LHR ,TGL_LHR ,JENIS_KLMIN ,TELP ,USER_SIAK ,USER_BCARD ,USER_BENROL FROM VW_SIAK_USER WHERE USER_ID = '$user_id'";
			$r = DB::select($sql);
			return $r;
		}
		public function lgn_check_user($user_id)
        {
         	$sql = "SELECT USER_ID, NAMA_LGKP, NIK, TMPT_LHR, TO_CHAR(TGL_LHR,'DD-MM-YYYY') AS TGL_LHR, JENIS_KLMIN, GOL_DRH, PENDIDIKAN, PEKERJAAN, NAMA_KANTOR, ALAMAT_KANTOR, TELP, ALAMAT_RUMAH, USER_LEVEL, NO_PROP, NO_KAB, NO_KEC, NO_KEL,NO_RW,NO_RT, NAMA_DPN, USER_PWD, IS_ASN,IPADDRESS_CHECK FROM SIAK_USER_PLUS WHERE USER_ID = '$user_id' ";
    	 	$r = DB::select($sql);
			return $r;
        }
        public function lgn_change_pwd($user_id, $new_password)
        {
                $sql = "UPDATE SIAK_USER_PLUS SET USER_PWD = '$new_password'  WHERE USER_ID = '$user_id'";
                $r = DB::update($sql);
        }
        public function act_cek_daily_absensi($user_id){
			$sql = "SELECT A.USER_ID, A.NAMA_LGKP,TO_CHAR(B.TANGGAL,'DD-MM-YYYY') AS TANGGAL, B.HARI, B.JAM_MASUK, B.JAM_KELUAR,CONCAT(TRUNC(24*(to_date(CONCAT(TO_CHAR(B.TANGGAL,'DD-MM-YYYY'),CONCAT(' ',B.JAM_KELUAR)), 'DD-MM-YYYY hh24:mi') - to_date(CONCAT(TO_CHAR(B.TANGGAL,'DD-MM-YYYY'),CONCAT(' ',B.JAM_MASUK)), 'DD-MM-YYYY hh24:mi')),0),' Jam') AS JAM_KERJA FROM SIAK_USER_PLUS A INNER JOIN
					(SELECT 
					  A.TANGGAL
					  , CASE 
					  WHEN TO_CHAR (A.TANGGAL, 'DY') = 'SAT' THEN 'SABTU'
					  WHEN TO_CHAR (A.TANGGAL, 'DY') = 'SUN' THEN 'MINGGU'
					  WHEN TO_CHAR (A.TANGGAL, 'DY') = 'MON' THEN 'SENIN'
					  WHEN TO_CHAR (A.TANGGAL, 'DY') = 'TUE' THEN 'SELASA'
					  WHEN TO_CHAR (A.TANGGAL, 'DY') = 'WED' THEN 'RABU'
					  WHEN TO_CHAR (A.TANGGAL, 'DY') = 'THU' THEN 'KAMIS'
					  WHEN TO_CHAR (A.TANGGAL, 'DY') = 'FRI' THEN 'JUMAT'
					  ELSE '' END AS HARI
					  , '$user_id' AS USER_ID
					  , CASE WHEN JAM_MASUK IS NULL THEN '-' ELSE JAM_MASUK END AS JAM_MASUK
					  , CASE WHEN JAM_KELUAR IS NULL THEN '-' ELSE JAM_KELUAR END AS JAM_KELUAR
					   FROM (SELECT TRUNC(SYSDATE, 'YYYY') + LEVEL - 1 AS TANGGAL	FROM DUAL CONNECT BY TRUNC(TRUNC(SYSDATE, 'YYYY') + LEVEL - 1, 'YYYY') = TRUNC(SYSDATE, 'YYYY')) A LEFT JOIN (select USER_ID,TANGGAL, NAMA, JAM_MASUK, JAM_KELUAR, KETERANGAN FROM SIAK_ABSENSI WHERE USER_ID ='$user_id') B ON A.TANGGAL = B.TANGGAL WHERE A.TANGGAL >= TO_DATE('02-02-2018','DD-MM-YYYY') AND A.TANGGAL <= TRUNC(SYSDATE)+1 ) B ON A.USER_ID = B.USER_ID  ORDER BY B.TANGGAL";
			$r = DB::select($sql);
			return $r;
		}
		public function act_cek_count_daily_absensi($user_id){
			$sql = "SELECT COUNT(1) AS JML FROM SIAK_USER_PLUS A INNER JOIN
					(SELECT 
					  A.TANGGAL
					  , CASE 
					  WHEN TO_CHAR (A.TANGGAL, 'DY') = 'SAT' THEN 'SABTU'
					  WHEN TO_CHAR (A.TANGGAL, 'DY') = 'SUN' THEN 'MINGGU'
					  WHEN TO_CHAR (A.TANGGAL, 'DY') = 'MON' THEN 'SENIN'
					  WHEN TO_CHAR (A.TANGGAL, 'DY') = 'TUE' THEN 'SELASA'
					  WHEN TO_CHAR (A.TANGGAL, 'DY') = 'WED' THEN 'RABU'
					  WHEN TO_CHAR (A.TANGGAL, 'DY') = 'THU' THEN 'KAMIS'
					  WHEN TO_CHAR (A.TANGGAL, 'DY') = 'FRI' THEN 'JUMAT'
					  ELSE '' END AS HARI
					  , '$user_id' AS USER_ID
					  , CASE WHEN JAM_MASUK IS NULL THEN '-' ELSE JAM_MASUK END AS JAM_MASUK
					  , CASE WHEN JAM_KELUAR IS NULL THEN '-' ELSE JAM_KELUAR END AS JAM_KELUAR
					   FROM (SELECT TRUNC(SYSDATE, 'YYYY') + LEVEL - 1 AS TANGGAL	FROM DUAL CONNECT BY TRUNC(TRUNC(SYSDATE, 'YYYY') + LEVEL - 1, 'YYYY') = TRUNC(SYSDATE, 'YYYY')) A LEFT JOIN (select USER_ID,TANGGAL, NAMA, JAM_MASUK, JAM_KELUAR, KETERANGAN FROM SIAK_ABSENSI WHERE USER_ID ='$user_id') B ON A.TANGGAL = B.TANGGAL WHERE A.TANGGAL >= TO_DATE('02-02-2018','DD-MM-YYYY') AND A.TANGGAL <= TRUNC(SYSDATE)+1 ) B ON A.USER_ID = B.USER_ID  ORDER BY B.TANGGAL";
			$r = DB::select($sql);
			return $r[0]->jml;
		}
		public function act_cek_daily_activity($tgl_start,$tgl_end,$user_id){
			$sql = "SELECT USER_ID,(SELECT SIAK_USER_PLUS.NAMA_LGKP FROM SIAK_USER_PLUS WHERE SIAK_DAILY.USER_ID = SIAK_USER_PLUS.USER_ID ) AS NAMA, CASE WHEN (SELECT SIAK_ACTIVITY.ACTIVITY FROM SIAK_ACTIVITY WHERE SIAK_ACTIVITY.ACTIVITY_ID = SIAK_DAILY.ACTIVITY_ID) IS NULL THEN 'LAINNYA' ELSE (SELECT SIAK_ACTIVITY.ACTIVITY FROM SIAK_ACTIVITY WHERE SIAK_ACTIVITY.ACTIVITY_ID = SIAK_DAILY.ACTIVITY_ID) END AS AKTIVITAS,UPPER(DESCRIPTION) KETERANGAN,  TO_CHAR(CREATED_DT,'DD-MM-YYYY') AS TGL_AKTIVITAS,START_ACTIVITY AS DARI,END_ACTIVITY AS SAMPAI,NUMBER_ACTIVITY AS JUMLAH_AKTIVITAS FROM SIAK_DAILY WHERE USER_ID = '$user_id' AND CREATED_DT >= TO_DATE('$tgl_start','DD-MM-YYYY') AND CREATED_DT < TO_DATE('$tgl_end','DD-MM-YYYY') +1 ORDER BY USER_ID,CREATED_DT DESC";
			$r = DB::select($sql);
			return $r;
		}
		public function act_cek_count_daily_activity($tgl_start,$tgl_end,$user_id){
			$sql = "SELECT COUNT(1) AS JML FROM SIAK_DAILY WHERE USER_ID = '$user_id' AND CREATED_DT >= TO_DATE('$tgl_start','DD-MM-YYYY') AND CREATED_DT < TO_DATE('$tgl_end','DD-MM-YYYY') +1";
			$r = DB::select($sql);
			return $r[0]->jml;
		}
}


