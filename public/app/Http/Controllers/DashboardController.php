<?php

namespace App\Http\Controllers;

use DB;
use Session;
use DateTime;
use PDF;
use Illuminate\Http\Request;
use App\Http\Controllers\SharedController as Shr;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class DashboardController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    public function index(Request $request){
    	return redirect()->route('logout');
    }

    public function ktpel(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
		    $menu_id = 5;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$data = array(
		 		"stitle"=>'Dashboard Ktp-El',
		 		"back_title"=>'Dashboard',
		 		"backurl"=>'Dashboard',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			return view('dashboard_ktpel/main',$data);
		}else{
			return redirect()->route('logout');
		}

	}

    public function Biodata(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
		    $menu_id = 6;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$data = array(
		 		"stitle"=>'Dashboard Biodata',
		 		"back_title"=>'Dashboard',
		 		"backurl"=>'Dashboard',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			return view('dashboard_biodata/main',$data);
		}else{
			return redirect()->route('logout');
		}

	}

    public function Mobilitas(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
		    $menu_id = 7;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$data = array(
		 		"stitle"=>'Dashboard Mobilitas',
		 		"back_title"=>'Dashboard',
		 		"backurl"=>'Dashboard',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			return view('dashboard_mobilitas/main',$data);
		}else{
			return redirect()->route('logout');
		}

	}

    public function Capil(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
		    $menu_id = 8;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$data = array(
		 		"stitle"=>'Dashboard Capil',
		 		"back_title"=>'Dashboard',
		 		"backurl"=>'Dashboard',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			return view('dashboard_capil/main',$data);
		}else{
			return redirect()->route('logout');
		}

	}

    public function Userktpel(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {

		    $menu_id = 3;
			$is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$r = $this->get_user_cetak();
			$data = array(
		 		"stitle"=>'Dashboard',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_cetak"=>$r,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			return view('dashboard_cetak_ktpel/main', $data);
		}else{
			return redirect()->route('logout');
		}

	}
	public function UserBcard(Request $request, $user_bcard)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 3;
		   $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$r = $this->get_detail_user_cetak($user_bcard);
			// $dtl_agama = $this->get_detail_agama_user_cetak($user_bcard);
			$dtl_kec = $this->get_detail_kec_user_cetak($user_bcard);
			$dtl_kel = $this->get_detail_kel_user_cetak($user_bcard);
			// $dtl_rw = $this->get_detail_rw_user_cetak($user_bcard);
			// $dtl_rt = $this->get_detail_rt_user_cetak($user_bcard);
			if (count($r) > 0 ){
				$data = array(
		 		"stitle"=>'Detail Cetak User '.$user_bcard,
		 		"mtitle"=>'Detail Cetak User '.$user_bcard,
		 		"my_url"=>'UserBcard',
		 		"type_tgl"=>'Request',
		 		"data"=>$r,
		 		"dtl_kec"=>$dtl_kec,
		 		"dtl_kel"=>$dtl_kel,
		 		// "dtl_rw"=>$dtl_rw,
		 		// "dtl_rt"=>$dtl_rt,
		 		// "dtl_agama"=>$dtl_agama,
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			return view('Cekcetak_byuser/main',$data);
			}else{
				return redirect()->route('cetak');
			}
		}else{
			return redirect()->route('logout');
		}
	}
	public function UserBcardTanggal(Request $request, $user_bcard, $tanggal)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 3;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$r = $this->get_detail_user_cetak_tanggal($user_bcard,$tanggal);
			$dtl_kec = $this->get_detail_kec_user_cetak_tanggal($user_bcard,$tanggal);
			$dtl_kel = $this->get_detail_kel_user_cetak_tanggal($user_bcard,$tanggal);
			if (count($r) > 0 ){
				$data = array(
		 		"stitle"=>'Detail Cetak User '.$user_bcard.' '.$tanggal,
		 		"mtitle"=>'Detail Cetak User '.$user_bcard.' '.$tanggal,
		 		"my_url"=>'UserBcard',
		 		"type_tgl"=>'Request',
		 		"data"=>$r,
		 		"dtl_kec"=>$dtl_kec,
		 		"dtl_kel"=>$dtl_kel,
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			return view('Cekcetak_byuser/main',$data);
			}else{
				return redirect()->route('cetak');
			}
		}else{
			return redirect()->route('logout');
		}
	}


    public function dashboard_graph(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
		    $menu_id = 5;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$data = array(
		 		"stitle"=>'Grafik Dashboard',
		 		"back_title"=>'Dashboard',
		 		"backurl"=>'Dashboard',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			return view('dashboard_withgraph/main',$data);
		}else{
			return redirect()->route('logout');
		}

	}

    public function gdert(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {

		    $menu_id = 5;
			$is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$tbl_name = "DEMOGRAPHICS";
			$date_col = "CREATED";
			$pk_col = "NIK";
			$check_exists_bio = 0;
			$is_today = 1;
			$is_database = "221";
			$is_dblink = "@DB222";
			$is_coklit = 0;
			$r = $this->get_detail_rek_global($tbl_name,$date_col,$pk_col,$check_exists_bio,$is_today,$is_database,$is_dblink,'','',$is_coklit);
			if($is_coklit == 1){
				$data = array(
				"function_id"=>'gdert',
		 		"status_tmp"=>0,
		 		"stitle"=>'Detail Perekaman Ktp-El',
		 		"stabletitle"=>'Detail Perekaman Ktp-El Tanggal '.strval(date("d-m-Y")).' Jam '.strval(date("H:i:s")),
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"back_title"=>'Dashboard Ktp-El',
		 		"backurl"=>'Dashboard/Ktpel',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    			);
			}else{
				$data = array(
				"function_id"=>'gdert',
		 		"status_tmp"=>1,
		 		"stitle"=>'Detail Perekaman Ktp-El',
		 		"stabletitle"=>'Detail Perekaman Ktp-El Tanggal '.strval(date("d-m-Y")).' Jam '.strval(date("H:i:s")),
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"back_title"=>'Dashboard Ktp-El',
		 		"backurl"=>'Dashboard/Ktpel',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    			);
			}
			
			return view('temporary/main',$data);
		}else{
			return redirect()->route('logout');
		}

	}
	public function gdery(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {

		    $menu_id = 5;
			$is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$tbl_name = "DEMOGRAPHICS";
			$date_col = "CREATED";
			$pk_col = "NIK";
			$check_exists_bio = 0;
			$is_today = 2;
			$is_database = "221";
			$is_dblink = "@DB222";
			$is_coklit = 0;
			$dt = new DateTime();
			$dt->modify('-1 day');
			$renewalDate  = $dt->format('d-m-Y');
			$r = $this->get_detail_rek_global($tbl_name,$date_col,$pk_col,$check_exists_bio,$is_today,$is_database,$is_dblink,'','',$is_coklit);
			if($is_coklit == 1){
				$data = array(
				"function_id"=>'gdery',
		 		"status_tmp"=>0,
		 		"stitle"=>'Detail Perekaman Ktp-El',
		 		"stabletitle"=>'Detail Perekaman Ktp-El Tanggal '.$renewalDate,
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"back_title"=>'Dashboard Ktp-El',
		 		"backurl"=>'Dashboard/Ktpel',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    			);
			}else{
				$data = array(
				"function_id"=>'gdery',
		 		"status_tmp"=>0,
		 		"stitle"=>'Detail Perekaman Ktp-El',
		 		"stabletitle"=>'Detail Perekaman Ktp-El Tanggal '.$renewalDate,
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"back_title"=>'Dashboard Ktp-El',
		 		"backurl"=>'Dashboard/Ktpel',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    			);
			}
			
			return view('temporary/main',$data);
		}else{
			return redirect()->route('logout');
		}

	}

    public function gdect(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {

		    $menu_id = 5;
			$is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$tbl_name = "CARD_MANAGEMENT";
			$date_col = "PERSONALIZED_DATE";
			$pk_col = "NIK";
			$check_exists_bio = 0;
			$is_today = 1;
			$is_database = "2";
			$is_dblink = "@DB222";
			$r = $this->get_detail_ctk_global($is_today,$is_database);
			$data = array(
				"function_id"=>'gdect',
		 		"status_tmp"=>1,
		 		"stitle"=>'Detail Pencetakan Ktp-El',
		 		"stabletitle"=>'Detail Pencetakan Ktp-El Tanggal '.strval(date("d-m-Y")).' Jam '.strval(date("H:i:s")),
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"back_title"=>'Dashboard Ktp-El',
		 		"backurl"=>'Dashboard/Ktpel',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			return view('temporary/main',$data);
		}else{
			return redirect()->route('logout');
		}

	}
	public function gdecy(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {

		    $menu_id = 5;
			$is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$tbl_name = "CARD_MANAGEMENT";
			$date_col = "PERSONALIZED_DATE";
			$pk_col = "NIK";
			$check_exists_bio = 0;
			$is_today = 2;
			$is_database = "2";
			$is_dblink = "@DB222";
			$dt = new DateTime();
			$dt->modify('-1 day');
			$renewalDate  = $dt->format('d-m-Y');
			$r = $this->get_detail_ctk_global($is_today,$is_database);
			$data = array(
				"function_id"=>'gdecy',
		 		"status_tmp"=>1,
		 		"stitle"=>'Detail Pencetakan Ktp-El',
		 		"stabletitle"=>'Detail Pencetakan Ktp-El Tanggal '.$renewalDate,
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"back_title"=>'Dashboard Ktp-El',
		 		"backurl"=>'Dashboard/Ktpel',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			return view('temporary/main',$data);
		}else{
			return redirect()->route('logout');
		}

	}

    public function gdest(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {

		    $menu_id = 5;
			$is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$tbl_name = "T7_HIST_SUKET";
			$date_col = "PRINTED_DATE";
			$pk_col = "NIK";
			$check_exists_bio = 0;
			$is_today = 1;
			$is_database = "222";
			$r = $this->get_detail_suket($tbl_name,$date_col,$pk_col,$check_exists_bio,$is_today,$is_database);
			$data = array(
				"function_id"=>'gdest',
		 		"status_tmp"=>1,
		 		"stitle"=>'Detail Pencetakan Suket',
		 		"stabletitle"=>'Detail Pencetakan Suket Tanggal '.strval(date("d-m-Y")).' Jam '.strval(date("H:i:s")),
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"back_title"=>'Dashboard Ktp-El',
		 		"backurl"=>'Dashboard/Ktpel',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			return view('temporary/main',$data);
		}else{
			return redirect()->route('logout');
		}

	}
	public function gdesy(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {

		    $menu_id = 5;
			$is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$tbl_name = "T7_HIST_SUKET";
			$date_col = "PRINTED_DATE";
			$pk_col = "NIK";
			$check_exists_bio = 0;
			$is_today = 2;
			$is_database = "222";
			$dt = new DateTime();
			$dt->modify('-1 day');
			$renewalDate  = $dt->format('d-m-Y');
			$r = $this->get_detail_suket($tbl_name,$date_col,$pk_col,$check_exists_bio,$is_today,$is_database);
			$data = array(
				"function_id"=>'gdesy',
		 		"status_tmp"=>1,
		 		"stitle"=>'Detail Pencetakan Suket',
		 		"stabletitle"=>'Detail Pencetakan Suket Tanggal '.$renewalDate,
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"back_title"=>'Dashboard Ktp-El',
		 		"backurl"=>'Dashboard/Ktpel',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			return view('temporary/main',$data);
		}else{
			return redirect()->route('logout');
		}

	}


    public function gdbkt(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {

		    $menu_id = 6;
			$is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$tbl_name = "DATA_KELUARGA";
			$date_col = "TGL_INSERTION";
			$pk_col = "NO_KK";
			$check_exists_bio = 0;
			$is_today = 1;
			$is_database = "222";
			$r = $this->get_detail_global($tbl_name,$date_col,$pk_col,$check_exists_bio,$is_today,$is_database);
			$data = array(
				"function_id"=>'gdbkt',
		 		"status_tmp"=>1,
		 		"stitle"=>'Detail Pendaftaran KK',
		 		"stabletitle"=>'Detail Pendaftaran KK Tanggal '.strval(date("d-m-Y")).' Jam '.strval(date("H:i:s")),
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"back_title"=>'Dashboard Biodata',
		 		"backurl"=>'Dashboard/Biodata',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			return view('temporary/main',$data);
		}else{
			return redirect()->route('logout');
		}

	}
	public function gdbky(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {

		    $menu_id = 6;
			$is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$tbl_name = "DATA_KELUARGA";
			$date_col = "TGL_INSERTION";
			$pk_col = "NO_KK";
			$check_exists_bio = 0;
			$is_today = 2;
			$is_database = "222";
			$dt = new DateTime();
			$dt->modify('-1 day');
			$renewalDate  = $dt->format('d-m-Y');
			$r = $this->get_detail_global($tbl_name,$date_col,$pk_col,$check_exists_bio,$is_today,$is_database);
			$data = array(
				"function_id"=>'gdbky',
		 		"status_tmp"=>1,
		 		"stitle"=>'Detail Pendaftaran KK',
		 		"stabletitle"=>'Detail Pendaftaran KK Tanggal '.$renewalDate,
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"back_title"=>'Dashboard Biodata',
		 		"backurl"=>'Dashboard/Biodata',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			return view('temporary/main',$data);
		}else{
			return redirect()->route('logout');
		}

	}


    public function gdbbt(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {

		    $menu_id = 6;
			$is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$tbl_name = "BIODATA_WNI";
			$date_col = "TGL_ENTRI";
			$pk_col = "NIK";
			$check_exists_bio = 0;
			$is_today = 1;
			$is_database = "222";
			$r = $this->get_detail_global($tbl_name,$date_col,$pk_col,$check_exists_bio,$is_today,$is_database);
			$data = array(
				"function_id"=>'gdbbt',
		 		"status_tmp"=>1,
		 		"stitle"=>'Detail Pendaftaran Biodata',
		 		"stabletitle"=>'Detail Pendaftaran Biodata Tanggal '.strval(date("d-m-Y")).' Jam '.strval(date("H:i:s")),
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"back_title"=>'Dashboard Biodata',
		 		"backurl"=>'Dashboard/Biodata',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			return view('temporary/main',$data);
		}else{
			return redirect()->route('logout');
		}

	}
	public function gdbby(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {

		    $menu_id = 6;
			$is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$tbl_name = "BIODATA_WNI";
			$date_col = "TGL_ENTRI";
			$pk_col = "NIK";
			$check_exists_bio = 0;
			$is_today = 2;
			$is_database = "222";
			$dt = new DateTime();
			$dt->modify('-1 day');
			$renewalDate  = $dt->format('d-m-Y');
			$r = $this->get_detail_global($tbl_name,$date_col,$pk_col,$check_exists_bio,$is_today,$is_database);
			$data = array(
				"function_id"=>'gdbby',
		 		"status_tmp"=>1,
		 		"stitle"=>'Detail Pendaftaran Biodata',
		 		"stabletitle"=>'Detail Pendaftaran Biodata Tanggal '.$renewalDate,
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"back_title"=>'Dashboard Biodata',
		 		"backurl"=>'Dashboard/Biodata',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			return view('temporary/main',$data);
		}else{
			return redirect()->route('logout');
		}

	}


    public function gdmdt(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {

		    $menu_id = 7;
			$is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$tbl_name = "DATANG_HEADER A INNER JOIN DATANG_DETAIL B";
			$date_col = "CREATED_DATE";
			$pk_col = "NO_DATANG";
			$check_is_pindah = 0;
			$is_today = 1;
			$r = $this->get_detail_global_mobilitas($tbl_name,$date_col,$pk_col,$check_is_pindah,$is_today);
			$data = array(
				"function_id"=>'gdmdt',
		 		"status_tmp"=>2,
		 		"stitle"=>'Detail Kedatangan',
		 		"stabletitle"=>'Detail Pendaftaran Kedatangan Tanggal '.strval(date("d-m-Y")).' Jam '.strval(date("H:i:s")),
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"back_title"=>'Dashboard Mobilitas',
		 		"backurl"=>'Dashboard/Mobilitas',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			return view('temporary/main',$data);
		}else{
			return redirect()->route('logout');
		}

	}
	public function gdmdy(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {

		    $menu_id = 7;
			$is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$tbl_name = "DATANG_HEADER A INNER JOIN DATANG_DETAIL B";
			$date_col = "CREATED_DATE";
			$pk_col = "NO_DATANG";
			$check_is_pindah = 0;
			$is_today = 2;
			$dt = new DateTime();
			$dt->modify('-1 day');
			$renewalDate  = $dt->format('d-m-Y');
			$r = $this->get_detail_global_mobilitas($tbl_name,$date_col,$pk_col,$check_is_pindah,$is_today);
			$data = array(
				"function_id"=>'gdmdy',
		 		"status_tmp"=>2,
		 		"stitle"=>'Detail Kedatangan',
		 		"stabletitle"=>'Detail Pendaftaran Kedatangan Tanggal '.$renewalDate,
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"back_title"=>'Dashboard Mobilitas',
		 		"backurl"=>'Dashboard/Mobilitas',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			return view('temporary/main',$data);
		}else{
			return redirect()->route('logout');
		}

	}


    public function gdmpt(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {

		    $menu_id = 7;
			$is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$tbl_name = "PINDAH_HEADER A INNER JOIN PINDAH_DETAIL B";
			$date_col = "CREATED_DATE";
			$pk_col = "NO_PINDAH";
			$check_is_pindah = 1;
			$is_today = 1;
			$r = $this->get_detail_global_mobilitas($tbl_name,$date_col,$pk_col,$check_is_pindah,$is_today);
			$data = array(
				"function_id"=>'gdmpt',
		 		"status_tmp"=>2,
		 		"stitle"=>'Detail Kepindahan',
		 		"stabletitle"=>'Detail Pendaftaran Kepindahan Tanggal '.strval(date("d-m-Y")).' Jam '.strval(date("H:i:s")),
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"back_title"=>'Dashboard Mobilitas',
		 		"backurl"=>'Dashboard/Mobilitas',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			return view('temporary/main',$data);
		}else{
			return redirect()->route('logout');
		}

	}
	public function gdmpy(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {

		    $menu_id = 7;
			$is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$tbl_name = "PINDAH_HEADER A INNER JOIN PINDAH_DETAIL B";
			$date_col = "CREATED_DATE";
			$pk_col = "NO_PINDAH";
			$check_is_pindah = 1;
			$is_today = 2;
			$dt = new DateTime();
			$dt->modify('-1 day');
			$renewalDate  = $dt->format('d-m-Y');
			$r = $this->get_detail_global_mobilitas($tbl_name,$date_col,$pk_col,$check_is_pindah,$is_today);
			$data = array(
				"function_id"=>'gdmpy',
		 		"status_tmp"=>2,
		 		"stitle"=>'Detail Kepindahan',
		 		"stabletitle"=>'Detail Pendaftaran Kepindahan Tanggal '.$renewalDate,
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"back_title"=>'Dashboard Mobilitas',
		 		"backurl"=>'Dashboard/Mobilitas',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			return view('temporary/main',$data);
		}else{
			return redirect()->route('logout');
		}

	}


    public function gdclut(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {

		    $menu_id = 8;
			$is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$tbl_name = "CAPIL_LAHIR";
			$date_col = "ADM_TGL_ENTRY";
			$pk_col = "BAYI_NIK";
			$check_exists_bio = 0;
			$is_today = 1;
			$is_database = "222";
			$is_where = " AND A.ADM_AKTA_NO LIKE '%LU%' AND A.ADM_NO_PROV= 32 AND A.ADM_NO_KAB=73 AND A.BAYI_ORG_ASING = 'T' AND A.BAYI_DOM_LAHIR = 'D' ";
			$r = $this->get_detail_capil_lahir($tbl_name,$date_col,$pk_col,$check_exists_bio,$is_today,$is_database,$is_where);
			$data = array(
				"function_id"=>'gdclut',
		 		"status_tmp"=>1,
		 		"stitle"=>'Detail Kelahiran Umum',
		 		"stabletitle"=>'Detail Kelahiran Umum Tanggal '.strval(date("d-m-Y")).' Jam '.strval(date("H:i:s")),
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"back_title"=>'Dashboard Capil',
		 		"backurl"=>'Dashboard/Capil',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			return view('temporary/main',$data);
		}else{
			return redirect()->route('logout');
		}

	}
	public function gdcluy(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {

		    $menu_id = 8;
			$is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$tbl_name = "CAPIL_LAHIR";
			$date_col = "ADM_TGL_ENTRY";
			$pk_col = "BAYI_NIK";
			$check_exists_bio = 0;
			$is_today = 2;
			$is_database = "222";
			$is_where = " AND A.ADM_AKTA_NO LIKE '%LU%' AND A.ADM_NO_PROV= 32 AND A.ADM_NO_KAB=73 AND A.BAYI_ORG_ASING = 'T' AND A.BAYI_DOM_LAHIR = 'D' ";
			$dt = new DateTime();
			$dt->modify('-1 day');
			$renewalDate  = $dt->format('d-m-Y');
			$r = $this->get_detail_capil_lahir($tbl_name,$date_col,$pk_col,$check_exists_bio,$is_today,$is_database,$is_where);
			$data = array(
				"function_id"=>'gdcluy',
		 		"status_tmp"=>1,
		 		"stitle"=>'Detail Kelahiran Umum',
		 		"stabletitle"=>'Detail Kelahiran Umum Tanggal '.$renewalDate,
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"back_title"=>'Dashboard Capil',
		 		"backurl"=>'Dashboard/Capil',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			return view('temporary/main',$data);
		}else{
			return redirect()->route('logout');
		}

	}


    public function gdcltt(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {

		    $menu_id = 8;
			$is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$tbl_name = "CAPIL_LAHIR";
			$date_col = "ADM_TGL_ENTRY";
			$pk_col = "BAYI_NIK";
			$check_exists_bio = 0;
			$is_today = 1;
			$is_database = "222";
			$is_where = " AND A.ADM_AKTA_NO LIKE '%LT%' AND A.ADM_NO_PROV= 32 AND A.ADM_NO_KAB=73 AND A.BAYI_ORG_ASING = 'T' AND A.BAYI_DOM_LAHIR = 'D' ";
			$r = $this->get_detail_capil_lahir($tbl_name,$date_col,$pk_col,$check_exists_bio,$is_today,$is_database,$is_where);
			$data = array(
				"function_id"=>'gdcltt',
		 		"status_tmp"=>1,
		 		"stitle"=>'Detail Kelahiran Terlambat',
		 		"stabletitle"=>'Detail Kelahiran Terlambat Tanggal '.strval(date("d-m-Y")).' Jam '.strval(date("H:i:s")),
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"back_title"=>'Dashboard Capil',
		 		"backurl"=>'Dashboard/Capil',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			return view('temporary/main',$data);
		}else{
			return redirect()->route('logout');
		}

	}
	public function gdclty(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {

		    $menu_id = 8;
			$is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$tbl_name = "CAPIL_LAHIR";
			$date_col = "ADM_TGL_ENTRY";
			$pk_col = "BAYI_NIK";
			$check_exists_bio = 0;
			$is_today = 2;
			$is_database = "222";
			$is_where = " AND A.ADM_AKTA_NO LIKE '%LT%' AND A.ADM_NO_PROV= 32 AND A.ADM_NO_KAB=73 AND A.BAYI_ORG_ASING = 'T' AND A.BAYI_DOM_LAHIR = 'D' ";
			$dt = new DateTime();
			$dt->modify('-1 day');
			$renewalDate  = $dt->format('d-m-Y');
			$r = $this->get_detail_capil_lahir($tbl_name,$date_col,$pk_col,$check_exists_bio,$is_today,$is_database,$is_where);
			$data = array(
				"function_id"=>'gdclty',
		 		"status_tmp"=>1,
		 		"stitle"=>'Detail Kelahiran Terlambat',
		 		"stabletitle"=>'Detail Kelahiran Terlambat Tanggal '.$renewalDate,
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"back_title"=>'Dashboard Capil',
		 		"backurl"=>'Dashboard/Capil',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			return view('temporary/main',$data);
		}else{
			return redirect()->route('logout');
		}

	}


    public function gdckt(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {

		    $menu_id = 8;
			$is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$tbl_name = "CAPIL_KAWIN";
			$date_col = "KAWIN_TGL_LAPOR";
			$get_col = " A.KAWIN_NO AS CAPIL_1
    	 				, TO_CHAR(A.KAWIN_TGL_KAWIN,'DD-MM-YYYY') AS CAPIL_2
    	 				, A.SUAMI_NAMA_LGKP AS CAPIL_3
    	 				, A.ISTRI_NAMA_LGKP AS CAPIL_4
    	 				, A.KAWIN_TGL_LAPOR AS CAPIL_5 ";
			$is_today = 1;
			$r = $this->get_detail_capil($tbl_name,$date_col,$is_today,$get_col);
			$data = array(
				"function_id"=>'gdckt',
		 		"status_tmp"=>3,
		 		"stitle"=>'Detail Perkawinan',
		 		"stabletitle"=>'Detail Pencatatan Perkawinan Tanggal '.strval(date("d-m-Y")).' Jam '.strval(date("H:i:s")),
		 		"data"=>$r,
		 		"back_title"=>'Dashboard Capil',
		 		"backurl"=>'Dashboard/Capil',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"col1"=>"No Kawin",
		 		"col2"=>"Tanggal Kawin",
		 		"col3"=>"Nama Suami",
		 		"col4"=>"Nama Istri",
		 		"col5"=>"Tanggal Lapor Kawin"
    		);
			return view('temporary/main',$data);
		}else{
			return redirect()->route('logout');
		}

	}
	public function gdcky(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {

		    $menu_id = 8;
			$is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$tbl_name = "CAPIL_KAWIN";
			$date_col = "KAWIN_TGL_LAPOR";
			$get_col = " A.KAWIN_NO AS CAPIL_1
    	 				, TO_CHAR(A.KAWIN_TGL_KAWIN,'DD-MM-YYYY') AS CAPIL_2
    	 				, A.SUAMI_NAMA_LGKP AS CAPIL_3
    	 				, A.ISTRI_NAMA_LGKP AS CAPIL_4
    	 				, A.KAWIN_TGL_LAPOR AS CAPIL_5 ";
			$is_today = 2;
			$dt = new DateTime();
			$dt->modify('-1 day');
			$renewalDate  = $dt->format('d-m-Y');
			$r = $this->get_detail_capil($tbl_name,$date_col,$is_today,$get_col);
			$data = array(
				"function_id"=>'gdcky',
		 		"status_tmp"=>3,
		 		"stitle"=>'Detail Perkawinan',
		 		"stabletitle"=>'Detail Pencatatan Perkawinan Tanggal '.$renewalDate,
		 		"data"=>$r,
		 		"back_title"=>'Dashboard Capil',
		 		"backurl"=>'Dashboard/Capil',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"col1"=>"No Kawin",
		 		"col2"=>"Tanggal Kawin",
		 		"col3"=>"Nama Suami",
		 		"col4"=>"Nama Istri",
		 		"col5"=>"Tanggal Lapor Kawin"
    		);
			return view('temporary/main',$data);
		}else{
			return redirect()->route('logout');
		}

	}


    public function gdcct(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {

		    $menu_id = 8;
			$is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$tbl_name = "CAPIL_CERAI";
			$date_col = "CERAI_TGL_LAPOR";
			$get_col = " A.CERAI_NO AS CAPIL_1
    	 				, TO_CHAR(A.CERAI_TGL_AKTA_KAWIN,'DD-MM-YYYY') AS CAPIL_2
    	 				, A.SUAMI_NAMA_LGKP AS CAPIL_3
    	 				, A.ISTRI_NAMA_LGKP AS CAPIL_4
    	 				, A.CERAI_TGL_LAPOR AS CAPIL_5 ";
			$is_today = 1;
			$r = $this->get_detail_capil($tbl_name,$date_col,$is_today,$get_col);
			$data = array(
				"function_id"=>'gdcct',
		 		"status_tmp"=>3,
		 		"stitle"=>'Detail Perceraian',
		 		"stabletitle"=>'Detail Pencatatan Perceraian Tanggal '.strval(date("d-m-Y")).' Jam '.strval(date("H:i:s")),
		 		"data"=>$r,
		 		"back_title"=>'Dashboard Capil',
		 		"backurl"=>'Dashboard/Capil',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"col1"=>"No Cerai",
		 		"col2"=>"Tanggal Kawin",
		 		"col3"=>"Nama Suami",
		 		"col4"=>"Nama Istri",
		 		"col5"=>"Tanggal Lapor Cerai"
    		);
			return view('temporary/main',$data);
		}else{
			return redirect()->route('logout');
		}

	}
	public function gdccy(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {

		    $menu_id = 8;
			$is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$tbl_name = "CAPIL_CERAI";
			$date_col = "CERAI_TGL_LAPOR";
			$get_col = " A.CERAI_NO AS CAPIL_1
    	 				, TO_CHAR(A.CERAI_TGL_AKTA_KAWIN,'DD-MM-YYYY') AS CAPIL_2
    	 				, A.SUAMI_NAMA_LGKP AS CAPIL_3
    	 				, A.ISTRI_NAMA_LGKP AS CAPIL_4
    	 				, A.CERAI_TGL_LAPOR AS CAPIL_5 ";
			$is_today = 2;
			$dt = new DateTime();
			$dt->modify('-1 day');
			$renewalDate  = $dt->format('d-m-Y');
			$r = $this->get_detail_capil($tbl_name,$date_col,$is_today,$get_col);
			$data = array(
				"function_id"=>'gdccy',
		 		"status_tmp"=>3,
		 		"stitle"=>'Detail Perceraian',
		 		"stabletitle"=>'Detail Pencatatan Perceraian Tanggal '.$renewalDate,
		 		"data"=>$r,
		 		"back_title"=>'Dashboard Capil',
		 		"backurl"=>'Dashboard/Capil',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"col1"=>"No Cerai",
		 		"col2"=>"Tanggal Kawin",
		 		"col3"=>"Nama Suami",
		 		"col4"=>"Nama Istri",
		 		"col5"=>"Tanggal Lapor Cerai"
    		);
			return view('temporary/main',$data);
		}else{
			return redirect()->route('logout');
		}

	}


    public function gdcmt(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {

		    $menu_id = 8;
			$is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$tbl_name = "CAPIL_MATI";
			$date_col = "ADM_TGL_ENTRY";
			$get_col = " A.ADM_AKTA_NO AS CAPIL_1
    	 				, A.MATI_NAMA_LGKP AS CAPIL_2
    	 				, A.MATI_TGL_LAHIR AS CAPIL_3
    	 				, A.MATI_TGL_MATI AS CAPIL_4
    	 				, A.ADM_TGL_ENTRY AS CAPIL_5 ";
			$is_today = 1;
			$r = $this->get_detail_capil($tbl_name,$date_col,$is_today,$get_col);
			$data = array(
				"function_id"=>'gdcmt',
		 		"status_tmp"=>3,
		 		"stitle"=>'Detail Kematian',
		 		"stabletitle"=>'Detail Pencatatan Kematian Tanggal '.strval(date("d-m-Y")).' Jam '.strval(date("H:i:s")),
		 		"data"=>$r,
		 		"back_title"=>'Dashboard Capil',
		 		"backurl"=>'Dashboard/Capil',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"col1"=>"No Mati",
		 		"col2"=>"Nama Lengkap",
		 		"col3"=>"Tanggal Lahir",
		 		"col4"=>"Tanggal Mati",
		 		"col5"=>"Tanggal Lapor Mati"
    		);
			return view('temporary/main',$data);
		}else{
			return redirect()->route('logout');
		}

	}
	public function gdcmy(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {

		    $menu_id = 8;
			$is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$tbl_name = "CAPIL_MATI";
			$date_col = "ADM_TGL_ENTRY";
			$get_col = " A.ADM_AKTA_NO AS CAPIL_1
    	 				, A.MATI_NAMA_LGKP AS CAPIL_2
    	 				, A.MATI_TGL_LAHIR AS CAPIL_3
    	 				, A.MATI_TGL_MATI AS CAPIL_4
    	 				, A.ADM_TGL_ENTRY AS CAPIL_5 ";
			$is_today = 2;
			$dt = new DateTime();
			$dt->modify('-1 day');
			$renewalDate  = $dt->format('d-m-Y');
			$r = $this->get_detail_capil($tbl_name,$date_col,$is_today,$get_col);
			$data = array(
				"function_id"=>'gdcmy',
		 		"status_tmp"=>3,
		 		"stitle"=>'Detail Kematian',
		 		"stabletitle"=>'Detail Pencatatan Kematian Tanggal '.$renewalDate,
		 		"data"=>$r,
		 		"back_title"=>'Dashboard Capil',
		 		"backurl"=>'Dashboard/Capil',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"col1"=>"No Mati",
		 		"col2"=>"Nama Lengkap",
		 		"col3"=>"Tanggal Lahir",
		 		"col4"=>"Tanggal Mati",
		 		"col5"=>"Tanggal Lapor Mati"
    		);
			return view('temporary/main',$data);
		}else{
			return redirect()->route('logout');
		}

	}


    public function gdsjrt(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {

		    $menu_id = 5;
			$is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$tbl_name = "SIAK_REQ_CETAK_KTP@YZDB";
			$date_col = "REQ_DATE";
			$get_col = " TO_CHAR(A.REQ_DATE,'DD-MM-YYYY HH24:MI') AS CAPIL_1
						, A.NIK AS CAPIL_2
    	 				, A.NO_KK AS CAPIL_3
    	 				, A.NAMA_LENGKAP AS CAPIL_4
    	 				, A.REQ_BY AS CAPIL_5 ";
    	 	$is_where = '';
			$is_today = 1;
			$r = $this->get_detail_request($tbl_name,$date_col,$is_today,$get_col,$is_where);
			$data = array(
				"function_id"=>'gdsjrt',
		 		"status_tmp"=>3,
		 		"stitle"=>'Detail Suket',
		 		"stabletitle"=>'Detail Jumlah Request Tanggal '.strval(date("d-m-Y")).' Jam '.strval(date("H:i:s")),
		 		"data"=>$r,
		 		"back_title"=>'Dashboard Ktp-El',
		 		"backurl"=>'Dashboard/Ktpel',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"col1"=>"Tanggal Request",
		 		"col2"=>"NIK",
		 		"col3"=>"No KK",
		 		"col4"=>"Nama Lengkap",
		 		"col5"=>"User Request"
    		);
			return view('temporary/main',$data);
		}else{
			return redirect()->route('logout');
		}

	}
	public function gdsjry(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {

		    $menu_id = 5;
			$is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$tbl_name = "SIAK_REQ_CETAK_KTP@YZDB";
			$date_col = "REQ_DATE";
			$get_col = " TO_CHAR(A.REQ_DATE,'DD-MM-YYYY HH24:MI') AS CAPIL_1
						, A.NIK AS CAPIL_2
    	 				, A.NO_KK AS CAPIL_3
    	 				, A.NAMA_LENGKAP AS CAPIL_4
    	 				, A.REQ_BY AS CAPIL_5 ";
			$is_where = '';
			$is_today = 2;
			$dt = new DateTime();
			$dt->modify('-1 day');
			$renewalDate  = $dt->format('d-m-Y');
			$r = $this->get_detail_request($tbl_name,$date_col,$is_today,$get_col,$is_where);
			$data = array(
				"function_id"=>'gdsjry',
		 		"status_tmp"=>3,
		 		"stitle"=>'Detail Suket',
		 		"stabletitle"=>'Detail Jumlah Request Tanggal '.$renewalDate,
		 		"data"=>$r,
		 		"back_title"=>'Dashboard Ktp-El',
		 		"backurl"=>'Dashboard/Ktpel',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"col1"=>"Tanggal Request",
		 		"col2"=>"NIK",
		 		"col3"=>"No KK",
		 		"col4"=>"Nama Lengkap",
		 		"col5"=>"User Request"
    		);
			return view('temporary/main',$data);
		}else{
			return redirect()->route('logout');
		}

	}


    public function gdsjnrt(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {

		    $menu_id = 5;
			$is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$tbl_name = "T7_HIST_SUKET";
			$date_col = "PRINTED_DATE";
			$get_col = " TO_CHAR(A.PRINTED_DATE,'DD-MM-YYYY HH24:MI') AS CAPIL_1
						, A.NIK AS CAPIL_2
    	 				, A.NO_KK AS CAPIL_3
    	 				, A.NAMA_LGKP AS CAPIL_4
    	 				, (SELECT NAMA_KEC FROM SETUP_KEC C WHERE C.NO_PROP = 32 AND NO_KAB =73 AND C.NO_KEC = A.NO_KEC) AS CAPIL_5 ";
    	 	$is_where = " AND NOT EXISTS(SELECT 1 FROM SIAK_REQ_CETAK_KTP@YZDB B WHERE A.NIK = B.NIK AND TO_CHAR(B.REQ_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY')) AND NOT EXISTS(SELECT 1 FROM SIAK_REQ_CETAK_KTP@YZDB B WHERE A.NIK = B.NIK AND TO_CHAR(B.REQ_DATE,'DD/MM/YYYY') <> TO_CHAR(SYSDATE,'DD/MM/YYYY'))";
			$is_today = 1;
			$r = $this->get_detail_request($tbl_name,$date_col,$is_today,$get_col,$is_where);
			$data = array(
				"function_id"=>'gdsjnrt',
		 		"status_tmp"=>3,
		 		"stitle"=>'Detail Cetak Non Request',
		 		"stabletitle"=>'Detail Jumlah Cetak Non Request Tanggal '.strval(date("d-m-Y")).' Jam '.strval(date("H:i:s")),
		 		"data"=>$r,
		 		"back_title"=>'Dashboard Ktp-El',
		 		"backurl"=>'Dashboard/Ktpel',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"col1"=>"Tanggal Cetak",
		 		"col2"=>"NIK",
		 		"col3"=>"No KK",
		 		"col4"=>"Nama Lengkap",
		 		"col5"=>"Nama Kecamatan"
    		);
			return view('temporary/main',$data);
		}else{
			return redirect()->route('logout');
		}

	}
	public function gdsjnry(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {

		    $menu_id = 5;
			$is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$tbl_name = "T7_HIST_SUKET";
			$date_col = "PRINTED_DATE";
			$get_col = " TO_CHAR(A.PRINTED_DATE,'DD-MM-YYYY HH24:MI') AS CAPIL_1
						, A.NIK AS CAPIL_2
    	 				, A.NO_KK AS CAPIL_3
    	 				, A.NAMA_LGKP AS CAPIL_4
    	 				, (SELECT NAMA_KEC FROM SETUP_KEC C WHERE C.NO_PROP = 32 AND NO_KAB =73 AND C.NO_KEC = A.NO_KEC) AS CAPIL_5 ";
    	 	$is_where = " AND NOT EXISTS(SELECT 1 FROM SIAK_REQ_CETAK_KTP@YZDB B WHERE A.NIK = B.NIK AND TO_CHAR(B.REQ_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY')) AND NOT EXISTS(SELECT 1 FROM SIAK_REQ_CETAK_KTP@YZDB B WHERE A.NIK = B.NIK AND TO_CHAR(B.REQ_DATE,'DD/MM/YYYY') <> TO_CHAR(SYSDATE-1,'DD/MM/YYYY'))";
			$is_today = 2;
			$dt = new DateTime();
			$dt->modify('-1 day');
			$renewalDate  = $dt->format('d-m-Y');
			$r = $this->get_detail_request($tbl_name,$date_col,$is_today,$get_col,$is_where);
			$data = array(
				"function_id"=>'gdsjnry',
		 		"status_tmp"=>3,
		 		"stitle"=>'Detail Cetak Non Request',
		 		"stabletitle"=>'Detail Jumlah Cetak Non Request Tanggal '.$renewalDate,
		 		"data"=>$r,
		 		"back_title"=>'Dashboard Ktp-El',
		 		"backurl"=>'Dashboard/Ktpel',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"col1"=>"Tanggal Cetak",
		 		"col2"=>"NIK",
		 		"col3"=>"No KK",
		 		"col4"=>"Nama Lengkap",
		 		"col5"=>"Nama Kecamatan"
    		);
			return view('temporary/main',$data);
		}else{
			return redirect()->route('logout');
		}

	}


    public function gdsjdrt(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {

		    $menu_id = 5;
			$is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$tbl_name = "T7_HIST_SUKET";
			$date_col = "PRINTED_DATE";
			$get_col = " TO_CHAR(A.PRINTED_DATE,'DD-MM-YYYY HH24:MI') AS CAPIL_1
						, A.NIK AS CAPIL_2
    	 				, A.NO_KK AS CAPIL_3
    	 				, A.NAMA_LGKP AS CAPIL_4
    	 				, (SELECT NAMA_KEC FROM SETUP_KEC C WHERE C.NO_PROP = 32 AND NO_KAB =73 AND C.NO_KEC = A.NO_KEC) AS CAPIL_5 ";
    	 	$is_where = " AND NOT EXISTS(SELECT 1 FROM SIAK_REQ_CETAK_KTP@YZDB B WHERE A.NIK = B.NIK AND TO_CHAR(B.REQ_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY')) AND EXISTS(SELECT 1 FROM SIAK_REQ_CETAK_KTP@YZDB B WHERE A.NIK = B.NIK AND TO_CHAR(B.REQ_DATE,'DD/MM/YYYY') <> TO_CHAR(SYSDATE,'DD/MM/YYYY'))";
			$is_today = 1;
			$r = $this->get_detail_request($tbl_name,$date_col,$is_today,$get_col,$is_where);
			$data = array(
				"function_id"=>'gdsjdrt',
		 		"status_tmp"=>3,
		 		"stitle"=>'Detail Cetak Non Request',
		 		"stabletitle"=>'Detail Jumlah Cetak Non Request Tanggal '.strval(date("d-m-Y")).' Jam '.strval(date("H:i:s")),
		 		"data"=>$r,
		 		"back_title"=>'Dashboard Ktp-El',
		 		"backurl"=>'Dashboard/Ktpel',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"col1"=>"Tanggal Cetak",
		 		"col2"=>"NIK",
		 		"col3"=>"No KK",
		 		"col4"=>"Nama Lengkap",
		 		"col5"=>"Nama Kecamatan"
    		);
			return view('temporary/main',$data);
		}else{
			return redirect()->route('logout');
		}

	}
	public function gdsjdry(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {

		    $menu_id = 5;
			$is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$tbl_name = "T7_HIST_SUKET";
			$date_col = "PRINTED_DATE";
			$get_col = " TO_CHAR(A.PRINTED_DATE,'DD-MM-YYYY HH24:MI') AS CAPIL_1
						, A.NIK AS CAPIL_2
    	 				, A.NO_KK AS CAPIL_3
    	 				, A.NAMA_LGKP AS CAPIL_4
    	 				, (SELECT NAMA_KEC FROM SETUP_KEC C WHERE C.NO_PROP = 32 AND NO_KAB =73 AND C.NO_KEC = A.NO_KEC) AS CAPIL_5 ";
    	 	$is_where = " AND NOT EXISTS(SELECT 1 FROM SIAK_REQ_CETAK_KTP@YZDB B WHERE A.NIK = B.NIK AND TO_CHAR(B.REQ_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY')) AND EXISTS(SELECT 1 FROM SIAK_REQ_CETAK_KTP@YZDB B WHERE A.NIK = B.NIK AND TO_CHAR(B.REQ_DATE,'DD/MM/YYYY') <> TO_CHAR(SYSDATE,'DD/MM/YYYY'))";
			$is_today = 2;
			$dt = new DateTime();
			$dt->modify('-1 day');
			$renewalDate  = $dt->format('d-m-Y');
			$r = $this->get_detail_request($tbl_name,$date_col,$is_today,$get_col,$is_where);
			$data = array(
				"function_id"=>'gdsjdry',
		 		"status_tmp"=>3,
		 		"stitle"=>'Detail Cetak Non Request',
		 		"stabletitle"=>'Detail Jumlah Cetak Non Request Tanggal '.$renewalDate,
		 		"data"=>$r,
		 		"back_title"=>'Dashboard Ktp-El',
		 		"backurl"=>'Dashboard/Ktpel',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"col1"=>"Tanggal Cetak",
		 		"col2"=>"NIK",
		 		"col3"=>"No KK",
		 		"col4"=>"Nama Lengkap",
		 		"col5"=>"Nama Kecamatan"
    		);
			return view('temporary/main',$data);
		}else{
			return redirect()->route('logout');
		}

	}


    public function gdsjct(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {

		    $menu_id = 5;
			$is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$tbl_name = "SIAK_REQ_CETAK_KTP@YZDB";
			$date_col = "REQ_DATE";
			$get_col = " TO_CHAR(A.REQ_DATE,'DD-MM-YYYY HH24:MI') AS CAPIL_1
						, A.NIK AS CAPIL_2
    	 				, A.NO_KK AS CAPIL_3
    	 				, A.NAMA_LENGKAP AS CAPIL_4
    	 				, A.REQ_BY AS CAPIL_5 ";
    	 	$is_where = 'AND EXISTS(SELECT 1 FROM T7_HIST_SUKET B WHERE A.NIK = B.NIK AND (B.PRINTED_DATE > SYSDATE-7 AND B.PRINTED_DATE > SYSDATE-7))';
			$is_today = 1;
			$r = $this->get_detail_request($tbl_name,$date_col,$is_today,$get_col,$is_where);
			$data = array(
				"function_id"=>'gdsjct',
		 		"status_tmp"=>3,
		 		"stitle"=>'Detail Suket',
		 		"stabletitle"=>'Detail Jumlah Request Sudah Tercetak Tanggal '.strval(date("d-m-Y")).' Jam '.strval(date("H:i:s")),
		 		"data"=>$r,
		 		"back_title"=>'Dashboard Ktp-El',
		 		"backurl"=>'Dashboard/Ktpel',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"col1"=>"Tanggal Request",
		 		"col2"=>"NIK",
		 		"col3"=>"No KK",
		 		"col4"=>"Nama Lengkap",
		 		"col5"=>"User Request"
    		);
			return view('temporary/main',$data);
		}else{
			return redirect()->route('logout');
		}

	}
	public function gdsjcy(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {

		    $menu_id = 5;
			$is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$tbl_name = "SIAK_REQ_CETAK_KTP@YZDB";
			$date_col = "REQ_DATE";
			$get_col = " TO_CHAR(A.REQ_DATE,'DD-MM-YYYY HH24:MI') AS CAPIL_1
						, A.NIK AS CAPIL_2
    	 				, CASE WHEN A.STEP_PROC = 'C' THEN 'STATUS CETAK SUKET BELUM PRINT' WHEN A.STEP_PROC = 'T' THEN 'STATUS DITOLAK' ELSE 'MASIH TAHAP PENGAJUAN' END  AS CAPIL_3
    	 				, A.NAMA_LENGKAP AS CAPIL_4
    	 				, A.REQ_BY AS CAPIL_5 ";
			$is_where = " AND NOT EXISTS(SELECT 1 FROM T7_HIST_SUKET B WHERE A.NIK = B.NIK AND TO_CHAR(B.PRINTED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY'))";
			$is_today = 2;
			$dt = new DateTime();
			$dt->modify('-1 day');
			$renewalDate  = $dt->format('d-m-Y');
			$r = $this->get_detail_request($tbl_name,$date_col,$is_today,$get_col,$is_where);
			$data = array(
				"function_id"=>'gdsjcy',
		 		"status_tmp"=>3,
		 		"stitle"=>'Detail Suket',
		 		"stabletitle"=>'Detail Jumlah Request Sudah Tercetak Tanggal '.$renewalDate,
		 		"data"=>$r,
		 		"back_title"=>'Dashboard Ktp-El',
		 		"backurl"=>'Dashboard/Ktpel',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"col1"=>"Tanggal Request",
		 		"col2"=>"NIK",
		 		"col3"=>"Status Request",
		 		"col4"=>"Nama Lengkap",
		 		"col5"=>"User Request"
    		);
			return view('temporary/main',$data);
		}else{
			return redirect()->route('logout');
		}

	}


    public function gdsjbt(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {

		    $menu_id = 5;
			$is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$tbl_name = "SIAK_REQ_CETAK_KTP@YZDB";
			$date_col = "REQ_DATE";
			$get_col = " TO_CHAR(A.REQ_DATE,'DD-MM-YYYY HH24:MI') AS CAPIL_1
						, A.NIK AS CAPIL_2
    	 				, CASE WHEN A.STEP_PROC = 'C' THEN 'STATUS CETAK SUKET BELUM PRINT' WHEN A.STEP_PROC = 'T' THEN 'STATUS DITOLAK' ELSE 'MASIH TAHAP PENGAJUAN' END  AS CAPIL_3
    	 				, A.NAMA_LENGKAP AS CAPIL_4
    	 				, A.REQ_BY AS CAPIL_5 ";
    	 	$is_where = " AND NOT EXISTS(SELECT 1 FROM T7_HIST_SUKET B WHERE A.NIK = B.NIK AND TO_CHAR(B.PRINTED_DATE,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY'))";
			$is_today = 1;
			$r = $this->get_detail_request($tbl_name,$date_col,$is_today,$get_col,$is_where);
			$data = array(
				"function_id"=>'gdsjbt',
		 		"status_tmp"=>3,
		 		"stitle"=>'Detail Suket',
		 		"stabletitle"=>'Detail Jumlah Request Belum Tercetak Tanggal '.strval(date("d-m-Y")).' Jam '.strval(date("H:i:s")),
		 		"data"=>$r,
		 		"back_title"=>'Dashboard Ktp-El',
		 		"backurl"=>'Dashboard/Ktpel',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"col1"=>"Tanggal Request",
		 		"col2"=>"NIK",
		 		"col3"=>"Status Request",
		 		"col4"=>"Nama Lengkap",
		 		"col5"=>"User Request"
    		);
			return view('temporary/main',$data);
		}else{
			return redirect()->route('logout');
		}

	}
	public function gdsjby(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {

		    $menu_id = 5;
			$is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$tbl_name = "SIAK_REQ_CETAK_KTP@YZDB";
			$date_col = "REQ_DATE";
			$get_col = " TO_CHAR(A.REQ_DATE,'DD-MM-YYYY HH24:MI') AS CAPIL_1
						, A.NIK AS CAPIL_2
    	 				, A.NO_KK AS CAPIL_3
    	 				, A.NAMA_LENGKAP AS CAPIL_4
    	 				, A.REQ_BY AS CAPIL_5 ";
			$is_where = 'AND NOT EXISTS(SELECT 1 FROM T7_HIST_SUKET B WHERE A.NIK = B.NIK AND (B.PRINTED_DATE > SYSDATE-7 AND B.PRINTED_DATE > SYSDATE-7))';
			$is_today = 2;
			$dt = new DateTime();
			$dt->modify('-1 day');
			$renewalDate  = $dt->format('d-m-Y');
			$r = $this->get_detail_request($tbl_name,$date_col,$is_today,$get_col,$is_where);
			$data = array(
				"function_id"=>'gdsjby',
		 		"status_tmp"=>3,
		 		"stitle"=>'Detail Suket',
		 		"stabletitle"=>'Detail Jumlah Request Belum Tercetak Tanggal '.$renewalDate,
		 		"data"=>$r,
		 		"back_title"=>'Dashboard Ktp-El',
		 		"backurl"=>'Dashboard/Ktpel',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"col1"=>"Tanggal Request",
		 		"col2"=>"NIK",
		 		"col3"=>"No KK",
		 		"col4"=>"Nama Lengkap",
		 		"col5"=>"User Request"
    		);
			return view('temporary/main',$data);
		}else{
			return redirect()->route('logout');
		}

	}


    public function gdrsbiot(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {

		    $menu_id = 5;
			$is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$tbl_name = "DEMOGRAPHICS";
			$date_col = "CREATED";
			$get_col = " CONCAT('''',A.NIK) AS CAPIL_1
						, A.NAMA_LGKP AS CAPIL_2
    	 				, A.NAMA_KEC AS CAPIL_3
    	 				, TO_CHAR(A.CREATED,'DD-MM-YYYY') AS CAPIL_4
    	 				, A.CURRENT_STATUS_CODE AS CAPIL_5 ";
    	 	$is_where = " AND EXISTS(SELECT 1 FROM BIODATA_WNI@DB222 B WHERE A.NIK =B.NIK AND B.FLAG_STATUS = 0) AND A.CURRENT_STATUS_CODE = 'BIO_CAPTURED'";
			$is_today = 1;
			$r = $this->get_detail_status_rekam($tbl_name,$date_col,$is_today,$get_col,$is_where);
			$data = array(
				"function_id"=>'gdrsbiot',
		 		"status_tmp"=>3,
		 		"stitle"=>'Detail Perekaman',
		 		"stabletitle"=>'Detail Status Perekaman Bio Captured Tanggal '.strval(date("d-m-Y")).' Jam '.strval(date("H:i:s")),
		 		"data"=>$r,
		 		"back_title"=>'Dashboard Ktp-El',
		 		"backurl"=>'Dashboard/Ktpel',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"col1"=>"Nik",
		 		"col2"=>"Nama Lengkap",
		 		"col3"=>"Nama Kecamatan",
		 		"col4"=>"Tanggal Rekam",
		 		"col5"=>"Status Perekaman"
    		);
			return view('temporary/main',$data);
		}else{
			return redirect()->route('logout');
		}

	}
	public function gdrsbioy(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {

		    $menu_id = 5;
			$is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$tbl_name = "DEMOGRAPHICS";
			$date_col = "CREATED";
			$get_col = " CONCAT('''',A.NIK) AS CAPIL_1
						, A.NAMA_LGKP AS CAPIL_2
    	 				, A.NAMA_KEC AS CAPIL_3
    	 				, TO_CHAR(A.CREATED,'DD-MM-YYYY') AS CAPIL_4
    	 				, A.CURRENT_STATUS_CODE AS CAPIL_5 ";
    	 	$is_where = " AND EXISTS(SELECT 1 FROM BIODATA_WNI@DB222 B WHERE A.NIK =B.NIK AND B.FLAG_STATUS = 0) AND A.CURRENT_STATUS_CODE = 'BIO_CAPTURED'";
			$is_today = 2;
			$dt = new DateTime();
			$dt->modify('-1 day');
			$renewalDate  = $dt->format('d-m-Y');
			$r = $this->get_detail_status_rekam($tbl_name,$date_col,$is_today,$get_col,$is_where);
			$data = array(
				"function_id"=>'gdrsbioy',
		 		"status_tmp"=>3,
		 		"stitle"=>'Detail Perekaman',
		 		"stabletitle"=>'Detail Status Perekaman Bio Captured Tanggal '.$renewalDate,
		 		"data"=>$r,
		 		"back_title"=>'Dashboard Ktp-El',
		 		"backurl"=>'Dashboard/Ktpel',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"col1"=>"Nik",
		 		"col2"=>"Nama Lengkap",
		 		"col3"=>"Nama Kecamatan",
		 		"col4"=>"Tanggal Rekam",
		 		"col5"=>"Status Perekaman"
    		);
			return view('temporary/main',$data);
		}else{
			return redirect()->route('logout');
		}

	}


    public function gdrsproct(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {

		    $menu_id = 5;
			$is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$tbl_name = "DEMOGRAPHICS";
			$date_col = "CREATED";
			$get_col = " CONCAT('''',A.NIK) AS CAPIL_1
						, A.NAMA_LGKP AS CAPIL_2
    	 				, A.NAMA_KEC AS CAPIL_3
    	 				, TO_CHAR(A.CREATED,'DD-MM-YYYY') AS CAPIL_4
    	 				, A.CURRENT_STATUS_CODE AS CAPIL_5 ";
    	 	$is_where = " AND EXISTS(SELECT 1 FROM BIODATA_WNI@DB222 B WHERE A.NIK =B.NIK AND B.FLAG_STATUS = 0) AND A.CURRENT_STATUS_CODE = 'PROCESSING'";
			$is_today = 1;
			$r = $this->get_detail_status_rekam($tbl_name,$date_col,$is_today,$get_col,$is_where);
			$data = array(
				"function_id"=>'gdrsproct',
		 		"status_tmp"=>3,
		 		"stitle"=>'Detail Perekaman',
		 		"stabletitle"=>'Detail Status Perekaman Processing Tanggal '.strval(date("d-m-Y")).' Jam '.strval(date("H:i:s")),
		 		"data"=>$r,
		 		"back_title"=>'Dashboard Ktp-El',
		 		"backurl"=>'Dashboard/Ktpel',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"col1"=>"Nik",
		 		"col2"=>"Nama Lengkap",
		 		"col3"=>"Nama Kecamatan",
		 		"col4"=>"Tanggal Rekam",
		 		"col5"=>"Status Perekaman"
    		);
			return view('temporary/main',$data);
		}else{
			return redirect()->route('logout');
		}

	}
	public function gdrsprocy(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {

		    $menu_id = 5;
			$is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$tbl_name = "DEMOGRAPHICS";
			$date_col = "CREATED";
			$get_col = " CONCAT('''',A.NIK) AS CAPIL_1
						, A.NAMA_LGKP AS CAPIL_2
    	 				, A.NAMA_KEC AS CAPIL_3
    	 				, TO_CHAR(A.CREATED,'DD-MM-YYYY') AS CAPIL_4
    	 				, A.CURRENT_STATUS_CODE AS CAPIL_5 ";
    	 	$is_where = " AND EXISTS(SELECT 1 FROM BIODATA_WNI@DB222 B WHERE A.NIK =B.NIK AND B.FLAG_STATUS = 0) AND A.CURRENT_STATUS_CODE = 'PROCESSING'";
			$is_today = 2;
			$dt = new DateTime();
			$dt->modify('-1 day');
			$renewalDate  = $dt->format('d-m-Y');
			$r = $this->get_detail_status_rekam($tbl_name,$date_col,$is_today,$get_col,$is_where);
			$data = array(
				"function_id"=>'gdrsprocy',
		 		"status_tmp"=>3,
		 		"stitle"=>'Detail Perekaman',
		 		"stabletitle"=>'Detail Status Perekaman Processing Tanggal '.$renewalDate,
		 		"data"=>$r,
		 		"back_title"=>'Dashboard Ktp-El',
		 		"backurl"=>'Dashboard/Ktpel',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"col1"=>"Nik",
		 		"col2"=>"Nama Lengkap",
		 		"col3"=>"Nama Kecamatan",
		 		"col4"=>"Tanggal Rekam",
		 		"col5"=>"Status Perekaman"
    		);
			return view('temporary/main',$data);
		}else{
			return redirect()->route('logout');
		}

	}


    public function gdrsadjt(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {

		    $menu_id = 5;
			$is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$tbl_name = "DEMOGRAPHICS";
			$date_col = "CREATED";
			$get_col = " CONCAT('''',A.NIK) AS CAPIL_1
						, A.NAMA_LGKP AS CAPIL_2
    	 				, A.NAMA_KEC AS CAPIL_3
    	 				, TO_CHAR(A.CREATED,'DD-MM-YYYY') AS CAPIL_4
    	 				, A.CURRENT_STATUS_CODE AS CAPIL_5 ";
    	 	$is_where = " AND EXISTS(SELECT 1 FROM BIODATA_WNI@DB222 B WHERE A.NIK =B.NIK AND B.FLAG_STATUS = 0) AND A.CURRENT_STATUS_CODE = 'ADJUDICATE_RECORD'";
			$is_today = 1;
			$r = $this->get_detail_status_rekam($tbl_name,$date_col,$is_today,$get_col,$is_where);
			$data = array(
				"function_id"=>'gdrsadjt',
		 		"status_tmp"=>3,
		 		"stitle"=>'Detail Perekaman',
		 		"stabletitle"=>'Detail Status Perekaman Adjudicate Record Tanggal '.strval(date("d-m-Y")).' Jam '.strval(date("H:i:s")),
		 		"data"=>$r,
		 		"back_title"=>'Dashboard Ktp-El',
		 		"backurl"=>'Dashboard/Ktpel',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"col1"=>"Nik",
		 		"col2"=>"Nama Lengkap",
		 		"col3"=>"Nama Kecamatan",
		 		"col4"=>"Tanggal Rekam",
		 		"col5"=>"Status Perekaman"
    		);
			return view('temporary/main',$data);
		}else{
			return redirect()->route('logout');
		}

	}
	public function gdrsadjy(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {

		    $menu_id = 5;
			$is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$tbl_name = "DEMOGRAPHICS";
			$date_col = "CREATED";
			$get_col = " CONCAT('''',A.NIK) AS CAPIL_1
						, A.NAMA_LGKP AS CAPIL_2
    	 				, A.NAMA_KEC AS CAPIL_3
    	 				, TO_CHAR(A.CREATED,'DD-MM-YYYY') AS CAPIL_4
    	 				, A.CURRENT_STATUS_CODE AS CAPIL_5 ";
    	 	$is_where = " AND EXISTS(SELECT 1 FROM BIODATA_WNI@DB222 B WHERE A.NIK =B.NIK AND B.FLAG_STATUS = 0) AND A.CURRENT_STATUS_CODE = 'ADJUDICATE_RECORD'";
			$is_today = 2;
			$dt = new DateTime();
			$dt->modify('-1 day');
			$renewalDate  = $dt->format('d-m-Y');
			$r = $this->get_detail_status_rekam($tbl_name,$date_col,$is_today,$get_col,$is_where);
			$data = array(
				"function_id"=>'gdrsadjy',
		 		"status_tmp"=>3,
		 		"stitle"=>'Detail Perekaman',
		 		"stabletitle"=>'Detail Status Perekaman Adjudicate Record Tanggal '.$renewalDate,
		 		"data"=>$r,
		 		"back_title"=>'Dashboard Ktp-El',
		 		"backurl"=>'Dashboard/Ktpel',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"col1"=>"Nik",
		 		"col2"=>"Nama Lengkap",
		 		"col3"=>"Nama Kecamatan",
		 		"col4"=>"Tanggal Rekam",
		 		"col5"=>"Status Perekaman"
    		);
			return view('temporary/main',$data);
		}else{
			return redirect()->route('logout');
		}

	}


    public function gdrsefact(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {

		    $menu_id = 5;
			$is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$tbl_name = "DEMOGRAPHICS";
			$date_col = "CREATED";
			$get_col = " CONCAT('''',A.NIK) AS CAPIL_1
						, A.NAMA_LGKP AS CAPIL_2
    	 				, A.NAMA_KEC AS CAPIL_3
    	 				, TO_CHAR(A.CREATED,'DD-MM-YYYY') AS CAPIL_4
    	 				, A.CURRENT_STATUS_CODE AS CAPIL_5 ";
    	 	$is_where = " AND EXISTS(SELECT 1 FROM BIODATA_WNI@DB222 B WHERE A.NIK =B.NIK AND B.FLAG_STATUS = 0) AND A.CURRENT_STATUS_CODE = 'ENROLL_FAILURE_AT_CENTRAL'";
			$is_today = 1;
			$r = $this->get_detail_status_rekam($tbl_name,$date_col,$is_today,$get_col,$is_where);
			$data = array(
				"function_id"=>'gdrsefact',
		 		"status_tmp"=>3,
		 		"stitle"=>'Detail Perekaman',
		 		"stabletitle"=>'Detail Status Perekaman Enroll Failure At Central Tanggal '.strval(date("d-m-Y")).' Jam '.strval(date("H:i:s")),
		 		"data"=>$r,
		 		"back_title"=>'Dashboard Ktp-El',
		 		"backurl"=>'Dashboard/Ktpel',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"col1"=>"Nik",
		 		"col2"=>"Nama Lengkap",
		 		"col3"=>"Nama Kecamatan",
		 		"col4"=>"Tanggal Rekam",
		 		"col5"=>"Status Perekaman"
    		);
			return view('temporary/main',$data);
		}else{
			return redirect()->route('logout');
		}

	}
	public function gdrsefacy(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {

		    $menu_id = 5;
			$is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$tbl_name = "DEMOGRAPHICS";
			$date_col = "CREATED";
			$get_col = " CONCAT('''',A.NIK) AS CAPIL_1
						, A.NAMA_LGKP AS CAPIL_2
    	 				, A.NAMA_KEC AS CAPIL_3
    	 				, TO_CHAR(A.CREATED,'DD-MM-YYYY') AS CAPIL_4
    	 				, A.CURRENT_STATUS_CODE AS CAPIL_5 ";
    	 	$is_where = " AND EXISTS(SELECT 1 FROM BIODATA_WNI@DB222 B WHERE A.NIK =B.NIK AND B.FLAG_STATUS = 0) AND A.CURRENT_STATUS_CODE = 'ENROLL_FAILURE_AT_CENTRAL'";
			$is_today = 2;
			$dt = new DateTime();
			$dt->modify('-1 day');
			$renewalDate  = $dt->format('d-m-Y');
			$r = $this->get_detail_status_rekam($tbl_name,$date_col,$is_today,$get_col,$is_where);
			$data = array(
				"function_id"=>'gdrsefacy',
		 		"status_tmp"=>3,
		 		"stitle"=>'Detail Perekaman',
		 		"stabletitle"=>'Detail Status Perekaman Enroll Failure At Central Tanggal '.$renewalDate,
		 		"data"=>$r,
		 		"back_title"=>'Dashboard Ktp-El',
		 		"backurl"=>'Dashboard/Ktpel',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"col1"=>"Nik",
		 		"col2"=>"Nama Lengkap",
		 		"col3"=>"Nama Kecamatan",
		 		"col4"=>"Tanggal Rekam",
		 		"col5"=>"Status Perekaman"
    		);
			return view('temporary/main',$data);
		}else{
			return redirect()->route('logout');
		}

	}


    public function gdrssfet(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {

		    $menu_id = 5;
			$is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$tbl_name = "DEMOGRAPHICS";
			$date_col = "CREATED";
			$get_col = " CONCAT('''',A.NIK) AS CAPIL_1
						, A.NAMA_LGKP AS CAPIL_2
    	 				, A.NAMA_KEC AS CAPIL_3
    	 				, TO_CHAR(A.CREATED,'DD-MM-YYYY') AS CAPIL_4
    	 				, A.CURRENT_STATUS_CODE AS CAPIL_5 ";
    	 	$is_where = " AND EXISTS(SELECT 1 FROM BIODATA_WNI@DB222 B WHERE A.NIK =B.NIK AND B.FLAG_STATUS = 0) AND A.CURRENT_STATUS_CODE = 'SENT_FOR_ENROLLMENT'";
			$is_today = 1;
			$r = $this->get_detail_status_rekam($tbl_name,$date_col,$is_today,$get_col,$is_where);
			$data = array(
				"function_id"=>'gdrssfet',
		 		"status_tmp"=>3,
		 		"stitle"=>'Detail Perekaman',
		 		"stabletitle"=>'Detail Status Perekaman Sent For Enrollment Tanggal '.strval(date("d-m-Y")).' Jam '.strval(date("H:i:s")),
		 		"data"=>$r,
		 		"back_title"=>'Dashboard Ktp-El',
		 		"backurl"=>'Dashboard/Ktpel',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"col1"=>"Nik",
		 		"col2"=>"Nama Lengkap",
		 		"col3"=>"Nama Kecamatan",
		 		"col4"=>"Tanggal Rekam",
		 		"col5"=>"Status Perekaman"
    		);
			return view('temporary/main',$data);
		}else{
			return redirect()->route('logout');
		}

	}
	public function gdrssfey(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {

		    $menu_id = 5;
			$is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$tbl_name = "DEMOGRAPHICS";
			$date_col = "CREATED";
			$get_col = " CONCAT('''',A.NIK) AS CAPIL_1
						, A.NAMA_LGKP AS CAPIL_2
    	 				, A.NAMA_KEC AS CAPIL_3
    	 				, TO_CHAR(A.CREATED,'DD-MM-YYYY') AS CAPIL_4
    	 				, A.CURRENT_STATUS_CODE AS CAPIL_5 ";
    	 	$is_where = " AND EXISTS(SELECT 1 FROM BIODATA_WNI@DB222 B WHERE A.NIK =B.NIK AND B.FLAG_STATUS = 0) AND A.CURRENT_STATUS_CODE = 'SENT_FOR_ENROLLMENT'";
			$is_today = 2;
			$dt = new DateTime();
			$dt->modify('-1 day');
			$renewalDate  = $dt->format('d-m-Y');
			$r = $this->get_detail_status_rekam($tbl_name,$date_col,$is_today,$get_col,$is_where);
			$data = array(
				"function_id"=>'gdrssfey',
		 		"status_tmp"=>3,
		 		"stitle"=>'Detail Perekaman',
		 		"stabletitle"=>'Detail Status Perekaman Sent For Enrollment Tanggal '.$renewalDate,
		 		"data"=>$r,
		 		"back_title"=>'Dashboard Ktp-El',
		 		"backurl"=>'Dashboard/Ktpel',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"col1"=>"Nik",
		 		"col2"=>"Nama Lengkap",
		 		"col3"=>"Nama Kecamatan",
		 		"col4"=>"Tanggal Rekam",
		 		"col5"=>"Status Perekaman"
    		);
			return view('temporary/main',$data);
		}else{
			return redirect()->route('logout');
		}

	}


    public function gdrsprrt(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {

		    $menu_id = 5;
			$is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$tbl_name = "DEMOGRAPHICS";
			$date_col = "CREATED";
			$get_col = " CONCAT('''',A.NIK) AS CAPIL_1
						, A.NAMA_LGKP AS CAPIL_2
    	 				, A.NAMA_KEC AS CAPIL_3
    	 				, TO_CHAR(A.CREATED,'DD-MM-YYYY') AS CAPIL_4
    	 				, A.CURRENT_STATUS_CODE AS CAPIL_5 ";
    	 	$is_where = " AND EXISTS(SELECT 1 FROM BIODATA_WNI@DB222 B WHERE A.NIK =B.NIK AND B.FLAG_STATUS = 0) AND A.CURRENT_STATUS_CODE = 'PRINT_READY_RECORD'";
			$is_today = 1;
			$r = $this->get_detail_status_rekam($tbl_name,$date_col,$is_today,$get_col,$is_where);
			$data = array(
				"function_id"=>'gdrsprrt',
		 		"status_tmp"=>3,
		 		"stitle"=>'Detail Perekaman',
		 		"stabletitle"=>'Detail Status Perekaman Print Ready Record Tanggal '.strval(date("d-m-Y")).' Jam '.strval(date("H:i:s")),
		 		"data"=>$r,
		 		"back_title"=>'Dashboard Ktp-El',
		 		"backurl"=>'Dashboard/Ktpel',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"col1"=>"Nik",
		 		"col2"=>"Nama Lengkap",
		 		"col3"=>"Nama Kecamatan",
		 		"col4"=>"Tanggal Rekam",
		 		"col5"=>"Status Perekaman"
    		);
			return view('temporary/main',$data);
		}else{
			return redirect()->route('logout');
		}

	}
	public function gdrsprry(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {

		    $menu_id = 5;
			$is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$tbl_name = "DEMOGRAPHICS";
			$date_col = "CREATED";
			$get_col = " CONCAT('''',A.NIK) AS CAPIL_1
						, A.NAMA_LGKP AS CAPIL_2
    	 				, A.NAMA_KEC AS CAPIL_3
    	 				, TO_CHAR(A.CREATED,'DD-MM-YYYY') AS CAPIL_4
    	 				, A.CURRENT_STATUS_CODE AS CAPIL_5 ";
    	 	$is_where = " AND EXISTS(SELECT 1 FROM BIODATA_WNI@DB222 B WHERE A.NIK =B.NIK AND B.FLAG_STATUS = 0) AND A.CURRENT_STATUS_CODE = 'PRINT_READY_RECORD'";
			$is_today = 2;
			$dt = new DateTime();
			$dt->modify('-1 day');
			$renewalDate  = $dt->format('d-m-Y');
			$r = $this->get_detail_status_rekam($tbl_name,$date_col,$is_today,$get_col,$is_where);
			$data = array(
				"function_id"=>'gdrsprry',
		 		"status_tmp"=>3,
		 		"stitle"=>'Detail Perekaman',
		 		"stabletitle"=>'Detail Status Perekaman Print Ready Record Tanggal '.$renewalDate,
		 		"data"=>$r,
		 		"back_title"=>'Dashboard Ktp-El',
		 		"backurl"=>'Dashboard/Ktpel',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"col1"=>"Nik",
		 		"col2"=>"Nama Lengkap",
		 		"col3"=>"Nama Kecamatan",
		 		"col4"=>"Tanggal Rekam",
		 		"col5"=>"Status Perekaman"
    		);
			return view('temporary/main',$data);
		}else{
			return redirect()->route('logout');
		}

	}


    public function gdrsdurect(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {

		    $menu_id = 5;
			$is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$tbl_name = "DEMOGRAPHICS";
			$date_col = "CREATED";
			$get_col = " CONCAT('''',A.NIK) AS CAPIL_1
						, A.NAMA_LGKP AS CAPIL_2
    	 				, A.NAMA_KEC AS CAPIL_3
    	 				, TO_CHAR(A.CREATED,'DD-MM-YYYY') AS CAPIL_4
    	 				, A.CURRENT_STATUS_CODE AS CAPIL_5 ";
    	 	$is_where = " AND EXISTS(SELECT 1 FROM BIODATA_WNI@DB222 B WHERE A.NIK =B.NIK AND B.FLAG_STATUS = 0) AND A.CURRENT_STATUS_CODE = 'DUPLICATE_RECORD'";
			$is_today = 1;
			$r = $this->get_detail_status_rekam($tbl_name,$date_col,$is_today,$get_col,$is_where);
			$data = array(
				"function_id"=>'gdrsdurect',
		 		"status_tmp"=>3,
		 		"stitle"=>'Detail Perekaman',
		 		"stabletitle"=>'Detail Status Perekaman Duplicate Record Tanggal '.strval(date("d-m-Y")).' Jam '.strval(date("H:i:s")),
		 		"data"=>$r,
		 		"back_title"=>'Dashboard Ktp-El',
		 		"backurl"=>'Dashboard/Ktpel',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"col1"=>"Nik",
		 		"col2"=>"Nama Lengkap",
		 		"col3"=>"Nama Kecamatan",
		 		"col4"=>"Tanggal Rekam",
		 		"col5"=>"Status Perekaman"
    		);
			return view('temporary/main',$data);
		}else{
			return redirect()->route('logout');
		}

	}
	public function gdrsdurecy(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {

		    $menu_id = 5;
			$is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$tbl_name = "DEMOGRAPHICS";
			$date_col = "CREATED";
			$get_col = " CONCAT('''',A.NIK) AS CAPIL_1
						, A.NAMA_LGKP AS CAPIL_2
    	 				, A.NAMA_KEC AS CAPIL_3
    	 				, TO_CHAR(A.CREATED,'DD-MM-YYYY') AS CAPIL_4
    	 				, A.CURRENT_STATUS_CODE AS CAPIL_5 ";
    	 	$is_where = " AND EXISTS(SELECT 1 FROM BIODATA_WNI@DB222 B WHERE A.NIK =B.NIK AND B.FLAG_STATUS = 0) AND A.CURRENT_STATUS_CODE = 'DUPLICATE_RECORD'";
			$is_today = 2;
			$dt = new DateTime();
			$dt->modify('-1 day');
			$renewalDate  = $dt->format('d-m-Y');
			$r = $this->get_detail_status_rekam($tbl_name,$date_col,$is_today,$get_col,$is_where);
			$data = array(
				"function_id"=>'gdrsdurecy',
		 		"status_tmp"=>3,
		 		"stitle"=>'Detail Perekaman',
		 		"stabletitle"=>'Detail Status Perekaman Duplicate Record Tanggal '.$renewalDate,
		 		"data"=>$r,
		 		"back_title"=>'Dashboard Ktp-El',
		 		"backurl"=>'Dashboard/Ktpel',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"col1"=>"Nik",
		 		"col2"=>"Nama Lengkap",
		 		"col3"=>"Nama Kecamatan",
		 		"col4"=>"Tanggal Rekam",
		 		"col5"=>"Status Perekaman"
    		);
			return view('temporary/main',$data);
		}else{
			return redirect()->route('logout');
		}

	}


    public function gdrscapt(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {

		    $menu_id = 5;
			$is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$tbl_name = "DEMOGRAPHICS";
			$date_col = "CREATED";
			$get_col = " CONCAT('''',A.NIK) AS CAPIL_1
						, A.NAMA_LGKP AS CAPIL_2
    	 				, A.NAMA_KEC AS CAPIL_3
    	 				, TO_CHAR(A.CREATED,'DD-MM-YYYY') AS CAPIL_4
    	 				, A.CURRENT_STATUS_CODE AS CAPIL_5 ";
    	 	$is_where = " AND EXISTS(SELECT 1 FROM BIODATA_WNI@DB222 B WHERE A.NIK =B.NIK AND B.FLAG_STATUS = 0) AND A.CURRENT_STATUS_CODE <> 'SENT_FOR_ENROLLMENT' AND A.CURRENT_STATUS_CODE <> 'BIO_CAPTURED' AND A.CURRENT_STATUS_CODE <> 'DUPLICATE_RECORD' AND A.CURRENT_STATUS_CODE <> 'PRINT_READY_RECORD' AND A.CURRENT_STATUS_CODE <> 'PROCESSING' AND A.CURRENT_STATUS_CODE <> 'ADJUDICATE_RECORD' AND A.CURRENT_STATUS_CODE <> 'ENROLL_FAILURE_AT_CENTRAL' ";
			$is_today = 1;
			$r = $this->get_detail_status_rekam($tbl_name,$date_col,$is_today,$get_col,$is_where);
			$data = array(
				"function_id"=>'gdrscapt',
		 		"status_tmp"=>3,
		 		"stitle"=>'Detail Perekaman',
		 		"stabletitle"=>'Detail Status Card Printed Tanggal '.strval(date("d-m-Y")).' Jam '.strval(date("H:i:s")),
		 		"data"=>$r,
		 		"back_title"=>'Dashboard Ktp-El',
		 		"backurl"=>'Dashboard/Ktpel',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"col1"=>"Nik",
		 		"col2"=>"Nama Lengkap",
		 		"col3"=>"Nama Kecamatan",
		 		"col4"=>"Tanggal Rekam",
		 		"col5"=>"Status Perekaman"
    		);
			return view('temporary/main',$data);
		}else{
			return redirect()->route('logout');
		}

	}
	public function gdrscapy(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {

		    $menu_id = 5;
			$is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$tbl_name = "DEMOGRAPHICS";
			$date_col = "CREATED";
			$get_col = " CONCAT('''',A.NIK) AS CAPIL_1
						, A.NAMA_LGKP AS CAPIL_2
    	 				, A.NAMA_KEC AS CAPIL_3
    	 				, TO_CHAR(A.CREATED,'DD-MM-YYYY') AS CAPIL_4
    	 				, A.CURRENT_STATUS_CODE AS CAPIL_5 ";
    	 	$is_where = " AND EXISTS(SELECT 1 FROM BIODATA_WNI@DB222 B WHERE A.NIK =B.NIK AND B.FLAG_STATUS = 0) AND A.CURRENT_STATUS_CODE <> 'SENT_FOR_ENROLLMENT' AND A.CURRENT_STATUS_CODE <> 'BIO_CAPTURED' AND A.CURRENT_STATUS_CODE <> 'DUPLICATE_RECORD' AND A.CURRENT_STATUS_CODE <> 'PRINT_READY_RECORD' AND A.CURRENT_STATUS_CODE <> 'PROCESSING' AND A.CURRENT_STATUS_CODE <> 'ADJUDICATE_RECORD' AND A.CURRENT_STATUS_CODE <> 'ENROLL_FAILURE_AT_CENTRAL' ";
			$is_today = 2;
			$dt = new DateTime();
			$dt->modify('-1 day');
			$renewalDate  = $dt->format('d-m-Y');
			$r = $this->get_detail_status_rekam($tbl_name,$date_col,$is_today,$get_col,$is_where);
			$data = array(
				"function_id"=>'gdrscapy',
		 		"status_tmp"=>3,
		 		"stitle"=>'Detail Perekaman',
		 		"stabletitle"=>'Detail Status Card Printed Tanggal '.$renewalDate,
		 		"data"=>$r,
		 		"back_title"=>'Dashboard Ktp-El',
		 		"backurl"=>'Dashboard/Ktpel',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"col1"=>"Nik",
		 		"col2"=>"Nama Lengkap",
		 		"col3"=>"Nama Kecamatan",
		 		"col4"=>"Tanggal Rekam",
		 		"col5"=>"Status Perekaman"
    		);
			return view('temporary/main',$data);
		}else{
			return redirect()->route('logout');
		}

	}

    
	public function get_user_cetak(){
            $sql ="SELECT USER_ID, TGL, MULAI, SELESAI, JML FROM VW_SIAK_USER_CETAK";
            $r = DB::select($sql);
            return $r;
    }
    public function get_detail_global($tbl_name,$date_col,$pk_col,$check_exists_bio,$is_today,$is_database,$is_dblink ='',$start_date = '',$end_date = '',$is_coklit = 0)
        {
        	$sql = "";
        	$sql .= "SELECT 
                        A.NO_KEC 
                        , A.NAMA_KEC";
            if($is_today == 1){
				$sql .= " , CASE WHEN B.TANGGAL IS NULL THEN TO_CHAR(SYSDATE,'DD/MM/YYYY') ELSE B.TANGGAL END AS TANGGAL ";
			}else if($is_today == 2){
				$sql .= " , CASE WHEN B.TANGGAL IS NULL THEN TO_CHAR(SYSDATE-1,'DD/MM/YYYY') ELSE B.TANGGAL END AS TANGGAL ";
			}
			if ($is_coklit == 1){
				$sql .= " , CASE WHEN B.UMUM IS NULL THEN 0 ELSE B.UMUM END AS UMUM
                        , CASE WHEN B.COKLIT IS NULL THEN 0 ELSE B.COKLIT END AS COKLIT
                		, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH";
			}else{
				$sql .= " , CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH";
            }
            
            if ($is_database == '222'){
				$sql .= "  FROM SETUP_KEC A LEFT JOIN";
			}else{
				$sql .= "  FROM SETUP_KEC".$is_dblink." A LEFT JOIN";
			}
                 $sql .= "  
           			(SELECT 
					    X.NO_KEC
					    , X.NAMA_KEC";
			if ($is_coklit == 1){
				$sql .= " , SUM(CASE WHEN NOT EXISTS(SELECT 1 FROM A_KPU_FINAL D WHERE TO_CHAR(A.NIK) = D.NIK) THEN 1 ELSE 0 END) AS UMUM
					    , SUM(CASE WHEN EXISTS(SELECT 1 FROM A_KPU_FINAL D WHERE TO_CHAR(A.NIK) = D.NIK) THEN 1 ELSE 0 END) AS COKLIT";
			}
				$sql .= " , to_char(A.".$date_col.",'DD/MM/YYYY') AS TANGGAL
					    , COUNT(1) AS JUMLAH
					FROM 
					    ".$tbl_name." A ";
		if($check_exists_bio == 1){
				$sql .= " INNER JOIN ";
			if ($is_database == '222'){
				$sql .= " BIODATA_WNI C ";
				}else{
				$sql .= " BIODATA_WNI".$is_dblink." C ";
				}
				$sql .= " ON A.".$pk_col." = C.NIK 
					INNER JOIN ";
			if ($is_database == '222'){
				$sql .= "   SETUP_KEC X ";
			}else{
				$sql .= "   SETUP_KEC".$is_dblink." X ";
			}
				$sql .= "		    
					    ON C.NO_PROP = X.NO_PROP 
					    AND C.NO_KAB = X.NO_KAB 
					    AND C.NO_KEC = X.NO_KEC ";
			}else{
				$sql .= " INNER JOIN ";
				if ($is_database == '222'){
				$sql .= "   SETUP_KEC X ";
			}else{
				$sql .= "   SETUP_KEC".$is_dblink." X ";
			}
				$sql .= "		    
					    ON A.NO_PROP = X.NO_PROP 
					    AND A.NO_KAB = X.NO_KAB 
					    AND A.NO_KEC = X.NO_KEC ";
			}
				$sql .= "	   
					WHERE
						1=1";
			
			if($is_today == 1){
				$sql .= " AND TO_CHAR(A.".$date_col.",'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') ";
			}else if($is_today == 2){
				$sql .= " AND TO_CHAR(A.".$date_col.",'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') ";
			}
			if ($is_database == '222'){
				if($check_exists_bio == 1){
		            $sql .= " AND EXISTS(SELECT 1 FROM BIODATA_WNI B WHERE A.".$pk_col." =B.NIK) ";
		        } 
		    }else{
		    	if($check_exists_bio == 1){
		            $sql .= " AND EXISTS(SELECT 1 FROM BIODATA_WNI".$is_dblink." B WHERE A.".$pk_col." =B.NIK) ";
		        }
		    }
				$sql .= " GROUP BY 
					    X.NO_KEC
					    ,X.NAMA_KEC,to_char(A.".$date_col.",'DD/MM/YYYY')
					ORDER BY 
					    X.NO_KEC,X.NAMA_KEC) B ON 
                        A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB =73 ORDER BY A.NO_KEC";
 				
 			

             if ($is_database == '222'){
             	$r = DB::connection('db222')->select($sql);
             }else if($is_database == '221'){
             	$r = DB::connection('db221')->select($sql);
             }else if($is_database == '2'){
             	$r = DB::connection('db2')->select($sql);
             }else if($is_database == 'YZDB'){
             	$r = DB::select($sql);
             }

               return $r;
        }
         
         function get_jumlah_global($tbl_name,$date_col,$pk_col,$check_exists_bio,$is_today,$is_database,$is_dblink ='',$start_date = '',$end_date = '',$is_coklit = 0)
        {
        	$sql = "";
        	$sql .= "SELECT  COUNT(1) AS JUMLAH";
			if ($is_coklit == 1){
				$sql .= " , SUM(CASE WHEN NOT EXISTS(SELECT 1 FROM A_KPU_FINAL D WHERE TO_CHAR(A.NIK) = D.NIK) THEN 1 ELSE 0 END) AS UMUM
					      , SUM(CASE WHEN EXISTS(SELECT 1 FROM A_KPU_FINAL D WHERE TO_CHAR(A.NIK) = D.NIK) THEN 1 ELSE 0 END) AS COKLIT";
			}
				$sql .= " FROM 
					    ".$tbl_name." A ";
		if($check_exists_bio == 1){
				$sql .= " INNER JOIN ";
			if ($is_database == '222'){
				$sql .= " BIODATA_WNI C ";
				}else{
				$sql .= " BIODATA_WNI".$is_dblink." C ";
				}
				$sql .= " ON A.".$pk_col." = C.NIK ";
			}
				$sql .= "	   
					WHERE
						1=1";
			
			if($is_today == 1){
				$sql .= " AND TO_CHAR(A.".$date_col.",'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') ";
			}else if($is_today == 2){
				$sql .= " AND TO_CHAR(A.".$date_col.",'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') ";
			}
			if ($is_database == '222'){
				if($check_exists_bio == 1){
		            $sql .= " AND EXISTS(SELECT 1 FROM BIODATA_WNI B WHERE A.".$pk_col." =B.NIK) ";
		        } 
		    }else{
		    	if($check_exists_bio == 1){
		            $sql .= " AND EXISTS(SELECT 1 FROM BIODATA_WNI".$is_dblink." B WHERE A.".$pk_col." =B.NIK) ";
		        }
		    }

             if ($is_database == '222'){
             	$r = DB::connection('db222')->select($sql);
             }else if($is_database == '221'){
             	$r = DB::connection('db221')->select($sql);
             }else if($is_database == '2'){
             	$r = DB::connection('db2')->select($sql);
             }else if($is_database == 'YZDB'){
             	$r = DB::select($sql);
             }

               return $r;
        }

	public function get_detail_rek_global($tbl_name,$date_col,$pk_col,$check_exists_bio,$is_today,$is_database,$is_dblink ='',$start_date = '',$end_date = '',$is_coklit = 0)
        {
        	$sql = "";
        	$sql .= "SELECT 
                        A.NO_KEC 
                        , A.NAMA_KEC";
            if($is_today == 1){
				$sql .= " , CASE WHEN B.TANGGAL IS NULL THEN TO_CHAR(SYSDATE,'DD/MM/YYYY') ELSE B.TANGGAL END AS TANGGAL ";
			}else if($is_today == 2){
				$sql .= " , CASE WHEN B.TANGGAL IS NULL THEN TO_CHAR(SYSDATE-1,'DD/MM/YYYY') ELSE B.TANGGAL END AS TANGGAL ";
			}
			if ($is_coklit == 1){
				$sql .= " , CASE WHEN B.UMUM IS NULL THEN 0 ELSE B.UMUM END AS UMUM
                        , CASE WHEN B.COKLIT IS NULL THEN 0 ELSE B.COKLIT END AS COKLIT
                		, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH";
			}else{
				$sql .= " , CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH";
            }
            
            if ($is_database == '222'){
				$sql .= "  FROM SETUP_KEC A LEFT JOIN";
			}else{
				$sql .= "  FROM SETUP_KEC".$is_dblink." A LEFT JOIN";
			}
                 $sql .= "  
           			(SELECT 
					    X.NO_KEC
					    , X.NAMA_KEC";
			if ($is_coklit == 1){
				$sql .= " , SUM(CASE WHEN NOT EXISTS(SELECT 1 FROM A_KPU_FINAL D WHERE TO_CHAR(A.NIK) = D.NIK) THEN 1 ELSE 0 END) AS UMUM
					    , SUM(CASE WHEN EXISTS(SELECT 1 FROM A_KPU_FINAL D WHERE TO_CHAR(A.NIK) = D.NIK) THEN 1 ELSE 0 END) AS COKLIT";
			}
				$sql .= " , to_char(A.".$date_col.",'DD/MM/YYYY') AS TANGGAL
					    , COUNT(1) AS JUMLAH
					FROM 
					    ".$tbl_name." A ";
		if($check_exists_bio == 1){
				$sql .= " INNER JOIN ";
			if ($is_database == '222'){
				$sql .= " BIODATA_WNI C ";
				}else{
				$sql .= " BIODATA_WNI".$is_dblink." C ";
				}
				$sql .= " ON A.".$pk_col." = C.NIK 
					INNER JOIN ";
			if ($is_database == '222'){
				$sql .= "   SETUP_KEC X ";
			}else{
				$sql .= "   SETUP_KEC".$is_dblink." X ";
			}
				$sql .= "		    
					    ON C.NO_PROP = X.NO_PROP 
					    AND C.NO_KAB = X.NO_KAB 
					    AND C.NO_KEC = X.NO_KEC ";
			}else{
				$sql .= " INNER JOIN ";
				if ($is_database == '222'){
				$sql .= "   SETUP_KEC X ";
			}else{
				$sql .= "   SETUP_KEC".$is_dblink." X ";
			}
				$sql .= "		    
					    ON A.NO_PROP = X.NO_PROP 
					    AND A.NO_KAB = X.NO_KAB 
					    AND A.NO_KEC = X.NO_KEC ";
			}
				$sql .= "	   
					WHERE
						1=1";
			
			if($is_today == 1){
				$sql .= " AND TO_CHAR(A.".$date_col.",'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') ";
			}else if($is_today == 2){
				$sql .= " AND TO_CHAR(A.".$date_col.",'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') ";
			}
			if ($is_database == '222'){
				if($check_exists_bio == 1){
		            $sql .= " AND EXISTS(SELECT 1 FROM BIODATA_WNI B WHERE A.".$pk_col." =B.NIK) ";
		        } 
		    }else{
		    	if($check_exists_bio == 1){
		            $sql .= " AND EXISTS(SELECT 1 FROM BIODATA_WNI".$is_dblink." B WHERE A.".$pk_col." =B.NIK) ";
		        }
		    }
		     $sql .= " AND EXISTS(SELECT 1 FROM FACES B WHERE A.".$pk_col." =B.NIK) ";
				$sql .= " GROUP BY 
					    X.NO_KEC
					    ,X.NAMA_KEC,to_char(A.".$date_col.",'DD/MM/YYYY')
					ORDER BY 
					    X.NO_KEC,X.NAMA_KEC) B ON 
                        A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB =73 ORDER BY A.NO_KEC";
             if ($is_database == '222'){
             	$r = DB::connection('db222')->select($sql);
             }else if($is_database == '221'){
             	$r = DB::connection('db221')->select($sql);
             }else if($is_database == '2'){
             	$r = DB::connection('db2')->select($sql);
             }else if($is_database == 'YZDB'){
             	$r = DB::select($sql);
             }

               return $r;
        }
        public function get_detail_ctk_global($is_today,$is_database)
        {
        	$sql = "";
        	$sql .= "SELECT A.NO_KEC , A.NAMA_KEC  ";
 			if($is_today == 1){
				$sql .= " ,  TO_CHAR(SYSDATE,'DD/MM/YYYY') AS TANGGAL ";
			}else if($is_today == 2){
				$sql .= " , TO_CHAR(SYSDATE-1,'DD/MM/YYYY') AS TANGGAL ";
			}
        	$sql .= ", CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEC@DB222 A LEFT JOIN
					(select 
					    C.NO_KEC
					    , C.NAMA_KEC
					    , COUNT(B.NIK) AS JUMLAH FROM CARD_MANAGEMENT@DB5CETAK B 
					    INNER JOIN DEMOGRAPHICS A 
					    ON A.NIK =  B.NIK INNER JOIN SETUP_KEC@DB222 C
					    ON A.NO_PROP = C.NO_PROP 
					    AND A.NO_KAB = C.NO_KAB 
					    AND A.NO_KEC = C.NO_KEC 
					    WHERE ";
				if($is_today == 1){
			    $sql .= "CASE WHEN B.LAST_UPDATE IS NULL THEN B.PERSONALIZED_DATE ELSE B.LAST_UPDATE END >= TRUNC(SYSDATE) AND CASE WHEN B.LAST_UPDATE IS NULL THEN B.PERSONALIZED_DATE ELSE B.LAST_UPDATE END < TRUNC(SYSDATE)+1 ";
				}else if($is_today == 2){
				$sql .= "CASE WHEN B.LAST_UPDATE IS NULL THEN B.PERSONALIZED_DATE ELSE B.LAST_UPDATE END >= TRUNC(SYSDATE-1) AND CASE WHEN B.LAST_UPDATE IS NULL THEN B.PERSONALIZED_DATE ELSE B.LAST_UPDATE END < TRUNC(SYSDATE-1)+1 ";							
						}
			    $sql .= "AND CASE WHEN UPPER(B.LAST_UPDATED_USERNAME) IS NULL THEN UPPER(B.CREATED_USERNAME) ELSE UPPER(B.LAST_UPDATED_USERNAME) END  NOT IN (SELECT UPPER(USER_BCARD) FROM SIAK_CETAK_GOIB@YZDB WHERE IS_ACTIVE = 1)
					    AND
					    C.NO_PROP = 32 AND C.NO_KAB = 73
					    GROUP BY C.NO_KEC, C.NAMA_KEC
					    ORDER BY C.NO_KEC, C.NAMA_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";
             if ($is_database == '222'){
             	$r = DB::connection('db222')->select($sql);
             }else if($is_database == '221'){
             	$r = DB::connection('db221')->select($sql);
             }else if($is_database == '2'){
             	$r = DB::connection('db2')->select($sql);
             }else if($is_database == 'YZDB'){
             	$r = DB::select($sql);
             }

               return $r;
        }
         
         function get_jumlah_rek_global($tbl_name,$date_col,$pk_col,$check_exists_bio,$is_today,$is_database,$is_dblink ='',$start_date = '',$end_date = '',$is_coklit = 0)
        {
        	$sql = "";
        	$sql .= "SELECT  COUNT(1) AS JUMLAH";
			if ($is_coklit == 1){
				$sql .= " , SUM(CASE WHEN NOT EXISTS(SELECT 1 FROM A_KPU_FINAL D WHERE TO_CHAR(A.NIK) = D.NIK) THEN 1 ELSE 0 END) AS UMUM
					      , SUM(CASE WHEN EXISTS(SELECT 1 FROM A_KPU_FINAL D WHERE TO_CHAR(A.NIK) = D.NIK) THEN 1 ELSE 0 END) AS COKLIT";
			}
				$sql .= " FROM 
					    ".$tbl_name." A ";
		if($check_exists_bio == 1){
				$sql .= " INNER JOIN ";
			if ($is_database == '222'){
				$sql .= " BIODATA_WNI C ";
				}else{
				$sql .= " BIODATA_WNI".$is_dblink." C ";
				}
				$sql .= " ON A.".$pk_col." = C.NIK ";
			}
				$sql .= "	   
					WHERE
						1=1";
			
			if($is_today == 1){
				$sql .= " AND TO_CHAR(A.".$date_col.",'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') ";
			}else if($is_today == 2){
				$sql .= " AND TO_CHAR(A.".$date_col.",'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') ";
			}
			if ($is_database == '222'){
				if($check_exists_bio == 1){
		            $sql .= " AND EXISTS(SELECT 1 FROM BIODATA_WNI B WHERE A.".$pk_col." =B.NIK) ";
		        } 
		    }else{
		    	if($check_exists_bio == 1){
		            $sql .= " AND EXISTS(SELECT 1 FROM BIODATA_WNI".$is_dblink." B WHERE A.".$pk_col." =B.NIK) ";
		        }
		    }
		     $sql .= " AND EXISTS(SELECT 1 FROM FACES B WHERE A.".$pk_col." =B.NIK) ";
             if ($is_database == '222'){
             	$r = DB::connection('db222')->select($sql);
             }else if($is_database == '221'){
             	$r = DB::connection('db221')->select($sql);
             }else if($is_database == '2'){
             	$r = DB::connection('db2')->select($sql);
             }else if($is_database == 'YZDB'){
             	$r = DB::select($sql);
             }

               return $r;
        }
        function get_jumlah_ctk_global()
        {
        	$sql = "";
        	$sql .= "SELECT  COUNT(1) AS JUMLAH";
			if ($is_coklit == 1){
				$sql .= " , SUM(CASE WHEN NOT EXISTS(SELECT 1 FROM A_KPU_FINAL D WHERE TO_CHAR(A.NIK) = D.NIK) THEN 1 ELSE 0 END) AS UMUM
					      , SUM(CASE WHEN EXISTS(SELECT 1 FROM A_KPU_FINAL D WHERE TO_CHAR(A.NIK) = D.NIK) THEN 1 ELSE 0 END) AS COKLIT";
			}
				$sql .= " FROM 
					    ".$tbl_name." A ";
		if($check_exists_bio == 1){
				$sql .= " INNER JOIN ";
			if ($is_database == '222'){
				$sql .= " BIODATA_WNI C ";
				}else{
				$sql .= " BIODATA_WNI".$is_dblink." C ";
				}
				$sql .= " ON A.".$pk_col." = C.NIK ";
			}
				$sql .= "	   
					WHERE
						1=1";
			
			if($is_today == 1){
				$sql .= " AND TO_CHAR(A.".$date_col.",'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') ";
			}else if($is_today == 2){
				$sql .= " AND TO_CHAR(A.".$date_col.",'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') ";
			}
			if ($is_database == '222'){
				if($check_exists_bio == 1){
		            $sql .= " AND EXISTS(SELECT 1 FROM BIODATA_WNI B WHERE A.".$pk_col." =B.NIK) ";
		        } 
		    }else{
		    	if($check_exists_bio == 1){
		            $sql .= " AND EXISTS(SELECT 1 FROM BIODATA_WNI".$is_dblink." B WHERE A.".$pk_col." =B.NIK) ";
		        }
		    }
		     $sql .= " AND EXISTS(SELECT 1 FROM FACES B WHERE A.".$pk_col." =B.NIK) ";
             if ($is_database == '222'){
             	$r = DB::connection('db222')->select($sql);
             }else if($is_database == '221'){
             	$r = DB::connection('db221')->select($sql);
             }else if($is_database == '2'){
             	$r = DB::connection('db2')->select($sql);
             }else if($is_database == 'YZDB'){
             	$r = DB::select($sql);
             }

               return $r;
        }
        public function get_detail_suket($tbl_name,$date_col,$pk_col,$check_exists_bio,$is_today,$is_database,$is_dblink ='',$start_date = '',$end_date = '',$is_coklit = 0)
        {
        	$sql = "";
        	$sql .= "SELECT 
                        A.NO_KEC 
                        , A.NAMA_KEC";
            if($is_today == 1){
				$sql .= " , CASE WHEN B.TANGGAL IS NULL THEN TO_CHAR(SYSDATE,'DD/MM/YYYY') ELSE B.TANGGAL END AS TANGGAL ";
			}else if($is_today == 2){
				$sql .= " , CASE WHEN B.TANGGAL IS NULL THEN TO_CHAR(SYSDATE-1,'DD/MM/YYYY') ELSE B.TANGGAL END AS TANGGAL ";
			}
			if ($is_coklit == 1){
				$sql .= " , CASE WHEN B.UMUM IS NULL THEN 0 ELSE B.UMUM END AS UMUM
                        , CASE WHEN B.COKLIT IS NULL THEN 0 ELSE B.COKLIT END AS COKLIT
                		, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH";
			}else{
				$sql .= " , CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH";
            }
            
            if ($is_database == '222'){
				$sql .= "  FROM SETUP_KEC A LEFT JOIN";
			}else{
				$sql .= "  FROM SETUP_KEC".$is_dblink." A LEFT JOIN";
			}
                 $sql .= "  
           			(SELECT 
					    X.NO_KEC
					    , X.NAMA_KEC";
			if ($is_coklit == 1){
				$sql .= " , SUM(CASE WHEN NOT EXISTS(SELECT 1 FROM A_KPU_FINAL D WHERE TO_CHAR(A.NIK) = D.NIK) THEN 1 ELSE 0 END) AS UMUM
					    , SUM(CASE WHEN EXISTS(SELECT 1 FROM A_KPU_FINAL D WHERE TO_CHAR(A.NIK) = D.NIK) THEN 1 ELSE 0 END) AS COKLIT";
			}
				$sql .= " , to_char(A.".$date_col.",'DD/MM/YYYY') AS TANGGAL
					    , COUNT(DISTINCT(A.NIK)) AS JUMLAH
					FROM 
					    ".$tbl_name." A ";
		if($check_exists_bio == 1){
				$sql .= " INNER JOIN ";
			if ($is_database == '222'){
				$sql .= " BIODATA_WNI C ";
				}else{
				$sql .= " BIODATA_WNI".$is_dblink." C ";
				}
				$sql .= " ON A.".$pk_col." = C.NIK 
					INNER JOIN ";
			if ($is_database == '222'){
				$sql .= "   SETUP_KEC X ";
			}else{
				$sql .= "   SETUP_KEC".$is_dblink." X ";
			}
				$sql .= "		    
					    ON C.NO_PROP = X.NO_PROP 
					    AND C.NO_KAB = X.NO_KAB 
					    AND C.NO_KEC = X.NO_KEC ";
			}else{
				$sql .= " INNER JOIN ";
				if ($is_database == '222'){
				$sql .= "   SETUP_KEC X ";
			}else{
				$sql .= "   SETUP_KEC".$is_dblink." X ";
			}
				$sql .= "		    
					    ON A.NO_PROP = X.NO_PROP 
					    AND A.NO_KAB = X.NO_KAB 
					    AND A.NO_KEC = X.NO_KEC ";
			}
				$sql .= "	   
					WHERE
						1=1";
			
			if($is_today == 1){
				$sql .= " AND TO_CHAR(A.".$date_col.",'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') ";
			}else if($is_today == 2){
				$sql .= " AND TO_CHAR(A.".$date_col.",'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') ";
			}
			if ($is_database == '222'){
				if($check_exists_bio == 1){
		            $sql .= " AND EXISTS(SELECT 1 FROM BIODATA_WNI B WHERE A.".$pk_col." =B.NIK) ";
		        } 
		    }else{
		    	if($check_exists_bio == 1){
		            $sql .= " AND EXISTS(SELECT 1 FROM BIODATA_WNI".$is_dblink." B WHERE A.".$pk_col." =B.NIK) ";
		        }
		    }
				$sql .= " GROUP BY 
					    X.NO_KEC
					    ,X.NAMA_KEC,to_char(A.".$date_col.",'DD/MM/YYYY')
					ORDER BY 
					    X.NO_KEC,X.NAMA_KEC) B ON 
                        A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB =73 ORDER BY A.NO_KEC";
 		
             if ($is_database == '222'){
             	$r = DB::connection('db222')->select($sql);
             }else if($is_database == '221'){
             	$r = DB::connection('db221')->select($sql);
             }else if($is_database == '2'){
             	$r = DB::connection('db2')->select($sql);
             }else if($is_database == 'YZDB'){
             	$r = DB::select($sql);
             }

               return $r;
        }
         
         function get_jumlah_suket($tbl_name,$date_col,$pk_col,$check_exists_bio,$is_today,$is_database,$is_dblink ='',$start_date = '',$end_date = '',$is_coklit = 0)
        {
        	$sql = "";
        	$sql .= "SELECT  COUNT(DISTINCT(A.NIK)) AS JUMLAH";
			if ($is_coklit == 1){
				$sql .= " , SUM(CASE WHEN NOT EXISTS(SELECT 1 FROM A_KPU_FINAL D WHERE TO_CHAR(A.NIK) = D.NIK) THEN 1 ELSE 0 END) AS UMUM
					      , SUM(CASE WHEN EXISTS(SELECT 1 FROM A_KPU_FINAL D WHERE TO_CHAR(A.NIK) = D.NIK) THEN 1 ELSE 0 END) AS COKLIT";
			}
				$sql .= " FROM 
					    ".$tbl_name." A ";
		if($check_exists_bio == 1){
				$sql .= " INNER JOIN ";
			if ($is_database == '222'){
				$sql .= " BIODATA_WNI C ";
				}else{
				$sql .= " BIODATA_WNI".$is_dblink." C ";
				}
				$sql .= " ON A.".$pk_col." = C.NIK ";
			}
				$sql .= "	   
					WHERE
						1=1";
			
			if($is_today == 1){
				$sql .= " AND TO_CHAR(A.".$date_col.",'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') ";
			}else if($is_today == 2){
				$sql .= " AND TO_CHAR(A.".$date_col.",'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') ";
			}
			if ($is_database == '222'){
				if($check_exists_bio == 1){
		            $sql .= " AND EXISTS(SELECT 1 FROM BIODATA_WNI B WHERE A.".$pk_col." =B.NIK) ";
		        } 
		    }else{
		    	if($check_exists_bio == 1){
		            $sql .= " AND EXISTS(SELECT 1 FROM BIODATA_WNI".$is_dblink." B WHERE A.".$pk_col." =B.NIK) ";
		        }
		    }
             if ($is_database == '222'){
             	$r = DB::connection('db222')->select($sql);
             }else if($is_database == '221'){
             	$r = DB::connection('db221')->select($sql);
             }else if($is_database == '2'){
             	$r = DB::connection('db2')->select($sql);
             }else if($is_database == 'YZDB'){
             	$r = DB::select($sql);
             }

               return $r;
        }
        public function get_detail_global_mobilitas($tbl_name,$date_col,$pk_col,$check_is_pindah,$is_today)
        {
        	$sql = "";
        	$sql .= "SELECT 
						A.NO_KEC 
						, A.NAMA_KEC ";
			if($is_today ==1){
				$sql .= " , CASE WHEN B.TANGGAL IS NULL THEN TO_CHAR(SYSDATE,'DD/MM/YYYY') ELSE B.TANGGAL END AS TANGGAL ";
			}else{
				$sql .= " , CASE WHEN B.TANGGAL IS NULL THEN TO_CHAR(SYSDATE-1,'DD/MM/YYYY') ELSE B.TANGGAL END AS TANGGAL ";
			}
				$sql .= " , CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH 
						, CASE WHEN B.JUMLAH_ANGGOTA IS NULL THEN 0 ELSE B.JUMLAH_ANGGOTA END AS JUMLAH_ANGGOTA 
					FROM SETUP_KEC A 
					LEFT JOIN 
						(
							SELECT 
								X.NO_KEC 
								, X.NAMA_KEC 
								, to_char(A.".$date_col.",'DD/MM/YYYY') AS TANGGAL 
								, COUNT(DISTINCT(A.".$pk_col.")) as JUMLAH
					            , COUNT(A.".$pk_col.") AS JUMLAH_ANGGOTA
								from ".$tbl_name." ON A.".$pk_col." = B.".$pk_col."
							INNER JOIN SETUP_KEC X";
							if($check_is_pindah ==1){
					$sql .= " ON A.FROM_NO_PROP = X.NO_PROP 
							AND A.FROM_NO_KAB = X.NO_KAB 
							AND A.FROM_NO_KEC = X.NO_KEC 
							WHERE 1=1 AND
					        A.FROM_NO_PROP = 32 AND A.FROM_NO_KAB =73";
							} else{
					$sql .= " ON A.NO_PROP = X.NO_PROP 
							AND A.NO_KAB = X.NO_KAB 
							AND A.NO_KEC = X.NO_KEC 
							WHERE 1=1 AND
					        A.NO_PROP = 32 AND A.NO_KAB =73";
							}
					if($is_today ==1){
						$sql .= " AND TO_CHAR(A.".$date_col.",'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') ";
					}else if($is_today ==2){
						$sql .= " AND TO_CHAR(A.".$date_col.",'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') ";
					}
					$sql .= "
							GROUP BY X.NO_KEC ,X.NAMA_KEC,to_char(A.".$date_col.",'DD/MM/YYYY') 
							ORDER BY X.NO_KEC,X.NAMA_KEC) B 
							ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB =73 ORDER BY A.NO_KEC";
							
             	$r = DB::connection('db222')->select($sql);
               return $r;
        }
         public function get_jumlah_global_mobilitas($tbl_name,$date_col,$pk_col,$check_is_pindah,$is_today)
        {
        	$sql = "";
        	$sql .= "SELECT COUNT(DISTINCT(A.".$pk_col.")) as JUMLAH
					        , COUNT(A.".$pk_col.") AS JUMLAH_ANGGOTA
							from ".$tbl_name." ON A.".$pk_col." = B.".$pk_col."
							WHERE 1=1";
					if($is_today ==1){
						$sql .= " AND TO_CHAR(A.".$date_col.",'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') ";
					}else if($is_today ==2){
						$sql .= " AND TO_CHAR(A.".$date_col.",'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') ";
					}
					if($check_is_pindah ==1){
					$sql .= " AND A.FROM_NO_PROP = 32 AND A.FROM_NO_KAB =73";
							} else{
					$sql .= " AND A.NO_PROP = 32 AND A.NO_KAB =73";
					}
             	$r = DB::connection('db222')->select($sql);
               return $r;
        }
        	
        public function get_detail_capil_lahir($tbl_name,$date_col,$pk_col,$check_exists_bio,$is_today,$is_database,$is_where = '')
        {
        	$sql = "";
        	$sql .= "SELECT 
                        A.NO_KEC 
                        , A.NAMA_KEC";
            if($is_today == 1){
				$sql .= " , CASE WHEN B.TANGGAL IS NULL THEN TO_CHAR(SYSDATE,'DD/MM/YYYY') ELSE B.TANGGAL END AS TANGGAL ";
			}else if($is_today == 2){
				$sql .= " , CASE WHEN B.TANGGAL IS NULL THEN TO_CHAR(SYSDATE-1,'DD/MM/YYYY') ELSE B.TANGGAL END AS TANGGAL ";
			}
				$sql .= " , CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH";
            
            if ($is_database == '222'){
				$sql .= "  FROM SETUP_KEC A LEFT JOIN";
			}else{
				$sql .= "  FROM SETUP_KEC".$is_dblink." A LEFT JOIN";
			}
                 $sql .= "  
           			(SELECT 
					    X.NO_KEC
					    , X.NAMA_KEC";
				$sql .= " , to_char(A.".$date_col.",'DD/MM/YYYY') AS TANGGAL
					    , COUNT(1) AS JUMLAH
					FROM 
					    ".$tbl_name." A ";
		if($check_exists_bio == 1){
				$sql .= " INNER JOIN ";
			if ($is_database == '222'){
				$sql .= " BIODATA_WNI C ";
				}else{
				$sql .= " BIODATA_WNI".$is_dblink." C ";
				}
				$sql .= " ON A.".$pk_col." = C.NIK 
					INNER JOIN ";
			if ($is_database == '222'){
				$sql .= "   SETUP_KEC X ";
			}else{
				$sql .= "   SETUP_KEC".$is_dblink." X ";
			}
				$sql .= "		    
					    ON C.NO_PROP = X.NO_PROP 
					    AND C.NO_KAB = X.NO_KAB 
					    AND C.NO_KEC = X.NO_KEC ";
			}
				$sql .= "	   
					WHERE
						1=1";
			if ($is_where != ''){
				$sql .= $is_where;
			}
			if($is_today == 1){
				$sql .= " AND TO_CHAR(A.".$date_col.",'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') ";
			}else if($is_today == 2){
				$sql .= " AND TO_CHAR(A.".$date_col.",'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') ";
			}
			if ($is_database == '222'){
				if($check_exists_bio == 1){
		            $sql .= " AND EXISTS(SELECT 1 FROM BIODATA_WNI B WHERE A.".$pk_col." =B.NIK) ";
		        } 
		    }else{
		    	if($check_exists_bio == 1){
		            $sql .= " AND EXISTS(SELECT 1 FROM BIODATA_WNI".$is_dblink." B WHERE A.".$pk_col." =B.NIK) ";
		        }
		    }
				$sql .= " GROUP BY 
					    X.NO_KEC
					    ,X.NAMA_KEC,to_char(A.".$date_col.",'DD/MM/YYYY')
					ORDER BY 
					    X.NO_KEC,X.NAMA_KEC) B ON 
                        A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB =73 ORDER BY A.NO_KEC";
 				
 				


             if ($is_database == '222'){
             	$r = DB::connection('db222')->select($sql);
             }else if($is_database == '221'){
             	$r = DB::connection('db221')->select($sql);
             }else if($is_database == '2'){
             	$r = DB::connection('db2')->select($sql);
             }else if($is_database == 'YZDB'){
             	$r = DB::select($sql);
             }

               return $r;
        	}

        	public function get_jumlah_capil_lahir($tbl_name,$date_col,$pk_col,$check_exists_bio,$is_today,$is_database,$is_where = '')
        	{
        	$sql = "";
        	$sql .= "SELECT COUNT(1) AS JUMLAH
					FROM 
					    ".$tbl_name." A ";
		if($check_exists_bio == 1){
				$sql .= " INNER JOIN ";
			if ($is_database == '222'){
				$sql .= " BIODATA_WNI C ";
				}else{
				$sql .= " BIODATA_WNI".$is_dblink." C ";
				}
				$sql .= " ON A.".$pk_col." = C.NIK ";
			}
				$sql .= "	   
					WHERE
						1=1";
			if ($is_where != ''){
				$sql .= $is_where;
			}
			if($is_today == 1){
				$sql .= " AND TO_CHAR(A.".$date_col.",'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') ";
			}else if($is_today == 2){
				$sql .= " AND TO_CHAR(A.".$date_col.",'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY') ";
			}
			if ($is_database == '222'){
				if($check_exists_bio == 1){
		            $sql .= " AND EXISTS(SELECT 1 FROM BIODATA_WNI B WHERE A.".$pk_col." =B.NIK) ";
		        } 
		    }else{
		    	if($check_exists_bio == 1){
		            $sql .= " AND EXISTS(SELECT 1 FROM BIODATA_WNI".$is_dblink." B WHERE A.".$pk_col." =B.NIK) ";
		        }
		    }

             if ($is_database == '222'){
             	$r = DB::connection('db222')->select($sql);
             }else if($is_database == '221'){
             	$r = DB::connection('db221')->select($sql);
             }else if($is_database == '2'){
             	$r = DB::connection('db2')->select($sql);
             }else if($is_database == 'YZDB'){
             	$r = DB::select($sql);
             }

               return $r;
        	}
        	 public function get_detail_capil($tbl_name,$date_col,$is_today,$get_col)
        	 {
        	 		$sql ="";
        	 		$sql .="
        	 				SELECT 
	        	 				".$get_col."
        	 				FROM 
        	 					".$tbl_name." A 
        	 				WHERE 
        	 					1=1";
 					if($is_today == 1){
						$sql .="
        	 					AND TO_CHAR(A.".$date_col.",'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY')";
 	 				}else{
 	 					$sql .="
        	 					AND TO_CHAR(A.".$date_col.",'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY')";
 	 				}
        	 		$r = DB::connection('db222')->select($sql);
               		return $r;

        	 }

        	  public function get_detail_request($tbl_name,$date_col,$is_today,$get_col,$is_where)
        	 {
        	 		$sql ="";
        	 		$sql .="
        	 				SELECT 
	        	 				".$get_col."
        	 				FROM 
        	 					".$tbl_name." A 
        	 				WHERE 
        	 					1=1";
 					if($is_today == 1){
						$sql .="
        	 					AND TO_CHAR(A.".$date_col.",'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY')";
 	 				}else{
 	 					$sql .="
        	 					AND TO_CHAR(A.".$date_col.",'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY')";
 	 				}
 	 					$sql .=" ".$is_where." ";
        	 		$r = DB::connection('db222')->select($sql);
               		return $r;

        	 }

        	  public function get_detail_status_rekam($tbl_name,$date_col,$is_today,$get_col,$is_where)
        	 {
        	 		$sql ="";
        	 		$sql .="
        	 				SELECT 
	        	 				".$get_col."
        	 				FROM 
        	 					".$tbl_name." A 
        	 				WHERE 
        	 					1=1";
 					if($is_today == 1){
						$sql .="
        	 					AND TO_CHAR(A.".$date_col.",'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY')";
 	 				}else{
 	 					$sql .="
        	 					AND TO_CHAR(A.".$date_col.",'DD/MM/YYYY') = TO_CHAR(SYSDATE-1,'DD/MM/YYYY')";
 	 				}
 	 					$sql .=" ".$is_where." ";
        	 		$r = DB::connection('db221')->select($sql);
               		return $r;

        	 }
        	 public function get_detail_user_cetak($user_bcard){
            $sql ="SELECT 
					A.CHIP_ID
					, A.NIK
					, B.NAMA_LGKP NAMA_LENGKAP 
					, B.CURRENT_STATUS_CODE
					, TO_CHAR(B.TGL_LHR,'DD-MM-YYYY') AS TGL_LHR
					, TRUNC(MONTHS_BETWEEN(SYSDATE,B.TGL_LHR)/12) UMUR
					, B.AGAMA
					, CASE WHEN TO_CHAR(H.REQ_DATE,'DD-MM-YYYY') IS NULL THEN '-' ELSE TO_CHAR(H.REQ_DATE,'DD-MM-YYYY') END REQ_DATE
					, CASE WHEN H.REQ_BY IS NULL THEN '-' ELSE H.REQ_BY END REQ_BY
					, CASE WHEN TO_CHAR(LAST_UPDATE,'DD-MM-YYYY') IS NULL THEN TO_CHAR(PERSONALIZED_DATE,'DD-MM-YYYY') ELSE TO_CHAR(LAST_UPDATE,'DD-MM-YYYY') END PRINTED_DATE 
					, CASE WHEN LAST_UPDATE IS NULL THEN TO_CHAR(PERSONALIZED_DATE,'HH24:MI') ELSE TO_CHAR(LAST_UPDATE,'HH24:MI') END PRINTED_HR
					, CASE WHEN A.LAST_UPDATED_USERNAME IS NULL THEN A.CREATED_USERNAME ELSE A.LAST_UPDATED_USERNAME END PRINTED_BY 
					, B.NAMA_KEC
					, B.NAMA_KEL
					, B.ALAMAT
					, LPAD(TO_CHAR(B.NO_RT), 3, '0') AS RT
					, LPAD(TO_CHAR(B.NO_RW), 3, '0') AS RW
					, CASE WHEN B.KODE_POS IS NULL THEN '-' ELSE TO_CHAR(B.KODE_POS) END KODE_POS
					, B.STAT_KWN
					, B.JENIS_PKRJN
					FROM CARD_MANAGEMENT A LEFT JOIN DEMOGRAPHICS B ON A.NIK = B.NIK
					LEFT JOIN (SELECT NIK,REQ_DATE,REQ_BY,PROC_DATE,PROC_BY,STEP_PROC FROM (SELECT NIK,REQ_DATE,REQ_BY,PROC_DATE,PROC_BY,STEP_PROC, RANK() 
					OVER (PARTITION BY NIK ORDER BY REQ_DATE,ID_CETAK DESC) RNK FROM SIAK_REQ_CETAK_KTP@YZDB) WHERE RNK = 1) H ON A.NIK = H.NIK
					WHERE CASE WHEN TRUNC(LAST_UPDATE) IS NULL THEN TRUNC(PERSONALIZED_DATE) ELSE TRUNC(LAST_UPDATE) END =  TRUNC(SYSDATE) 
					AND CASE WHEN UPPER(A.LAST_UPDATED_USERNAME) IS NULL THEN UPPER(TO_CHAR(A.CREATED_USERNAME)) ELSE UPPER(TO_CHAR(A.LAST_UPDATED_USERNAME)) END LIKE UPPER('%$user_bcard%')
					ORDER BY CASE WHEN LAST_UPDATE IS NULL THEN TO_CHAR(PERSONALIZED_DATE,'HH24:MI') ELSE TO_CHAR(LAST_UPDATE,'HH24:MI') END";
           		 $r = DB::connection('db2')->select($sql);
           		 return $r;
        	}

           public function get_detail_agama_user_cetak($user_bcard){
            $sql ="SELECT 
					B.AGAMA
					, COUNT(1) JML
					FROM CARD_MANAGEMENT@DB5CETAK A LEFT JOIN DEMOGRAPHICS B ON A.NIK = B.NIK
					WHERE CASE WHEN TRUNC(LAST_UPDATE) IS NULL THEN TRUNC(PERSONALIZED_DATE) ELSE TRUNC(LAST_UPDATE) END =  TRUNC(SYSDATE) 
					AND CASE WHEN UPPER(A.LAST_UPDATED_USERNAME) IS NULL THEN UPPER(TO_CHAR(A.CREATED_USERNAME)) ELSE UPPER(TO_CHAR(A.LAST_UPDATED_USERNAME)) END LIKE UPPER('%$user_bcard%')
					GROUP BY B.AGAMA";
	            $r = DB::connection('db2')->select($sql);
	            return $r;
   			 }

           public function get_detail_kec_user_cetak($user_bcard){
            $sql ="SELECT 
					CASE WHEN B.NAMA_KEC IS NULL THEN '-' ELSE B.NAMA_KEC END NAMA_KEC
					, COUNT(1) JML
					FROM CARD_MANAGEMENT@DB5CETAK A LEFT JOIN DEMOGRAPHICS B ON A.NIK = B.NIK
					WHERE CASE WHEN TRUNC(LAST_UPDATE) IS NULL THEN TRUNC(PERSONALIZED_DATE) ELSE TRUNC(LAST_UPDATE) END =  TRUNC(SYSDATE) 
					AND CASE WHEN UPPER(A.LAST_UPDATED_USERNAME) IS NULL THEN UPPER(TO_CHAR(A.CREATED_USERNAME)) ELSE UPPER(TO_CHAR(A.LAST_UPDATED_USERNAME)) END LIKE UPPER('%$user_bcard%')
					GROUP BY B.NAMA_KEC
					ORDER BY B.NAMA_KEC
					";
	            $r = DB::connection('db2')->select($sql);
	            return $r;
   			 }
        	
           public function get_detail_kel_user_cetak($user_bcard){
            $sql ="SELECT 
					CASE WHEN B.NAMA_KEC IS NULL THEN '-' ELSE B.NAMA_KEC END NAMA_KEC
					, CASE WHEN B.NAMA_KEL IS NULL THEN '-' ELSE B.NAMA_KEL END NAMA_KEL
					, COUNT(1) JML
					FROM CARD_MANAGEMENT@DB5CETAK A LEFT JOIN DEMOGRAPHICS B ON A.NIK = B.NIK
					WHERE CASE WHEN TRUNC(LAST_UPDATE) IS NULL THEN TRUNC(PERSONALIZED_DATE) ELSE TRUNC(LAST_UPDATE) END =  TRUNC(SYSDATE) 
					AND CASE WHEN UPPER(A.LAST_UPDATED_USERNAME) IS NULL THEN UPPER(TO_CHAR(A.CREATED_USERNAME)) ELSE UPPER(TO_CHAR(A.LAST_UPDATED_USERNAME)) END LIKE UPPER('%$user_bcard%')
					GROUP BY B.NAMA_KEC, B.NAMA_KEL
					ORDER BY B.NAMA_KEC, B.NAMA_KEL
					";
	            $r = DB::connection('db2')->select($sql);
	            return $r;
   			 }
        	
           public function get_detail_rw_user_cetak($user_bcard){
            $sql ="SELECT 
					CASE WHEN B.NAMA_KEC IS NULL THEN '-' ELSE B.NAMA_KEC END NAMA_KEC
					, CASE WHEN B.NAMA_KEL IS NULL THEN '-' ELSE B.NAMA_KEL END NAMA_KEL
					, LPAD(TO_CHAR(B.NO_RW), 3, '0') AS RW
					, COUNT(1) JML
					FROM CARD_MANAGEMENT@DB5CETAK A LEFT JOIN DEMOGRAPHICS B ON A.NIK = B.NIK
					WHERE CASE WHEN TRUNC(LAST_UPDATE) IS NULL THEN TRUNC(PERSONALIZED_DATE) ELSE TRUNC(LAST_UPDATE) END =  TRUNC(SYSDATE) 
					AND CASE WHEN UPPER(A.LAST_UPDATED_USERNAME) IS NULL THEN UPPER(TO_CHAR(A.CREATED_USERNAME)) ELSE UPPER(TO_CHAR(A.LAST_UPDATED_USERNAME)) END LIKE UPPER('%$user_bcard%')
					GROUP BY B.NAMA_KEC, B.NAMA_KEL,B.NO_RW
					ORDER BY B.NAMA_KEC, B.NAMA_KEL,B.NO_RW
					";
	            $r = DB::connection('db2')->select($sql);
	            return $r;
   			 }
        	
           public function get_detail_rt_user_cetak($user_bcard){
            $sql ="SELECT 
					CASE WHEN B.NAMA_KEC IS NULL THEN '-' ELSE B.NAMA_KEC END NAMA_KEC
					, CASE WHEN B.NAMA_KEL IS NULL THEN '-' ELSE B.NAMA_KEL END NAMA_KEL
					, LPAD(TO_CHAR(B.NO_RW), 3, '0') AS RW
					, LPAD(TO_CHAR(B.NO_RT), 3, '0') AS RT
					, COUNT(1) JML
					FROM CARD_MANAGEMENT@DB5CETAK A LEFT JOIN DEMOGRAPHICS B ON A.NIK = B.NIK
					WHERE CASE WHEN TRUNC(LAST_UPDATE) IS NULL THEN TRUNC(PERSONALIZED_DATE) ELSE TRUNC(LAST_UPDATE) END =  TRUNC(SYSDATE) 
					AND CASE WHEN UPPER(A.LAST_UPDATED_USERNAME) IS NULL THEN UPPER(TO_CHAR(A.CREATED_USERNAME)) ELSE UPPER(TO_CHAR(A.LAST_UPDATED_USERNAME)) END LIKE UPPER('%$user_bcard%')
					GROUP BY B.NAMA_KEC, B.NAMA_KEL,B.NO_RW,B.NO_RT
					ORDER BY B.NAMA_KEC, B.NAMA_KEL,B.NO_RW,B.NO_RT
					";
	            $r = DB::connection('db2')->select($sql);
	            return $r;
   			 }
        	 public function get_detail_user_cetak_tanggal($user_bcard,$tanggal){
            $sql ="SELECT 
					A.CHIP_ID
					, A.NIK
					, B.NAMA_LGKP NAMA_LENGKAP 
					, B.CURRENT_STATUS_CODE
					, TO_CHAR(B.TGL_LHR,'DD-MM-YYYY') AS TGL_LHR
					, TRUNC(MONTHS_BETWEEN(SYSDATE,B.TGL_LHR)/12) UMUR
					, B.AGAMA
					, CASE WHEN TO_CHAR(H.REQ_DATE,'DD-MM-YYYY') IS NULL THEN '-' ELSE TO_CHAR(H.REQ_DATE,'DD-MM-YYYY') END REQ_DATE
					, CASE WHEN H.REQ_BY IS NULL THEN '-' ELSE H.REQ_BY END REQ_BY
					, CASE WHEN TO_CHAR(LAST_UPDATE,'DD-MM-YYYY') IS NULL THEN TO_CHAR(PERSONALIZED_DATE,'DD-MM-YYYY') ELSE TO_CHAR(LAST_UPDATE,'DD-MM-YYYY') END PRINTED_DATE 
					, CASE WHEN LAST_UPDATE IS NULL THEN TO_CHAR(PERSONALIZED_DATE,'HH24:MI') ELSE TO_CHAR(LAST_UPDATE,'HH24:MI') END PRINTED_HR
					, CASE WHEN A.LAST_UPDATED_USERNAME IS NULL THEN A.CREATED_USERNAME ELSE A.LAST_UPDATED_USERNAME END PRINTED_BY 
					, B.NAMA_KEC
					, B.NAMA_KEL
					, B.ALAMAT
					, LPAD(TO_CHAR(B.NO_RT), 3, '0') AS RT
					, LPAD(TO_CHAR(B.NO_RW), 3, '0') AS RW
					, CASE WHEN B.KODE_POS IS NULL THEN '-' ELSE TO_CHAR(B.KODE_POS) END KODE_POS
					, B.STAT_KWN
					, B.JENIS_PKRJN
					FROM CARD_MANAGEMENT@DB5CETAK A LEFT JOIN DEMOGRAPHICS B ON A.NIK = B.NIK
					LEFT JOIN (SELECT NIK,REQ_DATE,REQ_BY,PROC_DATE,PROC_BY,STEP_PROC FROM (SELECT NIK,REQ_DATE,REQ_BY,PROC_DATE,PROC_BY,STEP_PROC, RANK() 
					OVER (PARTITION BY NIK ORDER BY REQ_DATE,ID_CETAK DESC) RNK FROM SIAK_REQ_CETAK_KTP@YZDB) WHERE RNK = 1) H ON A.NIK = H.NIK
					WHERE CASE WHEN TRUNC(LAST_UPDATE) IS NULL THEN TRUNC(PERSONALIZED_DATE) ELSE TRUNC(LAST_UPDATE) END =  TRUNC(TO_DATE('$tanggal','DD-MM-YYYY')) 
					AND CASE WHEN UPPER(A.LAST_UPDATED_USERNAME) IS NULL THEN UPPER(TO_CHAR(A.CREATED_USERNAME)) ELSE UPPER(TO_CHAR(A.LAST_UPDATED_USERNAME)) END LIKE UPPER('%$user_bcard%')
					ORDER BY CASE WHEN LAST_UPDATE IS NULL THEN TO_CHAR(PERSONALIZED_DATE,'HH24:MI') ELSE TO_CHAR(LAST_UPDATE,'HH24:MI') END";
           		 $r = DB::connection('db2')->select($sql);
           		 return $r;
        	}

           public function get_detail_agama_user_cetak_tanggal($user_bcard,$tanggal){
            $sql ="SELECT 
					B.AGAMA
					, COUNT(1) JML
					FROM CARD_MANAGEMENT@DB5CETAK A LEFT JOIN DEMOGRAPHICS B ON A.NIK = B.NIK
					WHERE CASE WHEN TRUNC(LAST_UPDATE) IS NULL THEN TRUNC(PERSONALIZED_DATE) ELSE TRUNC(LAST_UPDATE) END =  TRUNC(TO_DATE('$tanggal','DD-MM-YYYY')) 
					AND CASE WHEN UPPER(A.LAST_UPDATED_USERNAME) IS NULL THEN UPPER(TO_CHAR(A.CREATED_USERNAME)) ELSE UPPER(TO_CHAR(A.LAST_UPDATED_USERNAME)) END LIKE UPPER('%$user_bcard%')
					GROUP BY B.AGAMA";
	            $r = DB::connection('db2')->select($sql);
	            return $r;
   			 }

           public function get_detail_kec_user_cetak_tanggal($user_bcard,$tanggal){
            $sql ="SELECT 
					CASE WHEN B.NAMA_KEC IS NULL THEN '-' ELSE B.NAMA_KEC END NAMA_KEC
					, COUNT(1) JML
					FROM CARD_MANAGEMENT@DB5CETAK A LEFT JOIN DEMOGRAPHICS B ON A.NIK = B.NIK
					WHERE CASE WHEN TRUNC(LAST_UPDATE) IS NULL THEN TRUNC(PERSONALIZED_DATE) ELSE TRUNC(LAST_UPDATE) END =  TRUNC(TO_DATE('$tanggal','DD-MM-YYYY')) 
					AND CASE WHEN UPPER(A.LAST_UPDATED_USERNAME) IS NULL THEN UPPER(TO_CHAR(A.CREATED_USERNAME)) ELSE UPPER(TO_CHAR(A.LAST_UPDATED_USERNAME)) END LIKE UPPER('%$user_bcard%')
					GROUP BY B.NAMA_KEC
					ORDER BY B.NAMA_KEC
					";
	            $r = DB::connection('db2')->select($sql);
	            return $r;
   			 }
        	
           public function get_detail_kel_user_cetak_tanggal($user_bcard,$tanggal){
            $sql ="SELECT 
					CASE WHEN B.NAMA_KEC IS NULL THEN '-' ELSE B.NAMA_KEC END NAMA_KEC
					, CASE WHEN B.NAMA_KEL IS NULL THEN '-' ELSE B.NAMA_KEL END NAMA_KEL
					, COUNT(1) JML
					FROM CARD_MANAGEMENT@DB5CETAK A LEFT JOIN DEMOGRAPHICS B ON A.NIK = B.NIK
					WHERE CASE WHEN TRUNC(LAST_UPDATE) IS NULL THEN TRUNC(PERSONALIZED_DATE) ELSE TRUNC(LAST_UPDATE) END =  TRUNC(TO_DATE('$tanggal','DD-MM-YYYY')) 
					AND CASE WHEN UPPER(A.LAST_UPDATED_USERNAME) IS NULL THEN UPPER(TO_CHAR(A.CREATED_USERNAME)) ELSE UPPER(TO_CHAR(A.LAST_UPDATED_USERNAME)) END LIKE UPPER('%$user_bcard%')
					GROUP BY B.NAMA_KEC, B.NAMA_KEL
					ORDER BY B.NAMA_KEC, B.NAMA_KEL
					";
	            $r = DB::connection('db2')->select($sql);
	            return $r;
   			 }
        	
           public function get_detail_rw_user_cetak_tanggal($user_bcard,$tanggal){
            $sql ="SELECT 
					CASE WHEN B.NAMA_KEC IS NULL THEN '-' ELSE B.NAMA_KEC END NAMA_KEC
					, CASE WHEN B.NAMA_KEL IS NULL THEN '-' ELSE B.NAMA_KEL END NAMA_KEL
					, LPAD(TO_CHAR(B.NO_RW), 3, '0') AS RW
					, COUNT(1) JML
					FROM CARD_MANAGEMENT@DB5CETAK A LEFT JOIN DEMOGRAPHICS B ON A.NIK = B.NIK
					WHERE CASE WHEN TRUNC(LAST_UPDATE) IS NULL THEN TRUNC(PERSONALIZED_DATE) ELSE TRUNC(LAST_UPDATE) END =  TRUNC(TO_DATE('$tanggal','DD-MM-YYYY')) 
					AND CASE WHEN UPPER(A.LAST_UPDATED_USERNAME) IS NULL THEN UPPER(TO_CHAR(A.CREATED_USERNAME)) ELSE UPPER(TO_CHAR(A.LAST_UPDATED_USERNAME)) END LIKE UPPER('%$user_bcard%')
					GROUP BY B.NAMA_KEC, B.NAMA_KEL,B.NO_RW
					ORDER BY B.NAMA_KEC, B.NAMA_KEL,B.NO_RW
					";
	            $r = DB::connection('db2')->select($sql);
	            return $r;
   			 }
        	
           public function get_detail_rt_user_cetak_tanggal($user_bcard,$tanggal){
            $sql ="SELECT 
					CASE WHEN B.NAMA_KEC IS NULL THEN '-' ELSE B.NAMA_KEC END NAMA_KEC
					, CASE WHEN B.NAMA_KEL IS NULL THEN '-' ELSE B.NAMA_KEL END NAMA_KEL
					, LPAD(TO_CHAR(B.NO_RW), 3, '0') AS RW
					, LPAD(TO_CHAR(B.NO_RT), 3, '0') AS RT
					, COUNT(1) JML
					FROM CARD_MANAGEMENT@DB5CETAK A LEFT JOIN DEMOGRAPHICS B ON A.NIK = B.NIK
					WHERE CASE WHEN TRUNC(LAST_UPDATE) IS NULL THEN TRUNC(PERSONALIZED_DATE) ELSE TRUNC(LAST_UPDATE) END =  TRUNC(TO_DATE('$tanggal','DD-MM-YYYY')) 
					AND CASE WHEN UPPER(A.LAST_UPDATED_USERNAME) IS NULL THEN UPPER(TO_CHAR(A.CREATED_USERNAME)) ELSE UPPER(TO_CHAR(A.LAST_UPDATED_USERNAME)) END LIKE UPPER('%$user_bcard%')
					GROUP BY B.NAMA_KEC, B.NAMA_KEL,B.NO_RW,B.NO_RT
					ORDER BY B.NAMA_KEC, B.NAMA_KEL,B.NO_RW,B.NO_RT
					";
	            $r = DB::connection('db2')->select($sql);
	            return $r;
   			 }

}
