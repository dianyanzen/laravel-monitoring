<?php

namespace App\Http\Controllers;

use DB;
use Session;
use DateTime;
use PDF;
use Illuminate\Http\Request;
use App\Http\Controllers\SharedController as Shr;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class LaporanController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function Laporan_rekam(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 18;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->tanggal != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$title_wil = '';
			if($no_kec == 0){
				$no_wil = 'No Kec';
				$kode_wil = 'Nama Kecamatan';
			}else{
				$no_wil = 'No Kel';
				$kode_wil = 'Nama Kelurahan';
			}
			if($no_kec != 0 && $no_kel == 0){
				$title_wil = ' Kecamatan '.ucfirst(strtolower(Shr::do_get_nama_kec($no_kec)));
			}else if($no_kec != 0 && $no_kel != 0){
				$title_wil = ' Kelurahan '.ucfirst(strtolower(Shr::do_get_nama_kel($no_kec,$no_kel)));
			}
			$r = $this->rkm_get_data_rekam($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'Laporan Perekaman'.$title_wil.' Tanggal '.$tgl,
		 		"mtitle"=>'Laporan Perekaman',
		 		"my_url"=>'Laporan_rekam',
		 		"type_tgl"=>'Perekaman',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>$no_wil,
		 		"kode_wil"=>$kode_wil
    		);
			}else{
			$data = array(
		 		"stitle"=>'Laporan Perekaman',
		 		"mtitle"=>'Laporan Perekaman',
		 		"my_url"=>'Laporan_rekam',
		 		"type_tgl"=>'Perekaman',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>'No Kec',
		 		"kode_wil"=>'Nama Kecamatan'
    		);
    		}
			return view('Laporan/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function Grafik_rekam(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
        	$menu_id = 46;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->tanggal != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$title_wil = '';
			if($no_kec == 0){
				$no_wil = 'No Kec';
				$kode_wil = 'Nama Kecamatan';
			}else{
				$no_wil = 'No Kel';
				$kode_wil = 'Nama Kelurahan';
			}
			if($no_kec != 0 && $no_kel == 0){
				$title_wil = ' Kecamatan '.ucfirst(strtolower(Shr::do_get_nama_kec($no_kec)));
			}else if($no_kec != 0 && $no_kel != 0){
				$title_wil = ' Kelurahan '.ucfirst(strtolower(Shr::do_get_nama_kel($no_kec,$no_kel)));
			}
			$r = $this->rkm_get_data_rekam($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'Grafik Perekaman'.$title_wil.' Tanggal '.$tgl,
		 		"mtitle"=>'Grafik Perekaman',
		 		"my_url"=>'Grafik_rekam',
		 		"type_tgl"=>'Perekaman',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>$no_wil,
		 		"kode_wil"=>$kode_wil
    		);
			}else{
			$data = array(
		 		"stitle"=>'Grafik Perekaman',
		 		"mtitle"=>'Grafik Perekaman',
		 		"my_url"=>'Grafik_rekam',
		 		"type_tgl"=>'Perekaman',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>'No Kec',
		 		"kode_wil"=>'Nama Kecamatan'
    		);
    		}
			return view('Grafik/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}

	 public function Laporan_cetak(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 19;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->tanggal != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$title_wil = '';
			if($no_kec == 0){
				$no_wil = 'No Kec';
				$kode_wil = 'Nama Kecamatan';
			}else{
				$no_wil = 'No Kel';
				$kode_wil = 'Nama Kelurahan';
			}
			if($no_kec != 0 && $no_kel == 0){
				$title_wil = ' Kecamatan '.ucfirst(strtolower(Shr::do_get_nama_kec($no_kec)));
			}else if($no_kec != 0 && $no_kel != 0){
				$title_wil = ' Kelurahan '.ucfirst(strtolower(Shr::do_get_nama_kel($no_kec,$no_kel)));
			}
			$r = $this->ctk_get_data_cetak($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'Laporan Pencetakan'.$title_wil.' Tanggal '.$tgl,
		 		"mtitle"=>'Laporan Pencetakan',
		 		"my_url"=>'Laporan_cetak',
		 		"type_tgl"=>'Pencetakan',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>$no_wil,
		 		"kode_wil"=>$kode_wil
    		);
			}else{
			$data = array(
		 		"stitle"=>'Laporan Pencetakan',
		 		"mtitle"=>'Laporan Pencetakan',
		 		"my_url"=>'Laporan_cetak',
		 		"type_tgl"=>'Pencetakan',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>'No Kec',
		 		"kode_wil"=>'Nama Kecamatan'
    		);
    		}
			return view('Laporan/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function Grafik_cetak(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 52;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->tanggal != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$title_wil = '';
			if($no_kec == 0){
				$no_wil = 'No Kec';
				$kode_wil = 'Nama Kecamatan';
			}else{
				$no_wil = 'No Kel';
				$kode_wil = 'Nama Kelurahan';
			}
			if($no_kec != 0 && $no_kel == 0){
				$title_wil = ' Kecamatan '.ucfirst(strtolower(Shr::do_get_nama_kec($no_kec)));
			}else if($no_kec != 0 && $no_kel != 0){
				$title_wil = ' Kelurahan '.ucfirst(strtolower(Shr::do_get_nama_kel($no_kec,$no_kel)));
			}
			$r = $this->ctk_get_data_cetak($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'Grafik Pencetakan'.$title_wil.' Tanggal '.$tgl,
		 		"mtitle"=>'Grafik Pencetakan',
		 		"my_url"=>'Grafik_cetak',
		 		"type_tgl"=>'Pencetakan',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>$no_wil,
		 		"kode_wil"=>$kode_wil
    		);
			}else{
			$data = array(
		 		"stitle"=>'Grafik Pencetakan',
		 		"mtitle"=>'Grafik Pencetakan',
		 		"my_url"=>'Grafik_cetak',
		 		"type_tgl"=>'Pencetakan',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>'No Kec',
		 		"kode_wil"=>'Nama Kecamatan'
    		);
    		}
			return view('Grafik/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}

	 public function Laporan_suket(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 20;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->tanggal != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$title_wil = '';
			if($no_kec == 0){
				$no_wil = 'No Kec';
				$kode_wil = 'Nama Kecamatan';
			}else{
				$no_wil = 'No Kel';
				$kode_wil = 'Nama Kelurahan';
			}
			if($no_kec != 0 && $no_kel == 0){
				$title_wil = ' Kecamatan '.ucfirst(strtolower(Shr::do_get_nama_kec($no_kec)));
			}else if($no_kec != 0 && $no_kel != 0){
				$title_wil = ' Kelurahan '.ucfirst(strtolower(Shr::do_get_nama_kel($no_kec,$no_kel)));
			}
			$r = $this->skt_get_data_suket($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'Laporan Cetak Suket'.$title_wil.' Tanggal '.$tgl,
		 		"mtitle"=>'Laporan Cetak Suket',
		 		"my_url"=>'Laporan_suket',
		 		"type_tgl"=>'Cetak',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>$no_wil,
		 		"kode_wil"=>$kode_wil
    		);
			}else{
			$data = array(
		 		"stitle"=>'Laporan Cetak Suket',
		 		"mtitle"=>'Laporan Cetak Suket',
		 		"my_url"=>'Laporan_suket',
		 		"type_tgl"=>'Cetak',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>'No Kec',
		 		"kode_wil"=>'Nama Kecamatan'
    		);
    		}
			return view('Laporan/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}

	 public function Grafik_suket(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 53;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->tanggal != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$title_wil = '';
			if($no_kec == 0){
				$no_wil = 'No Kec';
				$kode_wil = 'Nama Kecamatan';
			}else{
				$no_wil = 'No Kel';
				$kode_wil = 'Nama Kelurahan';
			}
			if($no_kec != 0 && $no_kel == 0){
				$title_wil = ' Kecamatan '.ucfirst(strtolower(Shr::do_get_nama_kec($no_kec)));
			}else if($no_kec != 0 && $no_kel != 0){
				$title_wil = ' Kelurahan '.ucfirst(strtolower(Shr::do_get_nama_kel($no_kec,$no_kel)));
			}
			$r = $this->skt_get_data_suket($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'Grafik Cetak Suket'.$title_wil.' Tanggal '.$tgl,
		 		"mtitle"=>'Grafik Cetak Suket',
		 		"my_url"=>'Grafik_suket',
		 		"type_tgl"=>'Cetak',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>$no_wil,
		 		"kode_wil"=>$kode_wil
    		);
			}else{
			$data = array(
		 		"stitle"=>'Grafik Cetak Suket',
		 		"mtitle"=>'Grafik Cetak Suket',
		 		"my_url"=>'Grafik_suket',
		 		"type_tgl"=>'Cetak',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>'No Kec',
		 		"kode_wil"=>'Nama Kecamatan'
    		);
    		}
			return view('Grafik/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}

	 public function Laporan_kk(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 21;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->tanggal != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$title_wil = '';
			if($no_kec == 0){
				$no_wil = 'No Kec';
				$kode_wil = 'Nama Kecamatan';
			}else{
				$no_wil = 'No Kel';
				$kode_wil = 'Nama Kelurahan';
			}
			if($no_kec != 0 && $no_kel == 0){
				$title_wil = ' Kecamatan '.ucfirst(strtolower(Shr::do_get_nama_kec($no_kec)));
			}else if($no_kec != 0 && $no_kel != 0){
				$title_wil = ' Kelurahan '.ucfirst(strtolower(Shr::do_get_nama_kel($no_kec,$no_kel)));
			}
			$r = $this->get_data_kk($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'Laporan Cetak KK'.$title_wil.' Tanggal '.$tgl,
		 		"mtitle"=>'Laporan Cetak KK',
		 		"my_url"=>'Laporan_kk',
		 		"type_tgl"=>'Cetak',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>$no_wil,
		 		"kode_wil"=>$kode_wil
    		);
			}else{
			$data = array(
		 		"stitle"=>'Laporan Cetak KK',
		 		"mtitle"=>'Laporan Cetak KK',
		 		"my_url"=>'Laporan_kk',
		 		"type_tgl"=>'Cetak',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>'No Kec',
		 		"kode_wil"=>'Nama Kecamatan'
    		);
    		}
			return view('Laporan/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}

	 public function Grafik_kk(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 54;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->tanggal != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$title_wil = '';
			if($no_kec == 0){
				$no_wil = 'No Kec';
				$kode_wil = 'Nama Kecamatan';
			}else{
				$no_wil = 'No Kel';
				$kode_wil = 'Nama Kelurahan';
			}
			if($no_kec != 0 && $no_kel == 0){
				$title_wil = ' Kecamatan '.ucfirst(strtolower(Shr::do_get_nama_kec($no_kec)));
			}else if($no_kec != 0 && $no_kel != 0){
				$title_wil = ' Kelurahan '.ucfirst(strtolower(Shr::do_get_nama_kel($no_kec,$no_kel)));
			}
			$r = $this->get_data_kk($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'Grafik Cetak KK'.$title_wil.' Tanggal '.$tgl,
		 		"mtitle"=>'Grafik Cetak KK',
		 		"my_url"=>'Grafik_kk',
		 		"type_tgl"=>'Cetak',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>$no_wil,
		 		"kode_wil"=>$kode_wil
    		);
			}else{
			$data = array(
		 		"stitle"=>'Grafik Cetak KK',
		 		"mtitle"=>'Grafik Cetak KK',
		 		"my_url"=>'Grafik_kk',
		 		"type_tgl"=>'Cetak',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>'No Kec',
		 		"kode_wil"=>'Nama Kecamatan'
    		);
    		}
			return view('Grafik/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}

	 public function Laporan_kia(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 23;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->tanggal != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$title_wil = '';
			if($no_kec == 0){
				$no_wil = 'No Kec';
				$kode_wil = 'Nama Kecamatan';
			}else{
				$no_wil = 'No Kel';
				$kode_wil = 'Nama Kelurahan';
			}
			if($no_kec != 0 && $no_kel == 0){
				$title_wil = ' Kecamatan '.ucfirst(strtolower(Shr::do_get_nama_kec($no_kec)));
			}else if($no_kec != 0 && $no_kel != 0){
				$title_wil = ' Kelurahan '.ucfirst(strtolower(Shr::do_get_nama_kel($no_kec,$no_kel)));
			}
			$r = $this->get_data_kia($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'Laporan Cetak kia'.$title_wil.' Tanggal '.$tgl,
		 		"mtitle"=>'Laporan Cetak kia',
		 		"my_url"=>'Laporan_kia',
		 		"type_tgl"=>'Cetak',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>$no_wil,
		 		"kode_wil"=>$kode_wil
    		);
			}else{
			$data = array(
		 		"stitle"=>'Laporan Cetak kia',
		 		"mtitle"=>'Laporan Cetak kia',
		 		"my_url"=>'Laporan_kia',
		 		"type_tgl"=>'Cetak',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>'No Kec',
		 		"kode_wil"=>'Nama Kecamatan'
    		);
    		}
			return view('Laporan/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}

	 public function Grafik_kia(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 56;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->tanggal != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$title_wil = '';
			if($no_kec == 0){
				$no_wil = 'No Kec';
				$kode_wil = 'Nama Kecamatan';
			}else{
				$no_wil = 'No Kel';
				$kode_wil = 'Nama Kelurahan';
			}
			if($no_kec != 0 && $no_kel == 0){
				$title_wil = ' Kecamatan '.ucfirst(strtolower(Shr::do_get_nama_kec($no_kec)));
			}else if($no_kec != 0 && $no_kel != 0){
				$title_wil = ' Kelurahan '.ucfirst(strtolower(Shr::do_get_nama_kel($no_kec,$no_kel)));
			}
			$r = $this->get_data_kia($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'Grafik Cetak kia'.$title_wil.' Tanggal '.$tgl,
		 		"mtitle"=>'Grafik Cetak kia',
		 		"my_url"=>'Grafik_kia',
		 		"type_tgl"=>'Cetak',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>$no_wil,
		 		"kode_wil"=>$kode_wil
    		);
			}else{
			$data = array(
		 		"stitle"=>'Grafik Cetak kia',
		 		"mtitle"=>'Grafik Cetak kia',
		 		"my_url"=>'Grafik_kia',
		 		"type_tgl"=>'Cetak',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>'No Kec',
		 		"kode_wil"=>'Nama Kecamatan'
    		);
    		}
			return view('Grafik/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}

	 public function Laporan_biodata(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 22;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->tanggal != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$title_wil = '';
			if($no_kec == 0){
				$no_wil = 'No Kec';
				$kode_wil = 'Nama Kecamatan';
			}else{
				$no_wil = 'No Kel';
				$kode_wil = 'Nama Kelurahan';
			}
			if($no_kec != 0 && $no_kel == 0){
				$title_wil = ' Kecamatan '.ucfirst(strtolower(Shr::do_get_nama_kec($no_kec)));
			}else if($no_kec != 0 && $no_kel != 0){
				$title_wil = ' Kelurahan '.ucfirst(strtolower(Shr::do_get_nama_kel($no_kec,$no_kel)));
			}
			$r = $this->get_data_bio($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'Laporan Input Biodata'.$title_wil.' Tanggal '.$tgl,
		 		"mtitle"=>'Laporan Input Biodata',
		 		"my_url"=>'Laporan_biodata',
		 		"type_tgl"=>'Cetak',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>$no_wil,
		 		"kode_wil"=>$kode_wil
    		);
			}else{
			$data = array(
		 		"stitle"=>'Laporan Input Biodata',
		 		"mtitle"=>'Laporan Input Biodata',
		 		"my_url"=>'Laporan_biodata',
		 		"type_tgl"=>'Cetak',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>'No Kec',
		 		"kode_wil"=>'Nama Kecamatan'
    		);
    		}
			return view('Laporan/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}

	 public function Grafik_biodata(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 55;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->tanggal != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$title_wil = '';
			if($no_kec == 0){
				$no_wil = 'No Kec';
				$kode_wil = 'Nama Kecamatan';
			}else{
				$no_wil = 'No Kel';
				$kode_wil = 'Nama Kelurahan';
			}
			if($no_kec != 0 && $no_kel == 0){
				$title_wil = ' Kecamatan '.ucfirst(strtolower(Shr::do_get_nama_kec($no_kec)));
			}else if($no_kec != 0 && $no_kel != 0){
				$title_wil = ' Kelurahan '.ucfirst(strtolower(Shr::do_get_nama_kel($no_kec,$no_kel)));
			}
			$r = $this->get_data_bio($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'Grafik Input Biodata'.$title_wil.' Tanggal '.$tgl,
		 		"mtitle"=>'Grafik Input Biodata',
		 		"my_url"=>'Grafik_biodata',
		 		"type_tgl"=>'Cetak',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>$no_wil,
		 		"kode_wil"=>$kode_wil
    		);
			}else{
			$data = array(
		 		"stitle"=>'Grafik Input Biodata',
		 		"mtitle"=>'Grafik Input Biodata',
		 		"my_url"=>'Grafik_biodata',
		 		"type_tgl"=>'Cetak',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>'No Kec',
		 		"kode_wil"=>'Nama Kecamatan'
    		);
    		}
			return view('Grafik/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}

	 public function Laporan_surat_pindah(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 24;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->tanggal != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$title_wil = '';
			if($no_kec == 0){
				$no_wil = 'No Kec';
				$kode_wil = 'Nama Kecamatan';
			}else{
				$no_wil = 'No Kel';
				$kode_wil = 'Nama Kelurahan';
			}
			if($no_kec != 0 && $no_kel == 0){
				$title_wil = ' Kecamatan '.ucfirst(strtolower(Shr::do_get_nama_kec($no_kec)));
			}else if($no_kec != 0 && $no_kel != 0){
				$title_wil = ' Kelurahan '.ucfirst(strtolower(Shr::do_get_nama_kel($no_kec,$no_kel)));
			}
			$r = $this->get_data_surat_pindah($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'Laporan Surat Pindah'.$title_wil.' Tanggal '.$tgl,
		 		"mtitle"=>'Laporan surat pindah',
		 		"my_url"=>'Laporan_surat_pindah',
		 		"type_tgl"=>'Entri',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>$no_wil,
		 		"kode_wil"=>$kode_wil,
		 		"is_mobilitas"=>"Y"
    		);
			}else{
			$data = array(
		 		"stitle"=>'Laporan Surat Pindah',
		 		"mtitle"=>'Laporan Surat Pindah',
		 		"my_url"=>'Laporan_surat_pindah',
		 		"type_tgl"=>'Entri',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>'No Kec',
		 		"kode_wil"=>'Nama Kecamatan',
		 		"is_mobilitas"=>"Y"
    		);
    		}
			return view('Laporan/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}

	 public function Grafik_surat_pindah(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 57;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->tanggal != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$title_wil = '';
			if($no_kec == 0){
				$no_wil = 'No Kec';
				$kode_wil = 'Nama Kecamatan';
			}else{
				$no_wil = 'No Kel';
				$kode_wil = 'Nama Kelurahan';
			}
			if($no_kec != 0 && $no_kel == 0){
				$title_wil = ' Kecamatan '.ucfirst(strtolower(Shr::do_get_nama_kec($no_kec)));
			}else if($no_kec != 0 && $no_kel != 0){
				$title_wil = ' Kelurahan '.ucfirst(strtolower(Shr::do_get_nama_kel($no_kec,$no_kel)));
			}
			$r = $this->get_data_surat_pindah($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'Grafik Surat Pindah'.$title_wil.' Tanggal '.$tgl,
		 		"mtitle"=>'Grafik surat pindah',
		 		"my_url"=>'Grafik_surat_pindah',
		 		"type_tgl"=>'Entri',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>$no_wil,
		 		"kode_wil"=>$kode_wil,
		 		"is_mobilitas"=>"Y"
    		);
			}else{
			$data = array(
		 		"stitle"=>'Grafik Surat Pindah',
		 		"mtitle"=>'Grafik Surat Pindah',
		 		"my_url"=>'Grafik_surat_pindah',
		 		"type_tgl"=>'Entri',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>'No Kec',
		 		"kode_wil"=>'Nama Kecamatan',
		 		"is_mobilitas"=>"Y"
    		);
    		}
			return view('Grafik/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}

	 public function Laporan_surat_pindah_kec(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 25;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->tanggal != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$title_wil = '';
			if($no_kec == 0){
				$no_wil = 'No Kec';
				$kode_wil = 'Nama Kecamatan';
			}else{
				$no_wil = 'No Kel';
				$kode_wil = 'Nama Kelurahan';
			}
			if($no_kec != 0 && $no_kel == 0){
				$title_wil = ' Kecamatan '.ucfirst(strtolower(Shr::do_get_nama_kec($no_kec)));
			}else if($no_kec != 0 && $no_kel != 0){
				$title_wil = ' Kelurahan '.ucfirst(strtolower(Shr::do_get_nama_kel($no_kec,$no_kel)));
			}
			$r = $this->get_data_surat_pindah_kec($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'Laporan Surat Pindah Antar Kecamatan'.$title_wil.' Tanggal '.$tgl,
		 		"mtitle"=>'Laporan surat pindah Antar Kecamatan',
		 		"my_url"=>'Laporan_surat_pindah_kec',
		 		"type_tgl"=>'Entri',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>$no_wil,
		 		"kode_wil"=>$kode_wil,
		 		"is_mobilitas"=>"Y"
    		);
			}else{
			$data = array(
		 		"stitle"=>'Laporan Surat Pindah Antar Kecamatan',
		 		"mtitle"=>'Laporan Surat Pindah Antar Kecamatan',
		 		"my_url"=>'Laporan_surat_pindah_kec',
		 		"type_tgl"=>'Entri',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>'No Kec',
		 		"kode_wil"=>'Nama Kecamatan',
		 		"is_mobilitas"=>"Y"
    		);
    		}
			return view('Laporan/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}

	 public function Grafik_surat_pindah_kec(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 58;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->tanggal != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$title_wil = '';
			if($no_kec == 0){
				$no_wil = 'No Kec';
				$kode_wil = 'Nama Kecamatan';
			}else{
				$no_wil = 'No Kel';
				$kode_wil = 'Nama Kelurahan';
			}
			if($no_kec != 0 && $no_kel == 0){
				$title_wil = ' Kecamatan '.ucfirst(strtolower(Shr::do_get_nama_kec($no_kec)));
			}else if($no_kec != 0 && $no_kel != 0){
				$title_wil = ' Kelurahan '.ucfirst(strtolower(Shr::do_get_nama_kel($no_kec,$no_kel)));
			}
			$r = $this->get_data_surat_pindah_kec($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'Grafik Surat Pindah Antar Kecamatan'.$title_wil.' Tanggal '.$tgl,
		 		"mtitle"=>'Grafik surat pindah Antar Kecamatan',
		 		"my_url"=>'Grafik_surat_pindah_kec',
		 		"type_tgl"=>'Entri',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>$no_wil,
		 		"kode_wil"=>$kode_wil,
		 		"is_mobilitas"=>"Y"
    		);
			}else{
			$data = array(
		 		"stitle"=>'Grafik Surat Pindah Antar Kecamatan',
		 		"mtitle"=>'Grafik Surat Pindah Antar Kecamatan',
		 		"my_url"=>'Grafik_surat_pindah_kec',
		 		"type_tgl"=>'Entri',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>'No Kec',
		 		"kode_wil"=>'Nama Kecamatan',
		 		"is_mobilitas"=>"Y"
    		);
    		}
			return view('Grafik/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}

	 public function Laporan_surat_pindah_kel(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 26;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->tanggal != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$title_wil = '';
			if($no_kec == 0){
				$no_wil = 'No Kec';
				$kode_wil = 'Nama Kecamatan';
			}else{
				$no_wil = 'No Kel';
				$kode_wil = 'Nama Kelurahan';
			}
			if($no_kec != 0 && $no_kel == 0){
				$title_wil = ' Kecamatan '.ucfirst(strtolower(Shr::do_get_nama_kec($no_kec)));
			}else if($no_kec != 0 && $no_kel != 0){
				$title_wil = ' Kelurahan '.ucfirst(strtolower(Shr::do_get_nama_kel($no_kec,$no_kel)));
			}
			$r = $this->get_data_surat_pindah_kel($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'Laporan Surat Pindah Satu Kecamatan'.$title_wil.' Tanggal '.$tgl,
		 		"mtitle"=>'Laporan surat pindah Satu Kecamatan',
		 		"my_url"=>'Laporan_surat_pindah_kel',
		 		"type_tgl"=>'Entri',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>$no_wil,
		 		"kode_wil"=>$kode_wil,
		 		"is_mobilitas"=>"Y"
    		);
			}else{
			$data = array(
		 		"stitle"=>'Laporan Surat Pindah Satu Kecamatan',
		 		"mtitle"=>'Laporan Surat Pindah Satu Kecamatan',
		 		"my_url"=>'Laporan_surat_pindah_kel',
		 		"type_tgl"=>'Entri',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>'No Kec',
		 		"kode_wil"=>'Nama Kecamatan',
		 		"is_mobilitas"=>"Y"
    		);
    		}
			return view('Laporan/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}

	 public function Grafik_surat_pindah_kel(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 59;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->tanggal != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$title_wil = '';
			if($no_kec == 0){
				$no_wil = 'No Kec';
				$kode_wil = 'Nama Kecamatan';
			}else{
				$no_wil = 'No Kel';
				$kode_wil = 'Nama Kelurahan';
			}
			if($no_kec != 0 && $no_kel == 0){
				$title_wil = ' Kecamatan '.ucfirst(strtolower(Shr::do_get_nama_kec($no_kec)));
			}else if($no_kec != 0 && $no_kel != 0){
				$title_wil = ' Kelurahan '.ucfirst(strtolower(Shr::do_get_nama_kel($no_kec,$no_kel)));
			}
			$r = $this->get_data_surat_pindah_kel($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'Grafik Surat Pindah Satu Kecamatan'.$title_wil.' Tanggal '.$tgl,
		 		"mtitle"=>'Grafik surat pindah Satu Kecamatan',
		 		"my_url"=>'Grafik_surat_pindah_kel',
		 		"type_tgl"=>'Entri',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>$no_wil,
		 		"kode_wil"=>$kode_wil,
		 		"is_mobilitas"=>"Y"
    		);
			}else{
			$data = array(
		 		"stitle"=>'Grafik Surat Pindah Satu Kecamatan',
		 		"mtitle"=>'Grafik Surat Pindah Satu Kecamatan',
		 		"my_url"=>'Grafik_surat_pindah_kel',
		 		"type_tgl"=>'Entri',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>'No Kec',
		 		"kode_wil"=>'Nama Kecamatan',
		 		"is_mobilitas"=>"Y"
    		);
    		}
			return view('Grafik/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}

	 public function Laporan_surat_datang(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 27;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->tanggal != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$title_wil = '';
			if($no_kec == 0){
				$no_wil = 'No Kec';
				$kode_wil = 'Nama Kecamatan';
			}else{
				$no_wil = 'No Kel';
				$kode_wil = 'Nama Kelurahan';
			}
			if($no_kec != 0 && $no_kel == 0){
				$title_wil = ' Kecamatan '.ucfirst(strtolower(Shr::do_get_nama_kec($no_kec)));
			}else if($no_kec != 0 && $no_kel != 0){
				$title_wil = ' Kelurahan '.ucfirst(strtolower(Shr::do_get_nama_kel($no_kec,$no_kel)));
			}
			$r = $this->get_data_surat_datang($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'Laporan Surat Datang'.$title_wil.' Tanggal '.$tgl,
		 		"mtitle"=>'Laporan Surat Datang',
		 		"my_url"=>'Laporan_surat_datang',
		 		"type_tgl"=>'Cetak',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>$no_wil,
		 		"kode_wil"=>$kode_wil,
		 		"is_mobilitas"=>"Y"
    		);
			}else{
			$data = array(
		 		"stitle"=>'Laporan Surat Datang',
		 		"mtitle"=>'Laporan Surat Datang',
		 		"my_url"=>'Laporan_surat_datang',
		 		"type_tgl"=>'Cetak',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>'No Kec',
		 		"kode_wil"=>'Nama Kecamatan',
		 		"is_mobilitas"=>"Y"
    		);
    		}
			return view('Laporan/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}

	 public function Grafik_surat_datang(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 60;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->tanggal != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$title_wil = '';
			if($no_kec == 0){
				$no_wil = 'No Kec';
				$kode_wil = 'Nama Kecamatan';
			}else{
				$no_wil = 'No Kel';
				$kode_wil = 'Nama Kelurahan';
			}
			if($no_kec != 0 && $no_kel == 0){
				$title_wil = ' Kecamatan '.ucfirst(strtolower(Shr::do_get_nama_kec($no_kec)));
			}else if($no_kec != 0 && $no_kel != 0){
				$title_wil = ' Kelurahan '.ucfirst(strtolower(Shr::do_get_nama_kel($no_kec,$no_kel)));
			}
			$r = $this->get_data_surat_datang($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'Grafik Surat Datang'.$title_wil.' Tanggal '.$tgl,
		 		"mtitle"=>'Grafik Surat Datang',
		 		"my_url"=>'Grafik_surat_datang',
		 		"type_tgl"=>'Cetak',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>$no_wil,
		 		"kode_wil"=>$kode_wil,
		 		"is_mobilitas"=>"Y"
    		);
			}else{
			$data = array(
		 		"stitle"=>'Grafik Surat Datang',
		 		"mtitle"=>'Grafik Surat Datang',
		 		"my_url"=>'Grafik_surat_datang',
		 		"type_tgl"=>'Cetak',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>'No Kec',
		 		"kode_wil"=>'Nama Kecamatan',
		 		"is_mobilitas"=>"Y"
    		);
    		}
			return view('Grafik/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}

	 public function Laporan_surat_datang_kec(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 28;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->tanggal != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$title_wil = '';
			if($no_kec == 0){
				$no_wil = 'No Kec';
				$kode_wil = 'Nama Kecamatan';
			}else{
				$no_wil = 'No Kel';
				$kode_wil = 'Nama Kelurahan';
			}
			if($no_kec != 0 && $no_kel == 0){
				$title_wil = ' Kecamatan '.ucfirst(strtolower(Shr::do_get_nama_kec($no_kec)));
			}else if($no_kec != 0 && $no_kel != 0){
				$title_wil = ' Kelurahan '.ucfirst(strtolower(Shr::do_get_nama_kel($no_kec,$no_kel)));
			}
			$r = $this->get_data_surat_datang_kec($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'Laporan Surat Datang Antar Kecamatan'.$title_wil.' Tanggal '.$tgl,
		 		"mtitle"=>'Laporan Surat Datang Antar Kecamatan',
		 		"my_url"=>'Laporan_surat_datang_kec',
		 		"type_tgl"=>'Cetak',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>$no_wil,
		 		"kode_wil"=>$kode_wil,
		 		"is_mobilitas"=>"Y"
    		);
			}else{
			$data = array(
		 		"stitle"=>'Laporan Surat Datang Antar Kecamatan',
		 		"mtitle"=>'Laporan Surat Datang Antar Kecamatan',
		 		"my_url"=>'Laporan_surat_datang_kec',
		 		"type_tgl"=>'Cetak',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>'No Kec',
		 		"kode_wil"=>'Nama Kecamatan',
		 		"is_mobilitas"=>"Y"
    		);
    		}
			return view('Laporan/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}

	 public function Grafik_surat_datang_kec(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 61;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->tanggal != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$title_wil = '';
			if($no_kec == 0){
				$no_wil = 'No Kec';
				$kode_wil = 'Nama Kecamatan';
			}else{
				$no_wil = 'No Kel';
				$kode_wil = 'Nama Kelurahan';
			}
			if($no_kec != 0 && $no_kel == 0){
				$title_wil = ' Kecamatan '.ucfirst(strtolower(Shr::do_get_nama_kec($no_kec)));
			}else if($no_kec != 0 && $no_kel != 0){
				$title_wil = ' Kelurahan '.ucfirst(strtolower(Shr::do_get_nama_kel($no_kec,$no_kel)));
			}
			$r = $this->get_data_surat_datang_kec($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'Grafik Surat Datang Antar Kecamatan'.$title_wil.' Tanggal '.$tgl,
		 		"mtitle"=>'Grafik Surat Datang Antar Kecamatan',
		 		"my_url"=>'Grafik_surat_datang_kec',
		 		"type_tgl"=>'Cetak',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>$no_wil,
		 		"kode_wil"=>$kode_wil,
		 		"is_mobilitas"=>"Y"
    		);
			}else{
			$data = array(
		 		"stitle"=>'Grafik Surat Datang Antar Kecamatan',
		 		"mtitle"=>'Grafik Surat Datang Antar Kecamatan',
		 		"my_url"=>'Grafik_surat_datang_kec',
		 		"type_tgl"=>'Cetak',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>'No Kec',
		 		"kode_wil"=>'Nama Kecamatan',
		 		"is_mobilitas"=>"Y"
    		);
    		}
			return view('Grafik/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}

	 public function Laporan_surat_datang_kel(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 29;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->tanggal != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$title_wil = '';
			if($no_kec == 0){
				$no_wil = 'No Kec';
				$kode_wil = 'Nama Kecamatan';
			}else{
				$no_wil = 'No Kel';
				$kode_wil = 'Nama Kelurahan';
			}
			if($no_kec != 0 && $no_kel == 0){
				$title_wil = ' Kecamatan '.ucfirst(strtolower(Shr::do_get_nama_kec($no_kec)));
			}else if($no_kec != 0 && $no_kel != 0){
				$title_wil = ' Kelurahan '.ucfirst(strtolower(Shr::do_get_nama_kel($no_kec,$no_kel)));
			}
			$r = $this->get_data_surat_datang_kel($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'Laporan Surat Datang Satu Kecamatan'.$title_wil.' Tanggal '.$tgl,
		 		"mtitle"=>'Laporan Surat Datang Satu Kecamatan',
		 		"my_url"=>'Laporan_surat_datang_kel',
		 		"type_tgl"=>'Cetak',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>$no_wil,
		 		"kode_wil"=>$kode_wil,
		 		"is_mobilitas"=>"Y"
    		);
			}else{
			$data = array(
		 		"stitle"=>'Laporan Surat Datang Satu Kecamatan',
		 		"mtitle"=>'Laporan Surat Datang Satu Kecamatan',
		 		"my_url"=>'Laporan_surat_datang_kel',
		 		"type_tgl"=>'Cetak',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>'No Kec',
		 		"kode_wil"=>'Nama Kecamatan',
		 		"is_mobilitas"=>"Y"
    		);
    		}
			return view('Laporan/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}

	 public function Grafik_surat_datang_kel(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 62;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->tanggal != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$title_wil = '';
			if($no_kec == 0){
				$no_wil = 'No Kec';
				$kode_wil = 'Nama Kecamatan';
			}else{
				$no_wil = 'No Kel';
				$kode_wil = 'Nama Kelurahan';
			}
			if($no_kec != 0 && $no_kel == 0){
				$title_wil = ' Kecamatan '.ucfirst(strtolower(Shr::do_get_nama_kec($no_kec)));
			}else if($no_kec != 0 && $no_kel != 0){
				$title_wil = ' Kelurahan '.ucfirst(strtolower(Shr::do_get_nama_kel($no_kec,$no_kel)));
			}
			$r = $this->get_data_surat_datang_kel($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'Grafik Surat Datang Satu Kecamatan'.$title_wil.' Tanggal '.$tgl,
		 		"mtitle"=>'Grafik Surat Datang Satu Kecamatan',
		 		"my_url"=>'Grafik_surat_datang_kel',
		 		"type_tgl"=>'Cetak',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>$no_wil,
		 		"kode_wil"=>$kode_wil,
		 		"is_mobilitas"=>"Y"
    		);
			}else{
			$data = array(
		 		"stitle"=>'Grafik Surat Datang Satu Kecamatan',
		 		"mtitle"=>'Grafik Surat Datang Satu Kecamatan',
		 		"my_url"=>'Grafik_surat_datang_kel',
		 		"type_tgl"=>'Cetak',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>'No Kec',
		 		"kode_wil"=>'Nama Kecamatan',
		 		"is_mobilitas"=>"Y"
    		);
    		}
			return view('Grafik/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function Laporan_kelahiran(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 31;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->tanggal != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$jns_lap = $request->jns_lap;
			$usia_lap = $request->usia_lap;
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$title_wil = '';
			if($no_kec == 0){
				$no_wil = 'No Kec';
				$kode_wil = 'Nama Kecamatan';
			}else{
				$no_wil = 'No Kel';
				$kode_wil = 'Nama Kelurahan';
			}
			$title_lap = '';
			if($jns_lap == 1){
					if($usia_lap == 1){
						$title_lap = ' LU 0-18';
					}else if ($usia_lap == 2){
						$title_lap = ' LU DI ATAS 18';
					}else{
						$title_lap = ' LU';
					}
			}else if ($jns_lap == 2){
				if($usia_lap == 1){
						$title_lap = ' LT 0-18';
					}else if ($usia_lap == 2){
						$title_lap = ' LT DI ATAS 18';
					}else{
						$title_lap = ' LT';
					}
			}else{
				if($usia_lap == 1){
						$title_lap = ' 0-18';
					}else if ($usia_lap == 2){
						$title_lap = ' DI ATAS 18';
					}
			}
			if($no_kec != 0 && $no_kel == 0){
				$title_wil = ' Kecamatan '.ucfirst(strtolower(Shr::do_get_nama_kec($no_kec)));
			}else if($no_kec != 0 && $no_kel != 0){
				$title_wil = ' Kelurahan '.ucfirst(strtolower(Shr::do_get_nama_kel($no_kec,$no_kel)));
			}
			$r = $this->get_data_kelahiran($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel,$jns_lap,$usia_lap);
			$data = array(
		 		"stitle"=>'Laporan Akta Kelahiran'.$title_lap.''.$title_wil.' Tanggal '.$tgl,
		 		"mtitle"=>'Laporan Akta Kelahiran',
		 		"my_url"=>'Laporan_kelahiran',
		 		"type_tgl"=>'Penerbitan',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>$no_wil,
		 		"kode_wil"=>$kode_wil
    		);
			}else{
			$data = array(
		 		"stitle"=>'Laporan Akta Kelahiran',
		 		"mtitle"=>'Laporan Akta Kelahiran',
		 		"my_url"=>'Laporan_kelahiran',
		 		"type_tgl"=>'Penerbitan',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>'No Kec',
		 		"kode_wil"=>'Nama Kecamatan'
    		);
    		}
			return view('Laporan_kelahiran/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function Grafik_kelahiran(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 64;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->tanggal != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$jns_lap = $request->jns_lap;
			$usia_lap = $request->usia_lap;
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$title_wil = '';
			if($no_kec == 0){
				$no_wil = 'No Kec';
				$kode_wil = 'Nama Kecamatan';
			}else{
				$no_wil = 'No Kel';
				$kode_wil = 'Nama Kelurahan';
			}
			$title_lap = '';
			if($jns_lap == 1){
					if($usia_lap == 1){
						$title_lap = ' LU 0-18';
					}else if ($usia_lap == 2){
						$title_lap = ' LU DI ATAS 18';
					}else{
						$title_lap = ' LU';
					}
			}else if ($jns_lap == 2){
				if($usia_lap == 1){
						$title_lap = ' LT 0-18';
					}else if ($usia_lap == 2){
						$title_lap = ' LT DI ATAS 18';
					}else{
						$title_lap = ' LT';
					}
			}else{
				if($usia_lap == 1){
						$title_lap = ' 0-18';
					}else if ($usia_lap == 2){
						$title_lap = ' DI ATAS 18';
					}
			}
			if($no_kec != 0 && $no_kel == 0){
				$title_wil = ' Kecamatan '.ucfirst(strtolower(Shr::do_get_nama_kec($no_kec)));
			}else if($no_kec != 0 && $no_kel != 0){
				$title_wil = ' Kelurahan '.ucfirst(strtolower(Shr::do_get_nama_kel($no_kec,$no_kel)));
			}
			$r = $this->get_data_kelahiran($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel,$jns_lap,$usia_lap);
			$data = array(
		 		"stitle"=>'Grafik Akta Kelahiran'.$title_lap.''.$title_wil.' Tanggal '.$tgl,
		 		"mtitle"=>'Grafik Akta Kelahiran',
		 		"my_url"=>'Grafik_kelahiran',
		 		"type_tgl"=>'Penerbitan',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>$no_wil,
		 		"kode_wil"=>$kode_wil
    		);
			}else{
			$data = array(
		 		"stitle"=>'Grafik Akta Kelahiran',
		 		"mtitle"=>'Grafik Akta Kelahiran',
		 		"my_url"=>'Grafik_kelahiran',
		 		"type_tgl"=>'Penerbitan',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>'No Kec',
		 		"kode_wil"=>'Nama Kecamatan'
    		);
    		}
			return view('Grafik_kelahiran/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function Laporan_perceraian(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 34;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->tanggal != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = 0;
			$no_kel = 0;
			$title_wil = '';
			$no_wil = 'No';
			$kode_wil = 'Sebab Perceraian';
			$r = $this->get_data_perceraian($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'Laporan Akta Perceraian'.$title_wil.' Tanggal '.$tgl,
		 		"mtitle"=>'Laporan Akta Perceraian',
		 		"my_url"=>'Laporan_perceraian',
		 		"type_tgl"=>'Penerbitan',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>$no_wil,
		 		"kode_wil"=>$kode_wil,
		 		"is_colnotwil"=>'Y'
    		);
			}else{
			$data = array(
		 		"stitle"=>'Laporan Akta Perceraian',
		 		"mtitle"=>'Laporan Akta Perceraian',
		 		"my_url"=>'Laporan_perceraian',
		 		"type_tgl"=>'Penerbitan',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>'No',
		 		"kode_wil"=>'Sebab Perceraian',
		 		"is_colnotwil"=>'Y'
    		);
    		}
			return view('Laporan/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function Grafik_perceraian(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 67;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->tanggal != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = 0;
			$no_kel = 0;
			$title_wil = '';
			$no_wil = 'No';
			$kode_wil = 'Sebab Perceraian';
			$r = $this->get_data_perceraian($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'Grafik Akta Perceraian'.$title_wil.' Tanggal '.$tgl,
		 		"mtitle"=>'Grafik Akta Perceraian',
		 		"my_url"=>'Grafik_perceraian',
		 		"type_tgl"=>'Penerbitan',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>$no_wil,
		 		"kode_wil"=>$kode_wil,
		 		"is_colnotwil"=>'Y'
    		);
			}else{
			$data = array(
		 		"stitle"=>'Grafik Akta Perceraian',
		 		"mtitle"=>'Grafik Akta Perceraian',
		 		"my_url"=>'Grafik_perceraian',
		 		"type_tgl"=>'Penerbitan',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>'No',
		 		"kode_wil"=>'Sebab Perceraian',
		 		"is_colnotwil"=>'Y'
    		);
    		}
			return view('Grafik/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function Laporan_perkawinan(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 33;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->tanggal != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = 0;
			$no_kel = 0;
			$title_wil = '';
			$no_wil = 'No';
			$kode_wil = 'Agama';
			$r = $this->get_data_perkawinan($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'Laporan Akta Perkawinan'.$title_wil.' Tanggal '.$tgl,
		 		"mtitle"=>'Laporan Akta Perkawinan',
		 		"my_url"=>'Laporan_perkawinan',
		 		"type_tgl"=>'Penerbitan',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>$no_wil,
		 		"kode_wil"=>$kode_wil,
		 		"is_colnotwil"=>'Y'
    		);
			}else{
			$data = array(
		 		"stitle"=>'Laporan Akta Perkawinan',
		 		"mtitle"=>'Laporan Akta Perkawinan',
		 		"my_url"=>'Laporan_perkawinan',
		 		"type_tgl"=>'Penerbitan',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>'No',
		 		"kode_wil"=>'Agama',
		 		"is_colnotwil"=>'Y'
    		);
    		}
			return view('Laporan/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function Laporan_perkawinan_campuran(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 201;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->tanggal != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = 0;
			$no_kel = 0;
			$title_wil = '';
			$no_wil = 'No';
			$kode_wil = 'Agama';
			$r = $this->get_data_perkawinan_campuran($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'Laporan Perkawinan Campuran'.$title_wil.' Tanggal '.$tgl,
		 		"mtitle"=>'Laporan Perkawinan Campuran',
		 		"my_url"=>'KawinCampuran',
		 		"type_tgl"=>'Penerbitan',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>$no_wil,
		 		"kode_wil"=>$kode_wil,
		 		"is_colnotwil"=>'Y'
    		);
			}else{
			$data = array(
		 		"stitle"=>'Laporan Perkawinan Campuran',
		 		"mtitle"=>'Laporan Perkawinan Campuran',
		 		"my_url"=>'KawinCampuran',
		 		"type_tgl"=>'Penerbitan',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>'No',
		 		"kode_wil"=>'Agama',
		 		"is_colnotwil"=>'Y'
    		);
    		}
			return view('Laporan/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function Grafik_perkawinan(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 66;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->tanggal != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = 0;
			$no_kel = 0;
			$title_wil = '';
			$no_wil = 'No';
			$kode_wil = 'Agama';
			$r = $this->get_data_perkawinan($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'Grafik Akta Perkawinan'.$title_wil.' Tanggal '.$tgl,
		 		"mtitle"=>'Grafik Akta Perkawinan',
		 		"my_url"=>'Grafik_perkawinan',
		 		"type_tgl"=>'Penerbitan',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>$no_wil,
		 		"kode_wil"=>$kode_wil,
		 		"is_colnotwil"=>'Y'
    		);
			}else{
			$data = array(
		 		"stitle"=>'Grafik Akta Perkawinan',
		 		"mtitle"=>'Grafik Akta Perkawinan',
		 		"my_url"=>'Grafik_perkawinan',
		 		"type_tgl"=>'Penerbitan',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>'No',
		 		"kode_wil"=>'Agama',
		 		"is_colnotwil"=>'Y'
    		);
    		}
			return view('Grafik/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function Laporan_kematian(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 32;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->tanggal != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$title_wil = '';
			if($no_kec == 0){
				$no_wil = 'No Kec';
				$kode_wil = 'Nama Kecamatan';
			}else{
				$no_wil = 'No Kel';
				$kode_wil = 'Nama Kelurahan';
			}
			if($no_kec != 0 && $no_kel == 0){
				$title_wil = ' Kecamatan '.ucfirst(strtolower(Shr::do_get_nama_kec($no_kec)));
			}else if($no_kec != 0 && $no_kel != 0){
				$title_wil = ' Kelurahan '.ucfirst(strtolower(Shr::do_get_nama_kel($no_kec,$no_kel)));
			}
			$r = $this->get_data_kematian($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'Laporan Akta Kematian'.$title_wil.' Tanggal '.$tgl,
		 		"mtitle"=>'Laporan Akta Kematian',
		 		"my_url"=>'Laporan_kematian',
		 		"type_tgl"=>'Penerbitan',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>$no_wil,
		 		"kode_wil"=>$kode_wil
    		);
			}else{
			$data = array(
		 		"stitle"=>'Laporan Akta Kematian',
		 		"mtitle"=>'Laporan Akta Kematian',
		 		"my_url"=>'Laporan_kematian',
		 		"type_tgl"=>'Penerbitan',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>'No Kec',
		 		"kode_wil"=>'Nama Kecamatan'
    		);
    		}
			return view('Laporan_kematian/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function Laporan_kematian_asing(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 331;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->tanggal != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$title_wil = '';
			if($no_kec == 0){
				$no_wil = 'No Kec';
				$kode_wil = 'Nama Kecamatan';
			}else{
				$no_wil = 'No Kel';
				$kode_wil = 'Nama Kelurahan';
			}
			if($no_kec != 0 && $no_kel == 0){
				$title_wil = ' Kecamatan '.ucfirst(strtolower(Shr::do_get_nama_kec($no_kec)));
			}else if($no_kec != 0 && $no_kel != 0){
				$title_wil = ' Kelurahan '.ucfirst(strtolower(Shr::do_get_nama_kel($no_kec,$no_kel)));
			}
			$r = $this->get_data_kematian_asing($tgl,$tgl_start,$tgl_end);
			$data = array(
		 		"stitle"=>'Laporan Akta Kematian Asing'.$title_wil.' Tanggal '.$tgl,
		 		"mtitle"=>'Laporan Akta Kematian Asing',
		 		"my_url"=>'Laporan_kematian_asing',
		 		"type_tgl"=>'Penerbitan',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>$no_wil,
		 		"kode_wil"=>$kode_wil
    		);
			}else{
			$data = array(
		 		"stitle"=>'Laporan Akta Kematian Asing',
		 		"mtitle"=>'Laporan Akta Kematian Asing',
		 		"my_url"=>'Laporan_kematian_asing',
		 		"type_tgl"=>'Penerbitan',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>'No Kec',
		 		"kode_wil"=>'Nama Kecamatan'
    		);
    		}
			return view('Laporan_kematian_asing/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}

	public function Laporan_pengesahan_anak(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 205;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->tanggal != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$title_wil = '';
			if($no_kec == 0){
				$no_wil = 'No Kec';
				$kode_wil = 'Nama Kecamatan';
			}else{
				$no_wil = 'No Kel';
				$kode_wil = 'Nama Kelurahan';
			}
			if($no_kec != 0 && $no_kel == 0){
				$title_wil = ' Kecamatan '.ucfirst(strtolower(Shr::do_get_nama_kec($no_kec)));
			}else if($no_kec != 0 && $no_kel != 0){
				$title_wil = ' Kelurahan '.ucfirst(strtolower(Shr::do_get_nama_kel($no_kec,$no_kel)));
			}
			$r = $this->get_data_pengesahan_anak($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'Laporan Pengesahan Anak'.$title_wil.' Tanggal '.$tgl,
		 		"mtitle"=>'Laporan Pengesahan Anak',
		 		"my_url"=>'PengesahanAnak',
		 		"type_tgl"=>'Penerbitan',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>$no_wil,
		 		"kode_wil"=>$kode_wil
    		);
			}else{
			$data = array(
		 		"stitle"=>'Laporan Pengesahan Anak',
		 		"mtitle"=>'Laporan Pengesahan Anak',
		 		"my_url"=>'PengesahanAnak',
		 		"type_tgl"=>'Penerbitan',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>'No Kec',
		 		"kode_wil"=>'Nama Kecamatan'
    		);
    		}
			return view('Laporan/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}

	public function Laporan_perubahan_nama(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 206;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->tanggal != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$title_wil = '';
			if($no_kec == 0){
				$no_wil = 'No Kec';
				$kode_wil = 'Nama Kecamatan';
			}else{
				$no_wil = 'No Kel';
				$kode_wil = 'Nama Kelurahan';
			}
			if($no_kec != 0 && $no_kel == 0){
				$title_wil = ' Kecamatan '.ucfirst(strtolower(Shr::do_get_nama_kec($no_kec)));
			}else if($no_kec != 0 && $no_kel != 0){
				$title_wil = ' Kelurahan '.ucfirst(strtolower(Shr::do_get_nama_kel($no_kec,$no_kel)));
			}
			$r = $this->get_data_perubahan_nama($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'Laporan Perubahan Nama'.$title_wil.' Tanggal '.$tgl,
		 		"mtitle"=>'Laporan Perubahan Nama',
		 		"my_url"=>'PerubahanNama',
		 		"type_tgl"=>'Penerbitan',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>$no_wil,
		 		"kode_wil"=>$kode_wil
    		);
			}else{
			$data = array(
		 		"stitle"=>'Laporan Perubahan Nama',
		 		"mtitle"=>'Laporan Perubahan Nama',
		 		"my_url"=>'PerubahanNama',
		 		"type_tgl"=>'Penerbitan',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>'No Kec',
		 		"kode_wil"=>'Nama Kecamatan'
    		);
    		}
			return view('Laporan/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}

	public function Laporan_pengakuan_anak(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 204;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->tanggal != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$title_wil = '';
			if($no_kec == 0){
				$no_wil = 'No Kec';
				$kode_wil = 'Nama Kecamatan';
			}else{
				$no_wil = 'No Kel';
				$kode_wil = 'Nama Kelurahan';
			}
			if($no_kec != 0 && $no_kel == 0){
				$title_wil = ' Kecamatan '.ucfirst(strtolower(Shr::do_get_nama_kec($no_kec)));
			}else if($no_kec != 0 && $no_kel != 0){
				$title_wil = ' Kelurahan '.ucfirst(strtolower(Shr::do_get_nama_kel($no_kec,$no_kel)));
			}
			$r = $this->get_data_pengakuan_anak($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'Laporan Pengakuan Anak'.$title_wil.' Tanggal '.$tgl,
		 		"mtitle"=>'Laporan Pengakuan Anak',
		 		"my_url"=>'PengakuanAnak',
		 		"type_tgl"=>'Penerbitan',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>$no_wil,
		 		"kode_wil"=>$kode_wil
    		);
			}else{
			$data = array(
		 		"stitle"=>'Laporan Pengakuan Anak',
		 		"mtitle"=>'Laporan Pengakuan Anak',
		 		"my_url"=>'PengakuanAnak',
		 		"type_tgl"=>'Penerbitan',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>'No Kec',
		 		"kode_wil"=>'Nama Kecamatan'
    		);
    		}
			return view('Laporan/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}

	public function Laporan_pengangkatan_anak(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 203;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->tanggal != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$title_wil = '';
			if($no_kec == 0){
				$no_wil = 'No Kec';
				$kode_wil = 'Nama Kecamatan';
			}else{
				$no_wil = 'No Kel';
				$kode_wil = 'Nama Kelurahan';
			}
			if($no_kec != 0 && $no_kel == 0){
				$title_wil = ' Kecamatan '.ucfirst(strtolower(Shr::do_get_nama_kec($no_kec)));
			}else if($no_kec != 0 && $no_kel != 0){
				$title_wil = ' Kelurahan '.ucfirst(strtolower(Shr::do_get_nama_kel($no_kec,$no_kel)));
			}
			$r = $this->get_data_pengangkatan_anak($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'Laporan Pengangkatan Anak'.$title_wil.' Tanggal '.$tgl,
		 		"mtitle"=>'Laporan Pengangkatan Anak',
		 		"my_url"=>'PengangkatanAnak',
		 		"type_tgl"=>'Penerbitan',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>$no_wil,
		 		"kode_wil"=>$kode_wil
    		);
			}else{
			$data = array(
		 		"stitle"=>'Laporan Pengangkatan Anak',
		 		"mtitle"=>'Laporan Pengangkatan Anak',
		 		"my_url"=>'PengangkatanAnak',
		 		"type_tgl"=>'Penerbitan',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>'No Kec',
		 		"kode_wil"=>'Nama Kecamatan'
    		);
    		}
			return view('Laporan/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}

	public function Laporan_wna_wni(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 208;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->tanggal != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$title_wil = '';
			if($no_kec == 0){
				$no_wil = 'No Kec';
				$kode_wil = 'Nama Kecamatan';
			}else{
				$no_wil = 'No Kel';
				$kode_wil = 'Nama Kelurahan';
			}
			if($no_kec != 0 && $no_kel == 0){
				$title_wil = ' Kecamatan '.ucfirst(strtolower(Shr::do_get_nama_kec($no_kec)));
			}else if($no_kec != 0 && $no_kel != 0){
				$title_wil = ' Kelurahan '.ucfirst(strtolower(Shr::do_get_nama_kel($no_kec,$no_kel)));
			}
			$r = $this->get_data_wna_wni($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'Laporan Perubahan Wna Ke Wni'.$title_wil.' Tanggal '.$tgl,
		 		"mtitle"=>'Laporan Perubahan Wna Ke Wni',
		 		"my_url"=>'KonversiWnaWni',
		 		"type_tgl"=>'Penerbitan',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>$no_wil,
		 		"kode_wil"=>$kode_wil
    		);
			}else{
			$data = array(
		 		"stitle"=>'Laporan Perubahan Wna Ke Wni',
		 		"mtitle"=>'Laporan Perubahan Wna Ke Wni',
		 		"my_url"=>'KonversiWnaWni',
		 		"type_tgl"=>'Penerbitan',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>'No Kec',
		 		"kode_wil"=>'Nama Kecamatan'
    		);
    		}
			return view('Laporan/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}

	public function Laporan_wni_wna(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 207;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->tanggal != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$title_wil = '';
			if($no_kec == 0){
				$no_wil = 'No Kec';
				$kode_wil = 'Nama Kecamatan';
			}else{
				$no_wil = 'No Kel';
				$kode_wil = 'Nama Kelurahan';
			}
			if($no_kec != 0 && $no_kel == 0){
				$title_wil = ' Kecamatan '.ucfirst(strtolower(Shr::do_get_nama_kec($no_kec)));
			}else if($no_kec != 0 && $no_kel != 0){
				$title_wil = ' Kelurahan '.ucfirst(strtolower(Shr::do_get_nama_kel($no_kec,$no_kel)));
			}
			$r = $this->get_data_wni_wna($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'Laporan Perubahan Wni Ke Wna'.$title_wil.' Tanggal '.$tgl,
		 		"mtitle"=>'Laporan Perubahan Wni Ke Wna',
		 		"my_url"=>'KonversiWniWna',
		 		"type_tgl"=>'Penerbitan',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>$no_wil,
		 		"kode_wil"=>$kode_wil
    		);
			}else{
			$data = array(
		 		"stitle"=>'Laporan Perubahan Wni Ke Wna',
		 		"mtitle"=>'Laporan Perubahan Wni Ke Wna',
		 		"my_url"=>'KonversiWniWna',
		 		"type_tgl"=>'Penerbitan',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>'No Kec',
		 		"kode_wil"=>'Nama Kecamatan'
    		);
    		}
			return view('Laporan/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}

	public function Laporan_warga_ganda(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 209;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->tanggal != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$title_wil = '';
			if($no_kec == 0){
				$no_wil = 'No Kec';
				$kode_wil = 'Nama Kecamatan';
			}else{
				$no_wil = 'No Kel';
				$kode_wil = 'Nama Kelurahan';
			}
			if($no_kec != 0 && $no_kel == 0){
				$title_wil = ' Kecamatan '.ucfirst(strtolower(Shr::do_get_nama_kec($no_kec)));
			}else if($no_kec != 0 && $no_kel != 0){
				$title_wil = ' Kelurahan '.ucfirst(strtolower(Shr::do_get_nama_kel($no_kec,$no_kel)));
			}
			$r = $this->get_data_warga_ganda($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'Laporan Warga Negara Ganda'.$title_wil.' Tanggal '.$tgl,
		 		"mtitle"=>'Laporan Warga Negara Ganda',
		 		"my_url"=>'KewarganegaraanGanda',
		 		"type_tgl"=>'Penerbitan',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>$no_wil,
		 		"kode_wil"=>$kode_wil
    		);
			}else{
			$data = array(
		 		"stitle"=>'Laporan Warga Negara Ganda',
		 		"mtitle"=>'Laporan Warga Negara Ganda',
		 		"my_url"=>'KewarganegaraanGanda',
		 		"type_tgl"=>'Penerbitan',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>'No Kec',
		 		"kode_wil"=>'Nama Kecamatan'
    		);
    		}
			return view('Laporan/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}

	public function Laporan_pristiwa_penting(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 210;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->tanggal != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$title_wil = '';
			if($no_kec == 0){
				$no_wil = 'No Kec';
				$kode_wil = 'Nama Kecamatan';
			}else{
				$no_wil = 'No Kel';
				$kode_wil = 'Nama Kelurahan';
			}
			if($no_kec != 0 && $no_kel == 0){
				$title_wil = ' Kecamatan '.ucfirst(strtolower(Shr::do_get_nama_kec($no_kec)));
			}else if($no_kec != 0 && $no_kel != 0){
				$title_wil = ' Kelurahan '.ucfirst(strtolower(Shr::do_get_nama_kel($no_kec,$no_kel)));
			}
			$r = $this->get_data_pristiwa_penting($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'Laporan Pristiwa Penting'.$title_wil.' Tanggal '.$tgl,
		 		"mtitle"=>'Laporan Pristiwa Penting',
		 		"my_url"=>'KewarganegaraanGanda',
		 		"type_tgl"=>'Penerbitan',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>$no_wil,
		 		"kode_wil"=>$kode_wil
    		);
			}else{
			$data = array(
		 		"stitle"=>'Laporan Pristiwa Penting',
		 		"mtitle"=>'Laporan Pristiwa Penting',
		 		"my_url"=>'KewarganegaraanGanda',
		 		"type_tgl"=>'Penerbitan',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>'No Kec',
		 		"kode_wil"=>'Nama Kecamatan'
    		);
    		}
			return view('Laporan/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}

	public function Laporan_pembetulan_akta(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 211;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->tanggal != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$title_wil = '';
			if($no_kec == 0){
				$no_wil = 'No Kec';
				$kode_wil = 'Nama Kecamatan';
			}else{
				$no_wil = 'No Kel';
				$kode_wil = 'Nama Kelurahan';
			}
			if($no_kec != 0 && $no_kel == 0){
				$title_wil = ' Kecamatan '.ucfirst(strtolower(Shr::do_get_nama_kec($no_kec)));
			}else if($no_kec != 0 && $no_kel != 0){
				$title_wil = ' Kelurahan '.ucfirst(strtolower(Shr::do_get_nama_kel($no_kec,$no_kel)));
			}
			$r = $this->get_data_pembetulan_akta($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'Laporan Pembetulan Akta'.$title_wil.' Tanggal '.$tgl,
		 		"mtitle"=>'Laporan Pembetulan Akta',
		 		"my_url"=>'PembetulanAkta',
		 		"type_tgl"=>'Penerbitan',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>$no_wil,
		 		"kode_wil"=>$kode_wil
    		);
			}else{
			$data = array(
		 		"stitle"=>'Laporan Pembetulan Akta',
		 		"mtitle"=>'Laporan Pembetulan Akta',
		 		"my_url"=>'PembetulanAkta',
		 		"type_tgl"=>'Penerbitan',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>'No Kec',
		 		"kode_wil"=>'Nama Kecamatan'
    		);
    		}
			return view('Laporan/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}

	public function Laporan_pembatalan_akta(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 212;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->tanggal != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$title_wil = '';
			if($no_kec == 0){
				$no_wil = 'No Kec';
				$kode_wil = 'Nama Kecamatan';
			}else{
				$no_wil = 'No Kel';
				$kode_wil = 'Nama Kelurahan';
			}
			if($no_kec != 0 && $no_kel == 0){
				$title_wil = ' Kecamatan '.ucfirst(strtolower(Shr::do_get_nama_kec($no_kec)));
			}else if($no_kec != 0 && $no_kel != 0){
				$title_wil = ' Kelurahan '.ucfirst(strtolower(Shr::do_get_nama_kel($no_kec,$no_kel)));
			}
			$r = $this->get_data_pembatalan_akta($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'Laporan Pembatalan Akta'.$title_wil.' Tanggal '.$tgl,
		 		"mtitle"=>'Laporan Pembatalan Akta',
		 		"my_url"=>'PembatalanAkta',
		 		"type_tgl"=>'Penerbitan',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>$no_wil,
		 		"kode_wil"=>$kode_wil
    		);
			}else{
			$data = array(
		 		"stitle"=>'Laporan Pembatalan Akta',
		 		"mtitle"=>'Laporan Pembatalan Akta',
		 		"my_url"=>'PembatalanAkta',
		 		"type_tgl"=>'Penerbitan',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>'No Kec',
		 		"kode_wil"=>'Nama Kecamatan'
    		);
    		}
			return view('Laporan/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}

	public function Laporan_lahir_ln(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 213;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->tanggal != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$title_wil = '';
			if($no_kec == 0){
				$no_wil = 'No Kec';
				$kode_wil = 'Nama Kecamatan';
			}else{
				$no_wil = 'No Kel';
				$kode_wil = 'Nama Kelurahan';
			}
			if($no_kec != 0 && $no_kel == 0){
				$title_wil = ' Kecamatan '.ucfirst(strtolower(Shr::do_get_nama_kec($no_kec)));
			}else if($no_kec != 0 && $no_kel != 0){
				$title_wil = ' Kelurahan '.ucfirst(strtolower(Shr::do_get_nama_kel($no_kec,$no_kel)));
			}
			$r = $this->get_data_lahir_ln($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'Laporan Lahir Luar Negri'.$title_wil.' Tanggal '.$tgl,
		 		"mtitle"=>'Laporan Lahir Luar Negri',
		 		"my_url"=>'AktaLN',
		 		"type_tgl"=>'Penerbitan',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>$no_wil,
		 		"kode_wil"=>$kode_wil
    		);
			}else{
			$data = array(
		 		"stitle"=>'Laporan Lahir Luar Negri',
		 		"mtitle"=>'Laporan Lahir Luar Negri',
		 		"my_url"=>'AktaLN',
		 		"type_tgl"=>'Penerbitan',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>'No Kec',
		 		"kode_wil"=>'Nama Kecamatan'
    		);
    		}
			return view('Laporan/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}

	public function Laporan_lahir_bakak(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 213;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->tanggal != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$title_wil = '';
			if($no_kec == 0){
				$no_wil = 'No Kec';
				$kode_wil = 'Nama Kecamatan';
			}else{
				$no_wil = 'No Kel';
				$kode_wil = 'Nama Kelurahan';
			}
			if($no_kec != 0 && $no_kel == 0){
				$title_wil = ' Kecamatan '.ucfirst(strtolower(Shr::do_get_nama_kec($no_kec)));
			}else if($no_kec != 0 && $no_kel != 0){
				$title_wil = ' Kelurahan '.ucfirst(strtolower(Shr::do_get_nama_kel($no_kec,$no_kel)));
			}
			$r = $this->get_data_lahir_bakak($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'Laporan Kutipan 2x (Bakak)'.$title_wil.' Tanggal '.$tgl,
		 		"mtitle"=>'Laporan Kutipan 2x (Bakak)',
		 		"my_url"=>'Kutipan2x',
		 		"type_tgl"=>'Penerbitan',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>$no_wil,
		 		"kode_wil"=>$kode_wil
    		);
			}else{
			$data = array(
		 		"stitle"=>'Laporan Kutipan 2x (Bakak)',
		 		"mtitle"=>'Laporan Kutipan 2x (Bakak)',
		 		"my_url"=>'Kutipan2x',
		 		"type_tgl"=>'Penerbitan',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>'No Kec',
		 		"kode_wil"=>'Nama Kecamatan'
    		);
    		}
			return view('Laporan/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function Grafik_kematian(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 65;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->tanggal != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$title_wil = '';
			if($no_kec == 0){
				$no_wil = 'No Kec';
				$kode_wil = 'Nama Kecamatan';
			}else{
				$no_wil = 'No Kel';
				$kode_wil = 'Nama Kelurahan';
			}
			if($no_kec != 0 && $no_kel == 0){
				$title_wil = ' Kecamatan '.ucfirst(strtolower(Shr::do_get_nama_kec($no_kec)));
			}else if($no_kec != 0 && $no_kel != 0){
				$title_wil = ' Kelurahan '.ucfirst(strtolower(Shr::do_get_nama_kel($no_kec,$no_kel)));
			}
			$r = $this->get_data_kematian($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'Grafik Akta Kematian'.$title_wil.' Tanggal '.$tgl,
		 		"mtitle"=>'Grafik Akta Kematian',
		 		"my_url"=>'Grafik_kematian',
		 		"type_tgl"=>'Penerbitan',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>$no_wil,
		 		"kode_wil"=>$kode_wil
    		);
			}else{
			$data = array(
		 		"stitle"=>'Grafik Akta Kematian',
		 		"mtitle"=>'Grafik Akta Kematian',
		 		"my_url"=>'Grafik_kematian',
		 		"type_tgl"=>'Penerbitan',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>'No Kec',
		 		"kode_wil"=>'Nama Kecamatan'
    		);
    		}
			return view('Grafik/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function Master_laporan(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 42;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$t = Shr::do_get_dkb();
			$r = [];
			if($request->mlap != null && $request->mlap != 0){
				if($request->mdkb != null && $request->mdkb != 0){
					$getdkb = $this->all_get_dkb($request->mdkb);
					$t = $getdkb->dkb;
					$dkb_biodata = $getdkb->bio_name;
					$dkb_data_keluarga = $getdkb->kk_name;
					$dkb_cutoff = $getdkb->cutoff;
					$dkb_gisa = $getdkb->gisa;
				}else{
					$dkb_biodata = Shr::do_get_dkb_bio();
					$dkb_data_keluarga = Shr::do_get_dkb_keluarga();
					$dkb_cutoff = Shr::do_get_cut_off_date();
					$dkb_gisa = Shr::do_get_dkb_gisa();
					$mdkb = 0;
				}
			$mlap = $request->mlap;
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$title_wil = '';
			if($no_kec == 0){
				$no_wil = 'No Kec';
				$kode_wil = 'Nama Kecamatan';
			}else{
				$no_wil = 'No Kel';
				$kode_wil = 'Nama Kelurahan';
			}
			if($no_kec != 0 && $no_kel == 0){
				$title_wil = ' Kecamatan '.ucfirst(strtolower(Shr::do_get_nama_kec($no_kec)));
			}else if($no_kec != 0 && $no_kel != 0){
				$title_wil = ' Kelurahan '.ucfirst(strtolower(Shr::do_get_nama_kel($no_kec,$no_kel)));
			}
			$desc_lap = '';
			if($mlap == 1){
			$desc_lap = ' Jumlah Penduduk Menurut Jenis Kelamin';
			$r = $this->mst_get_laporan_kelamin($no_kec,$no_kel,$dkb_biodata,$dkb_cutoff,$dkb_gisa);
			}else if($mlap == 2){
			$desc_lap = ' Jumlah Penduduk Menurut Usia';
			$r = $this->mst_get_laporan_umur($no_kec,$no_kel,$dkb_biodata,$dkb_cutoff,$dkb_gisa);
			}else if($mlap == 3){
			$desc_lap = ' Jumlah Penduduk Menurut Usia Laki-Laki';
			$r = $this->mst_get_laporan_umur_laki_laki($no_kec,$no_kel,$dkb_biodata,$dkb_cutoff,$dkb_gisa);
			}else if($mlap == 4){
			$desc_lap = ' Jumlah Penduduk Menurut Usia Perempuan';
			$r = $this->mst_get_laporan_umur_perempuan($no_kec,$no_kel,$dkb_biodata,$dkb_cutoff,$dkb_gisa);
			}else if($mlap == 5){
			$desc_lap = ' Jumlah Kepala Keluarga Menurut Jenis Kelamin';
			$r = $this->mst_get_laporan_kk_kelamin($no_kec,$no_kel,$dkb_biodata,$dkb_cutoff,$dkb_gisa);
			}else if($mlap == 6){
			$desc_lap = ' Jumlah Penduduk Menurut Pendidikan';
			$r = $this->mst_get_laporan_pendidikan($no_kec,$no_kel,$dkb_biodata,$dkb_cutoff,$dkb_gisa);
			}else if($mlap == 7){
			$desc_lap = ' Jumlah Penduduk Menurut Pendidikan Laki-Laki';
			$r = $this->mst_get_laporan_pendidikan_laki_laki($no_kec,$no_kel,$dkb_biodata,$dkb_cutoff,$dkb_gisa);
			}else if($mlap == 8){
			$desc_lap = ' Jumlah Penduduk Menurut Pendidikan Perempuan';
			$r = $this->mst_get_laporan_pendidikan_perempuan($no_kec,$no_kel,$dkb_biodata,$dkb_cutoff,$dkb_gisa);
			}else if($mlap == 9){
			$desc_lap = ' Jumlah Penduduk Menurut Pekerjaan';
			$r = $this->mst_get_laporan_pekerjaan($no_kec,$no_kel,$dkb_biodata,$dkb_cutoff,$dkb_gisa);
			}else if($mlap == 10){
			$desc_lap = ' Jumlah Penduduk Menurut Pekerjaan Laki-Laki';
			$r = $this->mst_get_laporan_pekerjaan_laki_laki($no_kec,$no_kel,$dkb_biodata,$dkb_cutoff,$dkb_gisa);
			}else if($mlap == 11){
			$desc_lap = ' Jumlah Penduduk Menurut Pekerjaan Perempuan';
			$r = $this->mst_get_laporan_pekerjaan_perempuan($no_kec,$no_kel,$dkb_biodata,$dkb_cutoff,$dkb_gisa);
			}else if($mlap == 12){
			$desc_lap = ' Jumlah Penduduk Menurut Status Kawin';
			$r = $this->mst_get_laporan_status_kawin($no_kec,$no_kel,$dkb_biodata,$dkb_cutoff,$dkb_gisa);
			}else if($mlap == 13){
			$desc_lap = ' Jumlah Penduduk Menurut Status Kawin Laki-Laki';
			$r = $this->mst_get_laporan_status_kawin_laki_laki($no_kec,$no_kel,$dkb_biodata,$dkb_cutoff,$dkb_gisa);
			}else if($mlap == 14){
			$desc_lap = ' Jumlah Penduduk Menurut Status Kawin Perempuan';
			$r = $this->mst_get_laporan_status_kawin_perempuan($no_kec,$no_kel,$dkb_biodata,$dkb_cutoff,$dkb_gisa);
			}else if($mlap == 15){
			$desc_lap = ' Jumlah Penduduk Menurut Agama';
			$r = $this->mst_get_laporan_agama($no_kec,$no_kel,$dkb_biodata,$dkb_cutoff,$dkb_gisa);
			}else if($mlap == 16){
			$desc_lap = ' Jumlah Penduduk Menurut Agama Laki-Laki';
			$r = $this->mst_get_laporan_agama_laki_laki($no_kec,$no_kel,$dkb_biodata,$dkb_cutoff,$dkb_gisa);
			}else if($mlap == 17){
			$desc_lap = ' Jumlah Penduduk Menurut Agama Perempuan';
			$r = $this->mst_get_laporan_agama_perempuan($no_kec,$no_kel,$dkb_biodata,$dkb_cutoff,$dkb_gisa);
			}else if($mlap == 18){
			$desc_lap = ' Jumlah Penduduk Menurut Golongan Darah';
			$r = $this->mst_get_laporan_gol_drh($no_kec,$no_kel,$dkb_biodata,$dkb_cutoff,$dkb_gisa);
			}else if($mlap == 19){
			$desc_lap = ' Jumlah Penduduk Menurut Golongan Darah Laki-Laki';
			$r = $this->mst_get_laporan_gol_drh_laki_laki($no_kec,$no_kel,$dkb_biodata,$dkb_cutoff,$dkb_gisa);
			}else if($mlap == 20){
			$desc_lap = ' Jumlah Penduduk Menurut Golongan Darah Perempuan';
			$r = $this->mst_get_laporan_gol_drh_perempuan($no_kec,$no_kel,$dkb_biodata,$dkb_cutoff,$dkb_gisa);
			}else if($mlap == 21){
			$desc_lap = ' Jumlah Penduduk Menurut Jenis Kecacatan (Disabilitas)';
			$r = $this->mst_get_laporan_disabilitas($no_kec,$no_kel,$dkb_biodata,$dkb_cutoff,$dkb_gisa);
			}else if($mlap == 22){
			$desc_lap = ' Jumlah Penduduk Menurut Jenis Kecacatan (Disabilitas) Laki-Laki';
			$r = $this->mst_get_laporan_disabilitas_laki_laki($no_kec,$no_kel,$dkb_biodata,$dkb_cutoff,$dkb_gisa);
			}else if($mlap == 23){
			$desc_lap = ' Jumlah Penduduk Menurut Jenis Kecacatan (Disabilitas) Perempuan';
			$r = $this->mst_get_laporan_disabilitas_perempuan($no_kec,$no_kel,$dkb_biodata,$dkb_cutoff,$dkb_gisa);
			}else if($mlap == 24){
			$desc_lap = ' Jumlah Penduduk Menurut Usia Lansia';
			$r = $this->mst_get_laporan_umur_lansia($no_kec,$no_kel,$dkb_biodata,$dkb_cutoff,$dkb_gisa);
			}else if($mlap == 25){
			$desc_lap = ' Jumlah Penduduk Menurut Usia Lansia Laki-Laki';
			$r = $this->mst_get_laporan_umur_lansia_laki_laki($no_kec,$no_kel,$dkb_biodata,$dkb_cutoff,$dkb_gisa);
			}else if($mlap == 26){
			$desc_lap = ' Jumlah Penduduk Menurut Usia Lansia Perempuan';
			$r = $this->mst_get_laporan_umur_lansia_perempuan($no_kec,$no_kel,$dkb_biodata,$dkb_cutoff,$dkb_gisa);
			}else if($mlap == 27){
			$desc_lap = ' Jumlah Penduduk Menurut Wajib Ktp';
			$r = $this->mst_get_laporan_wajib_ktp($no_kec,$no_kel,$dkb_biodata,$dkb_cutoff,$dkb_gisa);
			}else if($mlap == 28){
			$desc_lap = ' Jumlah Penduduk Menurut Wajib Ktp';
			$r = $this->mst_get_laporan_non_ktp($no_kec,$no_kel,$dkb_biodata,$dkb_cutoff,$dkb_gisa);
			}else if($mlap == 29){
			$desc_lap = ' Jumlah Kepala Keluarga Menurut usia';
			$r = $this->mst_get_laporan_kk_usia($no_kec,$no_kel,$dkb_biodata,$dkb_cutoff,$dkb_gisa);
			}else if($mlap == 30){
			$desc_lap = ' Jumlah Kepala Keluarga Menurut usia Laki-Laki';
			$r = $this->mst_get_laporan_kk_usia_lk($no_kec,$no_kel,$dkb_biodata,$dkb_cutoff,$dkb_gisa);
			}else if($mlap == 31){
			$desc_lap = ' Jumlah Kepala Keluarga Menurut usia Perempuan';
			$r = $this->mst_get_laporan_kk_usia_pr($no_kec,$no_kel,$dkb_biodata,$dkb_cutoff,$dkb_gisa);
			}else if($mlap == 32){
			$desc_lap = ' Jumlah Kepala Keluarga Menurut pekerjaan';
			$r = $this->mst_get_laporan_kk_pekerjaan($no_kec,$no_kel,$dkb_biodata,$dkb_cutoff,$dkb_gisa);
			}else if($mlap == 33){
			$desc_lap = ' Jumlah Kepala Keluarga Menurut pekerjaan Laki-Laki';
			$r = $this->mst_get_laporan_kk_pekerjaan_lk($no_kec,$no_kel,$dkb_biodata,$dkb_cutoff,$dkb_gisa);
			}else if($mlap == 34){
			$desc_lap = ' Jumlah Kepala Keluarga Menurut pekerjaan Perempuan';
			$r = $this->mst_get_laporan_kk_pekerjaan_pr($no_kec,$no_kel,$dkb_biodata,$dkb_cutoff,$dkb_gisa);
			}

			$data = array(
		 		"stitle"=>'Laporan '.$t.''.$desc_lap.''.$title_wil,
		 		"mtitle"=>'Laporan '.$t,
		 		"my_url"=>'Master_laporan',
		 		"jenis_lap"=>$mlap,
		 		"data"=>$r,
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"no_wil"=>$no_wil,
		 		"kode_wil"=>$kode_wil
    		);
			}else{
			$data = array(
		 		"stitle"=>'Laporan '.$t,
		 		"mtitle"=>'Laporan '.$t,
		 		"my_url"=>'Master_laporan',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"no_wil"=>'No Kec',
		 		"kode_wil"=>'Nama Kecamatan'
    		);
    		}
			return view('Master_laporan/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function Grafik_master_laporan(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 69;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$t = Shr::do_get_dkb();
			$j = [];
			if($request->mlap != null && $request->mlap != 0){


			$mlap = $request->mlap;
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$title_wil = '';
			if($no_kec == 0){
				$no_wil = 'No Kec';
				$kode_wil = 'Nama Kecamatan';
			}else{
				$no_wil = 'No Kel';
				$kode_wil = 'Nama Kelurahan';
			}
			if($no_kec != 0 && $no_kel == 0){
				$title_wil = ' Kecamatan '.ucfirst(strtolower(Shr::do_get_nama_kec($no_kec)));
			}else if($no_kec != 0 && $no_kel != 0){
				$title_wil = ' Kelurahan '.ucfirst(strtolower(Shr::do_get_nama_kel($no_kec,$no_kel)));
			}
			$desc_lap = '';
			if($mlap == 1){
			$desc_lap = ' Jumlah Penduduk Menurut Jenis Kelamin';
			$j = $this->get_jumlah_laporan_kelamin($no_kec,$no_kel);
			}else if($mlap == 2){
			$desc_lap = ' Jumlah Penduduk Menurut Usia';
			$j = $this->get_jumlah_laporan_umur($no_kec,$no_kel);
			}else if($mlap == 3){
			$desc_lap = ' Jumlah Penduduk Menurut Usia Laki-Laki';
			$j = $this->get_jumlah_laporan_umur_laki_laki($no_kec,$no_kel);
			}else if($mlap == 4){
			$desc_lap = ' Jumlah Penduduk Menurut Usia Perempuan';
			$j = $this->get_jumlah_laporan_umur_perempuan($no_kec,$no_kel);
			}else if($mlap == 5){
			$desc_lap = ' Jumlah Kepala Keluarga Menurut Jenis Kelamin';
			$j = $this->get_jumlah_laporan_kk_kelamin($no_kec,$no_kel);
			}else if($mlap == 6){
			$desc_lap = ' Jumlah Penduduk Menurut Pendidikan';
			$j = $this->get_jumlah_laporan_pendidikan($no_kec,$no_kel);
			}else if($mlap == 7){
			$desc_lap = ' Jumlah Penduduk Menurut Pendidikan Laki-Laki';
			$j = $this->get_jumlah_laporan_pendidikan_laki_laki($no_kec,$no_kel);
			}else if($mlap == 8){
			$desc_lap = ' Jumlah Penduduk Menurut Pendidikan Perempuan';
			$j = $this->get_jumlah_laporan_pendidikan_perempuan($no_kec,$no_kel);
			}else if($mlap == 9){
			$desc_lap = ' Jumlah Penduduk Menurut Pekerjaan';
			$j = $this->get_jumlah_laporan_pekerjaan($no_kec,$no_kel);
			}else if($mlap == 10){
			$desc_lap = ' Jumlah Penduduk Menurut Pekerjaan Laki-Laki';
			$j = $this->get_jumlah_laporan_pekerjaan_laki_laki($no_kec,$no_kel);
			}else if($mlap == 11){
			$desc_lap = ' Jumlah Penduduk Menurut Pekerjaan Perempuan';
			$j = $this->get_jumlah_laporan_pekerjaan_perempuan($no_kec,$no_kel);
			}else if($mlap == 12){
			$desc_lap = ' Jumlah Penduduk Menurut Status Kawin';
			$j = $this->get_jumlah_laporan_status_kawin($no_kec,$no_kel);
			}else if($mlap == 13){
			$desc_lap = ' Jumlah Penduduk Menurut Status Kawin Laki-Laki';
			$j = $this->get_jumlah_laporan_status_kawin_laki_laki($no_kec,$no_kel);
			}else if($mlap == 14){
			$desc_lap = ' Jumlah Penduduk Menurut Status Kawin Perempuan';
			$j = $this->get_jumlah_laporan_status_kawin_perempuan($no_kec,$no_kel);
			}else if($mlap == 15){
			$desc_lap = ' Jumlah Penduduk Menurut Agama';
			$j = $this->get_jumlah_laporan_agama($no_kec,$no_kel);
			}else if($mlap == 16){
			$desc_lap = ' Jumlah Penduduk Menurut Agama Laki-Laki';
			$j = $this->get_jumlah_laporan_agama_laki_laki($no_kec,$no_kel);
			}else if($mlap == 17){
			$desc_lap = ' Jumlah Penduduk Menurut Agama Perempuan';
			$j = $this->get_jumlah_laporan_agama_perempuan($no_kec,$no_kel);
			}else if($mlap == 18){
			$desc_lap = ' Jumlah Penduduk Menurut Golongan Darah';
			$j = $this->get_jumlah_laporan_gol_drh($no_kec,$no_kel);
			}else if($mlap == 19){
			$desc_lap = ' Jumlah Penduduk Menurut Golongan Darah Laki-Laki';
			$j = $this->get_jumlah_laporan_gol_drh_laki_laki($no_kec,$no_kel);
			}else if($mlap == 20){
			$desc_lap = ' Jumlah Penduduk Menurut Golongan Darah Perempuan';
			$j = $this->get_jumlah_laporan_gol_drh_perempuan($no_kec,$no_kel);
			}else if($mlap == 21){
			$desc_lap = ' Jumlah Penduduk Menurut Jenis Kecacatan (Disabilitas)';
			$j = $this->get_jumlah_laporan_disabilitas($no_kec,$no_kel);
			}else if($mlap == 22){
			$desc_lap = ' Jumlah Penduduk Menurut Jenis Kecacatan (Disabilitas) Laki-Laki';
			$j = $this->get_jumlah_laporan_disabilitas_laki_laki($no_kec,$no_kel);
			}else if($mlap == 23){
			$desc_lap = ' Jumlah Penduduk Menurut Jenis Kecacatan (Disabilitas) Perempuan';
			$j = $this->get_jumlah_laporan_disabilitas_perempuan($no_kec,$no_kel);
			}else if($mlap == 24){
			$desc_lap = ' Jumlah Penduduk Menurut Usia Lansia';
			$j = $this->get_jumlah_laporan_umur_lansia($no_kec,$no_kel);
			}else if($mlap == 25){
			$desc_lap = ' Jumlah Penduduk Menurut Usia Lansia Laki-Laki';
			$j = $this->get_jumlah_laporan_umur_lansia_laki_laki($no_kec,$no_kel);
			}else if($mlap == 26){
			$desc_lap = ' Jumlah Penduduk Menurut Usia Lansia Perempuan';
			$j = $this->get_jumlah_laporan_umur_lansia_perempuan($no_kec,$no_kel);
			}else if($mlap == 27){
			$desc_lap = ' Jumlah Penduduk Menurut Wajib Ktp';
			$j = $this->get_jumlah_laporan_wajib_ktp($no_kec,$no_kel);
			}else if($mlap == 28){
			$desc_lap = ' Jumlah Penduduk Menurut Wajib Ktp';
			$j = $this->get_jumlah_laporan_non_ktp($no_kec,$no_kel);
			}

			$data = array(
		 		"stitle"=>'Grafik '.$t.''.$desc_lap.''.$title_wil,
		 		"mtitle"=>'Grafik '.$t,
		 		"my_url"=>'Grafik_master_laporan',
		 		"jenis_lap"=>$mlap,
		 		"jumlah"=>$j,
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"no_wil"=>$no_wil,
		 		"kode_wil"=>$kode_wil
    		);
			}else{
			$data = array(
		 		"stitle"=>'Grafik '.$t,
		 		"mtitle"=>'Grafik '.$t,
		 		"my_url"=>'Grafik_master_laporan',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"no_wil"=>'No Kec',
		 		"kode_wil"=>'Nama Kecamatan'
    		);
    		}
			return view('Grafik_master_laporan/main',$data);
			}else{
			return redirect()->route('logout');
		}
	}
	public function Master_laporan_usia(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 44;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$t = Shr::do_get_dkb();
			$r = [];
			if($request->mlap != null && $request->mlap != 0){
			$mlap = $request->mlap;
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$title_wil = '';
			if($no_kec == 0){
				$no_wil = 'No Kec';
				$kode_wil = 'Nama Kecamatan';
			}else{
				$no_wil = 'No Kel';
				$kode_wil = 'Nama Kelurahan';
			}
			if($no_kec != 0 && $no_kel == 0){
				$title_wil = ' Kecamatan '.ucfirst(strtolower(Shr::do_get_nama_kec($no_kec)));
			}else if($no_kec != 0 && $no_kel != 0){
				$title_wil = ' Kelurahan '.ucfirst(strtolower(Shr::do_get_nama_kel($no_kec,$no_kel)));
			}
			$desc_lap = '';
			if($mlap == 1){
			$desc_lap = ' Jumlah Penduduk Menurut Usia Per 5 Tahun';
			$r = $this->mlu_get_laporan_umur($no_kec,$no_kel);
			}else if($mlap == 2){
			$desc_lap = ' Jumlah Penduduk Menurut Usia Per 5 Tahun Laki-Laki';
			$r = $this->mlu_get_laporan_umur_laki_laki($no_kec,$no_kel);
			}else if($mlap == 3){
			$desc_lap = ' Jumlah Penduduk Menurut Usia Per 5 Tahun Perempuan';
			$r = $this->mlu_get_laporan_umur_perempuan($no_kec,$no_kel);
			}else if($mlap == 4){
			$desc_lap = ' Jumlah Penduduk Menurut Usia Per 10 Tahun';
			$r = $this->mlu_get_laporan_umur10($no_kec,$no_kel);
			}else if($mlap == 5){
			$desc_lap = ' Jumlah Penduduk Menurut Usia Per 10 Tahun Laki-Laki';
			$r = $this->mlu_get_laporan_umur10_laki_laki($no_kec,$no_kel);
			}else if($mlap == 6){
			$desc_lap = ' Jumlah Penduduk Menurut Usia Per 10 Tahun Perempuan';
			$r = $this->mlu_get_laporan_umur10_perempuan($no_kec,$no_kel);
			}else if($mlap == 7){
			$desc_lap = ' Jumlah Penduduk Menurut Usia Per 1 Tahun';
			$r = $this->mlu_get_laporan_umur1($no_kec,$no_kel);
			}else if($mlap == 8){
			$desc_lap = ' Jumlah Penduduk Menurut Usia Per 1 Tahun Laki-Laki';
			$r = $this->mlu_get_laporan_umur1_laki_laki($no_kec,$no_kel);
			}else if($mlap == 9){
			$desc_lap = ' Jumlah Penduduk Menurut Usia Per 1 Tahun Perempuan';
			$r = $this->mlu_get_laporan_umur1_perempuan($no_kec,$no_kel);
			}else if($mlap == 10){
			$desc_lap = ' Jumlah Penduduk Menurut Usia Sekolah';
			$r = $this->mlu_get_laporan_umur_sekolah($no_kec,$no_kel);
			}else if($mlap == 11){
			$desc_lap = ' Jumlah Penduduk Menurut Usia Sekolah Laki-Laki';
			$r = $this->mlu_get_laporan_umur_sekolah_laki_laki($no_kec,$no_kel);
			}else if($mlap == 12){
			$desc_lap = ' Jumlah Penduduk Menurut Usia Sekolah Perempuan';
			$r = $this->mlu_get_laporan_umur_sekolah_perempuan($no_kec,$no_kel);
			}else if($mlap == 13){
			$desc_lap = ' Jumlah Penduduk Menurut Usia Pemuda 16 Sampai 30 Tahun';
			$r = $this->mlu_get_laporan_umur_pemuda($no_kec,$no_kel);
			}else if($mlap == 14){
			$desc_lap = ' Jumlah Penduduk Menurut Usia Pemuda 16 Sampai 30 Tahun Per Pendidikan';
			$r = $this->mlu_get_laporan_umur_pemuda_pendidikan($no_kec,$no_kel);
			}

			$data = array(
		 		"stitle"=>'Laporan '.$t.''.$desc_lap.''.$title_wil,
		 		"mtitle"=>'Laporan '.$t,
		 		"my_url"=>'Master_laporan_usia',
		 		"jenis_lap"=>$mlap,
		 		"data"=>$r,
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"no_wil"=>$no_wil,
		 		"kode_wil"=>$kode_wil
    		);
			}else{
			$data = array(
		 		"stitle"=>'Laporan '.$t,
		 		"mtitle"=>'Laporan '.$t,
		 		"my_url"=>'Master_laporan_usia',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"no_wil"=>'No Kec',
		 		"kode_wil"=>'Nama Kecamatan'
    		);
    		}
			return view('Master_laporan_usia/main',$data);
		}else{
			return redirect()->route('logout');
		}
        
	}
	public function Master_cakupan(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 43;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$t = Shr::do_get_dkb();
			$r = [];
			$j = [];
			if($request->mlap != null && $request->mlap != 0){
			$mlap = $request->mlap;
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$title_wil = '';
			if($no_kec == 0){
				$no_wil = 'No Kec';
				$kode_wil = 'Nama Kecamatan';
			}else{
				$no_wil = 'No Kel';
				$kode_wil = 'Nama Kelurahan';
			}
			if($no_kec != 0 && $no_kel == 0){
				$title_wil = ' Kecamatan '.ucfirst(strtolower(Shr::do_get_nama_kec($no_kec)));
			}else if($no_kec != 0 && $no_kel != 0){
				$title_wil = ' Kelurahan '.ucfirst(strtolower(Shr::do_get_nama_kel($no_kec,$no_kel)));
			}
			$desc_lap = '';
			if($mlap == 1){
			$desc_lap = ' Jumlah Cakupan Kepemilikan Nik';
			$r = $this->mst_get_cakupan_nik($no_kec,$no_kel);
			}else if($mlap == 2){
			$desc_lap = ' Jumlah Cakupan Kepemilikan KK';
			$r = $this->mst_get_cakupan_kk($no_kec,$no_kel);
			}else if($mlap == 3){
			$desc_lap = ' Jumlah Cakupan Kepemilikan KIA';
			$r = $this->mst_get_cakupan_kia($no_kec,$no_kel);
			}else if($mlap == 4){
			$desc_lap = ' Jumlah Cakupan Perekaman KTP-El';
			$r = $this->mst_get_cakupan_sudah_rekam($no_kec,$no_kel);
			}else if($mlap == 5){
			$desc_lap = ' Jumlah Cakupan Pencetakan KTP-El';
			$r = $this->mst_get_cakupan_sudah_cetak($no_kec,$no_kel);
			}else if($mlap == 6){
			$desc_lap = ' Jumlah Cakupan Akta Kelahiran 0-18';
			$r = $this->mst_get_cakupan_akta_018($no_kec,$no_kel);
			}else if($mlap == 7){
			$desc_lap = ' Jumlah Cakupan Akta Kelahiran 18 Keatas';
			$r = $this->mst_get_cakupan_akta_18($no_kec,$no_kel);
			}else if($mlap == 8){
			$desc_lap = ' Jumlah Cakupan Akta Kelahiran Keseluruhan';
			$r = $this->mst_get_cakupan_akta($no_kec,$no_kel);
			}else if($mlap == 9){
			$desc_lap = ' Jumlah Cakupan Akta Perkawinan';
			$r = $this->mst_get_cakupan_perkawinan($no_kec,$no_kel);
			}else if($mlap == 10){
			$desc_lap = ' Jumlah Cakupan Akta Perkawinan Non Muslim';
			$r = $this->mst_get_cakupan_perkawinan_non_muslim($no_kec,$no_kel);
			}else if($mlap == 11){
			$desc_lap = ' Jumlah Cakupan Akta Perkawinan Muslim';
			$r = $this->mst_get_cakupan_perkawinan_muslim($no_kec,$no_kel);
			}else if($mlap == 12){
			$desc_lap = ' Jumlah Cakupan Akta Perceraian';
			$r = $this->mst_get_cakupan_perceraian($no_kec,$no_kel);
			}else if($mlap == 13){
			$desc_lap = ' Jumlah Cakupan Akta Perceraian Non Muslim';
			$r = $this->mst_get_cakupan_perceraian_non_muslim($no_kec,$no_kel);
			}else if($mlap == 14){
			$desc_lap = ' Jumlah Cakupan Akta Perceraian Muslim';
			$r = $this->mst_get_cakupan_perceraian_muslim($no_kec,$no_kel);
			}

			$data = array(
		 		"stitle"=>'Laporan '.$t.''.$desc_lap.''.$title_wil,
		 		"mtitle"=>'Laporan '.$t,
		 		"my_url"=>'Master_cakupan',
		 		"jenis_lap"=>$mlap,
		 		"data"=>$r,
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"no_wil"=>$no_wil,
		 		"kode_wil"=>$kode_wil
    		);
			}else{
			$data = array(
		 		"stitle"=>'Laporan '.$t,
		 		"mtitle"=>'Laporan '.$t,
		 		"my_url"=>'Master_cakupan',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"no_wil"=>'No Kec',
		 		"kode_wil"=>'Nama Kecamatan'
    		);
    		}
				return view('Master_cakupan/main',$data);
		}else{
			return redirect()->route('logout');
		}
        
	}
	public function Laporan_bln_rekam(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 36;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->bulan != null){
			$bln = $request->bulan;
			
			$r = $this->mst_get_bln_rekam_ktp($bln);
			$data = array(
		 		"stitle"=>'Rekapitulasi Laporan Perekaman Bulan '.$bln,
		 		"mtitle"=>'Rekapitulasi Laporan Perekaman',
		 		"my_url"=>'Laporan_bln_rekam',
		 		"type_tgl"=>'Cetak',
		 		"data"=>$r,
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"no_wil"=>'No Kec',
		 		"kode_wil"=>'Nama Kecamatan'
    		);
			}else{
			$data = array(
		 		"stitle"=>'Rekapitulasi Laporan Perekaman',
		 		"mtitle"=>'RRekapitulasi Laporan Perekaman',
		 		"my_url"=>'Laporan_bln_rekam',
		 		"type_tgl"=>'Cetak',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"no_wil"=>'No Kec',
		 		"kode_wil"=>'Nama Kecamatan'
    		);
    		}
			return view('laporan_bln/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function Laporan_bln_cetak(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 37;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->bulan != null){
			$bln = $request->bulan;
			
			$r = $this->mst_get_bln_cetak_ktp($bln);
			$data = array(
		 		"stitle"=>'Rekapitulasi Laporan Pencetakan Bulan '.$bln,
		 		"mtitle"=>'Rekapitulasi Laporan Pencetakan',
		 		"my_url"=>'Laporan_bln_cetak',
		 		"type_tgl"=>'Cetak',
		 		"data"=>$r,
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"no_wil"=>'No Kec',
		 		"kode_wil"=>'Nama Kecamatan'
    		);
			}else{
			$data = array(
		 		"stitle"=>'Rekapitulasi Laporan Pencetakan',
		 		"mtitle"=>'Rekapitulasi Laporan Pencetakan',
		 		"my_url"=>'Laporan_bln_cetak',
		 		"type_tgl"=>'Cetak',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"no_wil"=>'No Kec',
		 		"kode_wil"=>'Nama Kecamatan'
    		);
    		}
			return view('laporan_bln/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function Laporan_bln_kia(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 40;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->bulan != null){
			$bln = $request->bulan;
			
			$r = $this->mst_get_bln_kia($bln);
			$data = array(
		 		"stitle"=>'Rekapitulasi Laporan Kartu Identitas Anak '.$bln,
		 		"mtitle"=>'Rekapitulasi Laporan Kartu Identitas Anak',
		 		"my_url"=>'Laporan_bln_kia',
		 		"type_tgl"=>'Cetak',
		 		"data"=>$r,
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"no_wil"=>'No Kec',
		 		"kode_wil"=>'Nama Kecamatan'
    		);
			}else{
			$data = array(
		 		"stitle"=>'Rekapitulasi Laporan Kartu Identitas Anak',
		 		"mtitle"=>'Rekapitulasi Laporan Kartu Identitas Anak',
		 		"my_url"=>'Laporan_bln_kia',
		 		"type_tgl"=>'Cetak',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"no_wil"=>'No Kec',
		 		"kode_wil"=>'Nama Kecamatan'
    		);
    		}
			return view('laporan_bln/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function Laporan_bln_kk(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 39;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->bulan != null){
			$bln = $request->bulan;
			
			$r = $this->mst_get_bln_kk($bln);
			$data = array(
		 		"stitle"=>'Rekapitulasi Laporan Kartu Keluarga Bulan '.$bln,
		 		"mtitle"=>'Rekapitulasi Laporan Kartu Keluarga',
		 		"my_url"=>'Laporan_bln_kk',
		 		"type_tgl"=>'Cetak',
		 		"data"=>$r,
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"no_wil"=>'No Kec',
		 		"kode_wil"=>'Nama Kecamatan'
    		);
			}else{
			$data = array(
		 		"stitle"=>'Rekapitulasi Laporan Kartu Keluarga',
		 		"mtitle"=>'Rekapitulasi Laporan Kartu Keluarga',
		 		"my_url"=>'Laporan_bln_kk',
		 		"type_tgl"=>'Cetak',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"no_wil"=>'No Kec',
		 		"kode_wil"=>'Nama Kecamatan'
    		);
    		}
			return view('laporan_bln/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function Laporan_bln_suket(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 38;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->bulan != null){
			$bln = $request->bulan;
			
			$r = $this->mst_get_bln_suket($bln);
			$data = array(
		 		"stitle"=>'Rekapitulasi Laporan Suket Bulan '.$bln,
		 		"mtitle"=>'Rekapitulasi Laporan Suket',
		 		"my_url"=>'Laporan_bln_suket',
		 		"type_tgl"=>'Cetak',
		 		"data"=>$r,
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"no_wil"=>'No Kec',
		 		"kode_wil"=>'Nama Kecamatan'
    		);
			}else{
			$data = array(
		 		"stitle"=>'Rekapitulasi Laporan Suket',
		 		"mtitle"=>'Rekapitulasi Laporan Suket',
		 		"my_url"=>'Laporan_bln_suket',
		 		"type_tgl"=>'Cetak',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"no_wil"=>'No Kec',
		 		"kode_wil"=>'Nama Kecamatan'
    		);
    		}
			return view('laporan_bln/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function Laporan_ktp(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 45;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->bulan != null){
			$bln = $request->bulan;
			
			$r = $this->mst_get_lap_ktp($bln);
			$data = array(
		 		"stitle"=>'Rekapitulasi Status Perekaman Ktp Elektronik Bulan '.$bln,
		 		"mtitle"=>'Rekapitulasi Status Perekaman Ktp Elektronik',
		 		"my_url"=>'Laporan_ktp',
		 		"type_tgl"=>'Cetak',
		 		"data"=>$r,
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"no_wil"=>'No Kec',
		 		"kode_wil"=>'Nama Kecamatan'
    		);
			}else{
			$data = array(
		 		"stitle"=>'Rekapitulasi Status Perekaman Ktp Elektronik',
		 		"mtitle"=>'Rekapitulasi Status Perekaman Ktp Elektronik',
		 		"my_url"=>'Laporan_ktp',
		 		"type_tgl"=>'Cetak',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"no_wil"=>'No Kec',
		 		"kode_wil"=>'Nama Kecamatan'
    		);
    		}
			return view('Laporan_ktp/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function Laporan_sisa_suket(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
        	$menu_id = 46;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->get_data != null){
			
			$r = $this->mst_sisa_suket();
			$data = array(
		 		"stitle"=>'Rekapitulasi Sisa Suket',
		 		"mtitle"=>'Rekapitulasi Sisa Suket',
		 		"my_url"=>'Laporan_sisa_suket',
		 		"type_tgl"=>'Cetak',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"no_wil"=>'No Kec',
		 		"kode_wil"=>'Nama Kecamatan'
    		);
			}else{
			$data = array(
		 		"stitle"=>'Rekapitulasi Sisa Suket',
		 		"mtitle"=>'Rekapitulasi Sisa Suket',
		 		"my_url"=>'Laporan_sisa_suket',
		 		"type_tgl"=>'Cetak',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"no_wil"=>'No Kec',
		 		"kode_wil"=>'Nama Kecamatan'
    		);
    		}
			return view('Laporan_sisa_suket/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function Laporan_dafduk_wktp(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 114;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->no_kec != null){
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$r = $this->mst_get_laporan_wajib_ktp($no_kec,$no_kel);
			$r2 = $this->mst_get_laporan_wajib_ktp_pelayanan($no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'Laporan Wktp',
		 		"mtitle"=>'Laporan Wktp',
		 		"my_url"=>'Wajibktp',
		 		"type_tgl"=>'Tanggal',
		 		"data"=>$r,
		 		"data2"=>$r2,
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			}else{
			$data = array(
		 		"stitle"=>'Laporan Wktp',
		 		"mtitle"=>'Laporan Wktp',
		 		"my_url"=>'Wajibktp',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
    		}
			return view('laporan_dafduk_wktp/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function Laporan_capil_pemilik_akta(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 114;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->no_kec != null){
			$is_db = $request->is_db;
			$usia_lap = $request->usia_lap;
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			if($is_db == 0){
				if($usia_lap == 0){
					$titleadd = '';
					$r = $this->mst_get_cakupan_akta($no_kec,$no_kel);
					$r2 = $this->mst_get_cakupan_akta_pelayanan($no_kec,$no_kel);
				}else if($usia_lap == 1){
					$titleadd = '0-18';
					$r = $this->mst_get_cakupan_akta_018($no_kec,$no_kel);
					$r2 = $this->mst_get_cakupan_akta_018_pelayanan($no_kec,$no_kel);
				}else{
					$titleadd = '18 Keatas';
					$r = $this->mst_get_cakupan_akta_18($no_kec,$no_kel);
					$r2 = $this->mst_get_cakupan_akta_18_pelayanan($no_kec,$no_kel);
				}
			}else if($is_db == 1){
				if($usia_lap == 0){
					$titleadd = '';
					$r = $this->mst_get_cakupan_akta($no_kec,$no_kel);
					$r2 = [];
				}else if($usia_lap == 1){
					$titleadd = '0-18';
					$r = $this->mst_get_cakupan_akta_018($no_kec,$no_kel);
					$r2 = [];
				}else{
					$titleadd = '18 Keatas';
					$r = $this->mst_get_cakupan_akta_18($no_kec,$no_kel);
					$r2 = [];
				}
			}else{
				if($usia_lap == 0){
					$titleadd = '';
					$r = $this->mst_get_cakupan_akta_pelayanan($no_kec,$no_kel);
					$r2 = [];
				}else if($usia_lap == 1){
					$titleadd = '0-18';
					$r = $this->mst_get_cakupan_akta_018_pelayanan($no_kec,$no_kel);
					$r2 = [];
				}else{
					$titleadd = '18 Keatas';
					$r = $this->mst_get_cakupan_akta_18_pelayanan($no_kec,$no_kel);
					$r2 = [];
				}
			}
			
			$data = array(
		 		"stitle"=>'Laporan Kepemilikan Akta Kelahiran '.$titleadd,
		 		"mtitle"=>'Laporan Kepemilikan Akta Kelahiran '.$titleadd,
		 		"my_url"=>'PemilikAkta',
		 		"type_tgl"=>'Tanggal',
		 		"data"=>$r,
		 		"data2"=>$r2,
		 		"is_db"=>$is_db,
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			}else{
			$data = array(
		 		"stitle"=>'Laporan Kepemilikan Akta Kelahiran',
		 		"mtitle"=>'Laporan Kepemilikan Akta Kelahiran',
		 		"my_url"=>'PemilikAkta',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
    		}
			return view('laporan_capil_akta/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function Laporan_capil_pemilik_akta_018(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 114;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->no_kec != null){
			$is_db = $request->is_db;
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			if($is_db == 0){
					$titleadd = '0-18';
					$r = $this->mst_get_cakupan_akta_018($no_kec,$no_kel);
					$r2 = $this->mst_get_cakupan_akta_018_pelayanan($no_kec,$no_kel);
			}else if($is_db == 1){
					$titleadd = '0-18';
					$r = $this->mst_get_cakupan_akta_018($no_kec,$no_kel);
					$r2 = [];
			}else{
					$titleadd = '0-18';
					$r = $this->mst_get_cakupan_akta_018_pelayanan($no_kec,$no_kel);
					$r2 = [];
			}
			
			$data = array(
		 		"stitle"=>'Laporan Kepemilikan Akta Kelahiran '.$titleadd,
		 		"mtitle"=>'Laporan Kepemilikan Akta Kelahiran '.$titleadd,
		 		"my_url"=>'U18',
		 		"type_tgl"=>'Tanggal',
		 		"data"=>$r,
		 		"data2"=>$r2,
		 		"is_db"=>$is_db,
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			}else{
			$data = array(
		 		"stitle"=>'Laporan Kepemilikan Akta Kelahiran',
		 		"mtitle"=>'Laporan Kepemilikan Akta Kelahiran',
		 		"my_url"=>'U18',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
    		}
			return view('laporan_capil_akta_018/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function Laporan_capil_pemilik_akta_perkawinan(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 200;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->no_kec != null){
			$is_db = $request->is_db;
			$jenis_lap = $request->jenis_lap;
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			if($is_db == 0){
				if($jenis_lap == 0){
					$titleadd = '';
					$r = $this->mst_get_cakupan_perkawinan($no_kec,$no_kel);
					$r2 = $this->mst_get_cakupan_perkawinan_pelayanan($no_kec,$no_kel);
				}else if($jenis_lap == 1){
					$titleadd = 'Non Muslim';
					$r = $this->mst_get_cakupan_perkawinan_non_muslim($no_kec,$no_kel);
					$r2 = $this->mst_get_cakupan_perkawinan_non_muslim_pelayanan($no_kec,$no_kel);
				}else{
					$titleadd = 'Muslim';
					$r = $this->mst_get_cakupan_perkawinan_muslim($no_kec,$no_kel);
					$r2 = $this->mst_get_cakupan_perkawinan_muslim_pelayanan($no_kec,$no_kel);
				}
			}else if($is_db == 1){
				if($jenis_lap == 0){
					$titleadd = '';
					$r = $this->mst_get_cakupan_perkawinan($no_kec,$no_kel);
					$r2 = [];
				}else if($jenis_lap == 1){
					$titleadd = 'Non Muslim';
					$r = $this->mst_get_cakupan_perkawinan_non_muslim($no_kec,$no_kel);
					$r2 = [];
				}else{
					$titleadd = 'Muslim';
					$r = $this->mst_get_cakupan_perkawinan_muslim($no_kec,$no_kel);
					$r2 = [];
				}
			}else{
				if($jenis_lap == 0){
					$titleadd = '';
					$r = $this->mst_get_cakupan_perkawinan_pelayanan($no_kec,$no_kel);
					$r2 = [];
				}else if($jenis_lap == 1){
					$titleadd = 'Non Muslim';
					$r = $this->mst_get_cakupan_perkawinan_non_muslim_pelayanan($no_kec,$no_kel);
					$r2 = [];
				}else{
					$titleadd = 'Muslim';
					$r = $this->mst_get_cakupan_perkawinan_muslim_pelayanan($no_kec,$no_kel);
					$r2 = [];
				}
			}
			
			$data = array(
		 		"stitle"=>'Laporan Kepemilikan Akta Perkawinan '.$titleadd,
		 		"mtitle"=>'Laporan Kepemilikan Akta Perkawinan '.$titleadd,
		 		"my_url"=>'StatKwn',
		 		"type_tgl"=>'Tanggal',
		 		"data"=>$r,
		 		"data2"=>$r2,
		 		"is_db"=>$is_db,
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			}else{
			$data = array(
		 		"stitle"=>'Laporan Kepemilikan Akta Perkawinan',
		 		"mtitle"=>'Laporan Kepemilikan Akta Perkawinan',
		 		"my_url"=>'StatKwn',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
    		}
			return view('laporan_capil_perkawinan/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function Laporan_capil_pemilik_akta_perceraian(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 202;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->no_kec != null){
			$is_db = $request->is_db;
			$jenis_lap = $request->jenis_lap;
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			if($is_db == 0){
				if($jenis_lap == 0){
					$titleadd = '';
					$r = $this->mst_get_cakupan_perceraian($no_kec,$no_kel);
					$r2 = $this->mst_get_cakupan_perceraian_pelayanan($no_kec,$no_kel);
				}else if($jenis_lap == 1){
					$titleadd = 'Non Muslim';
					$r = $this->mst_get_cakupan_perceraian_non_muslim($no_kec,$no_kel);
					$r2 = $this->mst_get_cakupan_perceraian_non_muslim_pelayanan($no_kec,$no_kel);
				}else{
					$titleadd = 'Muslim';
					$r = $this->mst_get_cakupan_perceraian_muslim($no_kec,$no_kel);
					$r2 = $this->mst_get_cakupan_perceraian_muslim_pelayanan($no_kec,$no_kel);
				}
			}else if($is_db == 1){
				if($jenis_lap == 0){
					$titleadd = '';
					$r = $this->mst_get_cakupan_perceraian($no_kec,$no_kel);
					$r2 = [];
				}else if($jenis_lap == 1){
					$titleadd = 'Non Muslim';
					$r = $this->mst_get_cakupan_perceraian_non_muslim($no_kec,$no_kel);
					$r2 = [];
				}else{
					$titleadd = 'Muslim';
					$r = $this->mst_get_cakupan_perceraian_muslim($no_kec,$no_kel);
					$r2 = [];
				}
			}else{
				if($jenis_lap == 0){
					$titleadd = '';
					$r = $this->mst_get_cakupan_perceraian_pelayanan($no_kec,$no_kel);
					$r2 = [];
				}else if($jenis_lap == 1){
					$titleadd = 'Non Muslim';
					$r = $this->mst_get_cakupan_perceraian_non_muslim_pelayanan($no_kec,$no_kel);
					$r2 = [];
				}else{
					$titleadd = 'Muslim';
					$r = $this->mst_get_cakupan_perceraian_muslim_pelayanan($no_kec,$no_kel);
					$r2 = [];
				}
			}
			
			$data = array(
		 		"stitle"=>'Laporan Kepemilikan Akta Perceraian '.$titleadd,
		 		"mtitle"=>'Laporan Kepemilikan Akta Perceraian '.$titleadd,
		 		"my_url"=>'StatCrai',
		 		"type_tgl"=>'Tanggal',
		 		"data"=>$r,
		 		"data2"=>$r2,
		 		"is_db"=>$is_db,
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			}else{
			$data = array(
		 		"stitle"=>'Laporan Kepemilikan Akta Perceraian',
		 		"mtitle"=>'Laporan Kepemilikan Akta Perceraian',
		 		"my_url"=>'StatCrai',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
    		}
			return view('laporan_capil_perceraian/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function Laporan_dafduk_identitas_tunggal(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 114;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->no_kec != null){
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$r = $this->mst_get_laporan_identitas_tunggal($no_kec,$no_kel);
			$r2 = $this->mst_get_laporan_identitas_tunggal_pelayanan($no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'Laporan Identitas Tunggal',
		 		"mtitle"=>'Laporan Identitas Tunggal',
		 		"my_url"=>'IdentitasTunggal',
		 		"type_tgl"=>'Tanggal',
		 		"data"=>$r,
		 		"data2"=>$r2,
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			}else{
			$data = array(
		 		"stitle"=>'Laporan Identitas Tunggal',
		 		"mtitle"=>'Laporan Identitas Tunggal',
		 		"my_url"=>'IdentitasTunggal',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
    		}
			return view('Laporan_dafduk_identitas_tunggal/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function Laporan_dafduk_kitas(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 114;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->no_kec != null){
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$r = $this->mst_get_laporan_kitas($no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'Laporan Kitas/Limited Stay Permit Card',
		 		"mtitle"=>'Laporan Kitas/Limited Stay Permit Card',
		 		"my_url"=>'Kitas',
		 		"type_tgl"=>'Tanggal',
		 		"data"=>$r,
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			}else{
			$data = array(
		 		"stitle"=>'Laporan Kitas/Limited Stay Permit Card',
		 		"mtitle"=>'Laporan Kitas/Limited Stay Permit Card',
		 		"my_url"=>'Kitas',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
    		}
			return view('Laporan_dafduk_kitas/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function BlangkoEktp(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 114;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->tanggal != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$r = $this->mst_get_laporan_sisa_blangko($tgl_start,$tgl_end);
			$data = array(
		 		"stitle"=>'Laporan Sisa Blangko Ektp',
		 		"mtitle"=>'Laporan Sisa Blangko Ektp',
		 		"my_url"=>'BlangkoEktp',
		 		"type_tgl"=>'Tanggal',
		 		"data"=>$r,
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			}else{
			$data = array(
		 		"stitle"=>'Laporan Sisa Blangko Ektp',
		 		"mtitle"=>'Laporan Sisa Blangko Ektp',
		 		"my_url"=>'BlangkoEktp',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
    		}
			return view('Laporan_dafduk_blangko/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function BlangkoEktpMasuk(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 261;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->tanggal != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$r = $this->mst_get_laporan_blangko_masuk($tgl_start,$tgl_end);
			$data = array(
		 		"stitle"=>'Laporan Pemasukan Blangko Ektp',
		 		"mtitle"=>'Laporan Pemasukan Blangko Ektp',
		 		"my_url"=>'BlangkoEktpMasuk',
		 		"type_tgl"=>'Tanggal',
		 		"data"=>$r,
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			}else{
			$data = array(
		 		"stitle"=>'Laporan Pemasukan Blangko Ektp',
		 		"mtitle"=>'Laporan Pemasukan Blangko Ektp',
		 		"my_url"=>'BlangkoEktpMasuk',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
    		}
			return view('Laporan_dafduk_blangko_masuk/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function TTE_kk(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 114;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->tanggal != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$title_wil = '';
			if($no_kec == 0){
				$no_wil = 'No Kec';
				$kode_wil = 'Nama Kecamatan';
			}else{
				$no_wil = 'No Kel';
				$kode_wil = 'Nama Kelurahan';
			}
			if($no_kec != 0 && $no_kel == 0){
				$title_wil = ' Kecamatan '.ucfirst(strtolower(Shr::do_get_nama_kec($no_kec)));
			}else if($no_kec != 0 && $no_kel != 0){
				$title_wil = ' Kelurahan '.ucfirst(strtolower(Shr::do_get_nama_kel($no_kec,$no_kel)));
			}
			$r = $this->mst_get_laporan_tte_kk($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'Laporan TTE KK'.$title_wil.' Tanggal '.$tgl,
		 		"mtitle"=>'Laporan TTE KK',
		 		"my_url"=>'TTEkk',
		 		"type_tgl"=>'Tanggal',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>$no_wil,
		 		"kode_wil"=>$kode_wil
    		);
			}else{
			$no_wil = 'No Kec';
			$kode_wil = 'Nama Kecamatan';
			$data = array(
		 		"stitle"=>'Laporan TTE KK',
		 		"mtitle"=>'Laporan TTE KK',
		 		"my_url"=>'TTEkk',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>$no_wil,
		 		"kode_wil"=>$kode_wil
    		);
    		}
			return view('Laporan_dafduk_tte_kk/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function TTE_perpindahan(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 114;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->tanggal != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$r = $this->mst_get_laporan_tte_perpindahan($tgl,$tgl_start,$tgl_end);
			$data = array(
		 		"stitle"=>'Laporan TTE Perpindahan Tanggal '.$tgl,
		 		"mtitle"=>'Laporan TTE Perpindahan',
		 		"my_url"=>'TTEperpindahan',
		 		"type_tgl"=>'Tanggal',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
    		);
			}else{
			$data = array(
		 		"stitle"=>'Laporan TTE Perpindahan',
		 		"mtitle"=>'Laporan TTE Perpindahan',
		 		"my_url"=>'TTEperpindahan',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
    		);
    		}
			return view('Laporan_dafduk_tte_perpindahan/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function TTE_kelahiran(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 244;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->tanggal != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$r = $this->mst_get_laporan_tte_kelahiran($tgl,$tgl_start,$tgl_end);
			$data = array(
		 		"stitle"=>'Laporan TTE Kelahiran Tanggal '.$tgl,
		 		"mtitle"=>'Laporan TTE Kelahiran',
		 		"my_url"=>'TTEKelahiran',
		 		"type_tgl"=>'Tanggal',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
    		);
			}else{
			$data = array(
		 		"stitle"=>'Laporan TTE Kelahiran',
		 		"mtitle"=>'Laporan TTE Kelahiran',
		 		"my_url"=>'TTEKelahiran',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
    		);
    		}
			return view('Laporan_capil_tte_kelahiran/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function TTE_kematian(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 244;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->tanggal != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$r = $this->mst_get_laporan_tte_kematian($tgl,$tgl_start,$tgl_end);
			$data = array(
		 		"stitle"=>'Laporan TTE Kematian Tanggal '.$tgl,
		 		"mtitle"=>'Laporan TTE Kematian',
		 		"my_url"=>'TTEKematian',
		 		"type_tgl"=>'Tanggal',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
    		);
			}else{
			$data = array(
		 		"stitle"=>'Laporan TTE Kematian',
		 		"mtitle"=>'Laporan TTE Kematian',
		 		"my_url"=>'TTEKematian',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
    		);
    		}
			return view('Laporan_capil_tte_kematian/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function Laporan_dafduk_kitap(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 114;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->no_kec != null){
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$r = $this->mst_get_laporan_kitap($no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'Laporan Kitap/Permanent Stay Permit Card',
		 		"mtitle"=>'Laporan Kitap/Permanent Stay Permit Card',
		 		"my_url"=>'Kitap',
		 		"type_tgl"=>'Tanggal',
		 		"data"=>$r,
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			}else{
			$data = array(
		 		"stitle"=>'Laporan Kitap/Permanent Stay Permit Card',
		 		"mtitle"=>'Laporan Kitap/Permanent Stay Permit Card',
		 		"my_url"=>'Kitap',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
    		}
			return view('Laporan_dafduk_kitap/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function Laporan_dafduk_ktp_oa(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 114;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->no_kec != null){
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$r = $this->mst_get_laporan_ktp_oa($no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'Laporan KTP Orang Asing',
		 		"mtitle"=>'Laporan KTP Orang Asing',
		 		"my_url"=>'KtpOA',
		 		"type_tgl"=>'Tanggal',
		 		"data"=>$r,
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			}else{
			$data = array(
		 		"stitle"=>'Laporan KTP Orang Asing',
		 		"mtitle"=>'Laporan KTP Orang Asing',
		 		"my_url"=>'KtpOA',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
    		}
			return view('Laporan_dafduk_ktp_oa/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function Laporan_dafduk_lintas_batas(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 114;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->no_kec != null){
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$r = $this->mst_get_laporan_lintas_batas($no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'Laporan Lintas Batas',
		 		"mtitle"=>'Laporan Lintas Batas',
		 		"my_url"=>'LintasBatas',
		 		"type_tgl"=>'Tanggal',
		 		"data"=>$r,
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			}else{
			$data = array(
		 		"stitle"=>'Laporan Lintas Batas',
		 		"mtitle"=>'Laporan Lintas Batas',
		 		"my_url"=>'LintasBatas',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
    		}
			return view('Laporan_dafduk_lintas_batas/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function Laporan_dafduk_petugas_khusus(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 114;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->no_kec != null){
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$r = $this->mst_get_laporan_petugas_khusus($no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'Laporan Petugas Khusus',
		 		"mtitle"=>'Laporan Petugas Khusus',
		 		"my_url"=>'PetugasKhusus',
		 		"type_tgl"=>'Tanggal',
		 		"data"=>$r,
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
			}else{
			$data = array(
		 		"stitle"=>'Laporan Petugas Khusus',
		 		"mtitle"=>'Laporan Petugas Khusus',
		 		"my_url"=>'PetugasKhusus',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL')
    		);
    		}
			return view('Laporan_dafduk_petugas_khusus/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function Laporan_surat_pindah_ln(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 27;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->tanggal != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$title_wil = '';
			if($no_kec == 0){
				$no_wil = 'No Kec';
				$kode_wil = 'Nama Kecamatan';
			}else{
				$no_wil = 'No Kel';
				$kode_wil = 'Nama Kelurahan';
			}
			if($no_kec != 0 && $no_kel == 0){
				$title_wil = ' Kecamatan '.ucfirst(strtolower(Shr::do_get_nama_kec($no_kec)));
			}else if($no_kec != 0 && $no_kel != 0){
				$title_wil = ' Kelurahan '.ucfirst(strtolower(Shr::do_get_nama_kel($no_kec,$no_kel)));
			}
			$r = $this->get_data_surat_pindah_ln($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'Laporan Surat Pindah LN'.$title_wil.' Tanggal '.$tgl,
		 		"mtitle"=>'Laporan Surat Pindah LN',
		 		"my_url"=>'Skpln',
		 		"type_tgl"=>'Cetak',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>$no_wil,
		 		"kode_wil"=>$kode_wil,
		 		"is_mobilitas"=>"Y"
    		);
			}else{
			$data = array(
		 		"stitle"=>'Laporan Surat Pindah LN',
		 		"mtitle"=>'Laporan Surat Pindah LN',
		 		"my_url"=>'Skpln',
		 		"type_tgl"=>'Cetak',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>'No Kec',
		 		"kode_wil"=>'Nama Kecamatan',
		 		"is_mobilitas"=>"Y"
    		);
    		}
			return view('Laporan/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}

	public function Laporan_surat_datang_ln(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
			$menu_id = 27;
		    $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->tanggal != null){
			$tgl = $request->tanggal;
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $request->no_kec;
			$no_kel = $request->no_kel;
			$title_wil = '';
			if($no_kec == 0){
				$no_wil = 'No Kec';
				$kode_wil = 'Nama Kecamatan';
			}else{
				$no_wil = 'No Kel';
				$kode_wil = 'Nama Kelurahan';
			}
			if($no_kec != 0 && $no_kel == 0){
				$title_wil = ' Kecamatan '.ucfirst(strtolower(Shr::do_get_nama_kec($no_kec)));
			}else if($no_kec != 0 && $no_kel != 0){
				$title_wil = ' Kelurahan '.ucfirst(strtolower(Shr::do_get_nama_kel($no_kec,$no_kel)));
			}
			$r = $this->get_data_surat_datang_ln($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'Laporan Surat Datang LN'.$title_wil.' Tanggal '.$tgl,
		 		"mtitle"=>'Laporan Surat Datang LN',
		 		"my_url"=>'Skdln',
		 		"type_tgl"=>'Cetak',
		 		"data"=>$r,
		 		"jumlah"=>count($r),
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>$no_wil,
		 		"kode_wil"=>$kode_wil,
		 		"is_mobilitas"=>"Y"
    		);
			}else{
			$data = array(
		 		"stitle"=>'Laporan Surat Datang LN',
		 		"mtitle"=>'Laporan Surat Datang LN',
		 		"my_url"=>'Skdln',
		 		"type_tgl"=>'Cetak',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		 		"no_wil"=>'No Kec',
		 		"kode_wil"=>'Nama Kecamatan',
		 		"is_mobilitas"=>"Y"
    		);
    		}
			return view('Laporan/main',$data);
		}else{
			return redirect()->route('logout');
		}
	}
	public function Petugas_capil(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
		    $menu_id = 216;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			$data = array(
		 		"stitle"=>'Pengelola Siak Pencatatan Sipil',
		 		"mtitle"=>'Pengelola Siak Pencatatan Sipil',
		 		"my_url"=>'petugas',
		 		"type_tgl"=>'Tanggal',
		 		"menu"=>$menu,
       			"akses_kec"=>$isakses_kec,
       			"akses_kel"=>$isakses_kel,
		 		"user_id"=>$request->session()->get('S_USER_ID'),
		 		"user_nik"=>$request->session()->get('S_NIK'),
		 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
		 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
		 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
		 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
		 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
    		);
			return view('petugas_capil/main',$data);
			}else{
			return redirect()->route('logout');
		}

	}
	
	public function get_kpu(Request $request)
	{
		Shr::check_login();
		if (Session::get('S_USER_ID') != null) 
        {
		    $menu_id = 216;
		     $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
			if ($is_akses == 0){
				 return redirect()->route('404Notfound');
			}
			$menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
			$isakses_kec = Shr::get_give_kec();
			$isakses_kel = Shr::get_give_kel();
			if($request->bulan != null){
			$bulan = $request->bulan;
			$tipe_data = $request->tipe_data;
			$kategori_data = $request->kategori_data;


			if($tipe_data <> 0 && $kategori_data <>0){
				$r = $this->get_data_kpu($bulan,$tipe_data,$kategori_data);
	    		$data = array(
				 		"stitle"=>'Data Kpu Pleno '.$bulan,
				 		"mtitle"=>'Data Kpu Pleno '.$bulan,
				 		"my_url"=>'Data',
				 		"type_tgl"=>'Tanggal',
				 		"tipe_data"=>$tipe_data,
				 		"kategori_data"=>$kategori_data,
				 		"data"=>$r,
			 			"jumlah"=>count($r),
				 		"menu"=>$menu,
				 		"bulan"=>$bulan,
		       			"akses_kec"=>$isakses_kec,
		       			"akses_kel"=>$isakses_kel,
				 		"user_id"=>$request->session()->get('S_USER_ID'),
				 		"user_nik"=>$request->session()->get('S_NIK'),
				 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
				 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
				 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
				 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
				 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
		    		);
			}else{
				$data = array(
			 		"stitle"=>'Data Kpu Pleno',
			 		"mtitle"=>'Data Kpu Pleno',
			 		"my_url"=>'Data',
			 		"type_tgl"=>'Tanggal',
			 		"menu"=>$menu,
	       			"akses_kec"=>$isakses_kec,
	       			"akses_kel"=>$isakses_kel,
			 		"user_id"=>$request->session()->get('S_USER_ID'),
			 		"user_nik"=>$request->session()->get('S_NIK'),
			 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
			 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
			 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
			 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
			 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
	    		);
			}
			}else{
				$data = array(
			 		"stitle"=>'Data Kpu Pleno',
			 		"mtitle"=>'Data Kpu Pleno',
			 		"my_url"=>'Data',
			 		"type_tgl"=>'Tanggal',
			 		"menu"=>$menu,
	       			"akses_kec"=>$isakses_kec,
	       			"akses_kel"=>$isakses_kel,
			 		"user_id"=>$request->session()->get('S_USER_ID'),
			 		"user_nik"=>$request->session()->get('S_NIK'),
			 		"user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
			 		"user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
			 		"user_no_kel"=>$request->session()->get('S_NO_KEL'),
			 		"user_no_kec"=>$request->session()->get('S_NO_KEC'),
			 		"user_level"=>$request->session()->get('S_USER_LEVEL'),
	    		);
			}
			
			return view('petugas_kpu/main',$data);
			}else{
			return redirect()->route('logout');
		}

	}

	
	public function get_petugas_capil(Request $request)
     {
          $draw = intval($request->draw);
          $start = intval($request->start);
          $length = intval($request->length);

          $petugas = $this->do_get_petugas_capil();

          $data = array();
          $i = 0;
          foreach($petugas as $r) {
          		$i ++;
               $data[] = array(
               		$i,
                    $r->user_id,
                    $r->nama_lgkp,
                    $r->jenis_klmin,
                    $r->level_name,
                    $r->group_name,
               );
          }

          $output = array(
               "draw" => $draw,
                 "recordsTotal" => count($petugas),
                 "recordsFiltered" => count($petugas),
                 "data" => $data
            );
          return $output;
          exit();
		}


	

	public function rkm_get_data_rekam($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL, '$tgl' AS TANGGAL, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEC@DB222 A LEFT JOIN
					(SELECT 
					    C.NO_KEC
					    , C.NAMA_KEC
					    , COUNT(B.NIK) AS JUMLAH FROM DEMOGRAPHICS B 
					    INNER JOIN SETUP_KEC@DB222 C
					    ON B.NO_PROP = C.NO_PROP 
					    AND B.NO_KAB = C.NO_KAB 
					    AND B.NO_KEC = C.NO_KEC 
					    WHERE 
					     1=1 AND
					    B.CREATED >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND B.CREATED < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1
					    AND EXISTS(SELECT 1 FROM FACES D WHERE B.NIK = D.NIK)
					    AND
					    C.NO_PROP = 32 AND C.NO_KAB = 73
					    GROUP BY C.NO_KEC, C.NAMA_KEC
					    ORDER BY C.NO_KEC, C.NAMA_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL, '$tgl' AS TANGGAL, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
						FROM SETUP_KEL@DB222 A LEFT JOIN
						(SELECT 
						    C.NO_KEL
						    , C.NAMA_KEL
						    , COUNT(B.NIK) AS JUMLAH FROM DEMOGRAPHICS B 
						     INNER JOIN SETUP_KEL@DB222 C
						    ON B.NO_PROP = C.NO_PROP 
						    AND B.NO_KAB = C.NO_KAB 
						    AND B.NO_KEC = C.NO_KEC 
						    AND B.NO_KEL = C.NO_KEL 
						    WHERE 
						    1=1 AND
						    B.CREATED >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND B.CREATED < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1
						    AND EXISTS(SELECT 1 FROM FACES D WHERE B.NIK = D.NIK)
						    ";
						if($no_kel == 0){    
					$sql .=" AND C.NO_PROP = 32 AND C.NO_KAB = 73 AND C.NO_KEC =".$no_kec."
						    GROUP BY C.NO_KEL, C.NAMA_KEL
						    ORDER BY C.NO_KEL, C.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND C.NO_PROP = 32 AND C.NO_KAB = 73 AND C.NO_KEC =".$no_kec." and  C.NO_KEL =".$no_kel."
						    GROUP BY C.NO_KEL, C.NAMA_KEL
						    ORDER BY C.NO_KEL, C.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				
			$r = DB::connection('db221')->select($sql);
			return $r;

		}
		public function ctk_get_data_cetak($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL, '$tgl' AS TANGGAL, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEC@DB222 A LEFT JOIN
					(SELECT 
					    C.NO_KEC
					    , C.NAMA_KEC
					    , COUNT(B.NIK) AS JUMLAH FROM CARD_MANAGEMENT B 
					    INNER JOIN DEMOGRAPHICS A 
					    ON A.NIK =  B.NIK INNER JOIN SETUP_KEC@DB222 C
					    ON A.NO_PROP = C.NO_PROP 
					    AND A.NO_KAB = C.NO_KAB 
					    AND A.NO_KEC = C.NO_KEC 
					    WHERE 
					    CASE WHEN B.LAST_UPDATE IS NULL THEN B.PERSONALIZED_DATE ELSE B.LAST_UPDATE END >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND CASE WHEN B.LAST_UPDATE IS NULL THEN B.PERSONALIZED_DATE ELSE B.LAST_UPDATE END < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1
					    AND CASE WHEN UPPER(B.LAST_UPDATED_USERNAME) IS NULL THEN UPPER(B.CREATED_USERNAME) ELSE UPPER(B.LAST_UPDATED_USERNAME) END  NOT IN (SELECT UPPER(USER_BCARD) FROM SIAK_CETAK_GOIB@YZDB WHERE IS_ACTIVE = 1)
					    AND
					    C.NO_PROP = 32 AND C.NO_KAB = 73
					    GROUP BY C.NO_KEC, C.NAMA_KEC
					    ORDER BY C.NO_KEC, C.NAMA_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL, '$tgl' AS TANGGAL, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
						FROM SETUP_KEL@DB222 A LEFT JOIN
						(SELECT 
						    C.NO_KEL
						    , C.NAMA_KEL
						    , COUNT(B.NIK) AS JUMLAH FROM CARD_MANAGEMENT B 
						    INNER JOIN DEMOGRAPHICS A 
						    ON A.NIK =  B.NIK INNER JOIN SETUP_KEL@DB222 C
						    ON A.NO_PROP = C.NO_PROP 
						    AND A.NO_KAB = C.NO_KAB 
						    AND A.NO_KEC = C.NO_KEC 
						    AND A.NO_KEL = C.NO_KEL 
						    WHERE 
						   CASE WHEN B.LAST_UPDATE IS NULL THEN B.PERSONALIZED_DATE ELSE B.LAST_UPDATE END >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND CASE WHEN B.LAST_UPDATE IS NULL THEN B.PERSONALIZED_DATE ELSE B.LAST_UPDATE END < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1
					    AND CASE WHEN UPPER(B.LAST_UPDATED_USERNAME) IS NULL THEN UPPER(B.CREATED_USERNAME) ELSE UPPER(B.LAST_UPDATED_USERNAME) END  NOT IN (SELECT UPPER(USER_BCARD) FROM SIAK_CETAK_GOIB@YZDB WHERE IS_ACTIVE = 1)";
						if($no_kel == 0){    
					$sql .=" AND C.NO_PROP = 32 AND C.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY C.NO_KEL, C.NAMA_KEL
						    ORDER BY C.NO_KEL, C.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND C.NO_PROP = 32 AND C.NO_KAB = 73 AND A.NO_KEC =".$no_kec." and  A.NO_KEL =".$no_kel."
						    GROUP BY C.NO_KEL, C.NAMA_KEL
						    ORDER BY C.NO_KEL, C.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				
				$r = DB::connection('db2')->select($sql);
			return $r;
		}
		public function skt_get_data_suket($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL, '$tgl' AS TANGGAL, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEC A LEFT JOIN
					(SELECT 
					    C.NO_KEC
					    , C.NAMA_KEC
					    , COUNT(B.NIK) AS JUMLAH FROM T7_HIST_SUKET B 
					    INNER JOIN BIODATA_WNI A 
					    ON A.NIK =  B.NIK INNER JOIN SETUP_KEC C
					    ON A.NO_PROP = C.NO_PROP 
					    AND A.NO_KAB = C.NO_KAB 
					    AND A.NO_KEC = C.NO_KEC 
					    WHERE 
					     A.FLAG_STATUS = 0 AND
					    B.PRINTED_DATE >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND B.PRINTED_DATE < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1
					    AND
					    C.NO_PROP = 32 AND C.NO_KAB = 73
					    GROUP BY C.NO_KEC, C.NAMA_KEC
					    ORDER BY C.NO_KEC, C.NAMA_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL, '$tgl' AS TANGGAL, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
						FROM SETUP_KEL A LEFT JOIN
						(SELECT 
						    C.NO_KEL
						    , C.NAMA_KEL
						    , COUNT(B.NIK) AS JUMLAH FROM T7_HIST_SUKET B 
						    INNER JOIN BIODATA_WNI A 
						    ON A.NIK =  B.NIK INNER JOIN SETUP_KEL C
						    ON A.NO_PROP = C.NO_PROP 
						    AND A.NO_KAB = C.NO_KAB 
						    AND A.NO_KEC = C.NO_KEC 
						    AND A.NO_KEL = C.NO_KEL 
						    WHERE 
						    A.FLAG_STATUS = 0 AND
						    B.PRINTED_DATE >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND B.PRINTED_DATE < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1";
						if($no_kel == 0){    
					$sql .=" AND C.NO_PROP = 32 AND C.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY C.NO_KEL, C.NAMA_KEL
						    ORDER BY C.NO_KEL, C.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND C.NO_PROP = 32 AND C.NO_KAB = 73 AND A.NO_KEC =".$no_kec." and  A.NO_KEL =".$no_kel."
						    GROUP BY C.NO_KEL, C.NAMA_KEL
						    ORDER BY C.NO_KEL, C.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				
				$r = DB::connection('db222')->select($sql);
			return $r;
		}
		public function get_data_kk($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL, '$tgl' AS TANGGAL, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEC A LEFT JOIN
					(SELECT 
						C.NO_KEC,
						C.NAMA_KEC,
						COUNT(1) JUMLAH
						FROM T5_SEQN_PRINT_KK B
						INNER JOIN SETUP_KEC C 
						ON C.NO_PROP = B.NO_PROP
						AND C.NO_KAB = B.NO_KAB
						AND C.NO_KEC = B.NO_KEC
						WHERE 
						B.PRINT_TYPE =0 AND
						B.PRINTED_DATE >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND B.PRINTED_DATE < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1
						GROUP BY C.NO_KEC, C.NAMA_KEC ORDER BY C.NO_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL, '$tgl' AS TANGGAL, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
						FROM SETUP_KEL A LEFT JOIN
						(SELECT 
							C.NO_KEL,
							C.NAMA_KEL,
							COUNT(1) JUMLAH
							FROM T5_SEQN_PRINT_KK B
							INNER JOIN SETUP_KEL C 
							ON C.NO_PROP = B.NO_PROP
							AND C.NO_KAB = B.NO_KAB
							AND C.NO_KEC = B.NO_KEC
							AND C.NO_KEL = B.NO_KEL
							WHERE 
							B.PRINT_TYPE =0 AND
							B.PRINTED_DATE >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND B.PRINTED_DATE < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1";
						if($no_kel == 0){    
					$sql .=" AND C.NO_PROP = 32 AND C.NO_KAB = 73 AND C.NO_KEC =".$no_kec."
						    GROUP BY C.NO_KEL, C.NAMA_KEL
						    ORDER BY C.NO_KEL, C.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND C.NO_PROP = 32 AND C.NO_KAB = 73 AND C.NO_KEC =".$no_kec." and  C.NO_KEL =".$no_kel."
						    GROUP BY C.NO_KEL, C.NAMA_KEL
						    ORDER BY C.NO_KEL, C.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::connection('db222')->select($sql);
			return $r;

		}
		public function get_data_bio($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL, '$tgl' AS TANGGAL, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEC A LEFT JOIN
					(SELECT 
					    C.NO_KEC
					    , C.NAMA_KEC
					    , COUNT(DISTINCT(A.NIK)) AS JUMLAH FROM BIODATA_WNI A 
					    INNER JOIN SETUP_KEC C
					    ON A.NO_PROP = C.NO_PROP 
					    AND A.NO_KAB = C.NO_KAB 
					    AND A.NO_KEC = C.NO_KEC 
					    WHERE 
					     A.FLAG_STATUS = 0 AND
					    A.TGL_ENTRI >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND A.TGL_ENTRI < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1
					    AND
					    C.NO_PROP = 32 AND C.NO_KAB = 73
					    GROUP BY C.NO_KEC, C.NAMA_KEC
					    ORDER BY C.NO_KEC, C.NAMA_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL, '$tgl' AS TANGGAL, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
						FROM SETUP_KEL A LEFT JOIN
						(SELECT 
						    C.NO_KEL
						    , C.NAMA_KEL
						    , COUNT(DISTINCT(A.NIK)) AS JUMLAH FROM BIODATA_WNI A 
						    INNER JOIN SETUP_KEL C
						    ON A.NO_PROP = C.NO_PROP 
						    AND A.NO_KAB = C.NO_KAB 
						    AND A.NO_KEC = C.NO_KEC 
						    AND A.NO_KEL = C.NO_KEL 
						    WHERE 
						    A.FLAG_STATUS = 0 AND
						    A.TGL_ENTRI >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND A.TGL_ENTRI < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1";
						if($no_kel == 0){    
					$sql .=" AND C.NO_PROP = 32 AND C.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY C.NO_KEL, C.NAMA_KEL
						    ORDER BY C.NO_KEL, C.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND C.NO_PROP = 32 AND C.NO_KAB = 73 AND A.NO_KEC =".$no_kec." and  A.NO_KEL =".$no_kel."
						    GROUP BY C.NO_KEL, C.NAMA_KEL
						    ORDER BY C.NO_KEL, C.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::connection('db222')->select($sql);
			return $r;


		}
		public function get_data_surat_pindah_all($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL, '$tgl' AS TANGGAL, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH, CASE WHEN B.JUMLAH_ANGGOTA IS NULL THEN 0 ELSE B.JUMLAH_ANGGOTA END AS JUMLAH_ANGGOTA
					FROM SETUP_KEC A LEFT JOIN
					(SELECT 
					    C.NO_KEC
					    , C.NAMA_KEC
					    , COUNT(DISTINCT(A.NO_PINDAH)) AS JUMLAH
             			, COUNT(A.NO_PINDAH) AS JUMLAH_ANGGOTA
              			FROM PINDAH_HEADER A INNER JOIN PINDAH_DETAIL B ON A.NO_PINDAH = B.NO_PINDAH INNER JOIN SETUP_KEC C
					    ON A.FROM_NO_PROP = C.NO_PROP 
					    AND A.FROM_NO_KAB = C.NO_KAB 
					    AND A.FROM_NO_KEC = C.NO_KEC 
              			WHERE 1=1 
        				AND A.CREATED_DATE >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND A.CREATED_DATE < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1
					    AND
					    C.NO_PROP = 32 AND C.NO_KAB = 73
					    GROUP BY C.NO_KEC, C.NAMA_KEC
					    ORDER BY C.NO_KEC, C.NAMA_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL, '$tgl' AS TANGGAL, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH, CASE WHEN B.JUMLAH_ANGGOTA IS NULL THEN 0 ELSE B.JUMLAH_ANGGOTA END AS JUMLAH_ANGGOTA
					FROM SETUP_KEL A LEFT JOIN
					(SELECT 
					    C.NO_KEL
					    , C.NAMA_KEL
					    , COUNT(DISTINCT(A.NO_PINDAH)) AS JUMLAH
             			, COUNT(A.NO_PINDAH) AS JUMLAH_ANGGOTA
              			FROM PINDAH_HEADER A INNER JOIN PINDAH_DETAIL B ON A.NO_PINDAH = B.NO_PINDAH INNER JOIN SETUP_KEL C
					    ON A.FROM_NO_PROP = C.NO_PROP 
					    AND A.FROM_NO_KAB = C.NO_KAB 
					    AND A.FROM_NO_KEC = C.NO_KEC 
					    AND A.FROM_NO_KEL = C.NO_KEL 
              			WHERE 1=1 
        				AND A.CREATED_DATE >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND A.CREATED_DATE < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1";
						if($no_kel == 0){    
					$sql .=" AND C.NO_PROP = 32 AND C.NO_KAB = 73 AND C.NO_KEC =".$no_kec."
						    GROUP BY C.NO_KEL, C.NAMA_KEL
						    ORDER BY C.NO_KEL, C.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND C.NO_PROP = 32 AND C.NO_KAB = 73 AND C.NO_KEC =".$no_kec." AND C.NO_KEL =".$no_kel."
						    GROUP BY C.NO_KEL, C.NAMA_KEL
						    ORDER BY C.NO_KEL, C.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::connection('db222')->select($sql);
			return $r;


		}

		
		public function get_data_surat_pindah($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL, '$tgl' AS TANGGAL, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH, CASE WHEN B.JUMLAH_ANGGOTA IS NULL THEN 0 ELSE B.JUMLAH_ANGGOTA END AS JUMLAH_ANGGOTA
					FROM SETUP_KEC A LEFT JOIN
					(SELECT 
					    C.NO_KEC
					    , C.NAMA_KEC
					    , COUNT(DISTINCT(A.NO_PINDAH)) AS JUMLAH
             			, COUNT(A.NO_PINDAH) AS JUMLAH_ANGGOTA
              			FROM PINDAH_HEADER A INNER JOIN PINDAH_DETAIL B ON A.NO_PINDAH = B.NO_PINDAH INNER JOIN SETUP_KEC C
					    ON A.FROM_NO_PROP = C.NO_PROP 
					    AND A.FROM_NO_KAB = C.NO_KAB 
					    AND A.FROM_NO_KEC = C.NO_KEC 
              			WHERE 1=1 
              			AND A.KLASIFIKASI_PINDAH >3
        				AND A.CREATED_DATE >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND A.CREATED_DATE < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1
					    AND
					    C.NO_PROP = 32 AND C.NO_KAB = 73
					    GROUP BY C.NO_KEC, C.NAMA_KEC
					    ORDER BY C.NO_KEC, C.NAMA_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL, '$tgl' AS TANGGAL, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH, CASE WHEN B.JUMLAH_ANGGOTA IS NULL THEN 0 ELSE B.JUMLAH_ANGGOTA END AS JUMLAH_ANGGOTA
					FROM SETUP_KEL A LEFT JOIN
					(SELECT 
					    C.NO_KEL
					    , C.NAMA_KEL
					    , COUNT(DISTINCT(A.NO_PINDAH)) AS JUMLAH
             			, COUNT(A.NO_PINDAH) AS JUMLAH_ANGGOTA
              			FROM PINDAH_HEADER A INNER JOIN PINDAH_DETAIL B ON A.NO_PINDAH = B.NO_PINDAH INNER JOIN SETUP_KEL C
					    ON A.FROM_NO_PROP = C.NO_PROP 
					    AND A.FROM_NO_KAB = C.NO_KAB 
					    AND A.FROM_NO_KEC = C.NO_KEC 
					    AND A.FROM_NO_KEL = C.NO_KEL 
              			WHERE 1=1 
              			AND A.KLASIFIKASI_PINDAH >3
        				AND A.CREATED_DATE >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND A.CREATED_DATE < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1";
						if($no_kel == 0){    
					$sql .=" AND C.NO_PROP = 32 AND C.NO_KAB = 73 AND C.NO_KEC =".$no_kec."
						    GROUP BY C.NO_KEL, C.NAMA_KEL
						    ORDER BY C.NO_KEL, C.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND C.NO_PROP = 32 AND C.NO_KAB = 73 AND C.NO_KEC =".$no_kec." AND C.NO_KEL =".$no_kel."
						    GROUP BY C.NO_KEL, C.NAMA_KEL
						    ORDER BY C.NO_KEL, C.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::connection('db222')->select($sql);
			return $r;


		}

		
		public function get_data_surat_pindah_kec($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL, '$tgl' AS TANGGAL, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH, CASE WHEN B.JUMLAH_ANGGOTA IS NULL THEN 0 ELSE B.JUMLAH_ANGGOTA END AS JUMLAH_ANGGOTA
					FROM SETUP_KEC A LEFT JOIN
					(SELECT 
					    C.NO_KEC
					    , C.NAMA_KEC
					    , COUNT(DISTINCT(A.NO_PINDAH)) AS JUMLAH
             			, COUNT(A.NO_PINDAH) AS JUMLAH_ANGGOTA
              			FROM PINDAH_HEADER A INNER JOIN PINDAH_DETAIL B ON A.NO_PINDAH = B.NO_PINDAH INNER JOIN SETUP_KEC C
					    ON A.FROM_NO_PROP = C.NO_PROP 
					    AND A.FROM_NO_KAB = C.NO_KAB 
					    AND A.FROM_NO_KEC = C.NO_KEC 
              			WHERE 1=1 
              			AND A.KLASIFIKASI_PINDAH =3
        				AND A.CREATED_DATE >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND A.CREATED_DATE < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1
					    AND
					    C.NO_PROP = 32 AND C.NO_KAB = 73
					    GROUP BY C.NO_KEC, C.NAMA_KEC
					    ORDER BY C.NO_KEC, C.NAMA_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL, '$tgl' AS TANGGAL, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH, CASE WHEN B.JUMLAH_ANGGOTA IS NULL THEN 0 ELSE B.JUMLAH_ANGGOTA END AS JUMLAH_ANGGOTA
					FROM SETUP_KEL A LEFT JOIN
					(SELECT 
					    C.NO_KEL
					    , C.NAMA_KEL
					    , COUNT(DISTINCT(A.NO_PINDAH)) AS JUMLAH
             			, COUNT(A.NO_PINDAH) AS JUMLAH_ANGGOTA
              			FROM PINDAH_HEADER A INNER JOIN PINDAH_DETAIL B ON A.NO_PINDAH = B.NO_PINDAH INNER JOIN SETUP_KEL C
					    ON A.FROM_NO_PROP = C.NO_PROP 
					    AND A.FROM_NO_KAB = C.NO_KAB 
					    AND A.FROM_NO_KEC = C.NO_KEC 
					    AND A.FROM_NO_KEL = C.NO_KEL 
              			WHERE 1=1 
              			AND A.KLASIFIKASI_PINDAH =3
        				AND A.CREATED_DATE >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND A.CREATED_DATE < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1";
						if($no_kel == 0){    
					$sql .=" AND C.NO_PROP = 32 AND C.NO_KAB = 73 AND C.NO_KEC =".$no_kec."
						    GROUP BY C.NO_KEL, C.NAMA_KEL
						    ORDER BY C.NO_KEL, C.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND C.NO_PROP = 32 AND C.NO_KAB = 73 AND C.NO_KEC =".$no_kec." AND C.NO_KEL =".$no_kel."
						    GROUP BY C.NO_KEL, C.NAMA_KEL
						    ORDER BY C.NO_KEL, C.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::connection('db222')->select($sql);
			return $r;


		}

		
		public function get_data_surat_pindah_kel($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL, '$tgl' AS TANGGAL, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH, CASE WHEN B.JUMLAH_ANGGOTA IS NULL THEN 0 ELSE B.JUMLAH_ANGGOTA END AS JUMLAH_ANGGOTA
					FROM SETUP_KEC A LEFT JOIN
					(SELECT 
					    C.NO_KEC
					    , C.NAMA_KEC
					    , COUNT(DISTINCT(A.NO_PINDAH)) AS JUMLAH
             			, COUNT(A.NO_PINDAH) AS JUMLAH_ANGGOTA
              			FROM PINDAH_HEADER A INNER JOIN PINDAH_DETAIL B ON A.NO_PINDAH = B.NO_PINDAH INNER JOIN SETUP_KEC C
					    ON A.FROM_NO_PROP = C.NO_PROP 
					    AND A.FROM_NO_KAB = C.NO_KAB 
					    AND A.FROM_NO_KEC = C.NO_KEC 
              			WHERE 1=1 
              			AND A.KLASIFIKASI_PINDAH <3
        				AND A.CREATED_DATE >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND A.CREATED_DATE < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1
					    AND
					    C.NO_PROP = 32 AND C.NO_KAB = 73
					    GROUP BY C.NO_KEC, C.NAMA_KEC
					    ORDER BY C.NO_KEC, C.NAMA_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL, '$tgl' AS TANGGAL, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH, CASE WHEN B.JUMLAH_ANGGOTA IS NULL THEN 0 ELSE B.JUMLAH_ANGGOTA END AS JUMLAH_ANGGOTA
					FROM SETUP_KEL A LEFT JOIN
					(SELECT 
					    C.NO_KEL
					    , C.NAMA_KEL
					    , COUNT(DISTINCT(A.NO_PINDAH)) AS JUMLAH
             			, COUNT(A.NO_PINDAH) AS JUMLAH_ANGGOTA
              			FROM PINDAH_HEADER A INNER JOIN PINDAH_DETAIL B ON A.NO_PINDAH = B.NO_PINDAH INNER JOIN SETUP_KEL C
					    ON A.FROM_NO_PROP = C.NO_PROP 
					    AND A.FROM_NO_KAB = C.NO_KAB 
					    AND A.FROM_NO_KEC = C.NO_KEC 
					    AND A.FROM_NO_KEL = C.NO_KEL 
              			WHERE 1=1 
              			AND A.KLASIFIKASI_PINDAH <3
        				AND A.CREATED_DATE >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND A.CREATED_DATE < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1";
						if($no_kel == 0){    
					$sql .=" AND C.NO_PROP = 32 AND C.NO_KAB = 73 AND C.NO_KEC =".$no_kec."
						    GROUP BY C.NO_KEL, C.NAMA_KEL
						    ORDER BY C.NO_KEL, C.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND C.NO_PROP = 32 AND C.NO_KAB = 73 AND C.NO_KEC =".$no_kec." AND C.NO_KEL =".$no_kel."
						    GROUP BY C.NO_KEL, C.NAMA_KEL
						    ORDER BY C.NO_KEL, C.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::connection('db222')->select($sql);
			return $r;


		}

		
		public function get_data_surat_datang_all($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL, '$tgl' AS TANGGAL, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH, CASE WHEN B.JUMLAH_ANGGOTA IS NULL THEN 0 ELSE B.JUMLAH_ANGGOTA END AS JUMLAH_ANGGOTA
					FROM SETUP_KEC A LEFT JOIN
					(SELECT 
					    C.NO_KEC
					    , C.NAMA_KEC
					    , COUNT(DISTINCT(A.NO_DATANG)) AS JUMLAH
             			, COUNT(A.NO_DATANG) AS JUMLAH_ANGGOTA
              			FROM DATANG_HEADER A INNER JOIN DATANG_DETAIL B ON A.NO_DATANG = B.NO_DATANG INNER JOIN SETUP_KEC C
					    ON A.NO_PROP = C.NO_PROP 
					    AND A.NO_KAB = C.NO_KAB 
					    AND A.NO_KEC = C.NO_KEC 
              			WHERE 1=1 
        				AND A.CREATED_DATE >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND A.CREATED_DATE < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1
					    AND
					    C.NO_PROP = 32 AND C.NO_KAB = 73
					    GROUP BY C.NO_KEC, C.NAMA_KEC
					    ORDER BY C.NO_KEC, C.NAMA_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL, '$tgl' AS TANGGAL, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH, CASE WHEN B.JUMLAH_ANGGOTA IS NULL THEN 0 ELSE B.JUMLAH_ANGGOTA END AS JUMLAH_ANGGOTA
					FROM SETUP_KEL A LEFT JOIN
					(SELECT 
					    C.NO_KEL
					    , C.NAMA_KEL
					    , COUNT(DISTINCT(A.NO_DATANG)) AS JUMLAH
             			, COUNT(A.NO_DATANG) AS JUMLAH_ANGGOTA
              			FROM DATANG_HEADER A INNER JOIN DATANG_DETAIL B ON A.NO_DATANG = B.NO_DATANG INNER JOIN SETUP_KEL C
					    ON A.NO_PROP = C.NO_PROP 
					    AND A.NO_KAB = C.NO_KAB 
					    AND A.NO_KEC = C.NO_KEC 
					    AND A.NO_KEL = C.NO_KEL 
              			WHERE 1=1 
        				AND A.CREATED_DATE >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND A.CREATED_DATE < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1";
						if($no_kel == 0){    
					$sql .=" AND C.NO_PROP = 32 AND C.NO_KAB = 73 AND C.NO_KEC =".$no_kec."
						    GROUP BY C.NO_KEL, C.NAMA_KEL
						    ORDER BY C.NO_KEL, C.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND C.NO_PROP = 32 AND C.NO_KAB = 73 AND C.NO_KEC =".$no_kec." AND C.NO_KEL =".$no_kel."
						    GROUP BY C.NO_KEL, C.NAMA_KEL
						    ORDER BY C.NO_KEL, C.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::connection('db222')->select($sql);
			return $r;


		}

		
		public function get_data_surat_datang($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL, '$tgl' AS TANGGAL, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH, CASE WHEN B.JUMLAH_ANGGOTA IS NULL THEN 0 ELSE B.JUMLAH_ANGGOTA END AS JUMLAH_ANGGOTA
					FROM SETUP_KEC A LEFT JOIN
					(SELECT 
					    C.NO_KEC
					    , C.NAMA_KEC
					    , COUNT(DISTINCT(A.NO_DATANG)) AS JUMLAH
             			, COUNT(A.NO_DATANG) AS JUMLAH_ANGGOTA
              			FROM DATANG_HEADER A INNER JOIN DATANG_DETAIL B ON A.NO_DATANG = B.NO_DATANG INNER JOIN SETUP_KEC C
					    ON A.NO_PROP = C.NO_PROP 
					    AND A.NO_KAB = C.NO_KAB 
					    AND A.NO_KEC = C.NO_KEC 
              			WHERE 1=1 
              			AND A.KLASIFIKASI_PINDAH >3
        				AND A.CREATED_DATE >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND A.CREATED_DATE < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1
					    AND
					    C.NO_PROP = 32 AND C.NO_KAB = 73
					    GROUP BY C.NO_KEC, C.NAMA_KEC
					    ORDER BY C.NO_KEC, C.NAMA_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL, '$tgl' AS TANGGAL, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH, CASE WHEN B.JUMLAH_ANGGOTA IS NULL THEN 0 ELSE B.JUMLAH_ANGGOTA END AS JUMLAH_ANGGOTA
					FROM SETUP_KEL A LEFT JOIN
					(SELECT 
					    C.NO_KEL
					    , C.NAMA_KEL
					    , COUNT(DISTINCT(A.NO_DATANG)) AS JUMLAH
             			, COUNT(A.NO_DATANG) AS JUMLAH_ANGGOTA
              			FROM DATANG_HEADER A INNER JOIN DATANG_DETAIL B ON A.NO_DATANG = B.NO_DATANG INNER JOIN SETUP_KEL C
					    ON A.NO_PROP = C.NO_PROP 
					    AND A.NO_KAB = C.NO_KAB 
					    AND A.NO_KEC = C.NO_KEC 
					    AND A.NO_KEL = C.NO_KEL 
              			WHERE 1=1 
              			AND A.KLASIFIKASI_PINDAH >3
        				AND A.CREATED_DATE >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND A.CREATED_DATE < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1";
						if($no_kel == 0){    
					$sql .=" AND C.NO_PROP = 32 AND C.NO_KAB = 73 AND C.NO_KEC =".$no_kec."
						    GROUP BY C.NO_KEL, C.NAMA_KEL
						    ORDER BY C.NO_KEL, C.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND C.NO_PROP = 32 AND C.NO_KAB = 73 AND C.NO_KEC =".$no_kec." AND C.NO_KEL =".$no_kel."
						    GROUP BY C.NO_KEL, C.NAMA_KEL
						    ORDER BY C.NO_KEL, C.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::connection('db222')->select($sql);
			return $r;


		}

		
		public function get_data_surat_datang_kec($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL, '$tgl' AS TANGGAL, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH, CASE WHEN B.JUMLAH_ANGGOTA IS NULL THEN 0 ELSE B.JUMLAH_ANGGOTA END AS JUMLAH_ANGGOTA
					FROM SETUP_KEC A LEFT JOIN
					(SELECT 
					    C.NO_KEC
					    , C.NAMA_KEC
					    , COUNT(DISTINCT(A.NO_DATANG)) AS JUMLAH
             			, COUNT(A.NO_DATANG) AS JUMLAH_ANGGOTA
              			FROM DATANG_HEADER A INNER JOIN DATANG_DETAIL B ON A.NO_DATANG = B.NO_DATANG INNER JOIN SETUP_KEC C
					    ON A.NO_PROP = C.NO_PROP 
					    AND A.NO_KAB = C.NO_KAB 
					    AND A.NO_KEC = C.NO_KEC 
              			WHERE 1=1 
              			AND A.KLASIFIKASI_PINDAH =3
        				AND A.CREATED_DATE >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND A.CREATED_DATE < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1
					    AND
					    C.NO_PROP = 32 AND C.NO_KAB = 73
					    GROUP BY C.NO_KEC, C.NAMA_KEC
					    ORDER BY C.NO_KEC, C.NAMA_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL, '$tgl' AS TANGGAL, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH, CASE WHEN B.JUMLAH_ANGGOTA IS NULL THEN 0 ELSE B.JUMLAH_ANGGOTA END AS JUMLAH_ANGGOTA
					FROM SETUP_KEL A LEFT JOIN
					(SELECT 
					    C.NO_KEL
					    , C.NAMA_KEL
					    , COUNT(DISTINCT(A.NO_DATANG)) AS JUMLAH
             			, COUNT(A.NO_DATANG) AS JUMLAH_ANGGOTA
              			FROM DATANG_HEADER A INNER JOIN DATANG_DETAIL B ON A.NO_DATANG = B.NO_DATANG INNER JOIN SETUP_KEL C
					    ON A.NO_PROP = C.NO_PROP 
					    AND A.NO_KAB = C.NO_KAB 
					    AND A.NO_KEC = C.NO_KEC 
					    AND A.NO_KEL = C.NO_KEL 
              			WHERE 1=1 
              			AND A.KLASIFIKASI_PINDAH =3
        				AND A.CREATED_DATE >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND A.CREATED_DATE < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1";
						if($no_kel == 0){    
					$sql .=" AND C.NO_PROP = 32 AND C.NO_KAB = 73 AND C.NO_KEC =".$no_kec."
						    GROUP BY C.NO_KEL, C.NAMA_KEL
						    ORDER BY C.NO_KEL, C.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND C.NO_PROP = 32 AND C.NO_KAB = 73 AND C.NO_KEC =".$no_kec." AND C.NO_KEL =".$no_kel."
						    GROUP BY C.NO_KEL, C.NAMA_KEL
						    ORDER BY C.NO_KEL, C.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::connection('db222')->select($sql);
			return $r;


		}

		public function get_data_surat_datang_kel($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL, '$tgl' AS TANGGAL, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH, CASE WHEN B.JUMLAH_ANGGOTA IS NULL THEN 0 ELSE B.JUMLAH_ANGGOTA END AS JUMLAH_ANGGOTA
					FROM SETUP_KEC A LEFT JOIN
					(SELECT 
					    C.NO_KEC
					    , C.NAMA_KEC
					    , COUNT(DISTINCT(A.NO_DATANG)) AS JUMLAH
             			, COUNT(A.NO_DATANG) AS JUMLAH_ANGGOTA
              			FROM DATANG_HEADER A INNER JOIN DATANG_DETAIL B ON A.NO_DATANG = B.NO_DATANG INNER JOIN SETUP_KEC C
					    ON A.NO_PROP = C.NO_PROP 
					    AND A.NO_KAB = C.NO_KAB 
					    AND A.NO_KEC = C.NO_KEC 
              			WHERE 1=1 
              			AND A.KLASIFIKASI_PINDAH <3
        				AND A.CREATED_DATE >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND A.CREATED_DATE < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1
					    AND
					    C.NO_PROP = 32 AND C.NO_KAB = 73
					    GROUP BY C.NO_KEC, C.NAMA_KEC
					    ORDER BY C.NO_KEC, C.NAMA_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL, '$tgl' AS TANGGAL, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH, CASE WHEN B.JUMLAH_ANGGOTA IS NULL THEN 0 ELSE B.JUMLAH_ANGGOTA END AS JUMLAH_ANGGOTA
					FROM SETUP_KEL A LEFT JOIN
					(SELECT 
					    C.NO_KEL
					    , C.NAMA_KEL
					    , COUNT(DISTINCT(A.NO_DATANG)) AS JUMLAH
             			, COUNT(A.NO_DATANG) AS JUMLAH_ANGGOTA
              			FROM DATANG_HEADER A INNER JOIN DATANG_DETAIL B ON A.NO_DATANG = B.NO_DATANG INNER JOIN SETUP_KEL C
					    ON A.NO_PROP = C.NO_PROP 
					    AND A.NO_KAB = C.NO_KAB 
					    AND A.NO_KEC = C.NO_KEC 
					    AND A.NO_KEL = C.NO_KEL 
              			WHERE 1=1 
              			AND A.KLASIFIKASI_PINDAH <3
        				AND A.CREATED_DATE >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND A.CREATED_DATE < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1";
						if($no_kel == 0){    
					$sql .=" AND C.NO_PROP = 32 AND C.NO_KAB = 73 AND C.NO_KEC =".$no_kec."
						    GROUP BY C.NO_KEL, C.NAMA_KEL
						    ORDER BY C.NO_KEL, C.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND C.NO_PROP = 32 AND C.NO_KAB = 73 AND C.NO_KEC =".$no_kec." AND C.NO_KEL =".$no_kel."
						    GROUP BY C.NO_KEL, C.NAMA_KEL
						    ORDER BY C.NO_KEL, C.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::connection('db222')->select($sql);
			return $r;


		}
		public function get_data_kia($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL, '$tgl' AS TANGGAL, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEC A LEFT JOIN
					(SELECT 
						C.NO_KEC,
						C.NAMA_KEC,
						COUNT(1) JUMLAH
						FROM T5_SEQN_KIA_PRINT B
						INNER JOIN SETUP_KEC C 
						ON C.NO_PROP = B.NO_PROP
						AND C.NO_KAB = B.NO_KAB
						AND C.NO_KEC = B.NO_KEC
						WHERE 
						B.PRINT_TYPE =0 AND
						B.PRINTED_DATE >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND B.PRINTED_DATE < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1
						GROUP BY C.NO_KEC, C.NAMA_KEC ORDER BY C.NO_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL, '$tgl' AS TANGGAL, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
						FROM SETUP_KEL A LEFT JOIN
						(SELECT 
							C.NO_KEL,
							C.NAMA_KEL,
							COUNT(1) JUMLAH
							FROM T5_SEQN_KIA_PRINT B
							INNER JOIN SETUP_KEL C 
							ON C.NO_PROP = B.NO_PROP
							AND C.NO_KAB = B.NO_KAB
							AND C.NO_KEC = B.NO_KEC
							AND C.NO_KEL = B.NO_KEL
							WHERE 
							B.PRINT_TYPE =0 AND
							B.PRINTED_DATE >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND B.PRINTED_DATE < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1";
						if($no_kel == 0){    
					$sql .=" AND C.NO_PROP = 32 AND C.NO_KAB = 73 AND C.NO_KEC =".$no_kec."
						    GROUP BY C.NO_KEL, C.NAMA_KEL
						    ORDER BY C.NO_KEL, C.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND C.NO_PROP = 32 AND C.NO_KAB = 73 AND C.NO_KEC =".$no_kec." and  C.NO_KEL =".$no_kel."
						    GROUP BY C.NO_KEL, C.NAMA_KEL
						    ORDER BY C.NO_KEL, C.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				
				$r = DB::connection('db222')->select($sql);
				return $r;


		}
		public function get_data_kelahiran($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel,$jns_lap,$usia_lap){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL, '$tgl' AS TANGGAL, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH, CASE WHEN B.LK IS NULL THEN 0 ELSE B.LK END AS LK, CASE WHEN B.PR IS NULL THEN 0 ELSE B.PR END AS PR
					FROM SETUP_KEC A LEFT JOIN
					(SELECT 
					    C.NO_KEC
					    , C.NAMA_KEC
					    , SUM(CASE WHEN A.BAYI_JNS_KELAMIN = 1 THEN 1 ELSE 0 END) AS LK
					    , SUM(CASE WHEN A.BAYI_JNS_KELAMIN <> 1 OR A.BAYI_JNS_KELAMIN IS NULL THEN 1 ELSE 0 END) AS PR
					    , COUNT(1) AS JUMLAH
              			FROM CAPIL_LAHIR A INNER JOIN SETUP_KEC C
              ON A.ADM_NO_PROV = C.NO_PROP 
              AND A.ADM_NO_KAB = C.NO_KAB 
              AND A.ADM_NO_KEC = C.NO_KEC 
                    WHERE 1=1  AND A.ADM_NO_PROV=32 AND A.ADM_NO_KAB=73 AND A.BAYI_ORG_ASING = 'T' AND A.BAYI_DOM_LAHIR = 'D'";
          	if($jns_lap == 1){
              	$sql .= " AND A.ADM_AKTA_NO LIKE '%LU%' ";
          	}else if ($jns_lap == 2){
          		$sql .= " AND A.ADM_AKTA_NO LIKE '%LT%' ";
          	}
            if($usia_lap == 1){
                $sql .= " AND TRUNC(MONTHS_BETWEEN(A.ADM_TGL_ENTRY,A.BAYI_TGL_LAHIR)/12) <= 18";
            }else if ($usia_lap == 2){
              $sql .= " AND TRUNC(MONTHS_BETWEEN(A.ADM_TGL_ENTRY,A.BAYI_TGL_LAHIR)/12) > 18";
            }
        		$sql .= " AND A.ADM_TGL_ENTRY >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND A.ADM_TGL_ENTRY < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1
					    AND
					    C.NO_PROP = 32 AND C.NO_KAB = 73
					    GROUP BY C.NO_KEC, C.NAMA_KEC
					    ORDER BY C.NO_KEC, C.NAMA_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL, '$tgl' AS TANGGAL, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH, CASE WHEN B.LK IS NULL THEN 0 ELSE B.LK END AS LK, CASE WHEN B.PR IS NULL THEN 0 ELSE B.PR END AS PR
					FROM SETUP_KEL A LEFT JOIN
					(SELECT 
					    C.NO_KEL
					    , C.NAMA_KEL
					    , SUM(CASE WHEN A.BAYI_JNS_KELAMIN = 1 THEN 1 ELSE 0 END) AS LK
					    , SUM(CASE WHEN A.BAYI_JNS_KELAMIN <> 1 OR A.BAYI_JNS_KELAMIN IS NULL THEN 1 ELSE 0 END) AS PR
					    , COUNT(1) AS JUMLAH
              			FROM CAPIL_LAHIR A INNER JOIN SETUP_KEL C
              ON A.ADM_NO_PROV = C.NO_PROP 
              AND A.ADM_NO_KAB = C.NO_KAB 
              AND A.ADM_NO_KEC = C.NO_KEC 
              AND A.ADM_NO_KEL = C.NO_KEL
                    WHERE 1=1  AND A.ADM_NO_PROV=32 AND A.ADM_NO_KAB=73 AND A.BAYI_ORG_ASING = 'T' AND A.BAYI_DOM_LAHIR = 'D'";
          	if($jns_lap == 1){
              	$sql .= " AND A.ADM_AKTA_NO LIKE '%LU%' ";
          	}else if ($jns_lap == 2){
          		$sql .= " AND A.ADM_AKTA_NO LIKE '%LT%' ";
          	}
            if($usia_lap == 1){
                $sql .= " AND TRUNC(MONTHS_BETWEEN(A.ADM_TGL_ENTRY,A.BAYI_TGL_LAHIR)/12) <= 18";
            }else if ($usia_lap == 2){
              $sql .= " AND TRUNC(MONTHS_BETWEEN(A.ADM_TGL_ENTRY,A.BAYI_TGL_LAHIR)/12) > 18";
            }
        		$sql .= " AND A.ADM_TGL_ENTRY >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND A.ADM_TGL_ENTRY < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1";
						if($no_kel == 0){    
					$sql .=" AND C.NO_PROP = 32 AND C.NO_KAB = 73 AND C.NO_KEC =".$no_kec."
						    GROUP BY C.NO_KEL, C.NAMA_KEL
						    ORDER BY C.NO_KEL, C.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND C.NO_PROP = 32 AND C.NO_KAB = 73 AND C.NO_KEC =".$no_kec." AND C.NO_KEL =".$no_kel."
						    GROUP BY C.NO_KEL, C.NAMA_KEL
						    ORDER BY C.NO_KEL, C.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::connection('db222')->select($sql);
				return $r;


		}
		public function get_data_perceraian($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "SELECT 
						A.NO AS NO_WIL
						, UPPER(A.DESCRIP) AS NAMA_WIL
						, '$tgl' AS TANGGAL
						, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH 
					FROM 
						REF_SIAK_WNI A
					LEFT JOIN 
						(
							SELECT 
								CERAI_SEBAB
								, COUNT(1) AS JUMLAH 
							FROM
								CAPIL_CERAI
							WHERE 
								1=1
								AND ADM_NO_PROV=32 
								AND ADM_NO_KAB=73 
								AND CERAI_TGL_LAPOR >= TO_DATE('".$tgl_start."','DD-MM-YYYY') 
								AND CERAI_TGL_LAPOR < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1
               				GROUP BY 
               					CERAI_SEBAB
               			) B 
               		ON A.NO = B.CERAI_SEBAB 
					WHERE A.SECT =621
               		ORDER BY A.NO";
				$r = DB::connection('db222')->select($sql);
				return $r;


		}
		public function get_data_perkawinan($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "SELECT 
						A.NO AS NO_WIL
						, UPPER(A.DESCRIP) AS NAMA_WIL
						, '$tgl' AS TANGGAL
						, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH 
					FROM 
						REF_SIAK_WNI A
					LEFT JOIN 
						(
							SELECT 
								KAWIN_AGAMA
								, COUNT(1) AS JUMLAH 
							FROM
								CAPIL_KAWIN
							WHERE 
								1=1
								AND ADM_NO_PROV=32 
								AND ADM_NO_KAB=73 
								AND KAWIN_TGL_LAPOR >= TO_DATE('".$tgl_start."','DD-MM-YYYY') 
								AND KAWIN_TGL_LAPOR < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1
               				GROUP BY 
               					KAWIN_AGAMA
               			) B 
               		ON A.NO = B.KAWIN_AGAMA 
					WHERE A.SECT =511
               		ORDER BY A.NO";
				$r = DB::connection('db222')->select($sql);
				return $r;


		}
		public function get_data_perkawinan_campuran($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "SELECT 
						A.NO AS NO_WIL
						, UPPER(A.DESCRIP) AS NAMA_WIL
						, '$tgl' AS TANGGAL
						, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH 
					FROM 
						REF_SIAK_WNI A
					LEFT JOIN 
						(
							SELECT 
								KAWIN_AGAMA
								, COUNT(1) AS JUMLAH 
							FROM
								CAPIL_KAWIN
							WHERE 
								((SUAMI_WRG_NGR =2 AND ISTRI_WRG_NGR =1) OR (SUAMI_WRG_NGR =1 AND ISTRI_WRG_NGR =2))
								AND ADM_NO_PROV=32 
								AND ADM_NO_KAB=73 
								AND KAWIN_TGL_LAPOR >= TO_DATE('".$tgl_start."','DD-MM-YYYY') 
								AND KAWIN_TGL_LAPOR < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1
               				GROUP BY 
               					KAWIN_AGAMA
               			) B 
               		ON A.NO = B.KAWIN_AGAMA 
					WHERE A.SECT =511
               		ORDER BY A.NO";
				$r = DB::connection('db222')->select($sql);
				return $r;


		}
		public function get_data_kematian($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL, '$tgl' AS TANGGAL, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH, CASE WHEN B.LK IS NULL THEN 0 ELSE B.LK END AS LK, CASE WHEN B.PR IS NULL THEN 0 ELSE B.PR END AS PR
					FROM SETUP_KEC A LEFT JOIN
					(SELECT 
					    C.NO_KEC
					    , C.NAMA_KEC
					    , SUM(CASE WHEN TRUNC(ADM_TGL_ENTRY) - TRUNC(MATI_TGL_MATI) <= 30 THEN 1 ELSE 0 END) AS LK
					    , SUM(CASE WHEN TRUNC(ADM_TGL_ENTRY) - TRUNC(MATI_TGL_MATI) > 30 THEN 1 ELSE 0 END) AS PR
					    , COUNT(1) AS JUMLAH
              			FROM CAPIL_MATI A INNER JOIN SETUP_KEC C
					    ON A.ADM_NO_PROV = C.NO_PROP 
					    AND A.ADM_NO_KAB = C.NO_KAB 
					    AND A.ADM_NO_KEC = C.NO_KEC 
              			WHERE 1=1 AND MATI_LUAR_NEGERI = 'T' AND MATI_DOM_MATI = 'D'
        				AND A.ADM_TGL_ENTRY >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND A.ADM_TGL_ENTRY < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1
					    AND
					    C.NO_PROP = 32 AND C.NO_KAB = 73
					    GROUP BY C.NO_KEC, C.NAMA_KEC
					    ORDER BY C.NO_KEC, C.NAMA_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL, '$tgl' AS TANGGAL, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH, CASE WHEN B.LK IS NULL THEN 0 ELSE B.LK END AS LK, CASE WHEN B.PR IS NULL THEN 0 ELSE B.PR END AS PR
					FROM SETUP_KEL A LEFT JOIN
					(SELECT 
					    C.NO_KEL
					    , C.NAMA_KEL
					    , SUM(CASE WHEN TRUNC(ADM_TGL_ENTRY) - TRUNC(MATI_TGL_MATI) <= 30 THEN 1 ELSE 0 END) AS LK
					    , SUM(CASE WHEN TRUNC(ADM_TGL_ENTRY) - TRUNC(MATI_TGL_MATI) > 30 THEN 1 ELSE 0 END) AS PR
					    , COUNT(1) AS JUMLAH
              			FROM CAPIL_MATI A INNER JOIN SETUP_KEL C
					    ON A.ADM_NO_PROV = C.NO_PROP 
					    AND A.ADM_NO_KAB = C.NO_KAB 
					    AND A.ADM_NO_KEC = C.NO_KEC 
					    AND A.ADM_NO_KEL = C.NO_KEL 
              			WHERE 1=1 AND MATI_LUAR_NEGERI = 'T' AND MATI_DOM_MATI = 'D'
        				AND A.ADM_TGL_ENTRY >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND A.ADM_TGL_ENTRY < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1";
						if($no_kel == 0){    
					$sql .=" AND C.NO_PROP = 32 AND C.NO_KAB = 73 AND C.NO_KEC =".$no_kec."
						    GROUP BY C.NO_KEL, C.NAMA_KEL
						    ORDER BY C.NO_KEL, C.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND C.NO_PROP = 32 AND C.NO_KAB = 73 AND C.NO_KEC =".$no_kec." AND C.NO_KEL =".$no_kel."
						    GROUP BY C.NO_KEL, C.NAMA_KEL
						    ORDER BY C.NO_KEL, C.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::connection('db222')->select($sql);
				return $r;


		}

		public function get_data_kematian_asing($tgl,$tgl_start,$tgl_end){
			$sql = "";
			$sql .="SELECT A.MATI_NAMA_LGKP,  TO_CHAR(A.MATI_TGL_MATI,'DD-MM-YYYY') MATI_TGL_MATI, A.ADM_AKTA_NO, TO_CHAR(A.ADM_TGL_ENTRY,'DD-MM-YYYY') ADM_TGL_ENTRY FROM CAPIL_MATI A WHERE A.MATI_WRG_NGR = 2
					 AND A.ADM_TGL_ENTRY >= TO_DATE('".$tgl_start."','DD-MM-YYYY') 
					 AND A.ADM_TGL_ENTRY < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1
					  ORDER BY A.ADM_TGL_ENTRY DESC";
				$r = DB::connection('db222')->select($sql);
				return $r;


		}
		public function get_data_pengangkatan_anak($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL, '$tgl' AS TANGGAL, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEC A LEFT JOIN
					(SELECT 
					    C.NO_KEC
					    , C.NAMA_KEC
					    , COUNT(DISTINCT(A.ANAK_NO)) AS JUMLAH
              			FROM CAPIL_ANGKAT_ANAK A INNER JOIN SETUP_KEC C
					    ON A.ADM_NO_PROV = C.NO_PROP 
					    AND A.ADM_NO_KAB = C.NO_KAB 
					    AND A.ADM_NO_KEC = C.NO_KEC 
              			WHERE 1=1 AND ANAK_WNA = 'T'
        				AND A.ADM_TGL_ENTRY >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND A.ADM_TGL_ENTRY < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1
					    AND
					    C.NO_PROP = 32 AND C.NO_KAB = 73
					    GROUP BY C.NO_KEC, C.NAMA_KEC
					    ORDER BY C.NO_KEC, C.NAMA_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL, '$tgl' AS TANGGAL, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEL A LEFT JOIN
					(SELECT 
					    C.NO_KEL
					    , C.NAMA_KEL
					    , COUNT(DISTINCT(A.ANAK_NO)) AS JUMLAH
              			FROM CAPIL_ANGKAT_ANAK A INNER JOIN SETUP_KEL C
					    ON A.ADM_NO_PROV = C.NO_PROP 
					    AND A.ADM_NO_KAB = C.NO_KAB 
					    AND A.ADM_NO_KEC = C.NO_KEC 
					    AND A.ADM_NO_KEL = C.NO_KEL 
              			WHERE 1=1 AND ANAK_WNA = 'T'
        				AND A.ADM_TGL_ENTRY >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND A.ADM_TGL_ENTRY < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1";
						if($no_kel == 0){    
					$sql .=" AND C.NO_PROP = 32 AND C.NO_KAB = 73 AND C.NO_KEC =".$no_kec."
						    GROUP BY C.NO_KEL, C.NAMA_KEL
						    ORDER BY C.NO_KEL, C.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND C.NO_PROP = 32 AND C.NO_KAB = 73 AND C.NO_KEC =".$no_kec." AND C.NO_KEL =".$no_kel."
						    GROUP BY C.NO_KEL, C.NAMA_KEL
						    ORDER BY C.NO_KEL, C.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::connection('db222')->select($sql);
				return $r;


		}
		public function get_data_pengesahan_anak($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL, '$tgl' AS TANGGAL, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEC A LEFT JOIN
					(SELECT 
					    C.NO_KEC
					    , C.NAMA_KEC
					    , COUNT(DISTINCT(A.ANAK_NO)) AS JUMLAH
              			FROM CAPIL_SAH_ANAK A INNER JOIN SETUP_KEC C
					    ON A.ADM_NO_PROV = C.NO_PROP 
					    AND A.ADM_NO_KAB = C.NO_KAB 
					    AND A.ADM_NO_KEC = C.NO_KEC 
              			WHERE 1=1 AND ANAK_WNA = 'T'
        				AND A.ADM_TGL_ENTRY >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND A.ADM_TGL_ENTRY < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1
					    AND
					    C.NO_PROP = 32 AND C.NO_KAB = 73
					    GROUP BY C.NO_KEC, C.NAMA_KEC
					    ORDER BY C.NO_KEC, C.NAMA_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL, '$tgl' AS TANGGAL, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEL A LEFT JOIN
					(SELECT 
					    C.NO_KEL
					    , C.NAMA_KEL
					    , COUNT(DISTINCT(A.ANAK_NO)) AS JUMLAH
              			FROM CAPIL_SAH_ANAK A INNER JOIN SETUP_KEL C
					    ON A.ADM_NO_PROV = C.NO_PROP 
					    AND A.ADM_NO_KAB = C.NO_KAB 
					    AND A.ADM_NO_KEC = C.NO_KEC 
					    AND A.ADM_NO_KEL = C.NO_KEL 
              			WHERE 1=1 AND ANAK_WNA = 'T'
        				AND A.ADM_TGL_ENTRY >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND A.ADM_TGL_ENTRY < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1";
						if($no_kel == 0){    
					$sql .=" AND C.NO_PROP = 32 AND C.NO_KAB = 73 AND C.NO_KEC =".$no_kec."
						    GROUP BY C.NO_KEL, C.NAMA_KEL
						    ORDER BY C.NO_KEL, C.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND C.NO_PROP = 32 AND C.NO_KAB = 73 AND C.NO_KEC =".$no_kec." AND C.NO_KEL =".$no_kel."
						    GROUP BY C.NO_KEL, C.NAMA_KEL
						    ORDER BY C.NO_KEL, C.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::connection('db222')->select($sql);
				return $r;


		}
		public function get_data_perubahan_nama($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL, '$tgl' AS TANGGAL, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEC A LEFT JOIN
					(SELECT 
					    C.NO_KEC
					    , C.NAMA_KEC
					    , COUNT(DISTINCT(A.RUBAH_NO)) AS JUMLAH
              			FROM CAPIL_RUBAH_NAMA A INNER JOIN SETUP_KEC C
					    ON A.ADM_NO_PROV = C.NO_PROP 
					    AND A.ADM_NO_KAB = C.NO_KAB 
					    AND A.ADM_NO_KEC = C.NO_KEC 
              			WHERE 1=1 
        				AND A.ADM_TGL_ENTRY >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND A.ADM_TGL_ENTRY < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1
					    AND
					    C.NO_PROP = 32 AND C.NO_KAB = 73
					    GROUP BY C.NO_KEC, C.NAMA_KEC
					    ORDER BY C.NO_KEC, C.NAMA_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL, '$tgl' AS TANGGAL, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEL A LEFT JOIN
					(SELECT 
					    C.NO_KEL
					    , C.NAMA_KEL
					    , COUNT(DISTINCT(A.RUBAH_NO)) AS JUMLAH
              			FROM CAPIL_RUBAH_NAMA A INNER JOIN SETUP_KEL C
					    ON A.ADM_NO_PROV = C.NO_PROP 
					    AND A.ADM_NO_KAB = C.NO_KAB 
					    AND A.ADM_NO_KEC = C.NO_KEC 
					    AND A.ADM_NO_KEL = C.NO_KEL 
              			WHERE 1=1 
        				AND A.ADM_TGL_ENTRY >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND A.ADM_TGL_ENTRY < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1";
						if($no_kel == 0){    
					$sql .=" AND C.NO_PROP = 32 AND C.NO_KAB = 73 AND C.NO_KEC =".$no_kec."
						    GROUP BY C.NO_KEL, C.NAMA_KEL
						    ORDER BY C.NO_KEL, C.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND C.NO_PROP = 32 AND C.NO_KAB = 73 AND C.NO_KEC =".$no_kec." AND C.NO_KEL =".$no_kel."
						    GROUP BY C.NO_KEL, C.NAMA_KEL
						    ORDER BY C.NO_KEL, C.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::connection('db222')->select($sql);
				return $r;


		}
		public function get_data_pengakuan_anak($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL, '$tgl' AS TANGGAL, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEC A LEFT JOIN
					(SELECT 
					    C.NO_KEC
					    , C.NAMA_KEC
					    , COUNT(DISTINCT(A.ANAK_NO)) AS JUMLAH
              			FROM CAPIL_AKUI_ANAK A INNER JOIN SETUP_KEC C
					    ON A.ADM_NO_PROV = C.NO_PROP 
					    AND A.ADM_NO_KAB = C.NO_KAB 
					    AND A.ADM_NO_KEC = C.NO_KEC 
              			WHERE 1=1 AND ANAK_WNA = 'T'
        				AND A.ADM_TGL_ENTRY >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND A.ADM_TGL_ENTRY < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1
					    AND
					    C.NO_PROP = 32 AND C.NO_KAB = 73
					    GROUP BY C.NO_KEC, C.NAMA_KEC
					    ORDER BY C.NO_KEC, C.NAMA_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL, '$tgl' AS TANGGAL, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEL A LEFT JOIN
					(SELECT 
					    C.NO_KEL
					    , C.NAMA_KEL
					    , COUNT(DISTINCT(A.ANAK_NO)) AS JUMLAH
              			FROM CAPIL_AKUI_ANAK A INNER JOIN SETUP_KEL C
					    ON A.ADM_NO_PROV = C.NO_PROP 
					    AND A.ADM_NO_KAB = C.NO_KAB 
					    AND A.ADM_NO_KEC = C.NO_KEC 
					    AND A.ADM_NO_KEL = C.NO_KEL 
              			WHERE 1=1 AND ANAK_WNA = 'T'
        				AND A.ADM_TGL_ENTRY >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND A.ADM_TGL_ENTRY < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1";
						if($no_kel == 0){    
					$sql .=" AND C.NO_PROP = 32 AND C.NO_KAB = 73 AND C.NO_KEC =".$no_kec."
						    GROUP BY C.NO_KEL, C.NAMA_KEL
						    ORDER BY C.NO_KEL, C.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND C.NO_PROP = 32 AND C.NO_KAB = 73 AND C.NO_KEC =".$no_kec." AND C.NO_KEL =".$no_kel."
						    GROUP BY C.NO_KEL, C.NAMA_KEL
						    ORDER BY C.NO_KEL, C.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::connection('db222')->select($sql);
				return $r;


		}
		public function get_data_wna_wni($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL, '$tgl' AS TANGGAL, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEC A LEFT JOIN
					(SELECT 
					    C.NO_KEC
					    , C.NAMA_KEC
					    , COUNT(DISTINCT(A.WARGA_NO)) AS JUMLAH
              			FROM CAPIL_WARGA_WNA_WNI A INNER JOIN SETUP_KEC C
					    ON A.WARGA_NO_PROV = C.NO_PROP 
					    AND A.WARGA_NO_KAB = C.NO_KAB 
					    AND A.WARGA_NO_KEC = C.NO_KEC 
              			WHERE 1=1 
        				AND A.ADM_TGL_ENTRY >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND A.ADM_TGL_ENTRY < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1
					    AND
					    C.NO_PROP = 32 AND C.NO_KAB = 73
					    GROUP BY C.NO_KEC, C.NAMA_KEC
					    ORDER BY C.NO_KEC, C.NAMA_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL, '$tgl' AS TANGGAL, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEL A LEFT JOIN
					(SELECT 
					    C.NO_KEL
					    , C.NAMA_KEL
					    , COUNT(DISTINCT(A.WARGA_NO)) AS JUMLAH
              			FROM CAPIL_WARGA_WNA_WNI A INNER JOIN SETUP_KEL C
					    ON A.WARGA_NO_PROV = C.NO_PROP 
					    AND A.WARGA_NO_KAB = C.NO_KAB 
					    AND A.WARGA_NO_KEC = C.NO_KEC 
					    AND A.WARGA_NO_KEL = C.NO_KEL 
              			WHERE 1=1 
        				AND A.ADM_TGL_ENTRY >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND A.ADM_TGL_ENTRY < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1";
						if($no_kel == 0){    
					$sql .=" AND C.NO_PROP = 32 AND C.NO_KAB = 73 AND C.NO_KEC =".$no_kec."
						    GROUP BY C.NO_KEL, C.NAMA_KEL
						    ORDER BY C.NO_KEL, C.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND C.NO_PROP = 32 AND C.NO_KAB = 73 AND C.NO_KEC =".$no_kec." AND C.NO_KEL =".$no_kel."
						    GROUP BY C.NO_KEL, C.NAMA_KEL
						    ORDER BY C.NO_KEL, C.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::connection('db222')->select($sql);
				return $r;


		}
		public function get_data_wni_wna($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL, '$tgl' AS TANGGAL, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEC A LEFT JOIN
					(SELECT 
					    C.NO_KEC
					    , C.NAMA_KEC
					    , COUNT(DISTINCT(A.WARGA_NO)) AS JUMLAH
              			FROM CAPIL_WARGA_WNI_WNA A INNER JOIN SETUP_KEC C
					    ON A.WARGA_NO_PROV = C.NO_PROP 
					    AND A.WARGA_NO_KAB = C.NO_KAB 
					    AND A.WARGA_NO_KEC = C.NO_KEC 
              			WHERE 1=1 
        				AND A.ADM_TGL_ENTRY >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND A.ADM_TGL_ENTRY < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1
					    AND
					    C.NO_PROP = 32 AND C.NO_KAB = 73
					    GROUP BY C.NO_KEC, C.NAMA_KEC
					    ORDER BY C.NO_KEC, C.NAMA_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL, '$tgl' AS TANGGAL, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEL A LEFT JOIN
					(SELECT 
					    C.NO_KEL
					    , C.NAMA_KEL
					    , COUNT(DISTINCT(A.WARGA_NO)) AS JUMLAH
              			FROM CAPIL_WARGA_WNI_WNA A INNER JOIN SETUP_KEL C
					    ON A.WARGA_NO_PROV = C.NO_PROP 
					    AND A.WARGA_NO_KAB = C.NO_KAB 
					    AND A.WARGA_NO_KEC = C.NO_KEC 
					    AND A.WARGA_NO_KEL = C.NO_KEL 
              			WHERE 1=1 
        				AND A.ADM_TGL_ENTRY >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND A.ADM_TGL_ENTRY < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1";
						if($no_kel == 0){    
					$sql .=" AND C.NO_PROP = 32 AND C.NO_KAB = 73 AND C.NO_KEC =".$no_kec."
						    GROUP BY C.NO_KEL, C.NAMA_KEL
						    ORDER BY C.NO_KEL, C.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND C.NO_PROP = 32 AND C.NO_KAB = 73 AND C.NO_KEC =".$no_kec." AND C.NO_KEL =".$no_kel."
						    GROUP BY C.NO_KEL, C.NAMA_KEL
						    ORDER BY C.NO_KEL, C.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::connection('db222')->select($sql);
				return $r;


		}
		public function get_data_warga_ganda($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL, '$tgl' AS TANGGAL, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEC A LEFT JOIN
					(SELECT 
					    C.NO_KEC
					    , C.NAMA_KEC
					    , COUNT(DISTINCT(A.WARGA_NO)) AS JUMLAH
              			FROM CAPIL_WARGA_GANDA_TBTS A INNER JOIN SETUP_KEC C
					    ON A.WARGA_NO_PROV = C.NO_PROP 
					    AND A.WARGA_NO_KAB = C.NO_KAB 
					    AND A.WARGA_NO_KEC = C.NO_KEC 
              			WHERE 1=1 
        				AND A.ADM_TGL_ENTRY >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND A.ADM_TGL_ENTRY < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1
					    AND
					    C.NO_PROP = 32 AND C.NO_KAB = 73
					    GROUP BY C.NO_KEC, C.NAMA_KEC
					    ORDER BY C.NO_KEC, C.NAMA_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL, '$tgl' AS TANGGAL, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEL A LEFT JOIN
					(SELECT 
					    C.NO_KEL
					    , C.NAMA_KEL
					    , COUNT(DISTINCT(A.WARGA_NO)) AS JUMLAH
              			FROM CAPIL_WARGA_GANDA_TBTS A INNER JOIN SETUP_KEL C
					    ON A.WARGA_NO_PROV = C.NO_PROP 
					    AND A.WARGA_NO_KAB = C.NO_KAB 
					    AND A.WARGA_NO_KEC = C.NO_KEC 
					    AND A.WARGA_NO_KEL = C.NO_KEL 
              			WHERE 1=1 
        				AND A.ADM_TGL_ENTRY >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND A.ADM_TGL_ENTRY < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1";
						if($no_kel == 0){    
					$sql .=" AND C.NO_PROP = 32 AND C.NO_KAB = 73 AND C.NO_KEC =".$no_kec."
						    GROUP BY C.NO_KEL, C.NAMA_KEL
						    ORDER BY C.NO_KEL, C.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND C.NO_PROP = 32 AND C.NO_KAB = 73 AND C.NO_KEC =".$no_kec." AND C.NO_KEL =".$no_kel."
						    GROUP BY C.NO_KEL, C.NAMA_KEL
						    ORDER BY C.NO_KEL, C.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::connection('db222')->select($sql);
				return $r;


		}
		public function get_data_pristiwa_penting($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL, '$tgl' AS TANGGAL, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEC A LEFT JOIN
					(SELECT 
					    C.NO_KEC
					    , C.NAMA_KEC
					    , COUNT(DISTINCT(A.PERPEN_NO)) AS JUMLAH
              			FROM CAPIL_PERISTIWA_PENTING A INNER JOIN SETUP_KEC C
					    ON A.ADM_NO_PROV = C.NO_PROP 
					    AND A.ADM_NO_KAB = C.NO_KAB 
					    AND A.ADM_NO_KEC = C.NO_KEC 
              			WHERE 1=1 
        				AND A.ADM_TGL_ENTRY >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND A.ADM_TGL_ENTRY < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1
					    AND
					    C.NO_PROP = 32 AND C.NO_KAB = 73
					    GROUP BY C.NO_KEC, C.NAMA_KEC
					    ORDER BY C.NO_KEC, C.NAMA_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL, '$tgl' AS TANGGAL, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEL A LEFT JOIN
					(SELECT 
					    C.NO_KEL
					    , C.NAMA_KEL
					    , COUNT(DISTINCT(A.PERPEN_NO)) AS JUMLAH
              			FROM CAPIL_PERISTIWA_PENTING A INNER JOIN SETUP_KEL C
					    ON A.ADM_NO_PROV = C.NO_PROP 
					    AND A.ADM_NO_KAB = C.NO_KAB 
					    AND A.ADM_NO_KEC = C.NO_KEC 
					    AND A.ADM_NO_KEL = C.NO_KEL 
              			WHERE 1=1 
        				AND A.ADM_TGL_ENTRY >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND A.ADM_TGL_ENTRY < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1";
						if($no_kel == 0){    
					$sql .=" AND C.NO_PROP = 32 AND C.NO_KAB = 73 AND C.NO_KEC =".$no_kec."
						    GROUP BY C.NO_KEL, C.NAMA_KEL
						    ORDER BY C.NO_KEL, C.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND C.NO_PROP = 32 AND C.NO_KAB = 73 AND C.NO_KEC =".$no_kec." AND C.NO_KEL =".$no_kel."
						    GROUP BY C.NO_KEL, C.NAMA_KEL
						    ORDER BY C.NO_KEL, C.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::connection('db222')->select($sql);
				return $r;


		}
		public function get_data_pembetulan_akta($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL, '$tgl' AS TANGGAL, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEC A LEFT JOIN
					(SELECT 
					    C.NO_KEC
					    , C.NAMA_KEC
					    , COUNT(DISTINCT(A.BETUL_NO)) AS JUMLAH
              			FROM CAPIL_BETUL_AKTA A INNER JOIN SETUP_KEC C
					    ON A.ADM_NO_PROV = C.NO_PROP 
					    AND A.ADM_NO_KAB = C.NO_KAB 
					    AND A.ADM_NO_KEC = C.NO_KEC 
              			WHERE 1=1 
        				AND A.ADM_TGL_ENTRY >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND A.ADM_TGL_ENTRY < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1
					    AND
					    C.NO_PROP = 32 AND C.NO_KAB = 73
					    GROUP BY C.NO_KEC, C.NAMA_KEC
					    ORDER BY C.NO_KEC, C.NAMA_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL, '$tgl' AS TANGGAL, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEL A LEFT JOIN
					(SELECT 
					    C.NO_KEL
					    , C.NAMA_KEL
					    , COUNT(DISTINCT(A.BETUL_NO)) AS JUMLAH
              			FROM CAPIL_BETUL_AKTA A INNER JOIN SETUP_KEL C
					    ON A.ADM_NO_PROV = C.NO_PROP 
					    AND A.ADM_NO_KAB = C.NO_KAB 
					    AND A.ADM_NO_KEC = C.NO_KEC 
					    AND A.ADM_NO_KEL = C.NO_KEL 
              			WHERE 1=1 
        				AND A.ADM_TGL_ENTRY >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND A.ADM_TGL_ENTRY < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1";
						if($no_kel == 0){    
					$sql .=" AND C.NO_PROP = 32 AND C.NO_KAB = 73 AND C.NO_KEC =".$no_kec."
						    GROUP BY C.NO_KEL, C.NAMA_KEL
						    ORDER BY C.NO_KEL, C.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND C.NO_PROP = 32 AND C.NO_KAB = 73 AND C.NO_KEC =".$no_kec." AND C.NO_KEL =".$no_kel."
						    GROUP BY C.NO_KEL, C.NAMA_KEL
						    ORDER BY C.NO_KEL, C.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::connection('db222')->select($sql);
				return $r;


		}
		public function get_data_pembatalan_akta($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL, '$tgl' AS TANGGAL, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEC A LEFT JOIN
					(SELECT 
					    C.NO_KEC
					    , C.NAMA_KEC
					    , COUNT(DISTINCT(A.BATAL_NO)) AS JUMLAH
              			FROM CAPIL_BATAL_AKTA A INNER JOIN SETUP_KEC C
					    ON A.ADM_NO_PROV = C.NO_PROP 
					    AND A.ADM_NO_KAB = C.NO_KAB 
					    AND A.ADM_NO_KEC = C.NO_KEC 
              			WHERE 1=1 
        				AND A.ADM_TGL_ENTRY >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND A.ADM_TGL_ENTRY < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1
					    AND
					    C.NO_PROP = 32 AND C.NO_KAB = 73
					    GROUP BY C.NO_KEC, C.NAMA_KEC
					    ORDER BY C.NO_KEC, C.NAMA_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL, '$tgl' AS TANGGAL, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEL A LEFT JOIN
					(SELECT 
					    C.NO_KEL
					    , C.NAMA_KEL
					    , COUNT(DISTINCT(A.BATAL_NO)) AS JUMLAH
              			FROM CAPIL_BATAL_AKTA A INNER JOIN SETUP_KEL C
					    ON A.ADM_NO_PROV = C.NO_PROP 
					    AND A.ADM_NO_KAB = C.NO_KAB 
					    AND A.ADM_NO_KEC = C.NO_KEC 
					    AND A.ADM_NO_KEL = C.NO_KEL 
              			WHERE 1=1 
        				AND A.ADM_TGL_ENTRY >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND A.ADM_TGL_ENTRY < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1";
						if($no_kel == 0){    
					$sql .=" AND C.NO_PROP = 32 AND C.NO_KAB = 73 AND C.NO_KEC =".$no_kec."
						    GROUP BY C.NO_KEL, C.NAMA_KEL
						    ORDER BY C.NO_KEL, C.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND C.NO_PROP = 32 AND C.NO_KAB = 73 AND C.NO_KEC =".$no_kec." AND C.NO_KEL =".$no_kel."
						    GROUP BY C.NO_KEL, C.NAMA_KEL
						    ORDER BY C.NO_KEL, C.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::connection('db222')->select($sql);
				return $r;


		}
		public function get_data_lahir_ln($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL, '$tgl' AS TANGGAL, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEC A LEFT JOIN
					(SELECT 
					    C.NO_KEC
					    , C.NAMA_KEC
					    , COUNT(DISTINCT(A.BAYI_NO)) AS JUMLAH
              			FROM CAPIL_LAHIR_LN A INNER JOIN SETUP_KEC C
					    ON A.ADM_NO_PROV = C.NO_PROP 
					    AND A.ADM_NO_KAB = C.NO_KAB 
					    AND A.ADM_NO_KEC = C.NO_KEC 
              			WHERE 1=1 
        				AND A.ADM_TGL_ENTRY >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND A.ADM_TGL_ENTRY < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1
					    AND
					    C.NO_PROP = 32 AND C.NO_KAB = 73
					    GROUP BY C.NO_KEC, C.NAMA_KEC
					    ORDER BY C.NO_KEC, C.NAMA_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL, '$tgl' AS TANGGAL, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEL A LEFT JOIN
					(SELECT 
					    C.NO_KEL
					    , C.NAMA_KEL
					    , COUNT(DISTINCT(A.BAYI_NO)) AS JUMLAH
              			FROM CAPIL_LAHIR_LN A INNER JOIN SETUP_KEL C
					    ON A.ADM_NO_PROV = C.NO_PROP 
					    AND A.ADM_NO_KAB = C.NO_KAB 
					    AND A.ADM_NO_KEC = C.NO_KEC 
					    AND A.ADM_NO_KEL = C.NO_KEL 
              			WHERE 1=1 
        				AND A.ADM_TGL_ENTRY >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND A.ADM_TGL_ENTRY < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1";
						if($no_kel == 0){    
					$sql .=" AND C.NO_PROP = 32 AND C.NO_KAB = 73 AND C.NO_KEC =".$no_kec."
						    GROUP BY C.NO_KEL, C.NAMA_KEL
						    ORDER BY C.NO_KEL, C.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND C.NO_PROP = 32 AND C.NO_KAB = 73 AND C.NO_KEC =".$no_kec." AND C.NO_KEL =".$no_kel."
						    GROUP BY C.NO_KEL, C.NAMA_KEL
						    ORDER BY C.NO_KEL, C.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::connection('db222')->select($sql);
				return $r;


		}
		public function get_data_lahir_bakak($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL, '$tgl' AS TANGGAL, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEC A LEFT JOIN
					(SELECT 
					    C.NO_KEC
					    , C.NAMA_KEC
					    , COUNT(DISTINCT(A.BAYI_NO)) AS JUMLAH
              			FROM CAPIL_BAKAK A INNER JOIN SETUP_KEC C
					    ON A.ADM_NO_PROV = C.NO_PROP 
					    AND A.ADM_NO_KAB = C.NO_KAB 
					    AND A.ADM_NO_KEC = C.NO_KEC 
              			WHERE 1=1 
        				AND A.ADM_TGL_ENTRY >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND A.ADM_TGL_ENTRY < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1
					    AND
					    C.NO_PROP = 32 AND C.NO_KAB = 73
					    GROUP BY C.NO_KEC, C.NAMA_KEC
					    ORDER BY C.NO_KEC, C.NAMA_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL, '$tgl' AS TANGGAL, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEL A LEFT JOIN
					(SELECT 
					    C.NO_KEL
					    , C.NAMA_KEL
					    , COUNT(DISTINCT(A.BAYI_NO)) AS JUMLAH
              			FROM CAPIL_BAKAK A INNER JOIN SETUP_KEL C
					    ON A.ADM_NO_PROV = C.NO_PROP 
					    AND A.ADM_NO_KAB = C.NO_KAB 
					    AND A.ADM_NO_KEC = C.NO_KEC 
					    AND A.ADM_NO_KEL = C.NO_KEL 
              			WHERE 1=1 
        				AND A.ADM_TGL_ENTRY >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND A.ADM_TGL_ENTRY < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1";
						if($no_kel == 0){    
					$sql .=" AND C.NO_PROP = 32 AND C.NO_KAB = 73 AND C.NO_KEC =".$no_kec."
						    GROUP BY C.NO_KEL, C.NAMA_KEL
						    ORDER BY C.NO_KEL, C.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND C.NO_PROP = 32 AND C.NO_KAB = 73 AND C.NO_KEC =".$no_kec." AND C.NO_KEL =".$no_kel."
						    GROUP BY C.NO_KEL, C.NAMA_KEL
						    ORDER BY C.NO_KEL, C.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::connection('db222')->select($sql);
				return $r;


		}
		
		public function mst_get_laporan_kelamin($no_kec,$no_kel,$dkb_biodata = '',$dkb_cutoff = '',$dkb_gisa = ''){
			if (!empty($dkb_biodata)){
				$dkb_bio = $dkb_biodata;
			}else{
				$dkb_bio = Shr::do_get_dkb_bio();
			}
			if (!empty($dkb_cutoff)){
				$cut_off_dkb = $dkb_cutoff;
			}else{
				$cut_off_dkb = Shr::do_get_cut_off_date();
			}
			if (!empty($dkb_gisa)){
				$dkb_gisa = $dkb_gisa;
			}else{
				$dkb_gisa = Shr::do_get_dkb_gisa();
			}
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL
					, CASE WHEN B.LK IS NULL THEN 0 ELSE B.LK END AS LK
					, CASE WHEN B.PR IS NULL THEN 0 ELSE B.PR END AS PR
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEC A LEFT JOIN
					(SELECT 
						B.NO_KEC 
						,B.NAMA_KEC 
						, SUM(CASE WHEN A.JENIS_KLMIN = 1 THEN 1 ELSE 0 END) LK
						, SUM(CASE WHEN A.JENIS_KLMIN = 2 THEN 1 ELSE 0 END) PR
						, COUNT(1) JUMLAH
						FROM SETUP_KEC B LEFT JOIN ".$dkb_bio." A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC WHERE  (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) AND A.NO_PROP = 32 AND A.NO_KAB =73 GROUP BY B.NO_KEC,B.NAMA_KEC ORDER BY B.NO_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL
					, CASE WHEN B.LK IS NULL THEN 0 ELSE B.LK END AS LK
					, CASE WHEN B.PR IS NULL THEN 0 ELSE B.PR END AS PR
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEL A LEFT JOIN
					(SELECT 
						B.NO_KEL 
						,B.NAMA_KEL
						, SUM(CASE WHEN A.JENIS_KLMIN = 1 THEN 1 ELSE 0 END) LK
						, SUM(CASE WHEN A.JENIS_KLMIN = 2 THEN 1 ELSE 0 END) PR
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN ".$dkb_bio." A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL WHERE  (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) ";
						if($no_kel == 0){    
					$sql .=" AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec." and  A.NO_KEL =".$no_kel."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::select($sql);
               return $r;
		}


		

		public function mst_get_laporan_wajib_ktp($no_kec,$no_kel,$dkb_biodata = '',$dkb_cutoff = '',$dkb_gisa = ''){
			if (!empty($dkb_biodata)){
				$dkb_bio = $dkb_biodata;
			}else{
				$dkb_bio = Shr::do_get_dkb_bio();
			}
			if (!empty($dkb_cutoff)){
				$cut_off_dkb = $dkb_cutoff;
			}else{
				$cut_off_dkb = Shr::do_get_cut_off_date();
			}
			if (!empty($dkb_gisa)){
				$dkb_gisa = $dkb_gisa;
			}else{
				$dkb_gisa = Shr::do_get_dkb_gisa();
			}
			
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL
					, CASE WHEN B.LK IS NULL THEN 0 ELSE B.LK END AS LK
					, CASE WHEN B.PR IS NULL THEN 0 ELSE B.PR END AS PR
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEC A LEFT JOIN
					(SELECT 
						B.NO_KEC 
						,B.NAMA_KEC 
						, SUM(CASE WHEN A.JENIS_KLMIN = 1 THEN 1 ELSE 0 END) LK
						, SUM(CASE WHEN A.JENIS_KLMIN = 2 THEN 1 ELSE 0 END) PR
						, COUNT(1) JUMLAH
						FROM SETUP_KEC B LEFT JOIN ".$dkb_bio." A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC WHERE (A.TGL_LHR<=ADD_MONTHS(TO_DATE('".$cut_off_dkb."','DD/MM/YYYY'),-12*17) OR A.STAT_KWN<>1) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) AND A.NO_PROP = 32 AND A.NO_KAB =73 GROUP BY B.NO_KEC,B.NAMA_KEC ORDER BY B.NO_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL
					, CASE WHEN B.LK IS NULL THEN 0 ELSE B.LK END AS LK
					, CASE WHEN B.PR IS NULL THEN 0 ELSE B.PR END AS PR
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEL A LEFT JOIN
					(SELECT 
						B.NO_KEL 
						,B.NAMA_KEL
						, SUM(CASE WHEN A.JENIS_KLMIN = 1 THEN 1 ELSE 0 END) LK
						, SUM(CASE WHEN A.JENIS_KLMIN = 2 THEN 1 ELSE 0 END) PR
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN ".$dkb_bio." A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL WHERE (A.TGL_LHR<=ADD_MONTHS(TO_DATE('".$cut_off_dkb."','DD/MM/YYYY'),-12*17) OR A.STAT_KWN<>1) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) ";
						if($no_kel == 0){    
					$sql .=" AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec." and  A.NO_KEL =".$no_kel."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::select($sql);
               return $r;
		}


		public function mst_get_laporan_wajib_ktp_pelayanan($no_kec,$no_kel,$dkb_biodata = '',$dkb_cutoff = '',$dkb_gisa = ''){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL
					, CASE WHEN B.LK IS NULL THEN 0 ELSE B.LK END AS LK
					, CASE WHEN B.PR IS NULL THEN 0 ELSE B.PR END AS PR
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEC A LEFT JOIN
					(SELECT 
						B.NO_KEC 
						,B.NAMA_KEC 
						, SUM(CASE WHEN A.JENIS_KLMIN = 1 THEN 1 ELSE 0 END) LK
						, SUM(CASE WHEN A.JENIS_KLMIN = 2 THEN 1 ELSE 0 END) PR
						, COUNT(1) JUMLAH
						FROM SETUP_KEC B LEFT JOIN BIODATA_WNI A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC WHERE (A.TGL_LHR<=ADD_MONTHS(SYSDATE,-12*17) OR A.STAT_KWN<>1) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) AND A.NO_PROP = 32 AND A.NO_KAB =73 GROUP BY B.NO_KEC,B.NAMA_KEC ORDER BY B.NO_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL
					, CASE WHEN B.LK IS NULL THEN 0 ELSE B.LK END AS LK
					, CASE WHEN B.PR IS NULL THEN 0 ELSE B.PR END AS PR
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEL A LEFT JOIN
					(SELECT 
						B.NO_KEL 
						,B.NAMA_KEL
						, SUM(CASE WHEN A.JENIS_KLMIN = 1 THEN 1 ELSE 0 END) LK
						, SUM(CASE WHEN A.JENIS_KLMIN = 2 THEN 1 ELSE 0 END) PR
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN BIODATA_WNI A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL WHERE (A.TGL_LHR<=ADD_MONTHS(SYSDATE,-12*17) OR A.STAT_KWN<>1) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) ";
						if($no_kel == 0){    
					$sql .=" AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec." and  A.NO_KEL =".$no_kel."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::connection('db222')->select($sql);
               return $r;
		}


		

		public function mst_get_laporan_identitas_tunggal($no_kec,$no_kel,$dkb_biodata = '',$dkb_cutoff = '',$dkb_gisa = ''){
			if (!empty($dkb_biodata)){
				$dkb_bio = $dkb_biodata;
			}else{
				$dkb_bio = Shr::do_get_dkb_bio();
			}
			if (!empty($dkb_cutoff)){
				$cut_off_dkb = $dkb_cutoff;
			}else{
				$cut_off_dkb = Shr::do_get_cut_off_date();
			}
			if (!empty($dkb_gisa)){
				$dkb_gisa = $dkb_gisa;
			}else{
				$dkb_gisa = Shr::do_get_dkb_gisa();
			}
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL
					, CASE WHEN B.LK IS NULL THEN 0 ELSE B.LK END AS LK
					, CASE WHEN B.PR IS NULL THEN 0 ELSE B.PR END AS PR
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEC A LEFT JOIN
					(SELECT 
						B.NO_KEC 
						,B.NAMA_KEC 
						, SUM(CASE WHEN A.JENIS_KLMIN = 1 THEN 1 ELSE 0 END) LK
						, SUM(CASE WHEN A.JENIS_KLMIN = 2 THEN 1 ELSE 0 END) PR
						, COUNT(1) JUMLAH
						FROM SETUP_KEC B LEFT JOIN ".$dkb_bio." A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC INNER JOIN DEMOGRAPHICS_ALL@DB2 X ON A.NIK = X.NIK WHERE X.CURRENT_STATUS_CODE IN ('PRINT_READY_RECORD','SENT_FOR_CARD_PRINTING','CARD_PRINTED','CARD_ISSUED','CARD_SHIPPED','CARD_REISSUED_LOCALLY') AND (A.TGL_LHR<=ADD_MONTHS(TO_DATE('".$cut_off_dkb."','DD/MM/YYYY'),-12*17) OR A.STAT_KWN<>1) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) AND A.NO_PROP = 32 AND A.NO_KAB =73 GROUP BY B.NO_KEC,B.NAMA_KEC ORDER BY B.NO_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL
					, CASE WHEN B.LK IS NULL THEN 0 ELSE B.LK END AS LK
					, CASE WHEN B.PR IS NULL THEN 0 ELSE B.PR END AS PR
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEL A LEFT JOIN
					(SELECT 
						B.NO_KEL 
						,B.NAMA_KEL
						, SUM(CASE WHEN A.JENIS_KLMIN = 1 THEN 1 ELSE 0 END) LK
						, SUM(CASE WHEN A.JENIS_KLMIN = 2 THEN 1 ELSE 0 END) PR
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN ".$dkb_bio." A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL INNER JOIN DEMOGRAPHICS_ALL@DB2 X ON A.NIK = X.NIK WHERE X.CURRENT_STATUS_CODE IN ('PRINT_READY_RECORD','SENT_FOR_CARD_PRINTING','CARD_PRINTED','CARD_ISSUED','CARD_SHIPPED','CARD_REISSUED_LOCALLY') AND (A.TGL_LHR<=ADD_MONTHS(TO_DATE('".$cut_off_dkb."','DD/MM/YYYY'),-12*17) OR A.STAT_KWN<>1) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) ";
						if($no_kel == 0){    
					$sql .=" AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec." and  A.NO_KEL =".$no_kel."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::select($sql);
               return $r;
		}


		public function mst_get_laporan_identitas_tunggal_pelayanan($no_kec,$no_kel,$dkb_biodata = '',$dkb_cutoff = '',$dkb_gisa = ''){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL
					, CASE WHEN B.LK IS NULL THEN 0 ELSE B.LK END AS LK
					, CASE WHEN B.PR IS NULL THEN 0 ELSE B.PR END AS PR
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEC A LEFT JOIN
					(SELECT 
						B.NO_KEC 
						,B.NAMA_KEC 
						, SUM(CASE WHEN A.JENIS_KLMIN = 1 THEN 1 ELSE 0 END) LK
						, SUM(CASE WHEN A.JENIS_KLMIN = 2 THEN 1 ELSE 0 END) PR
						, COUNT(1) JUMLAH
						FROM SETUP_KEC B LEFT JOIN BIODATA_WNI A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC INNER JOIN DEMOGRAPHICS_ALL@DB2 X ON A.NIK = X.NIK WHERE X.CURRENT_STATUS_CODE IN ('PRINT_READY_RECORD','SENT_FOR_CARD_PRINTING','CARD_PRINTED','CARD_ISSUED','CARD_SHIPPED','CARD_REISSUED_LOCALLY') AND (A.TGL_LHR<=ADD_MONTHS(SYSDATE,-12*17) OR A.STAT_KWN<>1) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) AND A.NO_PROP = 32 AND A.NO_KAB =73 GROUP BY B.NO_KEC,B.NAMA_KEC ORDER BY B.NO_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL
					, CASE WHEN B.LK IS NULL THEN 0 ELSE B.LK END AS LK
					, CASE WHEN B.PR IS NULL THEN 0 ELSE B.PR END AS PR
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEL A LEFT JOIN
					(SELECT 
						B.NO_KEL 
						,B.NAMA_KEL
						, SUM(CASE WHEN A.JENIS_KLMIN = 1 THEN 1 ELSE 0 END) LK
						, SUM(CASE WHEN A.JENIS_KLMIN = 2 THEN 1 ELSE 0 END) PR
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN BIODATA_WNI A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL INNER JOIN DEMOGRAPHICS_ALL@DB2 X ON A.NIK = X.NIK WHERE X.CURRENT_STATUS_CODE IN ('PRINT_READY_RECORD','SENT_FOR_CARD_PRINTING','CARD_PRINTED','CARD_ISSUED','CARD_SHIPPED','CARD_REISSUED_LOCALLY') AND (A.TGL_LHR<=ADD_MONTHS(SYSDATE,-12*17) OR A.STAT_KWN<>1) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) ";
						if($no_kel == 0){    
					$sql .=" AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec." and  A.NO_KEL =".$no_kel."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::connection('db222')->select($sql);
               return $r;
		}
		public function mst_get_laporan_kitas($no_kec,$no_kel,$dkb_biodata = '',$dkb_cutoff = '',$dkb_gisa = ''){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL
					, CASE WHEN B.LK IS NULL THEN 0 ELSE B.LK END AS LK
					, CASE WHEN B.PR IS NULL THEN 0 ELSE B.PR END AS PR
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEC A LEFT JOIN
					(SELECT 
						B.NO_KEC 
						,B.NAMA_KEC 
						, SUM(CASE WHEN A.JENIS_KLMIN = 1 THEN 1 ELSE 0 END) LK
						, SUM(CASE WHEN A.JENIS_KLMIN = 2 THEN 1 ELSE 0 END) PR
						, COUNT(1) JUMLAH
						FROM SETUP_KEC B LEFT JOIN BIODATA_WNA A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC WHERE 1=1 AND A.DOK_IMGR = 1 AND A.NO_PROP = 32 AND A.NO_KAB =73 GROUP BY B.NO_KEC,B.NAMA_KEC ORDER BY B.NO_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL
					, CASE WHEN B.LK IS NULL THEN 0 ELSE B.LK END AS LK
					, CASE WHEN B.PR IS NULL THEN 0 ELSE B.PR END AS PR
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEL A LEFT JOIN
					(SELECT 
						B.NO_KEL 
						,B.NAMA_KEL
						, SUM(CASE WHEN A.JENIS_KLMIN = 1 THEN 1 ELSE 0 END) LK
						, SUM(CASE WHEN A.JENIS_KLMIN = 2 THEN 1 ELSE 0 END) PR
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN BIODATA_WNA A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL WHERE 1=1 AND A.DOK_IMGR = 1 ";
						if($no_kel == 0){    
					$sql .=" AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec." and  A.NO_KEL =".$no_kel."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::connection('db222')->select($sql);
               return $r;
		}
		public function mst_get_laporan_kitap($no_kec,$no_kel,$dkb_biodata = '',$dkb_cutoff = '',$dkb_gisa = ''){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL
					, CASE WHEN B.LK IS NULL THEN 0 ELSE B.LK END AS LK
					, CASE WHEN B.PR IS NULL THEN 0 ELSE B.PR END AS PR
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEC A LEFT JOIN
					(SELECT 
						B.NO_KEC 
						,B.NAMA_KEC 
						, SUM(CASE WHEN A.JENIS_KLMIN = 1 THEN 1 ELSE 0 END) LK
						, SUM(CASE WHEN A.JENIS_KLMIN = 2 THEN 1 ELSE 0 END) PR
						, COUNT(1) JUMLAH
						FROM SETUP_KEC B LEFT JOIN BIODATA_WNA A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC WHERE 1=1 AND A.DOK_IMGR = 2 AND A.NO_PROP = 32 AND A.NO_KAB =73 GROUP BY B.NO_KEC,B.NAMA_KEC ORDER BY B.NO_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL
					, CASE WHEN B.LK IS NULL THEN 0 ELSE B.LK END AS LK
					, CASE WHEN B.PR IS NULL THEN 0 ELSE B.PR END AS PR
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEL A LEFT JOIN
					(SELECT 
						B.NO_KEL 
						,B.NAMA_KEL
						, SUM(CASE WHEN A.JENIS_KLMIN = 1 THEN 1 ELSE 0 END) LK
						, SUM(CASE WHEN A.JENIS_KLMIN = 2 THEN 1 ELSE 0 END) PR
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN BIODATA_WNA A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL WHERE 1=1 AND A.DOK_IMGR = 2 ";
						if($no_kel == 0){    
					$sql .=" AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec." and  A.NO_KEL =".$no_kel."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::connection('db222')->select($sql);
               return $r;
		}
		public function mst_get_laporan_ktp_oa($no_kec,$no_kel,$dkb_biodata = '',$dkb_cutoff = '',$dkb_gisa = ''){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL
					, CASE WHEN B.LK IS NULL THEN 0 ELSE B.LK END AS LK
					, CASE WHEN B.PR IS NULL THEN 0 ELSE B.PR END AS PR
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEC A LEFT JOIN
					(SELECT 
						B.NO_KEC 
						,B.NAMA_KEC 
						, SUM(CASE WHEN A.JENIS_KLMIN = 1 THEN 1 ELSE 0 END) LK
						, SUM(CASE WHEN A.JENIS_KLMIN = 2 THEN 1 ELSE 0 END) PR
						, COUNT(1) JUMLAH
						FROM SETUP_KEC B LEFT JOIN BIODATA_WNA A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC WHERE 1=1 AND A.COUNT_KTP > 0 AND A.NO_PROP = 32 AND A.NO_KAB =73 GROUP BY B.NO_KEC,B.NAMA_KEC ORDER BY B.NO_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL
					, CASE WHEN B.LK IS NULL THEN 0 ELSE B.LK END AS LK
					, CASE WHEN B.PR IS NULL THEN 0 ELSE B.PR END AS PR
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEL A LEFT JOIN
					(SELECT 
						B.NO_KEL 
						,B.NAMA_KEL
						, SUM(CASE WHEN A.JENIS_KLMIN = 1 THEN 1 ELSE 0 END) LK
						, SUM(CASE WHEN A.JENIS_KLMIN = 2 THEN 1 ELSE 0 END) PR
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN BIODATA_WNA A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL WHERE 1=1 AND A.COUNT_KTP > 0 ";
						if($no_kel == 0){    
					$sql .=" AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec." and  A.NO_KEL =".$no_kel."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::connection('db222')->select($sql);
               return $r;
		}
		

		
		public function mst_get_laporan_non_ktp($no_kec,$no_kel,$dkb_biodata = '',$dkb_cutoff = '',$dkb_gisa = ''){
			if (!empty($dkb_biodata)){
				$dkb_bio = $dkb_biodata;
			}else{
				$dkb_bio = Shr::do_get_dkb_bio();
			}
			if (!empty($dkb_cutoff)){
				$cut_off_dkb = $dkb_cutoff;
			}else{
				$cut_off_dkb = Shr::do_get_cut_off_date();
			}
			if (!empty($dkb_gisa)){
				$dkb_gisa = $dkb_gisa;
			}else{
				$dkb_gisa = Shr::do_get_dkb_gisa();
			}
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL
					, CASE WHEN B.LK IS NULL THEN 0 ELSE B.LK END AS LK
					, CASE WHEN B.PR IS NULL THEN 0 ELSE B.PR END AS PR
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEC A LEFT JOIN
					(SELECT 
						B.NO_KEC 
						,B.NAMA_KEC 
						, SUM(CASE WHEN A.JENIS_KLMIN = 1 THEN 1 ELSE 0 END) LK
						, SUM(CASE WHEN A.JENIS_KLMIN = 2 THEN 1 ELSE 0 END) PR
						, COUNT(1) JUMLAH
						FROM SETUP_KEC B LEFT JOIN ".$dkb_bio." A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC WHERE (A.TGL_LHR>ADD_MONTHS(TO_DATE('".$cut_off_dkb."','DD/MM/YYYY'),-12*17) AND A.STAT_KWN=1) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) AND A.NO_PROP = 32 AND A.NO_KAB =73 GROUP BY B.NO_KEC,B.NAMA_KEC ORDER BY B.NO_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL
					, CASE WHEN B.LK IS NULL THEN 0 ELSE B.LK END AS LK
					, CASE WHEN B.PR IS NULL THEN 0 ELSE B.PR END AS PR
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEL A LEFT JOIN
					(SELECT 
						B.NO_KEL 
						,B.NAMA_KEL
						, SUM(CASE WHEN A.JENIS_KLMIN = 1 THEN 1 ELSE 0 END) LK
						, SUM(CASE WHEN A.JENIS_KLMIN = 2 THEN 1 ELSE 0 END) PR
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN ".$dkb_bio." A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL WHERE (A.TGL_LHR>ADD_MONTHS(TO_DATE('".$cut_off_dkb."','DD/MM/YYYY'),-12*17) AND A.STAT_KWN=1) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) ";
						if($no_kel == 0){    
					$sql .=" AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec." and  A.NO_KEL =".$no_kel."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::select($sql);
               return $r;
		}

		public function mst_get_laporan_kk_kelamin($no_kec,$no_kel,$dkb_biodata = '',$dkb_cutoff = '',$dkb_gisa = ''){
			if (!empty($dkb_biodata)){
				$dkb_bio = $dkb_biodata;
			}else{
				$dkb_bio = Shr::do_get_dkb_bio();
			}
			if (!empty($dkb_cutoff)){
				$cut_off_dkb = $dkb_cutoff;
			}else{
				$cut_off_dkb = Shr::do_get_cut_off_date();
			}
			if (!empty($dkb_gisa)){
				$dkb_gisa = $dkb_gisa;
			}else{
				$dkb_gisa = Shr::do_get_dkb_gisa();
			}
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT 
					  A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL, B.R1 LK , B.R2 PR, B.JUMLAH
					  FROM SETUP_KEC A LEFT JOIN (SELECT NO_KEC
                          , SUM(CASE WHEN JENIS_KELAMIN = 1 THEN 1 ELSE 0 END) R1
                          , SUM(CASE WHEN JENIS_KELAMIN <> 1 THEN 1 ELSE 0 END) R2
						  , COUNT(1) JUMLAH
					FROM ".$dkb_gisa." WHERE HUBUNGAN_DLM_KELUARGA = 1 AND 1=1 AND NO_PROP = 32 AND NO_KAB =73 
					GROUP BY NO_KEC
					ORDER BY NO_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT 
					  A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL, B.R1 LK, B.R2 PR, B.JUMLAH
					  FROM SETUP_KEL A LEFT JOIN (SELECT NO_KEL
                          , SUM(CASE WHEN JENIS_KELAMIN = 1 THEN 1 ELSE 0 END) R1
                          , SUM(CASE WHEN JENIS_KELAMIN <> 1 THEN 1 ELSE 0 END) R2
						  , COUNT(1) JUMLAH
					FROM ".$dkb_gisa." WHERE HUBUNGAN_DLM_KELUARGA = 1 AND 1=1 ";
						if($no_kel == 0){    
					$sql .=" AND NO_PROP = 32 AND NO_KAB = 73 AND NO_KEC =".$no_kec."
						    GROUP BY NO_KEL
						    ORDER BY NO_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND NO_PROP = 32 AND NO_KAB = 73 AND NO_KEC =".$no_kec." AND NO_KEL =".$no_kel."
						    GROUP BY NO_KEL
						    ORDER BY NO_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::select($sql);
               return $r;
		}
		
		public function mst_get_laporan_kk_usia($no_kec,$no_kel,$dkb_biodata = '',$dkb_cutoff = '',$dkb_gisa = ''){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT 
					  A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL, B.R1 , B.R2 , B.R3, B.R4 , B.R5 , B.R6 , B.R7 , B.R8 , B.R9 , B.R10 , B.R11 , B.R12 , B.R13, B.R14, B.R15, B.R16, B.JUMLAH
					  FROM SETUP_KEC A LEFT JOIN (SELECT NO_KEC
                          , SUM(CASE WHEN UMUR >= 0 AND UMUR <= 4 THEN 1 ELSE 0 END) R1
                          , SUM(CASE WHEN UMUR >= 5 AND UMUR <= 9 THEN 1 ELSE 0 END) R2
                          , SUM(CASE WHEN UMUR >= 10 AND UMUR <= 14 THEN 1 ELSE 0 END) R3
                          , SUM(CASE WHEN UMUR >= 15 AND UMUR <= 19 THEN 1 ELSE 0 END) R4
                          , SUM(CASE WHEN UMUR >= 20 AND UMUR <= 24 THEN 1 ELSE 0 END) R5
                          , SUM(CASE WHEN UMUR >= 25 AND UMUR <= 29 THEN 1 ELSE 0 END) R6
                          , SUM(CASE WHEN UMUR >= 30 AND UMUR <= 34 THEN 1 ELSE 0 END) R7
                          , SUM(CASE WHEN UMUR >= 35 AND UMUR <= 39 THEN 1 ELSE 0 END) R8
                          , SUM(CASE WHEN UMUR >= 40 AND UMUR <= 44 THEN 1 ELSE 0 END) R9
                          , SUM(CASE WHEN UMUR >= 45 AND UMUR <= 49 THEN 1 ELSE 0 END) R10
                          , SUM(CASE WHEN UMUR >= 50 AND UMUR <= 54 THEN 1 ELSE 0 END) R11
                          , SUM(CASE WHEN UMUR >= 55 AND UMUR <= 59 THEN 1 ELSE 0 END) R12
                          , SUM(CASE WHEN UMUR >= 60 AND UMUR <= 64 THEN 1 ELSE 0 END) R13
                          , SUM(CASE WHEN UMUR >= 65 AND UMUR <= 69 THEN 1 ELSE 0 END) R14
                          , SUM(CASE WHEN UMUR >= 70 AND UMUR <= 74 THEN 1 ELSE 0 END) R15
                          , SUM(CASE WHEN UMUR >= 75  THEN 1 ELSE 0 END) R16
						  , COUNT(1) JUMLAH
					FROM KAMPUNG_GISA_DKB WHERE HUBUNGAN_DLM_KELUARGA = 1 AND 1=1 AND NO_PROP = 32 AND NO_KAB =73 
					GROUP BY NO_KEC
					ORDER BY NO_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT 
					  A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL, B.R1 , B.R2 , B.R3, B.R4 , B.R5 , B.R6 , B.R7 , B.R8 , B.R9 , B.R10 , B.R11 , B.R12 , B.R13, B.R14, B.R15, B.R16, B.JUMLAH
					  FROM SETUP_KEL A LEFT JOIN (SELECT NO_KEL
                          , SUM(CASE WHEN UMUR >= 0 AND UMUR <= 4 THEN 1 ELSE 0 END) R1
                          , SUM(CASE WHEN UMUR >= 5 AND UMUR <= 9 THEN 1 ELSE 0 END) R2
                          , SUM(CASE WHEN UMUR >= 10 AND UMUR <= 14 THEN 1 ELSE 0 END) R3
                          , SUM(CASE WHEN UMUR >= 15 AND UMUR <= 19 THEN 1 ELSE 0 END) R4
                          , SUM(CASE WHEN UMUR >= 20 AND UMUR <= 24 THEN 1 ELSE 0 END) R5
                          , SUM(CASE WHEN UMUR >= 25 AND UMUR <= 29 THEN 1 ELSE 0 END) R6
                          , SUM(CASE WHEN UMUR >= 30 AND UMUR <= 34 THEN 1 ELSE 0 END) R7
                          , SUM(CASE WHEN UMUR >= 35 AND UMUR <= 39 THEN 1 ELSE 0 END) R8
                          , SUM(CASE WHEN UMUR >= 40 AND UMUR <= 44 THEN 1 ELSE 0 END) R9
                          , SUM(CASE WHEN UMUR >= 45 AND UMUR <= 49 THEN 1 ELSE 0 END) R10
                          , SUM(CASE WHEN UMUR >= 50 AND UMUR <= 54 THEN 1 ELSE 0 END) R11
                          , SUM(CASE WHEN UMUR >= 55 AND UMUR <= 59 THEN 1 ELSE 0 END) R12
                          , SUM(CASE WHEN UMUR >= 60 AND UMUR <= 64 THEN 1 ELSE 0 END) R13
                          , SUM(CASE WHEN UMUR >= 65 AND UMUR <= 69 THEN 1 ELSE 0 END) R14
                          , SUM(CASE WHEN UMUR >= 70 AND UMUR <= 74 THEN 1 ELSE 0 END) R15
                          , SUM(CASE WHEN UMUR >= 75  THEN 1 ELSE 0 END) R16
						  , COUNT(1) JUMLAH
					FROM KAMPUNG_GISA_DKB WHERE HUBUNGAN_DLM_KELUARGA = 1 AND 1=1 ";
						if($no_kel == 0){    
					$sql .=" AND NO_PROP = 32 AND NO_KAB = 73 AND NO_KEC =".$no_kec."
						    GROUP BY NO_KEL
						    ORDER BY NO_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND NO_PROP = 32 AND NO_KAB = 73 AND NO_KEC =".$no_kec." AND NO_KEL = ".$no_kel."
						    GROUP BY NO_KEL
						    ORDER BY NO_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::select($sql);
               return $r;
		}
		
		public function mst_get_laporan_kk_usia_lk($no_kec,$no_kel,$dkb_biodata = '',$dkb_cutoff = '',$dkb_gisa = ''){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT 
					  A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL, B.R1 , B.R2 , B.R3, B.R4 , B.R5 , B.R6 , B.R7 , B.R8 , B.R9 , B.R10 , B.R11 , B.R12 , B.R13, B.R14, B.R15, B.R16, B.JUMLAH
					  FROM SETUP_KEC A LEFT JOIN (SELECT NO_KEC
                          , SUM(CASE WHEN UMUR >= 0 AND UMUR <= 4 THEN 1 ELSE 0 END) R1
                          , SUM(CASE WHEN UMUR >= 5 AND UMUR <= 9 THEN 1 ELSE 0 END) R2
                          , SUM(CASE WHEN UMUR >= 10 AND UMUR <= 14 THEN 1 ELSE 0 END) R3
                          , SUM(CASE WHEN UMUR >= 15 AND UMUR <= 19 THEN 1 ELSE 0 END) R4
                          , SUM(CASE WHEN UMUR >= 20 AND UMUR <= 24 THEN 1 ELSE 0 END) R5
                          , SUM(CASE WHEN UMUR >= 25 AND UMUR <= 29 THEN 1 ELSE 0 END) R6
                          , SUM(CASE WHEN UMUR >= 30 AND UMUR <= 34 THEN 1 ELSE 0 END) R7
                          , SUM(CASE WHEN UMUR >= 35 AND UMUR <= 39 THEN 1 ELSE 0 END) R8
                          , SUM(CASE WHEN UMUR >= 40 AND UMUR <= 44 THEN 1 ELSE 0 END) R9
                          , SUM(CASE WHEN UMUR >= 45 AND UMUR <= 49 THEN 1 ELSE 0 END) R10
                          , SUM(CASE WHEN UMUR >= 50 AND UMUR <= 54 THEN 1 ELSE 0 END) R11
                          , SUM(CASE WHEN UMUR >= 55 AND UMUR <= 59 THEN 1 ELSE 0 END) R12
                          , SUM(CASE WHEN UMUR >= 60 AND UMUR <= 64 THEN 1 ELSE 0 END) R13
                          , SUM(CASE WHEN UMUR >= 65 AND UMUR <= 69 THEN 1 ELSE 0 END) R14
                          , SUM(CASE WHEN UMUR >= 70 AND UMUR <= 74 THEN 1 ELSE 0 END) R15
                          , SUM(CASE WHEN UMUR >= 75  THEN 1 ELSE 0 END) R16
						  , COUNT(1) JUMLAH
					FROM KAMPUNG_GISA_DKB WHERE HUBUNGAN_DLM_KELUARGA = 1 AND JENIS_KELAMIN =1 AND 1=1 AND NO_PROP = 32 AND NO_KAB =73 
					GROUP BY NO_KEC
					ORDER BY NO_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT 
					  A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL, B.R1 , B.R2 , B.R3, B.R4 , B.R5 , B.R6 , B.R7 , B.R8 , B.R9 , B.R10 , B.R11 , B.R12 , B.R13, B.R14, B.R15, B.R16, B.JUMLAH
					  FROM SETUP_KEL A LEFT JOIN (SELECT NO_KEL
                          , SUM(CASE WHEN UMUR >= 0 AND UMUR <= 4 THEN 1 ELSE 0 END) R1
                          , SUM(CASE WHEN UMUR >= 5 AND UMUR <= 9 THEN 1 ELSE 0 END) R2
                          , SUM(CASE WHEN UMUR >= 10 AND UMUR <= 14 THEN 1 ELSE 0 END) R3
                          , SUM(CASE WHEN UMUR >= 15 AND UMUR <= 19 THEN 1 ELSE 0 END) R4
                          , SUM(CASE WHEN UMUR >= 20 AND UMUR <= 24 THEN 1 ELSE 0 END) R5
                          , SUM(CASE WHEN UMUR >= 25 AND UMUR <= 29 THEN 1 ELSE 0 END) R6
                          , SUM(CASE WHEN UMUR >= 30 AND UMUR <= 34 THEN 1 ELSE 0 END) R7
                          , SUM(CASE WHEN UMUR >= 35 AND UMUR <= 39 THEN 1 ELSE 0 END) R8
                          , SUM(CASE WHEN UMUR >= 40 AND UMUR <= 44 THEN 1 ELSE 0 END) R9
                          , SUM(CASE WHEN UMUR >= 45 AND UMUR <= 49 THEN 1 ELSE 0 END) R10
                          , SUM(CASE WHEN UMUR >= 50 AND UMUR <= 54 THEN 1 ELSE 0 END) R11
                          , SUM(CASE WHEN UMUR >= 55 AND UMUR <= 59 THEN 1 ELSE 0 END) R12
                          , SUM(CASE WHEN UMUR >= 60 AND UMUR <= 64 THEN 1 ELSE 0 END) R13
                          , SUM(CASE WHEN UMUR >= 65 AND UMUR <= 69 THEN 1 ELSE 0 END) R14
                          , SUM(CASE WHEN UMUR >= 70 AND UMUR <= 74 THEN 1 ELSE 0 END) R15
                          , SUM(CASE WHEN UMUR >= 75  THEN 1 ELSE 0 END) R16
						  , COUNT(1) JUMLAH
					FROM KAMPUNG_GISA_DKB WHERE HUBUNGAN_DLM_KELUARGA = 1 AND JENIS_KELAMIN =1 AND 1=1 ";
						if($no_kel == 0){    
					$sql .=" AND NO_PROP = 32 AND NO_KAB = 73 AND NO_KEC =".$no_kec."
						    GROUP BY NO_KEL
						    ORDER BY NO_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND NO_PROP = 32 AND NO_KAB = 73 AND NO_KEC =".$no_kec." AND NO_KEL = ".$no_kel."
						    GROUP BY NO_KEL
						    ORDER BY NO_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::select($sql);
               return $r;
		}
		
		public function mst_get_laporan_kk_usia_pr($no_kec,$no_kel,$dkb_biodata = '',$dkb_cutoff = '',$dkb_gisa = ''){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT 
					  A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL, B.R1 , B.R2 , B.R3, B.R4 , B.R5 , B.R6 , B.R7 , B.R8 , B.R9 , B.R10 , B.R11 , B.R12 , B.R13, B.R14, B.R15, B.R16, B.JUMLAH
					  FROM SETUP_KEC A LEFT JOIN (SELECT NO_KEC
                          , SUM(CASE WHEN UMUR >= 0 AND UMUR <= 4 THEN 1 ELSE 0 END) R1
                          , SUM(CASE WHEN UMUR >= 5 AND UMUR <= 9 THEN 1 ELSE 0 END) R2
                          , SUM(CASE WHEN UMUR >= 10 AND UMUR <= 14 THEN 1 ELSE 0 END) R3
                          , SUM(CASE WHEN UMUR >= 15 AND UMUR <= 19 THEN 1 ELSE 0 END) R4
                          , SUM(CASE WHEN UMUR >= 20 AND UMUR <= 24 THEN 1 ELSE 0 END) R5
                          , SUM(CASE WHEN UMUR >= 25 AND UMUR <= 29 THEN 1 ELSE 0 END) R6
                          , SUM(CASE WHEN UMUR >= 30 AND UMUR <= 34 THEN 1 ELSE 0 END) R7
                          , SUM(CASE WHEN UMUR >= 35 AND UMUR <= 39 THEN 1 ELSE 0 END) R8
                          , SUM(CASE WHEN UMUR >= 40 AND UMUR <= 44 THEN 1 ELSE 0 END) R9
                          , SUM(CASE WHEN UMUR >= 45 AND UMUR <= 49 THEN 1 ELSE 0 END) R10
                          , SUM(CASE WHEN UMUR >= 50 AND UMUR <= 54 THEN 1 ELSE 0 END) R11
                          , SUM(CASE WHEN UMUR >= 55 AND UMUR <= 59 THEN 1 ELSE 0 END) R12
                          , SUM(CASE WHEN UMUR >= 60 AND UMUR <= 64 THEN 1 ELSE 0 END) R13
                          , SUM(CASE WHEN UMUR >= 65 AND UMUR <= 69 THEN 1 ELSE 0 END) R14
                          , SUM(CASE WHEN UMUR >= 70 AND UMUR <= 74 THEN 1 ELSE 0 END) R15
                          , SUM(CASE WHEN UMUR >= 75  THEN 1 ELSE 0 END) R16
						  , COUNT(1) JUMLAH
					FROM KAMPUNG_GISA_DKB WHERE HUBUNGAN_DLM_KELUARGA = 1 AND JENIS_KELAMIN =2 AND 1=1 AND NO_PROP = 32 AND NO_KAB =73 
					GROUP BY NO_KEC
					ORDER BY NO_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT 
					  A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL, B.R1 , B.R2 , B.R3, B.R4 , B.R5 , B.R6 , B.R7 , B.R8 , B.R9 , B.R10 , B.R11 , B.R12 , B.R13, B.R14, B.R15, B.R16, B.JUMLAH
					  FROM SETUP_KEL A LEFT JOIN (SELECT NO_KEL
                          , SUM(CASE WHEN UMUR >= 0 AND UMUR <= 4 THEN 1 ELSE 0 END) R1
                          , SUM(CASE WHEN UMUR >= 5 AND UMUR <= 9 THEN 1 ELSE 0 END) R2
                          , SUM(CASE WHEN UMUR >= 10 AND UMUR <= 14 THEN 1 ELSE 0 END) R3
                          , SUM(CASE WHEN UMUR >= 15 AND UMUR <= 19 THEN 1 ELSE 0 END) R4
                          , SUM(CASE WHEN UMUR >= 20 AND UMUR <= 24 THEN 1 ELSE 0 END) R5
                          , SUM(CASE WHEN UMUR >= 25 AND UMUR <= 29 THEN 1 ELSE 0 END) R6
                          , SUM(CASE WHEN UMUR >= 30 AND UMUR <= 34 THEN 1 ELSE 0 END) R7
                          , SUM(CASE WHEN UMUR >= 35 AND UMUR <= 39 THEN 1 ELSE 0 END) R8
                          , SUM(CASE WHEN UMUR >= 40 AND UMUR <= 44 THEN 1 ELSE 0 END) R9
                          , SUM(CASE WHEN UMUR >= 45 AND UMUR <= 49 THEN 1 ELSE 0 END) R10
                          , SUM(CASE WHEN UMUR >= 50 AND UMUR <= 54 THEN 1 ELSE 0 END) R11
                          , SUM(CASE WHEN UMUR >= 55 AND UMUR <= 59 THEN 1 ELSE 0 END) R12
                          , SUM(CASE WHEN UMUR >= 60 AND UMUR <= 64 THEN 1 ELSE 0 END) R13
                          , SUM(CASE WHEN UMUR >= 65 AND UMUR <= 69 THEN 1 ELSE 0 END) R14
                          , SUM(CASE WHEN UMUR >= 70 AND UMUR <= 74 THEN 1 ELSE 0 END) R15
                          , SUM(CASE WHEN UMUR >= 75  THEN 1 ELSE 0 END) R16
						  , COUNT(1) JUMLAH
					FROM KAMPUNG_GISA_DKB WHERE HUBUNGAN_DLM_KELUARGA = 1 AND JENIS_KELAMIN =2 AND 1=1 ";
						if($no_kel == 0){    
					$sql .=" AND NO_PROP = 32 AND NO_KAB = 73 AND NO_KEC =".$no_kec."
						    GROUP BY NO_KEL
						    ORDER BY NO_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND NO_PROP = 32 AND NO_KAB = 73 AND NO_KEC =".$no_kec." AND NO_KEL = ".$no_kel."
						    GROUP BY NO_KEL
						    ORDER BY NO_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::select($sql);
               return $r;
		}
		
		public function mst_get_laporan_kk_pekerjaan($no_kec,$no_kel,$dkb_biodata = '',$dkb_cutoff = '',$dkb_gisa = ''){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT 
					  A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL, B.R1 , B.R2 , B.R3, B.R4 , B.R5 , B.R6 , B.R7 , B.R8 , B.R9 , B.R10 , B.R11 , B.R12 , B.JUMLAH
					  FROM SETUP_KEC A LEFT JOIN (SELECT NO_KEC
                          , SUM(CASE WHEN JENIS_PEKERJAAN IN (1) THEN 1 ELSE 0 END) R1
						  , SUM(CASE WHEN JENIS_PEKERJAAN IN (2) THEN 1 ELSE 0 END) R2
						  , SUM(CASE WHEN JENIS_PEKERJAAN IN (3) THEN 1 ELSE 0 END) R3
						  , SUM(CASE WHEN JENIS_PEKERJAAN IN (5) THEN 1 ELSE 0 END) R4
						  , SUM(CASE WHEN JENIS_PEKERJAAN IN (6,7) THEN 1 ELSE 0 END) R5
						  , SUM(CASE WHEN JENIS_PEKERJAAN IN (4) THEN 1 ELSE 0 END) R6
						  , SUM(CASE WHEN JENIS_PEKERJAAN IN (15) THEN 1 ELSE 0 END) R7
						  , SUM(CASE WHEN JENIS_PEKERJAAN IN (16,17) THEN 1 ELSE 0 END) R8
						  , SUM(CASE WHEN JENIS_PEKERJAAN IN (31,72,73,74,75,76) THEN 1 ELSE 0 END) R9
						  , SUM(CASE WHEN JENIS_PEKERJAAN IN (88) THEN 1 ELSE 0 END) R10 
            			  , SUM(CASE WHEN JENIS_PEKERJAAN IN (64,65) THEN 1 ELSE 0 END) R11
            			  , SUM(CASE WHEN JENIS_PEKERJAAN NOT IN (1,2,3,4,5,6,7,15,16,17,31,64,65,72,73,74,75,76,88) THEN 1 ELSE 0 END) R12
						  , COUNT(1) JUMLAH
					FROM KAMPUNG_GISA_DKB WHERE HUBUNGAN_DLM_KELUARGA = 1 AND 1=1 AND NO_PROP = 32 AND NO_KAB =73 
					GROUP BY NO_KEC
					ORDER BY NO_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT 
					  A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL, B.R1 , B.R2 , B.R3, B.R4 , B.R5 , B.R6 , B.R7 , B.R8 , B.R9 , B.R10 , B.R11 , B.R12 , B.JUMLAH
					  FROM SETUP_KEL A LEFT JOIN (SELECT NO_KEL
                          , SUM(CASE WHEN JENIS_PEKERJAAN IN (1) THEN 1 ELSE 0 END) R1
						  , SUM(CASE WHEN JENIS_PEKERJAAN IN (2) THEN 1 ELSE 0 END) R2
						  , SUM(CASE WHEN JENIS_PEKERJAAN IN (3) THEN 1 ELSE 0 END) R3
						  , SUM(CASE WHEN JENIS_PEKERJAAN IN (5) THEN 1 ELSE 0 END) R4
						  , SUM(CASE WHEN JENIS_PEKERJAAN IN (6,7) THEN 1 ELSE 0 END) R5
						  , SUM(CASE WHEN JENIS_PEKERJAAN IN (4) THEN 1 ELSE 0 END) R6
						  , SUM(CASE WHEN JENIS_PEKERJAAN IN (15) THEN 1 ELSE 0 END) R7
						  , SUM(CASE WHEN JENIS_PEKERJAAN IN (16,17) THEN 1 ELSE 0 END) R8
						  , SUM(CASE WHEN JENIS_PEKERJAAN IN (31,72,73,74,75,76) THEN 1 ELSE 0 END) R9
						  , SUM(CASE WHEN JENIS_PEKERJAAN IN (88) THEN 1 ELSE 0 END) R10 
            			  , SUM(CASE WHEN JENIS_PEKERJAAN IN (64,65) THEN 1 ELSE 0 END) R11
            			  , SUM(CASE WHEN JENIS_PEKERJAAN NOT IN (1,2,3,4,5,6,7,15,16,17,31,64,65,72,73,74,75,76,88) THEN 1 ELSE 0 END) R12
						  , COUNT(1) JUMLAH
					FROM KAMPUNG_GISA_DKB WHERE HUBUNGAN_DLM_KELUARGA = 1 AND 1=1 ";
						if($no_kel == 0){    
					$sql .=" AND NO_PROP = 32 AND NO_KAB = 73 AND NO_KEC =".$no_kec."
						    GROUP BY NO_KEL
						    ORDER BY NO_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND NO_PROP = 32 AND NO_KAB = 73 AND NO_KEC =".$no_kec." AND NO_KEL = ".$no_kel."
						    GROUP BY NO_KEL
						    ORDER BY NO_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::select($sql);
               return $r;
		}
		
		public function mst_get_laporan_kk_pekerjaan_lk($no_kec,$no_kel,$dkb_biodata = '',$dkb_cutoff = '',$dkb_gisa = ''){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT 
					  A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL, B.R1 , B.R2 , B.R3, B.R4 , B.R5 , B.R6 , B.R7 , B.R8 , B.R9 , B.R10 , B.R11 , B.R12 , B.JUMLAH
					  FROM SETUP_KEC A LEFT JOIN (SELECT NO_KEC
                          , SUM(CASE WHEN JENIS_PEKERJAAN IN (1) THEN 1 ELSE 0 END) R1
						  , SUM(CASE WHEN JENIS_PEKERJAAN IN (2) THEN 1 ELSE 0 END) R2
						  , SUM(CASE WHEN JENIS_PEKERJAAN IN (3) THEN 1 ELSE 0 END) R3
						  , SUM(CASE WHEN JENIS_PEKERJAAN IN (5) THEN 1 ELSE 0 END) R4
						  , SUM(CASE WHEN JENIS_PEKERJAAN IN (6,7) THEN 1 ELSE 0 END) R5
						  , SUM(CASE WHEN JENIS_PEKERJAAN IN (4) THEN 1 ELSE 0 END) R6
						  , SUM(CASE WHEN JENIS_PEKERJAAN IN (15) THEN 1 ELSE 0 END) R7
						  , SUM(CASE WHEN JENIS_PEKERJAAN IN (16,17) THEN 1 ELSE 0 END) R8
						  , SUM(CASE WHEN JENIS_PEKERJAAN IN (31,72,73,74,75,76) THEN 1 ELSE 0 END) R9
						  , SUM(CASE WHEN JENIS_PEKERJAAN IN (88) THEN 1 ELSE 0 END) R10 
            			  , SUM(CASE WHEN JENIS_PEKERJAAN IN (64,65) THEN 1 ELSE 0 END) R11
            			  , SUM(CASE WHEN JENIS_PEKERJAAN NOT IN (1,2,3,4,5,6,7,15,16,17,31,64,65,72,73,74,75,76,88) THEN 1 ELSE 0 END) R12
						  , COUNT(1) JUMLAH
					FROM KAMPUNG_GISA_DKB WHERE HUBUNGAN_DLM_KELUARGA = 1 AND JENIS_KELAMIN =1 AND 1=1 AND NO_PROP = 32 AND NO_KAB =73 
					GROUP BY NO_KEC
					ORDER BY NO_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT 
					  A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL, B.R1 , B.R2 , B.R3, B.R4 , B.R5 , B.R6 , B.R7 , B.R8 , B.R9 , B.R10 , B.R11 , B.R12 , B.JUMLAH
					  FROM SETUP_KEL A LEFT JOIN (SELECT NO_KEL
                          , SUM(CASE WHEN JENIS_PEKERJAAN IN (1) THEN 1 ELSE 0 END) R1
						  , SUM(CASE WHEN JENIS_PEKERJAAN IN (2) THEN 1 ELSE 0 END) R2
						  , SUM(CASE WHEN JENIS_PEKERJAAN IN (3) THEN 1 ELSE 0 END) R3
						  , SUM(CASE WHEN JENIS_PEKERJAAN IN (5) THEN 1 ELSE 0 END) R4
						  , SUM(CASE WHEN JENIS_PEKERJAAN IN (6,7) THEN 1 ELSE 0 END) R5
						  , SUM(CASE WHEN JENIS_PEKERJAAN IN (4) THEN 1 ELSE 0 END) R6
						  , SUM(CASE WHEN JENIS_PEKERJAAN IN (15) THEN 1 ELSE 0 END) R7
						  , SUM(CASE WHEN JENIS_PEKERJAAN IN (16,17) THEN 1 ELSE 0 END) R8
						  , SUM(CASE WHEN JENIS_PEKERJAAN IN (31,72,73,74,75,76) THEN 1 ELSE 0 END) R9
						  , SUM(CASE WHEN JENIS_PEKERJAAN IN (88) THEN 1 ELSE 0 END) R10 
            			  , SUM(CASE WHEN JENIS_PEKERJAAN IN (64,65) THEN 1 ELSE 0 END) R11
            			  , SUM(CASE WHEN JENIS_PEKERJAAN NOT IN (1,2,3,4,5,6,7,15,16,17,31,64,65,72,73,74,75,76,88) THEN 1 ELSE 0 END) R12
						  , COUNT(1) JUMLAH
					FROM KAMPUNG_GISA_DKB WHERE HUBUNGAN_DLM_KELUARGA = 1 AND JENIS_KELAMIN =1 AND 1=1 ";
						if($no_kel == 0){    
					$sql .=" AND NO_PROP = 32 AND NO_KAB = 73 AND NO_KEC =".$no_kec."
						    GROUP BY NO_KEL
						    ORDER BY NO_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND NO_PROP = 32 AND NO_KAB = 73 AND NO_KEC =".$no_kec." AND NO_KEL = ".$no_kel."
						    GROUP BY NO_KEL
						    ORDER BY NO_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::select($sql);
               return $r;
		}
		
		public function mst_get_laporan_kk_pekerjaan_pr($no_kec,$no_kel,$dkb_biodata = '',$dkb_cutoff = '',$dkb_gisa = ''){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT 
					  A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL, B.R1 , B.R2 , B.R3, B.R4 , B.R5 , B.R6 , B.R7 , B.R8 , B.R9 , B.R10 , B.R11 , B.R12 , B.JUMLAH
					  FROM SETUP_KEC A LEFT JOIN (SELECT NO_KEC
                          , SUM(CASE WHEN JENIS_PEKERJAAN IN (1) THEN 1 ELSE 0 END) R1
						  , SUM(CASE WHEN JENIS_PEKERJAAN IN (2) THEN 1 ELSE 0 END) R2
						  , SUM(CASE WHEN JENIS_PEKERJAAN IN (3) THEN 1 ELSE 0 END) R3
						  , SUM(CASE WHEN JENIS_PEKERJAAN IN (5) THEN 1 ELSE 0 END) R4
						  , SUM(CASE WHEN JENIS_PEKERJAAN IN (6,7) THEN 1 ELSE 0 END) R5
						  , SUM(CASE WHEN JENIS_PEKERJAAN IN (4) THEN 1 ELSE 0 END) R6
						  , SUM(CASE WHEN JENIS_PEKERJAAN IN (15) THEN 1 ELSE 0 END) R7
						  , SUM(CASE WHEN JENIS_PEKERJAAN IN (16,17) THEN 1 ELSE 0 END) R8
						  , SUM(CASE WHEN JENIS_PEKERJAAN IN (31,72,73,74,75,76) THEN 1 ELSE 0 END) R9
						  , SUM(CASE WHEN JENIS_PEKERJAAN IN (88) THEN 1 ELSE 0 END) R10 
            			  , SUM(CASE WHEN JENIS_PEKERJAAN IN (64,65) THEN 1 ELSE 0 END) R11
            			  , SUM(CASE WHEN JENIS_PEKERJAAN NOT IN (1,2,3,4,5,6,7,15,16,17,31,64,65,72,73,74,75,76,88) THEN 1 ELSE 0 END) R12
						  , COUNT(1) JUMLAH
					FROM KAMPUNG_GISA_DKB WHERE HUBUNGAN_DLM_KELUARGA = 1 AND JENIS_KELAMIN =2 AND 1=1 AND NO_PROP = 32 AND NO_KAB =73 
					GROUP BY NO_KEC
					ORDER BY NO_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT 
					  A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL, B.R1 , B.R2 , B.R3, B.R4 , B.R5 , B.R6 , B.R7 , B.R8 , B.R9 , B.R10 , B.R11 , B.R12 , B.JUMLAH
					  FROM SETUP_KEL A LEFT JOIN (SELECT NO_KEL
                          , SUM(CASE WHEN JENIS_PEKERJAAN IN (1) THEN 1 ELSE 0 END) R1
						  , SUM(CASE WHEN JENIS_PEKERJAAN IN (2) THEN 1 ELSE 0 END) R2
						  , SUM(CASE WHEN JENIS_PEKERJAAN IN (3) THEN 1 ELSE 0 END) R3
						  , SUM(CASE WHEN JENIS_PEKERJAAN IN (5) THEN 1 ELSE 0 END) R4
						  , SUM(CASE WHEN JENIS_PEKERJAAN IN (6,7) THEN 1 ELSE 0 END) R5
						  , SUM(CASE WHEN JENIS_PEKERJAAN IN (4) THEN 1 ELSE 0 END) R6
						  , SUM(CASE WHEN JENIS_PEKERJAAN IN (15) THEN 1 ELSE 0 END) R7
						  , SUM(CASE WHEN JENIS_PEKERJAAN IN (16,17) THEN 1 ELSE 0 END) R8
						  , SUM(CASE WHEN JENIS_PEKERJAAN IN (31,72,73,74,75,76) THEN 1 ELSE 0 END) R9
						  , SUM(CASE WHEN JENIS_PEKERJAAN IN (88) THEN 1 ELSE 0 END) R10 
            			  , SUM(CASE WHEN JENIS_PEKERJAAN IN (64,65) THEN 1 ELSE 0 END) R11
            			  , SUM(CASE WHEN JENIS_PEKERJAAN NOT IN (1,2,3,4,5,6,7,15,16,17,31,64,65,72,73,74,75,76,88) THEN 1 ELSE 0 END) R12
						  , COUNT(1) JUMLAH
					FROM KAMPUNG_GISA_DKB WHERE HUBUNGAN_DLM_KELUARGA = 1 AND JENIS_KELAMIN =2 AND 1=1 ";
						if($no_kel == 0){    
					$sql .=" AND NO_PROP = 32 AND NO_KAB = 73 AND NO_KEC =".$no_kec."
						    GROUP BY NO_KEL
						    ORDER BY NO_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND NO_PROP = 32 AND NO_KAB = 73 AND NO_KEC =".$no_kec." AND NO_KEL = ".$no_kel."
						    GROUP BY NO_KEL
						    ORDER BY NO_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::select($sql);
               return $r;
		}

		
		public function mst_get_laporan_pendidikan($no_kec,$no_kel,$dkb_biodata = '',$dkb_cutoff = '',$dkb_gisa = ''){
			if (!empty($dkb_biodata)){
				$dkb_bio = $dkb_biodata;
			}else{
				$dkb_bio = Shr::do_get_dkb_bio();
			}
			if (!empty($dkb_cutoff)){
				$cut_off_dkb = $dkb_cutoff;
			}else{
				$cut_off_dkb = Shr::do_get_cut_off_date();
			}
			if (!empty($dkb_gisa)){
				$dkb_gisa = $dkb_gisa;
			}else{
				$dkb_gisa = Shr::do_get_dkb_gisa();
			}
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL
					, CASE WHEN B.R1 IS NULL THEN 0 ELSE B.R1 END AS R1
					, CASE WHEN B.R2 IS NULL THEN 0 ELSE B.R2 END AS R2
					, CASE WHEN B.R3 IS NULL THEN 0 ELSE B.R3 END AS R3
					, CASE WHEN B.R4 IS NULL THEN 0 ELSE B.R4 END AS R4
					, CASE WHEN B.R5 IS NULL THEN 0 ELSE B.R5 END AS R5
					, CASE WHEN B.R6 IS NULL THEN 0 ELSE B.R6 END AS R6
					, CASE WHEN B.R7 IS NULL THEN 0 ELSE B.R7 END AS R7
					, CASE WHEN B.R8 IS NULL THEN 0 ELSE B.R8 END AS R8
					, CASE WHEN B.R9 IS NULL THEN 0 ELSE B.R9 END AS R9
					, CASE WHEN B.R10 IS NULL THEN 0 ELSE B.R10 END AS R10
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEC A LEFT JOIN
					(SELECT 
						B.NO_KEC 
						,B.NAMA_KEC 
						, SUM(CASE WHEN A.PDDK_AKH NOT BETWEEN 2 AND 10 OR PDDK_AKH IS NULL THEN 1 ELSE 0 END) R1
						, SUM(CASE WHEN A.PDDK_AKH = 2 THEN 1 ELSE 0 END) R2
						, SUM(CASE WHEN A.PDDK_AKH = 3 THEN 1 ELSE 0 END) R3
						, SUM(CASE WHEN A.PDDK_AKH = 4 THEN 1 ELSE 0 END) R4
						, SUM(CASE WHEN A.PDDK_AKH = 5 THEN 1 ELSE 0 END) R5
						, SUM(CASE WHEN A.PDDK_AKH = 6 THEN 1 ELSE 0 END) R6
						, SUM(CASE WHEN A.PDDK_AKH = 7 THEN 1 ELSE 0 END) R7
						, SUM(CASE WHEN A.PDDK_AKH = 8 THEN 1 ELSE 0 END) R8
						, SUM(CASE WHEN A.PDDK_AKH = 9 THEN 1 ELSE 0 END) R9
						, SUM(CASE WHEN A.PDDK_AKH = 10 THEN 1 ELSE 0 END) R10
						, COUNT(1) JUMLAH
						FROM SETUP_KEC B LEFT JOIN ".$dkb_bio." A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC WHERE  (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) AND A.NO_PROP = 32 AND A.NO_KAB =73 GROUP BY B.NO_KEC,B.NAMA_KEC ORDER BY B.NO_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL
					, CASE WHEN B.R1 IS NULL THEN 0 ELSE B.R1 END AS R1
					, CASE WHEN B.R2 IS NULL THEN 0 ELSE B.R2 END AS R2
					, CASE WHEN B.R3 IS NULL THEN 0 ELSE B.R3 END AS R3
					, CASE WHEN B.R4 IS NULL THEN 0 ELSE B.R4 END AS R4
					, CASE WHEN B.R5 IS NULL THEN 0 ELSE B.R5 END AS R5
					, CASE WHEN B.R6 IS NULL THEN 0 ELSE B.R6 END AS R6
					, CASE WHEN B.R7 IS NULL THEN 0 ELSE B.R7 END AS R7
					, CASE WHEN B.R8 IS NULL THEN 0 ELSE B.R8 END AS R8
					, CASE WHEN B.R9 IS NULL THEN 0 ELSE B.R9 END AS R9
					, CASE WHEN B.R10 IS NULL THEN 0 ELSE B.R10 END AS R10
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEL A LEFT JOIN
					(SELECT 
						B.NO_KEL 
						,B.NAMA_KEL
						, SUM(CASE WHEN A.PDDK_AKH NOT BETWEEN 2 AND 10 OR PDDK_AKH IS NULL THEN 1 ELSE 0 END) R1
						, SUM(CASE WHEN A.PDDK_AKH = 2 THEN 1 ELSE 0 END) R2
						, SUM(CASE WHEN A.PDDK_AKH = 3 THEN 1 ELSE 0 END) R3
						, SUM(CASE WHEN A.PDDK_AKH = 4 THEN 1 ELSE 0 END) R4
						, SUM(CASE WHEN A.PDDK_AKH = 5 THEN 1 ELSE 0 END) R5
						, SUM(CASE WHEN A.PDDK_AKH = 6 THEN 1 ELSE 0 END) R6
						, SUM(CASE WHEN A.PDDK_AKH = 7 THEN 1 ELSE 0 END) R7
						, SUM(CASE WHEN A.PDDK_AKH = 8 THEN 1 ELSE 0 END) R8
						, SUM(CASE WHEN A.PDDK_AKH = 9 THEN 1 ELSE 0 END) R9
						, SUM(CASE WHEN A.PDDK_AKH = 10 THEN 1 ELSE 0 END) R10
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN ".$dkb_bio." A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL WHERE  (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) ";
						if($no_kel == 0){    
					$sql .=" AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec." and  A.NO_KEL =".$no_kel."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::select($sql);
               return $r;
		}

		
		public function mst_get_laporan_pendidikan_laki_laki($no_kec,$no_kel,$dkb_biodata = '',$dkb_cutoff = '',$dkb_gisa = ''){
			if (!empty($dkb_biodata)){
				$dkb_bio = $dkb_biodata;
			}else{
				$dkb_bio = Shr::do_get_dkb_bio();
			}
			if (!empty($dkb_cutoff)){
				$cut_off_dkb = $dkb_cutoff;
			}else{
				$cut_off_dkb = Shr::do_get_cut_off_date();
			}
			if (!empty($dkb_gisa)){
				$dkb_gisa = $dkb_gisa;
			}else{
				$dkb_gisa = Shr::do_get_dkb_gisa();
			}
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL
					, CASE WHEN B.R1 IS NULL THEN 0 ELSE B.R1 END AS R1
					, CASE WHEN B.R2 IS NULL THEN 0 ELSE B.R2 END AS R2
					, CASE WHEN B.R3 IS NULL THEN 0 ELSE B.R3 END AS R3
					, CASE WHEN B.R4 IS NULL THEN 0 ELSE B.R4 END AS R4
					, CASE WHEN B.R5 IS NULL THEN 0 ELSE B.R5 END AS R5
					, CASE WHEN B.R6 IS NULL THEN 0 ELSE B.R6 END AS R6
					, CASE WHEN B.R7 IS NULL THEN 0 ELSE B.R7 END AS R7
					, CASE WHEN B.R8 IS NULL THEN 0 ELSE B.R8 END AS R8
					, CASE WHEN B.R9 IS NULL THEN 0 ELSE B.R9 END AS R9
					, CASE WHEN B.R10 IS NULL THEN 0 ELSE B.R10 END AS R10
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEC A LEFT JOIN
					(SELECT 
						B.NO_KEC 
						,B.NAMA_KEC 
						, SUM(CASE WHEN A.PDDK_AKH NOT BETWEEN 2 AND 10 OR PDDK_AKH IS NULL THEN 1 ELSE 0 END) R1
						, SUM(CASE WHEN A.PDDK_AKH = 2 THEN 1 ELSE 0 END) R2
						, SUM(CASE WHEN A.PDDK_AKH = 3 THEN 1 ELSE 0 END) R3
						, SUM(CASE WHEN A.PDDK_AKH = 4 THEN 1 ELSE 0 END) R4
						, SUM(CASE WHEN A.PDDK_AKH = 5 THEN 1 ELSE 0 END) R5
						, SUM(CASE WHEN A.PDDK_AKH = 6 THEN 1 ELSE 0 END) R6
						, SUM(CASE WHEN A.PDDK_AKH = 7 THEN 1 ELSE 0 END) R7
						, SUM(CASE WHEN A.PDDK_AKH = 8 THEN 1 ELSE 0 END) R8
						, SUM(CASE WHEN A.PDDK_AKH = 9 THEN 1 ELSE 0 END) R9
						, SUM(CASE WHEN A.PDDK_AKH = 10 THEN 1 ELSE 0 END) R10
						, COUNT(1) JUMLAH
						FROM SETUP_KEC B LEFT JOIN ".$dkb_bio." A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC WHERE JENIS_KLMIN = 1 AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) AND A.NO_PROP = 32 AND A.NO_KAB =73 GROUP BY B.NO_KEC,B.NAMA_KEC ORDER BY B.NO_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL
					, CASE WHEN B.R1 IS NULL THEN 0 ELSE B.R1 END AS R1
					, CASE WHEN B.R2 IS NULL THEN 0 ELSE B.R2 END AS R2
					, CASE WHEN B.R3 IS NULL THEN 0 ELSE B.R3 END AS R3
					, CASE WHEN B.R4 IS NULL THEN 0 ELSE B.R4 END AS R4
					, CASE WHEN B.R5 IS NULL THEN 0 ELSE B.R5 END AS R5
					, CASE WHEN B.R6 IS NULL THEN 0 ELSE B.R6 END AS R6
					, CASE WHEN B.R7 IS NULL THEN 0 ELSE B.R7 END AS R7
					, CASE WHEN B.R8 IS NULL THEN 0 ELSE B.R8 END AS R8
					, CASE WHEN B.R9 IS NULL THEN 0 ELSE B.R9 END AS R9
					, CASE WHEN B.R10 IS NULL THEN 0 ELSE B.R10 END AS R10
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEL A LEFT JOIN
					(SELECT 
						B.NO_KEL 
						,B.NAMA_KEL
						, SUM(CASE WHEN A.PDDK_AKH NOT BETWEEN 2 AND 10 OR PDDK_AKH IS NULL THEN 1 ELSE 0 END) R1
						, SUM(CASE WHEN A.PDDK_AKH = 2 THEN 1 ELSE 0 END) R2
						, SUM(CASE WHEN A.PDDK_AKH = 3 THEN 1 ELSE 0 END) R3
						, SUM(CASE WHEN A.PDDK_AKH = 4 THEN 1 ELSE 0 END) R4
						, SUM(CASE WHEN A.PDDK_AKH = 5 THEN 1 ELSE 0 END) R5
						, SUM(CASE WHEN A.PDDK_AKH = 6 THEN 1 ELSE 0 END) R6
						, SUM(CASE WHEN A.PDDK_AKH = 7 THEN 1 ELSE 0 END) R7
						, SUM(CASE WHEN A.PDDK_AKH = 8 THEN 1 ELSE 0 END) R8
						, SUM(CASE WHEN A.PDDK_AKH = 9 THEN 1 ELSE 0 END) R9
						, SUM(CASE WHEN A.PDDK_AKH = 10 THEN 1 ELSE 0 END) R10
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN ".$dkb_bio." A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL WHERE JENIS_KLMIN = 1 AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) ";
						if($no_kel == 0){    
					$sql .=" AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec." and  A.NO_KEL =".$no_kel."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::select($sql);
               return $r;
		}

		
		public function mst_get_laporan_pendidikan_perempuan($no_kec,$no_kel,$dkb_biodata = '',$dkb_cutoff = '',$dkb_gisa = ''){
			if (!empty($dkb_biodata)){
				$dkb_bio = $dkb_biodata;
			}else{
				$dkb_bio = Shr::do_get_dkb_bio();
			}
			if (!empty($dkb_cutoff)){
				$cut_off_dkb = $dkb_cutoff;
			}else{
				$cut_off_dkb = Shr::do_get_cut_off_date();
			}
			if (!empty($dkb_gisa)){
				$dkb_gisa = $dkb_gisa;
			}else{
				$dkb_gisa = Shr::do_get_dkb_gisa();
			}
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL
					, CASE WHEN B.R1 IS NULL THEN 0 ELSE B.R1 END AS R1
					, CASE WHEN B.R2 IS NULL THEN 0 ELSE B.R2 END AS R2
					, CASE WHEN B.R3 IS NULL THEN 0 ELSE B.R3 END AS R3
					, CASE WHEN B.R4 IS NULL THEN 0 ELSE B.R4 END AS R4
					, CASE WHEN B.R5 IS NULL THEN 0 ELSE B.R5 END AS R5
					, CASE WHEN B.R6 IS NULL THEN 0 ELSE B.R6 END AS R6
					, CASE WHEN B.R7 IS NULL THEN 0 ELSE B.R7 END AS R7
					, CASE WHEN B.R8 IS NULL THEN 0 ELSE B.R8 END AS R8
					, CASE WHEN B.R9 IS NULL THEN 0 ELSE B.R9 END AS R9
					, CASE WHEN B.R10 IS NULL THEN 0 ELSE B.R10 END AS R10
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEC A LEFT JOIN
					(SELECT 
						B.NO_KEC 
						,B.NAMA_KEC 
						, SUM(CASE WHEN A.PDDK_AKH NOT BETWEEN 2 AND 10 OR PDDK_AKH IS NULL THEN 1 ELSE 0 END) R1
						, SUM(CASE WHEN A.PDDK_AKH = 2 THEN 1 ELSE 0 END) R2
						, SUM(CASE WHEN A.PDDK_AKH = 3 THEN 1 ELSE 0 END) R3
						, SUM(CASE WHEN A.PDDK_AKH = 4 THEN 1 ELSE 0 END) R4
						, SUM(CASE WHEN A.PDDK_AKH = 5 THEN 1 ELSE 0 END) R5
						, SUM(CASE WHEN A.PDDK_AKH = 6 THEN 1 ELSE 0 END) R6
						, SUM(CASE WHEN A.PDDK_AKH = 7 THEN 1 ELSE 0 END) R7
						, SUM(CASE WHEN A.PDDK_AKH = 8 THEN 1 ELSE 0 END) R8
						, SUM(CASE WHEN A.PDDK_AKH = 9 THEN 1 ELSE 0 END) R9
						, SUM(CASE WHEN A.PDDK_AKH = 10 THEN 1 ELSE 0 END) R10
						, COUNT(1) JUMLAH
						FROM SETUP_KEC B LEFT JOIN ".$dkb_bio." A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC WHERE JENIS_KLMIN = 2 AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) AND A.NO_PROP = 32 AND A.NO_KAB =73 GROUP BY B.NO_KEC,B.NAMA_KEC ORDER BY B.NO_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL
					, CASE WHEN B.R1 IS NULL THEN 0 ELSE B.R1 END AS R1
					, CASE WHEN B.R2 IS NULL THEN 0 ELSE B.R2 END AS R2
					, CASE WHEN B.R3 IS NULL THEN 0 ELSE B.R3 END AS R3
					, CASE WHEN B.R4 IS NULL THEN 0 ELSE B.R4 END AS R4
					, CASE WHEN B.R5 IS NULL THEN 0 ELSE B.R5 END AS R5
					, CASE WHEN B.R6 IS NULL THEN 0 ELSE B.R6 END AS R6
					, CASE WHEN B.R7 IS NULL THEN 0 ELSE B.R7 END AS R7
					, CASE WHEN B.R8 IS NULL THEN 0 ELSE B.R8 END AS R8
					, CASE WHEN B.R9 IS NULL THEN 0 ELSE B.R9 END AS R9
					, CASE WHEN B.R10 IS NULL THEN 0 ELSE B.R10 END AS R10
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEL A LEFT JOIN
					(SELECT 
						B.NO_KEL 
						,B.NAMA_KEL
						, SUM(CASE WHEN A.PDDK_AKH NOT BETWEEN 2 AND 10 OR PDDK_AKH IS NULL THEN 1 ELSE 0 END) R1
						, SUM(CASE WHEN A.PDDK_AKH = 2 THEN 1 ELSE 0 END) R2
						, SUM(CASE WHEN A.PDDK_AKH = 3 THEN 1 ELSE 0 END) R3
						, SUM(CASE WHEN A.PDDK_AKH = 4 THEN 1 ELSE 0 END) R4
						, SUM(CASE WHEN A.PDDK_AKH = 5 THEN 1 ELSE 0 END) R5
						, SUM(CASE WHEN A.PDDK_AKH = 6 THEN 1 ELSE 0 END) R6
						, SUM(CASE WHEN A.PDDK_AKH = 7 THEN 1 ELSE 0 END) R7
						, SUM(CASE WHEN A.PDDK_AKH = 8 THEN 1 ELSE 0 END) R8
						, SUM(CASE WHEN A.PDDK_AKH = 9 THEN 1 ELSE 0 END) R9
						, SUM(CASE WHEN A.PDDK_AKH = 10 THEN 1 ELSE 0 END) R10
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN ".$dkb_bio." A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL WHERE JENIS_KLMIN = 2 AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) ";
						if($no_kel == 0){    
					$sql .=" AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec." and  A.NO_KEL =".$no_kel."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::select($sql);
               return $r;
		}

		
		public function mst_get_laporan_pekerjaan($no_kec,$no_kel,$dkb_biodata = '',$dkb_cutoff = '',$dkb_gisa = ''){
			if (!empty($dkb_biodata)){
				$dkb_bio = $dkb_biodata;
			}else{
				$dkb_bio = Shr::do_get_dkb_bio();
			}
			if (!empty($dkb_cutoff)){
				$cut_off_dkb = $dkb_cutoff;
			}else{
				$cut_off_dkb = Shr::do_get_cut_off_date();
			}
			if (!empty($dkb_gisa)){
				$dkb_gisa = $dkb_gisa;
			}else{
				$dkb_gisa = Shr::do_get_dkb_gisa();
			}
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL
					, CASE WHEN B.R1 IS NULL THEN 0 ELSE B.R1 END AS R1
					, CASE WHEN B.R2 IS NULL THEN 0 ELSE B.R2 END AS R2
					, CASE WHEN B.R3 IS NULL THEN 0 ELSE B.R3 END AS R3
					, CASE WHEN B.R4 IS NULL THEN 0 ELSE B.R4 END AS R4
					, CASE WHEN B.R5 IS NULL THEN 0 ELSE B.R5 END AS R5
					, CASE WHEN B.R6 IS NULL THEN 0 ELSE B.R6 END AS R6
					, CASE WHEN B.R7 IS NULL THEN 0 ELSE B.R7 END AS R7
					, CASE WHEN B.R8 IS NULL THEN 0 ELSE B.R8 END AS R8
					, CASE WHEN B.R9 IS NULL THEN 0 ELSE B.R9 END AS R9
					, CASE WHEN B.R10 IS NULL THEN 0 ELSE B.R10 END AS R10
					, CASE WHEN B.R11 IS NULL THEN 0 ELSE B.R11 END AS R11
					, CASE WHEN B.R12 IS NULL THEN 0 ELSE B.R12 END AS R12
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEC A LEFT JOIN
					(SELECT 
						B.NO_KEC 
						,B.NAMA_KEC 
						, SUM(CASE WHEN A.JENIS_PKRJN IN (1) THEN 1 ELSE 0 END) R1
						, SUM(CASE WHEN A.JENIS_PKRJN IN (2) THEN 1 ELSE 0 END) R2
						, SUM(CASE WHEN A.JENIS_PKRJN IN (3) THEN 1 ELSE 0 END) R3
						, SUM(CASE WHEN A.JENIS_PKRJN IN (5) THEN 1 ELSE 0 END) R4
						, SUM(CASE WHEN A.JENIS_PKRJN IN (6,7) THEN 1 ELSE 0 END) R5
						, SUM(CASE WHEN A.JENIS_PKRJN IN (4) THEN 1 ELSE 0 END) R6
						, SUM(CASE WHEN A.JENIS_PKRJN IN (15) THEN 1 ELSE 0 END) R7
						, SUM(CASE WHEN A.JENIS_PKRJN IN (16,17) THEN 1 ELSE 0 END) R8
						, SUM(CASE WHEN A.JENIS_PKRJN IN (31,72,73,74,75,76) THEN 1 ELSE 0 END) R9
						, SUM(CASE WHEN A.JENIS_PKRJN IN (88) THEN 1 ELSE 0 END) R10 
            			, SUM(CASE WHEN A.JENIS_PKRJN IN (64,65) THEN 1 ELSE 0 END) R11
            			, SUM(CASE WHEN A.JENIS_PKRJN NOT IN (1,2,3,4,5,6,7,15,16,17,31,64,65,72,73,74,75,76,88) THEN 1 ELSE 0 END) R12
						, COUNT(1) JUMLAH
						FROM SETUP_KEC B LEFT JOIN ".$dkb_bio." A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC WHERE  (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) AND A.NO_PROP = 32 AND A.NO_KAB =73 GROUP BY B.NO_KEC,B.NAMA_KEC ORDER BY B.NO_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL
					, CASE WHEN B.R1 IS NULL THEN 0 ELSE B.R1 END AS R1
					, CASE WHEN B.R2 IS NULL THEN 0 ELSE B.R2 END AS R2
					, CASE WHEN B.R3 IS NULL THEN 0 ELSE B.R3 END AS R3
					, CASE WHEN B.R4 IS NULL THEN 0 ELSE B.R4 END AS R4
					, CASE WHEN B.R5 IS NULL THEN 0 ELSE B.R5 END AS R5
					, CASE WHEN B.R6 IS NULL THEN 0 ELSE B.R6 END AS R6
					, CASE WHEN B.R7 IS NULL THEN 0 ELSE B.R7 END AS R7
					, CASE WHEN B.R8 IS NULL THEN 0 ELSE B.R8 END AS R8
					, CASE WHEN B.R9 IS NULL THEN 0 ELSE B.R9 END AS R9
					, CASE WHEN B.R10 IS NULL THEN 0 ELSE B.R10 END AS R10
					, CASE WHEN B.R11 IS NULL THEN 0 ELSE B.R11 END AS R11
					, CASE WHEN B.R12 IS NULL THEN 0 ELSE B.R12 END AS R12
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEL A LEFT JOIN
					(SELECT 
						B.NO_KEL 
						,B.NAMA_KEL
						, SUM(CASE WHEN A.JENIS_PKRJN IN (1) THEN 1 ELSE 0 END) R1
						, SUM(CASE WHEN A.JENIS_PKRJN IN (2) THEN 1 ELSE 0 END) R2
						, SUM(CASE WHEN A.JENIS_PKRJN IN (3) THEN 1 ELSE 0 END) R3
						, SUM(CASE WHEN A.JENIS_PKRJN IN (5) THEN 1 ELSE 0 END) R4
						, SUM(CASE WHEN A.JENIS_PKRJN IN (6,7) THEN 1 ELSE 0 END) R5
						, SUM(CASE WHEN A.JENIS_PKRJN IN (4) THEN 1 ELSE 0 END) R6
						, SUM(CASE WHEN A.JENIS_PKRJN IN (15) THEN 1 ELSE 0 END) R7
						, SUM(CASE WHEN A.JENIS_PKRJN IN (16,17) THEN 1 ELSE 0 END) R8
						, SUM(CASE WHEN A.JENIS_PKRJN IN (31,72,73,74,75,76) THEN 1 ELSE 0 END) R9
						, SUM(CASE WHEN A.JENIS_PKRJN IN (88) THEN 1 ELSE 0 END) R10 
            			, SUM(CASE WHEN A.JENIS_PKRJN IN (64,65) THEN 1 ELSE 0 END) R11
            			, SUM(CASE WHEN A.JENIS_PKRJN NOT IN (1,2,3,4,5,6,7,15,16,17,31,64,65,72,73,74,75,76,88) THEN 1 ELSE 0 END) R12
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN ".$dkb_bio." A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL WHERE  (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) ";
						if($no_kel == 0){    
					$sql .=" AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec." and  A.NO_KEL =".$no_kel."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::select($sql);
               return $r;
		}

		
		public function mst_get_laporan_pekerjaan_laki_laki($no_kec,$no_kel,$dkb_biodata = '',$dkb_cutoff = '',$dkb_gisa = ''){
			if (!empty($dkb_biodata)){
				$dkb_bio = $dkb_biodata;
			}else{
				$dkb_bio = Shr::do_get_dkb_bio();
			}
			if (!empty($dkb_cutoff)){
				$cut_off_dkb = $dkb_cutoff;
			}else{
				$cut_off_dkb = Shr::do_get_cut_off_date();
			}
			if (!empty($dkb_gisa)){
				$dkb_gisa = $dkb_gisa;
			}else{
				$dkb_gisa = Shr::do_get_dkb_gisa();
			}
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL
					, CASE WHEN B.R1 IS NULL THEN 0 ELSE B.R1 END AS R1
					, CASE WHEN B.R2 IS NULL THEN 0 ELSE B.R2 END AS R2
					, CASE WHEN B.R3 IS NULL THEN 0 ELSE B.R3 END AS R3
					, CASE WHEN B.R4 IS NULL THEN 0 ELSE B.R4 END AS R4
					, CASE WHEN B.R5 IS NULL THEN 0 ELSE B.R5 END AS R5
					, CASE WHEN B.R6 IS NULL THEN 0 ELSE B.R6 END AS R6
					, CASE WHEN B.R7 IS NULL THEN 0 ELSE B.R7 END AS R7
					, CASE WHEN B.R8 IS NULL THEN 0 ELSE B.R8 END AS R8
					, CASE WHEN B.R9 IS NULL THEN 0 ELSE B.R9 END AS R9
					, CASE WHEN B.R10 IS NULL THEN 0 ELSE B.R10 END AS R10
					, CASE WHEN B.R11 IS NULL THEN 0 ELSE B.R11 END AS R11
					, CASE WHEN B.R12 IS NULL THEN 0 ELSE B.R12 END AS R12
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEC A LEFT JOIN
					(SELECT 
						B.NO_KEC 
						,B.NAMA_KEC 
						, SUM(CASE WHEN A.JENIS_PKRJN IN (1) THEN 1 ELSE 0 END) R1
						, SUM(CASE WHEN A.JENIS_PKRJN IN (2) THEN 1 ELSE 0 END) R2
						, SUM(CASE WHEN A.JENIS_PKRJN IN (3) THEN 1 ELSE 0 END) R3
						, SUM(CASE WHEN A.JENIS_PKRJN IN (5) THEN 1 ELSE 0 END) R4
						, SUM(CASE WHEN A.JENIS_PKRJN IN (6,7) THEN 1 ELSE 0 END) R5
						, SUM(CASE WHEN A.JENIS_PKRJN IN (4) THEN 1 ELSE 0 END) R6
						, SUM(CASE WHEN A.JENIS_PKRJN IN (15) THEN 1 ELSE 0 END) R7
						, SUM(CASE WHEN A.JENIS_PKRJN IN (16,17) THEN 1 ELSE 0 END) R8
						, SUM(CASE WHEN A.JENIS_PKRJN IN (31,72,73,74,75,76) THEN 1 ELSE 0 END) R9
						, SUM(CASE WHEN A.JENIS_PKRJN IN (88) THEN 1 ELSE 0 END) R10 
            			, SUM(CASE WHEN A.JENIS_PKRJN IN (64,65) THEN 1 ELSE 0 END) R11
            			, SUM(CASE WHEN A.JENIS_PKRJN NOT IN (1,2,3,4,5,6,7,15,16,17,31,64,65,72,73,74,75,76,88) THEN 1 ELSE 0 END) R12
						, COUNT(1) JUMLAH
						FROM SETUP_KEC B LEFT JOIN ".$dkb_bio." A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC WHERE JENIS_KLMIN = 1 AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) AND A.NO_PROP = 32 AND A.NO_KAB =73 GROUP BY B.NO_KEC,B.NAMA_KEC ORDER BY B.NO_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL
					, CASE WHEN B.R1 IS NULL THEN 0 ELSE B.R1 END AS R1
					, CASE WHEN B.R2 IS NULL THEN 0 ELSE B.R2 END AS R2
					, CASE WHEN B.R3 IS NULL THEN 0 ELSE B.R3 END AS R3
					, CASE WHEN B.R4 IS NULL THEN 0 ELSE B.R4 END AS R4
					, CASE WHEN B.R5 IS NULL THEN 0 ELSE B.R5 END AS R5
					, CASE WHEN B.R6 IS NULL THEN 0 ELSE B.R6 END AS R6
					, CASE WHEN B.R7 IS NULL THEN 0 ELSE B.R7 END AS R7
					, CASE WHEN B.R8 IS NULL THEN 0 ELSE B.R8 END AS R8
					, CASE WHEN B.R9 IS NULL THEN 0 ELSE B.R9 END AS R9
					, CASE WHEN B.R10 IS NULL THEN 0 ELSE B.R10 END AS R10
					, CASE WHEN B.R11 IS NULL THEN 0 ELSE B.R11 END AS R11
					, CASE WHEN B.R12 IS NULL THEN 0 ELSE B.R12 END AS R12
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEL A LEFT JOIN
					(SELECT 
						B.NO_KEL 
						,B.NAMA_KEL
						, SUM(CASE WHEN A.JENIS_PKRJN IN (1) THEN 1 ELSE 0 END) R1
						, SUM(CASE WHEN A.JENIS_PKRJN IN (2) THEN 1 ELSE 0 END) R2
						, SUM(CASE WHEN A.JENIS_PKRJN IN (3) THEN 1 ELSE 0 END) R3
						, SUM(CASE WHEN A.JENIS_PKRJN IN (5) THEN 1 ELSE 0 END) R4
						, SUM(CASE WHEN A.JENIS_PKRJN IN (6,7) THEN 1 ELSE 0 END) R5
						, SUM(CASE WHEN A.JENIS_PKRJN IN (4) THEN 1 ELSE 0 END) R6
						, SUM(CASE WHEN A.JENIS_PKRJN IN (15) THEN 1 ELSE 0 END) R7
						, SUM(CASE WHEN A.JENIS_PKRJN IN (16,17) THEN 1 ELSE 0 END) R8
						, SUM(CASE WHEN A.JENIS_PKRJN IN (31,72,73,74,75,76) THEN 1 ELSE 0 END) R9
						, SUM(CASE WHEN A.JENIS_PKRJN IN (88) THEN 1 ELSE 0 END) R10 
            			, SUM(CASE WHEN A.JENIS_PKRJN IN (64,65) THEN 1 ELSE 0 END) R11
            			, SUM(CASE WHEN A.JENIS_PKRJN NOT IN (1,2,3,4,5,6,7,15,16,17,31,64,65,72,73,74,75,76,88) THEN 1 ELSE 0 END) R12
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN ".$dkb_bio." A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL WHERE JENIS_KLMIN = 1 AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) ";
						if($no_kel == 0){    
					$sql .=" AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec." and  A.NO_KEL =".$no_kel."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::select($sql);
               return $r;
		}

		
		public function mst_get_laporan_pekerjaan_perempuan($no_kec,$no_kel,$dkb_biodata = '',$dkb_cutoff = '',$dkb_gisa = ''){
			if (!empty($dkb_biodata)){
				$dkb_bio = $dkb_biodata;
			}else{
				$dkb_bio = Shr::do_get_dkb_bio();
			}
			if (!empty($dkb_cutoff)){
				$cut_off_dkb = $dkb_cutoff;
			}else{
				$cut_off_dkb = Shr::do_get_cut_off_date();
			}
			if (!empty($dkb_gisa)){
				$dkb_gisa = $dkb_gisa;
			}else{
				$dkb_gisa = Shr::do_get_dkb_gisa();
			}
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL
					, CASE WHEN B.R1 IS NULL THEN 0 ELSE B.R1 END AS R1
					, CASE WHEN B.R2 IS NULL THEN 0 ELSE B.R2 END AS R2
					, CASE WHEN B.R3 IS NULL THEN 0 ELSE B.R3 END AS R3
					, CASE WHEN B.R4 IS NULL THEN 0 ELSE B.R4 END AS R4
					, CASE WHEN B.R5 IS NULL THEN 0 ELSE B.R5 END AS R5
					, CASE WHEN B.R6 IS NULL THEN 0 ELSE B.R6 END AS R6
					, CASE WHEN B.R7 IS NULL THEN 0 ELSE B.R7 END AS R7
					, CASE WHEN B.R8 IS NULL THEN 0 ELSE B.R8 END AS R8
					, CASE WHEN B.R9 IS NULL THEN 0 ELSE B.R9 END AS R9
					, CASE WHEN B.R10 IS NULL THEN 0 ELSE B.R10 END AS R10
					, CASE WHEN B.R11 IS NULL THEN 0 ELSE B.R11 END AS R11
					, CASE WHEN B.R12 IS NULL THEN 0 ELSE B.R12 END AS R12
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEC A LEFT JOIN
					(SELECT 
						B.NO_KEC 
						,B.NAMA_KEC 
						, SUM(CASE WHEN A.JENIS_PKRJN IN (1) THEN 1 ELSE 0 END) R1
						, SUM(CASE WHEN A.JENIS_PKRJN IN (2) THEN 1 ELSE 0 END) R2
						, SUM(CASE WHEN A.JENIS_PKRJN IN (3) THEN 1 ELSE 0 END) R3
						, SUM(CASE WHEN A.JENIS_PKRJN IN (5) THEN 1 ELSE 0 END) R4
						, SUM(CASE WHEN A.JENIS_PKRJN IN (6,7) THEN 1 ELSE 0 END) R5
						, SUM(CASE WHEN A.JENIS_PKRJN IN (4) THEN 1 ELSE 0 END) R6
						, SUM(CASE WHEN A.JENIS_PKRJN IN (15) THEN 1 ELSE 0 END) R7
						, SUM(CASE WHEN A.JENIS_PKRJN IN (16,17) THEN 1 ELSE 0 END) R8
						, SUM(CASE WHEN A.JENIS_PKRJN IN (31,72,73,74,75,76) THEN 1 ELSE 0 END) R9
						, SUM(CASE WHEN A.JENIS_PKRJN IN (88) THEN 1 ELSE 0 END) R10 
            			, SUM(CASE WHEN A.JENIS_PKRJN IN (64,65) THEN 1 ELSE 0 END) R11
            			, SUM(CASE WHEN A.JENIS_PKRJN NOT IN (1,2,3,4,5,6,7,15,16,17,31,64,65,72,73,74,75,76,88) THEN 1 ELSE 0 END) R12
						, COUNT(1) JUMLAH
						FROM SETUP_KEC B LEFT JOIN ".$dkb_bio." A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC WHERE JENIS_KLMIN = 2 AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) AND A.NO_PROP = 32 AND A.NO_KAB =73 GROUP BY B.NO_KEC,B.NAMA_KEC ORDER BY B.NO_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL
					, CASE WHEN B.R1 IS NULL THEN 0 ELSE B.R1 END AS R1
					, CASE WHEN B.R2 IS NULL THEN 0 ELSE B.R2 END AS R2
					, CASE WHEN B.R3 IS NULL THEN 0 ELSE B.R3 END AS R3
					, CASE WHEN B.R4 IS NULL THEN 0 ELSE B.R4 END AS R4
					, CASE WHEN B.R5 IS NULL THEN 0 ELSE B.R5 END AS R5
					, CASE WHEN B.R6 IS NULL THEN 0 ELSE B.R6 END AS R6
					, CASE WHEN B.R7 IS NULL THEN 0 ELSE B.R7 END AS R7
					, CASE WHEN B.R8 IS NULL THEN 0 ELSE B.R8 END AS R8
					, CASE WHEN B.R9 IS NULL THEN 0 ELSE B.R9 END AS R9
					, CASE WHEN B.R10 IS NULL THEN 0 ELSE B.R10 END AS R10
					, CASE WHEN B.R11 IS NULL THEN 0 ELSE B.R11 END AS R11
					, CASE WHEN B.R12 IS NULL THEN 0 ELSE B.R12 END AS R12
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEL A LEFT JOIN
					(SELECT 
						B.NO_KEL 
						,B.NAMA_KEL
						, SUM(CASE WHEN A.JENIS_PKRJN IN (1) THEN 1 ELSE 0 END) R1
						, SUM(CASE WHEN A.JENIS_PKRJN IN (2) THEN 1 ELSE 0 END) R2
						, SUM(CASE WHEN A.JENIS_PKRJN IN (3) THEN 1 ELSE 0 END) R3
						, SUM(CASE WHEN A.JENIS_PKRJN IN (5) THEN 1 ELSE 0 END) R4
						, SUM(CASE WHEN A.JENIS_PKRJN IN (6,7) THEN 1 ELSE 0 END) R5
						, SUM(CASE WHEN A.JENIS_PKRJN IN (4) THEN 1 ELSE 0 END) R6
						, SUM(CASE WHEN A.JENIS_PKRJN IN (15) THEN 1 ELSE 0 END) R7
						, SUM(CASE WHEN A.JENIS_PKRJN IN (16,17) THEN 1 ELSE 0 END) R8
						, SUM(CASE WHEN A.JENIS_PKRJN IN (31,72,73,74,75,76) THEN 1 ELSE 0 END) R9
						, SUM(CASE WHEN A.JENIS_PKRJN IN (88) THEN 1 ELSE 0 END) R10 
            			, SUM(CASE WHEN A.JENIS_PKRJN IN (64,65) THEN 1 ELSE 0 END) R11
            			, SUM(CASE WHEN A.JENIS_PKRJN NOT IN (1,2,3,4,5,6,7,15,16,17,31,64,65,72,73,74,75,76,88) THEN 1 ELSE 0 END) R12
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN ".$dkb_bio." A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL WHERE JENIS_KLMIN = 2 AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) ";
						if($no_kel == 0){    
					$sql .=" AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec." and  A.NO_KEL =".$no_kel."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::select($sql);
               return $r;
		}

		
		public function mst_get_laporan_status_kawin($no_kec,$no_kel,$dkb_biodata = '',$dkb_cutoff = '',$dkb_gisa = ''){
			if (!empty($dkb_biodata)){
				$dkb_bio = $dkb_biodata;
			}else{
				$dkb_bio = Shr::do_get_dkb_bio();
			}
			if (!empty($dkb_cutoff)){
				$cut_off_dkb = $dkb_cutoff;
			}else{
				$cut_off_dkb = Shr::do_get_cut_off_date();
			}
			if (!empty($dkb_gisa)){
				$dkb_gisa = $dkb_gisa;
			}else{
				$dkb_gisa = Shr::do_get_dkb_gisa();
			}
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL
					, CASE WHEN B.R1 IS NULL THEN 0 ELSE B.R1 END AS R1
					, CASE WHEN B.R2 IS NULL THEN 0 ELSE B.R2 END AS R2
					, CASE WHEN B.R3 IS NULL THEN 0 ELSE B.R3 END AS R3
					, CASE WHEN B.R4 IS NULL THEN 0 ELSE B.R4 END AS R4
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEC A LEFT JOIN
					(SELECT 
						B.NO_KEC 
						,B.NAMA_KEC 
						, SUM(CASE WHEN A.STAT_KWN NOT BETWEEN 2 AND 4 OR STAT_KWN IS NULL THEN 1 ELSE 0 END) R1
						, SUM(CASE WHEN A.STAT_KWN = 2 THEN 1 ELSE 0 END) R2
						, SUM(CASE WHEN A.STAT_KWN = 3 THEN 1 ELSE 0 END) R3
						, SUM(CASE WHEN A.STAT_KWN = 4 THEN 1 ELSE 0 END) R4
						, COUNT(1) JUMLAH
						FROM SETUP_KEC B LEFT JOIN ".$dkb_bio." A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC WHERE  (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) AND A.NO_PROP = 32 AND A.NO_KAB =73 GROUP BY B.NO_KEC,B.NAMA_KEC ORDER BY B.NO_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL
					, CASE WHEN B.R1 IS NULL THEN 0 ELSE B.R1 END AS R1
					, CASE WHEN B.R2 IS NULL THEN 0 ELSE B.R2 END AS R2
					, CASE WHEN B.R3 IS NULL THEN 0 ELSE B.R3 END AS R3
					, CASE WHEN B.R4 IS NULL THEN 0 ELSE B.R4 END AS R4
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEL A LEFT JOIN
					(SELECT 
						B.NO_KEL 
						,B.NAMA_KEL
						, SUM(CASE WHEN A.STAT_KWN NOT BETWEEN 2 AND 4 OR STAT_KWN IS NULL THEN 1 ELSE 0 END) R1
						, SUM(CASE WHEN A.STAT_KWN = 2 THEN 1 ELSE 0 END) R2
						, SUM(CASE WHEN A.STAT_KWN = 3 THEN 1 ELSE 0 END) R3
						, SUM(CASE WHEN A.STAT_KWN = 4 THEN 1 ELSE 0 END) R4
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN ".$dkb_bio." A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL WHERE  (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) ";
						if($no_kel == 0){    
					$sql .=" AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec." and  A.NO_KEL =".$no_kel."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::select($sql);
               return $r;
		}

		
		public function mst_get_laporan_status_kawin_laki_laki($no_kec,$no_kel,$dkb_biodata = '',$dkb_cutoff = '',$dkb_gisa = ''){
			if (!empty($dkb_biodata)){
				$dkb_bio = $dkb_biodata;
			}else{
				$dkb_bio = Shr::do_get_dkb_bio();
			}
			if (!empty($dkb_cutoff)){
				$cut_off_dkb = $dkb_cutoff;
			}else{
				$cut_off_dkb = Shr::do_get_cut_off_date();
			}
			if (!empty($dkb_gisa)){
				$dkb_gisa = $dkb_gisa;
			}else{
				$dkb_gisa = Shr::do_get_dkb_gisa();
			}
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL
					, CASE WHEN B.R1 IS NULL THEN 0 ELSE B.R1 END AS R1
					, CASE WHEN B.R2 IS NULL THEN 0 ELSE B.R2 END AS R2
					, CASE WHEN B.R3 IS NULL THEN 0 ELSE B.R3 END AS R3
					, CASE WHEN B.R4 IS NULL THEN 0 ELSE B.R4 END AS R4
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEC A LEFT JOIN
					(SELECT 
						B.NO_KEC 
						,B.NAMA_KEC 
						, SUM(CASE WHEN A.STAT_KWN NOT BETWEEN 2 AND 4 OR STAT_KWN IS NULL THEN 1 ELSE 0 END) R1
						, SUM(CASE WHEN A.STAT_KWN = 2 THEN 1 ELSE 0 END) R2
						, SUM(CASE WHEN A.STAT_KWN = 3 THEN 1 ELSE 0 END) R3
						, SUM(CASE WHEN A.STAT_KWN = 4 THEN 1 ELSE 0 END) R4
						, COUNT(1) JUMLAH
						FROM SETUP_KEC B LEFT JOIN ".$dkb_bio." A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC WHERE JENIS_KLMIN = 1 AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) AND A.NO_PROP = 32 AND A.NO_KAB =73 GROUP BY B.NO_KEC,B.NAMA_KEC ORDER BY B.NO_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL
					, CASE WHEN B.R1 IS NULL THEN 0 ELSE B.R1 END AS R1
					, CASE WHEN B.R2 IS NULL THEN 0 ELSE B.R2 END AS R2
					, CASE WHEN B.R3 IS NULL THEN 0 ELSE B.R3 END AS R3
					, CASE WHEN B.R4 IS NULL THEN 0 ELSE B.R4 END AS R4
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEL A LEFT JOIN
					(SELECT 
						B.NO_KEL 
						,B.NAMA_KEL
						, SUM(CASE WHEN A.STAT_KWN NOT BETWEEN 2 AND 4 OR STAT_KWN IS NULL THEN 1 ELSE 0 END) R1
						, SUM(CASE WHEN A.STAT_KWN = 2 THEN 1 ELSE 0 END) R2
						, SUM(CASE WHEN A.STAT_KWN = 3 THEN 1 ELSE 0 END) R3
						, SUM(CASE WHEN A.STAT_KWN = 4 THEN 1 ELSE 0 END) R4
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN ".$dkb_bio." A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL WHERE JENIS_KLMIN = 1 AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) ";
						if($no_kel == 0){    
					$sql .=" AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec." and  A.NO_KEL =".$no_kel."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::select($sql);
               return $r;
		}

		
		public function mst_get_laporan_status_kawin_perempuan($no_kec,$no_kel,$dkb_biodata = '',$dkb_cutoff = '',$dkb_gisa = ''){
			if (!empty($dkb_biodata)){
				$dkb_bio = $dkb_biodata;
			}else{
				$dkb_bio = Shr::do_get_dkb_bio();
			}
			if (!empty($dkb_cutoff)){
				$cut_off_dkb = $dkb_cutoff;
			}else{
				$cut_off_dkb = Shr::do_get_cut_off_date();
			}
			if (!empty($dkb_gisa)){
				$dkb_gisa = $dkb_gisa;
			}else{
				$dkb_gisa = Shr::do_get_dkb_gisa();
			}
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL
					, CASE WHEN B.R1 IS NULL THEN 0 ELSE B.R1 END AS R1
					, CASE WHEN B.R2 IS NULL THEN 0 ELSE B.R2 END AS R2
					, CASE WHEN B.R3 IS NULL THEN 0 ELSE B.R3 END AS R3
					, CASE WHEN B.R4 IS NULL THEN 0 ELSE B.R4 END AS R4
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEC A LEFT JOIN
					(SELECT 
						B.NO_KEC 
						,B.NAMA_KEC 
						, SUM(CASE WHEN A.STAT_KWN NOT BETWEEN 2 AND 4 OR STAT_KWN IS NULL THEN 1 ELSE 0 END) R1
						, SUM(CASE WHEN A.STAT_KWN = 2 THEN 1 ELSE 0 END) R2
						, SUM(CASE WHEN A.STAT_KWN = 3 THEN 1 ELSE 0 END) R3
						, SUM(CASE WHEN A.STAT_KWN = 4 THEN 1 ELSE 0 END) R4
						, COUNT(1) JUMLAH
						FROM SETUP_KEC B LEFT JOIN ".$dkb_bio." A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC WHERE JENIS_KLMIN = 2 AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) AND A.NO_PROP = 32 AND A.NO_KAB =73 GROUP BY B.NO_KEC,B.NAMA_KEC ORDER BY B.NO_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL
					, CASE WHEN B.R1 IS NULL THEN 0 ELSE B.R1 END AS R1
					, CASE WHEN B.R2 IS NULL THEN 0 ELSE B.R2 END AS R2
					, CASE WHEN B.R3 IS NULL THEN 0 ELSE B.R3 END AS R3
					, CASE WHEN B.R4 IS NULL THEN 0 ELSE B.R4 END AS R4
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEL A LEFT JOIN
					(SELECT 
						B.NO_KEL 
						,B.NAMA_KEL
						, SUM(CASE WHEN A.STAT_KWN NOT BETWEEN 2 AND 4 OR STAT_KWN IS NULL THEN 1 ELSE 0 END) R1
						, SUM(CASE WHEN A.STAT_KWN = 2 THEN 1 ELSE 0 END) R2
						, SUM(CASE WHEN A.STAT_KWN = 3 THEN 1 ELSE 0 END) R3
						, SUM(CASE WHEN A.STAT_KWN = 4 THEN 1 ELSE 0 END) R4
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN ".$dkb_bio." A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL WHERE JENIS_KLMIN = 2 AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) ";
						if($no_kel == 0){    
					$sql .=" AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec." and  A.NO_KEL =".$no_kel."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::select($sql);
               return $r;
		}

		
		public function mst_get_laporan_agama($no_kec,$no_kel,$dkb_biodata = '',$dkb_cutoff = '',$dkb_gisa = ''){
			if (!empty($dkb_biodata)){
				$dkb_bio = $dkb_biodata;
			}else{
				$dkb_bio = Shr::do_get_dkb_bio();
			}
			if (!empty($dkb_cutoff)){
				$cut_off_dkb = $dkb_cutoff;
			}else{
				$cut_off_dkb = Shr::do_get_cut_off_date();
			}
			if (!empty($dkb_gisa)){
				$dkb_gisa = $dkb_gisa;
			}else{
				$dkb_gisa = Shr::do_get_dkb_gisa();
			}
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL
					, CASE WHEN B.R1 IS NULL THEN 0 ELSE B.R1 END AS R1
					, CASE WHEN B.R2 IS NULL THEN 0 ELSE B.R2 END AS R2
					, CASE WHEN B.R3 IS NULL THEN 0 ELSE B.R3 END AS R3
					, CASE WHEN B.R4 IS NULL THEN 0 ELSE B.R4 END AS R4
					, CASE WHEN B.R5 IS NULL THEN 0 ELSE B.R5 END AS R5
					, CASE WHEN B.R6 IS NULL THEN 0 ELSE B.R6 END AS R6
					, CASE WHEN B.R7 IS NULL THEN 0 ELSE B.R7 END AS R7
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEC A LEFT JOIN
					(SELECT 
						B.NO_KEC 
						,B.NAMA_KEC 
						, SUM(CASE WHEN A.AGAMA NOT BETWEEN 2 AND 7 OR AGAMA IS NULL THEN 1 ELSE 0 END) R1
						, SUM(CASE WHEN A.AGAMA = 2 THEN 1 ELSE 0 END) R2
						, SUM(CASE WHEN A.AGAMA = 3 THEN 1 ELSE 0 END) R3
						, SUM(CASE WHEN A.AGAMA = 4 THEN 1 ELSE 0 END) R4
						, SUM(CASE WHEN A.AGAMA = 5 THEN 1 ELSE 0 END) R5
						, SUM(CASE WHEN A.AGAMA = 6 THEN 1 ELSE 0 END) R6
						, SUM(CASE WHEN A.AGAMA = 7 THEN 1 ELSE 0 END) R7
						, COUNT(1) JUMLAH
						FROM SETUP_KEC B LEFT JOIN ".$dkb_bio." A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC WHERE  (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) AND A.NO_PROP = 32 AND A.NO_KAB =73 GROUP BY B.NO_KEC,B.NAMA_KEC ORDER BY B.NO_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL
					, CASE WHEN B.R1 IS NULL THEN 0 ELSE B.R1 END AS R1
					, CASE WHEN B.R2 IS NULL THEN 0 ELSE B.R2 END AS R2
					, CASE WHEN B.R3 IS NULL THEN 0 ELSE B.R3 END AS R3
					, CASE WHEN B.R4 IS NULL THEN 0 ELSE B.R4 END AS R4
					, CASE WHEN B.R5 IS NULL THEN 0 ELSE B.R5 END AS R5
					, CASE WHEN B.R6 IS NULL THEN 0 ELSE B.R6 END AS R6
					, CASE WHEN B.R7 IS NULL THEN 0 ELSE B.R7 END AS R7
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEL A LEFT JOIN
					(SELECT 
						B.NO_KEL 
						,B.NAMA_KEL
						, SUM(CASE WHEN A.AGAMA NOT BETWEEN 2 AND 7 OR AGAMA IS NULL THEN 1 ELSE 0 END) R1
						, SUM(CASE WHEN A.AGAMA = 2 THEN 1 ELSE 0 END) R2
						, SUM(CASE WHEN A.AGAMA = 3 THEN 1 ELSE 0 END) R3
						, SUM(CASE WHEN A.AGAMA = 4 THEN 1 ELSE 0 END) R4
						, SUM(CASE WHEN A.AGAMA = 5 THEN 1 ELSE 0 END) R5
						, SUM(CASE WHEN A.AGAMA = 6 THEN 1 ELSE 0 END) R6
						, SUM(CASE WHEN A.AGAMA = 7 THEN 1 ELSE 0 END) R7
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN ".$dkb_bio." A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL WHERE  (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) ";
						if($no_kel == 0){    
					$sql .=" AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec." and  A.NO_KEL =".$no_kel."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::select($sql);
               return $r;
		}

		
		public function mst_get_laporan_agama_laki_laki($no_kec,$no_kel,$dkb_biodata = '',$dkb_cutoff = '',$dkb_gisa = ''){
			if (!empty($dkb_biodata)){
				$dkb_bio = $dkb_biodata;
			}else{
				$dkb_bio = Shr::do_get_dkb_bio();
			}
			if (!empty($dkb_cutoff)){
				$cut_off_dkb = $dkb_cutoff;
			}else{
				$cut_off_dkb = Shr::do_get_cut_off_date();
			}
			if (!empty($dkb_gisa)){
				$dkb_gisa = $dkb_gisa;
			}else{
				$dkb_gisa = Shr::do_get_dkb_gisa();
			}
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL
					, CASE WHEN B.R1 IS NULL THEN 0 ELSE B.R1 END AS R1
					, CASE WHEN B.R2 IS NULL THEN 0 ELSE B.R2 END AS R2
					, CASE WHEN B.R3 IS NULL THEN 0 ELSE B.R3 END AS R3
					, CASE WHEN B.R4 IS NULL THEN 0 ELSE B.R4 END AS R4
					, CASE WHEN B.R5 IS NULL THEN 0 ELSE B.R5 END AS R5
					, CASE WHEN B.R6 IS NULL THEN 0 ELSE B.R6 END AS R6
					, CASE WHEN B.R7 IS NULL THEN 0 ELSE B.R7 END AS R7
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEC A LEFT JOIN
					(SELECT 
						B.NO_KEC 
						,B.NAMA_KEC 
						, SUM(CASE WHEN A.AGAMA NOT BETWEEN 2 AND 7 OR AGAMA IS NULL THEN 1 ELSE 0 END) R1
						, SUM(CASE WHEN A.AGAMA = 2 THEN 1 ELSE 0 END) R2
						, SUM(CASE WHEN A.AGAMA = 3 THEN 1 ELSE 0 END) R3
						, SUM(CASE WHEN A.AGAMA = 4 THEN 1 ELSE 0 END) R4
						, SUM(CASE WHEN A.AGAMA = 5 THEN 1 ELSE 0 END) R5
						, SUM(CASE WHEN A.AGAMA = 6 THEN 1 ELSE 0 END) R6
						, SUM(CASE WHEN A.AGAMA = 7 THEN 1 ELSE 0 END) R7
						, COUNT(1) JUMLAH
						FROM SETUP_KEC B LEFT JOIN ".$dkb_bio." A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC WHERE JENIS_KLMIN = 1 AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) AND A.NO_PROP = 32 AND A.NO_KAB =73 GROUP BY B.NO_KEC,B.NAMA_KEC ORDER BY B.NO_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL
					, CASE WHEN B.R1 IS NULL THEN 0 ELSE B.R1 END AS R1
					, CASE WHEN B.R2 IS NULL THEN 0 ELSE B.R2 END AS R2
					, CASE WHEN B.R3 IS NULL THEN 0 ELSE B.R3 END AS R3
					, CASE WHEN B.R4 IS NULL THEN 0 ELSE B.R4 END AS R4
					, CASE WHEN B.R5 IS NULL THEN 0 ELSE B.R5 END AS R5
					, CASE WHEN B.R6 IS NULL THEN 0 ELSE B.R6 END AS R6
					, CASE WHEN B.R7 IS NULL THEN 0 ELSE B.R7 END AS R7
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEL A LEFT JOIN
					(SELECT 
						B.NO_KEL 
						,B.NAMA_KEL
						, SUM(CASE WHEN A.AGAMA NOT BETWEEN 2 AND 7 OR AGAMA IS NULL THEN 1 ELSE 0 END) R1
						, SUM(CASE WHEN A.AGAMA = 2 THEN 1 ELSE 0 END) R2
						, SUM(CASE WHEN A.AGAMA = 3 THEN 1 ELSE 0 END) R3
						, SUM(CASE WHEN A.AGAMA = 4 THEN 1 ELSE 0 END) R4
						, SUM(CASE WHEN A.AGAMA = 5 THEN 1 ELSE 0 END) R5
						, SUM(CASE WHEN A.AGAMA = 6 THEN 1 ELSE 0 END) R6
						, SUM(CASE WHEN A.AGAMA = 7 THEN 1 ELSE 0 END) R7
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN ".$dkb_bio." A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL WHERE JENIS_KLMIN = 1 AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) ";
						if($no_kel == 0){    
					$sql .=" AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec." and  A.NO_KEL =".$no_kel."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::select($sql);
               return $r;
		}

		
		public function mst_get_laporan_agama_perempuan($no_kec,$no_kel,$dkb_biodata = '',$dkb_cutoff = '',$dkb_gisa = ''){
			if (!empty($dkb_biodata)){
				$dkb_bio = $dkb_biodata;
			}else{
				$dkb_bio = Shr::do_get_dkb_bio();
			}
			if (!empty($dkb_cutoff)){
				$cut_off_dkb = $dkb_cutoff;
			}else{
				$cut_off_dkb = Shr::do_get_cut_off_date();
			}
			if (!empty($dkb_gisa)){
				$dkb_gisa = $dkb_gisa;
			}else{
				$dkb_gisa = Shr::do_get_dkb_gisa();
			}
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL
					, CASE WHEN B.R1 IS NULL THEN 0 ELSE B.R1 END AS R1
					, CASE WHEN B.R2 IS NULL THEN 0 ELSE B.R2 END AS R2
					, CASE WHEN B.R3 IS NULL THEN 0 ELSE B.R3 END AS R3
					, CASE WHEN B.R4 IS NULL THEN 0 ELSE B.R4 END AS R4
					, CASE WHEN B.R5 IS NULL THEN 0 ELSE B.R5 END AS R5
					, CASE WHEN B.R6 IS NULL THEN 0 ELSE B.R6 END AS R6
					, CASE WHEN B.R7 IS NULL THEN 0 ELSE B.R7 END AS R7
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEC A LEFT JOIN
					(SELECT 
						B.NO_KEC 
						,B.NAMA_KEC 
						, SUM(CASE WHEN A.AGAMA NOT BETWEEN 2 AND 7 OR AGAMA IS NULL THEN 1 ELSE 0 END) R1
						, SUM(CASE WHEN A.AGAMA = 2 THEN 1 ELSE 0 END) R2
						, SUM(CASE WHEN A.AGAMA = 3 THEN 1 ELSE 0 END) R3
						, SUM(CASE WHEN A.AGAMA = 4 THEN 1 ELSE 0 END) R4
						, SUM(CASE WHEN A.AGAMA = 5 THEN 1 ELSE 0 END) R5
						, SUM(CASE WHEN A.AGAMA = 6 THEN 1 ELSE 0 END) R6
						, SUM(CASE WHEN A.AGAMA = 7 THEN 1 ELSE 0 END) R7
						, COUNT(1) JUMLAH
						FROM SETUP_KEC B LEFT JOIN ".$dkb_bio." A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC WHERE JENIS_KLMIN = 2 AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) AND A.NO_PROP = 32 AND A.NO_KAB =73 GROUP BY B.NO_KEC,B.NAMA_KEC ORDER BY B.NO_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL
					, CASE WHEN B.R1 IS NULL THEN 0 ELSE B.R1 END AS R1
					, CASE WHEN B.R2 IS NULL THEN 0 ELSE B.R2 END AS R2
					, CASE WHEN B.R3 IS NULL THEN 0 ELSE B.R3 END AS R3
					, CASE WHEN B.R4 IS NULL THEN 0 ELSE B.R4 END AS R4
					, CASE WHEN B.R5 IS NULL THEN 0 ELSE B.R5 END AS R5
					, CASE WHEN B.R6 IS NULL THEN 0 ELSE B.R6 END AS R6
					, CASE WHEN B.R7 IS NULL THEN 0 ELSE B.R7 END AS R7
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEL A LEFT JOIN
					(SELECT 
						B.NO_KEL 
						,B.NAMA_KEL
						, SUM(CASE WHEN A.AGAMA NOT BETWEEN 2 AND 7 OR AGAMA IS NULL THEN 1 ELSE 0 END) R1
						, SUM(CASE WHEN A.AGAMA = 2 THEN 1 ELSE 0 END) R2
						, SUM(CASE WHEN A.AGAMA = 3 THEN 1 ELSE 0 END) R3
						, SUM(CASE WHEN A.AGAMA = 4 THEN 1 ELSE 0 END) R4
						, SUM(CASE WHEN A.AGAMA = 5 THEN 1 ELSE 0 END) R5
						, SUM(CASE WHEN A.AGAMA = 6 THEN 1 ELSE 0 END) R6
						, SUM(CASE WHEN A.AGAMA = 7 THEN 1 ELSE 0 END) R7
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN ".$dkb_bio." A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL WHERE JENIS_KLMIN = 2 AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) ";
						if($no_kel == 0){    
					$sql .=" AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec." and  A.NO_KEL =".$no_kel."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::select($sql);
               return $r;
		}

		
		public function mst_get_laporan_gol_drh($no_kec,$no_kel,$dkb_biodata = '',$dkb_cutoff = '',$dkb_gisa = ''){
			if (!empty($dkb_biodata)){
				$dkb_bio = $dkb_biodata;
			}else{
				$dkb_bio = Shr::do_get_dkb_bio();
			}
			if (!empty($dkb_cutoff)){
				$cut_off_dkb = $dkb_cutoff;
			}else{
				$cut_off_dkb = Shr::do_get_cut_off_date();
			}
			if (!empty($dkb_gisa)){
				$dkb_gisa = $dkb_gisa;
			}else{
				$dkb_gisa = Shr::do_get_dkb_gisa();
			}
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL
					, CASE WHEN B.R1 IS NULL THEN 0 ELSE B.R1 END AS R1
					, CASE WHEN B.R2 IS NULL THEN 0 ELSE B.R2 END AS R2
					, CASE WHEN B.R3 IS NULL THEN 0 ELSE B.R3 END AS R3
					, CASE WHEN B.R4 IS NULL THEN 0 ELSE B.R4 END AS R4
					, CASE WHEN B.R5 IS NULL THEN 0 ELSE B.R5 END AS R5
					, CASE WHEN B.R6 IS NULL THEN 0 ELSE B.R6 END AS R6
					, CASE WHEN B.R7 IS NULL THEN 0 ELSE B.R7 END AS R7
					, CASE WHEN B.R8 IS NULL THEN 0 ELSE B.R8 END AS R8
					, CASE WHEN B.R9 IS NULL THEN 0 ELSE B.R9 END AS R9
					, CASE WHEN B.R10 IS NULL THEN 0 ELSE B.R10 END AS R10
					, CASE WHEN B.R11 IS NULL THEN 0 ELSE B.R11 END AS R11
					, CASE WHEN B.R12 IS NULL THEN 0 ELSE B.R12 END AS R12
					, CASE WHEN B.R13 IS NULL THEN 0 ELSE B.R13 END AS R13
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEC A LEFT JOIN
					(SELECT 
						B.NO_KEC 
						,B.NAMA_KEC 
						, SUM(CASE WHEN A.GOL_DRH  = 1 THEN 1 ELSE 0 END) R1
						, SUM(CASE WHEN A.GOL_DRH = 2 THEN 1 ELSE 0 END) R2
						, SUM(CASE WHEN A.GOL_DRH = 3 THEN 1 ELSE 0 END) R3
						, SUM(CASE WHEN A.GOL_DRH = 4 THEN 1 ELSE 0 END) R4
						, SUM(CASE WHEN A.GOL_DRH = 5 THEN 1 ELSE 0 END) R5
						, SUM(CASE WHEN A.GOL_DRH = 6 THEN 1 ELSE 0 END) R6
						, SUM(CASE WHEN A.GOL_DRH = 7 THEN 1 ELSE 0 END) R7
						, SUM(CASE WHEN A.GOL_DRH = 8 THEN 1 ELSE 0 END) R8
						, SUM(CASE WHEN A.GOL_DRH = 9 THEN 1 ELSE 0 END) R9
						, SUM(CASE WHEN A.GOL_DRH = 10 THEN 1 ELSE 0 END) R10
						, SUM(CASE WHEN A.GOL_DRH = 11 THEN 1 ELSE 0 END) R11
						, SUM(CASE WHEN A.GOL_DRH = 12 THEN 1 ELSE 0 END) R12
						, SUM(CASE WHEN A.GOL_DRH NOT BETWEEN 1 AND 12 OR GOL_DRH IS NULL THEN 1 ELSE 0 END) R13
						, COUNT(1) JUMLAH
						FROM SETUP_KEC B LEFT JOIN ".$dkb_bio." A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC WHERE  (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) AND A.NO_PROP = 32 AND A.NO_KAB =73 GROUP BY B.NO_KEC,B.NAMA_KEC ORDER BY B.NO_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL
					, CASE WHEN B.R1 IS NULL THEN 0 ELSE B.R1 END AS R1
					, CASE WHEN B.R2 IS NULL THEN 0 ELSE B.R2 END AS R2
					, CASE WHEN B.R3 IS NULL THEN 0 ELSE B.R3 END AS R3
					, CASE WHEN B.R4 IS NULL THEN 0 ELSE B.R4 END AS R4
					, CASE WHEN B.R5 IS NULL THEN 0 ELSE B.R5 END AS R5
					, CASE WHEN B.R6 IS NULL THEN 0 ELSE B.R6 END AS R6
					, CASE WHEN B.R7 IS NULL THEN 0 ELSE B.R7 END AS R7
					, CASE WHEN B.R8 IS NULL THEN 0 ELSE B.R8 END AS R8
					, CASE WHEN B.R9 IS NULL THEN 0 ELSE B.R9 END AS R9
					, CASE WHEN B.R10 IS NULL THEN 0 ELSE B.R10 END AS R10
					, CASE WHEN B.R11 IS NULL THEN 0 ELSE B.R11 END AS R11
					, CASE WHEN B.R12 IS NULL THEN 0 ELSE B.R12 END AS R12
					, CASE WHEN B.R13 IS NULL THEN 0 ELSE B.R13 END AS R13
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEL A LEFT JOIN
					(SELECT 
						B.NO_KEL 
						,B.NAMA_KEL
						, SUM(CASE WHEN A.GOL_DRH  = 1 THEN 1 ELSE 0 END) R1
						, SUM(CASE WHEN A.GOL_DRH = 2 THEN 1 ELSE 0 END) R2
						, SUM(CASE WHEN A.GOL_DRH = 3 THEN 1 ELSE 0 END) R3
						, SUM(CASE WHEN A.GOL_DRH = 4 THEN 1 ELSE 0 END) R4
						, SUM(CASE WHEN A.GOL_DRH = 5 THEN 1 ELSE 0 END) R5
						, SUM(CASE WHEN A.GOL_DRH = 6 THEN 1 ELSE 0 END) R6
						, SUM(CASE WHEN A.GOL_DRH = 7 THEN 1 ELSE 0 END) R7
						, SUM(CASE WHEN A.GOL_DRH = 8 THEN 1 ELSE 0 END) R8
						, SUM(CASE WHEN A.GOL_DRH = 9 THEN 1 ELSE 0 END) R9
						, SUM(CASE WHEN A.GOL_DRH = 10 THEN 1 ELSE 0 END) R10
						, SUM(CASE WHEN A.GOL_DRH = 11 THEN 1 ELSE 0 END) R11
						, SUM(CASE WHEN A.GOL_DRH = 12 THEN 1 ELSE 0 END) R12
						, SUM(CASE WHEN A.GOL_DRH NOT BETWEEN 1 AND 12 OR GOL_DRH IS NULL THEN 1 ELSE 0 END) R13
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN ".$dkb_bio." A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL WHERE  (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) ";
						if($no_kel == 0){    
					$sql .=" AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec." and  A.NO_KEL =".$no_kel."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::select($sql);
               return $r;
		}

		
		public function mst_get_laporan_gol_drh_laki_laki($no_kec,$no_kel,$dkb_biodata = '',$dkb_cutoff = '',$dkb_gisa = ''){
			if (!empty($dkb_biodata)){
				$dkb_bio = $dkb_biodata;
			}else{
				$dkb_bio = Shr::do_get_dkb_bio();
			}
			if (!empty($dkb_cutoff)){
				$cut_off_dkb = $dkb_cutoff;
			}else{
				$cut_off_dkb = Shr::do_get_cut_off_date();
			}
			if (!empty($dkb_gisa)){
				$dkb_gisa = $dkb_gisa;
			}else{
				$dkb_gisa = Shr::do_get_dkb_gisa();
			}
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL
					, CASE WHEN B.R1 IS NULL THEN 0 ELSE B.R1 END AS R1
					, CASE WHEN B.R2 IS NULL THEN 0 ELSE B.R2 END AS R2
					, CASE WHEN B.R3 IS NULL THEN 0 ELSE B.R3 END AS R3
					, CASE WHEN B.R4 IS NULL THEN 0 ELSE B.R4 END AS R4
					, CASE WHEN B.R5 IS NULL THEN 0 ELSE B.R5 END AS R5
					, CASE WHEN B.R6 IS NULL THEN 0 ELSE B.R6 END AS R6
					, CASE WHEN B.R7 IS NULL THEN 0 ELSE B.R7 END AS R7
					, CASE WHEN B.R8 IS NULL THEN 0 ELSE B.R8 END AS R8
					, CASE WHEN B.R9 IS NULL THEN 0 ELSE B.R9 END AS R9
					, CASE WHEN B.R10 IS NULL THEN 0 ELSE B.R10 END AS R10
					, CASE WHEN B.R11 IS NULL THEN 0 ELSE B.R11 END AS R11
					, CASE WHEN B.R12 IS NULL THEN 0 ELSE B.R12 END AS R12
					, CASE WHEN B.R13 IS NULL THEN 0 ELSE B.R13 END AS R13
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEC A LEFT JOIN
					(SELECT 
						B.NO_KEC 
						,B.NAMA_KEC 
						, SUM(CASE WHEN A.GOL_DRH  = 1 THEN 1 ELSE 0 END) R1
						, SUM(CASE WHEN A.GOL_DRH = 2 THEN 1 ELSE 0 END) R2
						, SUM(CASE WHEN A.GOL_DRH = 3 THEN 1 ELSE 0 END) R3
						, SUM(CASE WHEN A.GOL_DRH = 4 THEN 1 ELSE 0 END) R4
						, SUM(CASE WHEN A.GOL_DRH = 5 THEN 1 ELSE 0 END) R5
						, SUM(CASE WHEN A.GOL_DRH = 6 THEN 1 ELSE 0 END) R6
						, SUM(CASE WHEN A.GOL_DRH = 7 THEN 1 ELSE 0 END) R7
						, SUM(CASE WHEN A.GOL_DRH = 8 THEN 1 ELSE 0 END) R8
						, SUM(CASE WHEN A.GOL_DRH = 9 THEN 1 ELSE 0 END) R9
						, SUM(CASE WHEN A.GOL_DRH = 10 THEN 1 ELSE 0 END) R10
						, SUM(CASE WHEN A.GOL_DRH = 11 THEN 1 ELSE 0 END) R11
						, SUM(CASE WHEN A.GOL_DRH = 12 THEN 1 ELSE 0 END) R12
						, SUM(CASE WHEN A.GOL_DRH NOT BETWEEN 1 AND 12 OR GOL_DRH IS NULL THEN 1 ELSE 0 END) R13
						, COUNT(1) JUMLAH
						FROM SETUP_KEC B LEFT JOIN ".$dkb_bio." A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC WHERE JENIS_KLMIN = 1 AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) AND A.NO_PROP = 32 AND A.NO_KAB =73 GROUP BY B.NO_KEC,B.NAMA_KEC ORDER BY B.NO_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL
					, CASE WHEN B.R1 IS NULL THEN 0 ELSE B.R1 END AS R1
					, CASE WHEN B.R2 IS NULL THEN 0 ELSE B.R2 END AS R2
					, CASE WHEN B.R3 IS NULL THEN 0 ELSE B.R3 END AS R3
					, CASE WHEN B.R4 IS NULL THEN 0 ELSE B.R4 END AS R4
					, CASE WHEN B.R5 IS NULL THEN 0 ELSE B.R5 END AS R5
					, CASE WHEN B.R6 IS NULL THEN 0 ELSE B.R6 END AS R6
					, CASE WHEN B.R7 IS NULL THEN 0 ELSE B.R7 END AS R7
					, CASE WHEN B.R8 IS NULL THEN 0 ELSE B.R8 END AS R8
					, CASE WHEN B.R9 IS NULL THEN 0 ELSE B.R9 END AS R9
					, CASE WHEN B.R10 IS NULL THEN 0 ELSE B.R10 END AS R10
					, CASE WHEN B.R11 IS NULL THEN 0 ELSE B.R11 END AS R11
					, CASE WHEN B.R12 IS NULL THEN 0 ELSE B.R12 END AS R12
					, CASE WHEN B.R13 IS NULL THEN 0 ELSE B.R13 END AS R13
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEL A LEFT JOIN
					(SELECT 
						B.NO_KEL 
						,B.NAMA_KEL
						, SUM(CASE WHEN A.GOL_DRH  = 1 THEN 1 ELSE 0 END) R1
						, SUM(CASE WHEN A.GOL_DRH = 2 THEN 1 ELSE 0 END) R2
						, SUM(CASE WHEN A.GOL_DRH = 3 THEN 1 ELSE 0 END) R3
						, SUM(CASE WHEN A.GOL_DRH = 4 THEN 1 ELSE 0 END) R4
						, SUM(CASE WHEN A.GOL_DRH = 5 THEN 1 ELSE 0 END) R5
						, SUM(CASE WHEN A.GOL_DRH = 6 THEN 1 ELSE 0 END) R6
						, SUM(CASE WHEN A.GOL_DRH = 7 THEN 1 ELSE 0 END) R7
						, SUM(CASE WHEN A.GOL_DRH = 8 THEN 1 ELSE 0 END) R8
						, SUM(CASE WHEN A.GOL_DRH = 9 THEN 1 ELSE 0 END) R9
						, SUM(CASE WHEN A.GOL_DRH = 10 THEN 1 ELSE 0 END) R10
						, SUM(CASE WHEN A.GOL_DRH = 11 THEN 1 ELSE 0 END) R11
						, SUM(CASE WHEN A.GOL_DRH = 12 THEN 1 ELSE 0 END) R12
						, SUM(CASE WHEN A.GOL_DRH NOT BETWEEN 1 AND 12 OR GOL_DRH IS NULL THEN 1 ELSE 0 END) R13
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN ".$dkb_bio." A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL WHERE JENIS_KLMIN = 1 AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) ";
						if($no_kel == 0){    
					$sql .=" AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec." and  A.NO_KEL =".$no_kel."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::select($sql);
               return $r;
		}

		
		public function mst_get_laporan_gol_drh_perempuan($no_kec,$no_kel,$dkb_biodata = '',$dkb_cutoff = '',$dkb_gisa = ''){
			if (!empty($dkb_biodata)){
				$dkb_bio = $dkb_biodata;
			}else{
				$dkb_bio = Shr::do_get_dkb_bio();
			}
			if (!empty($dkb_cutoff)){
				$cut_off_dkb = $dkb_cutoff;
			}else{
				$cut_off_dkb = Shr::do_get_cut_off_date();
			}
			if (!empty($dkb_gisa)){
				$dkb_gisa = $dkb_gisa;
			}else{
				$dkb_gisa = Shr::do_get_dkb_gisa();
			}
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL
					, CASE WHEN B.R1 IS NULL THEN 0 ELSE B.R1 END AS R1
					, CASE WHEN B.R2 IS NULL THEN 0 ELSE B.R2 END AS R2
					, CASE WHEN B.R3 IS NULL THEN 0 ELSE B.R3 END AS R3
					, CASE WHEN B.R4 IS NULL THEN 0 ELSE B.R4 END AS R4
					, CASE WHEN B.R5 IS NULL THEN 0 ELSE B.R5 END AS R5
					, CASE WHEN B.R6 IS NULL THEN 0 ELSE B.R6 END AS R6
					, CASE WHEN B.R7 IS NULL THEN 0 ELSE B.R7 END AS R7
					, CASE WHEN B.R8 IS NULL THEN 0 ELSE B.R8 END AS R8
					, CASE WHEN B.R9 IS NULL THEN 0 ELSE B.R9 END AS R9
					, CASE WHEN B.R10 IS NULL THEN 0 ELSE B.R10 END AS R10
					, CASE WHEN B.R11 IS NULL THEN 0 ELSE B.R11 END AS R11
					, CASE WHEN B.R12 IS NULL THEN 0 ELSE B.R12 END AS R12
					, CASE WHEN B.R13 IS NULL THEN 0 ELSE B.R13 END AS R13
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEC A LEFT JOIN
					(SELECT 
						B.NO_KEC 
						,B.NAMA_KEC 
						, SUM(CASE WHEN A.GOL_DRH  = 1 THEN 1 ELSE 0 END) R1
						, SUM(CASE WHEN A.GOL_DRH = 2 THEN 1 ELSE 0 END) R2
						, SUM(CASE WHEN A.GOL_DRH = 3 THEN 1 ELSE 0 END) R3
						, SUM(CASE WHEN A.GOL_DRH = 4 THEN 1 ELSE 0 END) R4
						, SUM(CASE WHEN A.GOL_DRH = 5 THEN 1 ELSE 0 END) R5
						, SUM(CASE WHEN A.GOL_DRH = 6 THEN 1 ELSE 0 END) R6
						, SUM(CASE WHEN A.GOL_DRH = 7 THEN 1 ELSE 0 END) R7
						, SUM(CASE WHEN A.GOL_DRH = 8 THEN 1 ELSE 0 END) R8
						, SUM(CASE WHEN A.GOL_DRH = 9 THEN 1 ELSE 0 END) R9
						, SUM(CASE WHEN A.GOL_DRH = 10 THEN 1 ELSE 0 END) R10
						, SUM(CASE WHEN A.GOL_DRH = 11 THEN 1 ELSE 0 END) R11
						, SUM(CASE WHEN A.GOL_DRH = 12 THEN 1 ELSE 0 END) R12
						, SUM(CASE WHEN A.GOL_DRH NOT BETWEEN 1 AND 12 OR GOL_DRH IS NULL THEN 1 ELSE 0 END) R13
						, COUNT(1) JUMLAH
						FROM SETUP_KEC B LEFT JOIN ".$dkb_bio." A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC WHERE JENIS_KLMIN = 2 AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) AND A.NO_PROP = 32 AND A.NO_KAB =73 GROUP BY B.NO_KEC,B.NAMA_KEC ORDER BY B.NO_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL
					, CASE WHEN B.R1 IS NULL THEN 0 ELSE B.R1 END AS R1
					, CASE WHEN B.R2 IS NULL THEN 0 ELSE B.R2 END AS R2
					, CASE WHEN B.R3 IS NULL THEN 0 ELSE B.R3 END AS R3
					, CASE WHEN B.R4 IS NULL THEN 0 ELSE B.R4 END AS R4
					, CASE WHEN B.R5 IS NULL THEN 0 ELSE B.R5 END AS R5
					, CASE WHEN B.R6 IS NULL THEN 0 ELSE B.R6 END AS R6
					, CASE WHEN B.R7 IS NULL THEN 0 ELSE B.R7 END AS R7
					, CASE WHEN B.R8 IS NULL THEN 0 ELSE B.R8 END AS R8
					, CASE WHEN B.R9 IS NULL THEN 0 ELSE B.R9 END AS R9
					, CASE WHEN B.R10 IS NULL THEN 0 ELSE B.R10 END AS R10
					, CASE WHEN B.R11 IS NULL THEN 0 ELSE B.R11 END AS R11
					, CASE WHEN B.R12 IS NULL THEN 0 ELSE B.R12 END AS R12
					, CASE WHEN B.R13 IS NULL THEN 0 ELSE B.R13 END AS R13
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEL A LEFT JOIN
					(SELECT 
						B.NO_KEL 
						,B.NAMA_KEL
						, SUM(CASE WHEN A.GOL_DRH  = 1 THEN 1 ELSE 0 END) R1
						, SUM(CASE WHEN A.GOL_DRH = 2 THEN 1 ELSE 0 END) R2
						, SUM(CASE WHEN A.GOL_DRH = 3 THEN 1 ELSE 0 END) R3
						, SUM(CASE WHEN A.GOL_DRH = 4 THEN 1 ELSE 0 END) R4
						, SUM(CASE WHEN A.GOL_DRH = 5 THEN 1 ELSE 0 END) R5
						, SUM(CASE WHEN A.GOL_DRH = 6 THEN 1 ELSE 0 END) R6
						, SUM(CASE WHEN A.GOL_DRH = 7 THEN 1 ELSE 0 END) R7
						, SUM(CASE WHEN A.GOL_DRH = 8 THEN 1 ELSE 0 END) R8
						, SUM(CASE WHEN A.GOL_DRH = 9 THEN 1 ELSE 0 END) R9
						, SUM(CASE WHEN A.GOL_DRH = 10 THEN 1 ELSE 0 END) R10
						, SUM(CASE WHEN A.GOL_DRH = 11 THEN 1 ELSE 0 END) R11
						, SUM(CASE WHEN A.GOL_DRH = 12 THEN 1 ELSE 0 END) R12
						, SUM(CASE WHEN A.GOL_DRH NOT BETWEEN 1 AND 12 OR GOL_DRH IS NULL THEN 1 ELSE 0 END) R13
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN ".$dkb_bio." A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL WHERE JENIS_KLMIN = 2 AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) ";
						if($no_kel == 0){    
					$sql .=" AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec." and  A.NO_KEL =".$no_kel."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::select($sql);
               return $r;
		}

		
		public function mst_get_laporan_disabilitas($no_kec,$no_kel,$dkb_biodata = '',$dkb_cutoff = '',$dkb_gisa = ''){
			if (!empty($dkb_biodata)){
				$dkb_bio = $dkb_biodata;
			}else{
				$dkb_bio = Shr::do_get_dkb_bio();
			}
			if (!empty($dkb_cutoff)){
				$cut_off_dkb = $dkb_cutoff;
			}else{
				$cut_off_dkb = Shr::do_get_cut_off_date();
			}
			if (!empty($dkb_gisa)){
				$dkb_gisa = $dkb_gisa;
			}else{
				$dkb_gisa = Shr::do_get_dkb_gisa();
			}
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL
					, CASE WHEN B.R1 IS NULL THEN 0 ELSE B.R1 END AS R1
					, CASE WHEN B.R2 IS NULL THEN 0 ELSE B.R2 END AS R2
					, CASE WHEN B.R3 IS NULL THEN 0 ELSE B.R3 END AS R3
					, CASE WHEN B.R4 IS NULL THEN 0 ELSE B.R4 END AS R4
					, CASE WHEN B.R5 IS NULL THEN 0 ELSE B.R5 END AS R5
					, CASE WHEN B.R6 IS NULL THEN 0 ELSE B.R6 END AS R6
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEC A LEFT JOIN
					(SELECT 
						B.NO_KEC 
						,B.NAMA_KEC 
						, SUM(CASE WHEN A.PNYDNG_CCT  = 1 THEN 1 ELSE 0 END) R1
						, SUM(CASE WHEN A.PNYDNG_CCT = 2 THEN 1 ELSE 0 END) R2
						, SUM(CASE WHEN A.PNYDNG_CCT = 3 THEN 1 ELSE 0 END) R3
						, SUM(CASE WHEN A.PNYDNG_CCT = 4 THEN 1 ELSE 0 END) R4
						, SUM(CASE WHEN A.PNYDNG_CCT = 5 THEN 1 ELSE 0 END) R5
						, SUM(CASE WHEN A.PNYDNG_CCT NOT BETWEEN 1 AND 5 OR PNYDNG_CCT IS NULL THEN 1 ELSE 0 END) R6
						, COUNT(1) JUMLAH
						FROM SETUP_KEC B LEFT JOIN ".$dkb_bio." A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC WHERE  KLAIN_FSK = 2 AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) AND A.NO_PROP = 32 AND A.NO_KAB =73 GROUP BY B.NO_KEC,B.NAMA_KEC ORDER BY B.NO_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL
					, CASE WHEN B.R1 IS NULL THEN 0 ELSE B.R1 END AS R1
					, CASE WHEN B.R2 IS NULL THEN 0 ELSE B.R2 END AS R2
					, CASE WHEN B.R3 IS NULL THEN 0 ELSE B.R3 END AS R3
					, CASE WHEN B.R4 IS NULL THEN 0 ELSE B.R4 END AS R4
					, CASE WHEN B.R5 IS NULL THEN 0 ELSE B.R5 END AS R5
					, CASE WHEN B.R6 IS NULL THEN 0 ELSE B.R6 END AS R6
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEL A LEFT JOIN
					(SELECT 
						B.NO_KEL 
						,B.NAMA_KEL
						, SUM(CASE WHEN A.PNYDNG_CCT  = 1 THEN 1 ELSE 0 END) R1
						, SUM(CASE WHEN A.PNYDNG_CCT = 2 THEN 1 ELSE 0 END) R2
						, SUM(CASE WHEN A.PNYDNG_CCT = 3 THEN 1 ELSE 0 END) R3
						, SUM(CASE WHEN A.PNYDNG_CCT = 4 THEN 1 ELSE 0 END) R4
						, SUM(CASE WHEN A.PNYDNG_CCT = 5 THEN 1 ELSE 0 END) R5
						, SUM(CASE WHEN A.PNYDNG_CCT NOT BETWEEN 1 AND 5 OR PNYDNG_CCT IS NULL THEN 1 ELSE 0 END) R6
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN ".$dkb_bio." A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL WHERE  KLAIN_FSK = 2 AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) ";
						if($no_kel == 0){    
					$sql .=" AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec." and  A.NO_KEL =".$no_kel."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::select($sql);
               return $r;
		}

		
		public function mst_get_laporan_disabilitas_laki_laki($no_kec,$no_kel,$dkb_biodata = '',$dkb_cutoff = '',$dkb_gisa = ''){
			if (!empty($dkb_biodata)){
				$dkb_bio = $dkb_biodata;
			}else{
				$dkb_bio = Shr::do_get_dkb_bio();
			}
			if (!empty($dkb_cutoff)){
				$cut_off_dkb = $dkb_cutoff;
			}else{
				$cut_off_dkb = Shr::do_get_cut_off_date();
			}
			if (!empty($dkb_gisa)){
				$dkb_gisa = $dkb_gisa;
			}else{
				$dkb_gisa = Shr::do_get_dkb_gisa();
			}
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL
					, CASE WHEN B.R1 IS NULL THEN 0 ELSE B.R1 END AS R1
					, CASE WHEN B.R2 IS NULL THEN 0 ELSE B.R2 END AS R2
					, CASE WHEN B.R3 IS NULL THEN 0 ELSE B.R3 END AS R3
					, CASE WHEN B.R4 IS NULL THEN 0 ELSE B.R4 END AS R4
					, CASE WHEN B.R5 IS NULL THEN 0 ELSE B.R5 END AS R5
					, CASE WHEN B.R6 IS NULL THEN 0 ELSE B.R6 END AS R6
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEC A LEFT JOIN
					(SELECT 
						B.NO_KEC 
						,B.NAMA_KEC 
						, SUM(CASE WHEN A.PNYDNG_CCT  = 1 THEN 1 ELSE 0 END) R1
						, SUM(CASE WHEN A.PNYDNG_CCT = 2 THEN 1 ELSE 0 END) R2
						, SUM(CASE WHEN A.PNYDNG_CCT = 3 THEN 1 ELSE 0 END) R3
						, SUM(CASE WHEN A.PNYDNG_CCT = 4 THEN 1 ELSE 0 END) R4
						, SUM(CASE WHEN A.PNYDNG_CCT = 5 THEN 1 ELSE 0 END) R5
						, SUM(CASE WHEN A.PNYDNG_CCT NOT BETWEEN 1 AND 5 OR PNYDNG_CCT IS NULL THEN 1 ELSE 0 END) R6
						, COUNT(1) JUMLAH
						FROM SETUP_KEC B LEFT JOIN ".$dkb_bio." A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC WHERE JENIS_KLMIN = 1 AND KLAIN_FSK = 2 AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) AND A.NO_PROP = 32 AND A.NO_KAB =73 GROUP BY B.NO_KEC,B.NAMA_KEC ORDER BY B.NO_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL
					, CASE WHEN B.R1 IS NULL THEN 0 ELSE B.R1 END AS R1
					, CASE WHEN B.R2 IS NULL THEN 0 ELSE B.R2 END AS R2
					, CASE WHEN B.R3 IS NULL THEN 0 ELSE B.R3 END AS R3
					, CASE WHEN B.R4 IS NULL THEN 0 ELSE B.R4 END AS R4
					, CASE WHEN B.R5 IS NULL THEN 0 ELSE B.R5 END AS R5
					, CASE WHEN B.R6 IS NULL THEN 0 ELSE B.R6 END AS R6
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEL A LEFT JOIN
					(SELECT 
						B.NO_KEL 
						,B.NAMA_KEL
						, SUM(CASE WHEN A.PNYDNG_CCT  = 1 THEN 1 ELSE 0 END) R1
						, SUM(CASE WHEN A.PNYDNG_CCT = 2 THEN 1 ELSE 0 END) R2
						, SUM(CASE WHEN A.PNYDNG_CCT = 3 THEN 1 ELSE 0 END) R3
						, SUM(CASE WHEN A.PNYDNG_CCT = 4 THEN 1 ELSE 0 END) R4
						, SUM(CASE WHEN A.PNYDNG_CCT = 5 THEN 1 ELSE 0 END) R5
						, SUM(CASE WHEN A.PNYDNG_CCT NOT BETWEEN 1 AND 5 OR PNYDNG_CCT IS NULL THEN 1 ELSE 0 END) R6
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN ".$dkb_bio." A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL WHERE JENIS_KLMIN = 1 AND KLAIN_FSK = 2 AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) ";
						if($no_kel == 0){    
					$sql .=" AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec." and  A.NO_KEL =".$no_kel."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::select($sql);
               return $r;
		}

		
		public function mst_get_laporan_disabilitas_perempuan($no_kec,$no_kel,$dkb_biodata = '',$dkb_cutoff = '',$dkb_gisa = ''){
			if (!empty($dkb_biodata)){
				$dkb_bio = $dkb_biodata;
			}else{
				$dkb_bio = Shr::do_get_dkb_bio();
			}
			if (!empty($dkb_cutoff)){
				$cut_off_dkb = $dkb_cutoff;
			}else{
				$cut_off_dkb = Shr::do_get_cut_off_date();
			}
			if (!empty($dkb_gisa)){
				$dkb_gisa = $dkb_gisa;
			}else{
				$dkb_gisa = Shr::do_get_dkb_gisa();
			}
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL
					, CASE WHEN B.R1 IS NULL THEN 0 ELSE B.R1 END AS R1
					, CASE WHEN B.R2 IS NULL THEN 0 ELSE B.R2 END AS R2
					, CASE WHEN B.R3 IS NULL THEN 0 ELSE B.R3 END AS R3
					, CASE WHEN B.R4 IS NULL THEN 0 ELSE B.R4 END AS R4
					, CASE WHEN B.R5 IS NULL THEN 0 ELSE B.R5 END AS R5
					, CASE WHEN B.R6 IS NULL THEN 0 ELSE B.R6 END AS R6
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEC A LEFT JOIN
					(SELECT 
						B.NO_KEC 
						,B.NAMA_KEC 
						, SUM(CASE WHEN A.PNYDNG_CCT  = 1 THEN 1 ELSE 0 END) R1
						, SUM(CASE WHEN A.PNYDNG_CCT = 2 THEN 1 ELSE 0 END) R2
						, SUM(CASE WHEN A.PNYDNG_CCT = 3 THEN 1 ELSE 0 END) R3
						, SUM(CASE WHEN A.PNYDNG_CCT = 4 THEN 1 ELSE 0 END) R4
						, SUM(CASE WHEN A.PNYDNG_CCT = 5 THEN 1 ELSE 0 END) R5
						, SUM(CASE WHEN A.PNYDNG_CCT NOT BETWEEN 1 AND 5 OR PNYDNG_CCT IS NULL THEN 1 ELSE 0 END) R6
						, COUNT(1) JUMLAH
						FROM SETUP_KEC B LEFT JOIN ".$dkb_bio." A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC WHERE JENIS_KLMIN = 2 AND KLAIN_FSK = 2 AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) AND A.NO_PROP = 32 AND A.NO_KAB =73 GROUP BY B.NO_KEC,B.NAMA_KEC ORDER BY B.NO_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL
					, CASE WHEN B.R1 IS NULL THEN 0 ELSE B.R1 END AS R1
					, CASE WHEN B.R2 IS NULL THEN 0 ELSE B.R2 END AS R2
					, CASE WHEN B.R3 IS NULL THEN 0 ELSE B.R3 END AS R3
					, CASE WHEN B.R4 IS NULL THEN 0 ELSE B.R4 END AS R4
					, CASE WHEN B.R5 IS NULL THEN 0 ELSE B.R5 END AS R5
					, CASE WHEN B.R6 IS NULL THEN 0 ELSE B.R6 END AS R6
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEL A LEFT JOIN
					(SELECT 
						B.NO_KEL 
						,B.NAMA_KEL
						, SUM(CASE WHEN A.PNYDNG_CCT  = 1 THEN 1 ELSE 0 END) R1
						, SUM(CASE WHEN A.PNYDNG_CCT = 2 THEN 1 ELSE 0 END) R2
						, SUM(CASE WHEN A.PNYDNG_CCT = 3 THEN 1 ELSE 0 END) R3
						, SUM(CASE WHEN A.PNYDNG_CCT = 4 THEN 1 ELSE 0 END) R4
						, SUM(CASE WHEN A.PNYDNG_CCT = 5 THEN 1 ELSE 0 END) R5
						, SUM(CASE WHEN A.PNYDNG_CCT NOT BETWEEN 1 AND 5 OR PNYDNG_CCT IS NULL THEN 1 ELSE 0 END) R6
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN ".$dkb_bio." A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL WHERE JENIS_KLMIN = 2 AND KLAIN_FSK = 2 AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) ";
						if($no_kel == 0){    
					$sql .=" AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec." and  A.NO_KEL =".$no_kel."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::select($sql);
               return $r;
		}

		
		public function mst_get_laporan_umur($no_kec,$no_kel,$dkb_biodata = '',$dkb_cutoff = '',$dkb_gisa = ''){
			if (!empty($dkb_biodata)){
				$dkb_bio = $dkb_biodata;
			}else{
				$dkb_bio = Shr::do_get_dkb_bio();
			}
			if (!empty($dkb_cutoff)){
				$cut_off_dkb = $dkb_cutoff;
			}else{
				$cut_off_dkb = Shr::do_get_cut_off_date();
			}
			if (!empty($dkb_gisa)){
				$dkb_gisa = $dkb_gisa;
			}else{
				$dkb_gisa = Shr::do_get_dkb_gisa();
			}
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT 
					  A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL, B.R1 , B.R2 , B.R3, B.R4 , B.R5 , B.R6 , B.R7 , B.R8 , B.R9 , B.R10 , B.R11 , B.R12 , B.R13, B.R14, B.R15, B.R16, B.JUMLAH
					  FROM SETUP_KEC A LEFT JOIN (SELECT NO_KEC
                          , SUM(CASE WHEN UMUR >= 0 AND UMUR <= 4 THEN 1 ELSE 0 END) R1
                          , SUM(CASE WHEN UMUR >= 5 AND UMUR <= 9 THEN 1 ELSE 0 END) R2
                          , SUM(CASE WHEN UMUR >= 10 AND UMUR <= 14 THEN 1 ELSE 0 END) R3
                          , SUM(CASE WHEN UMUR >= 15 AND UMUR <= 19 THEN 1 ELSE 0 END) R4
                          , SUM(CASE WHEN UMUR >= 20 AND UMUR <= 24 THEN 1 ELSE 0 END) R5
                          , SUM(CASE WHEN UMUR >= 25 AND UMUR <= 29 THEN 1 ELSE 0 END) R6
                          , SUM(CASE WHEN UMUR >= 30 AND UMUR <= 34 THEN 1 ELSE 0 END) R7
                          , SUM(CASE WHEN UMUR >= 35 AND UMUR <= 39 THEN 1 ELSE 0 END) R8
                          , SUM(CASE WHEN UMUR >= 40 AND UMUR <= 44 THEN 1 ELSE 0 END) R9
                          , SUM(CASE WHEN UMUR >= 45 AND UMUR <= 49 THEN 1 ELSE 0 END) R10
                          , SUM(CASE WHEN UMUR >= 50 AND UMUR <= 54 THEN 1 ELSE 0 END) R11
                          , SUM(CASE WHEN UMUR >= 55 AND UMUR <= 59 THEN 1 ELSE 0 END) R12
                          , SUM(CASE WHEN UMUR >= 60 AND UMUR <= 64 THEN 1 ELSE 0 END) R13
                          , SUM(CASE WHEN UMUR >= 65 AND UMUR <= 69 THEN 1 ELSE 0 END) R14
                          , SUM(CASE WHEN UMUR >= 70 AND UMUR <= 74 THEN 1 ELSE 0 END) R15
                          , SUM(CASE WHEN UMUR >= 75  THEN 1 ELSE 0 END) R16
						  , COUNT(1) JUMLAH
					FROM ".$dkb_gisa." WHERE 1=1 AND NO_PROP = 32 AND NO_KAB =73 
					GROUP BY NO_KEC
					ORDER BY NO_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT 
					  A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL, B.R1 , B.R2 , B.R3, B.R4 , B.R5 , B.R6 , B.R7 , B.R8 , B.R9 , B.R10 , B.R11 , B.R12 , B.R13, B.R14, B.R15, B.R16, B.JUMLAH
					  FROM SETUP_KEL A LEFT JOIN (SELECT NO_KEL
                          , SUM(CASE WHEN UMUR >= 0 AND UMUR <= 4 THEN 1 ELSE 0 END) R1
                          , SUM(CASE WHEN UMUR >= 5 AND UMUR <= 9 THEN 1 ELSE 0 END) R2
                          , SUM(CASE WHEN UMUR >= 10 AND UMUR <= 14 THEN 1 ELSE 0 END) R3
                          , SUM(CASE WHEN UMUR >= 15 AND UMUR <= 19 THEN 1 ELSE 0 END) R4
                          , SUM(CASE WHEN UMUR >= 20 AND UMUR <= 24 THEN 1 ELSE 0 END) R5
                          , SUM(CASE WHEN UMUR >= 25 AND UMUR <= 29 THEN 1 ELSE 0 END) R6
                          , SUM(CASE WHEN UMUR >= 30 AND UMUR <= 34 THEN 1 ELSE 0 END) R7
                          , SUM(CASE WHEN UMUR >= 35 AND UMUR <= 39 THEN 1 ELSE 0 END) R8
                          , SUM(CASE WHEN UMUR >= 40 AND UMUR <= 44 THEN 1 ELSE 0 END) R9
                          , SUM(CASE WHEN UMUR >= 45 AND UMUR <= 49 THEN 1 ELSE 0 END) R10
                          , SUM(CASE WHEN UMUR >= 50 AND UMUR <= 54 THEN 1 ELSE 0 END) R11
                          , SUM(CASE WHEN UMUR >= 55 AND UMUR <= 59 THEN 1 ELSE 0 END) R12
                          , SUM(CASE WHEN UMUR >= 60 AND UMUR <= 64 THEN 1 ELSE 0 END) R13
                          , SUM(CASE WHEN UMUR >= 65 AND UMUR <= 69 THEN 1 ELSE 0 END) R14
                          , SUM(CASE WHEN UMUR >= 70 AND UMUR <= 74 THEN 1 ELSE 0 END) R15
                          , SUM(CASE WHEN UMUR >= 75  THEN 1 ELSE 0 END) R16
						  , COUNT(1) JUMLAH
					FROM ".$dkb_gisa." WHERE 1=1 ";
						if($no_kel == 0){    
					$sql .=" AND NO_PROP = 32 AND NO_KAB = 73 AND NO_KEC =".$no_kec."
						    GROUP BY NO_KEL
						    ORDER BY NO_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND NO_PROP = 32 AND NO_KAB = 73 AND NO_KEC =".$no_kec." AND NO_KEL = ".$no_kel."
						    GROUP BY NO_KEL
						    ORDER BY NO_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::select($sql);
               return $r;
		}
		
		
		public function mst_get_laporan_umur_laki_laki($no_kec,$no_kel,$dkb_biodata = '',$dkb_cutoff = '',$dkb_gisa = ''){
			if (!empty($dkb_biodata)){
				$dkb_bio = $dkb_biodata;
			}else{
				$dkb_bio = Shr::do_get_dkb_bio();
			}
			if (!empty($dkb_cutoff)){
				$cut_off_dkb = $dkb_cutoff;
			}else{
				$cut_off_dkb = Shr::do_get_cut_off_date();
			}
			if (!empty($dkb_gisa)){
				$dkb_gisa = $dkb_gisa;
			}else{
				$dkb_gisa = Shr::do_get_dkb_gisa();
			}
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT 
					  A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL, B.R1 , B.R2 , B.R3, B.R4 , B.R5 , B.R6 , B.R7 , B.R8 , B.R9 , B.R10 , B.R11 , B.R12 , B.R13, B.R14, B.R15, B.R16, B.JUMLAH
					  FROM SETUP_KEC A LEFT JOIN (SELECT NO_KEC
                          , SUM(CASE WHEN UMUR >= 0 AND UMUR <= 4 THEN 1 ELSE 0 END) R1
                          , SUM(CASE WHEN UMUR >= 5 AND UMUR <= 9 THEN 1 ELSE 0 END) R2
                          , SUM(CASE WHEN UMUR >= 10 AND UMUR <= 14 THEN 1 ELSE 0 END) R3
                          , SUM(CASE WHEN UMUR >= 15 AND UMUR <= 19 THEN 1 ELSE 0 END) R4
                          , SUM(CASE WHEN UMUR >= 20 AND UMUR <= 24 THEN 1 ELSE 0 END) R5
                          , SUM(CASE WHEN UMUR >= 25 AND UMUR <= 29 THEN 1 ELSE 0 END) R6
                          , SUM(CASE WHEN UMUR >= 30 AND UMUR <= 34 THEN 1 ELSE 0 END) R7
                          , SUM(CASE WHEN UMUR >= 35 AND UMUR <= 39 THEN 1 ELSE 0 END) R8
                          , SUM(CASE WHEN UMUR >= 40 AND UMUR <= 44 THEN 1 ELSE 0 END) R9
                          , SUM(CASE WHEN UMUR >= 45 AND UMUR <= 49 THEN 1 ELSE 0 END) R10
                          , SUM(CASE WHEN UMUR >= 50 AND UMUR <= 54 THEN 1 ELSE 0 END) R11
                          , SUM(CASE WHEN UMUR >= 55 AND UMUR <= 59 THEN 1 ELSE 0 END) R12
                          , SUM(CASE WHEN UMUR >= 60 AND UMUR <= 64 THEN 1 ELSE 0 END) R13
                          , SUM(CASE WHEN UMUR >= 65 AND UMUR <= 69 THEN 1 ELSE 0 END) R14
                          , SUM(CASE WHEN UMUR >= 70 AND UMUR <= 74 THEN 1 ELSE 0 END) R15
                          , SUM(CASE WHEN UMUR >= 75  THEN 1 ELSE 0 END) R16
						  , COUNT(1) JUMLAH
					FROM ".$dkb_gisa." WHERE JENIS_KELAMIN = 1 AND 1=1 AND NO_PROP = 32 AND NO_KAB =73 
					GROUP BY NO_KEC
					ORDER BY NO_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT 
					  A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL, B.R1 , B.R2 , B.R3, B.R4 , B.R5 , B.R6 , B.R7 , B.R8 , B.R9 , B.R10 , B.R11 , B.R12 , B.R13, B.R14, B.R15, B.R16, B.JUMLAH
					  FROM SETUP_KEL A LEFT JOIN (SELECT NO_KEL
                          , SUM(CASE WHEN UMUR >= 0 AND UMUR <= 4 THEN 1 ELSE 0 END) R1
                          , SUM(CASE WHEN UMUR >= 5 AND UMUR <= 9 THEN 1 ELSE 0 END) R2
                          , SUM(CASE WHEN UMUR >= 10 AND UMUR <= 14 THEN 1 ELSE 0 END) R3
                          , SUM(CASE WHEN UMUR >= 15 AND UMUR <= 19 THEN 1 ELSE 0 END) R4
                          , SUM(CASE WHEN UMUR >= 20 AND UMUR <= 24 THEN 1 ELSE 0 END) R5
                          , SUM(CASE WHEN UMUR >= 25 AND UMUR <= 29 THEN 1 ELSE 0 END) R6
                          , SUM(CASE WHEN UMUR >= 30 AND UMUR <= 34 THEN 1 ELSE 0 END) R7
                          , SUM(CASE WHEN UMUR >= 35 AND UMUR <= 39 THEN 1 ELSE 0 END) R8
                          , SUM(CASE WHEN UMUR >= 40 AND UMUR <= 44 THEN 1 ELSE 0 END) R9
                          , SUM(CASE WHEN UMUR >= 45 AND UMUR <= 49 THEN 1 ELSE 0 END) R10
                          , SUM(CASE WHEN UMUR >= 50 AND UMUR <= 54 THEN 1 ELSE 0 END) R11
                          , SUM(CASE WHEN UMUR >= 55 AND UMUR <= 59 THEN 1 ELSE 0 END) R12
                          , SUM(CASE WHEN UMUR >= 60 AND UMUR <= 64 THEN 1 ELSE 0 END) R13
                          , SUM(CASE WHEN UMUR >= 65 AND UMUR <= 69 THEN 1 ELSE 0 END) R14
                          , SUM(CASE WHEN UMUR >= 70 AND UMUR <= 74 THEN 1 ELSE 0 END) R15
                          , SUM(CASE WHEN UMUR >= 75  THEN 1 ELSE 0 END) R16
						  , COUNT(1) JUMLAH
					FROM ".$dkb_gisa." WHERE JENIS_KELAMIN = 1 AND 1=1 ";
						if($no_kel == 0){    
					$sql .=" AND NO_PROP = 32 AND NO_KAB = 73 AND NO_KEC =".$no_kec."
						    GROUP BY NO_KEL
						    ORDER BY NO_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND NO_PROP = 32 AND NO_KAB = 73 AND NO_KEC =".$no_kec." AND NO_KEL = ".$no_kel."
						    GROUP BY NO_KEL
						    ORDER BY NO_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::select($sql);
               return $r;
		}
		
		

		public function mst_get_laporan_umur_perempuan($no_kec,$no_kel,$dkb_biodata = '',$dkb_cutoff = '',$dkb_gisa = ''){
			if (!empty($dkb_biodata)){
				$dkb_bio = $dkb_biodata;
			}else{
				$dkb_bio = Shr::do_get_dkb_bio();
			}
			if (!empty($dkb_cutoff)){
				$cut_off_dkb = $dkb_cutoff;
			}else{
				$cut_off_dkb = Shr::do_get_cut_off_date();
			}
			if (!empty($dkb_gisa)){
				$dkb_gisa = $dkb_gisa;
			}else{
				$dkb_gisa = Shr::do_get_dkb_gisa();
			}
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT 
					  A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL, B.R1 , B.R2 , B.R3, B.R4 , B.R5 , B.R6 , B.R7 , B.R8 , B.R9 , B.R10 , B.R11 , B.R12 , B.R13, B.R14, B.R15, B.R16, B.JUMLAH
					  FROM SETUP_KEC A LEFT JOIN (SELECT NO_KEC
                          , SUM(CASE WHEN UMUR >= 0 AND UMUR <= 4 THEN 1 ELSE 0 END) R1
                          , SUM(CASE WHEN UMUR >= 5 AND UMUR <= 9 THEN 1 ELSE 0 END) R2
                          , SUM(CASE WHEN UMUR >= 10 AND UMUR <= 14 THEN 1 ELSE 0 END) R3
                          , SUM(CASE WHEN UMUR >= 15 AND UMUR <= 19 THEN 1 ELSE 0 END) R4
                          , SUM(CASE WHEN UMUR >= 20 AND UMUR <= 24 THEN 1 ELSE 0 END) R5
                          , SUM(CASE WHEN UMUR >= 25 AND UMUR <= 29 THEN 1 ELSE 0 END) R6
                          , SUM(CASE WHEN UMUR >= 30 AND UMUR <= 34 THEN 1 ELSE 0 END) R7
                          , SUM(CASE WHEN UMUR >= 35 AND UMUR <= 39 THEN 1 ELSE 0 END) R8
                          , SUM(CASE WHEN UMUR >= 40 AND UMUR <= 44 THEN 1 ELSE 0 END) R9
                          , SUM(CASE WHEN UMUR >= 45 AND UMUR <= 49 THEN 1 ELSE 0 END) R10
                          , SUM(CASE WHEN UMUR >= 50 AND UMUR <= 54 THEN 1 ELSE 0 END) R11
                          , SUM(CASE WHEN UMUR >= 55 AND UMUR <= 59 THEN 1 ELSE 0 END) R12
                          , SUM(CASE WHEN UMUR >= 60 AND UMUR <= 64 THEN 1 ELSE 0 END) R13
                          , SUM(CASE WHEN UMUR >= 65 AND UMUR <= 69 THEN 1 ELSE 0 END) R14
                          , SUM(CASE WHEN UMUR >= 70 AND UMUR <= 74 THEN 1 ELSE 0 END) R15
                          , SUM(CASE WHEN UMUR >= 75  THEN 1 ELSE 0 END) R16
						  , COUNT(1) JUMLAH
					FROM ".$dkb_gisa." WHERE JENIS_KELAMIN = 2 AND 1=1 AND NO_PROP = 32 AND NO_KAB =73 
					GROUP BY NO_KEC
					ORDER BY NO_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT 
					  A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL, B.R1 , B.R2 , B.R3, B.R4 , B.R5 , B.R6 , B.R7 , B.R8 , B.R9 , B.R10 , B.R11 , B.R12 , B.R13, B.R14, B.R15, B.R16, B.JUMLAH
					  FROM SETUP_KEL A LEFT JOIN (SELECT NO_KEL
                          , SUM(CASE WHEN UMUR >= 0 AND UMUR <= 4 THEN 1 ELSE 0 END) R1
                          , SUM(CASE WHEN UMUR >= 5 AND UMUR <= 9 THEN 1 ELSE 0 END) R2
                          , SUM(CASE WHEN UMUR >= 10 AND UMUR <= 14 THEN 1 ELSE 0 END) R3
                          , SUM(CASE WHEN UMUR >= 15 AND UMUR <= 19 THEN 1 ELSE 0 END) R4
                          , SUM(CASE WHEN UMUR >= 20 AND UMUR <= 24 THEN 1 ELSE 0 END) R5
                          , SUM(CASE WHEN UMUR >= 25 AND UMUR <= 29 THEN 1 ELSE 0 END) R6
                          , SUM(CASE WHEN UMUR >= 30 AND UMUR <= 34 THEN 1 ELSE 0 END) R7
                          , SUM(CASE WHEN UMUR >= 35 AND UMUR <= 39 THEN 1 ELSE 0 END) R8
                          , SUM(CASE WHEN UMUR >= 40 AND UMUR <= 44 THEN 1 ELSE 0 END) R9
                          , SUM(CASE WHEN UMUR >= 45 AND UMUR <= 49 THEN 1 ELSE 0 END) R10
                          , SUM(CASE WHEN UMUR >= 50 AND UMUR <= 54 THEN 1 ELSE 0 END) R11
                          , SUM(CASE WHEN UMUR >= 55 AND UMUR <= 59 THEN 1 ELSE 0 END) R12
                          , SUM(CASE WHEN UMUR >= 60 AND UMUR <= 64 THEN 1 ELSE 0 END) R13
                          , SUM(CASE WHEN UMUR >= 65 AND UMUR <= 69 THEN 1 ELSE 0 END) R14
                          , SUM(CASE WHEN UMUR >= 70 AND UMUR <= 74 THEN 1 ELSE 0 END) R15
                          , SUM(CASE WHEN UMUR >= 75  THEN 1 ELSE 0 END) R16
						  , COUNT(1) JUMLAH
					FROM ".$dkb_gisa." WHERE JENIS_KELAMIN = 2 AND 1=1 ";
						if($no_kel == 0){    
					$sql .=" AND NO_PROP = 32 AND NO_KAB = 73 AND NO_KEC =".$no_kec."
						    GROUP BY NO_KEL
						    ORDER BY NO_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND NO_PROP = 32 AND NO_KAB = 73 AND NO_KEC =".$no_kec." AND NO_KEL = ".$no_kel."
						    GROUP BY NO_KEL
						    ORDER BY NO_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::select($sql);
               return $r;
		}
		
		
		public function mst_get_laporan_umur_lansia($no_kec,$no_kel,$dkb_biodata = '',$dkb_cutoff = '',$dkb_gisa = ''){
			if (!empty($dkb_biodata)){
				$dkb_bio = $dkb_biodata;
			}else{
				$dkb_bio = Shr::do_get_dkb_bio();
			}
			if (!empty($dkb_cutoff)){
				$cut_off_dkb = $dkb_cutoff;
			}else{
				$cut_off_dkb = Shr::do_get_cut_off_date();
			}
			if (!empty($dkb_gisa)){
				$dkb_gisa = $dkb_gisa;
			}else{
				$dkb_gisa = Shr::do_get_dkb_gisa();
			}
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT 
					  A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL, B.R1 , B.R2 , B.R3, B.R4 , B.R5 , B.R6 , B.R7 , B.R8 , B.R9, B.JUMLAH
					  FROM SETUP_KEC A LEFT JOIN (SELECT NO_KEC
                          , SUM(CASE WHEN UMUR >= 60 AND UMUR <= 64 THEN 1 ELSE 0 END) R1
                          , SUM(CASE WHEN UMUR >= 65 AND UMUR <= 69 THEN 1 ELSE 0 END) R2
                          , SUM(CASE WHEN UMUR >= 70 AND UMUR <= 74 THEN 1 ELSE 0 END) R3
                          , SUM(CASE WHEN UMUR >= 75 AND UMUR <= 79 THEN 1 ELSE 0 END) R4
                          , SUM(CASE WHEN UMUR >= 80 AND UMUR <= 84 THEN 1 ELSE 0 END) R5
                          , SUM(CASE WHEN UMUR >= 85 AND UMUR <= 89 THEN 1 ELSE 0 END) R6
                          , SUM(CASE WHEN UMUR >= 90 AND UMUR <= 94 THEN 1 ELSE 0 END) R7
                          , SUM(CASE WHEN UMUR >= 95 AND UMUR <= 99 THEN 1 ELSE 0 END) R8
                          , SUM(CASE WHEN UMUR >= 100  THEN 1 ELSE 0 END) R9
						  , COUNT(1) JUMLAH
					FROM ".$dkb_gisa." WHERE  1=1 AND NO_PROP = 32 AND NO_KAB =73 
					GROUP BY NO_KEC
					ORDER BY NO_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT 
					  A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL, B.R1 , B.R2 , B.R3, B.R4 , B.R5 , B.R6 , B.R7 , B.R8 , B.R9, B.JUMLAH
					  FROM SETUP_KEL A LEFT JOIN (SELECT NO_KEL
                          , SUM(CASE WHEN UMUR >= 60 AND UMUR <= 64 THEN 1 ELSE 0 END) R1
                          , SUM(CASE WHEN UMUR >= 65 AND UMUR <= 69 THEN 1 ELSE 0 END) R2
                          , SUM(CASE WHEN UMUR >= 70 AND UMUR <= 74 THEN 1 ELSE 0 END) R3
                          , SUM(CASE WHEN UMUR >= 75 AND UMUR <= 79 THEN 1 ELSE 0 END) R4
                          , SUM(CASE WHEN UMUR >= 80 AND UMUR <= 84 THEN 1 ELSE 0 END) R5
                          , SUM(CASE WHEN UMUR >= 85 AND UMUR <= 89 THEN 1 ELSE 0 END) R6
                          , SUM(CASE WHEN UMUR >= 90 AND UMUR <= 94 THEN 1 ELSE 0 END) R7
                          , SUM(CASE WHEN UMUR >= 95 AND UMUR <= 99 THEN 1 ELSE 0 END) R8
                          , SUM(CASE WHEN UMUR >= 100  THEN 1 ELSE 0 END) R9
						  , COUNT(1) JUMLAH
					FROM ".$dkb_gisa." WHERE  1=1 ";
						if($no_kel == 0){    
					$sql .=" AND NO_PROP = 32 AND NO_KAB = 73 AND NO_KEC =".$no_kec."
						    GROUP BY NO_KEL
						    ORDER BY NO_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND NO_PROP = 32 AND NO_KAB = 73 AND NO_KEC =".$no_kec." AND NO_KEL = ".$no_kel."
						    GROUP BY NO_KEL
						    ORDER BY NO_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::select($sql);
               return $r;
		}
		
		

		public function mst_get_laporan_umur_lansia_laki_laki($no_kec,$no_kel,$dkb_biodata = '',$dkb_cutoff = '',$dkb_gisa = ''){
			if (!empty($dkb_biodata)){
				$dkb_bio = $dkb_biodata;
			}else{
				$dkb_bio = Shr::do_get_dkb_bio();
			}
			if (!empty($dkb_cutoff)){
				$cut_off_dkb = $dkb_cutoff;
			}else{
				$cut_off_dkb = Shr::do_get_cut_off_date();
			}
			if (!empty($dkb_gisa)){
				$dkb_gisa = $dkb_gisa;
			}else{
				$dkb_gisa = Shr::do_get_dkb_gisa();
			}
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT 
					  A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL, B.R1 , B.R2 , B.R3, B.R4 , B.R5 , B.R6 , B.R7 , B.R8 , B.R9, B.JUMLAH
					  FROM SETUP_KEC A LEFT JOIN (SELECT NO_KEC
                          , SUM(CASE WHEN UMUR >= 60 AND UMUR <= 64 THEN 1 ELSE 0 END) R1
                          , SUM(CASE WHEN UMUR >= 65 AND UMUR <= 69 THEN 1 ELSE 0 END) R2
                          , SUM(CASE WHEN UMUR >= 70 AND UMUR <= 74 THEN 1 ELSE 0 END) R3
                          , SUM(CASE WHEN UMUR >= 75 AND UMUR <= 79 THEN 1 ELSE 0 END) R4
                          , SUM(CASE WHEN UMUR >= 80 AND UMUR <= 84 THEN 1 ELSE 0 END) R5
                          , SUM(CASE WHEN UMUR >= 85 AND UMUR <= 89 THEN 1 ELSE 0 END) R6
                          , SUM(CASE WHEN UMUR >= 90 AND UMUR <= 94 THEN 1 ELSE 0 END) R7
                          , SUM(CASE WHEN UMUR >= 95 AND UMUR <= 99 THEN 1 ELSE 0 END) R8
                          , SUM(CASE WHEN UMUR >= 100  THEN 1 ELSE 0 END) R9
						  , COUNT(1) JUMLAH
					FROM ".$dkb_gisa." WHERE JENIS_KELAMIN = 1 AND 1=1 AND NO_PROP = 32 AND NO_KAB =73 
					GROUP BY NO_KEC
					ORDER BY NO_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT 
					  A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL, B.R1 , B.R2 , B.R3, B.R4 , B.R5 , B.R6 , B.R7 , B.R8 , B.R9, B.JUMLAH
					  FROM SETUP_KEL A LEFT JOIN (SELECT NO_KEL
                          , SUM(CASE WHEN UMUR >= 60 AND UMUR <= 64 THEN 1 ELSE 0 END) R1
                          , SUM(CASE WHEN UMUR >= 65 AND UMUR <= 69 THEN 1 ELSE 0 END) R2
                          , SUM(CASE WHEN UMUR >= 70 AND UMUR <= 74 THEN 1 ELSE 0 END) R3
                          , SUM(CASE WHEN UMUR >= 75 AND UMUR <= 79 THEN 1 ELSE 0 END) R4
                          , SUM(CASE WHEN UMUR >= 80 AND UMUR <= 84 THEN 1 ELSE 0 END) R5
                          , SUM(CASE WHEN UMUR >= 85 AND UMUR <= 89 THEN 1 ELSE 0 END) R6
                          , SUM(CASE WHEN UMUR >= 90 AND UMUR <= 94 THEN 1 ELSE 0 END) R7
                          , SUM(CASE WHEN UMUR >= 95 AND UMUR <= 99 THEN 1 ELSE 0 END) R8
                          , SUM(CASE WHEN UMUR >= 100  THEN 1 ELSE 0 END) R9
						  , COUNT(1) JUMLAH
					FROM ".$dkb_gisa." WHERE JENIS_KELAMIN = 1 AND 1=1 ";
						if($no_kel == 0){    
					$sql .=" AND NO_PROP = 32 AND NO_KAB = 73 AND NO_KEC =".$no_kec."
						    GROUP BY NO_KEL
						    ORDER BY NO_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND NO_PROP = 32 AND NO_KAB = 73 AND NO_KEC =".$no_kec." AND NO_KEL = ".$no_kel."
						    GROUP BY NO_KEL
						    ORDER BY NO_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::select($sql);
               return $r;
		}
		
		
		public function mst_get_laporan_umur_lansia_perempuan($no_kec,$no_kel,$dkb_biodata = '',$dkb_cutoff = '',$dkb_gisa = ''){
			if (!empty($dkb_biodata)){
				$dkb_bio = $dkb_biodata;
			}else{
				$dkb_bio = Shr::do_get_dkb_bio();
			}
			if (!empty($dkb_cutoff)){
				$cut_off_dkb = $dkb_cutoff;
			}else{
				$cut_off_dkb = Shr::do_get_cut_off_date();
			}
			if (!empty($dkb_gisa)){
				$dkb_gisa = $dkb_gisa;
			}else{
				$dkb_gisa = Shr::do_get_dkb_gisa();
			}
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT 
					  A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL, B.R1 , B.R2 , B.R3, B.R4 , B.R5 , B.R6 , B.R7 , B.R8 , B.R9, B.JUMLAH
					  FROM SETUP_KEC A LEFT JOIN (SELECT NO_KEC
                          , SUM(CASE WHEN UMUR >= 60 AND UMUR <= 64 THEN 1 ELSE 0 END) R1
                          , SUM(CASE WHEN UMUR >= 65 AND UMUR <= 69 THEN 1 ELSE 0 END) R2
                          , SUM(CASE WHEN UMUR >= 70 AND UMUR <= 74 THEN 1 ELSE 0 END) R3
                          , SUM(CASE WHEN UMUR >= 75 AND UMUR <= 79 THEN 1 ELSE 0 END) R4
                          , SUM(CASE WHEN UMUR >= 80 AND UMUR <= 84 THEN 1 ELSE 0 END) R5
                          , SUM(CASE WHEN UMUR >= 85 AND UMUR <= 89 THEN 1 ELSE 0 END) R6
                          , SUM(CASE WHEN UMUR >= 90 AND UMUR <= 94 THEN 1 ELSE 0 END) R7
                          , SUM(CASE WHEN UMUR >= 95 AND UMUR <= 99 THEN 1 ELSE 0 END) R8
                          , SUM(CASE WHEN UMUR >= 100  THEN 1 ELSE 0 END) R9
						  , COUNT(1) JUMLAH
					FROM ".$dkb_gisa." WHERE JENIS_KELAMIN = 2 AND 1=1 AND NO_PROP = 32 AND NO_KAB =73 
					GROUP BY NO_KEC
					ORDER BY NO_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT 
					  A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL, B.R1 , B.R2 , B.R3, B.R4 , B.R5 , B.R6 , B.R7 , B.R8 , B.R9, B.JUMLAH
					  FROM SETUP_KEL A LEFT JOIN (SELECT NO_KEL
                          , SUM(CASE WHEN UMUR >= 60 AND UMUR <= 64 THEN 1 ELSE 0 END) R1
                          , SUM(CASE WHEN UMUR >= 65 AND UMUR <= 69 THEN 1 ELSE 0 END) R2
                          , SUM(CASE WHEN UMUR >= 70 AND UMUR <= 74 THEN 1 ELSE 0 END) R3
                          , SUM(CASE WHEN UMUR >= 75 AND UMUR <= 79 THEN 1 ELSE 0 END) R4
                          , SUM(CASE WHEN UMUR >= 80 AND UMUR <= 84 THEN 1 ELSE 0 END) R5
                          , SUM(CASE WHEN UMUR >= 85 AND UMUR <= 89 THEN 1 ELSE 0 END) R6
                          , SUM(CASE WHEN UMUR >= 90 AND UMUR <= 94 THEN 1 ELSE 0 END) R7
                          , SUM(CASE WHEN UMUR >= 95 AND UMUR <= 99 THEN 1 ELSE 0 END) R8
                          , SUM(CASE WHEN UMUR >= 100  THEN 1 ELSE 0 END) R9
						  , COUNT(1) JUMLAH
					FROM ".$dkb_gisa." WHERE JENIS_KELAMIN = 2 AND 1=1 ";
						if($no_kel == 0){    
					$sql .=" AND NO_PROP = 32 AND NO_KAB = 73 AND NO_KEC =".$no_kec."
						    GROUP BY NO_KEL
						    ORDER BY NO_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND NO_PROP = 32 AND NO_KAB = 73 AND NO_KEC =".$no_kec." AND NO_KEL = ".$no_kel."
						    GROUP BY NO_KEL
						    ORDER BY NO_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::select($sql);
               return $r;
		}
		
		
		public function mlu_get_laporan_umur($no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT NO_KEC NO_WIL,
					F5_GET_NAMA_KECAMATAN(32,73,NO_KEC) NAMA_WIL
                          , SUM(CASE WHEN UMUR >= 0 AND UMUR <= 4 THEN 1 ELSE 0 END) R1
                          , SUM(CASE WHEN UMUR >= 5 AND UMUR <= 9 THEN 1 ELSE 0 END) R2
                          , SUM(CASE WHEN UMUR >= 10 AND UMUR <= 14 THEN 1 ELSE 0 END) R3
                          , SUM(CASE WHEN UMUR >= 15 AND UMUR <= 19 THEN 1 ELSE 0 END) R4
                          , SUM(CASE WHEN UMUR >= 20 AND UMUR <= 24 THEN 1 ELSE 0 END) R5
                          , SUM(CASE WHEN UMUR >= 25 AND UMUR <= 29 THEN 1 ELSE 0 END) R6
                          , SUM(CASE WHEN UMUR >= 30 AND UMUR <= 34 THEN 1 ELSE 0 END) R7
                          , SUM(CASE WHEN UMUR >= 35 AND UMUR <= 39 THEN 1 ELSE 0 END) R8
                          , SUM(CASE WHEN UMUR >= 40 AND UMUR <= 44 THEN 1 ELSE 0 END) R9
                          , SUM(CASE WHEN UMUR >= 45 AND UMUR <= 49 THEN 1 ELSE 0 END) R10
                          , SUM(CASE WHEN UMUR >= 50 AND UMUR <= 54 THEN 1 ELSE 0 END) R11
                          , SUM(CASE WHEN UMUR >= 55 AND UMUR <= 59 THEN 1 ELSE 0 END) R12
                          , SUM(CASE WHEN UMUR >= 60 AND UMUR <= 64 THEN 1 ELSE 0 END) R13
                          , SUM(CASE WHEN UMUR >= 65 AND UMUR <= 69 THEN 1 ELSE 0 END) R14
                          , SUM(CASE WHEN UMUR >= 70 AND UMUR <= 74 THEN 1 ELSE 0 END) R15
                          , SUM(CASE WHEN UMUR >= 75  THEN 1 ELSE 0 END) R16
						  , COUNT(1) JUMLAH
					FROM KAMPUNG_GISA_DKB WHERE  1=1 AND NO_PROP = 32 AND NO_KAB =73 
					GROUP BY NO_KEC
					ORDER BY NO_KEC";	
			}else{
				$sql .="SELECT NO_KEL NO_WIL,
					  F5_GET_NAMA_KELURAHAN(32,73,NO_KEC,NO_KEL) NAMA_WIL
                          , SUM(CASE WHEN UMUR >= 0 AND UMUR <= 4 THEN 1 ELSE 0 END) R1
                          , SUM(CASE WHEN UMUR >= 5 AND UMUR <= 9 THEN 1 ELSE 0 END) R2
                          , SUM(CASE WHEN UMUR >= 10 AND UMUR <= 14 THEN 1 ELSE 0 END) R3
                          , SUM(CASE WHEN UMUR >= 15 AND UMUR <= 19 THEN 1 ELSE 0 END) R4
                          , SUM(CASE WHEN UMUR >= 20 AND UMUR <= 24 THEN 1 ELSE 0 END) R5
                          , SUM(CASE WHEN UMUR >= 25 AND UMUR <= 29 THEN 1 ELSE 0 END) R6
                          , SUM(CASE WHEN UMUR >= 30 AND UMUR <= 34 THEN 1 ELSE 0 END) R7
                          , SUM(CASE WHEN UMUR >= 35 AND UMUR <= 39 THEN 1 ELSE 0 END) R8
                          , SUM(CASE WHEN UMUR >= 40 AND UMUR <= 44 THEN 1 ELSE 0 END) R9
                          , SUM(CASE WHEN UMUR >= 45 AND UMUR <= 49 THEN 1 ELSE 0 END) R10
                          , SUM(CASE WHEN UMUR >= 50 AND UMUR <= 54 THEN 1 ELSE 0 END) R11
                          , SUM(CASE WHEN UMUR >= 55 AND UMUR <= 59 THEN 1 ELSE 0 END) R12
                          , SUM(CASE WHEN UMUR >= 60 AND UMUR <= 64 THEN 1 ELSE 0 END) R13
                          , SUM(CASE WHEN UMUR >= 65 AND UMUR <= 69 THEN 1 ELSE 0 END) R14
                          , SUM(CASE WHEN UMUR >= 70 AND UMUR <= 74 THEN 1 ELSE 0 END) R15
                          , SUM(CASE WHEN UMUR >= 75  THEN 1 ELSE 0 END) R16
						  , COUNT(1) JUMLAH
					FROM KAMPUNG_GISA_DKB WHERE  1=1 ";
						if($no_kel == 0){    
					$sql .=" AND NO_PROP = 32 AND NO_KAB = 73 AND NO_KEC =".$no_kec."
						    GROUP BY NO_KEC,NO_KEL
						    ORDER BY NO_KEC,NO_KEL";
						}else{
					$sql .=" AND NO_PROP = 32 AND NO_KAB = 73 AND NO_KEC =".$no_kec." AND NO_KEL = ".$no_kel."
						    GROUP BY NO_KEC,NO_KEL
						    ORDER BY NO_KEC,NO_KEL";
						}
				}
				$r = DB::select($sql);
               return $r;
		}
		
		

		public function mlu_get_laporan_umur_laki_laki($no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT 
					  A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL, B.R1 , B.R2 , B.R3, B.R4 , B.R5 , B.R6 , B.R7 , B.R8 , B.R9 , B.R10 , B.R11 , B.R12 , B.R13, B.R14, B.R15, B.R16, B.JUMLAH
					  FROM SETUP_KEC A LEFT JOIN (SELECT NO_KEC
                          , SUM(CASE WHEN UMUR >= 0 AND UMUR <= 4 THEN 1 ELSE 0 END) R1
                          , SUM(CASE WHEN UMUR >= 5 AND UMUR <= 9 THEN 1 ELSE 0 END) R2
                          , SUM(CASE WHEN UMUR >= 10 AND UMUR <= 14 THEN 1 ELSE 0 END) R3
                          , SUM(CASE WHEN UMUR >= 15 AND UMUR <= 19 THEN 1 ELSE 0 END) R4
                          , SUM(CASE WHEN UMUR >= 20 AND UMUR <= 24 THEN 1 ELSE 0 END) R5
                          , SUM(CASE WHEN UMUR >= 25 AND UMUR <= 29 THEN 1 ELSE 0 END) R6
                          , SUM(CASE WHEN UMUR >= 30 AND UMUR <= 34 THEN 1 ELSE 0 END) R7
                          , SUM(CASE WHEN UMUR >= 35 AND UMUR <= 39 THEN 1 ELSE 0 END) R8
                          , SUM(CASE WHEN UMUR >= 40 AND UMUR <= 44 THEN 1 ELSE 0 END) R9
                          , SUM(CASE WHEN UMUR >= 45 AND UMUR <= 49 THEN 1 ELSE 0 END) R10
                          , SUM(CASE WHEN UMUR >= 50 AND UMUR <= 54 THEN 1 ELSE 0 END) R11
                          , SUM(CASE WHEN UMUR >= 55 AND UMUR <= 59 THEN 1 ELSE 0 END) R12
                          , SUM(CASE WHEN UMUR >= 60 AND UMUR <= 64 THEN 1 ELSE 0 END) R13
                          , SUM(CASE WHEN UMUR >= 65 AND UMUR <= 69 THEN 1 ELSE 0 END) R14
                          , SUM(CASE WHEN UMUR >= 70 AND UMUR <= 74 THEN 1 ELSE 0 END) R15
                          , SUM(CASE WHEN UMUR >= 75  THEN 1 ELSE 0 END) R16
						  , COUNT(1) JUMLAH
					FROM KAMPUNG_GISA_DKB WHERE JENIS_KELAMIN = 1 AND 1=1 AND NO_PROP = 32 AND NO_KAB =73 
					GROUP BY NO_KEC
					ORDER BY NO_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT 
					  A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL, B.R1 , B.R2 , B.R3, B.R4 , B.R5 , B.R6 , B.R7 , B.R8 , B.R9 , B.R10 , B.R11 , B.R12 , B.R13, B.R14, B.R15, B.R16, B.JUMLAH
					  FROM SETUP_KEL A LEFT JOIN (SELECT NO_KEL
                          , SUM(CASE WHEN UMUR >= 0 AND UMUR <= 4 THEN 1 ELSE 0 END) R1
                          , SUM(CASE WHEN UMUR >= 5 AND UMUR <= 9 THEN 1 ELSE 0 END) R2
                          , SUM(CASE WHEN UMUR >= 10 AND UMUR <= 14 THEN 1 ELSE 0 END) R3
                          , SUM(CASE WHEN UMUR >= 15 AND UMUR <= 19 THEN 1 ELSE 0 END) R4
                          , SUM(CASE WHEN UMUR >= 20 AND UMUR <= 24 THEN 1 ELSE 0 END) R5
                          , SUM(CASE WHEN UMUR >= 25 AND UMUR <= 29 THEN 1 ELSE 0 END) R6
                          , SUM(CASE WHEN UMUR >= 30 AND UMUR <= 34 THEN 1 ELSE 0 END) R7
                          , SUM(CASE WHEN UMUR >= 35 AND UMUR <= 39 THEN 1 ELSE 0 END) R8
                          , SUM(CASE WHEN UMUR >= 40 AND UMUR <= 44 THEN 1 ELSE 0 END) R9
                          , SUM(CASE WHEN UMUR >= 45 AND UMUR <= 49 THEN 1 ELSE 0 END) R10
                          , SUM(CASE WHEN UMUR >= 50 AND UMUR <= 54 THEN 1 ELSE 0 END) R11
                          , SUM(CASE WHEN UMUR >= 55 AND UMUR <= 59 THEN 1 ELSE 0 END) R12
                          , SUM(CASE WHEN UMUR >= 60 AND UMUR <= 64 THEN 1 ELSE 0 END) R13
                          , SUM(CASE WHEN UMUR >= 65 AND UMUR <= 69 THEN 1 ELSE 0 END) R14
                          , SUM(CASE WHEN UMUR >= 70 AND UMUR <= 74 THEN 1 ELSE 0 END) R15
                          , SUM(CASE WHEN UMUR >= 75  THEN 1 ELSE 0 END) R16
						  , COUNT(1) JUMLAH
					FROM KAMPUNG_GISA_DKB WHERE JENIS_KELAMIN = 1 AND 1=1 ";
						if($no_kel == 0){    
					$sql .=" AND NO_PROP = 32 AND NO_KAB = 73 AND NO_KEC =".$no_kec."
						    GROUP BY NO_KEL
						    ORDER BY NO_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND NO_PROP = 32 AND NO_KAB = 73 AND NO_KEC =".$no_kec." AND NO_KEL = ".$no_kel."
						    GROUP BY NO_KEL
						    ORDER BY NO_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::select($sql);
               return $r;
		}
		
		

		public function mlu_get_laporan_umur_perempuan($no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT 
					  A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL, B.R1 , B.R2 , B.R3, B.R4 , B.R5 , B.R6 , B.R7 , B.R8 , B.R9 , B.R10 , B.R11 , B.R12 , B.R13, B.R14, B.R15, B.R16, B.JUMLAH
					  FROM SETUP_KEC A LEFT JOIN (SELECT NO_KEC
                          , SUM(CASE WHEN UMUR >= 0 AND UMUR <= 4 THEN 1 ELSE 0 END) R1
                          , SUM(CASE WHEN UMUR >= 5 AND UMUR <= 9 THEN 1 ELSE 0 END) R2
                          , SUM(CASE WHEN UMUR >= 10 AND UMUR <= 14 THEN 1 ELSE 0 END) R3
                          , SUM(CASE WHEN UMUR >= 15 AND UMUR <= 19 THEN 1 ELSE 0 END) R4
                          , SUM(CASE WHEN UMUR >= 20 AND UMUR <= 24 THEN 1 ELSE 0 END) R5
                          , SUM(CASE WHEN UMUR >= 25 AND UMUR <= 29 THEN 1 ELSE 0 END) R6
                          , SUM(CASE WHEN UMUR >= 30 AND UMUR <= 34 THEN 1 ELSE 0 END) R7
                          , SUM(CASE WHEN UMUR >= 35 AND UMUR <= 39 THEN 1 ELSE 0 END) R8
                          , SUM(CASE WHEN UMUR >= 40 AND UMUR <= 44 THEN 1 ELSE 0 END) R9
                          , SUM(CASE WHEN UMUR >= 45 AND UMUR <= 49 THEN 1 ELSE 0 END) R10
                          , SUM(CASE WHEN UMUR >= 50 AND UMUR <= 54 THEN 1 ELSE 0 END) R11
                          , SUM(CASE WHEN UMUR >= 55 AND UMUR <= 59 THEN 1 ELSE 0 END) R12
                          , SUM(CASE WHEN UMUR >= 60 AND UMUR <= 64 THEN 1 ELSE 0 END) R13
                          , SUM(CASE WHEN UMUR >= 65 AND UMUR <= 69 THEN 1 ELSE 0 END) R14
                          , SUM(CASE WHEN UMUR >= 70 AND UMUR <= 74 THEN 1 ELSE 0 END) R15
                          , SUM(CASE WHEN UMUR >= 75  THEN 1 ELSE 0 END) R16
						  , COUNT(1) JUMLAH
					FROM KAMPUNG_GISA_DKB WHERE JENIS_KELAMIN = 2 AND 1=1 AND NO_PROP = 32 AND NO_KAB =73 
					GROUP BY NO_KEC
					ORDER BY NO_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT 
					  A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL, B.R1 , B.R2 , B.R3, B.R4 , B.R5 , B.R6 , B.R7 , B.R8 , B.R9 , B.R10 , B.R11 , B.R12 , B.R13, B.R14, B.R15, B.R16, B.JUMLAH
					  FROM SETUP_KEL A LEFT JOIN (SELECT NO_KEL
                          , SUM(CASE WHEN UMUR >= 0 AND UMUR <= 4 THEN 1 ELSE 0 END) R1
                          , SUM(CASE WHEN UMUR >= 5 AND UMUR <= 9 THEN 1 ELSE 0 END) R2
                          , SUM(CASE WHEN UMUR >= 10 AND UMUR <= 14 THEN 1 ELSE 0 END) R3
                          , SUM(CASE WHEN UMUR >= 15 AND UMUR <= 19 THEN 1 ELSE 0 END) R4
                          , SUM(CASE WHEN UMUR >= 20 AND UMUR <= 24 THEN 1 ELSE 0 END) R5
                          , SUM(CASE WHEN UMUR >= 25 AND UMUR <= 29 THEN 1 ELSE 0 END) R6
                          , SUM(CASE WHEN UMUR >= 30 AND UMUR <= 34 THEN 1 ELSE 0 END) R7
                          , SUM(CASE WHEN UMUR >= 35 AND UMUR <= 39 THEN 1 ELSE 0 END) R8
                          , SUM(CASE WHEN UMUR >= 40 AND UMUR <= 44 THEN 1 ELSE 0 END) R9
                          , SUM(CASE WHEN UMUR >= 45 AND UMUR <= 49 THEN 1 ELSE 0 END) R10
                          , SUM(CASE WHEN UMUR >= 50 AND UMUR <= 54 THEN 1 ELSE 0 END) R11
                          , SUM(CASE WHEN UMUR >= 55 AND UMUR <= 59 THEN 1 ELSE 0 END) R12
                          , SUM(CASE WHEN UMUR >= 60 AND UMUR <= 64 THEN 1 ELSE 0 END) R13
                          , SUM(CASE WHEN UMUR >= 65 AND UMUR <= 69 THEN 1 ELSE 0 END) R14
                          , SUM(CASE WHEN UMUR >= 70 AND UMUR <= 74 THEN 1 ELSE 0 END) R15
                          , SUM(CASE WHEN UMUR >= 75  THEN 1 ELSE 0 END) R16
						  , COUNT(1) JUMLAH
					FROM KAMPUNG_GISA_DKB WHERE JENIS_KELAMIN = 2 AND 1=1 ";
						if($no_kel == 0){    
					$sql .=" AND NO_PROP = 32 AND NO_KAB = 73 AND NO_KEC =".$no_kec."
						    GROUP BY NO_KEL
						    ORDER BY NO_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND NO_PROP = 32 AND NO_KAB = 73 AND NO_KEC =".$no_kec." AND NO_KEL = ".$no_kel."
						    GROUP BY NO_KEL
						    ORDER BY NO_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::select($sql);
               return $r;
		}
		
		

		// usia 0-10 tahun
		public function mlu_get_laporan_umur10($no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT 
					  A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL, B.R1 , B.R2 , B.R3, B.R4 , B.R5 , B.R6 , B.R7 , B.R8 , B.R9, B.JUMLAH
					  FROM SETUP_KEC A LEFT JOIN (SELECT NO_KEC 
					  	  , SUM(CASE WHEN UMUR >= 0 AND UMUR <= 9 THEN 1 ELSE 0 END) R1
                          , SUM(CASE WHEN UMUR >= 10 AND UMUR <= 19 THEN 1 ELSE 0 END) R2
                          , SUM(CASE WHEN UMUR >= 20 AND UMUR <= 29 THEN 1 ELSE 0 END) R3
                          , SUM(CASE WHEN UMUR >= 30 AND UMUR <= 39 THEN 1 ELSE 0 END) R4
                          , SUM(CASE WHEN UMUR >= 40 AND UMUR <= 49 THEN 1 ELSE 0 END) R5
                          , SUM(CASE WHEN UMUR >= 50 AND UMUR <= 59 THEN 1 ELSE 0 END) R6
                          , SUM(CASE WHEN UMUR >= 60 AND UMUR <= 69 THEN 1 ELSE 0 END) R7
                          , SUM(CASE WHEN UMUR >= 70 AND UMUR <= 79 THEN 1 ELSE 0 END) R8
                          , SUM(CASE WHEN UMUR >= 80  THEN 1 ELSE 0 END) R9
						  , COUNT(1) JUMLAH
					FROM KAMPUNG_GISA_DKB WHERE  1=1 AND NO_PROP = 32 AND NO_KAB =73 
					GROUP BY NO_KEC
					ORDER BY NO_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT 
					  A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL, B.R1 , B.R2 , B.R3, B.R4 , B.R5 , B.R6 , B.R7 , B.R8 , B.R9, B.JUMLAH
					  FROM SETUP_KEL A LEFT JOIN (SELECT NO_KEL
					  	  , SUM(CASE WHEN UMUR >= 0 AND UMUR <= 9 THEN 1 ELSE 0 END) R1
                          , SUM(CASE WHEN UMUR >= 10 AND UMUR <= 19 THEN 1 ELSE 0 END) R2
                          , SUM(CASE WHEN UMUR >= 20 AND UMUR <= 29 THEN 1 ELSE 0 END) R3
                          , SUM(CASE WHEN UMUR >= 30 AND UMUR <= 39 THEN 1 ELSE 0 END) R4
                          , SUM(CASE WHEN UMUR >= 40 AND UMUR <= 49 THEN 1 ELSE 0 END) R5
                          , SUM(CASE WHEN UMUR >= 50 AND UMUR <= 59 THEN 1 ELSE 0 END) R6
                          , SUM(CASE WHEN UMUR >= 60 AND UMUR <= 69 THEN 1 ELSE 0 END) R7
                          , SUM(CASE WHEN UMUR >= 70 AND UMUR <= 79 THEN 1 ELSE 0 END) R8
                          , SUM(CASE WHEN UMUR >= 80  THEN 1 ELSE 0 END) R9
						  , COUNT(1) JUMLAH
					FROM KAMPUNG_GISA_DKB WHERE  1=1 ";
						if($no_kel == 0){    
					$sql .=" AND NO_PROP = 32 AND NO_KAB = 73 AND NO_KEC =".$no_kec."
						    GROUP BY NO_KEL
						    ORDER BY NO_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND NO_PROP = 32 AND NO_KAB = 73 AND NO_KEC =".$no_kec." AND NO_KEL = ".$no_kel."
						    GROUP BY NO_KEL
						    ORDER BY NO_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::select($sql);
               return $r;
		}
		
		

		public function mlu_get_laporan_umur10_laki_laki($no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT 
					  A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL, B.R1 , B.R2 , B.R3, B.R4 , B.R5 , B.R6 , B.R7 , B.R8 , B.R9, B.JUMLAH
					  FROM SETUP_KEC A LEFT JOIN (SELECT NO_KEC
					  	  , SUM(CASE WHEN UMUR >= 0 AND UMUR <= 9 THEN 1 ELSE 0 END) R1
                          , SUM(CASE WHEN UMUR >= 10 AND UMUR <= 19 THEN 1 ELSE 0 END) R2
                          , SUM(CASE WHEN UMUR >= 20 AND UMUR <= 29 THEN 1 ELSE 0 END) R3
                          , SUM(CASE WHEN UMUR >= 30 AND UMUR <= 39 THEN 1 ELSE 0 END) R4
                          , SUM(CASE WHEN UMUR >= 40 AND UMUR <= 49 THEN 1 ELSE 0 END) R5
                          , SUM(CASE WHEN UMUR >= 50 AND UMUR <= 59 THEN 1 ELSE 0 END) R6
                          , SUM(CASE WHEN UMUR >= 60 AND UMUR <= 69 THEN 1 ELSE 0 END) R7
                          , SUM(CASE WHEN UMUR >= 70 AND UMUR <= 79 THEN 1 ELSE 0 END) R8
                          , SUM(CASE WHEN UMUR >= 80  THEN 1 ELSE 0 END) R9
						  , COUNT(1) JUMLAH
					FROM KAMPUNG_GISA_DKB WHERE JENIS_KELAMIN = 1 AND 1=1 AND NO_PROP = 32 AND NO_KAB =73 
					GROUP BY NO_KEC
					ORDER BY NO_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT 
					  A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL, B.R1 , B.R2 , B.R3, B.R4 , B.R5 , B.R6 , B.R7 , B.R8 , B.R9, B.JUMLAH
					  FROM SETUP_KEL A LEFT JOIN (SELECT NO_KEL
					  	  , SUM(CASE WHEN UMUR >= 0 AND UMUR <= 9 THEN 1 ELSE 0 END) R1
                          , SUM(CASE WHEN UMUR >= 10 AND UMUR <= 19 THEN 1 ELSE 0 END) R2
                          , SUM(CASE WHEN UMUR >= 20 AND UMUR <= 29 THEN 1 ELSE 0 END) R3
                          , SUM(CASE WHEN UMUR >= 30 AND UMUR <= 39 THEN 1 ELSE 0 END) R4
                          , SUM(CASE WHEN UMUR >= 40 AND UMUR <= 49 THEN 1 ELSE 0 END) R5
                          , SUM(CASE WHEN UMUR >= 50 AND UMUR <= 59 THEN 1 ELSE 0 END) R6
                          , SUM(CASE WHEN UMUR >= 60 AND UMUR <= 69 THEN 1 ELSE 0 END) R7
                          , SUM(CASE WHEN UMUR >= 70 AND UMUR <= 79 THEN 1 ELSE 0 END) R8
                          , SUM(CASE WHEN UMUR >= 80  THEN 1 ELSE 0 END) R9
						  , COUNT(1) JUMLAH
					FROM KAMPUNG_GISA_DKB WHERE JENIS_KELAMIN = 1 AND 1=1 ";
						if($no_kel == 0){    
					$sql .=" AND NO_PROP = 32 AND NO_KAB = 73 AND NO_KEC =".$no_kec."
						    GROUP BY NO_KEL
						    ORDER BY NO_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND NO_PROP = 32 AND NO_KAB = 73 AND NO_KEC =".$no_kec." AND NO_KEL = ".$no_kel."
						    GROUP BY NO_KEL
						    ORDER BY NO_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::select($sql);
               return $r;
		}
		
		

		public function mlu_get_laporan_umur10_perempuan($no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT 
					  A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL, B.R1 , B.R2 , B.R3, B.R4 , B.R5 , B.R6 , B.R7 , B.R8 , B.R9, B.JUMLAH
					  FROM SETUP_KEC A LEFT JOIN (SELECT NO_KEC
					  	  , SUM(CASE WHEN UMUR >= 0 AND UMUR <= 9 THEN 1 ELSE 0 END) R1
                          , SUM(CASE WHEN UMUR >= 10 AND UMUR <= 19 THEN 1 ELSE 0 END) R2
                          , SUM(CASE WHEN UMUR >= 20 AND UMUR <= 29 THEN 1 ELSE 0 END) R3
                          , SUM(CASE WHEN UMUR >= 30 AND UMUR <= 39 THEN 1 ELSE 0 END) R4
                          , SUM(CASE WHEN UMUR >= 40 AND UMUR <= 49 THEN 1 ELSE 0 END) R5
                          , SUM(CASE WHEN UMUR >= 50 AND UMUR <= 59 THEN 1 ELSE 0 END) R6
                          , SUM(CASE WHEN UMUR >= 60 AND UMUR <= 69 THEN 1 ELSE 0 END) R7
                          , SUM(CASE WHEN UMUR >= 70 AND UMUR <= 79 THEN 1 ELSE 0 END) R8
                          , SUM(CASE WHEN UMUR >= 80  THEN 1 ELSE 0 END) R9
						  , COUNT(1) JUMLAH
					FROM KAMPUNG_GISA_DKB WHERE JENIS_KELAMIN = 2 AND 1=1 AND NO_PROP = 32 AND NO_KAB =73 
					GROUP BY NO_KEC
					ORDER BY NO_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT 
					  A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL, B.R1 , B.R2 , B.R3, B.R4 , B.R5 , B.R6 , B.R7 , B.R8 , B.R9, B.JUMLAH
					  FROM SETUP_KEL A LEFT JOIN (SELECT NO_KEL
					  	  , SUM(CASE WHEN UMUR >= 0 AND UMUR <= 9 THEN 1 ELSE 0 END) R1
                          , SUM(CASE WHEN UMUR >= 10 AND UMUR <= 19 THEN 1 ELSE 0 END) R2
                          , SUM(CASE WHEN UMUR >= 20 AND UMUR <= 29 THEN 1 ELSE 0 END) R3
                          , SUM(CASE WHEN UMUR >= 30 AND UMUR <= 39 THEN 1 ELSE 0 END) R4
                          , SUM(CASE WHEN UMUR >= 40 AND UMUR <= 49 THEN 1 ELSE 0 END) R5
                          , SUM(CASE WHEN UMUR >= 50 AND UMUR <= 59 THEN 1 ELSE 0 END) R6
                          , SUM(CASE WHEN UMUR >= 60 AND UMUR <= 69 THEN 1 ELSE 0 END) R7
                          , SUM(CASE WHEN UMUR >= 70 AND UMUR <= 79 THEN 1 ELSE 0 END) R8
                          , SUM(CASE WHEN UMUR >= 80  THEN 1 ELSE 0 END) R9
						  , COUNT(1) JUMLAH
					FROM KAMPUNG_GISA_DKB WHERE JENIS_KELAMIN = 2 AND 1=1 ";
						if($no_kel == 0){    
					$sql .=" AND NO_PROP = 32 AND NO_KAB = 73 AND NO_KEC =".$no_kec."
						    GROUP BY NO_KEL
						    ORDER BY NO_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND NO_PROP = 32 AND NO_KAB = 73 AND NO_KEC =".$no_kec." AND NO_KEL = ".$no_kel."
						    GROUP BY NO_KEL
						    ORDER BY NO_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::select($sql);
               return $r;
		}
		
		

		// usia 0-1 tahun
		public function mlu_get_laporan_umur1($no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT 
					  A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL, B.R1,B.R2,B.R3,B.R4,B.R5,B.R6,B.R7,B.R8,B.R9,B.R10,B.R11,B.R12,B.R13,B.R14,B.R15,B.R16,B.R17,B.R18,B.R19,B.R20,B.R21,B.R22,B.R23,B.R24,B.R25,B.R26,B.R27,B.R28,B.R29,B.R30,B.R31,B.R32,B.R33,B.R34,B.R35,B.R36,B.R37,B.R38,B.R39,B.R40,B.R41,B.R42,B.R43,B.R44,B.R45,B.R46,B.R47,B.R48,B.R49,B.R50,B.R51,B.R52,B.R53,B.R54,B.R55,B.R56,B.R57,B.R58,B.R59,B.R60,B.R61,B.R62,B.R63,B.R64,B.R65,B.R66,B.R67,B.R68,B.R69,B.R70,B.R71,B.R72,B.R73,B.R74,B.R75,B.R76,B.R77,B.R78,B.R79,B.R80,B.REND,B.JUMLAH
					  FROM SETUP_KEC A LEFT JOIN 
                      (
                      SELECT NO_KEC
                          , SUM(CASE WHEN UMUR = 0 THEN 1 ELSE 0 END) R1
                          , SUM(CASE WHEN UMUR = 1 THEN 1 ELSE 0 END) R2
                          , SUM(CASE WHEN UMUR = 2 THEN 1 ELSE 0 END) R3
                          , SUM(CASE WHEN UMUR = 3 THEN 1 ELSE 0 END) R4
                          , SUM(CASE WHEN UMUR = 4 THEN 1 ELSE 0 END) R5
                          , SUM(CASE WHEN UMUR = 5 THEN 1 ELSE 0 END) R6
                          , SUM(CASE WHEN UMUR = 6 THEN 1 ELSE 0 END) R7
                          , SUM(CASE WHEN UMUR = 7 THEN 1 ELSE 0 END) R8
                          , SUM(CASE WHEN UMUR = 8 THEN 1 ELSE 0 END) R9
                          , SUM(CASE WHEN UMUR = 9 THEN 1 ELSE 0 END) R10
                          , SUM(CASE WHEN UMUR = 10 THEN 1 ELSE 0 END) R11
                          , SUM(CASE WHEN UMUR = 11 THEN 1 ELSE 0 END) R12
                          , SUM(CASE WHEN UMUR = 12 THEN 1 ELSE 0 END) R13
                          , SUM(CASE WHEN UMUR = 13 THEN 1 ELSE 0 END) R14
                          , SUM(CASE WHEN UMUR = 14 THEN 1 ELSE 0 END) R15
                          , SUM(CASE WHEN UMUR = 15 THEN 1 ELSE 0 END) R16
                          , SUM(CASE WHEN UMUR = 16 THEN 1 ELSE 0 END) R17
                          , SUM(CASE WHEN UMUR = 17 THEN 1 ELSE 0 END) R18
                          , SUM(CASE WHEN UMUR = 18 THEN 1 ELSE 0 END) R19
                          , SUM(CASE WHEN UMUR = 19 THEN 1 ELSE 0 END) R20
                          , SUM(CASE WHEN UMUR = 20 THEN 1 ELSE 0 END) R21
                          , SUM(CASE WHEN UMUR = 21 THEN 1 ELSE 0 END) R22
                          , SUM(CASE WHEN UMUR = 22 THEN 1 ELSE 0 END) R23
                          , SUM(CASE WHEN UMUR = 23 THEN 1 ELSE 0 END) R24
                          , SUM(CASE WHEN UMUR = 24 THEN 1 ELSE 0 END) R25
                          , SUM(CASE WHEN UMUR = 25 THEN 1 ELSE 0 END) R26
                          , SUM(CASE WHEN UMUR = 26 THEN 1 ELSE 0 END) R27
                          , SUM(CASE WHEN UMUR = 27 THEN 1 ELSE 0 END) R28
                          , SUM(CASE WHEN UMUR = 28 THEN 1 ELSE 0 END) R29
                          , SUM(CASE WHEN UMUR = 29 THEN 1 ELSE 0 END) R30
                          , SUM(CASE WHEN UMUR = 30 THEN 1 ELSE 0 END) R31
                          , SUM(CASE WHEN UMUR = 31 THEN 1 ELSE 0 END) R32
                          , SUM(CASE WHEN UMUR = 32 THEN 1 ELSE 0 END) R33
                          , SUM(CASE WHEN UMUR = 33 THEN 1 ELSE 0 END) R34
                          , SUM(CASE WHEN UMUR = 34 THEN 1 ELSE 0 END) R35
                          , SUM(CASE WHEN UMUR = 35 THEN 1 ELSE 0 END) R36
                          , SUM(CASE WHEN UMUR = 36 THEN 1 ELSE 0 END) R37
                          , SUM(CASE WHEN UMUR = 37 THEN 1 ELSE 0 END) R38
                          , SUM(CASE WHEN UMUR = 38 THEN 1 ELSE 0 END) R39
                          , SUM(CASE WHEN UMUR = 39 THEN 1 ELSE 0 END) R40
                          , SUM(CASE WHEN UMUR = 40 THEN 1 ELSE 0 END) R41
                          , SUM(CASE WHEN UMUR = 41 THEN 1 ELSE 0 END) R42
                          , SUM(CASE WHEN UMUR = 42 THEN 1 ELSE 0 END) R43
                          , SUM(CASE WHEN UMUR = 43 THEN 1 ELSE 0 END) R44
                          , SUM(CASE WHEN UMUR = 44 THEN 1 ELSE 0 END) R45
                          , SUM(CASE WHEN UMUR = 45 THEN 1 ELSE 0 END) R46
                          , SUM(CASE WHEN UMUR = 46 THEN 1 ELSE 0 END) R47
                          , SUM(CASE WHEN UMUR = 47 THEN 1 ELSE 0 END) R48
                          , SUM(CASE WHEN UMUR = 48 THEN 1 ELSE 0 END) R49
                          , SUM(CASE WHEN UMUR = 49 THEN 1 ELSE 0 END) R50
                          , SUM(CASE WHEN UMUR = 50 THEN 1 ELSE 0 END) R51
                          , SUM(CASE WHEN UMUR = 51 THEN 1 ELSE 0 END) R52
                          , SUM(CASE WHEN UMUR = 52 THEN 1 ELSE 0 END) R53
                          , SUM(CASE WHEN UMUR = 53 THEN 1 ELSE 0 END) R54
                          , SUM(CASE WHEN UMUR = 54 THEN 1 ELSE 0 END) R55
                          , SUM(CASE WHEN UMUR = 55 THEN 1 ELSE 0 END) R56
                          , SUM(CASE WHEN UMUR = 56 THEN 1 ELSE 0 END) R57
                          , SUM(CASE WHEN UMUR = 57 THEN 1 ELSE 0 END) R58
                          , SUM(CASE WHEN UMUR = 58 THEN 1 ELSE 0 END) R59
                          , SUM(CASE WHEN UMUR = 59 THEN 1 ELSE 0 END) R60
                          , SUM(CASE WHEN UMUR = 60 THEN 1 ELSE 0 END) R61
                          , SUM(CASE WHEN UMUR = 61 THEN 1 ELSE 0 END) R62
                          , SUM(CASE WHEN UMUR = 62 THEN 1 ELSE 0 END) R63
                          , SUM(CASE WHEN UMUR = 63 THEN 1 ELSE 0 END) R64
                          , SUM(CASE WHEN UMUR = 64 THEN 1 ELSE 0 END) R65
                          , SUM(CASE WHEN UMUR = 65 THEN 1 ELSE 0 END) R66
                          , SUM(CASE WHEN UMUR = 66 THEN 1 ELSE 0 END) R67
                          , SUM(CASE WHEN UMUR = 67 THEN 1 ELSE 0 END) R68
                          , SUM(CASE WHEN UMUR = 68 THEN 1 ELSE 0 END) R69
                          , SUM(CASE WHEN UMUR = 69 THEN 1 ELSE 0 END) R70
                          , SUM(CASE WHEN UMUR = 70 THEN 1 ELSE 0 END) R71
                          , SUM(CASE WHEN UMUR = 71 THEN 1 ELSE 0 END) R72
                          , SUM(CASE WHEN UMUR = 72 THEN 1 ELSE 0 END) R73
                          , SUM(CASE WHEN UMUR = 73 THEN 1 ELSE 0 END) R74
                          , SUM(CASE WHEN UMUR = 74 THEN 1 ELSE 0 END) R75
                          , SUM(CASE WHEN UMUR = 75 THEN 1 ELSE 0 END) R76
                          , SUM(CASE WHEN UMUR = 76 THEN 1 ELSE 0 END) R77
                          , SUM(CASE WHEN UMUR = 77 THEN 1 ELSE 0 END) R78
                          , SUM(CASE WHEN UMUR = 78 THEN 1 ELSE 0 END) R79
                          , SUM(CASE WHEN UMUR = 79 THEN 1 ELSE 0 END) R80
                          , SUM(CASE WHEN UMUR = 80 THEN 1 ELSE 0 END) R81
                          , SUM(CASE WHEN UMUR >= 80 THEN 1 ELSE 0 END) REND
                          , COUNT(1) JUMLAH
                        FROM KAMPUNG_GISA_DKB WHERE 1=1 
					GROUP BY NO_KEC
					ORDER BY NO_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT 
					  A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL, B.R1,B.R2,B.R3,B.R4,B.R5,B.R6,B.R7,B.R8,B.R9,B.R10,B.R11,B.R12,B.R13,B.R14,B.R15,B.R16,B.R17,B.R18,B.R19,B.R20,B.R21,B.R22,B.R23,B.R24,B.R25,B.R26,B.R27,B.R28,B.R29,B.R30,B.R31,B.R32,B.R33,B.R34,B.R35,B.R36,B.R37,B.R38,B.R39,B.R40,B.R41,B.R42,B.R43,B.R44,B.R45,B.R46,B.R47,B.R48,B.R49,B.R50,B.R51,B.R52,B.R53,B.R54,B.R55,B.R56,B.R57,B.R58,B.R59,B.R60,B.R61,B.R62,B.R63,B.R64,B.R65,B.R66,B.R67,B.R68,B.R69,B.R70,B.R71,B.R72,B.R73,B.R74,B.R75,B.R76,B.R77,B.R78,B.R79,B.R80,B.REND,B.JUMLAH
					  FROM SETUP_KEL A LEFT JOIN (SELECT NO_KEL
						  , SUM(CASE WHEN UMUR = 0 THEN 1 ELSE 0 END) R1
                          , SUM(CASE WHEN UMUR = 1 THEN 1 ELSE 0 END) R2
                          , SUM(CASE WHEN UMUR = 2 THEN 1 ELSE 0 END) R3
                          , SUM(CASE WHEN UMUR = 3 THEN 1 ELSE 0 END) R4
                          , SUM(CASE WHEN UMUR = 4 THEN 1 ELSE 0 END) R5
                          , SUM(CASE WHEN UMUR = 5 THEN 1 ELSE 0 END) R6
                          , SUM(CASE WHEN UMUR = 6 THEN 1 ELSE 0 END) R7
                          , SUM(CASE WHEN UMUR = 7 THEN 1 ELSE 0 END) R8
                          , SUM(CASE WHEN UMUR = 8 THEN 1 ELSE 0 END) R9
                          , SUM(CASE WHEN UMUR = 9 THEN 1 ELSE 0 END) R10
                          , SUM(CASE WHEN UMUR = 10 THEN 1 ELSE 0 END) R11
                          , SUM(CASE WHEN UMUR = 11 THEN 1 ELSE 0 END) R12
                          , SUM(CASE WHEN UMUR = 12 THEN 1 ELSE 0 END) R13
                          , SUM(CASE WHEN UMUR = 13 THEN 1 ELSE 0 END) R14
                          , SUM(CASE WHEN UMUR = 14 THEN 1 ELSE 0 END) R15
                          , SUM(CASE WHEN UMUR = 15 THEN 1 ELSE 0 END) R16
                          , SUM(CASE WHEN UMUR = 16 THEN 1 ELSE 0 END) R17
                          , SUM(CASE WHEN UMUR = 17 THEN 1 ELSE 0 END) R18
                          , SUM(CASE WHEN UMUR = 18 THEN 1 ELSE 0 END) R19
                          , SUM(CASE WHEN UMUR = 19 THEN 1 ELSE 0 END) R20
                          , SUM(CASE WHEN UMUR = 20 THEN 1 ELSE 0 END) R21
                          , SUM(CASE WHEN UMUR = 21 THEN 1 ELSE 0 END) R22
                          , SUM(CASE WHEN UMUR = 22 THEN 1 ELSE 0 END) R23
                          , SUM(CASE WHEN UMUR = 23 THEN 1 ELSE 0 END) R24
                          , SUM(CASE WHEN UMUR = 24 THEN 1 ELSE 0 END) R25
                          , SUM(CASE WHEN UMUR = 25 THEN 1 ELSE 0 END) R26
                          , SUM(CASE WHEN UMUR = 26 THEN 1 ELSE 0 END) R27
                          , SUM(CASE WHEN UMUR = 27 THEN 1 ELSE 0 END) R28
                          , SUM(CASE WHEN UMUR = 28 THEN 1 ELSE 0 END) R29
                          , SUM(CASE WHEN UMUR = 29 THEN 1 ELSE 0 END) R30
                          , SUM(CASE WHEN UMUR = 30 THEN 1 ELSE 0 END) R31
                          , SUM(CASE WHEN UMUR = 31 THEN 1 ELSE 0 END) R32
                          , SUM(CASE WHEN UMUR = 32 THEN 1 ELSE 0 END) R33
                          , SUM(CASE WHEN UMUR = 33 THEN 1 ELSE 0 END) R34
                          , SUM(CASE WHEN UMUR = 34 THEN 1 ELSE 0 END) R35
                          , SUM(CASE WHEN UMUR = 35 THEN 1 ELSE 0 END) R36
                          , SUM(CASE WHEN UMUR = 36 THEN 1 ELSE 0 END) R37
                          , SUM(CASE WHEN UMUR = 37 THEN 1 ELSE 0 END) R38
                          , SUM(CASE WHEN UMUR = 38 THEN 1 ELSE 0 END) R39
                          , SUM(CASE WHEN UMUR = 39 THEN 1 ELSE 0 END) R40
                          , SUM(CASE WHEN UMUR = 40 THEN 1 ELSE 0 END) R41
                          , SUM(CASE WHEN UMUR = 41 THEN 1 ELSE 0 END) R42
                          , SUM(CASE WHEN UMUR = 42 THEN 1 ELSE 0 END) R43
                          , SUM(CASE WHEN UMUR = 43 THEN 1 ELSE 0 END) R44
                          , SUM(CASE WHEN UMUR = 44 THEN 1 ELSE 0 END) R45
                          , SUM(CASE WHEN UMUR = 45 THEN 1 ELSE 0 END) R46
                          , SUM(CASE WHEN UMUR = 46 THEN 1 ELSE 0 END) R47
                          , SUM(CASE WHEN UMUR = 47 THEN 1 ELSE 0 END) R48
                          , SUM(CASE WHEN UMUR = 48 THEN 1 ELSE 0 END) R49
                          , SUM(CASE WHEN UMUR = 49 THEN 1 ELSE 0 END) R50
                          , SUM(CASE WHEN UMUR = 50 THEN 1 ELSE 0 END) R51
                          , SUM(CASE WHEN UMUR = 51 THEN 1 ELSE 0 END) R52
                          , SUM(CASE WHEN UMUR = 52 THEN 1 ELSE 0 END) R53
                          , SUM(CASE WHEN UMUR = 53 THEN 1 ELSE 0 END) R54
                          , SUM(CASE WHEN UMUR = 54 THEN 1 ELSE 0 END) R55
                          , SUM(CASE WHEN UMUR = 55 THEN 1 ELSE 0 END) R56
                          , SUM(CASE WHEN UMUR = 56 THEN 1 ELSE 0 END) R57
                          , SUM(CASE WHEN UMUR = 57 THEN 1 ELSE 0 END) R58
                          , SUM(CASE WHEN UMUR = 58 THEN 1 ELSE 0 END) R59
                          , SUM(CASE WHEN UMUR = 59 THEN 1 ELSE 0 END) R60
                          , SUM(CASE WHEN UMUR = 60 THEN 1 ELSE 0 END) R61
                          , SUM(CASE WHEN UMUR = 61 THEN 1 ELSE 0 END) R62
                          , SUM(CASE WHEN UMUR = 62 THEN 1 ELSE 0 END) R63
                          , SUM(CASE WHEN UMUR = 63 THEN 1 ELSE 0 END) R64
                          , SUM(CASE WHEN UMUR = 64 THEN 1 ELSE 0 END) R65
                          , SUM(CASE WHEN UMUR = 65 THEN 1 ELSE 0 END) R66
                          , SUM(CASE WHEN UMUR = 66 THEN 1 ELSE 0 END) R67
                          , SUM(CASE WHEN UMUR = 67 THEN 1 ELSE 0 END) R68
                          , SUM(CASE WHEN UMUR = 68 THEN 1 ELSE 0 END) R69
                          , SUM(CASE WHEN UMUR = 69 THEN 1 ELSE 0 END) R70
                          , SUM(CASE WHEN UMUR = 70 THEN 1 ELSE 0 END) R71
                          , SUM(CASE WHEN UMUR = 71 THEN 1 ELSE 0 END) R72
                          , SUM(CASE WHEN UMUR = 72 THEN 1 ELSE 0 END) R73
                          , SUM(CASE WHEN UMUR = 73 THEN 1 ELSE 0 END) R74
                          , SUM(CASE WHEN UMUR = 74 THEN 1 ELSE 0 END) R75
                          , SUM(CASE WHEN UMUR = 75 THEN 1 ELSE 0 END) R76
                          , SUM(CASE WHEN UMUR = 76 THEN 1 ELSE 0 END) R77
                          , SUM(CASE WHEN UMUR = 77 THEN 1 ELSE 0 END) R78
                          , SUM(CASE WHEN UMUR = 78 THEN 1 ELSE 0 END) R79
                          , SUM(CASE WHEN UMUR = 79 THEN 1 ELSE 0 END) R80
                          , SUM(CASE WHEN UMUR = 80 THEN 1 ELSE 0 END) R81
                          , SUM(CASE WHEN UMUR >= 80 THEN 1 ELSE 0 END) REND
                          , COUNT(1) JUMLAH
                        FROM KAMPUNG_GISA_DKB WHERE 1=1 ";
						if($no_kel == 0){    
					$sql .=" AND NO_PROP = 32 AND NO_KAB = 73 AND NO_KEC =".$no_kec."
						    GROUP BY NO_KEL
						    ORDER BY NO_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND NO_PROP = 32 AND NO_KAB = 73 AND NO_KEC =".$no_kec." AND NO_KEL = ".$no_kel."
						    GROUP BY NO_KEL
						    ORDER BY NO_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::select($sql);
               return $r;
		}
		
		

		public function mlu_get_laporan_umur1_laki_laki($no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT 
					  A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL, B.R1,B.R2,B.R3,B.R4,B.R5,B.R6,B.R7,B.R8,B.R9,B.R10,B.R11,B.R12,B.R13,B.R14,B.R15,B.R16,B.R17,B.R18,B.R19,B.R20,B.R21,B.R22,B.R23,B.R24,B.R25,B.R26,B.R27,B.R28,B.R29,B.R30,B.R31,B.R32,B.R33,B.R34,B.R35,B.R36,B.R37,B.R38,B.R39,B.R40,B.R41,B.R42,B.R43,B.R44,B.R45,B.R46,B.R47,B.R48,B.R49,B.R50,B.R51,B.R52,B.R53,B.R54,B.R55,B.R56,B.R57,B.R58,B.R59,B.R60,B.R61,B.R62,B.R63,B.R64,B.R65,B.R66,B.R67,B.R68,B.R69,B.R70,B.R71,B.R72,B.R73,B.R74,B.R75,B.R76,B.R77,B.R78,B.R79,B.R80,B.REND,B.JUMLAH
					  FROM SETUP_KEC A LEFT JOIN (SELECT NO_KEC
                          , SUM(CASE WHEN UMUR = 0 THEN 1 ELSE 0 END) R1
                          , SUM(CASE WHEN UMUR = 1 THEN 1 ELSE 0 END) R2
                          , SUM(CASE WHEN UMUR = 2 THEN 1 ELSE 0 END) R3
                          , SUM(CASE WHEN UMUR = 3 THEN 1 ELSE 0 END) R4
                          , SUM(CASE WHEN UMUR = 4 THEN 1 ELSE 0 END) R5
                          , SUM(CASE WHEN UMUR = 5 THEN 1 ELSE 0 END) R6
                          , SUM(CASE WHEN UMUR = 6 THEN 1 ELSE 0 END) R7
                          , SUM(CASE WHEN UMUR = 7 THEN 1 ELSE 0 END) R8
                          , SUM(CASE WHEN UMUR = 8 THEN 1 ELSE 0 END) R9
                          , SUM(CASE WHEN UMUR = 9 THEN 1 ELSE 0 END) R10
                          , SUM(CASE WHEN UMUR = 10 THEN 1 ELSE 0 END) R11
                          , SUM(CASE WHEN UMUR = 11 THEN 1 ELSE 0 END) R12
                          , SUM(CASE WHEN UMUR = 12 THEN 1 ELSE 0 END) R13
                          , SUM(CASE WHEN UMUR = 13 THEN 1 ELSE 0 END) R14
                          , SUM(CASE WHEN UMUR = 14 THEN 1 ELSE 0 END) R15
                          , SUM(CASE WHEN UMUR = 15 THEN 1 ELSE 0 END) R16
                          , SUM(CASE WHEN UMUR = 16 THEN 1 ELSE 0 END) R17
                          , SUM(CASE WHEN UMUR = 17 THEN 1 ELSE 0 END) R18
                          , SUM(CASE WHEN UMUR = 18 THEN 1 ELSE 0 END) R19
                          , SUM(CASE WHEN UMUR = 19 THEN 1 ELSE 0 END) R20
                          , SUM(CASE WHEN UMUR = 20 THEN 1 ELSE 0 END) R21
                          , SUM(CASE WHEN UMUR = 21 THEN 1 ELSE 0 END) R22
                          , SUM(CASE WHEN UMUR = 22 THEN 1 ELSE 0 END) R23
                          , SUM(CASE WHEN UMUR = 23 THEN 1 ELSE 0 END) R24
                          , SUM(CASE WHEN UMUR = 24 THEN 1 ELSE 0 END) R25
                          , SUM(CASE WHEN UMUR = 25 THEN 1 ELSE 0 END) R26
                          , SUM(CASE WHEN UMUR = 26 THEN 1 ELSE 0 END) R27
                          , SUM(CASE WHEN UMUR = 27 THEN 1 ELSE 0 END) R28
                          , SUM(CASE WHEN UMUR = 28 THEN 1 ELSE 0 END) R29
                          , SUM(CASE WHEN UMUR = 29 THEN 1 ELSE 0 END) R30
                          , SUM(CASE WHEN UMUR = 30 THEN 1 ELSE 0 END) R31
                          , SUM(CASE WHEN UMUR = 31 THEN 1 ELSE 0 END) R32
                          , SUM(CASE WHEN UMUR = 32 THEN 1 ELSE 0 END) R33
                          , SUM(CASE WHEN UMUR = 33 THEN 1 ELSE 0 END) R34
                          , SUM(CASE WHEN UMUR = 34 THEN 1 ELSE 0 END) R35
                          , SUM(CASE WHEN UMUR = 35 THEN 1 ELSE 0 END) R36
                          , SUM(CASE WHEN UMUR = 36 THEN 1 ELSE 0 END) R37
                          , SUM(CASE WHEN UMUR = 37 THEN 1 ELSE 0 END) R38
                          , SUM(CASE WHEN UMUR = 38 THEN 1 ELSE 0 END) R39
                          , SUM(CASE WHEN UMUR = 39 THEN 1 ELSE 0 END) R40
                          , SUM(CASE WHEN UMUR = 40 THEN 1 ELSE 0 END) R41
                          , SUM(CASE WHEN UMUR = 41 THEN 1 ELSE 0 END) R42
                          , SUM(CASE WHEN UMUR = 42 THEN 1 ELSE 0 END) R43
                          , SUM(CASE WHEN UMUR = 43 THEN 1 ELSE 0 END) R44
                          , SUM(CASE WHEN UMUR = 44 THEN 1 ELSE 0 END) R45
                          , SUM(CASE WHEN UMUR = 45 THEN 1 ELSE 0 END) R46
                          , SUM(CASE WHEN UMUR = 46 THEN 1 ELSE 0 END) R47
                          , SUM(CASE WHEN UMUR = 47 THEN 1 ELSE 0 END) R48
                          , SUM(CASE WHEN UMUR = 48 THEN 1 ELSE 0 END) R49
                          , SUM(CASE WHEN UMUR = 49 THEN 1 ELSE 0 END) R50
                          , SUM(CASE WHEN UMUR = 50 THEN 1 ELSE 0 END) R51
                          , SUM(CASE WHEN UMUR = 51 THEN 1 ELSE 0 END) R52
                          , SUM(CASE WHEN UMUR = 52 THEN 1 ELSE 0 END) R53
                          , SUM(CASE WHEN UMUR = 53 THEN 1 ELSE 0 END) R54
                          , SUM(CASE WHEN UMUR = 54 THEN 1 ELSE 0 END) R55
                          , SUM(CASE WHEN UMUR = 55 THEN 1 ELSE 0 END) R56
                          , SUM(CASE WHEN UMUR = 56 THEN 1 ELSE 0 END) R57
                          , SUM(CASE WHEN UMUR = 57 THEN 1 ELSE 0 END) R58
                          , SUM(CASE WHEN UMUR = 58 THEN 1 ELSE 0 END) R59
                          , SUM(CASE WHEN UMUR = 59 THEN 1 ELSE 0 END) R60
                          , SUM(CASE WHEN UMUR = 60 THEN 1 ELSE 0 END) R61
                          , SUM(CASE WHEN UMUR = 61 THEN 1 ELSE 0 END) R62
                          , SUM(CASE WHEN UMUR = 62 THEN 1 ELSE 0 END) R63
                          , SUM(CASE WHEN UMUR = 63 THEN 1 ELSE 0 END) R64
                          , SUM(CASE WHEN UMUR = 64 THEN 1 ELSE 0 END) R65
                          , SUM(CASE WHEN UMUR = 65 THEN 1 ELSE 0 END) R66
                          , SUM(CASE WHEN UMUR = 66 THEN 1 ELSE 0 END) R67
                          , SUM(CASE WHEN UMUR = 67 THEN 1 ELSE 0 END) R68
                          , SUM(CASE WHEN UMUR = 68 THEN 1 ELSE 0 END) R69
                          , SUM(CASE WHEN UMUR = 69 THEN 1 ELSE 0 END) R70
                          , SUM(CASE WHEN UMUR = 70 THEN 1 ELSE 0 END) R71
                          , SUM(CASE WHEN UMUR = 71 THEN 1 ELSE 0 END) R72
                          , SUM(CASE WHEN UMUR = 72 THEN 1 ELSE 0 END) R73
                          , SUM(CASE WHEN UMUR = 73 THEN 1 ELSE 0 END) R74
                          , SUM(CASE WHEN UMUR = 74 THEN 1 ELSE 0 END) R75
                          , SUM(CASE WHEN UMUR = 75 THEN 1 ELSE 0 END) R76
                          , SUM(CASE WHEN UMUR = 76 THEN 1 ELSE 0 END) R77
                          , SUM(CASE WHEN UMUR = 77 THEN 1 ELSE 0 END) R78
                          , SUM(CASE WHEN UMUR = 78 THEN 1 ELSE 0 END) R79
                          , SUM(CASE WHEN UMUR = 79 THEN 1 ELSE 0 END) R80
                          , SUM(CASE WHEN UMUR = 80 THEN 1 ELSE 0 END) R81
                          , SUM(CASE WHEN UMUR >= 80 THEN 1 ELSE 0 END) REND
                          , COUNT(1) JUMLAH
                        FROM KAMPUNG_GISA_DKB WHERE 1=1 AND JENIS_KELAMIN = 1 
					GROUP BY NO_KEC
					ORDER BY NO_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT 
					  A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL, B.R1,B.R2,B.R3,B.R4,B.R5,B.R6,B.R7,B.R8,B.R9,B.R10,B.R11,B.R12,B.R13,B.R14,B.R15,B.R16,B.R17,B.R18,B.R19,B.R20,B.R21,B.R22,B.R23,B.R24,B.R25,B.R26,B.R27,B.R28,B.R29,B.R30,B.R31,B.R32,B.R33,B.R34,B.R35,B.R36,B.R37,B.R38,B.R39,B.R40,B.R41,B.R42,B.R43,B.R44,B.R45,B.R46,B.R47,B.R48,B.R49,B.R50,B.R51,B.R52,B.R53,B.R54,B.R55,B.R56,B.R57,B.R58,B.R59,B.R60,B.R61,B.R62,B.R63,B.R64,B.R65,B.R66,B.R67,B.R68,B.R69,B.R70,B.R71,B.R72,B.R73,B.R74,B.R75,B.R76,B.R77,B.R78,B.R79,B.R80,B.REND,B.JUMLAH
					  FROM SETUP_KEL A LEFT JOIN (SELECT NO_KEL
                          , SUM(CASE WHEN UMUR = 0 THEN 1 ELSE 0 END) R1
                          , SUM(CASE WHEN UMUR = 1 THEN 1 ELSE 0 END) R2
                          , SUM(CASE WHEN UMUR = 2 THEN 1 ELSE 0 END) R3
                          , SUM(CASE WHEN UMUR = 3 THEN 1 ELSE 0 END) R4
                          , SUM(CASE WHEN UMUR = 4 THEN 1 ELSE 0 END) R5
                          , SUM(CASE WHEN UMUR = 5 THEN 1 ELSE 0 END) R6
                          , SUM(CASE WHEN UMUR = 6 THEN 1 ELSE 0 END) R7
                          , SUM(CASE WHEN UMUR = 7 THEN 1 ELSE 0 END) R8
                          , SUM(CASE WHEN UMUR = 8 THEN 1 ELSE 0 END) R9
                          , SUM(CASE WHEN UMUR = 9 THEN 1 ELSE 0 END) R10
                          , SUM(CASE WHEN UMUR = 10 THEN 1 ELSE 0 END) R11
                          , SUM(CASE WHEN UMUR = 11 THEN 1 ELSE 0 END) R12
                          , SUM(CASE WHEN UMUR = 12 THEN 1 ELSE 0 END) R13
                          , SUM(CASE WHEN UMUR = 13 THEN 1 ELSE 0 END) R14
                          , SUM(CASE WHEN UMUR = 14 THEN 1 ELSE 0 END) R15
                          , SUM(CASE WHEN UMUR = 15 THEN 1 ELSE 0 END) R16
                          , SUM(CASE WHEN UMUR = 16 THEN 1 ELSE 0 END) R17
                          , SUM(CASE WHEN UMUR = 17 THEN 1 ELSE 0 END) R18
                          , SUM(CASE WHEN UMUR = 18 THEN 1 ELSE 0 END) R19
                          , SUM(CASE WHEN UMUR = 19 THEN 1 ELSE 0 END) R20
                          , SUM(CASE WHEN UMUR = 20 THEN 1 ELSE 0 END) R21
                          , SUM(CASE WHEN UMUR = 21 THEN 1 ELSE 0 END) R22
                          , SUM(CASE WHEN UMUR = 22 THEN 1 ELSE 0 END) R23
                          , SUM(CASE WHEN UMUR = 23 THEN 1 ELSE 0 END) R24
                          , SUM(CASE WHEN UMUR = 24 THEN 1 ELSE 0 END) R25
                          , SUM(CASE WHEN UMUR = 25 THEN 1 ELSE 0 END) R26
                          , SUM(CASE WHEN UMUR = 26 THEN 1 ELSE 0 END) R27
                          , SUM(CASE WHEN UMUR = 27 THEN 1 ELSE 0 END) R28
                          , SUM(CASE WHEN UMUR = 28 THEN 1 ELSE 0 END) R29
                          , SUM(CASE WHEN UMUR = 29 THEN 1 ELSE 0 END) R30
                          , SUM(CASE WHEN UMUR = 30 THEN 1 ELSE 0 END) R31
                          , SUM(CASE WHEN UMUR = 31 THEN 1 ELSE 0 END) R32
                          , SUM(CASE WHEN UMUR = 32 THEN 1 ELSE 0 END) R33
                          , SUM(CASE WHEN UMUR = 33 THEN 1 ELSE 0 END) R34
                          , SUM(CASE WHEN UMUR = 34 THEN 1 ELSE 0 END) R35
                          , SUM(CASE WHEN UMUR = 35 THEN 1 ELSE 0 END) R36
                          , SUM(CASE WHEN UMUR = 36 THEN 1 ELSE 0 END) R37
                          , SUM(CASE WHEN UMUR = 37 THEN 1 ELSE 0 END) R38
                          , SUM(CASE WHEN UMUR = 38 THEN 1 ELSE 0 END) R39
                          , SUM(CASE WHEN UMUR = 39 THEN 1 ELSE 0 END) R40
                          , SUM(CASE WHEN UMUR = 40 THEN 1 ELSE 0 END) R41
                          , SUM(CASE WHEN UMUR = 41 THEN 1 ELSE 0 END) R42
                          , SUM(CASE WHEN UMUR = 42 THEN 1 ELSE 0 END) R43
                          , SUM(CASE WHEN UMUR = 43 THEN 1 ELSE 0 END) R44
                          , SUM(CASE WHEN UMUR = 44 THEN 1 ELSE 0 END) R45
                          , SUM(CASE WHEN UMUR = 45 THEN 1 ELSE 0 END) R46
                          , SUM(CASE WHEN UMUR = 46 THEN 1 ELSE 0 END) R47
                          , SUM(CASE WHEN UMUR = 47 THEN 1 ELSE 0 END) R48
                          , SUM(CASE WHEN UMUR = 48 THEN 1 ELSE 0 END) R49
                          , SUM(CASE WHEN UMUR = 49 THEN 1 ELSE 0 END) R50
                          , SUM(CASE WHEN UMUR = 50 THEN 1 ELSE 0 END) R51
                          , SUM(CASE WHEN UMUR = 51 THEN 1 ELSE 0 END) R52
                          , SUM(CASE WHEN UMUR = 52 THEN 1 ELSE 0 END) R53
                          , SUM(CASE WHEN UMUR = 53 THEN 1 ELSE 0 END) R54
                          , SUM(CASE WHEN UMUR = 54 THEN 1 ELSE 0 END) R55
                          , SUM(CASE WHEN UMUR = 55 THEN 1 ELSE 0 END) R56
                          , SUM(CASE WHEN UMUR = 56 THEN 1 ELSE 0 END) R57
                          , SUM(CASE WHEN UMUR = 57 THEN 1 ELSE 0 END) R58
                          , SUM(CASE WHEN UMUR = 58 THEN 1 ELSE 0 END) R59
                          , SUM(CASE WHEN UMUR = 59 THEN 1 ELSE 0 END) R60
                          , SUM(CASE WHEN UMUR = 60 THEN 1 ELSE 0 END) R61
                          , SUM(CASE WHEN UMUR = 61 THEN 1 ELSE 0 END) R62
                          , SUM(CASE WHEN UMUR = 62 THEN 1 ELSE 0 END) R63
                          , SUM(CASE WHEN UMUR = 63 THEN 1 ELSE 0 END) R64
                          , SUM(CASE WHEN UMUR = 64 THEN 1 ELSE 0 END) R65
                          , SUM(CASE WHEN UMUR = 65 THEN 1 ELSE 0 END) R66
                          , SUM(CASE WHEN UMUR = 66 THEN 1 ELSE 0 END) R67
                          , SUM(CASE WHEN UMUR = 67 THEN 1 ELSE 0 END) R68
                          , SUM(CASE WHEN UMUR = 68 THEN 1 ELSE 0 END) R69
                          , SUM(CASE WHEN UMUR = 69 THEN 1 ELSE 0 END) R70
                          , SUM(CASE WHEN UMUR = 70 THEN 1 ELSE 0 END) R71
                          , SUM(CASE WHEN UMUR = 71 THEN 1 ELSE 0 END) R72
                          , SUM(CASE WHEN UMUR = 72 THEN 1 ELSE 0 END) R73
                          , SUM(CASE WHEN UMUR = 73 THEN 1 ELSE 0 END) R74
                          , SUM(CASE WHEN UMUR = 74 THEN 1 ELSE 0 END) R75
                          , SUM(CASE WHEN UMUR = 75 THEN 1 ELSE 0 END) R76
                          , SUM(CASE WHEN UMUR = 76 THEN 1 ELSE 0 END) R77
                          , SUM(CASE WHEN UMUR = 77 THEN 1 ELSE 0 END) R78
                          , SUM(CASE WHEN UMUR = 78 THEN 1 ELSE 0 END) R79
                          , SUM(CASE WHEN UMUR = 79 THEN 1 ELSE 0 END) R80
                          , SUM(CASE WHEN UMUR = 80 THEN 1 ELSE 0 END) R81
                          , SUM(CASE WHEN UMUR >= 80 THEN 1 ELSE 0 END) REND
                          , COUNT(1) JUMLAH
                        FROM KAMPUNG_GISA_DKB WHERE 1=1 AND JENIS_KELAMIN = 1  ";
						if($no_kel == 0){    
					$sql .=" AND NO_PROP = 32 AND NO_KAB = 73 AND NO_KEC =".$no_kec."
						    GROUP BY NO_KEL
						    ORDER BY NO_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND NO_PROP = 32 AND NO_KAB = 73 AND NO_KEC =".$no_kec." AND NO_KEL = ".$no_kel."
						    GROUP BY NO_KEL
						    ORDER BY NO_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::select($sql);
               return $r;
		}
		
		

		public function mlu_get_laporan_umur1_perempuan($no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT 
					  A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL, B.R1,B.R2,B.R3,B.R4,B.R5,B.R6,B.R7,B.R8,B.R9,B.R10,B.R11,B.R12,B.R13,B.R14,B.R15,B.R16,B.R17,B.R18,B.R19,B.R20,B.R21,B.R22,B.R23,B.R24,B.R25,B.R26,B.R27,B.R28,B.R29,B.R30,B.R31,B.R32,B.R33,B.R34,B.R35,B.R36,B.R37,B.R38,B.R39,B.R40,B.R41,B.R42,B.R43,B.R44,B.R45,B.R46,B.R47,B.R48,B.R49,B.R50,B.R51,B.R52,B.R53,B.R54,B.R55,B.R56,B.R57,B.R58,B.R59,B.R60,B.R61,B.R62,B.R63,B.R64,B.R65,B.R66,B.R67,B.R68,B.R69,B.R70,B.R71,B.R72,B.R73,B.R74,B.R75,B.R76,B.R77,B.R78,B.R79,B.R80,B.REND,B.JUMLAH
					  FROM SETUP_KEC A LEFT JOIN (SELECT NO_KEC
                          , SUM(CASE WHEN UMUR = 0 THEN 1 ELSE 0 END) R1
                          , SUM(CASE WHEN UMUR = 1 THEN 1 ELSE 0 END) R2
                          , SUM(CASE WHEN UMUR = 2 THEN 1 ELSE 0 END) R3
                          , SUM(CASE WHEN UMUR = 3 THEN 1 ELSE 0 END) R4
                          , SUM(CASE WHEN UMUR = 4 THEN 1 ELSE 0 END) R5
                          , SUM(CASE WHEN UMUR = 5 THEN 1 ELSE 0 END) R6
                          , SUM(CASE WHEN UMUR = 6 THEN 1 ELSE 0 END) R7
                          , SUM(CASE WHEN UMUR = 7 THEN 1 ELSE 0 END) R8
                          , SUM(CASE WHEN UMUR = 8 THEN 1 ELSE 0 END) R9
                          , SUM(CASE WHEN UMUR = 9 THEN 1 ELSE 0 END) R10
                          , SUM(CASE WHEN UMUR = 10 THEN 1 ELSE 0 END) R11
                          , SUM(CASE WHEN UMUR = 11 THEN 1 ELSE 0 END) R12
                          , SUM(CASE WHEN UMUR = 12 THEN 1 ELSE 0 END) R13
                          , SUM(CASE WHEN UMUR = 13 THEN 1 ELSE 0 END) R14
                          , SUM(CASE WHEN UMUR = 14 THEN 1 ELSE 0 END) R15
                          , SUM(CASE WHEN UMUR = 15 THEN 1 ELSE 0 END) R16
                          , SUM(CASE WHEN UMUR = 16 THEN 1 ELSE 0 END) R17
                          , SUM(CASE WHEN UMUR = 17 THEN 1 ELSE 0 END) R18
                          , SUM(CASE WHEN UMUR = 18 THEN 1 ELSE 0 END) R19
                          , SUM(CASE WHEN UMUR = 19 THEN 1 ELSE 0 END) R20
                          , SUM(CASE WHEN UMUR = 20 THEN 1 ELSE 0 END) R21
                          , SUM(CASE WHEN UMUR = 21 THEN 1 ELSE 0 END) R22
                          , SUM(CASE WHEN UMUR = 22 THEN 1 ELSE 0 END) R23
                          , SUM(CASE WHEN UMUR = 23 THEN 1 ELSE 0 END) R24
                          , SUM(CASE WHEN UMUR = 24 THEN 1 ELSE 0 END) R25
                          , SUM(CASE WHEN UMUR = 25 THEN 1 ELSE 0 END) R26
                          , SUM(CASE WHEN UMUR = 26 THEN 1 ELSE 0 END) R27
                          , SUM(CASE WHEN UMUR = 27 THEN 1 ELSE 0 END) R28
                          , SUM(CASE WHEN UMUR = 28 THEN 1 ELSE 0 END) R29
                          , SUM(CASE WHEN UMUR = 29 THEN 1 ELSE 0 END) R30
                          , SUM(CASE WHEN UMUR = 30 THEN 1 ELSE 0 END) R31
                          , SUM(CASE WHEN UMUR = 31 THEN 1 ELSE 0 END) R32
                          , SUM(CASE WHEN UMUR = 32 THEN 1 ELSE 0 END) R33
                          , SUM(CASE WHEN UMUR = 33 THEN 1 ELSE 0 END) R34
                          , SUM(CASE WHEN UMUR = 34 THEN 1 ELSE 0 END) R35
                          , SUM(CASE WHEN UMUR = 35 THEN 1 ELSE 0 END) R36
                          , SUM(CASE WHEN UMUR = 36 THEN 1 ELSE 0 END) R37
                          , SUM(CASE WHEN UMUR = 37 THEN 1 ELSE 0 END) R38
                          , SUM(CASE WHEN UMUR = 38 THEN 1 ELSE 0 END) R39
                          , SUM(CASE WHEN UMUR = 39 THEN 1 ELSE 0 END) R40
                          , SUM(CASE WHEN UMUR = 40 THEN 1 ELSE 0 END) R41
                          , SUM(CASE WHEN UMUR = 41 THEN 1 ELSE 0 END) R42
                          , SUM(CASE WHEN UMUR = 42 THEN 1 ELSE 0 END) R43
                          , SUM(CASE WHEN UMUR = 43 THEN 1 ELSE 0 END) R44
                          , SUM(CASE WHEN UMUR = 44 THEN 1 ELSE 0 END) R45
                          , SUM(CASE WHEN UMUR = 45 THEN 1 ELSE 0 END) R46
                          , SUM(CASE WHEN UMUR = 46 THEN 1 ELSE 0 END) R47
                          , SUM(CASE WHEN UMUR = 47 THEN 1 ELSE 0 END) R48
                          , SUM(CASE WHEN UMUR = 48 THEN 1 ELSE 0 END) R49
                          , SUM(CASE WHEN UMUR = 49 THEN 1 ELSE 0 END) R50
                          , SUM(CASE WHEN UMUR = 50 THEN 1 ELSE 0 END) R51
                          , SUM(CASE WHEN UMUR = 51 THEN 1 ELSE 0 END) R52
                          , SUM(CASE WHEN UMUR = 52 THEN 1 ELSE 0 END) R53
                          , SUM(CASE WHEN UMUR = 53 THEN 1 ELSE 0 END) R54
                          , SUM(CASE WHEN UMUR = 54 THEN 1 ELSE 0 END) R55
                          , SUM(CASE WHEN UMUR = 55 THEN 1 ELSE 0 END) R56
                          , SUM(CASE WHEN UMUR = 56 THEN 1 ELSE 0 END) R57
                          , SUM(CASE WHEN UMUR = 57 THEN 1 ELSE 0 END) R58
                          , SUM(CASE WHEN UMUR = 58 THEN 1 ELSE 0 END) R59
                          , SUM(CASE WHEN UMUR = 59 THEN 1 ELSE 0 END) R60
                          , SUM(CASE WHEN UMUR = 60 THEN 1 ELSE 0 END) R61
                          , SUM(CASE WHEN UMUR = 61 THEN 1 ELSE 0 END) R62
                          , SUM(CASE WHEN UMUR = 62 THEN 1 ELSE 0 END) R63
                          , SUM(CASE WHEN UMUR = 63 THEN 1 ELSE 0 END) R64
                          , SUM(CASE WHEN UMUR = 64 THEN 1 ELSE 0 END) R65
                          , SUM(CASE WHEN UMUR = 65 THEN 1 ELSE 0 END) R66
                          , SUM(CASE WHEN UMUR = 66 THEN 1 ELSE 0 END) R67
                          , SUM(CASE WHEN UMUR = 67 THEN 1 ELSE 0 END) R68
                          , SUM(CASE WHEN UMUR = 68 THEN 1 ELSE 0 END) R69
                          , SUM(CASE WHEN UMUR = 69 THEN 1 ELSE 0 END) R70
                          , SUM(CASE WHEN UMUR = 70 THEN 1 ELSE 0 END) R71
                          , SUM(CASE WHEN UMUR = 71 THEN 1 ELSE 0 END) R72
                          , SUM(CASE WHEN UMUR = 72 THEN 1 ELSE 0 END) R73
                          , SUM(CASE WHEN UMUR = 73 THEN 1 ELSE 0 END) R74
                          , SUM(CASE WHEN UMUR = 74 THEN 1 ELSE 0 END) R75
                          , SUM(CASE WHEN UMUR = 75 THEN 1 ELSE 0 END) R76
                          , SUM(CASE WHEN UMUR = 76 THEN 1 ELSE 0 END) R77
                          , SUM(CASE WHEN UMUR = 77 THEN 1 ELSE 0 END) R78
                          , SUM(CASE WHEN UMUR = 78 THEN 1 ELSE 0 END) R79
                          , SUM(CASE WHEN UMUR = 79 THEN 1 ELSE 0 END) R80
                          , SUM(CASE WHEN UMUR = 80 THEN 1 ELSE 0 END) R81
                          , SUM(CASE WHEN UMUR >= 80 THEN 1 ELSE 0 END) REND
                          , COUNT(1) JUMLAH
                        FROM KAMPUNG_GISA_DKB WHERE 1=1 AND JENIS_KELAMIN = 2
					GROUP BY NO_KEC
					ORDER BY NO_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT 
					  A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL, B.R1,B.R2,B.R3,B.R4,B.R5,B.R6,B.R7,B.R8,B.R9,B.R10,B.R11,B.R12,B.R13,B.R14,B.R15,B.R16,B.R17,B.R18,B.R19,B.R20,B.R21,B.R22,B.R23,B.R24,B.R25,B.R26,B.R27,B.R28,B.R29,B.R30,B.R31,B.R32,B.R33,B.R34,B.R35,B.R36,B.R37,B.R38,B.R39,B.R40,B.R41,B.R42,B.R43,B.R44,B.R45,B.R46,B.R47,B.R48,B.R49,B.R50,B.R51,B.R52,B.R53,B.R54,B.R55,B.R56,B.R57,B.R58,B.R59,B.R60,B.R61,B.R62,B.R63,B.R64,B.R65,B.R66,B.R67,B.R68,B.R69,B.R70,B.R71,B.R72,B.R73,B.R74,B.R75,B.R76,B.R77,B.R78,B.R79,B.R80,B.REND,B.JUMLAH
					  FROM SETUP_KEL A LEFT JOIN (SELECT NO_KEL
                          , SUM(CASE WHEN UMUR = 0 THEN 1 ELSE 0 END) R1
                          , SUM(CASE WHEN UMUR = 1 THEN 1 ELSE 0 END) R2
                          , SUM(CASE WHEN UMUR = 2 THEN 1 ELSE 0 END) R3
                          , SUM(CASE WHEN UMUR = 3 THEN 1 ELSE 0 END) R4
                          , SUM(CASE WHEN UMUR = 4 THEN 1 ELSE 0 END) R5
                          , SUM(CASE WHEN UMUR = 5 THEN 1 ELSE 0 END) R6
                          , SUM(CASE WHEN UMUR = 6 THEN 1 ELSE 0 END) R7
                          , SUM(CASE WHEN UMUR = 7 THEN 1 ELSE 0 END) R8
                          , SUM(CASE WHEN UMUR = 8 THEN 1 ELSE 0 END) R9
                          , SUM(CASE WHEN UMUR = 9 THEN 1 ELSE 0 END) R10
                          , SUM(CASE WHEN UMUR = 10 THEN 1 ELSE 0 END) R11
                          , SUM(CASE WHEN UMUR = 11 THEN 1 ELSE 0 END) R12
                          , SUM(CASE WHEN UMUR = 12 THEN 1 ELSE 0 END) R13
                          , SUM(CASE WHEN UMUR = 13 THEN 1 ELSE 0 END) R14
                          , SUM(CASE WHEN UMUR = 14 THEN 1 ELSE 0 END) R15
                          , SUM(CASE WHEN UMUR = 15 THEN 1 ELSE 0 END) R16
                          , SUM(CASE WHEN UMUR = 16 THEN 1 ELSE 0 END) R17
                          , SUM(CASE WHEN UMUR = 17 THEN 1 ELSE 0 END) R18
                          , SUM(CASE WHEN UMUR = 18 THEN 1 ELSE 0 END) R19
                          , SUM(CASE WHEN UMUR = 19 THEN 1 ELSE 0 END) R20
                          , SUM(CASE WHEN UMUR = 20 THEN 1 ELSE 0 END) R21
                          , SUM(CASE WHEN UMUR = 21 THEN 1 ELSE 0 END) R22
                          , SUM(CASE WHEN UMUR = 22 THEN 1 ELSE 0 END) R23
                          , SUM(CASE WHEN UMUR = 23 THEN 1 ELSE 0 END) R24
                          , SUM(CASE WHEN UMUR = 24 THEN 1 ELSE 0 END) R25
                          , SUM(CASE WHEN UMUR = 25 THEN 1 ELSE 0 END) R26
                          , SUM(CASE WHEN UMUR = 26 THEN 1 ELSE 0 END) R27
                          , SUM(CASE WHEN UMUR = 27 THEN 1 ELSE 0 END) R28
                          , SUM(CASE WHEN UMUR = 28 THEN 1 ELSE 0 END) R29
                          , SUM(CASE WHEN UMUR = 29 THEN 1 ELSE 0 END) R30
                          , SUM(CASE WHEN UMUR = 30 THEN 1 ELSE 0 END) R31
                          , SUM(CASE WHEN UMUR = 31 THEN 1 ELSE 0 END) R32
                          , SUM(CASE WHEN UMUR = 32 THEN 1 ELSE 0 END) R33
                          , SUM(CASE WHEN UMUR = 33 THEN 1 ELSE 0 END) R34
                          , SUM(CASE WHEN UMUR = 34 THEN 1 ELSE 0 END) R35
                          , SUM(CASE WHEN UMUR = 35 THEN 1 ELSE 0 END) R36
                          , SUM(CASE WHEN UMUR = 36 THEN 1 ELSE 0 END) R37
                          , SUM(CASE WHEN UMUR = 37 THEN 1 ELSE 0 END) R38
                          , SUM(CASE WHEN UMUR = 38 THEN 1 ELSE 0 END) R39
                          , SUM(CASE WHEN UMUR = 39 THEN 1 ELSE 0 END) R40
                          , SUM(CASE WHEN UMUR = 40 THEN 1 ELSE 0 END) R41
                          , SUM(CASE WHEN UMUR = 41 THEN 1 ELSE 0 END) R42
                          , SUM(CASE WHEN UMUR = 42 THEN 1 ELSE 0 END) R43
                          , SUM(CASE WHEN UMUR = 43 THEN 1 ELSE 0 END) R44
                          , SUM(CASE WHEN UMUR = 44 THEN 1 ELSE 0 END) R45
                          , SUM(CASE WHEN UMUR = 45 THEN 1 ELSE 0 END) R46
                          , SUM(CASE WHEN UMUR = 46 THEN 1 ELSE 0 END) R47
                          , SUM(CASE WHEN UMUR = 47 THEN 1 ELSE 0 END) R48
                          , SUM(CASE WHEN UMUR = 48 THEN 1 ELSE 0 END) R49
                          , SUM(CASE WHEN UMUR = 49 THEN 1 ELSE 0 END) R50
                          , SUM(CASE WHEN UMUR = 50 THEN 1 ELSE 0 END) R51
                          , SUM(CASE WHEN UMUR = 51 THEN 1 ELSE 0 END) R52
                          , SUM(CASE WHEN UMUR = 52 THEN 1 ELSE 0 END) R53
                          , SUM(CASE WHEN UMUR = 53 THEN 1 ELSE 0 END) R54
                          , SUM(CASE WHEN UMUR = 54 THEN 1 ELSE 0 END) R55
                          , SUM(CASE WHEN UMUR = 55 THEN 1 ELSE 0 END) R56
                          , SUM(CASE WHEN UMUR = 56 THEN 1 ELSE 0 END) R57
                          , SUM(CASE WHEN UMUR = 57 THEN 1 ELSE 0 END) R58
                          , SUM(CASE WHEN UMUR = 58 THEN 1 ELSE 0 END) R59
                          , SUM(CASE WHEN UMUR = 59 THEN 1 ELSE 0 END) R60
                          , SUM(CASE WHEN UMUR = 60 THEN 1 ELSE 0 END) R61
                          , SUM(CASE WHEN UMUR = 61 THEN 1 ELSE 0 END) R62
                          , SUM(CASE WHEN UMUR = 62 THEN 1 ELSE 0 END) R63
                          , SUM(CASE WHEN UMUR = 63 THEN 1 ELSE 0 END) R64
                          , SUM(CASE WHEN UMUR = 64 THEN 1 ELSE 0 END) R65
                          , SUM(CASE WHEN UMUR = 65 THEN 1 ELSE 0 END) R66
                          , SUM(CASE WHEN UMUR = 66 THEN 1 ELSE 0 END) R67
                          , SUM(CASE WHEN UMUR = 67 THEN 1 ELSE 0 END) R68
                          , SUM(CASE WHEN UMUR = 68 THEN 1 ELSE 0 END) R69
                          , SUM(CASE WHEN UMUR = 69 THEN 1 ELSE 0 END) R70
                          , SUM(CASE WHEN UMUR = 70 THEN 1 ELSE 0 END) R71
                          , SUM(CASE WHEN UMUR = 71 THEN 1 ELSE 0 END) R72
                          , SUM(CASE WHEN UMUR = 72 THEN 1 ELSE 0 END) R73
                          , SUM(CASE WHEN UMUR = 73 THEN 1 ELSE 0 END) R74
                          , SUM(CASE WHEN UMUR = 74 THEN 1 ELSE 0 END) R75
                          , SUM(CASE WHEN UMUR = 75 THEN 1 ELSE 0 END) R76
                          , SUM(CASE WHEN UMUR = 76 THEN 1 ELSE 0 END) R77
                          , SUM(CASE WHEN UMUR = 77 THEN 1 ELSE 0 END) R78
                          , SUM(CASE WHEN UMUR = 78 THEN 1 ELSE 0 END) R79
                          , SUM(CASE WHEN UMUR = 79 THEN 1 ELSE 0 END) R80
                          , SUM(CASE WHEN UMUR = 80 THEN 1 ELSE 0 END) R81
                          , SUM(CASE WHEN UMUR >= 80 THEN 1 ELSE 0 END) REND
                          , COUNT(1) JUMLAH
                        FROM KAMPUNG_GISA_DKB WHERE 1=1 AND JENIS_KELAMIN = 2 ";
						if($no_kel == 0){    
					$sql .=" AND NO_PROP = 32 AND NO_KAB = 73 AND NO_KEC =".$no_kec."
						    GROUP BY NO_KEL
						    ORDER BY NO_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND NO_PROP = 32 AND NO_KAB = 73 AND NO_KEC =".$no_kec." AND NO_KEL = ".$no_kel."
						    GROUP BY NO_KEL
						    ORDER BY NO_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::select($sql);
               return $r;
		}
		
		
		// usia sekolah tahun
		public function mlu_get_laporan_umur_sekolah($no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT 
					  A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL, B.R1 , B.R2 , B.R3, B.R4 , B.R5, (B.R1 + B.R2 + B.R3+ B.R4 + B.R5) JUMLAH
					  FROM SETUP_KEC A LEFT JOIN (SELECT NO_KEC
                          , SUM(CASE WHEN UMUR >= 0 AND UMUR <= 4 THEN 1 ELSE 0 END) R1
                          , SUM(CASE WHEN UMUR >= 5 AND UMUR <= 6 THEN 1 ELSE 0 END) R2
                          , SUM(CASE WHEN UMUR >= 7 AND UMUR <= 12 THEN 1 ELSE 0 END) R3
                          , SUM(CASE WHEN UMUR >= 13 AND UMUR <= 15 THEN 1 ELSE 0 END) R4
                          , SUM(CASE WHEN UMUR >= 16 AND UMUR <= 18 THEN 1 ELSE 0 END) R5
                        FROM KAMPUNG_GISA_DKB WHERE 1=1 
					GROUP BY NO_KEC
					ORDER BY NO_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT 
					  A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL, B.R1 , B.R2 , B.R3, B.R4 , B.R5, (B.R1 + B.R2 + B.R3+ B.R4 + B.R5) JUMLAH
					  FROM SETUP_KEL A LEFT JOIN (SELECT NO_KEL
                          , SUM(CASE WHEN UMUR >= 0 AND UMUR <= 4 THEN 1 ELSE 0 END) R1
                          , SUM(CASE WHEN UMUR >= 5 AND UMUR <= 6 THEN 1 ELSE 0 END) R2
                          , SUM(CASE WHEN UMUR >= 7 AND UMUR <= 12 THEN 1 ELSE 0 END) R3
                          , SUM(CASE WHEN UMUR >= 13 AND UMUR <= 15 THEN 1 ELSE 0 END) R4
                          , SUM(CASE WHEN UMUR >= 16 AND UMUR <= 18 THEN 1 ELSE 0 END) R5
                        FROM KAMPUNG_GISA_DKB WHERE 1=1  ";
						if($no_kel == 0){    
					$sql .=" AND NO_PROP = 32 AND NO_KAB = 73 AND NO_KEC =".$no_kec."
						    GROUP BY NO_KEL
						    ORDER BY NO_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND NO_PROP = 32 AND NO_KAB = 73 AND NO_KEC =".$no_kec." AND NO_KEL = ".$no_kel."
						    GROUP BY NO_KEL
						    ORDER BY NO_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::select($sql);
               return $r;
		}
		
		

		public function mlu_get_laporan_umur_sekolah_laki_laki($no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT 
					  A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL, B.R1 , B.R2 , B.R3, B.R4 , B.R5, (B.R1 + B.R2 + B.R3+ B.R4 + B.R5) JUMLAH
					  FROM SETUP_KEC A LEFT JOIN (SELECT NO_KEC
                          , SUM(CASE WHEN UMUR >= 0 AND UMUR <= 4 THEN 1 ELSE 0 END) R1
                          , SUM(CASE WHEN UMUR >= 5 AND UMUR <= 6 THEN 1 ELSE 0 END) R2
                          , SUM(CASE WHEN UMUR >= 7 AND UMUR <= 12 THEN 1 ELSE 0 END) R3
                          , SUM(CASE WHEN UMUR >= 13 AND UMUR <= 15 THEN 1 ELSE 0 END) R4
                          , SUM(CASE WHEN UMUR >= 16 AND UMUR <= 18 THEN 1 ELSE 0 END) R5
                        FROM KAMPUNG_GISA_DKB WHERE 1=1 AND JENIS_KELAMIN = 1
					GROUP BY NO_KEC
					ORDER BY NO_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT 
					  A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL, B.R1 , B.R2 , B.R3, B.R4 , B.R5, (B.R1 + B.R2 + B.R3+ B.R4 + B.R5) JUMLAH
					  FROM SETUP_KEL A LEFT JOIN (SELECT NO_KEL
                          , SUM(CASE WHEN UMUR >= 0 AND UMUR <= 4 THEN 1 ELSE 0 END) R1
                          , SUM(CASE WHEN UMUR >= 5 AND UMUR <= 6 THEN 1 ELSE 0 END) R2
                          , SUM(CASE WHEN UMUR >= 7 AND UMUR <= 12 THEN 1 ELSE 0 END) R3
                          , SUM(CASE WHEN UMUR >= 13 AND UMUR <= 15 THEN 1 ELSE 0 END) R4
                          , SUM(CASE WHEN UMUR >= 16 AND UMUR <= 18 THEN 1 ELSE 0 END) R5
                        FROM KAMPUNG_GISA_DKB WHERE 1=1 AND JENIS_KELAMIN = 1 ";
						if($no_kel == 0){    
					$sql .=" AND NO_PROP = 32 AND NO_KAB = 73 AND NO_KEC =".$no_kec."
						    GROUP BY NO_KEL
						    ORDER BY NO_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND NO_PROP = 32 AND NO_KAB = 73 AND NO_KEC =".$no_kec." AND NO_KEL = ".$no_kel."
						    GROUP BY NO_KEL
						    ORDER BY NO_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::select($sql);
               return $r;
		}
		
		

		public function mlu_get_laporan_umur_sekolah_perempuan($no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT 
					  A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL, B.R1 , B.R2 , B.R3, B.R4 , B.R5, (B.R1 + B.R2 + B.R3+ B.R4 + B.R5) JUMLAH
					  FROM SETUP_KEC A LEFT JOIN (SELECT NO_KEC
                          , SUM(CASE WHEN UMUR >= 0 AND UMUR <= 4 THEN 1 ELSE 0 END) R1
                          , SUM(CASE WHEN UMUR >= 5 AND UMUR <= 6 THEN 1 ELSE 0 END) R2
                          , SUM(CASE WHEN UMUR >= 7 AND UMUR <= 12 THEN 1 ELSE 0 END) R3
                          , SUM(CASE WHEN UMUR >= 13 AND UMUR <= 15 THEN 1 ELSE 0 END) R4
                          , SUM(CASE WHEN UMUR >= 16 AND UMUR <= 18 THEN 1 ELSE 0 END) R5
                        FROM KAMPUNG_GISA_DKB WHERE 1=1 AND JENIS_KELAMIN = 2
					GROUP BY NO_KEC
					ORDER BY NO_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT 
					  A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL, B.R1 , B.R2 , B.R3, B.R4 , B.R5, (B.R1 + B.R2 + B.R3+ B.R4 + B.R5) JUMLAH
					  FROM SETUP_KEL A LEFT JOIN (SELECT NO_KEL
                          , SUM(CASE WHEN UMUR >= 0 AND UMUR <= 4 THEN 1 ELSE 0 END) R1
                          , SUM(CASE WHEN UMUR >= 5 AND UMUR <= 6 THEN 1 ELSE 0 END) R2
                          , SUM(CASE WHEN UMUR >= 7 AND UMUR <= 12 THEN 1 ELSE 0 END) R3
                          , SUM(CASE WHEN UMUR >= 13 AND UMUR <= 15 THEN 1 ELSE 0 END) R4
                          , SUM(CASE WHEN UMUR >= 16 AND UMUR <= 18 THEN 1 ELSE 0 END) R5
                        FROM KAMPUNG_GISA_DKB WHERE 1=1 AND JENIS_KELAMIN = 2 ";
						if($no_kel == 0){    
					$sql .=" AND NO_PROP = 32 AND NO_KAB = 73 AND NO_KEC =".$no_kec."
						    GROUP BY NO_KEL
						    ORDER BY NO_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND NO_PROP = 32 AND NO_KAB = 73 AND NO_KEC =".$no_kec." AND NO_KEL = ".$no_kel."
						    GROUP BY NO_KEL
						    ORDER BY NO_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::select($sql);
               return $r;
		}
		
		
		public function mlu_get_laporan_umur_pemuda($no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT 
					  A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL, B.R1 , B.R2, (B.R1 + B.R2) JUMLAH
					  FROM SETUP_KEC A LEFT JOIN (SELECT NO_KEC,
					SUM (CASE WHEN JENIS_KELAMIN = 1 THEN 1 ELSE 0 END)  AS R1,
          			SUM (CASE WHEN JENIS_KELAMIN = 2 THEN 1 ELSE 0 END)  AS R2
					FROM KAMPUNG_GISA_DKB WHERE 1=1 AND NO_PROP = 32 AND NO_KAB =73 
					AND UMUR >=16 AND UMUR <=30
					GROUP BY NO_KEC
					ORDER BY NO_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT 
					  A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL, B.R1 , B.R2, (B.R1 + B.R2) JUMLAH
					  FROM SETUP_KEL A LEFT JOIN (SELECT NO_KEL,
					SUM (CASE WHEN JENIS_KELAMIN = 1 THEN 1 ELSE 0 END)  AS R1,
          			SUM (CASE WHEN JENIS_KELAMIN = 2 THEN 1 ELSE 0 END)  AS R2
					FROM KAMPUNG_GISA_DKB WHERE 1=1 
						AND UMUR >=16 AND UMUR <=30 ";
						if($no_kel == 0){    
					$sql .=" AND NO_PROP = 32 AND NO_KAB = 73 AND NO_KEC =".$no_kec."
						    GROUP BY NO_KEL
						    ORDER BY NO_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND NO_PROP = 32 AND NO_KAB = 73 AND NO_KEC =".$no_kec." AND NO_KEL = ".$no_kel."
						    GROUP BY NO_KEL
						    ORDER BY NO_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::select($sql);
               return $r;
		}
		
		
		public function mlu_get_laporan_umur_pemuda_pendidikan($no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT 
					  A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL, B.R1,B.R2,B.R3,B.R4,B.R5,B.R6,B.R7,B.R8,B.R9,B.R10, (B.R1+B.R2+B.R3+B.R4+B.R5+B.R6+B.R7+B.R8+B.R9+B.R10) JUMLAH
					  FROM SETUP_KEC A LEFT JOIN (SELECT NO_KEC,
					SUM(CASE WHEN PENDIDIKAN NOT BETWEEN 2 AND 10 OR PENDIDIKAN IS NULL THEN 1 ELSE 0 END) R1
						, SUM(CASE WHEN PENDIDIKAN = 2 THEN 1 ELSE 0 END) R2
						, SUM(CASE WHEN PENDIDIKAN = 3 THEN 1 ELSE 0 END) R3
						, SUM(CASE WHEN PENDIDIKAN = 4 THEN 1 ELSE 0 END) R4
						, SUM(CASE WHEN PENDIDIKAN = 5 THEN 1 ELSE 0 END) R5
						, SUM(CASE WHEN PENDIDIKAN = 6 THEN 1 ELSE 0 END) R6
						, SUM(CASE WHEN PENDIDIKAN = 7 THEN 1 ELSE 0 END) R7
						, SUM(CASE WHEN PENDIDIKAN = 8 THEN 1 ELSE 0 END) R8
						, SUM(CASE WHEN PENDIDIKAN = 9 THEN 1 ELSE 0 END) R9
						, SUM(CASE WHEN PENDIDIKAN = 10 THEN 1 ELSE 0 END) R10
						, COUNT(1) AS JUMLAH
					FROM KAMPUNG_GISA_DKB WHERE 1=1 AND NO_PROP = 32 AND NO_KAB =73 
					AND UMUR >=16 AND UMUR <=30
					GROUP BY NO_KEC
					ORDER BY NO_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT 
					  A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL, B.R1,B.R2,B.R3,B.R4,B.R5,B.R6,B.R7,B.R8,B.R9,B.R10, (B.R1+B.R2+B.R3+B.R4+B.R5+B.R6+B.R7+B.R8+B.R9+B.R10) JUMLAH
					  FROM SETUP_KEL A LEFT JOIN (SELECT NO_KEL,
					SUM(CASE WHEN PENDIDIKAN NOT BETWEEN 2 AND 10 OR PENDIDIKAN IS NULL THEN 1 ELSE 0 END) R1
						, SUM(CASE WHEN PENDIDIKAN = 2 THEN 1 ELSE 0 END) R2
						, SUM(CASE WHEN PENDIDIKAN = 3 THEN 1 ELSE 0 END) R3
						, SUM(CASE WHEN PENDIDIKAN = 4 THEN 1 ELSE 0 END) R4
						, SUM(CASE WHEN PENDIDIKAN = 5 THEN 1 ELSE 0 END) R5
						, SUM(CASE WHEN PENDIDIKAN = 6 THEN 1 ELSE 0 END) R6
						, SUM(CASE WHEN PENDIDIKAN = 7 THEN 1 ELSE 0 END) R7
						, SUM(CASE WHEN PENDIDIKAN = 8 THEN 1 ELSE 0 END) R8
						, SUM(CASE WHEN PENDIDIKAN = 9 THEN 1 ELSE 0 END) R9
						, SUM(CASE WHEN PENDIDIKAN = 10 THEN 1 ELSE 0 END) R10
						, COUNT(1) AS JUMLAH
					FROM KAMPUNG_GISA_DKB WHERE 1=1 
						AND UMUR >=16 AND UMUR <=30 ";
						if($no_kel == 0){    
					$sql .=" AND NO_PROP = 32 AND NO_KAB = 73 AND NO_KEC =".$no_kec."
						    GROUP BY NO_KEL
						    ORDER BY NO_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND NO_PROP = 32 AND NO_KAB = 73 AND NO_KEC =".$no_kec." AND NO_KEL = ".$no_kel."
						    GROUP BY NO_KEL
						    ORDER BY NO_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::select($sql);
               return $r;
		}
		public function mst_get_cakupan_nik($no_kec,$no_kel){
			$dkb_bio = Shr::do_get_dkb_bio();
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS R1
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS R2
         			, B.JUMLAH - B.JUMLAH AS R3
          			, CONCAT(TO_CHAR(ROUND(B.JUMLAH/B.JUMLAH, 4)*100),'%') AS PRS 
					FROM SETUP_KEC A LEFT JOIN
					(SELECT 
						B.NO_KEC 
						,B.NAMA_KEC 
						, COUNT(1) JUMLAH
						FROM SETUP_KEC B LEFT JOIN ".$dkb_bio."  A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC WHERE  (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) AND A.NO_PROP = 32 AND A.NO_KAB =73 GROUP BY B.NO_KEC,B.NAMA_KEC ORDER BY B.NO_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS R1
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS R2
         			, B.JUMLAH - B.JUMLAH AS R3
          			, CONCAT(TO_CHAR(ROUND(B.JUMLAH/B.JUMLAH, 4)*100),'%') AS PRS 
					FROM SETUP_KEL A LEFT JOIN
					(SELECT 
						B.NO_KEL 
						,B.NAMA_KEL 
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN ".$dkb_bio." A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL WHERE  (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) ";
						if($no_kel == 0){    
					$sql .=" AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec." and  A.NO_KEL =".$no_kel."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::select($sql);
               return $r;
		}

		

		public function mst_get_cakupan_kk($no_kec,$no_kel){
			$dkb_bio = Shr::do_get_dkb_bio();
			$dkb_keluarga = Shr::do_get_dkb_keluarga();
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS R1
					, CASE WHEN B.ADA_KK IS NULL THEN 0 ELSE B.ADA_KK END AS R2
          			, B.JUMLAH - B.ADA_KK AS R3
          			, CONCAT(TO_CHAR(ROUND(B.ADA_KK/B.JUMLAH, 4)*100),'%') AS PRS 
					FROM SETUP_KEC A LEFT JOIN
					(SELECT 
					B.NO_KEC 
					,B.NAMA_KEC 
					, COUNT(1) JUMLAH
		            , SUM(CASE WHEN A.PRINT_KK IS NOT NULL THEN 1 ELSE 0 END) ADA_KK
		            , SUM(CASE WHEN A.PRINT_KK IS NULL THEN 1 ELSE 0 END) GA_ADA_KK
					FROM SETUP_KEC B LEFT JOIN 
		            (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NO_KK, A.JENIS_KLMIN, X.PRINT_KK  
		            FROM BIODATA_WNI_DKB A INNER JOIN DATA_KELUARGA_DKB C ON A.NO_KK = C.NO_KK 
		            LEFT JOIN (SELECT DISTINCT(PRINT_KK) PRINT_KK FROM (SELECT TO_NUMBER(NO_KK) AS PRINT_KK FROM T5_SEQN_PRINT_KK UNION ALL SELECT TO_NUMBER(NO_KK) AS PRINT_KK FROM SEQN_PRINT_KK)) X ON X.PRINT_KK = C.NO_KK WHERE A.STAT_HBKEL =1 AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC  
					WHERE 1=1 AND A.NO_PROP = 32 AND A.NO_KAB =73 GROUP BY B.NO_KEC,B.NAMA_KEC ORDER BY B.NO_KEC) B ON A.NO_KEC = B.NO_KEC 
            		WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS R1
					, CASE WHEN B.ADA_KK IS NULL THEN 0 ELSE B.ADA_KK END AS R2
          			, B.JUMLAH - B.ADA_KK AS R3
          			, CONCAT(TO_CHAR(ROUND(B.ADA_KK/B.JUMLAH, 4)*100),'%') AS PRS
					FROM SETUP_KEL A LEFT JOIN
					 ";
						if($no_kel == 0){    
					$sql .=" (SELECT 
						B.NO_KEL
						, B.NAMA_KEL 
						, COUNT(1) JUMLAH
            			, SUM(CASE WHEN A.PRINT_KK IS NOT NULL THEN 1 ELSE 0 END) ADA_KK
            			, SUM(CASE WHEN A.PRINT_KK IS NULL THEN 1 ELSE 0 END) GA_ADA_KK
						FROM SETUP_KEL B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NO_KK, A.JENIS_KLMIN, X.PRINT_KK FROM ".$dkb_bio." A INNER JOIN ".$dkb_keluarga." C ON A.NO_KK = C.NO_KK LEFT JOIN (SELECT DISTINCT(PRINT_KK) PRINT_KK FROM (SELECT TO_NUMBER(NO_KK) AS PRINT_KK FROM T5_SEQN_PRINT_KK UNION ALL SELECT TO_NUMBER(NO_KK) AS PRINT_KK FROM SEQN_PRINT_KK)) X ON X.PRINT_KK = C.NO_KK WHERE A.STAT_HBKEL =1 AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL  WHERE 1=1  AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL
						    WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" (SELECT 
						B.NO_KEL
						, B.NAMA_KEL 
						, COUNT(1) JUMLAH
            			, SUM(CASE WHEN A.PRINT_KK IS NOT NULL THEN 1 ELSE 0 END) ADA_KK
            			, SUM(CASE WHEN A.PRINT_KK IS NULL THEN 1 ELSE 0 END) GA_ADA_KK
						FROM SETUP_KEL B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NO_KK, A.JENIS_KLMIN, X.PRINT_KK FROM ".$dkb_bio." A INNER JOIN ".$dkb_keluarga." C ON A.NO_KK = C.NO_KK LEFT JOIN (SELECT DISTINCT(PRINT_KK) PRINT_KK FROM (SELECT TO_NUMBER(NO_KK) AS PRINT_KK FROM T5_SEQN_PRINT_KK UNION ALL SELECT TO_NUMBER(NO_KK) AS PRINT_KK FROM SEQN_PRINT_KK)) X ON X.PRINT_KK = C.NO_KK WHERE A.STAT_HBKEL =1 AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL  WHERE 1=1  AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL
						    WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::select($sql);
               return $r;
		}

		
		public function mst_get_cakupan_kia($no_kec,$no_kel){
			$dkb_bio = Shr::do_get_dkb_bio();
			$cut_off_dkb = Shr::do_get_cut_off_date();
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS R1
					, CASE WHEN C.JUMLAH IS NULL THEN 0 ELSE C.JUMLAH END AS R2
          			, B.JUMLAH - C.JUMLAH AS R3
          			, CONCAT(TO_CHAR(ROUND(C.JUMLAH/B.JUMLAH, 4)*100),'%') AS PRS 
					FROM SETUP_KEC A LEFT JOIN
					(SELECT 
						B.NO_KEC 
						,B.NAMA_KEC 
						, COUNT(1) JUMLAH
						FROM SETUP_KEC B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM ".$dkb_bio." A WHERE A.TGL_LHR>ADD_MONTHS(TO_DATE('".$cut_off_dkb."','DD/MM/YYYY'),-12*17) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC  
			            WHERE 1=1 AND A.NO_PROP = 32 AND A.NO_KAB =73 GROUP BY B.NO_KEC,B.NAMA_KEC ORDER BY B.NO_KEC) B ON A.NO_KEC = B.NO_KEC
			            LEFT JOIN
					(SELECT 
						B.NO_KEC 
						,B.NAMA_KEC 
						, COUNT(1) JUMLAH
						FROM SETUP_KEC B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM ".$dkb_bio." A WHERE A.TGL_LHR>ADD_MONTHS(TO_DATE('".$cut_off_dkb."','DD/MM/YYYY'),-12*17) AND EXISTS (SELECT 1 FROM T5_SEQN_KIA_PRINT C WHERE A.NIK = C.NIK) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC  
            		WHERE 1=1 AND A.NO_PROP = 32 AND A.NO_KAB =73 GROUP BY B.NO_KEC,B.NAMA_KEC ORDER BY B.NO_KEC) C ON A.NO_KEC = B.NO_KEC AND B.NO_KEC =C.NO_KEC
            		WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS R1
					, CASE WHEN C.JUMLAH IS NULL THEN 0 ELSE C.JUMLAH END AS R2
          			, B.JUMLAH - C.JUMLAH AS R3
          			, CONCAT(TO_CHAR(ROUND(C.JUMLAH/B.JUMLAH, 4)*100),'%') AS PRS 
					FROM SETUP_KEL A LEFT JOIN
					 ";
						if($no_kel == 0){    
					$sql .=" (SELECT 
						B.NO_KEL
						, B.NAMA_KEL 
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM ".$dkb_bio." A WHERE A.TGL_LHR>ADD_MONTHS(TO_DATE('".$cut_off_dkb."','DD/MM/YYYY'),-12*17) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL  WHERE 1=1  AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL
						    LEFT JOIN
						    (SELECT 
						B.NO_KEL
						, B.NAMA_KEL 
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM ".$dkb_bio." A WHERE A.TGL_LHR>ADD_MONTHS(TO_DATE('".$cut_off_dkb."','DD/MM/YYYY'),-12*17) AND EXISTS (SELECT 1 FROM T5_SEQN_KIA_PRINT C WHERE A.NIK = C.NIK) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL  WHERE 1=1 AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) C ON A.NO_KEL = B.NO_KEL AND B.NO_KEL =C.NO_KEL
						    WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" (SELECT 
						B.NO_KEL
						, B.NAMA_KEL 
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM ".$dkb_bio." A WHERE A.TGL_LHR>ADD_MONTHS(TO_DATE('".$cut_off_dkb."','DD/MM/YYYY'),-12*17) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL  WHERE 1=1  AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL = ".$no_kel."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL
						    LEFT JOIN
						    (SELECT 
						B.NO_KEL
						, B.NAMA_KEL 
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM ".$dkb_bio." A WHERE A.TGL_LHR>ADD_MONTHS(TO_DATE('".$cut_off_dkb."','DD/MM/YYYY'),-12*17) AND EXISTS (SELECT 1 FROM T5_SEQN_KIA_PRINT C WHERE A.NIK = C.NIK) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL  WHERE 1=1 AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) C ON A.NO_KEL = B.NO_KEL AND B.NO_KEL =C.NO_KEL
						    WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::select($sql);
               return $r;
		}

		
		public function mst_get_cakupan_sudah_rekam($no_kec,$no_kel){
			$dkb_bio = Shr::do_get_dkb_bio();
			$cut_off_dkb = Shr::do_get_cut_off_date();
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS R1
					, CASE WHEN C.JUMLAH IS NULL THEN 0 ELSE C.JUMLAH END AS R2
          			, B.JUMLAH - C.JUMLAH AS R3
          			, CONCAT(TO_CHAR(ROUND(C.JUMLAH/B.JUMLAH, 4)*100),'%') AS PRS 
					FROM SETUP_KEC A LEFT JOIN
					(SELECT 
						B.NO_KEC 
						,B.NAMA_KEC 
						, COUNT(1) JUMLAH
						FROM SETUP_KEC B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM ".$dkb_bio." A WHERE (A.TGL_LHR<=ADD_MONTHS(TO_DATE('".$cut_off_dkb."','DD/MM/YYYY'),-12*17) OR A.STAT_KWN<>1) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC  
			            WHERE 1=1 AND A.NO_PROP = 32 AND A.NO_KAB =73 GROUP BY B.NO_KEC,B.NAMA_KEC ORDER BY B.NO_KEC) B ON A.NO_KEC = B.NO_KEC
			            LEFT JOIN
					(SELECT 
						B.NO_KEC 
						,B.NAMA_KEC 
						, COUNT(1) JUMLAH
						FROM SETUP_KEC B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM ".$dkb_bio." A WHERE (A.TGL_LHR<=ADD_MONTHS(TO_DATE('".$cut_off_dkb."','DD/MM/YYYY'),-12*17) OR A.STAT_KWN<>1) AND EXISTS (SELECT 1 FROM DEMOGRAPHICS_ALL C WHERE A.NIK = C.NIK) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC  
            		WHERE 1=1 AND A.NO_PROP = 32 AND A.NO_KAB =73 GROUP BY B.NO_KEC,B.NAMA_KEC ORDER BY B.NO_KEC) C ON A.NO_KEC = B.NO_KEC AND B.NO_KEC =C.NO_KEC
            		WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS R1
					, CASE WHEN C.JUMLAH IS NULL THEN 0 ELSE C.JUMLAH END AS R2
          			, B.JUMLAH - C.JUMLAH AS R3
          			, CONCAT(TO_CHAR(ROUND(C.JUMLAH/B.JUMLAH, 4)*100),'%') AS PRS 
					FROM SETUP_KEL A LEFT JOIN
					 ";
						if($no_kel == 0){    
					$sql .=" (SELECT 
						B.NO_KEL
						, B.NAMA_KEL 
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM ".$dkb_bio." A WHERE (A.TGL_LHR<=ADD_MONTHS(TO_DATE('".$cut_off_dkb."','DD/MM/YYYY'),-12*17) OR A.STAT_KWN<>1) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL  WHERE 1=1  AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL
						    LEFT JOIN
						    (SELECT 
						B.NO_KEL
						, B.NAMA_KEL 
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM ".$dkb_bio." A WHERE (A.TGL_LHR<=ADD_MONTHS(TO_DATE('".$cut_off_dkb."','DD/MM/YYYY'),-12*17) OR A.STAT_KWN<>1) AND EXISTS (SELECT 1 FROM DEMOGRAPHICS_ALL C WHERE A.NIK = C.NIK) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL  WHERE 1=1 AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) C ON A.NO_KEL = B.NO_KEL AND B.NO_KEL =C.NO_KEL
						    WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" (SELECT 
						B.NO_KEL
						, B.NAMA_KEL 
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM ".$dkb_bio." A WHERE (A.TGL_LHR<=ADD_MONTHS(TO_DATE('".$cut_off_dkb."','DD/MM/YYYY'),-12*17) OR A.STAT_KWN<>1) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL  WHERE 1=1  AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL = ".$no_kel."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL
						    LEFT JOIN
						    (SELECT 
						B.NO_KEL
						, B.NAMA_KEL 
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM ".$dkb_bio." A WHERE (A.TGL_LHR<=ADD_MONTHS(TO_DATE('".$cut_off_dkb."','DD/MM/YYYY'),-12*17) OR A.STAT_KWN<>1) AND EXISTS (SELECT 1 FROM DEMOGRAPHICS_ALL C WHERE A.NIK = C.NIK) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL  WHERE 1=1 AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) C ON A.NO_KEL = B.NO_KEL AND B.NO_KEL =C.NO_KEL
						    WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::select($sql);
               return $r;
		}

		
		public function mst_get_cakupan_sudah_cetak($no_kec,$no_kel){
			$dkb_bio = Shr::do_get_dkb_bio();
			$cut_off_dkb = Shr::do_get_cut_off_date();
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS R1
					, CASE WHEN C.JUMLAH IS NULL THEN 0 ELSE C.JUMLAH END AS R2
          			, B.JUMLAH - C.JUMLAH AS R3
          			, CONCAT(TO_CHAR(ROUND(C.JUMLAH/B.JUMLAH, 4)*100),'%') AS PRS 
					FROM SETUP_KEC A LEFT JOIN
					(SELECT 
						B.NO_KEC 
						,B.NAMA_KEC 
						, COUNT(1) JUMLAH
						FROM SETUP_KEC B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM ".$dkb_bio." A WHERE (A.TGL_LHR<=ADD_MONTHS(TO_DATE('".$cut_off_dkb."','DD/MM/YYYY'),-12*17) OR A.STAT_KWN<>1) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC  
			            WHERE 1=1 AND A.NO_PROP = 32 AND A.NO_KAB =73 GROUP BY B.NO_KEC,B.NAMA_KEC ORDER BY B.NO_KEC) B ON A.NO_KEC = B.NO_KEC
			            LEFT JOIN
					(SELECT 
						B.NO_KEC 
						,B.NAMA_KEC 
						, COUNT(1) JUMLAH
						FROM SETUP_KEC B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM ".$dkb_bio." A WHERE (A.TGL_LHR<=ADD_MONTHS(TO_DATE('".$cut_off_dkb."','DD/MM/YYYY'),-12*17) OR A.STAT_KWN<>1) AND EXISTS (SELECT 1 FROM DEMOGRAPHICS_ALL C WHERE A.NIK = C.NIK AND C.CURRENT_STATUS_CODE LIKE '%CARD%') AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC  
            		WHERE 1=1 AND A.NO_PROP = 32 AND A.NO_KAB =73 GROUP BY B.NO_KEC,B.NAMA_KEC ORDER BY B.NO_KEC) C ON A.NO_KEC = B.NO_KEC AND B.NO_KEC =C.NO_KEC
            		WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS R1
					, CASE WHEN C.JUMLAH IS NULL THEN 0 ELSE C.JUMLAH END AS R2
          			, B.JUMLAH - C.JUMLAH AS R3
          			, CONCAT(TO_CHAR(ROUND(C.JUMLAH/B.JUMLAH, 4)*100),'%') AS PRS 
					FROM SETUP_KEL A LEFT JOIN
					 ";
						if($no_kel == 0){    
					$sql .=" (SELECT 
						B.NO_KEL
						, B.NAMA_KEL 
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM ".$dkb_bio." A WHERE (A.TGL_LHR<=ADD_MONTHS(TO_DATE('".$cut_off_dkb."','DD/MM/YYYY'),-12*17) OR A.STAT_KWN<>1) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL  WHERE 1=1  AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL
						    LEFT JOIN
						    (SELECT 
						B.NO_KEL
						, B.NAMA_KEL 
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM ".$dkb_bio." A WHERE (A.TGL_LHR<=ADD_MONTHS(TO_DATE('".$cut_off_dkb."','DD/MM/YYYY'),-12*17) OR A.STAT_KWN<>1) AND EXISTS (SELECT 1 FROM DEMOGRAPHICS_ALL C WHERE A.NIK = C.NIK AND C.CURRENT_STATUS_CODE LIKE '%CARD%') AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL  WHERE 1=1 AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) C ON A.NO_KEL = B.NO_KEL AND B.NO_KEL =C.NO_KEL
						    WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" (SELECT 
						B.NO_KEL
						, B.NAMA_KEL 
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM ".$dkb_bio." A WHERE (A.TGL_LHR<=ADD_MONTHS(TO_DATE('".$cut_off_dkb."','DD/MM/YYYY'),-12*17) OR A.STAT_KWN<>1) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL  WHERE 1=1  AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL = ".$no_kel."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL
						    LEFT JOIN
						    (SELECT 
						B.NO_KEL
						, B.NAMA_KEL 
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM ".$dkb_bio." A WHERE (A.TGL_LHR<=ADD_MONTHS(TO_DATE('".$cut_off_dkb."','DD/MM/YYYY'),-12*17) OR A.STAT_KWN<>1) AND EXISTS (SELECT 1 FROM DEMOGRAPHICS_ALL C WHERE A.NIK = C.NIK AND C.CURRENT_STATUS_CODE LIKE '%CARD%') AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL  WHERE 1=1 AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) C ON A.NO_KEL = B.NO_KEL AND B.NO_KEL =C.NO_KEL
						    WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::select($sql);
               return $r;
		}

		
		public function mst_get_cakupan_akta_018($no_kec,$no_kel){
			$dkb_bio = Shr::do_get_dkb_bio();
			$cut_off_dkb = Shr::do_get_cut_off_date();
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS R1
					, CASE WHEN C.JUMLAH IS NULL THEN 0 ELSE C.JUMLAH END AS R2
          			, B.JUMLAH - C.JUMLAH AS R3
          			, CASE WHEN B.LK IS NULL THEN 0 ELSE B.LK END AS R1_LK
					, CASE WHEN B.PR IS NULL THEN 0 ELSE B.PR END AS R1_PR
					, CASE WHEN C.LK IS NULL THEN 0 ELSE C.LK END AS R2_LK
					, CASE WHEN C.PR IS NULL THEN 0 ELSE C.PR END AS R2_PR
					, B.LK - C.LK AS R3_LK
					, B.PR - C.PR AS R3_PR
          			, CONCAT(TO_CHAR(ROUND(C.JUMLAH/B.JUMLAH, 4)*100),'%') AS PRS 
					FROM SETUP_KEC A LEFT JOIN
					(SELECT 
						B.NO_KEC 
						,B.NAMA_KEC 
						, SUM(CASE WHEN A.JENIS_KLMIN = 1 THEN 1 ELSE 0 END) LK 
						, SUM(CASE WHEN A.JENIS_KLMIN = 2 THEN 1 ELSE 0 END) PR
						, COUNT(1) JUMLAH
						FROM SETUP_KEC B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM ".$dkb_bio." A WHERE A.TGL_LHR>ADD_MONTHS(TO_DATE('".$cut_off_dkb."','DD/MM/YYYY'),-12*18) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC  
			            WHERE 1=1 AND A.NO_PROP = 32 AND A.NO_KAB =73 GROUP BY B.NO_KEC,B.NAMA_KEC ORDER BY B.NO_KEC) B ON A.NO_KEC = B.NO_KEC
			            LEFT JOIN
					(SELECT 
						B.NO_KEC 
						,B.NAMA_KEC 
						, SUM(CASE WHEN A.JENIS_KLMIN = 1 THEN 1 ELSE 0 END) LK 
						, SUM(CASE WHEN A.JENIS_KLMIN = 2 THEN 1 ELSE 0 END) PR
						, COUNT(1) JUMLAH
						FROM SETUP_KEC B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM ".$dkb_bio." A WHERE A.TGL_LHR>ADD_MONTHS(TO_DATE('".$cut_off_dkb."','DD/MM/YYYY'),-12*18) AND EXISTS (SELECT 1 FROM (SELECT TO_CHAR(NIK) AS NIK FROM BIODATA_WNI C WHERE C.AKTA_LHR = 2 UNION ALL SELECT C.BAYI_NIK AS NIK FROM CAPIL_LAHIR C) C WHERE TO_CHAR(A.NIK) = C.NIK ) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC  
            		WHERE 1=1 AND A.NO_PROP = 32 AND A.NO_KAB =73 GROUP BY B.NO_KEC,B.NAMA_KEC ORDER BY B.NO_KEC) C ON A.NO_KEC = B.NO_KEC AND B.NO_KEC =C.NO_KEC
            		WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS R1
					, CASE WHEN C.JUMLAH IS NULL THEN 0 ELSE C.JUMLAH END AS R2
          			, B.JUMLAH - C.JUMLAH AS R3
          			, CASE WHEN B.LK IS NULL THEN 0 ELSE B.LK END AS R1_LK
					, CASE WHEN B.PR IS NULL THEN 0 ELSE B.PR END AS R1_PR
					, CASE WHEN C.LK IS NULL THEN 0 ELSE C.LK END AS R2_LK
					, CASE WHEN C.PR IS NULL THEN 0 ELSE C.PR END AS R2_PR
					, B.LK - C.LK AS R3_LK
					, B.PR - C.PR AS R3_PR
          			, CONCAT(TO_CHAR(ROUND(C.JUMLAH/B.JUMLAH, 4)*100),'%') AS PRS 
					FROM SETUP_KEL A LEFT JOIN
					 ";
						if($no_kel == 0){    
					$sql .=" (SELECT 
						B.NO_KEL
						, B.NAMA_KEL 
						, SUM(CASE WHEN A.JENIS_KLMIN = 1 THEN 1 ELSE 0 END) LK 
						, SUM(CASE WHEN A.JENIS_KLMIN = 2 THEN 1 ELSE 0 END) PR
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM ".$dkb_bio." A WHERE A.TGL_LHR>ADD_MONTHS(TO_DATE('".$cut_off_dkb."','DD/MM/YYYY'),-12*18) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL  WHERE 1=1  AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL
						    LEFT JOIN
						    (SELECT 
						B.NO_KEL
						, B.NAMA_KEL 
						, SUM(CASE WHEN A.JENIS_KLMIN = 1 THEN 1 ELSE 0 END) LK 
						, SUM(CASE WHEN A.JENIS_KLMIN = 2 THEN 1 ELSE 0 END) PR
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM ".$dkb_bio." A WHERE A.TGL_LHR>ADD_MONTHS(TO_DATE('".$cut_off_dkb."','DD/MM/YYYY'),-12*18) AND EXISTS (SELECT 1 FROM (SELECT TO_CHAR(NIK) AS NIK FROM BIODATA_WNI C WHERE C.AKTA_LHR = 2 UNION ALL SELECT C.BAYI_NIK AS NIK FROM CAPIL_LAHIR C) C WHERE TO_CHAR(A.NIK) = C.NIK ) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL  WHERE 1=1 AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) C ON A.NO_KEL = B.NO_KEL AND B.NO_KEL =C.NO_KEL
						    WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" (SELECT 
						B.NO_KEL
						, B.NAMA_KEL 
						, SUM(CASE WHEN A.JENIS_KLMIN = 1 THEN 1 ELSE 0 END) LK 
						, SUM(CASE WHEN A.JENIS_KLMIN = 2 THEN 1 ELSE 0 END) PR
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM ".$dkb_bio." A WHERE A.TGL_LHR>ADD_MONTHS(TO_DATE('".$cut_off_dkb."','DD/MM/YYYY'),-12*18) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL  WHERE 1=1  AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL = ".$no_kel."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL
						    LEFT JOIN
						    (SELECT 
						B.NO_KEL
						, B.NAMA_KEL 
						, SUM(CASE WHEN A.JENIS_KLMIN = 1 THEN 1 ELSE 0 END) LK 
						, SUM(CASE WHEN A.JENIS_KLMIN = 2 THEN 1 ELSE 0 END) PR
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM ".$dkb_bio." A WHERE A.TGL_LHR>ADD_MONTHS(TO_DATE('".$cut_off_dkb."','DD/MM/YYYY'),-12*18) AND EXISTS (SELECT 1 FROM (SELECT TO_CHAR(NIK) AS NIK FROM BIODATA_WNI C WHERE C.AKTA_LHR = 2 UNION ALL SELECT C.BAYI_NIK AS NIK FROM CAPIL_LAHIR C) C WHERE TO_CHAR(A.NIK) = C.NIK ) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL  WHERE 1=1 AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) C ON A.NO_KEL = B.NO_KEL AND B.NO_KEL =C.NO_KEL
						    WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::select($sql);
               return $r;
		}

		
		public function mst_get_cakupan_akta_018_pelayanan($no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS R1
					, CASE WHEN C.JUMLAH IS NULL THEN 0 ELSE C.JUMLAH END AS R2
          			, B.JUMLAH - C.JUMLAH AS R3
          			, CASE WHEN B.LK IS NULL THEN 0 ELSE B.LK END AS R1_LK
					, CASE WHEN B.PR IS NULL THEN 0 ELSE B.PR END AS R1_PR
					, CASE WHEN C.LK IS NULL THEN 0 ELSE C.LK END AS R2_LK
					, CASE WHEN C.PR IS NULL THEN 0 ELSE C.PR END AS R2_PR
					, B.LK - C.LK AS R3_LK
					, B.PR - C.PR AS R3_PR
          			, CONCAT(TO_CHAR(ROUND(C.JUMLAH/B.JUMLAH, 4)*100),'%') AS PRS 
					FROM SETUP_KEC A LEFT JOIN
					(SELECT 
						B.NO_KEC 
						,B.NAMA_KEC 
						, SUM(CASE WHEN A.JENIS_KLMIN = 1 THEN 1 ELSE 0 END) LK 
						, SUM(CASE WHEN A.JENIS_KLMIN = 2 THEN 1 ELSE 0 END) PR
						, COUNT(1) JUMLAH
						FROM SETUP_KEC B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM BIODATA_WNI A WHERE A.TGL_LHR>ADD_MONTHS(SYSDATE,-12*18) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC  
			            WHERE 1=1 AND A.NO_PROP = 32 AND A.NO_KAB =73 GROUP BY B.NO_KEC,B.NAMA_KEC ORDER BY B.NO_KEC) B ON A.NO_KEC = B.NO_KEC
			            LEFT JOIN
					(SELECT 
						B.NO_KEC 
						,B.NAMA_KEC 
						, SUM(CASE WHEN A.JENIS_KLMIN = 1 THEN 1 ELSE 0 END) LK 
						, SUM(CASE WHEN A.JENIS_KLMIN = 2 THEN 1 ELSE 0 END) PR
						, COUNT(1) JUMLAH
						FROM SETUP_KEC B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM BIODATA_WNI A WHERE A.TGL_LHR>ADD_MONTHS(SYSDATE,-12*18) AND EXISTS (SELECT 1 FROM (SELECT TO_CHAR(NIK) AS NIK FROM BIODATA_WNI C WHERE C.AKTA_LHR = 2 UNION ALL SELECT C.BAYI_NIK AS NIK FROM CAPIL_LAHIR C) C WHERE TO_CHAR(A.NIK) = C.NIK ) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC  
            		WHERE 1=1 AND A.NO_PROP = 32 AND A.NO_KAB =73 GROUP BY B.NO_KEC,B.NAMA_KEC ORDER BY B.NO_KEC) C ON A.NO_KEC = B.NO_KEC AND B.NO_KEC =C.NO_KEC
            		WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS R1
					, CASE WHEN C.JUMLAH IS NULL THEN 0 ELSE C.JUMLAH END AS R2
          			, B.JUMLAH - C.JUMLAH AS R3
          			, CASE WHEN B.LK IS NULL THEN 0 ELSE B.LK END AS R1_LK
					, CASE WHEN B.PR IS NULL THEN 0 ELSE B.PR END AS R1_PR
					, CASE WHEN C.LK IS NULL THEN 0 ELSE C.LK END AS R2_LK
					, CASE WHEN C.PR IS NULL THEN 0 ELSE C.PR END AS R2_PR
					, B.LK - C.LK AS R3_LK
					, B.PR - C.PR AS R3_PR
          			, CONCAT(TO_CHAR(ROUND(C.JUMLAH/B.JUMLAH, 4)*100),'%') AS PRS 
					FROM SETUP_KEL A LEFT JOIN
					 ";
						if($no_kel == 0){    
					$sql .=" (SELECT 
						B.NO_KEL
						, B.NAMA_KEL 
						, SUM(CASE WHEN A.JENIS_KLMIN = 1 THEN 1 ELSE 0 END) LK 
						, SUM(CASE WHEN A.JENIS_KLMIN = 2 THEN 1 ELSE 0 END) PR
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM BIODATA_WNI A WHERE A.TGL_LHR>ADD_MONTHS(SYSDATE,-12*18) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL  WHERE 1=1  AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL
						    LEFT JOIN
						    (SELECT 
						B.NO_KEL
						, B.NAMA_KEL 
						, SUM(CASE WHEN A.JENIS_KLMIN = 1 THEN 1 ELSE 0 END) LK 
						, SUM(CASE WHEN A.JENIS_KLMIN = 2 THEN 1 ELSE 0 END) PR
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM BIODATA_WNI A WHERE A.TGL_LHR>ADD_MONTHS(SYSDATE,-12*18) AND EXISTS (SELECT 1 FROM (SELECT TO_CHAR(NIK) AS NIK FROM BIODATA_WNI C WHERE C.AKTA_LHR = 2 UNION ALL SELECT C.BAYI_NIK AS NIK FROM CAPIL_LAHIR C) C WHERE TO_CHAR(A.NIK) = C.NIK ) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL  WHERE 1=1 AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) C ON A.NO_KEL = B.NO_KEL AND B.NO_KEL =C.NO_KEL
						    WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" (SELECT 
						B.NO_KEL
						, B.NAMA_KEL 
						, SUM(CASE WHEN A.JENIS_KLMIN = 1 THEN 1 ELSE 0 END) LK 
						, SUM(CASE WHEN A.JENIS_KLMIN = 2 THEN 1 ELSE 0 END) PR
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM BIODATA_WNI A WHERE A.TGL_LHR>ADD_MONTHS(SYSDATE,-12*18) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL  WHERE 1=1  AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL = ".$no_kel."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL
						    LEFT JOIN
						    (SELECT 
						B.NO_KEL
						, B.NAMA_KEL 
						, SUM(CASE WHEN A.JENIS_KLMIN = 1 THEN 1 ELSE 0 END) LK 
						, SUM(CASE WHEN A.JENIS_KLMIN = 2 THEN 1 ELSE 0 END) PR
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM BIODATA_WNI A WHERE A.TGL_LHR>ADD_MONTHS(SYSDATE,-12*18) AND EXISTS (SELECT 1 FROM (SELECT TO_CHAR(NIK) AS NIK FROM BIODATA_WNI C WHERE C.AKTA_LHR = 2 UNION ALL SELECT C.BAYI_NIK AS NIK FROM CAPIL_LAHIR C) C WHERE TO_CHAR(A.NIK) = C.NIK ) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL  WHERE 1=1 AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) C ON A.NO_KEL = B.NO_KEL AND B.NO_KEL =C.NO_KEL
						    WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::connection('db222')->select($sql);
               return $r;
		}

		
		public function mst_get_cakupan_akta_18($no_kec,$no_kel){
			$dkb_bio = Shr::do_get_dkb_bio();
			$cut_off_dkb = Shr::do_get_cut_off_date();
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS R1
					, CASE WHEN C.JUMLAH IS NULL THEN 0 ELSE C.JUMLAH END AS R2
          			, B.JUMLAH - C.JUMLAH AS R3
          			, CASE WHEN B.LK IS NULL THEN 0 ELSE B.LK END AS R1_LK
					, CASE WHEN B.PR IS NULL THEN 0 ELSE B.PR END AS R1_PR
					, CASE WHEN C.LK IS NULL THEN 0 ELSE C.LK END AS R2_LK
					, CASE WHEN C.PR IS NULL THEN 0 ELSE C.PR END AS R2_PR
					, B.LK - C.LK AS R3_LK
					, B.PR - C.PR AS R3_PR
          			, CONCAT(TO_CHAR(ROUND(C.JUMLAH/B.JUMLAH, 4)*100),'%') AS PRS 
					FROM SETUP_KEC A LEFT JOIN
					(SELECT 
						B.NO_KEC 
						,B.NAMA_KEC 
						, SUM(CASE WHEN A.JENIS_KLMIN = 1 THEN 1 ELSE 0 END) LK 
						, SUM(CASE WHEN A.JENIS_KLMIN = 2 THEN 1 ELSE 0 END) PR
						, COUNT(1) JUMLAH
						FROM SETUP_KEC B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM ".$dkb_bio." A WHERE A.TGL_LHR<=ADD_MONTHS(TO_DATE('".$cut_off_dkb."','DD/MM/YYYY'),-12*18) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC  
			            WHERE 1=1 AND A.NO_PROP = 32 AND A.NO_KAB =73 GROUP BY B.NO_KEC,B.NAMA_KEC ORDER BY B.NO_KEC) B ON A.NO_KEC = B.NO_KEC
			            LEFT JOIN
					(SELECT 
						B.NO_KEC 
						,B.NAMA_KEC 
						, SUM(CASE WHEN A.JENIS_KLMIN = 1 THEN 1 ELSE 0 END) LK 
						, SUM(CASE WHEN A.JENIS_KLMIN = 2 THEN 1 ELSE 0 END) PR
						, COUNT(1) JUMLAH
						FROM SETUP_KEC B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM ".$dkb_bio." A WHERE A.TGL_LHR<=ADD_MONTHS(TO_DATE('".$cut_off_dkb."','DD/MM/YYYY'),-12*18) AND EXISTS (SELECT 1 FROM (SELECT TO_CHAR(NIK) AS NIK FROM BIODATA_WNI C WHERE C.AKTA_LHR = 2 UNION ALL SELECT C.BAYI_NIK AS NIK FROM CAPIL_LAHIR C) C WHERE TO_CHAR(A.NIK) = C.NIK ) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC  
            		WHERE 1=1 AND A.NO_PROP = 32 AND A.NO_KAB =73 GROUP BY B.NO_KEC,B.NAMA_KEC ORDER BY B.NO_KEC) C ON A.NO_KEC = B.NO_KEC AND B.NO_KEC =C.NO_KEC
            		WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS R1
					, CASE WHEN C.JUMLAH IS NULL THEN 0 ELSE C.JUMLAH END AS R2
          			, B.JUMLAH - C.JUMLAH AS R3
          			, CASE WHEN B.LK IS NULL THEN 0 ELSE B.LK END AS R1_LK
					, CASE WHEN B.PR IS NULL THEN 0 ELSE B.PR END AS R1_PR
					, CASE WHEN C.LK IS NULL THEN 0 ELSE C.LK END AS R2_LK
					, CASE WHEN C.PR IS NULL THEN 0 ELSE C.PR END AS R2_PR
					, B.LK - C.LK AS R3_LK
					, B.PR - C.PR AS R3_PR
          			, CONCAT(TO_CHAR(ROUND(C.JUMLAH/B.JUMLAH, 4)*100),'%') AS PRS 
					FROM SETUP_KEL A LEFT JOIN
					 ";
						if($no_kel == 0){    
					$sql .=" (SELECT 
						B.NO_KEL
						, B.NAMA_KEL 
						, SUM(CASE WHEN A.JENIS_KLMIN = 1 THEN 1 ELSE 0 END) LK 
						, SUM(CASE WHEN A.JENIS_KLMIN = 2 THEN 1 ELSE 0 END) PR
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM ".$dkb_bio." A WHERE A.TGL_LHR<=ADD_MONTHS(TO_DATE('".$cut_off_dkb."','DD/MM/YYYY'),-12*18) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL  WHERE 1=1  AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL
						    LEFT JOIN
						    (SELECT 
						B.NO_KEL
						, B.NAMA_KEL 
						, SUM(CASE WHEN A.JENIS_KLMIN = 1 THEN 1 ELSE 0 END) LK 
						, SUM(CASE WHEN A.JENIS_KLMIN = 2 THEN 1 ELSE 0 END) PR
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM ".$dkb_bio." A WHERE A.TGL_LHR<=ADD_MONTHS(TO_DATE('".$cut_off_dkb."','DD/MM/YYYY'),-12*18) AND EXISTS (SELECT 1 FROM (SELECT TO_CHAR(NIK) AS NIK FROM BIODATA_WNI C WHERE C.AKTA_LHR = 2 UNION ALL SELECT C.BAYI_NIK AS NIK FROM CAPIL_LAHIR C) C WHERE TO_CHAR(A.NIK) = C.NIK ) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL  WHERE 1=1 AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) C ON A.NO_KEL = B.NO_KEL AND B.NO_KEL =C.NO_KEL
						    WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" (SELECT 
						B.NO_KEL
						, B.NAMA_KEL 
						, SUM(CASE WHEN A.JENIS_KLMIN = 1 THEN 1 ELSE 0 END) LK 
						, SUM(CASE WHEN A.JENIS_KLMIN = 2 THEN 1 ELSE 0 END) PR
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM ".$dkb_bio." A WHERE A.TGL_LHR<=ADD_MONTHS(TO_DATE('".$cut_off_dkb."','DD/MM/YYYY'),-12*18) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL  WHERE 1=1  AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL = ".$no_kel."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL
						    LEFT JOIN
						    (SELECT 
						B.NO_KEL
						, B.NAMA_KEL 
						, SUM(CASE WHEN A.JENIS_KLMIN = 1 THEN 1 ELSE 0 END) LK 
						, SUM(CASE WHEN A.JENIS_KLMIN = 2 THEN 1 ELSE 0 END) PR
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM ".$dkb_bio." A WHERE A.TGL_LHR<=ADD_MONTHS(TO_DATE('".$cut_off_dkb."','DD/MM/YYYY'),-12*18) AND EXISTS (SELECT 1 FROM (SELECT TO_CHAR(NIK) AS NIK FROM BIODATA_WNI C WHERE C.AKTA_LHR = 2 UNION ALL SELECT C.BAYI_NIK AS NIK FROM CAPIL_LAHIR C) C WHERE TO_CHAR(A.NIK) = C.NIK ) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL  WHERE 1=1 AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) C ON A.NO_KEL = B.NO_KEL AND B.NO_KEL =C.NO_KEL
						    WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::select($sql);
               return $r;
		}

		
		public function mst_get_cakupan_akta_18_pelayanan($no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS R1
					, CASE WHEN C.JUMLAH IS NULL THEN 0 ELSE C.JUMLAH END AS R2
          			, B.JUMLAH - C.JUMLAH AS R3
          			, CASE WHEN B.LK IS NULL THEN 0 ELSE B.LK END AS R1_LK
					, CASE WHEN B.PR IS NULL THEN 0 ELSE B.PR END AS R1_PR
					, CASE WHEN C.LK IS NULL THEN 0 ELSE C.LK END AS R2_LK
					, CASE WHEN C.PR IS NULL THEN 0 ELSE C.PR END AS R2_PR
					, B.LK - C.LK AS R3_LK
					, B.PR - C.PR AS R3_PR
          			, CONCAT(TO_CHAR(ROUND(C.JUMLAH/B.JUMLAH, 4)*100),'%') AS PRS 
					FROM SETUP_KEC A LEFT JOIN
					(SELECT 
						B.NO_KEC 
						,B.NAMA_KEC 
						, SUM(CASE WHEN A.JENIS_KLMIN = 1 THEN 1 ELSE 0 END) LK 
						, SUM(CASE WHEN A.JENIS_KLMIN = 2 THEN 1 ELSE 0 END) PR
						, COUNT(1) JUMLAH
						FROM SETUP_KEC B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM BIODATA_WNI A WHERE A.TGL_LHR<=ADD_MONTHS(SYSDATE,-12*18) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC  
			            WHERE 1=1 AND A.NO_PROP = 32 AND A.NO_KAB =73 GROUP BY B.NO_KEC,B.NAMA_KEC ORDER BY B.NO_KEC) B ON A.NO_KEC = B.NO_KEC
			            LEFT JOIN
					(SELECT 
						B.NO_KEC 
						,B.NAMA_KEC 
						, SUM(CASE WHEN A.JENIS_KLMIN = 1 THEN 1 ELSE 0 END) LK 
						, SUM(CASE WHEN A.JENIS_KLMIN = 2 THEN 1 ELSE 0 END) PR
						, COUNT(1) JUMLAH
						FROM SETUP_KEC B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM BIODATA_WNI A WHERE A.TGL_LHR<=ADD_MONTHS(SYSDATE,-12*18) AND EXISTS (SELECT 1 FROM (SELECT TO_CHAR(NIK) AS NIK FROM BIODATA_WNI C WHERE C.AKTA_LHR = 2 UNION ALL SELECT C.BAYI_NIK AS NIK FROM CAPIL_LAHIR C) C WHERE TO_CHAR(A.NIK) = C.NIK ) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC  
            		WHERE 1=1 AND A.NO_PROP = 32 AND A.NO_KAB =73 GROUP BY B.NO_KEC,B.NAMA_KEC ORDER BY B.NO_KEC) C ON A.NO_KEC = B.NO_KEC AND B.NO_KEC =C.NO_KEC
            		WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS R1
					, CASE WHEN C.JUMLAH IS NULL THEN 0 ELSE C.JUMLAH END AS R2
          			, B.JUMLAH - C.JUMLAH AS R3
          			, CASE WHEN B.LK IS NULL THEN 0 ELSE B.LK END AS R1_LK
					, CASE WHEN B.PR IS NULL THEN 0 ELSE B.PR END AS R1_PR
					, CASE WHEN C.LK IS NULL THEN 0 ELSE C.LK END AS R2_LK
					, CASE WHEN C.PR IS NULL THEN 0 ELSE C.PR END AS R2_PR
					, B.LK - C.LK AS R3_LK
					, B.PR - C.PR AS R3_PR
          			, CONCAT(TO_CHAR(ROUND(C.JUMLAH/B.JUMLAH, 4)*100),'%') AS PRS 
					FROM SETUP_KEL A LEFT JOIN
					 ";
						if($no_kel == 0){    
					$sql .=" (SELECT 
						B.NO_KEL
						, B.NAMA_KEL 
						, SUM(CASE WHEN A.JENIS_KLMIN = 1 THEN 1 ELSE 0 END) LK 
						, SUM(CASE WHEN A.JENIS_KLMIN = 2 THEN 1 ELSE 0 END) PR
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM BIODATA_WNI A WHERE A.TGL_LHR<=ADD_MONTHS(SYSDATE,-12*18) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL  WHERE 1=1  AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL
						    LEFT JOIN
						    (SELECT 
						B.NO_KEL
						, B.NAMA_KEL 
						, SUM(CASE WHEN A.JENIS_KLMIN = 1 THEN 1 ELSE 0 END) LK 
						, SUM(CASE WHEN A.JENIS_KLMIN = 2 THEN 1 ELSE 0 END) PR
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM BIODATA_WNI A WHERE A.TGL_LHR<=ADD_MONTHS(SYSDATE,-12*18) AND EXISTS (SELECT 1 FROM (SELECT TO_CHAR(NIK) AS NIK FROM BIODATA_WNI C WHERE C.AKTA_LHR = 2 UNION ALL SELECT C.BAYI_NIK AS NIK FROM CAPIL_LAHIR C) C WHERE TO_CHAR(A.NIK) = C.NIK ) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL  WHERE 1=1 AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) C ON A.NO_KEL = B.NO_KEL AND B.NO_KEL =C.NO_KEL
						    WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" (SELECT 
						B.NO_KEL
						, B.NAMA_KEL 
						, SUM(CASE WHEN A.JENIS_KLMIN = 1 THEN 1 ELSE 0 END) LK 
						, SUM(CASE WHEN A.JENIS_KLMIN = 2 THEN 1 ELSE 0 END) PR
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM BIODATA_WNI A WHERE A.TGL_LHR<=ADD_MONTHS(SYSDATE,-12*18) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL  WHERE 1=1  AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL = ".$no_kel."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL
						    LEFT JOIN
						    (SELECT 
						B.NO_KEL
						, B.NAMA_KEL 
						, SUM(CASE WHEN A.JENIS_KLMIN = 1 THEN 1 ELSE 0 END) LK 
						, SUM(CASE WHEN A.JENIS_KLMIN = 2 THEN 1 ELSE 0 END) PR
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM BIODATA_WNI A WHERE A.TGL_LHR<=ADD_MONTHS(SYSDATE,-12*18) AND EXISTS (SELECT 1 FROM (SELECT TO_CHAR(NIK) AS NIK FROM BIODATA_WNI C WHERE C.AKTA_LHR = 2 UNION ALL SELECT C.BAYI_NIK AS NIK FROM CAPIL_LAHIR C) C WHERE TO_CHAR(A.NIK) = C.NIK ) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL  WHERE 1=1 AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) C ON A.NO_KEL = B.NO_KEL AND B.NO_KEL =C.NO_KEL
						    WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::connection('db222')->select($sql);
               return $r;
		}

		
		public function mst_get_cakupan_akta($no_kec,$no_kel){
			$dkb_bio = Shr::do_get_dkb_bio();
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS R1
					, CASE WHEN C.JUMLAH IS NULL THEN 0 ELSE C.JUMLAH END AS R2
					, B.JUMLAH - C.JUMLAH AS R3
					, CASE WHEN B.LK IS NULL THEN 0 ELSE B.LK END AS R1_LK
					, CASE WHEN B.PR IS NULL THEN 0 ELSE B.PR END AS R1_PR
					, CASE WHEN C.LK IS NULL THEN 0 ELSE C.LK END AS R2_LK
					, CASE WHEN C.PR IS NULL THEN 0 ELSE C.PR END AS R2_PR
					, B.LK - C.LK AS R3_LK
					, B.PR - C.PR AS R3_PR
          			, CONCAT(TO_CHAR(ROUND(C.JUMLAH/B.JUMLAH, 4)*100),'%') AS PRS 
					FROM SETUP_KEC A LEFT JOIN
					(SELECT 
						B.NO_KEC 
						,B.NAMA_KEC 
						, SUM(CASE WHEN A.JENIS_KLMIN = 1 THEN 1 ELSE 0 END) LK 
						, SUM(CASE WHEN A.JENIS_KLMIN = 2 THEN 1 ELSE 0 END) PR
						, COUNT(1) JUMLAH
						FROM SETUP_KEC B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM ".$dkb_bio." A WHERE (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC  
			            WHERE 1=1 AND A.NO_PROP = 32 AND A.NO_KAB =73 GROUP BY B.NO_KEC,B.NAMA_KEC ORDER BY B.NO_KEC) B ON A.NO_KEC = B.NO_KEC
			            LEFT JOIN
					(SELECT 
						B.NO_KEC 
						,B.NAMA_KEC 
						, SUM(CASE WHEN A.JENIS_KLMIN = 1 THEN 1 ELSE 0 END) LK 
						, SUM(CASE WHEN A.JENIS_KLMIN = 2 THEN 1 ELSE 0 END) PR
						, COUNT(1) JUMLAH
						FROM SETUP_KEC B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM ".$dkb_bio." A WHERE EXISTS (SELECT 1 FROM (SELECT TO_CHAR(NIK) AS NIK FROM BIODATA_WNI C WHERE C.AKTA_LHR = 2 UNION ALL SELECT C.BAYI_NIK AS NIK FROM CAPIL_LAHIR C) C WHERE TO_CHAR(A.NIK) = C.NIK ) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC  
            		WHERE 1=1 AND A.NO_PROP = 32 AND A.NO_KAB =73 GROUP BY B.NO_KEC,B.NAMA_KEC ORDER BY B.NO_KEC) C ON A.NO_KEC = B.NO_KEC AND B.NO_KEC =C.NO_KEC
            		WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS R1
					, CASE WHEN C.JUMLAH IS NULL THEN 0 ELSE C.JUMLAH END AS R2
					, B.JUMLAH - C.JUMLAH AS R3
					, CASE WHEN B.LK IS NULL THEN 0 ELSE B.LK END AS R1_LK
					, CASE WHEN B.PR IS NULL THEN 0 ELSE B.PR END AS R1_PR
					, CASE WHEN C.LK IS NULL THEN 0 ELSE C.LK END AS R2_LK
					, CASE WHEN C.PR IS NULL THEN 0 ELSE C.PR END AS R2_PR
					, B.LK - C.LK AS R3_LK
					, B.PR - C.PR AS R3_PR
          			, CONCAT(TO_CHAR(ROUND(C.JUMLAH/B.JUMLAH, 4)*100),'%') AS PRS 
					FROM SETUP_KEL A LEFT JOIN
					 ";
						if($no_kel == 0){    
					$sql .=" (SELECT 
						B.NO_KEL
						, B.NAMA_KEL
						, SUM(CASE WHEN A.JENIS_KLMIN = 1 THEN 1 ELSE 0 END) LK 
						, SUM(CASE WHEN A.JENIS_KLMIN = 2 THEN 1 ELSE 0 END) PR
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM ".$dkb_bio." A WHERE (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL  WHERE 1=1  AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL
						    LEFT JOIN
						    (SELECT 
						B.NO_KEL
						, B.NAMA_KEL 
						, SUM(CASE WHEN A.JENIS_KLMIN = 1 THEN 1 ELSE 0 END) LK 
						, SUM(CASE WHEN A.JENIS_KLMIN = 2 THEN 1 ELSE 0 END) PR
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM ".$dkb_bio." A WHERE EXISTS (SELECT 1 FROM (SELECT TO_CHAR(NIK) AS NIK FROM BIODATA_WNI C WHERE C.AKTA_LHR = 2 UNION ALL SELECT C.BAYI_NIK AS NIK FROM CAPIL_LAHIR C) C WHERE TO_CHAR(A.NIK) = C.NIK ) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL  WHERE 1=1 AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) C ON A.NO_KEL = B.NO_KEL AND B.NO_KEL =C.NO_KEL
						    WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" (SELECT 
						B.NO_KEL
						, B.NAMA_KEL 
						, SUM(CASE WHEN A.JENIS_KLMIN = 1 THEN 1 ELSE 0 END) LK 
						, SUM(CASE WHEN A.JENIS_KLMIN = 2 THEN 1 ELSE 0 END) PR
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM ".$dkb_bio." A WHERE (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL  WHERE 1=1  AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL = ".$no_kel."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL
						    LEFT JOIN
						    (SELECT 
						B.NO_KEL
						, B.NAMA_KEL 
						, SUM(CASE WHEN A.JENIS_KLMIN = 1 THEN 1 ELSE 0 END) LK 
						, SUM(CASE WHEN A.JENIS_KLMIN = 2 THEN 1 ELSE 0 END) PR
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM ".$dkb_bio." A WHERE EXISTS (SELECT 1 FROM (SELECT TO_CHAR(NIK) AS NIK FROM BIODATA_WNI C WHERE C.AKTA_LHR = 2 UNION ALL SELECT C.BAYI_NIK AS NIK FROM CAPIL_LAHIR C) C WHERE TO_CHAR(A.NIK) = C.NIK ) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL  WHERE 1=1 AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) C ON A.NO_KEL = B.NO_KEL AND B.NO_KEL =C.NO_KEL
						    WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::select($sql);
               return $r;
		}
		public function mst_get_cakupan_akta_pelayanan($no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS R1
					, CASE WHEN C.JUMLAH IS NULL THEN 0 ELSE C.JUMLAH END AS R2
					, B.JUMLAH - C.JUMLAH AS R3
					, CASE WHEN B.LK IS NULL THEN 0 ELSE B.LK END AS R1_LK
					, CASE WHEN B.PR IS NULL THEN 0 ELSE B.PR END AS R1_PR
					, CASE WHEN C.LK IS NULL THEN 0 ELSE C.LK END AS R2_LK
					, CASE WHEN C.PR IS NULL THEN 0 ELSE C.PR END AS R2_PR
					, B.LK - C.LK AS R3_LK
					, B.PR - C.PR AS R3_PR
          			, CONCAT(TO_CHAR(ROUND(C.JUMLAH/B.JUMLAH, 4)*100),'%') AS PRS 
					FROM SETUP_KEC A LEFT JOIN
					(SELECT 
						B.NO_KEC 
						,B.NAMA_KEC
						, SUM(CASE WHEN A.JENIS_KLMIN = 1 THEN 1 ELSE 0 END) LK 
						, SUM(CASE WHEN A.JENIS_KLMIN = 2 THEN 1 ELSE 0 END) PR
						, COUNT(1) JUMLAH
						FROM SETUP_KEC B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM BIODATA_WNI A WHERE (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC  
			            WHERE 1=1 AND A.NO_PROP = 32 AND A.NO_KAB =73 GROUP BY B.NO_KEC,B.NAMA_KEC ORDER BY B.NO_KEC) B ON A.NO_KEC = B.NO_KEC
			            LEFT JOIN
					(SELECT 
						B.NO_KEC 
						,B.NAMA_KEC
						, SUM(CASE WHEN A.JENIS_KLMIN = 1 THEN 1 ELSE 0 END) LK 
						, SUM(CASE WHEN A.JENIS_KLMIN = 2 THEN 1 ELSE 0 END) PR
						, COUNT(1) JUMLAH
						FROM SETUP_KEC B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM BIODATA_WNI A WHERE EXISTS (SELECT 1 FROM (SELECT TO_CHAR(NIK) AS NIK FROM BIODATA_WNI C WHERE C.AKTA_LHR = 2 UNION ALL SELECT C.BAYI_NIK AS NIK FROM CAPIL_LAHIR C) C WHERE TO_CHAR(A.NIK) = C.NIK ) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC  
            		WHERE 1=1 AND A.NO_PROP = 32 AND A.NO_KAB =73 GROUP BY B.NO_KEC,B.NAMA_KEC ORDER BY B.NO_KEC) C ON A.NO_KEC = B.NO_KEC AND B.NO_KEC =C.NO_KEC
            		WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS R1
					, CASE WHEN C.JUMLAH IS NULL THEN 0 ELSE C.JUMLAH END AS R2
					, B.JUMLAH - C.JUMLAH AS R3
					, CASE WHEN B.LK IS NULL THEN 0 ELSE B.LK END AS R1_LK
					, CASE WHEN B.PR IS NULL THEN 0 ELSE B.PR END AS R1_PR
					, CASE WHEN C.LK IS NULL THEN 0 ELSE C.LK END AS R2_LK
					, CASE WHEN C.PR IS NULL THEN 0 ELSE C.PR END AS R2_PR
					, B.LK - C.LK AS R3_LK
					, B.PR - C.PR AS R3_PR
          			, CONCAT(TO_CHAR(ROUND(C.JUMLAH/B.JUMLAH, 4)*100),'%') AS PRS 
					FROM SETUP_KEL A LEFT JOIN
					 ";
						if($no_kel == 0){    
					$sql .=" (SELECT 
						B.NO_KEL
						, B.NAMA_KEL
						, SUM(CASE WHEN A.JENIS_KLMIN = 1 THEN 1 ELSE 0 END) LK 
						, SUM(CASE WHEN A.JENIS_KLMIN = 2 THEN 1 ELSE 0 END) PR
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM BIODATA_WNI A WHERE (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL  WHERE 1=1  AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL
						    LEFT JOIN
						    (SELECT 
						B.NO_KEL
						, B.NAMA_KEL
						, SUM(CASE WHEN A.JENIS_KLMIN = 1 THEN 1 ELSE 0 END) LK 
						, SUM(CASE WHEN A.JENIS_KLMIN = 2 THEN 1 ELSE 0 END) PR
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM BIODATA_WNI A WHERE EXISTS (SELECT 1 FROM (SELECT TO_CHAR(NIK) AS NIK FROM BIODATA_WNI C WHERE C.AKTA_LHR = 2 UNION ALL SELECT C.BAYI_NIK AS NIK FROM CAPIL_LAHIR C) C WHERE TO_CHAR(A.NIK) = C.NIK ) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL  WHERE 1=1 AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) C ON A.NO_KEL = B.NO_KEL AND B.NO_KEL =C.NO_KEL
						    WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" (SELECT 
						B.NO_KEL
						, B.NAMA_KEL
						, SUM(CASE WHEN A.JENIS_KLMIN = 1 THEN 1 ELSE 0 END) LK 
						, SUM(CASE WHEN A.JENIS_KLMIN = 2 THEN 1 ELSE 0 END) PR
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM BIODATA_WNI A WHERE (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL  WHERE 1=1  AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL = ".$no_kel."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL
						    LEFT JOIN
						    (SELECT 
						B.NO_KEL
						, B.NAMA_KEL 
						, COUNT(1) JUMLAH
						, SUM(CASE WHEN A.JENIS_KLMIN = 1 THEN 1 ELSE 0 END) LK 
						, SUM(CASE WHEN A.JENIS_KLMIN = 2 THEN 1 ELSE 0 END) PR
						FROM SETUP_KEL B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM BIODATA_WNI A WHERE EXISTS (SELECT 1 FROM (SELECT TO_CHAR(NIK) AS NIK FROM BIODATA_WNI C WHERE C.AKTA_LHR = 2 UNION ALL SELECT C.BAYI_NIK AS NIK FROM CAPIL_LAHIR C) C WHERE TO_CHAR(A.NIK) = C.NIK ) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL  WHERE 1=1 AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) C ON A.NO_KEL = B.NO_KEL AND B.NO_KEL =C.NO_KEL
						    WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::connection('db222')->select($sql);
               return $r;
		}

		
		function mst_get_lap_ktp($bln){
			$sql = "SELECT BLN, NO_WIL, NAMA_WIL, JUMDUK_LK,JUMDUK_PR, JUMDUK, WKP_LK,WKP_PR, WKP,SUDAHRKM, BLMRKM,SUDAHCTK,PRR,DUPREC, BIOCAPTURE,SFE,GAGALRKM  FROM SIAK_LAP_KTP WHERE TO_CHAR(TO_DATE(BLN,'DD-MM-YYYY'),'MM/YYYY') = '$bln'";
				$r = DB::select($sql);
               return $r;
		}
		public function mst_get_cakupan_perkawinan($no_kec,$no_kel){
			$dkb_bio = Shr::do_get_dkb_bio();
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS R1
					, CASE WHEN C.JUMLAH IS NULL THEN 0 ELSE C.JUMLAH END AS R2
          			, B.JUMLAH - C.JUMLAH AS R3
          			, CONCAT(TO_CHAR(ROUND(C.JUMLAH/B.JUMLAH, 4)*100),'%') AS PRS 
					FROM SETUP_KEC A LEFT JOIN
					(SELECT 
						B.NO_KEC 
						,B.NAMA_KEC 
						, COUNT(1) JUMLAH
						FROM SETUP_KEC B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM ".$dkb_bio." A WHERE A.STAT_KWN IN (2) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC  
			            WHERE 1=1 AND A.NO_PROP = 32 AND A.NO_KAB =73 GROUP BY B.NO_KEC,B.NAMA_KEC ORDER BY B.NO_KEC) B ON A.NO_KEC = B.NO_KEC
			            LEFT JOIN
					(SELECT 
						B.NO_KEC 
						,B.NAMA_KEC 
						, COUNT(1) JUMLAH
						FROM SETUP_KEC B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM ".$dkb_bio." A WHERE A.STAT_KWN IN (2) AND  (A.AKTA_KWN = 2)) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC  
            		WHERE 1=1 AND A.NO_PROP = 32 AND A.NO_KAB =73 GROUP BY B.NO_KEC,B.NAMA_KEC ORDER BY B.NO_KEC) C ON A.NO_KEC = B.NO_KEC AND B.NO_KEC =C.NO_KEC
            		WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS R1
					, CASE WHEN C.JUMLAH IS NULL THEN 0 ELSE C.JUMLAH END AS R2
          			, B.JUMLAH - C.JUMLAH AS R3
          			, CONCAT(TO_CHAR(ROUND(C.JUMLAH/B.JUMLAH, 4)*100),'%') AS PRS 
					FROM SETUP_KEL A LEFT JOIN
					 ";
						if($no_kel == 0){    
					$sql .=" (SELECT 
						B.NO_KEL
						, B.NAMA_KEL 
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM ".$dkb_bio." A WHERE A.STAT_KWN IN (2) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL  WHERE 1=1  AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL
						    LEFT JOIN
						    (SELECT 
						B.NO_KEL
						, B.NAMA_KEL 
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM ".$dkb_bio." A WHERE A.STAT_KWN IN (2) AND  (A.AKTA_KWN = 2)) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL  WHERE 1=1 AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) C ON A.NO_KEL = B.NO_KEL AND B.NO_KEL =C.NO_KEL
						    WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" (SELECT 
						B.NO_KEL
						, B.NAMA_KEL 
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM ".$dkb_bio." A WHERE A.STAT_KWN IN (2) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL  WHERE 1=1  AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL = ".$no_kel."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL
						    LEFT JOIN
						    (SELECT 
						B.NO_KEL
						, B.NAMA_KEL 
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM ".$dkb_bio." A WHERE A.STAT_KWN IN (2) AND  (A.AKTA_KWN = 2)) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL  WHERE 1=1 AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) C ON A.NO_KEL = B.NO_KEL AND B.NO_KEL =C.NO_KEL
						    WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::select($sql);
               return $r;
		}
		public function mst_get_cakupan_perkawinan_pelayanan($no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS R1
					, CASE WHEN C.JUMLAH IS NULL THEN 0 ELSE C.JUMLAH END AS R2
          			, B.JUMLAH - C.JUMLAH AS R3
          			, CONCAT(TO_CHAR(ROUND(C.JUMLAH/B.JUMLAH, 4)*100),'%') AS PRS 
					FROM SETUP_KEC A LEFT JOIN
					(SELECT 
						B.NO_KEC 
						,B.NAMA_KEC 
						, COUNT(1) JUMLAH
						FROM SETUP_KEC B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM BIODATA_WNI A WHERE A.STAT_KWN IN (2) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC  
			            WHERE 1=1 AND A.NO_PROP = 32 AND A.NO_KAB =73 GROUP BY B.NO_KEC,B.NAMA_KEC ORDER BY B.NO_KEC) B ON A.NO_KEC = B.NO_KEC
			            LEFT JOIN
					(SELECT 
						B.NO_KEC 
						,B.NAMA_KEC 
						, COUNT(1) JUMLAH
						FROM SETUP_KEC B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM BIODATA_WNI A WHERE A.STAT_KWN IN (2) AND  (A.AKTA_KWN = 2)) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC  
            		WHERE 1=1 AND A.NO_PROP = 32 AND A.NO_KAB =73 GROUP BY B.NO_KEC,B.NAMA_KEC ORDER BY B.NO_KEC) C ON A.NO_KEC = B.NO_KEC AND B.NO_KEC =C.NO_KEC
            		WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS R1
					, CASE WHEN C.JUMLAH IS NULL THEN 0 ELSE C.JUMLAH END AS R2
          			, B.JUMLAH - C.JUMLAH AS R3
          			, CONCAT(TO_CHAR(ROUND(C.JUMLAH/B.JUMLAH, 4)*100),'%') AS PRS 
					FROM SETUP_KEL A LEFT JOIN
					 ";
						if($no_kel == 0){    
					$sql .=" (SELECT 
						B.NO_KEL
						, B.NAMA_KEL 
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM BIODATA_WNI A WHERE A.STAT_KWN IN (2) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL  WHERE 1=1  AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL
						    LEFT JOIN
						    (SELECT 
						B.NO_KEL
						, B.NAMA_KEL 
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM BIODATA_WNI A WHERE A.STAT_KWN IN (2) AND  (A.AKTA_KWN = 2)) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL  WHERE 1=1 AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) C ON A.NO_KEL = B.NO_KEL AND B.NO_KEL =C.NO_KEL
						    WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" (SELECT 
						B.NO_KEL
						, B.NAMA_KEL 
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM BIODATA_WNI A WHERE A.STAT_KWN IN (2) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL  WHERE 1=1  AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL = ".$no_kel."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL
						    LEFT JOIN
						    (SELECT 
						B.NO_KEL
						, B.NAMA_KEL 
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM BIODATA_WNI A WHERE A.STAT_KWN IN (2) AND  (A.AKTA_KWN = 2)) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL  WHERE 1=1 AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) C ON A.NO_KEL = B.NO_KEL AND B.NO_KEL =C.NO_KEL
						    WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::connection('db222')->select($sql);
               return $r;
		}

		
		public function mst_get_cakupan_perkawinan_non_muslim($no_kec,$no_kel){
			$dkb_bio = Shr::do_get_dkb_bio();
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS R1
					, CASE WHEN C.JUMLAH IS NULL THEN 0 ELSE C.JUMLAH END AS R2
          			, B.JUMLAH - C.JUMLAH AS R3
          			, CONCAT(TO_CHAR(ROUND(C.JUMLAH/B.JUMLAH, 4)*100),'%') AS PRS 
					FROM SETUP_KEC A LEFT JOIN
					(SELECT 
						B.NO_KEC 
						,B.NAMA_KEC 
						, COUNT(1) JUMLAH
						FROM SETUP_KEC B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM ".$dkb_bio." A WHERE A.AGAMA <> 1 AND A.STAT_KWN IN (2) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC  
			            WHERE 1=1 AND A.NO_PROP = 32 AND A.NO_KAB =73 GROUP BY B.NO_KEC,B.NAMA_KEC ORDER BY B.NO_KEC) B ON A.NO_KEC = B.NO_KEC
			            LEFT JOIN
					(SELECT 
						B.NO_KEC 
						,B.NAMA_KEC 
						, COUNT(1) JUMLAH
						FROM SETUP_KEC B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM ".$dkb_bio." A WHERE A.AGAMA <> 1 AND A.STAT_KWN IN (2) AND  (A.AKTA_KWN = 2)) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC  
            		WHERE 1=1 AND A.NO_PROP = 32 AND A.NO_KAB =73 GROUP BY B.NO_KEC,B.NAMA_KEC ORDER BY B.NO_KEC) C ON A.NO_KEC = B.NO_KEC AND B.NO_KEC =C.NO_KEC
            		WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS R1
					, CASE WHEN C.JUMLAH IS NULL THEN 0 ELSE C.JUMLAH END AS R2
          			, B.JUMLAH - C.JUMLAH AS R3
          			, CONCAT(TO_CHAR(ROUND(C.JUMLAH/B.JUMLAH, 4)*100),'%') AS PRS 
					FROM SETUP_KEL A LEFT JOIN
					 ";
						if($no_kel == 0){    
					$sql .=" (SELECT 
						B.NO_KEL
						, B.NAMA_KEL 
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM ".$dkb_bio." A WHERE A.AGAMA <> 1 AND A.STAT_KWN IN (2) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL  WHERE 1=1  AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL
						    LEFT JOIN
						    (SELECT 
						B.NO_KEL
						, B.NAMA_KEL 
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM ".$dkb_bio." A WHERE A.AGAMA <> 1 AND A.STAT_KWN IN (2) AND  (A.AKTA_KWN = 2)) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL  WHERE 1=1 AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) C ON A.NO_KEL = B.NO_KEL AND B.NO_KEL =C.NO_KEL
						    WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" (SELECT 
						B.NO_KEL
						, B.NAMA_KEL 
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM ".$dkb_bio." A WHERE A.AGAMA <> 1 AND A.STAT_KWN IN (2) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL  WHERE 1=1  AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL = ".$no_kel."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL
						    LEFT JOIN
						    (SELECT 
						B.NO_KEL
						, B.NAMA_KEL 
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM ".$dkb_bio." A WHERE A.AGAMA <> 1 AND A.STAT_KWN IN (2) AND  (A.AKTA_KWN = 2)) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL  WHERE 1=1 AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) C ON A.NO_KEL = B.NO_KEL AND B.NO_KEL =C.NO_KEL
						    WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::select($sql);
               return $r;
		}

		
		public function mst_get_cakupan_perkawinan_non_muslim_pelayanan($no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS R1
					, CASE WHEN C.JUMLAH IS NULL THEN 0 ELSE C.JUMLAH END AS R2
          			, B.JUMLAH - C.JUMLAH AS R3
          			, CONCAT(TO_CHAR(ROUND(C.JUMLAH/B.JUMLAH, 4)*100),'%') AS PRS 
					FROM SETUP_KEC A LEFT JOIN
					(SELECT 
						B.NO_KEC 
						,B.NAMA_KEC 
						, COUNT(1) JUMLAH
						FROM SETUP_KEC B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM BIODATA_WNI A WHERE A.AGAMA <> 1 AND A.STAT_KWN IN (2) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC  
			            WHERE 1=1 AND A.NO_PROP = 32 AND A.NO_KAB =73 GROUP BY B.NO_KEC,B.NAMA_KEC ORDER BY B.NO_KEC) B ON A.NO_KEC = B.NO_KEC
			            LEFT JOIN
					(SELECT 
						B.NO_KEC 
						,B.NAMA_KEC 
						, COUNT(1) JUMLAH
						FROM SETUP_KEC B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM BIODATA_WNI A WHERE A.AGAMA <> 1 AND A.STAT_KWN IN (2) AND  (A.AKTA_KWN = 2)) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC  
            		WHERE 1=1 AND A.NO_PROP = 32 AND A.NO_KAB =73 GROUP BY B.NO_KEC,B.NAMA_KEC ORDER BY B.NO_KEC) C ON A.NO_KEC = B.NO_KEC AND B.NO_KEC =C.NO_KEC
            		WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS R1
					, CASE WHEN C.JUMLAH IS NULL THEN 0 ELSE C.JUMLAH END AS R2
          			, B.JUMLAH - C.JUMLAH AS R3
          			, CONCAT(TO_CHAR(ROUND(C.JUMLAH/B.JUMLAH, 4)*100),'%') AS PRS 
					FROM SETUP_KEL A LEFT JOIN
					 ";
						if($no_kel == 0){    
					$sql .=" (SELECT 
						B.NO_KEL
						, B.NAMA_KEL 
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM BIODATA_WNI A WHERE A.AGAMA <> 1 AND A.STAT_KWN IN (2) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL  WHERE 1=1  AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL
						    LEFT JOIN
						    (SELECT 
						B.NO_KEL
						, B.NAMA_KEL 
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM BIODATA_WNI A WHERE A.AGAMA <> 1 AND A.STAT_KWN IN (2) AND  (A.AKTA_KWN = 2)) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL  WHERE 1=1 AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) C ON A.NO_KEL = B.NO_KEL AND B.NO_KEL =C.NO_KEL
						    WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" (SELECT 
						B.NO_KEL
						, B.NAMA_KEL 
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM BIODATA_WNI A WHERE A.AGAMA <> 1 AND A.STAT_KWN IN (2) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL  WHERE 1=1  AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL = ".$no_kel."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL
						    LEFT JOIN
						    (SELECT 
						B.NO_KEL
						, B.NAMA_KEL 
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM BIODATA_WNI A WHERE A.AGAMA <> 1 AND A.STAT_KWN IN (2) AND  (A.AKTA_KWN = 2)) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL  WHERE 1=1 AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) C ON A.NO_KEL = B.NO_KEL AND B.NO_KEL =C.NO_KEL
						    WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::connection('db222')->select($sql);
               return $r;
		}

		
		public function mst_get_cakupan_perkawinan_muslim($no_kec,$no_kel){
			$dkb_bio = Shr::do_get_dkb_bio();
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS R1
					, CASE WHEN C.JUMLAH IS NULL THEN 0 ELSE C.JUMLAH END AS R2
          			, B.JUMLAH - C.JUMLAH AS R3
          			, CONCAT(TO_CHAR(ROUND(C.JUMLAH/B.JUMLAH, 4)*100),'%') AS PRS 
					FROM SETUP_KEC A LEFT JOIN
					(SELECT 
						B.NO_KEC 
						,B.NAMA_KEC 
						, COUNT(1) JUMLAH
						FROM SETUP_KEC B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM ".$dkb_bio." A WHERE A.AGAMA = 1 AND A.STAT_KWN IN (2) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC  
			            WHERE 1=1 AND A.NO_PROP = 32 AND A.NO_KAB =73 GROUP BY B.NO_KEC,B.NAMA_KEC ORDER BY B.NO_KEC) B ON A.NO_KEC = B.NO_KEC
			            LEFT JOIN
					(SELECT 
						B.NO_KEC 
						,B.NAMA_KEC 
						, COUNT(1) JUMLAH
						FROM SETUP_KEC B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM ".$dkb_bio." A WHERE A.AGAMA = 1 AND A.STAT_KWN IN (2) AND  (A.AKTA_KWN = 2)) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC  
            		WHERE 1=1 AND A.NO_PROP = 32 AND A.NO_KAB =73 GROUP BY B.NO_KEC,B.NAMA_KEC ORDER BY B.NO_KEC) C ON A.NO_KEC = B.NO_KEC AND B.NO_KEC =C.NO_KEC
            		WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS R1
					, CASE WHEN C.JUMLAH IS NULL THEN 0 ELSE C.JUMLAH END AS R2
          			, B.JUMLAH - C.JUMLAH AS R3
          			, CONCAT(TO_CHAR(ROUND(C.JUMLAH/B.JUMLAH, 4)*100),'%') AS PRS 
					FROM SETUP_KEL A LEFT JOIN
					 ";
						if($no_kel == 0){    
					$sql .=" (SELECT 
						B.NO_KEL
						, B.NAMA_KEL 
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM ".$dkb_bio." A WHERE A.AGAMA = 1 AND A.STAT_KWN IN (2) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL  WHERE 1=1  AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL
						    LEFT JOIN
						    (SELECT 
						B.NO_KEL
						, B.NAMA_KEL 
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM ".$dkb_bio." A WHERE A.AGAMA = 1 AND A.STAT_KWN IN (2) AND  (A.AKTA_KWN = 2)) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL  WHERE 1=1 AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) C ON A.NO_KEL = B.NO_KEL AND B.NO_KEL =C.NO_KEL
						    WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" (SELECT 
						B.NO_KEL
						, B.NAMA_KEL 
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM ".$dkb_bio." A WHERE A.AGAMA = 1 AND A.STAT_KWN IN (2) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL  WHERE 1=1  AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL = ".$no_kel."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL
						    LEFT JOIN
						    (SELECT 
						B.NO_KEL
						, B.NAMA_KEL 
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM ".$dkb_bio." A WHERE A.AGAMA = 1 AND A.STAT_KWN IN (2) AND  (A.AKTA_KWN = 2)) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL  WHERE 1=1 AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) C ON A.NO_KEL = B.NO_KEL AND B.NO_KEL =C.NO_KEL
						    WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::select($sql);
               return $r;
		}

		
		public function mst_get_cakupan_perkawinan_muslim_pelayanan($no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS R1
					, CASE WHEN C.JUMLAH IS NULL THEN 0 ELSE C.JUMLAH END AS R2
          			, B.JUMLAH - C.JUMLAH AS R3
          			, CONCAT(TO_CHAR(ROUND(C.JUMLAH/B.JUMLAH, 4)*100),'%') AS PRS 
					FROM SETUP_KEC A LEFT JOIN
					(SELECT 
						B.NO_KEC 
						,B.NAMA_KEC 
						, COUNT(1) JUMLAH
						FROM SETUP_KEC B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM BIODATA_WNI A WHERE A.AGAMA = 1 AND A.STAT_KWN IN (2) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC  
			            WHERE 1=1 AND A.NO_PROP = 32 AND A.NO_KAB =73 GROUP BY B.NO_KEC,B.NAMA_KEC ORDER BY B.NO_KEC) B ON A.NO_KEC = B.NO_KEC
			            LEFT JOIN
					(SELECT 
						B.NO_KEC 
						,B.NAMA_KEC 
						, COUNT(1) JUMLAH
						FROM SETUP_KEC B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM BIODATA_WNI A WHERE A.AGAMA = 1 AND A.STAT_KWN IN (2) AND  (A.AKTA_KWN = 2)) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC  
            		WHERE 1=1 AND A.NO_PROP = 32 AND A.NO_KAB =73 GROUP BY B.NO_KEC,B.NAMA_KEC ORDER BY B.NO_KEC) C ON A.NO_KEC = B.NO_KEC AND B.NO_KEC =C.NO_KEC
            		WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS R1
					, CASE WHEN C.JUMLAH IS NULL THEN 0 ELSE C.JUMLAH END AS R2
          			, B.JUMLAH - C.JUMLAH AS R3
          			, CONCAT(TO_CHAR(ROUND(C.JUMLAH/B.JUMLAH, 4)*100),'%') AS PRS 
					FROM SETUP_KEL A LEFT JOIN
					 ";
						if($no_kel == 0){    
					$sql .=" (SELECT 
						B.NO_KEL
						, B.NAMA_KEL 
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM BIODATA_WNI A WHERE A.AGAMA = 1 AND A.STAT_KWN IN (2) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL  WHERE 1=1  AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL
						    LEFT JOIN
						    (SELECT 
						B.NO_KEL
						, B.NAMA_KEL 
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM BIODATA_WNI A WHERE A.AGAMA = 1 AND A.STAT_KWN IN (2) AND  (A.AKTA_KWN = 2)) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL  WHERE 1=1 AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) C ON A.NO_KEL = B.NO_KEL AND B.NO_KEL =C.NO_KEL
						    WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" (SELECT 
						B.NO_KEL
						, B.NAMA_KEL 
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM BIODATA_WNI A WHERE A.AGAMA = 1 AND A.STAT_KWN IN (2) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL  WHERE 1=1  AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL = ".$no_kel."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL
						    LEFT JOIN
						    (SELECT 
						B.NO_KEL
						, B.NAMA_KEL 
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM BIODATA_WNI A WHERE A.AGAMA = 1 AND A.STAT_KWN IN (2) AND  (A.AKTA_KWN = 2)) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL  WHERE 1=1 AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) C ON A.NO_KEL = B.NO_KEL AND B.NO_KEL =C.NO_KEL
						    WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::connection('db222')->select($sql);
               return $r;
		}

		
		public function mst_get_cakupan_perceraian($no_kec,$no_kel){
			$dkb_bio = Shr::do_get_dkb_bio();
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS R1
					, CASE WHEN C.JUMLAH IS NULL THEN 0 ELSE C.JUMLAH END AS R2
          			, B.JUMLAH - C.JUMLAH AS R3
          			, CONCAT(TO_CHAR(ROUND(C.JUMLAH/B.JUMLAH, 4)*100),'%') AS PRS 
					FROM SETUP_KEC A LEFT JOIN
					(SELECT 
						B.NO_KEC 
						,B.NAMA_KEC 
						, COUNT(1) JUMLAH
						FROM SETUP_KEC B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM ".$dkb_bio." A WHERE A.STAT_KWN IN (3,4) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC  
			            WHERE 1=1 AND A.NO_PROP = 32 AND A.NO_KAB =73 GROUP BY B.NO_KEC,B.NAMA_KEC ORDER BY B.NO_KEC) B ON A.NO_KEC = B.NO_KEC
			            LEFT JOIN
					(SELECT 
						B.NO_KEC 
						,B.NAMA_KEC 
						, COUNT(1) JUMLAH
						FROM SETUP_KEC B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM ".$dkb_bio." A WHERE A.STAT_KWN IN (3,4) AND  (A.AKTA_CRAI = 2)) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC  
            		WHERE 1=1 AND A.NO_PROP = 32 AND A.NO_KAB =73 GROUP BY B.NO_KEC,B.NAMA_KEC ORDER BY B.NO_KEC) C ON A.NO_KEC = B.NO_KEC AND B.NO_KEC =C.NO_KEC
            		WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS R1
					, CASE WHEN C.JUMLAH IS NULL THEN 0 ELSE C.JUMLAH END AS R2
          			, B.JUMLAH - C.JUMLAH AS R3
          			, CONCAT(TO_CHAR(ROUND(C.JUMLAH/B.JUMLAH, 4)*100),'%') AS PRS 
					FROM SETUP_KEL A LEFT JOIN
					 ";
						if($no_kel == 0){    
					$sql .=" (SELECT 
						B.NO_KEL
						, B.NAMA_KEL 
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM ".$dkb_bio." A WHERE A.STAT_KWN IN (3,4) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL  WHERE 1=1  AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL
						    LEFT JOIN
						    (SELECT 
						B.NO_KEL
						, B.NAMA_KEL 
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM ".$dkb_bio." A WHERE A.STAT_KWN IN (3,4) AND  (A.AKTA_CRAI = 2)) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL  WHERE 1=1 AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) C ON A.NO_KEL = B.NO_KEL AND B.NO_KEL =C.NO_KEL
						    WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" (SELECT 
						B.NO_KEL
						, B.NAMA_KEL 
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM ".$dkb_bio." A WHERE A.STAT_KWN IN (3,4) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL  WHERE 1=1  AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL = ".$no_kel."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL
						    LEFT JOIN
						    (SELECT 
						B.NO_KEL
						, B.NAMA_KEL 
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM ".$dkb_bio." A WHERE A.STAT_KWN IN (3,4) AND  (A.AKTA_CRAI = 2)) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL  WHERE 1=1 AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) C ON A.NO_KEL = B.NO_KEL AND B.NO_KEL =C.NO_KEL
						    WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::select($sql);
               return $r;
		}

		
		public function mst_get_cakupan_perceraian_pelayanan($no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS R1
					, CASE WHEN C.JUMLAH IS NULL THEN 0 ELSE C.JUMLAH END AS R2
          			, B.JUMLAH - C.JUMLAH AS R3
          			, CONCAT(TO_CHAR(ROUND(C.JUMLAH/B.JUMLAH, 4)*100),'%') AS PRS 
					FROM SETUP_KEC A LEFT JOIN
					(SELECT 
						B.NO_KEC 
						,B.NAMA_KEC 
						, COUNT(1) JUMLAH
						FROM SETUP_KEC B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM BIODATA_WNI A WHERE A.STAT_KWN IN (3,4) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC  
			            WHERE 1=1 AND A.NO_PROP = 32 AND A.NO_KAB =73 GROUP BY B.NO_KEC,B.NAMA_KEC ORDER BY B.NO_KEC) B ON A.NO_KEC = B.NO_KEC
			            LEFT JOIN
					(SELECT 
						B.NO_KEC 
						,B.NAMA_KEC 
						, COUNT(1) JUMLAH
						FROM SETUP_KEC B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM BIODATA_WNI A WHERE A.STAT_KWN IN (3,4) AND  (A.AKTA_CRAI = 2)) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC  
            		WHERE 1=1 AND A.NO_PROP = 32 AND A.NO_KAB =73 GROUP BY B.NO_KEC,B.NAMA_KEC ORDER BY B.NO_KEC) C ON A.NO_KEC = B.NO_KEC AND B.NO_KEC =C.NO_KEC
            		WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS R1
					, CASE WHEN C.JUMLAH IS NULL THEN 0 ELSE C.JUMLAH END AS R2
          			, B.JUMLAH - C.JUMLAH AS R3
          			, CONCAT(TO_CHAR(ROUND(C.JUMLAH/B.JUMLAH, 4)*100),'%') AS PRS 
					FROM SETUP_KEL A LEFT JOIN
					 ";
						if($no_kel == 0){    
					$sql .=" (SELECT 
						B.NO_KEL
						, B.NAMA_KEL 
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM BIODATA_WNI A WHERE A.STAT_KWN IN (3,4) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL  WHERE 1=1  AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL
						    LEFT JOIN
						    (SELECT 
						B.NO_KEL
						, B.NAMA_KEL 
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM BIODATA_WNI A WHERE A.STAT_KWN IN (3,4) AND  (A.AKTA_CRAI = 2)) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL  WHERE 1=1 AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) C ON A.NO_KEL = B.NO_KEL AND B.NO_KEL =C.NO_KEL
						    WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" (SELECT 
						B.NO_KEL
						, B.NAMA_KEL 
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM BIODATA_WNI A WHERE A.STAT_KWN IN (3,4) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL  WHERE 1=1  AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL = ".$no_kel."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL
						    LEFT JOIN
						    (SELECT 
						B.NO_KEL
						, B.NAMA_KEL 
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM BIODATA_WNI A WHERE A.STAT_KWN IN (3,4) AND  (A.AKTA_CRAI = 2)) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL  WHERE 1=1 AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) C ON A.NO_KEL = B.NO_KEL AND B.NO_KEL =C.NO_KEL
						    WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::connection('db222')->select($sql);
               return $r;
		}

		
		public function mst_get_cakupan_perceraian_non_muslim($no_kec,$no_kel){
			$dkb_bio = Shr::do_get_dkb_bio();
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS R1
					, CASE WHEN C.JUMLAH IS NULL THEN 0 ELSE C.JUMLAH END AS R2
          			, B.JUMLAH - C.JUMLAH AS R3
          			, CONCAT(TO_CHAR(ROUND(C.JUMLAH/B.JUMLAH, 4)*100),'%') AS PRS 
					FROM SETUP_KEC A LEFT JOIN
					(SELECT 
						B.NO_KEC 
						,B.NAMA_KEC 
						, COUNT(1) JUMLAH
						FROM SETUP_KEC B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM ".$dkb_bio." A WHERE A.AGAMA <> 1 AND A.STAT_KWN IN (3,4) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC  
			            WHERE 1=1 AND A.NO_PROP = 32 AND A.NO_KAB =73 GROUP BY B.NO_KEC,B.NAMA_KEC ORDER BY B.NO_KEC) B ON A.NO_KEC = B.NO_KEC
			            LEFT JOIN
					(SELECT 
						B.NO_KEC 
						,B.NAMA_KEC 
						, COUNT(1) JUMLAH
						FROM SETUP_KEC B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM ".$dkb_bio." A WHERE A.AGAMA <> 1 AND A.STAT_KWN IN (3,4) AND  (A.AKTA_CRAI = 2)) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC  
            		WHERE 1=1 AND A.NO_PROP = 32 AND A.NO_KAB =73 GROUP BY B.NO_KEC,B.NAMA_KEC ORDER BY B.NO_KEC) C ON A.NO_KEC = B.NO_KEC AND B.NO_KEC =C.NO_KEC
            		WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS R1
					, CASE WHEN C.JUMLAH IS NULL THEN 0 ELSE C.JUMLAH END AS R2
          			, B.JUMLAH - C.JUMLAH AS R3
          			, CONCAT(TO_CHAR(ROUND(C.JUMLAH/B.JUMLAH, 4)*100),'%') AS PRS 
					FROM SETUP_KEL A LEFT JOIN
					 ";
						if($no_kel == 0){    
					$sql .=" (SELECT 
						B.NO_KEL
						, B.NAMA_KEL 
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM ".$dkb_bio." A WHERE A.AGAMA <> 1 AND A.STAT_KWN IN (3,4) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL  WHERE 1=1  AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL
						    LEFT JOIN
						    (SELECT 
						B.NO_KEL
						, B.NAMA_KEL 
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM ".$dkb_bio." A WHERE A.AGAMA <> 1 AND A.STAT_KWN IN (3,4) AND  (A.AKTA_CRAI = 2)) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL  WHERE 1=1 AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) C ON A.NO_KEL = B.NO_KEL AND B.NO_KEL =C.NO_KEL
						    WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" (SELECT 
						B.NO_KEL
						, B.NAMA_KEL 
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM ".$dkb_bio." A WHERE A.AGAMA <> 1 AND A.STAT_KWN IN (3,4) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL  WHERE 1=1  AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL = ".$no_kel."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL
						    LEFT JOIN
						    (SELECT 
						B.NO_KEL
						, B.NAMA_KEL 
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM ".$dkb_bio." A WHERE A.AGAMA <> 1 AND A.STAT_KWN IN (3,4) AND  (A.AKTA_CRAI = 2)) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL  WHERE 1=1 AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) C ON A.NO_KEL = B.NO_KEL AND B.NO_KEL =C.NO_KEL
						    WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::select($sql);
               return $r;
		}

		
		public function mst_get_cakupan_perceraian_muslim($no_kec,$no_kel){
			$dkb_bio = Shr::do_get_dkb_bio();
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS R1
					, CASE WHEN C.JUMLAH IS NULL THEN 0 ELSE C.JUMLAH END AS R2
          			, B.JUMLAH - C.JUMLAH AS R3
          			, CONCAT(TO_CHAR(ROUND(C.JUMLAH/B.JUMLAH, 4)*100),'%') AS PRS 
					FROM SETUP_KEC A LEFT JOIN
					(SELECT 
						B.NO_KEC 
						,B.NAMA_KEC 
						, COUNT(1) JUMLAH
						FROM SETUP_KEC B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM ".$dkb_bio." A WHERE A.AGAMA = 1 AND A.STAT_KWN IN (2) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC  
			            WHERE 1=1 AND A.NO_PROP = 32 AND A.NO_KAB =73 GROUP BY B.NO_KEC,B.NAMA_KEC ORDER BY B.NO_KEC) B ON A.NO_KEC = B.NO_KEC
			            LEFT JOIN
					(SELECT 
						B.NO_KEC 
						,B.NAMA_KEC 
						, COUNT(1) JUMLAH
						FROM SETUP_KEC B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM ".$dkb_bio." A WHERE A.AGAMA = 1 AND A.STAT_KWN IN (2) AND  (A.AKTA_CRAI = 2)) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC  
            		WHERE 1=1 AND A.NO_PROP = 32 AND A.NO_KAB =73 GROUP BY B.NO_KEC,B.NAMA_KEC ORDER BY B.NO_KEC) C ON A.NO_KEC = B.NO_KEC AND B.NO_KEC =C.NO_KEC
            		WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS R1
					, CASE WHEN C.JUMLAH IS NULL THEN 0 ELSE C.JUMLAH END AS R2
          			, B.JUMLAH - C.JUMLAH AS R3
          			, CONCAT(TO_CHAR(ROUND(C.JUMLAH/B.JUMLAH, 4)*100),'%') AS PRS 
					FROM SETUP_KEL A LEFT JOIN
					 ";
						if($no_kel == 0){    
					$sql .=" (SELECT 
						B.NO_KEL
						, B.NAMA_KEL 
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM ".$dkb_bio." A WHERE A.AGAMA = 1 AND A.STAT_KWN IN (2) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL  WHERE 1=1  AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL
						    LEFT JOIN
						    (SELECT 
						B.NO_KEL
						, B.NAMA_KEL 
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM ".$dkb_bio." A WHERE A.AGAMA = 1 AND A.STAT_KWN IN (2) AND  (A.AKTA_CRAI = 2)) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL  WHERE 1=1 AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) C ON A.NO_KEL = B.NO_KEL AND B.NO_KEL =C.NO_KEL
						    WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" (SELECT 
						B.NO_KEL
						, B.NAMA_KEL 
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM ".$dkb_bio." A WHERE A.AGAMA = 1 AND A.STAT_KWN IN (2) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL  WHERE 1=1  AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL = ".$no_kel."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL
						    LEFT JOIN
						    (SELECT 
						B.NO_KEL
						, B.NAMA_KEL 
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NIK, A.JENIS_KLMIN  FROM ".$dkb_bio." A WHERE A.AGAMA = 1 AND A.STAT_KWN IN (2) AND  (A.AKTA_CRAI = 2)) A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC AND A.NO_KEL = B.NO_KEL  WHERE 1=1 AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) C ON A.NO_KEL = B.NO_KEL AND B.NO_KEL =C.NO_KEL
						    WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::select($sql);
               return $r;
		}

		
		function mst_get_bln_cetak_ktp($bln){
			$sql = "SELECT 
						  TO_CHAR(A.DAY,'DD-MM-YYYY') AS DAY
						  , CASE WHEN B.R1 IS NULL THEN 0 ELSE B.R1 END AS R1
						  , CASE WHEN B.R2 IS NULL THEN 0 ELSE B.R2 END AS R2
						  , CASE WHEN B.R3 IS NULL THEN 0 ELSE B.R3 END AS R3
						  , CASE WHEN B.R4 IS NULL THEN 0 ELSE B.R4 END AS R4
						  , CASE WHEN B.R5 IS NULL THEN 0 ELSE B.R5 END AS R5
						  , CASE WHEN B.R6 IS NULL THEN 0 ELSE B.R6 END AS R6
						  , CASE WHEN B.R7 IS NULL THEN 0 ELSE B.R7 END AS R7
						  , CASE WHEN B.R8 IS NULL THEN 0 ELSE B.R8 END AS R8
						  , CASE WHEN B.R9 IS NULL THEN 0 ELSE B.R9 END AS R9
						  , CASE WHEN B.R10 IS NULL THEN 0 ELSE B.R10 END AS R10
						  , CASE WHEN B.R11 IS NULL THEN 0 ELSE B.R11 END AS R11
						  , CASE WHEN B.R12 IS NULL THEN 0 ELSE B.R12 END AS R12
						  , CASE WHEN B.R13 IS NULL THEN 0 ELSE B.R13 END AS R13
						  , CASE WHEN B.R14 IS NULL THEN 0 ELSE B.R14 END AS R14
						  , CASE WHEN B.R15 IS NULL THEN 0 ELSE B.R15 END AS R15
						  , CASE WHEN B.R16 IS NULL THEN 0 ELSE B.R16 END AS R16
						  , CASE WHEN B.R17 IS NULL THEN 0 ELSE B.R17 END AS R17
						  , CASE WHEN B.R18 IS NULL THEN 0 ELSE B.R18 END AS R18
						  , CASE WHEN B.R19 IS NULL THEN 0 ELSE B.R19 END AS R19
						  , CASE WHEN B.R20 IS NULL THEN 0 ELSE B.R20 END AS R20
						  , CASE WHEN B.R21 IS NULL THEN 0 ELSE B.R21 END AS R21
						  , CASE WHEN B.R22 IS NULL THEN 0 ELSE B.R22 END AS R22
						  , CASE WHEN B.R23 IS NULL THEN 0 ELSE B.R23 END AS R23
						  , CASE WHEN B.R24 IS NULL THEN 0 ELSE B.R24 END AS R24
						  , CASE WHEN B.R25 IS NULL THEN 0 ELSE B.R25 END AS R25
						  , CASE WHEN B.R26 IS NULL THEN 0 ELSE B.R26 END AS R26
						  , CASE WHEN B.R27 IS NULL THEN 0 ELSE B.R27 END AS R27
						  , CASE WHEN B.R28 IS NULL THEN 0 ELSE B.R28 END AS R28
						  , CASE WHEN B.R29 IS NULL THEN 0 ELSE B.R29 END AS R29
						  , CASE WHEN B.R30 IS NULL THEN 0 ELSE B.R30 END AS R30
						FROM 
						  (SELECT 
						        TRUNC(TO_DATE('".$bln."','MM/YYYY'), 'MM') + LEVEL - 1 AS DAY 
						    FROM DUAL 
						CONNECT BY 
						        TRUNC(TRUNC(TO_DATE('".$bln."','MM/YYYY'), 'MM') + LEVEL - 1, 'MM') = TRUNC(TO_DATE('".$bln."','MM/YYYY'), 'MM')) A 
						    LEFT JOIN
						  (SELECT 
					       CASE WHEN B.LAST_UPDATE IS NULL THEN TRUNC(B.PERSONALIZED_DATE) ELSE TRUNC(B.LAST_UPDATE) END AS DAY
						  , SUM(CASE WHEN A.NO_KEC =1 THEN 1 ELSE 0 END) AS R1
						  , SUM(CASE WHEN A.NO_KEC =2 THEN 1 ELSE 0 END) AS R2
						  , SUM(CASE WHEN A.NO_KEC =3 THEN 1 ELSE 0 END) AS R3
						  , SUM(CASE WHEN A.NO_KEC =4 THEN 1 ELSE 0 END) AS R4
						  , SUM(CASE WHEN A.NO_KEC =5 THEN 1 ELSE 0 END) AS R5
						  , SUM(CASE WHEN A.NO_KEC =6 THEN 1 ELSE 0 END) AS R6
						  , SUM(CASE WHEN A.NO_KEC =7 THEN 1 ELSE 0 END) AS R7
						  , SUM(CASE WHEN A.NO_KEC =8 THEN 1 ELSE 0 END) AS R8
						  , SUM(CASE WHEN A.NO_KEC =9 THEN 1 ELSE 0 END) AS R9
						  , SUM(CASE WHEN A.NO_KEC =10 THEN 1 ELSE 0 END) AS R10
						  , SUM(CASE WHEN A.NO_KEC =11 THEN 1 ELSE 0 END) AS R11
						  , SUM(CASE WHEN A.NO_KEC =12 THEN 1 ELSE 0 END) AS R12
						  , SUM(CASE WHEN A.NO_KEC =13 THEN 1 ELSE 0 END) AS R13
						  , SUM(CASE WHEN A.NO_KEC =14 THEN 1 ELSE 0 END) AS R14
						  , SUM(CASE WHEN A.NO_KEC =15 THEN 1 ELSE 0 END) AS R15
						  , SUM(CASE WHEN A.NO_KEC =16 THEN 1 ELSE 0 END) AS R16
						  , SUM(CASE WHEN A.NO_KEC =17 THEN 1 ELSE 0 END) AS R17
						  , SUM(CASE WHEN A.NO_KEC =18 THEN 1 ELSE 0 END) AS R18
						  , SUM(CASE WHEN A.NO_KEC =19 THEN 1 ELSE 0 END) AS R19
						  , SUM(CASE WHEN A.NO_KEC =20 THEN 1 ELSE 0 END) AS R20
						  , SUM(CASE WHEN A.NO_KEC =21 THEN 1 ELSE 0 END) AS R21
						  , SUM(CASE WHEN A.NO_KEC =22 THEN 1 ELSE 0 END) AS R22
						  , SUM(CASE WHEN A.NO_KEC =23 THEN 1 ELSE 0 END) AS R23
						  , SUM(CASE WHEN A.NO_KEC =24 THEN 1 ELSE 0 END) AS R24
						  , SUM(CASE WHEN A.NO_KEC =25 THEN 1 ELSE 0 END) AS R25
						  , SUM(CASE WHEN A.NO_KEC =26 THEN 1 ELSE 0 END) AS R26
						  , SUM(CASE WHEN A.NO_KEC =27 THEN 1 ELSE 0 END) AS R27
						  , SUM(CASE WHEN A.NO_KEC =28 THEN 1 ELSE 0 END) AS R28
						  , SUM(CASE WHEN A.NO_KEC =29 THEN 1 ELSE 0 END) AS R29
						  , SUM(CASE WHEN A.NO_KEC =30 THEN 1 ELSE 0 END) AS R30
						  FROM CARD_MANAGEMENT@DB5CETAK B INNER JOIN BIODATA_WNI A ON A.NIK = B.NIK
						 WHERE 
						  CASE WHEN B.LAST_UPDATE IS NULL THEN TO_CHAR(B.PERSONALIZED_DATE,'MM/YYYY') ELSE TO_CHAR(B.LAST_UPDATE,'MM/YYYY') END = '".$bln."'
					    AND CASE WHEN UPPER(B.LAST_UPDATED_USERNAME) IS NULL THEN UPPER(B.CREATED_USERNAME) ELSE UPPER(B.LAST_UPDATED_USERNAME) END  NOT IN (SELECT UPPER(USER_BCARD) FROM SIAK_CETAK_GOIB@YZDB WHERE IS_ACTIVE = 1)
					    AND A.NO_PROP = 32 AND A.NO_KAB = 73
						 GROUP BY CASE WHEN B.LAST_UPDATE IS NULL THEN TRUNC(B.PERSONALIZED_DATE) ELSE TRUNC(B.LAST_UPDATE) END
						 ORDER BY CASE WHEN B.LAST_UPDATE IS NULL THEN TRUNC(B.PERSONALIZED_DATE) ELSE TRUNC(B.LAST_UPDATE) END) B 
						 ON A.DAY = B.DAY ORDER BY A.DAY";
				$r = DB::select($sql);
               return $r;
		}
		
		function mst_get_bln_rekam_ktp($bln){
			$sql = "SELECT 
						  TO_CHAR(A.DAY,'DD-MM-YYYY') AS DAY
						  , CASE WHEN B.R1 IS NULL THEN 0 ELSE B.R1 END AS R1
						  , CASE WHEN B.R2 IS NULL THEN 0 ELSE B.R2 END AS R2
						  , CASE WHEN B.R3 IS NULL THEN 0 ELSE B.R3 END AS R3
						  , CASE WHEN B.R4 IS NULL THEN 0 ELSE B.R4 END AS R4
						  , CASE WHEN B.R5 IS NULL THEN 0 ELSE B.R5 END AS R5
						  , CASE WHEN B.R6 IS NULL THEN 0 ELSE B.R6 END AS R6
						  , CASE WHEN B.R7 IS NULL THEN 0 ELSE B.R7 END AS R7
						  , CASE WHEN B.R8 IS NULL THEN 0 ELSE B.R8 END AS R8
						  , CASE WHEN B.R9 IS NULL THEN 0 ELSE B.R9 END AS R9
						  , CASE WHEN B.R10 IS NULL THEN 0 ELSE B.R10 END AS R10
						  , CASE WHEN B.R11 IS NULL THEN 0 ELSE B.R11 END AS R11
						  , CASE WHEN B.R12 IS NULL THEN 0 ELSE B.R12 END AS R12
						  , CASE WHEN B.R13 IS NULL THEN 0 ELSE B.R13 END AS R13
						  , CASE WHEN B.R14 IS NULL THEN 0 ELSE B.R14 END AS R14
						  , CASE WHEN B.R15 IS NULL THEN 0 ELSE B.R15 END AS R15
						  , CASE WHEN B.R16 IS NULL THEN 0 ELSE B.R16 END AS R16
						  , CASE WHEN B.R17 IS NULL THEN 0 ELSE B.R17 END AS R17
						  , CASE WHEN B.R18 IS NULL THEN 0 ELSE B.R18 END AS R18
						  , CASE WHEN B.R19 IS NULL THEN 0 ELSE B.R19 END AS R19
						  , CASE WHEN B.R20 IS NULL THEN 0 ELSE B.R20 END AS R20
						  , CASE WHEN B.R21 IS NULL THEN 0 ELSE B.R21 END AS R21
						  , CASE WHEN B.R22 IS NULL THEN 0 ELSE B.R22 END AS R22
						  , CASE WHEN B.R23 IS NULL THEN 0 ELSE B.R23 END AS R23
						  , CASE WHEN B.R24 IS NULL THEN 0 ELSE B.R24 END AS R24
						  , CASE WHEN B.R25 IS NULL THEN 0 ELSE B.R25 END AS R25
						  , CASE WHEN B.R26 IS NULL THEN 0 ELSE B.R26 END AS R26
						  , CASE WHEN B.R27 IS NULL THEN 0 ELSE B.R27 END AS R27
						  , CASE WHEN B.R28 IS NULL THEN 0 ELSE B.R28 END AS R28
						  , CASE WHEN B.R29 IS NULL THEN 0 ELSE B.R29 END AS R29
						  , CASE WHEN B.R30 IS NULL THEN 0 ELSE B.R30 END AS R30
						FROM 
						  (SELECT 
						        TRUNC(TO_DATE('".$bln."','MM/YYYY'), 'MM') + LEVEL - 1 AS DAY 
						    FROM DUAL 
						CONNECT BY 
						        TRUNC(TRUNC(TO_DATE('".$bln."','MM/YYYY'), 'MM') + LEVEL - 1, 'MM') = TRUNC(TO_DATE('".$bln."','MM/YYYY'), 'MM')) A 
						    LEFT JOIN
						  (SELECT 
					        TRUNC(CREATED) AS DAY
						  , SUM(CASE WHEN A.NO_KEC =1 THEN 1 ELSE 0 END) AS R1
						  , SUM(CASE WHEN A.NO_KEC =2 THEN 1 ELSE 0 END) AS R2
						  , SUM(CASE WHEN A.NO_KEC =3 THEN 1 ELSE 0 END) AS R3
						  , SUM(CASE WHEN A.NO_KEC =4 THEN 1 ELSE 0 END) AS R4
						  , SUM(CASE WHEN A.NO_KEC =5 THEN 1 ELSE 0 END) AS R5
						  , SUM(CASE WHEN A.NO_KEC =6 THEN 1 ELSE 0 END) AS R6
						  , SUM(CASE WHEN A.NO_KEC =7 THEN 1 ELSE 0 END) AS R7
						  , SUM(CASE WHEN A.NO_KEC =8 THEN 1 ELSE 0 END) AS R8
						  , SUM(CASE WHEN A.NO_KEC =9 THEN 1 ELSE 0 END) AS R9
						  , SUM(CASE WHEN A.NO_KEC =10 THEN 1 ELSE 0 END) AS R10
						  , SUM(CASE WHEN A.NO_KEC =11 THEN 1 ELSE 0 END) AS R11
						  , SUM(CASE WHEN A.NO_KEC =12 THEN 1 ELSE 0 END) AS R12
						  , SUM(CASE WHEN A.NO_KEC =13 THEN 1 ELSE 0 END) AS R13
						  , SUM(CASE WHEN A.NO_KEC =14 THEN 1 ELSE 0 END) AS R14
						  , SUM(CASE WHEN A.NO_KEC =15 THEN 1 ELSE 0 END) AS R15
						  , SUM(CASE WHEN A.NO_KEC =16 THEN 1 ELSE 0 END) AS R16
						  , SUM(CASE WHEN A.NO_KEC =17 THEN 1 ELSE 0 END) AS R17
						  , SUM(CASE WHEN A.NO_KEC =18 THEN 1 ELSE 0 END) AS R18
						  , SUM(CASE WHEN A.NO_KEC =19 THEN 1 ELSE 0 END) AS R19
						  , SUM(CASE WHEN A.NO_KEC =20 THEN 1 ELSE 0 END) AS R20
						  , SUM(CASE WHEN A.NO_KEC =21 THEN 1 ELSE 0 END) AS R21
						  , SUM(CASE WHEN A.NO_KEC =22 THEN 1 ELSE 0 END) AS R22
						  , SUM(CASE WHEN A.NO_KEC =23 THEN 1 ELSE 0 END) AS R23
						  , SUM(CASE WHEN A.NO_KEC =24 THEN 1 ELSE 0 END) AS R24
						  , SUM(CASE WHEN A.NO_KEC =25 THEN 1 ELSE 0 END) AS R25
						  , SUM(CASE WHEN A.NO_KEC =26 THEN 1 ELSE 0 END) AS R26
						  , SUM(CASE WHEN A.NO_KEC =27 THEN 1 ELSE 0 END) AS R27
						  , SUM(CASE WHEN A.NO_KEC =28 THEN 1 ELSE 0 END) AS R28
						  , SUM(CASE WHEN A.NO_KEC =29 THEN 1 ELSE 0 END) AS R29
						  , SUM(CASE WHEN A.NO_KEC =30 THEN 1 ELSE 0 END) AS R30
						  FROM DEMOGRAPHICS_ALL B INNER JOIN BIODATA_WNI A ON A.NIK = B.NIK
						 WHERE TO_CHAR(CREATED,'MM/YYYY') = '".$bln."' AND A.NO_PROP = 32 AND A.NO_KAB =73
						 GROUP BY TRUNC(CREATED)
						 ORDER BY TRUNC(CREATED)) B 
						 ON A.DAY = B.DAY ORDER BY A.DAY";
				$r = DB::select($sql);
               return $r;
		}
		
		function mst_get_bln_suket($bln){
			$sql = "SELECT 
						  TO_CHAR(A.DAY,'DD-MM-YYYY') AS DAY
						  , CASE WHEN B.R1 IS NULL THEN 0 ELSE B.R1 END AS R1
						  , CASE WHEN B.R2 IS NULL THEN 0 ELSE B.R2 END AS R2
						  , CASE WHEN B.R3 IS NULL THEN 0 ELSE B.R3 END AS R3
						  , CASE WHEN B.R4 IS NULL THEN 0 ELSE B.R4 END AS R4
						  , CASE WHEN B.R5 IS NULL THEN 0 ELSE B.R5 END AS R5
						  , CASE WHEN B.R6 IS NULL THEN 0 ELSE B.R6 END AS R6
						  , CASE WHEN B.R7 IS NULL THEN 0 ELSE B.R7 END AS R7
						  , CASE WHEN B.R8 IS NULL THEN 0 ELSE B.R8 END AS R8
						  , CASE WHEN B.R9 IS NULL THEN 0 ELSE B.R9 END AS R9
						  , CASE WHEN B.R10 IS NULL THEN 0 ELSE B.R10 END AS R10
						  , CASE WHEN B.R11 IS NULL THEN 0 ELSE B.R11 END AS R11
						  , CASE WHEN B.R12 IS NULL THEN 0 ELSE B.R12 END AS R12
						  , CASE WHEN B.R13 IS NULL THEN 0 ELSE B.R13 END AS R13
						  , CASE WHEN B.R14 IS NULL THEN 0 ELSE B.R14 END AS R14
						  , CASE WHEN B.R15 IS NULL THEN 0 ELSE B.R15 END AS R15
						  , CASE WHEN B.R16 IS NULL THEN 0 ELSE B.R16 END AS R16
						  , CASE WHEN B.R17 IS NULL THEN 0 ELSE B.R17 END AS R17
						  , CASE WHEN B.R18 IS NULL THEN 0 ELSE B.R18 END AS R18
						  , CASE WHEN B.R19 IS NULL THEN 0 ELSE B.R19 END AS R19
						  , CASE WHEN B.R20 IS NULL THEN 0 ELSE B.R20 END AS R20
						  , CASE WHEN B.R21 IS NULL THEN 0 ELSE B.R21 END AS R21
						  , CASE WHEN B.R22 IS NULL THEN 0 ELSE B.R22 END AS R22
						  , CASE WHEN B.R23 IS NULL THEN 0 ELSE B.R23 END AS R23
						  , CASE WHEN B.R24 IS NULL THEN 0 ELSE B.R24 END AS R24
						  , CASE WHEN B.R25 IS NULL THEN 0 ELSE B.R25 END AS R25
						  , CASE WHEN B.R26 IS NULL THEN 0 ELSE B.R26 END AS R26
						  , CASE WHEN B.R27 IS NULL THEN 0 ELSE B.R27 END AS R27
						  , CASE WHEN B.R28 IS NULL THEN 0 ELSE B.R28 END AS R28
						  , CASE WHEN B.R29 IS NULL THEN 0 ELSE B.R29 END AS R29
						  , CASE WHEN B.R30 IS NULL THEN 0 ELSE B.R30 END AS R30
						FROM 
						  (SELECT 
						        TRUNC(TO_DATE('".$bln."','MM/YYYY'), 'MM') + LEVEL - 1 AS DAY 
						    FROM DUAL 
						CONNECT BY 
						        TRUNC(TRUNC(TO_DATE('".$bln."','MM/YYYY'), 'MM') + LEVEL - 1, 'MM') = TRUNC(TO_DATE('".$bln."','MM/YYYY'), 'MM')) A 
						    LEFT JOIN
						  (SELECT 
					        TRUNC(PRINTED_DATE) AS DAY
						  , SUM(CASE WHEN A.NO_KEC =1 THEN 1 ELSE 0 END) AS R1
						  , SUM(CASE WHEN A.NO_KEC =2 THEN 1 ELSE 0 END) AS R2
						  , SUM(CASE WHEN A.NO_KEC =3 THEN 1 ELSE 0 END) AS R3
						  , SUM(CASE WHEN A.NO_KEC =4 THEN 1 ELSE 0 END) AS R4
						  , SUM(CASE WHEN A.NO_KEC =5 THEN 1 ELSE 0 END) AS R5
						  , SUM(CASE WHEN A.NO_KEC =6 THEN 1 ELSE 0 END) AS R6
						  , SUM(CASE WHEN A.NO_KEC =7 THEN 1 ELSE 0 END) AS R7
						  , SUM(CASE WHEN A.NO_KEC =8 THEN 1 ELSE 0 END) AS R8
						  , SUM(CASE WHEN A.NO_KEC =9 THEN 1 ELSE 0 END) AS R9
						  , SUM(CASE WHEN A.NO_KEC =10 THEN 1 ELSE 0 END) AS R10
						  , SUM(CASE WHEN A.NO_KEC =11 THEN 1 ELSE 0 END) AS R11
						  , SUM(CASE WHEN A.NO_KEC =12 THEN 1 ELSE 0 END) AS R12
						  , SUM(CASE WHEN A.NO_KEC =13 THEN 1 ELSE 0 END) AS R13
						  , SUM(CASE WHEN A.NO_KEC =14 THEN 1 ELSE 0 END) AS R14
						  , SUM(CASE WHEN A.NO_KEC =15 THEN 1 ELSE 0 END) AS R15
						  , SUM(CASE WHEN A.NO_KEC =16 THEN 1 ELSE 0 END) AS R16
						  , SUM(CASE WHEN A.NO_KEC =17 THEN 1 ELSE 0 END) AS R17
						  , SUM(CASE WHEN A.NO_KEC =18 THEN 1 ELSE 0 END) AS R18
						  , SUM(CASE WHEN A.NO_KEC =19 THEN 1 ELSE 0 END) AS R19
						  , SUM(CASE WHEN A.NO_KEC =20 THEN 1 ELSE 0 END) AS R20
						  , SUM(CASE WHEN A.NO_KEC =21 THEN 1 ELSE 0 END) AS R21
						  , SUM(CASE WHEN A.NO_KEC =22 THEN 1 ELSE 0 END) AS R22
						  , SUM(CASE WHEN A.NO_KEC =23 THEN 1 ELSE 0 END) AS R23
						  , SUM(CASE WHEN A.NO_KEC =24 THEN 1 ELSE 0 END) AS R24
						  , SUM(CASE WHEN A.NO_KEC =25 THEN 1 ELSE 0 END) AS R25
						  , SUM(CASE WHEN A.NO_KEC =26 THEN 1 ELSE 0 END) AS R26
						  , SUM(CASE WHEN A.NO_KEC =27 THEN 1 ELSE 0 END) AS R27
						  , SUM(CASE WHEN A.NO_KEC =28 THEN 1 ELSE 0 END) AS R28
						  , SUM(CASE WHEN A.NO_KEC =29 THEN 1 ELSE 0 END) AS R29
						  , SUM(CASE WHEN A.NO_KEC =30 THEN 1 ELSE 0 END) AS R30
						  FROM T7_HIST_SUKET B INNER JOIN BIODATA_WNI A ON A.NIK = B.NIK
						 WHERE TO_CHAR(PRINTED_DATE,'MM/YYYY') = '".$bln."' AND A.NO_PROP = 32 AND A.NO_KAB =73
						 GROUP BY TRUNC(PRINTED_DATE)
						 ORDER BY TRUNC(PRINTED_DATE)) B 
						 ON A.DAY = B.DAY ORDER BY A.DAY";
				$r = DB::select($sql);
               return $r;
		}
		
		function mst_get_bln_kk($bln){
			$sql = "SELECT 
						  TO_CHAR(A.DAY,'DD-MM-YYYY') AS DAY
						  , CASE WHEN B.R1 IS NULL THEN 0 ELSE B.R1 END AS R1
						  , CASE WHEN B.R2 IS NULL THEN 0 ELSE B.R2 END AS R2
						  , CASE WHEN B.R3 IS NULL THEN 0 ELSE B.R3 END AS R3
						  , CASE WHEN B.R4 IS NULL THEN 0 ELSE B.R4 END AS R4
						  , CASE WHEN B.R5 IS NULL THEN 0 ELSE B.R5 END AS R5
						  , CASE WHEN B.R6 IS NULL THEN 0 ELSE B.R6 END AS R6
						  , CASE WHEN B.R7 IS NULL THEN 0 ELSE B.R7 END AS R7
						  , CASE WHEN B.R8 IS NULL THEN 0 ELSE B.R8 END AS R8
						  , CASE WHEN B.R9 IS NULL THEN 0 ELSE B.R9 END AS R9
						  , CASE WHEN B.R10 IS NULL THEN 0 ELSE B.R10 END AS R10
						  , CASE WHEN B.R11 IS NULL THEN 0 ELSE B.R11 END AS R11
						  , CASE WHEN B.R12 IS NULL THEN 0 ELSE B.R12 END AS R12
						  , CASE WHEN B.R13 IS NULL THEN 0 ELSE B.R13 END AS R13
						  , CASE WHEN B.R14 IS NULL THEN 0 ELSE B.R14 END AS R14
						  , CASE WHEN B.R15 IS NULL THEN 0 ELSE B.R15 END AS R15
						  , CASE WHEN B.R16 IS NULL THEN 0 ELSE B.R16 END AS R16
						  , CASE WHEN B.R17 IS NULL THEN 0 ELSE B.R17 END AS R17
						  , CASE WHEN B.R18 IS NULL THEN 0 ELSE B.R18 END AS R18
						  , CASE WHEN B.R19 IS NULL THEN 0 ELSE B.R19 END AS R19
						  , CASE WHEN B.R20 IS NULL THEN 0 ELSE B.R20 END AS R20
						  , CASE WHEN B.R21 IS NULL THEN 0 ELSE B.R21 END AS R21
						  , CASE WHEN B.R22 IS NULL THEN 0 ELSE B.R22 END AS R22
						  , CASE WHEN B.R23 IS NULL THEN 0 ELSE B.R23 END AS R23
						  , CASE WHEN B.R24 IS NULL THEN 0 ELSE B.R24 END AS R24
						  , CASE WHEN B.R25 IS NULL THEN 0 ELSE B.R25 END AS R25
						  , CASE WHEN B.R26 IS NULL THEN 0 ELSE B.R26 END AS R26
						  , CASE WHEN B.R27 IS NULL THEN 0 ELSE B.R27 END AS R27
						  , CASE WHEN B.R28 IS NULL THEN 0 ELSE B.R28 END AS R28
						  , CASE WHEN B.R29 IS NULL THEN 0 ELSE B.R29 END AS R29
						  , CASE WHEN B.R30 IS NULL THEN 0 ELSE B.R30 END AS R30
						FROM 
						  (SELECT 
						        TRUNC(TO_DATE('".$bln."','MM/YYYY'), 'MM') + LEVEL - 1 AS DAY 
						    FROM DUAL 
						CONNECT BY 
						        TRUNC(TRUNC(TO_DATE('".$bln."','MM/YYYY'), 'MM') + LEVEL - 1, 'MM') = TRUNC(TO_DATE('".$bln."','MM/YYYY'), 'MM')) A 
						    LEFT JOIN
						  (SELECT 
					        TRUNC(PRINTED_DATE) AS DAY
						  , SUM(CASE WHEN A.NO_KEC =1 THEN 1 ELSE 0 END) AS R1
						  , SUM(CASE WHEN A.NO_KEC =2 THEN 1 ELSE 0 END) AS R2
						  , SUM(CASE WHEN A.NO_KEC =3 THEN 1 ELSE 0 END) AS R3
						  , SUM(CASE WHEN A.NO_KEC =4 THEN 1 ELSE 0 END) AS R4
						  , SUM(CASE WHEN A.NO_KEC =5 THEN 1 ELSE 0 END) AS R5
						  , SUM(CASE WHEN A.NO_KEC =6 THEN 1 ELSE 0 END) AS R6
						  , SUM(CASE WHEN A.NO_KEC =7 THEN 1 ELSE 0 END) AS R7
						  , SUM(CASE WHEN A.NO_KEC =8 THEN 1 ELSE 0 END) AS R8
						  , SUM(CASE WHEN A.NO_KEC =9 THEN 1 ELSE 0 END) AS R9
						  , SUM(CASE WHEN A.NO_KEC =10 THEN 1 ELSE 0 END) AS R10
						  , SUM(CASE WHEN A.NO_KEC =11 THEN 1 ELSE 0 END) AS R11
						  , SUM(CASE WHEN A.NO_KEC =12 THEN 1 ELSE 0 END) AS R12
						  , SUM(CASE WHEN A.NO_KEC =13 THEN 1 ELSE 0 END) AS R13
						  , SUM(CASE WHEN A.NO_KEC =14 THEN 1 ELSE 0 END) AS R14
						  , SUM(CASE WHEN A.NO_KEC =15 THEN 1 ELSE 0 END) AS R15
						  , SUM(CASE WHEN A.NO_KEC =16 THEN 1 ELSE 0 END) AS R16
						  , SUM(CASE WHEN A.NO_KEC =17 THEN 1 ELSE 0 END) AS R17
						  , SUM(CASE WHEN A.NO_KEC =18 THEN 1 ELSE 0 END) AS R18
						  , SUM(CASE WHEN A.NO_KEC =19 THEN 1 ELSE 0 END) AS R19
						  , SUM(CASE WHEN A.NO_KEC =20 THEN 1 ELSE 0 END) AS R20
						  , SUM(CASE WHEN A.NO_KEC =21 THEN 1 ELSE 0 END) AS R21
						  , SUM(CASE WHEN A.NO_KEC =22 THEN 1 ELSE 0 END) AS R22
						  , SUM(CASE WHEN A.NO_KEC =23 THEN 1 ELSE 0 END) AS R23
						  , SUM(CASE WHEN A.NO_KEC =24 THEN 1 ELSE 0 END) AS R24
						  , SUM(CASE WHEN A.NO_KEC =25 THEN 1 ELSE 0 END) AS R25
						  , SUM(CASE WHEN A.NO_KEC =26 THEN 1 ELSE 0 END) AS R26
						  , SUM(CASE WHEN A.NO_KEC =27 THEN 1 ELSE 0 END) AS R27
						  , SUM(CASE WHEN A.NO_KEC =28 THEN 1 ELSE 0 END) AS R28
						  , SUM(CASE WHEN A.NO_KEC =29 THEN 1 ELSE 0 END) AS R29
						  , SUM(CASE WHEN A.NO_KEC =30 THEN 1 ELSE 0 END) AS R30
						  FROM T5_SEQN_PRINT_KK B INNER JOIN DATA_KELUARGA A ON A.NO_KK = B.NO_KK
						 WHERE TO_CHAR(PRINTED_DATE,'MM/YYYY') = '".$bln."' AND  A.TIPE_KK = 1 AND A.NO_PROP = 32 AND A.NO_KAB =73
						 GROUP BY TRUNC(PRINTED_DATE)
						 ORDER BY TRUNC(PRINTED_DATE)) B 
						 ON A.DAY = B.DAY ORDER BY A.DAY";
				$r = DB::select($sql);
               return $r;
		}
		
		function mst_get_bln_kia($bln){
			$sql = "SELECT 
						  TO_CHAR(A.DAY,'DD-MM-YYYY') AS DAY
						  , CASE WHEN B.R1 IS NULL THEN 0 ELSE B.R1 END AS R1
						  , CASE WHEN B.R2 IS NULL THEN 0 ELSE B.R2 END AS R2
						  , CASE WHEN B.R3 IS NULL THEN 0 ELSE B.R3 END AS R3
						  , CASE WHEN B.R4 IS NULL THEN 0 ELSE B.R4 END AS R4
						  , CASE WHEN B.R5 IS NULL THEN 0 ELSE B.R5 END AS R5
						  , CASE WHEN B.R6 IS NULL THEN 0 ELSE B.R6 END AS R6
						  , CASE WHEN B.R7 IS NULL THEN 0 ELSE B.R7 END AS R7
						  , CASE WHEN B.R8 IS NULL THEN 0 ELSE B.R8 END AS R8
						  , CASE WHEN B.R9 IS NULL THEN 0 ELSE B.R9 END AS R9
						  , CASE WHEN B.R10 IS NULL THEN 0 ELSE B.R10 END AS R10
						  , CASE WHEN B.R11 IS NULL THEN 0 ELSE B.R11 END AS R11
						  , CASE WHEN B.R12 IS NULL THEN 0 ELSE B.R12 END AS R12
						  , CASE WHEN B.R13 IS NULL THEN 0 ELSE B.R13 END AS R13
						  , CASE WHEN B.R14 IS NULL THEN 0 ELSE B.R14 END AS R14
						  , CASE WHEN B.R15 IS NULL THEN 0 ELSE B.R15 END AS R15
						  , CASE WHEN B.R16 IS NULL THEN 0 ELSE B.R16 END AS R16
						  , CASE WHEN B.R17 IS NULL THEN 0 ELSE B.R17 END AS R17
						  , CASE WHEN B.R18 IS NULL THEN 0 ELSE B.R18 END AS R18
						  , CASE WHEN B.R19 IS NULL THEN 0 ELSE B.R19 END AS R19
						  , CASE WHEN B.R20 IS NULL THEN 0 ELSE B.R20 END AS R20
						  , CASE WHEN B.R21 IS NULL THEN 0 ELSE B.R21 END AS R21
						  , CASE WHEN B.R22 IS NULL THEN 0 ELSE B.R22 END AS R22
						  , CASE WHEN B.R23 IS NULL THEN 0 ELSE B.R23 END AS R23
						  , CASE WHEN B.R24 IS NULL THEN 0 ELSE B.R24 END AS R24
						  , CASE WHEN B.R25 IS NULL THEN 0 ELSE B.R25 END AS R25
						  , CASE WHEN B.R26 IS NULL THEN 0 ELSE B.R26 END AS R26
						  , CASE WHEN B.R27 IS NULL THEN 0 ELSE B.R27 END AS R27
						  , CASE WHEN B.R28 IS NULL THEN 0 ELSE B.R28 END AS R28
						  , CASE WHEN B.R29 IS NULL THEN 0 ELSE B.R29 END AS R29
						  , CASE WHEN B.R30 IS NULL THEN 0 ELSE B.R30 END AS R30
						FROM 
						  (SELECT 
						        TRUNC(TO_DATE('".$bln."','MM/YYYY'), 'MM') + LEVEL - 1 AS DAY 
						    FROM DUAL 
						CONNECT BY 
						        TRUNC(TRUNC(TO_DATE('".$bln."','MM/YYYY'), 'MM') + LEVEL - 1, 'MM') = TRUNC(TO_DATE('".$bln."','MM/YYYY'), 'MM')) A 
						    LEFT JOIN
						  (SELECT 
					        TRUNC(PRINTED_DATE) AS DAY
						  , SUM(CASE WHEN A.NO_KEC =1 THEN 1 ELSE 0 END) AS R1
						  , SUM(CASE WHEN A.NO_KEC =2 THEN 1 ELSE 0 END) AS R2
						  , SUM(CASE WHEN A.NO_KEC =3 THEN 1 ELSE 0 END) AS R3
						  , SUM(CASE WHEN A.NO_KEC =4 THEN 1 ELSE 0 END) AS R4
						  , SUM(CASE WHEN A.NO_KEC =5 THEN 1 ELSE 0 END) AS R5
						  , SUM(CASE WHEN A.NO_KEC =6 THEN 1 ELSE 0 END) AS R6
						  , SUM(CASE WHEN A.NO_KEC =7 THEN 1 ELSE 0 END) AS R7
						  , SUM(CASE WHEN A.NO_KEC =8 THEN 1 ELSE 0 END) AS R8
						  , SUM(CASE WHEN A.NO_KEC =9 THEN 1 ELSE 0 END) AS R9
						  , SUM(CASE WHEN A.NO_KEC =10 THEN 1 ELSE 0 END) AS R10
						  , SUM(CASE WHEN A.NO_KEC =11 THEN 1 ELSE 0 END) AS R11
						  , SUM(CASE WHEN A.NO_KEC =12 THEN 1 ELSE 0 END) AS R12
						  , SUM(CASE WHEN A.NO_KEC =13 THEN 1 ELSE 0 END) AS R13
						  , SUM(CASE WHEN A.NO_KEC =14 THEN 1 ELSE 0 END) AS R14
						  , SUM(CASE WHEN A.NO_KEC =15 THEN 1 ELSE 0 END) AS R15
						  , SUM(CASE WHEN A.NO_KEC =16 THEN 1 ELSE 0 END) AS R16
						  , SUM(CASE WHEN A.NO_KEC =17 THEN 1 ELSE 0 END) AS R17
						  , SUM(CASE WHEN A.NO_KEC =18 THEN 1 ELSE 0 END) AS R18
						  , SUM(CASE WHEN A.NO_KEC =19 THEN 1 ELSE 0 END) AS R19
						  , SUM(CASE WHEN A.NO_KEC =20 THEN 1 ELSE 0 END) AS R20
						  , SUM(CASE WHEN A.NO_KEC =21 THEN 1 ELSE 0 END) AS R21
						  , SUM(CASE WHEN A.NO_KEC =22 THEN 1 ELSE 0 END) AS R22
						  , SUM(CASE WHEN A.NO_KEC =23 THEN 1 ELSE 0 END) AS R23
						  , SUM(CASE WHEN A.NO_KEC =24 THEN 1 ELSE 0 END) AS R24
						  , SUM(CASE WHEN A.NO_KEC =25 THEN 1 ELSE 0 END) AS R25
						  , SUM(CASE WHEN A.NO_KEC =26 THEN 1 ELSE 0 END) AS R26
						  , SUM(CASE WHEN A.NO_KEC =27 THEN 1 ELSE 0 END) AS R27
						  , SUM(CASE WHEN A.NO_KEC =28 THEN 1 ELSE 0 END) AS R28
						  , SUM(CASE WHEN A.NO_KEC =29 THEN 1 ELSE 0 END) AS R29
						  , SUM(CASE WHEN A.NO_KEC =30 THEN 1 ELSE 0 END) AS R30
						  FROM T5_SEQN_KIA_PRINT B INNER JOIN BIODATA_WNI A ON A.NIK = B.NIK
						 WHERE TO_CHAR(PRINTED_DATE,'MM/YYYY') = '".$bln."' AND A.NO_PROP = 32 AND A.NO_KAB =73
						 GROUP BY TRUNC(PRINTED_DATE)
						 ORDER BY TRUNC(PRINTED_DATE)) B 
						 ON A.DAY = B.DAY ORDER BY A.DAY";
				$r = DB::select($sql);
               return $r;
		}
		public function mst_sisa_suket(){
			$sql = "SELECT 
					  A.NO_KEC
					  , A.NAMA_KEC
					  , A.NO_KEL
					  , A.NAMA_KEL
					  , TO_CHAR(A.NIK) NIK
					  , TO_CHAR(A.NO_KK) NO_KK
					  , A.NAMA_LGKP
					  , A.CURRENT_STATUS_CODE
					  , TO_CHAR(A.PRINTED_DATE,'YYYY-MM-DD') TGL_SUKET   
					  , TO_CHAR(A.PRINTED_DATE,'YYYY') THN_SUKET   
					  , CASE WHEN TO_CHAR(A.KTP_DT,'DD-MM-YYYY') IS NULL THEN '-' ELSE TO_CHAR(A.KTP_DT,'DD-MM-YYYY') END TGL_KTP
					  FROM (SELECT 
					  A.NO_KEC
					  , F5_GET_NAMA_KECAMATAN(A.NO_PROP,A.NO_KAB,A.NO_KEC) NAMA_KEC
					  , A.NO_KEL
					  , F5_GET_NAMA_KELURAHAN(A.NO_PROP,A.NO_KAB,A.NO_KEC,A.NO_KEL) NAMA_KEL
					  , A.NIK
					  , A.NO_KK
					  , A.NAMA_LGKP
					  , CASE WHEN D.CURRENT_STATUS_CODE IS NULL THEN '-' ELSE D.CURRENT_STATUS_CODE END AS CURRENT_STATUS_CODE
					  , B.PRINTED_DATE
					  , CASE WHEN  C.LAST_UPDATE IS NULL AND C.PERSONALIZED_DATE IS NULL THEN NULL WHEN C.LAST_UPDATE IS NULL AND C.PERSONALIZED_DATE IS NOT NULL THEN C.PERSONALIZED_DATE ELSE  C.LAST_UPDATE END AS KTP_DT
					  FROM BIODATA_WNI A 
					INNER JOIN (SELECT NIK, PRINTED_DATE,PRINTED_BY,NO_DOKUMEN  FROM (SELECT NIK,PRINTED_DATE,PRINTED_BY,NO_DOKUMEN, RANK() OVER (PARTITION BY NIK ORDER BY PRINTED_DATE,NO_DOKUMEN DESC) RNK FROM T7_HIST_SUKET) 
					WHERE RNK = 1) B ON A.NIK = B.NIK
					LEFT JOIN (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME,LAST_UPDATE FROM (SELECT NIK,PERSONALIZED_DATE,CREATED_USERNAME,LAST_UPDATED_USERNAME,LAST_UPDATE, RANK() OVER (PARTITION BY NIK ORDER BY  LAST_UPDATE DESC, PERSONALIZED_DATE DESC) RNK FROM CARD_MANAGEMENT@DB5CETAK) 
					WHERE RNK = 1) C ON A.NIK =C.NIK
					LEFT JOIN DEMOGRAPHICS_ALL@DB2 D ON A.NIK = D.NIK
					WHERE  (A.TGL_LHR<=ADD_MONTHS(SYSDATE,-12*17) OR A.STAT_KWN<>1) 
                    AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3))) 
                    AND A.NO_PROP = 32 AND A.NO_KAB =73 
					) A WHERE KTP_DT IS NULL AND (CURRENT_STATUS_CODE LIKE '%CARD%' OR CURRENT_STATUS_CODE = 'PRINT_READY_RECORD') 
					ORDER BY A.NO_KEC ASC,A.NO_KEL ASC,A.PRINTED_DATE DESC";
			$r = DB::connection('db222')->select($sql);
            return $r;
		}

		


		public function get_jumlah_laporan_kelamin($no_kec,$no_kel){
			$dkb_bio = Shr::do_get_dkb_bio();
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT 
						 SUM(CASE WHEN A.JENIS_KLMIN = 1 THEN 1 ELSE 0 END) LK
						, SUM(CASE WHEN A.JENIS_KLMIN = 2 THEN 1 ELSE 0 END) PR
						, COUNT(1) JUMLAH
						FROM ".$dkb_bio." A WHERE  (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) AND A.NO_PROP = 32 AND A.NO_KAB =73";	
			}else{
				$sql .="SELECT 
						 SUM(CASE WHEN A.JENIS_KLMIN = 1 THEN 1 ELSE 0 END) LK
						, SUM(CASE WHEN A.JENIS_KLMIN = 2 THEN 1 ELSE 0 END) PR
						, COUNT(1) JUMLAH
						FROM ".$dkb_bio." A WHERE (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) AND A.NO_PROP = 32 AND A.NO_KAB =73";
						if($no_kel == 0){    
					$sql .=" AND A.NO_KEC =".$no_kec." ";
						}else{
					$sql .=" AND A.NO_KEC =".$no_kec." and A.NO_KEL =".$no_kel." ";
						}
				}
				$r = DB::select($sql);
               return $r;
		}


		public function get_jumlah_laporan_wajib_ktp($no_kec,$no_kel){
			$dkb_bio = Shr::do_get_dkb_bio();
			$cut_off_dkb = Shr::do_get_cut_off_date();
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT 
						 SUM(CASE WHEN A.JENIS_KLMIN = 1 THEN 1 ELSE 0 END) LK
						, SUM(CASE WHEN A.JENIS_KLMIN = 2 THEN 1 ELSE 0 END) PR
						, COUNT(1) JUMLAH
						FROM ".$dkb_bio." A WHERE (A.TGL_LHR<=ADD_MONTHS(TO_DATE('".$cut_off_dkb."','DD/MM/YYYY'),-12*17) OR A.STAT_KWN<>1) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) AND A.NO_PROP = 32 AND A.NO_KAB =73";	
			}else{
				$sql .="SELECT 
						 SUM(CASE WHEN A.JENIS_KLMIN = 1 THEN 1 ELSE 0 END) LK
						, SUM(CASE WHEN A.JENIS_KLMIN = 2 THEN 1 ELSE 0 END) PR
						, COUNT(1) JUMLAH
						FROM ".$dkb_bio." A WHERE (A.TGL_LHR<=ADD_MONTHS(TO_DATE('".$cut_off_dkb."','DD/MM/YYYY'),-12*17) OR A.STAT_KWN<>1) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) AND A.NO_PROP = 32 AND A.NO_KAB =73";
						if($no_kel == 0){    
					$sql .=" AND A.NO_KEC =".$no_kec." ";
						}else{
					$sql .=" AND A.NO_KEC =".$no_kec." and A.NO_KEL =".$no_kel." ";
						}
				}
				$r = DB::select($sql);
               return $r;
		}


		public function get_jumlah_laporan_wajib_ktp_pelayanan($no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT 
						 SUM(CASE WHEN A.JENIS_KLMIN = 1 THEN 1 ELSE 0 END) LK
						, SUM(CASE WHEN A.JENIS_KLMIN = 2 THEN 1 ELSE 0 END) PR
						, COUNT(1) JUMLAH
						FROM BIODATA_WNI A WHERE (A.TGL_LHR<=ADD_MONTHS(SYSDATE,-12*17) OR A.STAT_KWN<>1) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3))) AND A.NO_PROP = 32 AND A.NO_KAB =73";	
			}else{
				$sql .="SELECT 
						 SUM(CASE WHEN A.JENIS_KLMIN = 1 THEN 1 ELSE 0 END) LK
						, SUM(CASE WHEN A.JENIS_KLMIN = 2 THEN 1 ELSE 0 END) PR
						, COUNT(1) JUMLAH
						FROM BIODATA_WNI A WHERE (A.TGL_LHR<=ADD_MONTHS(SYSDATE,-12*17) OR A.STAT_KWN<>1) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3))) AND A.NO_PROP = 32 AND A.NO_KAB =73";
						if($no_kel == 0){    
					$sql .=" AND A.NO_KEC =".$no_kec." ";
						}else{
					$sql .=" AND A.NO_KEC =".$no_kec." and A.NO_KEL =".$no_kel." ";
						}
				}
				$r = DB::connection('db222')->select($sql);
               return $r;
		}

		public function get_jumlah_laporan_non_ktp($no_kec,$no_kel){
			$dkb_bio = Shr::do_get_dkb_bio();
			$cut_off_dkb = Shr::do_get_cut_off_date();
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT 
						 SUM(CASE WHEN A.JENIS_KLMIN = 1 THEN 1 ELSE 0 END) LK
						, SUM(CASE WHEN A.JENIS_KLMIN = 2 THEN 1 ELSE 0 END) PR
						, COUNT(1) JUMLAH
						FROM ".$dkb_bio." A WHERE (A.TGL_LHR>ADD_MONTHS(TO_DATE('".$cut_off_dkb."','DD/MM/YYYY'),-12*17) AND A.STAT_KWN=1) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) AND A.NO_PROP = 32 AND A.NO_KAB =73";	
			}else{
				$sql .="SELECT 
						 SUM(CASE WHEN A.JENIS_KLMIN = 1 THEN 1 ELSE 0 END) LK
						, SUM(CASE WHEN A.JENIS_KLMIN = 2 THEN 1 ELSE 0 END) PR
						, COUNT(1) JUMLAH
						FROM ".$dkb_bio." A WHERE (A.TGL_LHR>ADD_MONTHS(TO_DATE('".$cut_off_dkb."','DD/MM/YYYY'),-12*17) AND A.STAT_KWN=1) AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) AND A.NO_PROP = 32 AND A.NO_KAB =73";
						if($no_kel == 0){    
					$sql .=" AND A.NO_KEC =".$no_kec." ";
						}else{
					$sql .=" AND A.NO_KEC =".$no_kec." and A.NO_KEL =".$no_kel." ";
						}
				}
				$r = DB::select($sql);
               return $r;
		}


		public function get_jumlah_laporan_kk_kelamin($no_kec,$no_kel){
			$dkb_bio = Shr::do_get_dkb_bio();
			$dkb_keluarga = Shr::do_get_dkb_keluarga();
			$cut_off_dkb = Shr::do_get_cut_off_date();
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT 
						 SUM(CASE WHEN A.JENIS_KLMIN = 1 THEN 1 ELSE 0 END) LK
						, SUM(CASE WHEN A.JENIS_KLMIN = 2 THEN 1 ELSE 0 END) PR
						, COUNT(1) JUMLAH
						FROM (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NO_KK, A.JENIS_KLMIN  FROM ".$dkb_bio." A INNER JOIN ".$dkb_keluarga." C ON A.NO_KK = C.NO_KK WHERE A.STAT_HBKEL =1 AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5)))) A WHERE  1=1 AND A.NO_PROP = 32 AND A.NO_KAB =73";	
			}else{
				$sql .="SELECT 
						 SUM(CASE WHEN A.JENIS_KLMIN = 1 THEN 1 ELSE 0 END) LK
						, SUM(CASE WHEN A.JENIS_KLMIN = 2 THEN 1 ELSE 0 END) PR
						, COUNT(1) JUMLAH
						FROM (SELECT A.NO_PROP,A.NO_KAB, A.NO_KEC,A.NO_KEL, A.NO_KK, A.JENIS_KLMIN  FROM ".$dkb_bio." A INNER JOIN ".$dkb_keluarga." C ON A.NO_KK = C.NO_KK WHERE A.STAT_HBKEL =1 AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))))  A WHERE 1=1 AND A.NO_PROP = 32 AND A.NO_KAB =73";
						if($no_kel == 0){    
					$sql .=" AND A.NO_KEC =".$no_kec." ";
						}else{
					$sql .=" AND A.NO_KEC =".$no_kec." and A.NO_KEL =".$no_kel." ";
						}
				}
				$r = DB::select($sql);
               return $r;
		}

		public function get_jumlah_laporan_pendidikan($no_kec,$no_kel){
			$dkb_bio = Shr::do_get_dkb_bio();
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT 
						 SUM(CASE WHEN A.PDDK_AKH NOT BETWEEN 2 AND 10 OR PDDK_AKH IS NULL THEN 1 ELSE 0 END) R1
						, SUM(CASE WHEN A.PDDK_AKH = 2 THEN 1 ELSE 0 END) R2
						, SUM(CASE WHEN A.PDDK_AKH = 3 THEN 1 ELSE 0 END) R3
						, SUM(CASE WHEN A.PDDK_AKH = 4 THEN 1 ELSE 0 END) R4
						, SUM(CASE WHEN A.PDDK_AKH = 5 THEN 1 ELSE 0 END) R5
						, SUM(CASE WHEN A.PDDK_AKH = 6 THEN 1 ELSE 0 END) R6
						, SUM(CASE WHEN A.PDDK_AKH = 7 THEN 1 ELSE 0 END) R7
						, SUM(CASE WHEN A.PDDK_AKH = 8 THEN 1 ELSE 0 END) R8
						, SUM(CASE WHEN A.PDDK_AKH = 9 THEN 1 ELSE 0 END) R9
						, SUM(CASE WHEN A.PDDK_AKH = 10 THEN 1 ELSE 0 END) R10
						, COUNT(1) JUMLAH
						FROM ".$dkb_bio." A WHERE  (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) AND A.NO_PROP = 32 AND A.NO_KAB =73";	
			}else{
				$sql .="SELECT 
						 SUM(CASE WHEN A.PDDK_AKH NOT BETWEEN 2 AND 10 OR PDDK_AKH IS NULL THEN 1 ELSE 0 END) R1
						, SUM(CASE WHEN A.PDDK_AKH = 2 THEN 1 ELSE 0 END) R2
						, SUM(CASE WHEN A.PDDK_AKH = 3 THEN 1 ELSE 0 END) R3
						, SUM(CASE WHEN A.PDDK_AKH = 4 THEN 1 ELSE 0 END) R4
						, SUM(CASE WHEN A.PDDK_AKH = 5 THEN 1 ELSE 0 END) R5
						, SUM(CASE WHEN A.PDDK_AKH = 6 THEN 1 ELSE 0 END) R6
						, SUM(CASE WHEN A.PDDK_AKH = 7 THEN 1 ELSE 0 END) R7
						, SUM(CASE WHEN A.PDDK_AKH = 8 THEN 1 ELSE 0 END) R8
						, SUM(CASE WHEN A.PDDK_AKH = 9 THEN 1 ELSE 0 END) R9
						, SUM(CASE WHEN A.PDDK_AKH = 10 THEN 1 ELSE 0 END) R10
						, COUNT(1) JUMLAH
						FROM ".$dkb_bio." A WHERE (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) AND A.NO_PROP = 32 AND A.NO_KAB =73";
						if($no_kel == 0){    
					$sql .=" AND A.NO_KEC =".$no_kec." ";
						}else{
					$sql .=" AND A.NO_KEC =".$no_kec." and A.NO_KEL =".$no_kel." ";
						}
				}
				$r = DB::select($sql);
               return $r;
		}

		public function get_jumlah_laporan_pendidikan_laki_laki($no_kec,$no_kel){
			$dkb_bio = Shr::do_get_dkb_bio();
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT 
						 SUM(CASE WHEN A.PDDK_AKH NOT BETWEEN 2 AND 10 OR PDDK_AKH IS NULL THEN 1 ELSE 0 END) R1
						, SUM(CASE WHEN A.PDDK_AKH = 2 THEN 1 ELSE 0 END) R2
						, SUM(CASE WHEN A.PDDK_AKH = 3 THEN 1 ELSE 0 END) R3
						, SUM(CASE WHEN A.PDDK_AKH = 4 THEN 1 ELSE 0 END) R4
						, SUM(CASE WHEN A.PDDK_AKH = 5 THEN 1 ELSE 0 END) R5
						, SUM(CASE WHEN A.PDDK_AKH = 6 THEN 1 ELSE 0 END) R6
						, SUM(CASE WHEN A.PDDK_AKH = 7 THEN 1 ELSE 0 END) R7
						, SUM(CASE WHEN A.PDDK_AKH = 8 THEN 1 ELSE 0 END) R8
						, SUM(CASE WHEN A.PDDK_AKH = 9 THEN 1 ELSE 0 END) R9
						, SUM(CASE WHEN A.PDDK_AKH = 10 THEN 1 ELSE 0 END) R10
						, COUNT(1) JUMLAH
						FROM ".$dkb_bio." A WHERE JENIS_KLMIN = 1 AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) AND A.NO_PROP = 32 AND A.NO_KAB =73";	
			}else{
				$sql .="SELECT 
						 SUM(CASE WHEN A.PDDK_AKH NOT BETWEEN 2 AND 10 OR PDDK_AKH IS NULL THEN 1 ELSE 0 END) R1
						, SUM(CASE WHEN A.PDDK_AKH = 2 THEN 1 ELSE 0 END) R2
						, SUM(CASE WHEN A.PDDK_AKH = 3 THEN 1 ELSE 0 END) R3
						, SUM(CASE WHEN A.PDDK_AKH = 4 THEN 1 ELSE 0 END) R4
						, SUM(CASE WHEN A.PDDK_AKH = 5 THEN 1 ELSE 0 END) R5
						, SUM(CASE WHEN A.PDDK_AKH = 6 THEN 1 ELSE 0 END) R6
						, SUM(CASE WHEN A.PDDK_AKH = 7 THEN 1 ELSE 0 END) R7
						, SUM(CASE WHEN A.PDDK_AKH = 8 THEN 1 ELSE 0 END) R8
						, SUM(CASE WHEN A.PDDK_AKH = 9 THEN 1 ELSE 0 END) R9
						, SUM(CASE WHEN A.PDDK_AKH = 10 THEN 1 ELSE 0 END) R10
						, COUNT(1) JUMLAH
						FROM ".$dkb_bio." A WHERE JENIS_KLMIN = 1 AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) AND A.NO_PROP = 32 AND A.NO_KAB =73";
						if($no_kel == 0){    
					$sql .=" AND A.NO_KEC =".$no_kec." ";
						}else{
					$sql .=" AND A.NO_KEC =".$no_kec." and A.NO_KEL =".$no_kel." ";
						}
				}
				$r = DB::select($sql);
               return $r;
		}

		public function get_jumlah_laporan_pendidikan_perempuan($no_kec,$no_kel){
			$dkb_bio = Shr::do_get_dkb_bio();
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT 
						 SUM(CASE WHEN A.PDDK_AKH NOT BETWEEN 2 AND 10 OR PDDK_AKH IS NULL THEN 1 ELSE 0 END) R1
						, SUM(CASE WHEN A.PDDK_AKH = 2 THEN 1 ELSE 0 END) R2
						, SUM(CASE WHEN A.PDDK_AKH = 3 THEN 1 ELSE 0 END) R3
						, SUM(CASE WHEN A.PDDK_AKH = 4 THEN 1 ELSE 0 END) R4
						, SUM(CASE WHEN A.PDDK_AKH = 5 THEN 1 ELSE 0 END) R5
						, SUM(CASE WHEN A.PDDK_AKH = 6 THEN 1 ELSE 0 END) R6
						, SUM(CASE WHEN A.PDDK_AKH = 7 THEN 1 ELSE 0 END) R7
						, SUM(CASE WHEN A.PDDK_AKH = 8 THEN 1 ELSE 0 END) R8
						, SUM(CASE WHEN A.PDDK_AKH = 9 THEN 1 ELSE 0 END) R9
						, SUM(CASE WHEN A.PDDK_AKH = 10 THEN 1 ELSE 0 END) R10
						, COUNT(1) JUMLAH
						FROM ".$dkb_bio." A WHERE JENIS_KLMIN = 2 AND  (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) AND A.NO_PROP = 32 AND A.NO_KAB =73";	
			}else{
				$sql .="SELECT 
						 SUM(CASE WHEN A.PDDK_AKH NOT BETWEEN 2 AND 10 OR PDDK_AKH IS NULL THEN 1 ELSE 0 END) R1
						, SUM(CASE WHEN A.PDDK_AKH = 2 THEN 1 ELSE 0 END) R2
						, SUM(CASE WHEN A.PDDK_AKH = 3 THEN 1 ELSE 0 END) R3
						, SUM(CASE WHEN A.PDDK_AKH = 4 THEN 1 ELSE 0 END) R4
						, SUM(CASE WHEN A.PDDK_AKH = 5 THEN 1 ELSE 0 END) R5
						, SUM(CASE WHEN A.PDDK_AKH = 6 THEN 1 ELSE 0 END) R6
						, SUM(CASE WHEN A.PDDK_AKH = 7 THEN 1 ELSE 0 END) R7
						, SUM(CASE WHEN A.PDDK_AKH = 8 THEN 1 ELSE 0 END) R8
						, SUM(CASE WHEN A.PDDK_AKH = 9 THEN 1 ELSE 0 END) R9
						, SUM(CASE WHEN A.PDDK_AKH = 10 THEN 1 ELSE 0 END) R10
						, COUNT(1) JUMLAH
						FROM ".$dkb_bio." A WHERE JENIS_KLMIN = 2 AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) AND A.NO_PROP = 32 AND A.NO_KAB =73";
						if($no_kel == 0){    
					$sql .=" AND A.NO_KEC =".$no_kec." ";
						}else{
					$sql .=" AND A.NO_KEC =".$no_kec." and A.NO_KEL =".$no_kel." ";
						}
				}
				$r = DB::select($sql);
               return $r;
		}

		public function get_jumlah_laporan_pekerjaan($no_kec,$no_kel){
			$dkb_bio = Shr::do_get_dkb_bio();
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT 
						 SUM(CASE WHEN A.JENIS_PKRJN IN (1) THEN 1 ELSE 0 END) R1
						, SUM(CASE WHEN A.JENIS_PKRJN IN (2) THEN 1 ELSE 0 END) R2
						, SUM(CASE WHEN A.JENIS_PKRJN IN (3) THEN 1 ELSE 0 END) R3
						, SUM(CASE WHEN A.JENIS_PKRJN IN (5) THEN 1 ELSE 0 END) R4
						, SUM(CASE WHEN A.JENIS_PKRJN IN (6,7) THEN 1 ELSE 0 END) R5
						, SUM(CASE WHEN A.JENIS_PKRJN IN (4) THEN 1 ELSE 0 END) R6
						, SUM(CASE WHEN A.JENIS_PKRJN IN (15) THEN 1 ELSE 0 END) R7
						, SUM(CASE WHEN A.JENIS_PKRJN IN (16,17) THEN 1 ELSE 0 END) R8
						, SUM(CASE WHEN A.JENIS_PKRJN IN (31,72,73,74,75,76) THEN 1 ELSE 0 END) R9
						, SUM(CASE WHEN A.JENIS_PKRJN IN (88) THEN 1 ELSE 0 END) R10 
            			, SUM(CASE WHEN A.JENIS_PKRJN IN (64,65) THEN 1 ELSE 0 END) R11
            			, SUM(CASE WHEN A.JENIS_PKRJN NOT IN (1,2,3,4,5,6,7,15,16,17,31,64,65,72,73,74,75,76,88) THEN 1 ELSE 0 END) R12
						, COUNT(1) JUMLAH
						FROM ".$dkb_bio." A WHERE  (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) AND A.NO_PROP = 32 AND A.NO_KAB =73";	
			}else{
				$sql .="SELECT 
						 SUM(CASE WHEN A.JENIS_PKRJN IN (1) THEN 1 ELSE 0 END) R1
						, SUM(CASE WHEN A.JENIS_PKRJN IN (2) THEN 1 ELSE 0 END) R2
						, SUM(CASE WHEN A.JENIS_PKRJN IN (3) THEN 1 ELSE 0 END) R3
						, SUM(CASE WHEN A.JENIS_PKRJN IN (5) THEN 1 ELSE 0 END) R4
						, SUM(CASE WHEN A.JENIS_PKRJN IN (6,7) THEN 1 ELSE 0 END) R5
						, SUM(CASE WHEN A.JENIS_PKRJN IN (4) THEN 1 ELSE 0 END) R6
						, SUM(CASE WHEN A.JENIS_PKRJN IN (15) THEN 1 ELSE 0 END) R7
						, SUM(CASE WHEN A.JENIS_PKRJN IN (16,17) THEN 1 ELSE 0 END) R8
						, SUM(CASE WHEN A.JENIS_PKRJN IN (31,72,73,74,75,76) THEN 1 ELSE 0 END) R9
						, SUM(CASE WHEN A.JENIS_PKRJN IN (88) THEN 1 ELSE 0 END) R10 
            			, SUM(CASE WHEN A.JENIS_PKRJN IN (64,65) THEN 1 ELSE 0 END) R11
            			, SUM(CASE WHEN A.JENIS_PKRJN NOT IN (1,2,3,4,5,6,7,15,16,17,31,64,65,72,73,74,75,76,88) THEN 1 ELSE 0 END) R12
						, COUNT(1) JUMLAH
						FROM ".$dkb_bio." A WHERE (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) AND A.NO_PROP = 32 AND A.NO_KAB =73";
						if($no_kel == 0){    
					$sql .=" AND A.NO_KEC =".$no_kec." ";
						}else{
					$sql .=" AND A.NO_KEC =".$no_kec." and A.NO_KEL =".$no_kel." ";
						}
				}
				$r = DB::select($sql);
               return $r;
		}
		public function get_jumlah_laporan_pekerjaan_laki_laki($no_kec,$no_kel){
			$dkb_bio = Shr::do_get_dkb_bio();
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT 
						 SUM(CASE WHEN A.JENIS_PKRJN IN (1) THEN 1 ELSE 0 END) R1
						, SUM(CASE WHEN A.JENIS_PKRJN IN (2) THEN 1 ELSE 0 END) R2
						, SUM(CASE WHEN A.JENIS_PKRJN IN (3) THEN 1 ELSE 0 END) R3
						, SUM(CASE WHEN A.JENIS_PKRJN IN (5) THEN 1 ELSE 0 END) R4
						, SUM(CASE WHEN A.JENIS_PKRJN IN (6,7) THEN 1 ELSE 0 END) R5
						, SUM(CASE WHEN A.JENIS_PKRJN IN (4) THEN 1 ELSE 0 END) R6
						, SUM(CASE WHEN A.JENIS_PKRJN IN (15) THEN 1 ELSE 0 END) R7
						, SUM(CASE WHEN A.JENIS_PKRJN IN (16,17) THEN 1 ELSE 0 END) R8
						, SUM(CASE WHEN A.JENIS_PKRJN IN (31,72,73,74,75,76) THEN 1 ELSE 0 END) R9
						, SUM(CASE WHEN A.JENIS_PKRJN IN (88) THEN 1 ELSE 0 END) R10 
            			, SUM(CASE WHEN A.JENIS_PKRJN IN (64,65) THEN 1 ELSE 0 END) R11
            			, SUM(CASE WHEN A.JENIS_PKRJN NOT IN (1,2,3,4,5,6,7,15,16,17,31,64,65,72,73,74,75,76,88) THEN 1 ELSE 0 END) R12
						, COUNT(1) JUMLAH
						FROM ".$dkb_bio." A WHERE JENIS_KLMIN = 1 AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) AND A.NO_PROP = 32 AND A.NO_KAB =73";	
			}else{
				$sql .="SELECT 
						 SUM(CASE WHEN A.JENIS_PKRJN IN (1) THEN 1 ELSE 0 END) R1
						, SUM(CASE WHEN A.JENIS_PKRJN IN (2) THEN 1 ELSE 0 END) R2
						, SUM(CASE WHEN A.JENIS_PKRJN IN (3) THEN 1 ELSE 0 END) R3
						, SUM(CASE WHEN A.JENIS_PKRJN IN (5) THEN 1 ELSE 0 END) R4
						, SUM(CASE WHEN A.JENIS_PKRJN IN (6,7) THEN 1 ELSE 0 END) R5
						, SUM(CASE WHEN A.JENIS_PKRJN IN (4) THEN 1 ELSE 0 END) R6
						, SUM(CASE WHEN A.JENIS_PKRJN IN (15) THEN 1 ELSE 0 END) R7
						, SUM(CASE WHEN A.JENIS_PKRJN IN (16,17) THEN 1 ELSE 0 END) R8
						, SUM(CASE WHEN A.JENIS_PKRJN IN (31,72,73,74,75,76) THEN 1 ELSE 0 END) R9
						, SUM(CASE WHEN A.JENIS_PKRJN IN (88) THEN 1 ELSE 0 END) R10 
            			, SUM(CASE WHEN A.JENIS_PKRJN IN (64,65) THEN 1 ELSE 0 END) R11
            			, SUM(CASE WHEN A.JENIS_PKRJN NOT IN (1,2,3,4,5,6,7,15,16,17,31,64,65,72,73,74,75,76,88) THEN 1 ELSE 0 END) R12
						, COUNT(1) JUMLAH
						FROM ".$dkb_bio." A WHERE JENIS_KLMIN = 1 AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) AND A.NO_PROP = 32 AND A.NO_KAB =73";
						if($no_kel == 0){    
					$sql .=" AND A.NO_KEC =".$no_kec." ";
						}else{
					$sql .=" AND A.NO_KEC =".$no_kec." and A.NO_KEL =".$no_kel." ";
						}
				}
				$r = DB::select($sql);
               return $r;
		}
		public function get_jumlah_laporan_pekerjaan_perempuan($no_kec,$no_kel){
			$dkb_bio = Shr::do_get_dkb_bio();
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT 
						 SUM(CASE WHEN A.JENIS_PKRJN IN (1) THEN 1 ELSE 0 END) R1
						, SUM(CASE WHEN A.JENIS_PKRJN IN (2) THEN 1 ELSE 0 END) R2
						, SUM(CASE WHEN A.JENIS_PKRJN IN (3) THEN 1 ELSE 0 END) R3
						, SUM(CASE WHEN A.JENIS_PKRJN IN (5) THEN 1 ELSE 0 END) R4
						, SUM(CASE WHEN A.JENIS_PKRJN IN (6,7) THEN 1 ELSE 0 END) R5
						, SUM(CASE WHEN A.JENIS_PKRJN IN (4) THEN 1 ELSE 0 END) R6
						, SUM(CASE WHEN A.JENIS_PKRJN IN (15) THEN 1 ELSE 0 END) R7
						, SUM(CASE WHEN A.JENIS_PKRJN IN (16,17) THEN 1 ELSE 0 END) R8
						, SUM(CASE WHEN A.JENIS_PKRJN IN (72,73,74,75,76) THEN 1 ELSE 0 END) R9
						, SUM(CASE WHEN A.JENIS_PKRJN IN (88) THEN 1 ELSE 0 END) R10 
            			, SUM(CASE WHEN A.JENIS_PKRJN IN (64,65) THEN 1 ELSE 0 END) R11
            			, SUM(CASE WHEN A.JENIS_PKRJN NOT IN (1,2,3,4,5,6,7,15,16,17,64,65,72,73,74,75,76,88) THEN 1 ELSE 0 END) R12
						, COUNT(1) JUMLAH
						FROM ".$dkb_bio." A WHERE JENIS_KLMIN = 2 AND  (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) AND A.NO_PROP = 32 AND A.NO_KAB =73";	
			}else{
				$sql .="SELECT 
						 SUM(CASE WHEN A.JENIS_PKRJN IN (1) THEN 1 ELSE 0 END) R1
						, SUM(CASE WHEN A.JENIS_PKRJN IN (2) THEN 1 ELSE 0 END) R2
						, SUM(CASE WHEN A.JENIS_PKRJN IN (3) THEN 1 ELSE 0 END) R3
						, SUM(CASE WHEN A.JENIS_PKRJN IN (5) THEN 1 ELSE 0 END) R4
						, SUM(CASE WHEN A.JENIS_PKRJN IN (6,7) THEN 1 ELSE 0 END) R5
						, SUM(CASE WHEN A.JENIS_PKRJN IN (4) THEN 1 ELSE 0 END) R6
						, SUM(CASE WHEN A.JENIS_PKRJN IN (15) THEN 1 ELSE 0 END) R7
						, SUM(CASE WHEN A.JENIS_PKRJN IN (16,17) THEN 1 ELSE 0 END) R8
						, SUM(CASE WHEN A.JENIS_PKRJN IN (72,73,74,75,76) THEN 1 ELSE 0 END) R9
						, SUM(CASE WHEN A.JENIS_PKRJN IN (88) THEN 1 ELSE 0 END) R10 
            			, SUM(CASE WHEN A.JENIS_PKRJN IN (64,65) THEN 1 ELSE 0 END) R11
            			, SUM(CASE WHEN A.JENIS_PKRJN NOT IN (1,2,3,4,5,6,7,15,16,17,64,65,72,73,74,75,76,88) THEN 1 ELSE 0 END) R12
						, COUNT(1) JUMLAH
						FROM ".$dkb_bio." A WHERE JENIS_KLMIN = 2 AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) AND A.NO_PROP = 32 AND A.NO_KAB =73";
						if($no_kel == 0){    
					$sql .=" AND A.NO_KEC =".$no_kec." ";
						}else{
					$sql .=" AND A.NO_KEC =".$no_kec." and A.NO_KEL =".$no_kel." ";
						}
				}
				$r = DB::select($sql);
               return $r;
		}
		public function get_jumlah_laporan_status_kawin($no_kec,$no_kel){
			$dkb_bio = Shr::do_get_dkb_bio();
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT 
						 SUM(CASE WHEN A.STAT_KWN NOT BETWEEN 2 AND 4 OR STAT_KWN IS NULL THEN 1 ELSE 0 END) R1
						, SUM(CASE WHEN A.STAT_KWN = 2 THEN 1 ELSE 0 END) R2
						, SUM(CASE WHEN A.STAT_KWN = 3 THEN 1 ELSE 0 END) R3
						, SUM(CASE WHEN A.STAT_KWN = 4 THEN 1 ELSE 0 END) R4
						, COUNT(1) JUMLAH
						FROM ".$dkb_bio." A WHERE  (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) AND A.NO_PROP = 32 AND A.NO_KAB =73";	
			}else{
				$sql .="SELECT 
						 SUM(CASE WHEN A.STAT_KWN NOT BETWEEN 2 AND 4 OR STAT_KWN IS NULL THEN 1 ELSE 0 END) R1
						, SUM(CASE WHEN A.STAT_KWN = 2 THEN 1 ELSE 0 END) R2
						, SUM(CASE WHEN A.STAT_KWN = 3 THEN 1 ELSE 0 END) R3
						, SUM(CASE WHEN A.STAT_KWN = 4 THEN 1 ELSE 0 END) R4
						, COUNT(1) JUMLAH
						FROM ".$dkb_bio." A WHERE (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) AND A.NO_PROP = 32 AND A.NO_KAB =73";
						if($no_kel == 0){    
					$sql .=" AND A.NO_KEC =".$no_kec." ";
						}else{
					$sql .=" AND A.NO_KEC =".$no_kec." and A.NO_KEL =".$no_kel." ";
						}
				}
				$r = DB::select($sql);
               return $r;
		}
		public function get_jumlah_laporan_status_kawin_laki_laki($no_kec,$no_kel){
			$dkb_bio = Shr::do_get_dkb_bio();
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT 
						 SUM(CASE WHEN A.STAT_KWN NOT BETWEEN 2 AND 4 OR STAT_KWN IS NULL THEN 1 ELSE 0 END) R1
						, SUM(CASE WHEN A.STAT_KWN = 2 THEN 1 ELSE 0 END) R2
						, SUM(CASE WHEN A.STAT_KWN = 3 THEN 1 ELSE 0 END) R3
						, SUM(CASE WHEN A.STAT_KWN = 4 THEN 1 ELSE 0 END) R4
						, COUNT(1) JUMLAH
						FROM ".$dkb_bio." A WHERE JENIS_KLMIN = 1 AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) AND A.NO_PROP = 32 AND A.NO_KAB =73";	
			}else{
				$sql .="SELECT 
						 SUM(CASE WHEN A.STAT_KWN NOT BETWEEN 2 AND 4 OR STAT_KWN IS NULL THEN 1 ELSE 0 END) R1
						, SUM(CASE WHEN A.STAT_KWN = 2 THEN 1 ELSE 0 END) R2
						, SUM(CASE WHEN A.STAT_KWN = 3 THEN 1 ELSE 0 END) R3
						, SUM(CASE WHEN A.STAT_KWN = 4 THEN 1 ELSE 0 END) R4
						, COUNT(1) JUMLAH
						FROM ".$dkb_bio." A WHERE JENIS_KLMIN = 1 AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) AND A.NO_PROP = 32 AND A.NO_KAB =73";
						if($no_kel == 0){    
					$sql .=" AND A.NO_KEC =".$no_kec." ";
						}else{
					$sql .=" AND A.NO_KEC =".$no_kec." and A.NO_KEL =".$no_kel." ";
						}
				}
				$r = DB::select($sql);
               return $r;
		}
		public function get_jumlah_laporan_status_kawin_perempuan($no_kec,$no_kel){
			$dkb_bio = Shr::do_get_dkb_bio();
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT 
						 SUM(CASE WHEN A.STAT_KWN NOT BETWEEN 2 AND 4 OR STAT_KWN IS NULL THEN 1 ELSE 0 END) R1
						, SUM(CASE WHEN A.STAT_KWN = 2 THEN 1 ELSE 0 END) R2
						, SUM(CASE WHEN A.STAT_KWN = 3 THEN 1 ELSE 0 END) R3
						, SUM(CASE WHEN A.STAT_KWN = 4 THEN 1 ELSE 0 END) R4
						, COUNT(1) JUMLAH
						FROM ".$dkb_bio." A WHERE JENIS_KLMIN = 2 AND  (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) AND A.NO_PROP = 32 AND A.NO_KAB =73";	
			}else{
				$sql .="SELECT 
						 SUM(CASE WHEN A.STAT_KWN NOT BETWEEN 2 AND 4 OR STAT_KWN IS NULL THEN 1 ELSE 0 END) R1
						, SUM(CASE WHEN A.STAT_KWN = 2 THEN 1 ELSE 0 END) R2
						, SUM(CASE WHEN A.STAT_KWN = 3 THEN 1 ELSE 0 END) R3
						, SUM(CASE WHEN A.STAT_KWN = 4 THEN 1 ELSE 0 END) R4
						, COUNT(1) JUMLAH
						FROM ".$dkb_bio." A WHERE JENIS_KLMIN = 2 AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) AND A.NO_PROP = 32 AND A.NO_KAB =73";
						if($no_kel == 0){    
					$sql .=" AND A.NO_KEC =".$no_kec." ";
						}else{
					$sql .=" AND A.NO_KEC =".$no_kec." and A.NO_KEL =".$no_kel." ";
						}
				}
				$r = DB::select($sql);
               return $r;
		}
		public function get_jumlah_laporan_agama($no_kec,$no_kel){
			$dkb_bio = Shr::do_get_dkb_bio();
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT 
						 SUM(CASE WHEN A.AGAMA NOT BETWEEN 2 AND 7 OR AGAMA IS NULL THEN 1 ELSE 0 END) R1
						, SUM(CASE WHEN A.AGAMA = 2 THEN 1 ELSE 0 END) R2
						, SUM(CASE WHEN A.AGAMA = 3 THEN 1 ELSE 0 END) R3
						, SUM(CASE WHEN A.AGAMA = 4 THEN 1 ELSE 0 END) R4
						, SUM(CASE WHEN A.AGAMA = 5 THEN 1 ELSE 0 END) R5
						, SUM(CASE WHEN A.AGAMA = 6 THEN 1 ELSE 0 END) R6
						, SUM(CASE WHEN A.AGAMA = 7 THEN 1 ELSE 0 END) R7
						, COUNT(1) JUMLAH
						FROM ".$dkb_bio." A WHERE  (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) AND A.NO_PROP = 32 AND A.NO_KAB =73";	
			}else{
				$sql .="SELECT 
						 SUM(CASE WHEN A.AGAMA NOT BETWEEN 2 AND 7 OR AGAMA IS NULL THEN 1 ELSE 0 END) R1
						, SUM(CASE WHEN A.AGAMA = 2 THEN 1 ELSE 0 END) R2
						, SUM(CASE WHEN A.AGAMA = 3 THEN 1 ELSE 0 END) R3
						, SUM(CASE WHEN A.AGAMA = 4 THEN 1 ELSE 0 END) R4
						, SUM(CASE WHEN A.AGAMA = 5 THEN 1 ELSE 0 END) R5
						, SUM(CASE WHEN A.AGAMA = 6 THEN 1 ELSE 0 END) R6
						, SUM(CASE WHEN A.AGAMA = 7 THEN 1 ELSE 0 END) R7
						, COUNT(1) JUMLAH
						FROM ".$dkb_bio." A WHERE (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) AND A.NO_PROP = 32 AND A.NO_KAB =73";
						if($no_kel == 0){    
					$sql .=" AND A.NO_KEC =".$no_kec." ";
						}else{
					$sql .=" AND A.NO_KEC =".$no_kec." and A.NO_KEL =".$no_kel." ";
						}
				}
				$r = DB::select($sql);
               return $r;
		}
		public function get_jumlah_laporan_agama_laki_laki($no_kec,$no_kel){
			$dkb_bio = Shr::do_get_dkb_bio();
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT 
						 SUM(CASE WHEN A.AGAMA NOT BETWEEN 2 AND 7 OR AGAMA IS NULL THEN 1 ELSE 0 END) R1
						, SUM(CASE WHEN A.AGAMA = 2 THEN 1 ELSE 0 END) R2
						, SUM(CASE WHEN A.AGAMA = 3 THEN 1 ELSE 0 END) R3
						, SUM(CASE WHEN A.AGAMA = 4 THEN 1 ELSE 0 END) R4
						, SUM(CASE WHEN A.AGAMA = 5 THEN 1 ELSE 0 END) R5
						, SUM(CASE WHEN A.AGAMA = 6 THEN 1 ELSE 0 END) R6
						, SUM(CASE WHEN A.AGAMA = 7 THEN 1 ELSE 0 END) R7
						, COUNT(1) JUMLAH
						FROM ".$dkb_bio." A WHERE JENIS_KLMIN = 1 AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) AND A.NO_PROP = 32 AND A.NO_KAB =73";	
			}else{
				$sql .="SELECT 
						 SUM(CASE WHEN A.AGAMA NOT BETWEEN 2 AND 7 OR AGAMA IS NULL THEN 1 ELSE 0 END) R1
						, SUM(CASE WHEN A.AGAMA = 2 THEN 1 ELSE 0 END) R2
						, SUM(CASE WHEN A.AGAMA = 3 THEN 1 ELSE 0 END) R3
						, SUM(CASE WHEN A.AGAMA = 4 THEN 1 ELSE 0 END) R4
						, SUM(CASE WHEN A.AGAMA = 5 THEN 1 ELSE 0 END) R5
						, SUM(CASE WHEN A.AGAMA = 6 THEN 1 ELSE 0 END) R6
						, SUM(CASE WHEN A.AGAMA = 7 THEN 1 ELSE 0 END) R7
						, COUNT(1) JUMLAH
						FROM ".$dkb_bio." A WHERE JENIS_KLMIN = 1 AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) AND A.NO_PROP = 32 AND A.NO_KAB =73";
						if($no_kel == 0){    
					$sql .=" AND A.NO_KEC =".$no_kec." ";
						}else{
					$sql .=" AND A.NO_KEC =".$no_kec." and A.NO_KEL =".$no_kel." ";
						}
				}
				$r = DB::select($sql);
               return $r;
		}
		public function get_jumlah_laporan_agama_perempuan($no_kec,$no_kel){
			$dkb_bio = Shr::do_get_dkb_bio();
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT 
						 SUM(CASE WHEN A.AGAMA NOT BETWEEN 2 AND 7 OR AGAMA IS NULL THEN 1 ELSE 0 END) R1
						, SUM(CASE WHEN A.AGAMA = 2 THEN 1 ELSE 0 END) R2
						, SUM(CASE WHEN A.AGAMA = 3 THEN 1 ELSE 0 END) R3
						, SUM(CASE WHEN A.AGAMA = 4 THEN 1 ELSE 0 END) R4
						, SUM(CASE WHEN A.AGAMA = 5 THEN 1 ELSE 0 END) R5
						, SUM(CASE WHEN A.AGAMA = 6 THEN 1 ELSE 0 END) R6
						, SUM(CASE WHEN A.AGAMA = 7 THEN 1 ELSE 0 END) R7
						, COUNT(1) JUMLAH
						FROM ".$dkb_bio." A WHERE JENIS_KLMIN = 2 AND  (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) AND A.NO_PROP = 32 AND A.NO_KAB =73";	
			}else{
				$sql .="SELECT 
						 SUM(CASE WHEN A.AGAMA NOT BETWEEN 2 AND 7 OR AGAMA IS NULL THEN 1 ELSE 0 END) R1
						, SUM(CASE WHEN A.AGAMA = 2 THEN 1 ELSE 0 END) R2
						, SUM(CASE WHEN A.AGAMA = 3 THEN 1 ELSE 0 END) R3
						, SUM(CASE WHEN A.AGAMA = 4 THEN 1 ELSE 0 END) R4
						, SUM(CASE WHEN A.AGAMA = 5 THEN 1 ELSE 0 END) R5
						, SUM(CASE WHEN A.AGAMA = 6 THEN 1 ELSE 0 END) R6
						, SUM(CASE WHEN A.AGAMA = 7 THEN 1 ELSE 0 END) R7
						, COUNT(1) JUMLAH
						FROM ".$dkb_bio." A WHERE JENIS_KLMIN = 2 AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) AND A.NO_PROP = 32 AND A.NO_KAB =73";
						if($no_kel == 0){    
					$sql .=" AND A.NO_KEC =".$no_kec." ";
						}else{
					$sql .=" AND A.NO_KEC =".$no_kec." and A.NO_KEL =".$no_kel." ";
						}
				}
				$r = DB::select($sql);
               return $r;
		}

		public function get_jumlah_laporan_gol_drh($no_kec,$no_kel){
			$dkb_bio = Shr::do_get_dkb_bio();
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT 
						 SUM(CASE WHEN A.GOL_DRH  = 1 THEN 1 ELSE 0 END) R1
						, SUM(CASE WHEN A.GOL_DRH = 2 THEN 1 ELSE 0 END) R2
						, SUM(CASE WHEN A.GOL_DRH = 3 THEN 1 ELSE 0 END) R3
						, SUM(CASE WHEN A.GOL_DRH = 4 THEN 1 ELSE 0 END) R4
						, SUM(CASE WHEN A.GOL_DRH = 5 THEN 1 ELSE 0 END) R5
						, SUM(CASE WHEN A.GOL_DRH = 6 THEN 1 ELSE 0 END) R6
						, SUM(CASE WHEN A.GOL_DRH = 7 THEN 1 ELSE 0 END) R7
						, SUM(CASE WHEN A.GOL_DRH = 8 THEN 1 ELSE 0 END) R8
						, SUM(CASE WHEN A.GOL_DRH = 9 THEN 1 ELSE 0 END) R9
						, SUM(CASE WHEN A.GOL_DRH = 10 THEN 1 ELSE 0 END) R10
						, SUM(CASE WHEN A.GOL_DRH = 11 THEN 1 ELSE 0 END) R11
						, SUM(CASE WHEN A.GOL_DRH = 12 THEN 1 ELSE 0 END) R12
						, SUM(CASE WHEN A.GOL_DRH NOT BETWEEN 1 AND 12 OR GOL_DRH IS NULL THEN 1 ELSE 0 END) R13
						, COUNT(1) JUMLAH
						FROM ".$dkb_bio." A WHERE  (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) AND A.NO_PROP = 32 AND A.NO_KAB =73";	
			}else{
				$sql .="SELECT 
						 SUM(CASE WHEN A.GOL_DRH  = 1 THEN 1 ELSE 0 END) R1
						, SUM(CASE WHEN A.GOL_DRH = 2 THEN 1 ELSE 0 END) R2
						, SUM(CASE WHEN A.GOL_DRH = 3 THEN 1 ELSE 0 END) R3
						, SUM(CASE WHEN A.GOL_DRH = 4 THEN 1 ELSE 0 END) R4
						, SUM(CASE WHEN A.GOL_DRH = 5 THEN 1 ELSE 0 END) R5
						, SUM(CASE WHEN A.GOL_DRH = 6 THEN 1 ELSE 0 END) R6
						, SUM(CASE WHEN A.GOL_DRH = 7 THEN 1 ELSE 0 END) R7
						, SUM(CASE WHEN A.GOL_DRH = 8 THEN 1 ELSE 0 END) R8
						, SUM(CASE WHEN A.GOL_DRH = 9 THEN 1 ELSE 0 END) R9
						, SUM(CASE WHEN A.GOL_DRH = 10 THEN 1 ELSE 0 END) R10
						, SUM(CASE WHEN A.GOL_DRH = 11 THEN 1 ELSE 0 END) R11
						, SUM(CASE WHEN A.GOL_DRH = 12 THEN 1 ELSE 0 END) R12
						, SUM(CASE WHEN A.GOL_DRH NOT BETWEEN 1 AND 12 OR GOL_DRH IS NULL THEN 1 ELSE 0 END) R13
						, COUNT(1) JUMLAH
						FROM ".$dkb_bio." A WHERE (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) AND A.NO_PROP = 32 AND A.NO_KAB =73";
						if($no_kel == 0){    
					$sql .=" AND A.NO_KEC =".$no_kec." ";
						}else{
					$sql .=" AND A.NO_KEC =".$no_kec." and A.NO_KEL =".$no_kel." ";
						}
				}
				$r = DB::select($sql);
               return $r;
		}

		public function get_jumlah_laporan_gol_drh_laki_laki($no_kec,$no_kel){
			$dkb_bio = Shr::do_get_dkb_bio();
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT 
						 SUM(CASE WHEN A.GOL_DRH  = 1 THEN 1 ELSE 0 END) R1
						, SUM(CASE WHEN A.GOL_DRH = 2 THEN 1 ELSE 0 END) R2
						, SUM(CASE WHEN A.GOL_DRH = 3 THEN 1 ELSE 0 END) R3
						, SUM(CASE WHEN A.GOL_DRH = 4 THEN 1 ELSE 0 END) R4
						, SUM(CASE WHEN A.GOL_DRH = 5 THEN 1 ELSE 0 END) R5
						, SUM(CASE WHEN A.GOL_DRH = 6 THEN 1 ELSE 0 END) R6
						, SUM(CASE WHEN A.GOL_DRH = 7 THEN 1 ELSE 0 END) R7
						, SUM(CASE WHEN A.GOL_DRH = 8 THEN 1 ELSE 0 END) R8
						, SUM(CASE WHEN A.GOL_DRH = 9 THEN 1 ELSE 0 END) R9
						, SUM(CASE WHEN A.GOL_DRH = 10 THEN 1 ELSE 0 END) R10
						, SUM(CASE WHEN A.GOL_DRH = 11 THEN 1 ELSE 0 END) R11
						, SUM(CASE WHEN A.GOL_DRH = 12 THEN 1 ELSE 0 END) R12
						, SUM(CASE WHEN A.GOL_DRH NOT BETWEEN 1 AND 12 OR GOL_DRH IS NULL THEN 1 ELSE 0 END) R13
						, COUNT(1) JUMLAH
						FROM ".$dkb_bio." A WHERE JENIS_KLMIN = 1 AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) AND A.NO_PROP = 32 AND A.NO_KAB =73";	
			}else{
				$sql .="SELECT 
						 SUM(CASE WHEN A.GOL_DRH  = 1 THEN 1 ELSE 0 END) R1
						, SUM(CASE WHEN A.GOL_DRH = 2 THEN 1 ELSE 0 END) R2
						, SUM(CASE WHEN A.GOL_DRH = 3 THEN 1 ELSE 0 END) R3
						, SUM(CASE WHEN A.GOL_DRH = 4 THEN 1 ELSE 0 END) R4
						, SUM(CASE WHEN A.GOL_DRH = 5 THEN 1 ELSE 0 END) R5
						, SUM(CASE WHEN A.GOL_DRH = 6 THEN 1 ELSE 0 END) R6
						, SUM(CASE WHEN A.GOL_DRH = 7 THEN 1 ELSE 0 END) R7
						, SUM(CASE WHEN A.GOL_DRH = 8 THEN 1 ELSE 0 END) R8
						, SUM(CASE WHEN A.GOL_DRH = 9 THEN 1 ELSE 0 END) R9
						, SUM(CASE WHEN A.GOL_DRH = 10 THEN 1 ELSE 0 END) R10
						, SUM(CASE WHEN A.GOL_DRH = 11 THEN 1 ELSE 0 END) R11
						, SUM(CASE WHEN A.GOL_DRH = 12 THEN 1 ELSE 0 END) R12
						, SUM(CASE WHEN A.GOL_DRH NOT BETWEEN 1 AND 12 OR GOL_DRH IS NULL THEN 1 ELSE 0 END) R13
						, COUNT(1) JUMLAH
						FROM ".$dkb_bio." A WHERE JENIS_KLMIN = 1 AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) AND A.NO_PROP = 32 AND A.NO_KAB =73";
						if($no_kel == 0){    
					$sql .=" AND A.NO_KEC =".$no_kec." ";
						}else{
					$sql .=" AND A.NO_KEC =".$no_kec." and A.NO_KEL =".$no_kel." ";
						}
				}
				$r = DB::select($sql);
               return $r;
		}
		public function get_jumlah_laporan_gol_drh_perempuan($no_kec,$no_kel){
			$dkb_bio = Shr::do_get_dkb_bio();
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT 
						 SUM(CASE WHEN A.GOL_DRH  = 1 THEN 1 ELSE 0 END) R1
						, SUM(CASE WHEN A.GOL_DRH = 2 THEN 1 ELSE 0 END) R2
						, SUM(CASE WHEN A.GOL_DRH = 3 THEN 1 ELSE 0 END) R3
						, SUM(CASE WHEN A.GOL_DRH = 4 THEN 1 ELSE 0 END) R4
						, SUM(CASE WHEN A.GOL_DRH = 5 THEN 1 ELSE 0 END) R5
						, SUM(CASE WHEN A.GOL_DRH = 6 THEN 1 ELSE 0 END) R6
						, SUM(CASE WHEN A.GOL_DRH = 7 THEN 1 ELSE 0 END) R7
						, SUM(CASE WHEN A.GOL_DRH = 8 THEN 1 ELSE 0 END) R8
						, SUM(CASE WHEN A.GOL_DRH = 9 THEN 1 ELSE 0 END) R9
						, SUM(CASE WHEN A.GOL_DRH = 10 THEN 1 ELSE 0 END) R10
						, SUM(CASE WHEN A.GOL_DRH = 11 THEN 1 ELSE 0 END) R11
						, SUM(CASE WHEN A.GOL_DRH = 12 THEN 1 ELSE 0 END) R12
						, SUM(CASE WHEN A.GOL_DRH NOT BETWEEN 1 AND 12 OR GOL_DRH IS NULL THEN 1 ELSE 0 END) R13
						, COUNT(1) JUMLAH
						FROM ".$dkb_bio." A WHERE JENIS_KLMIN = 2 AND  (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) AND A.NO_PROP = 32 AND A.NO_KAB =73";	
			}else{
				$sql .="SELECT 
						 SUM(CASE WHEN A.GOL_DRH  = 1 THEN 1 ELSE 0 END) R1
						, SUM(CASE WHEN A.GOL_DRH = 2 THEN 1 ELSE 0 END) R2
						, SUM(CASE WHEN A.GOL_DRH = 3 THEN 1 ELSE 0 END) R3
						, SUM(CASE WHEN A.GOL_DRH = 4 THEN 1 ELSE 0 END) R4
						, SUM(CASE WHEN A.GOL_DRH = 5 THEN 1 ELSE 0 END) R5
						, SUM(CASE WHEN A.GOL_DRH = 6 THEN 1 ELSE 0 END) R6
						, SUM(CASE WHEN A.GOL_DRH = 7 THEN 1 ELSE 0 END) R7
						, SUM(CASE WHEN A.GOL_DRH = 8 THEN 1 ELSE 0 END) R8
						, SUM(CASE WHEN A.GOL_DRH = 9 THEN 1 ELSE 0 END) R9
						, SUM(CASE WHEN A.GOL_DRH = 10 THEN 1 ELSE 0 END) R10
						, SUM(CASE WHEN A.GOL_DRH = 11 THEN 1 ELSE 0 END) R11
						, SUM(CASE WHEN A.GOL_DRH = 12 THEN 1 ELSE 0 END) R12
						, SUM(CASE WHEN A.GOL_DRH NOT BETWEEN 1 AND 12 OR GOL_DRH IS NULL THEN 1 ELSE 0 END) R13
						, COUNT(1) JUMLAH
						FROM ".$dkb_bio." A WHERE JENIS_KLMIN = 2 AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) AND A.NO_PROP = 32 AND A.NO_KAB =73";
						if($no_kel == 0){    
					$sql .=" AND A.NO_KEC =".$no_kec." ";
						}else{
					$sql .=" AND A.NO_KEC =".$no_kec." and A.NO_KEL =".$no_kel." ";
						}
				}
				$r = DB::select($sql);
               return $r;
		}
		public function get_jumlah_laporan_disabilitas($no_kec,$no_kel){
			$dkb_bio = Shr::do_get_dkb_bio();
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT 
						 SUM(CASE WHEN A.PNYDNG_CCT  = 1 THEN 1 ELSE 0 END) R1
						, SUM(CASE WHEN A.PNYDNG_CCT = 2 THEN 1 ELSE 0 END) R2
						, SUM(CASE WHEN A.PNYDNG_CCT = 3 THEN 1 ELSE 0 END) R3
						, SUM(CASE WHEN A.PNYDNG_CCT = 4 THEN 1 ELSE 0 END) R4
						, SUM(CASE WHEN A.PNYDNG_CCT = 5 THEN 1 ELSE 0 END) R5
						, SUM(CASE WHEN A.PNYDNG_CCT NOT BETWEEN 1 AND 5 OR PNYDNG_CCT IS NULL THEN 1 ELSE 0 END) R6
						, COUNT(1) JUMLAH
						FROM ".$dkb_bio." A WHERE  KLAIN_FSK = 2 AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) AND A.NO_PROP = 32 AND A.NO_KAB =73";	
			}else{
				$sql .="SELECT 
						 SUM(CASE WHEN A.PNYDNG_CCT  = 1 THEN 1 ELSE 0 END) R1
						, SUM(CASE WHEN A.PNYDNG_CCT = 2 THEN 1 ELSE 0 END) R2
						, SUM(CASE WHEN A.PNYDNG_CCT = 3 THEN 1 ELSE 0 END) R3
						, SUM(CASE WHEN A.PNYDNG_CCT = 4 THEN 1 ELSE 0 END) R4
						, SUM(CASE WHEN A.PNYDNG_CCT = 5 THEN 1 ELSE 0 END) R5
						, SUM(CASE WHEN A.PNYDNG_CCT NOT BETWEEN 1 AND 5 OR PNYDNG_CCT IS NULL THEN 1 ELSE 0 END) R6
						, COUNT(1) JUMLAH
						FROM ".$dkb_bio." A WHERE KLAIN_FSK = 2 AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) AND A.NO_PROP = 32 AND A.NO_KAB =73";
						if($no_kel == 0){    
					$sql .=" AND A.NO_KEC =".$no_kec." ";
						}else{
					$sql .=" AND A.NO_KEC =".$no_kec." and A.NO_KEL =".$no_kel." ";
						}
				}
				$r = DB::select($sql);
               return $r;
		}

		public function get_jumlah_laporan_disabilitas_laki_laki($no_kec,$no_kel){
			$dkb_bio = Shr::do_get_dkb_bio();
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT 
						 SUM(CASE WHEN A.PNYDNG_CCT  = 1 THEN 1 ELSE 0 END) R1
						, SUM(CASE WHEN A.PNYDNG_CCT = 2 THEN 1 ELSE 0 END) R2
						, SUM(CASE WHEN A.PNYDNG_CCT = 3 THEN 1 ELSE 0 END) R3
						, SUM(CASE WHEN A.PNYDNG_CCT = 4 THEN 1 ELSE 0 END) R4
						, SUM(CASE WHEN A.PNYDNG_CCT = 5 THEN 1 ELSE 0 END) R5
						, SUM(CASE WHEN A.PNYDNG_CCT NOT BETWEEN 1 AND 5 OR PNYDNG_CCT IS NULL THEN 1 ELSE 0 END) R6
						, COUNT(1) JUMLAH
						FROM ".$dkb_bio." A WHERE JENIS_KLMIN = 1 AND KLAIN_FSK = 2 AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) AND A.NO_PROP = 32 AND A.NO_KAB =73";	
			}else{
				$sql .="SELECT 
						 SUM(CASE WHEN A.PNYDNG_CCT  = 1 THEN 1 ELSE 0 END) R1
						, SUM(CASE WHEN A.PNYDNG_CCT = 2 THEN 1 ELSE 0 END) R2
						, SUM(CASE WHEN A.PNYDNG_CCT = 3 THEN 1 ELSE 0 END) R3
						, SUM(CASE WHEN A.PNYDNG_CCT = 4 THEN 1 ELSE 0 END) R4
						, SUM(CASE WHEN A.PNYDNG_CCT = 5 THEN 1 ELSE 0 END) R5
						, SUM(CASE WHEN A.PNYDNG_CCT NOT BETWEEN 1 AND 5 OR PNYDNG_CCT IS NULL THEN 1 ELSE 0 END) R6
						, COUNT(1) JUMLAH
						FROM ".$dkb_bio." A WHERE JENIS_KLMIN = 1 AND KLAIN_FSK = 2 AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) AND A.NO_PROP = 32 AND A.NO_KAB =73";
						if($no_kel == 0){    
					$sql .=" AND A.NO_KEC =".$no_kec." ";
						}else{
					$sql .=" AND A.NO_KEC =".$no_kec." and A.NO_KEL =".$no_kel." ";
						}
				}
				$r = DB::select($sql);
               return $r;
		}
		public function get_jumlah_laporan_disabilitas_perempuan($no_kec,$no_kel){
			$dkb_bio = Shr::do_get_dkb_bio();
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT 
						 SUM(CASE WHEN A.PNYDNG_CCT  = 1 THEN 1 ELSE 0 END) R1
						, SUM(CASE WHEN A.PNYDNG_CCT = 2 THEN 1 ELSE 0 END) R2
						, SUM(CASE WHEN A.PNYDNG_CCT = 3 THEN 1 ELSE 0 END) R3
						, SUM(CASE WHEN A.PNYDNG_CCT = 4 THEN 1 ELSE 0 END) R4
						, SUM(CASE WHEN A.PNYDNG_CCT = 5 THEN 1 ELSE 0 END) R5
						, SUM(CASE WHEN A.PNYDNG_CCT NOT BETWEEN 1 AND 5 OR PNYDNG_CCT IS NULL THEN 1 ELSE 0 END) R6
						, COUNT(1) JUMLAH
						FROM ".$dkb_bio." A WHERE JENIS_KLMIN = 2 AND  KLAIN_FSK = 2 AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) AND A.NO_PROP = 32 AND A.NO_KAB =73";	
			}else{
				$sql .="SELECT 
						 SUM(CASE WHEN A.PNYDNG_CCT  = 1 THEN 1 ELSE 0 END) R1
						, SUM(CASE WHEN A.PNYDNG_CCT = 2 THEN 1 ELSE 0 END) R2
						, SUM(CASE WHEN A.PNYDNG_CCT = 3 THEN 1 ELSE 0 END) R3
						, SUM(CASE WHEN A.PNYDNG_CCT = 4 THEN 1 ELSE 0 END) R4
						, SUM(CASE WHEN A.PNYDNG_CCT = 5 THEN 1 ELSE 0 END) R5
						, SUM(CASE WHEN A.PNYDNG_CCT NOT BETWEEN 1 AND 5 OR PNYDNG_CCT IS NULL THEN 1 ELSE 0 END) R6
						, COUNT(1) JUMLAH
						FROM ".$dkb_bio." A WHERE JENIS_KLMIN = 2 AND KLAIN_FSK = 2 AND (A.FLAG_STATUS = '0' OR (A.FLAG_STATUS = '2' AND A.FLAG_PINDAH IN(1,2,3,4,5))) AND A.NO_PROP = 32 AND A.NO_KAB =73";
						if($no_kel == 0){    
					$sql .=" AND A.NO_KEC =".$no_kec." ";
						}else{
					$sql .=" AND A.NO_KEC =".$no_kec." and A.NO_KEL =".$no_kel." ";
						}
				}
				$r = DB::select($sql);
               return $r;
		}
		public function get_jumlah_laporan_umur($no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT
						 	SUM(CASE WHEN UMUR >= 0 AND UMUR <= 4 THEN 1 ELSE 0 END) R1
                          , SUM(CASE WHEN UMUR >= 5 AND UMUR <= 9 THEN 1 ELSE 0 END) R2
                          , SUM(CASE WHEN UMUR >= 10 AND UMUR <= 14 THEN 1 ELSE 0 END) R3
                          , SUM(CASE WHEN UMUR >= 15 AND UMUR <= 19 THEN 1 ELSE 0 END) R4
                          , SUM(CASE WHEN UMUR >= 20 AND UMUR <= 24 THEN 1 ELSE 0 END) R5
                          , SUM(CASE WHEN UMUR >= 25 AND UMUR <= 29 THEN 1 ELSE 0 END) R6
                          , SUM(CASE WHEN UMUR >= 30 AND UMUR <= 34 THEN 1 ELSE 0 END) R7
                          , SUM(CASE WHEN UMUR >= 35 AND UMUR <= 39 THEN 1 ELSE 0 END) R8
                          , SUM(CASE WHEN UMUR >= 40 AND UMUR <= 44 THEN 1 ELSE 0 END) R9
                          , SUM(CASE WHEN UMUR >= 45 AND UMUR <= 49 THEN 1 ELSE 0 END) R10
                          , SUM(CASE WHEN UMUR >= 50 AND UMUR <= 54 THEN 1 ELSE 0 END) R11
                          , SUM(CASE WHEN UMUR >= 55 AND UMUR <= 59 THEN 1 ELSE 0 END) R12
                          , SUM(CASE WHEN UMUR >= 60 AND UMUR <= 64 THEN 1 ELSE 0 END) R13
                          , SUM(CASE WHEN UMUR >= 65 AND UMUR <= 69 THEN 1 ELSE 0 END) R14
                          , SUM(CASE WHEN UMUR >= 70 AND UMUR <= 74 THEN 1 ELSE 0 END) R15
                          , SUM(CASE WHEN UMUR >= 75  THEN 1 ELSE 0 END) R16
						  , COUNT(1) JUMLAH
					FROM KAMPUNG_GISA_DKB WHERE  1=1 AND NO_PROP = 32 AND NO_KAB =73";	
			}else{
				$sql .="SELECT
							SUM(CASE WHEN UMUR >= 0 AND UMUR <= 4 THEN 1 ELSE 0 END) R1
                          , SUM(CASE WHEN UMUR >= 5 AND UMUR <= 9 THEN 1 ELSE 0 END) R2
                          , SUM(CASE WHEN UMUR >= 10 AND UMUR <= 14 THEN 1 ELSE 0 END) R3
                          , SUM(CASE WHEN UMUR >= 15 AND UMUR <= 19 THEN 1 ELSE 0 END) R4
                          , SUM(CASE WHEN UMUR >= 20 AND UMUR <= 24 THEN 1 ELSE 0 END) R5
                          , SUM(CASE WHEN UMUR >= 25 AND UMUR <= 29 THEN 1 ELSE 0 END) R6
                          , SUM(CASE WHEN UMUR >= 30 AND UMUR <= 34 THEN 1 ELSE 0 END) R7
                          , SUM(CASE WHEN UMUR >= 35 AND UMUR <= 39 THEN 1 ELSE 0 END) R8
                          , SUM(CASE WHEN UMUR >= 40 AND UMUR <= 44 THEN 1 ELSE 0 END) R9
                          , SUM(CASE WHEN UMUR >= 45 AND UMUR <= 49 THEN 1 ELSE 0 END) R10
                          , SUM(CASE WHEN UMUR >= 50 AND UMUR <= 54 THEN 1 ELSE 0 END) R11
                          , SUM(CASE WHEN UMUR >= 55 AND UMUR <= 59 THEN 1 ELSE 0 END) R12
                          , SUM(CASE WHEN UMUR >= 60 AND UMUR <= 64 THEN 1 ELSE 0 END) R13
                          , SUM(CASE WHEN UMUR >= 65 AND UMUR <= 69 THEN 1 ELSE 0 END) R14
                          , SUM(CASE WHEN UMUR >= 70 AND UMUR <= 74 THEN 1 ELSE 0 END) R15
                          , SUM(CASE WHEN UMUR >= 75  THEN 1 ELSE 0 END) R16
						  , COUNT(1) JUMLAH
					FROM KAMPUNG_GISA_DKB WHERE  1=1 AND NO_PROP = 32 AND NO_KAB =73";
					if($no_kel == 0){    
					$sql .=" AND NO_KEC =".$no_kec." ";
						}else{
					$sql .=" AND NO_KEC =".$no_kec." and NO_KEL =".$no_kel." ";
						}
				}
				$r = DB::select($sql);
               return $r;
		}

		public function get_jumlah_laporan_umur_laki_laki($no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT 
							SUM(CASE WHEN UMUR >= 0 AND UMUR <= 4 THEN 1 ELSE 0 END) R1
                          , SUM(CASE WHEN UMUR >= 5 AND UMUR <= 9 THEN 1 ELSE 0 END) R2
                          , SUM(CASE WHEN UMUR >= 10 AND UMUR <= 14 THEN 1 ELSE 0 END) R3
                          , SUM(CASE WHEN UMUR >= 15 AND UMUR <= 19 THEN 1 ELSE 0 END) R4
                          , SUM(CASE WHEN UMUR >= 20 AND UMUR <= 24 THEN 1 ELSE 0 END) R5
                          , SUM(CASE WHEN UMUR >= 25 AND UMUR <= 29 THEN 1 ELSE 0 END) R6
                          , SUM(CASE WHEN UMUR >= 30 AND UMUR <= 34 THEN 1 ELSE 0 END) R7
                          , SUM(CASE WHEN UMUR >= 35 AND UMUR <= 39 THEN 1 ELSE 0 END) R8
                          , SUM(CASE WHEN UMUR >= 40 AND UMUR <= 44 THEN 1 ELSE 0 END) R9
                          , SUM(CASE WHEN UMUR >= 45 AND UMUR <= 49 THEN 1 ELSE 0 END) R10
                          , SUM(CASE WHEN UMUR >= 50 AND UMUR <= 54 THEN 1 ELSE 0 END) R11
                          , SUM(CASE WHEN UMUR >= 55 AND UMUR <= 59 THEN 1 ELSE 0 END) R12
                          , SUM(CASE WHEN UMUR >= 60 AND UMUR <= 64 THEN 1 ELSE 0 END) R13
                          , SUM(CASE WHEN UMUR >= 65 AND UMUR <= 69 THEN 1 ELSE 0 END) R14
                          , SUM(CASE WHEN UMUR >= 70 AND UMUR <= 74 THEN 1 ELSE 0 END) R15
                          , SUM(CASE WHEN UMUR >= 75  THEN 1 ELSE 0 END) R16
						  , COUNT(1) JUMLAH
					FROM KAMPUNG_GISA_DKB WHERE JENIS_KELAMIN = 1 AND 1=1 AND NO_PROP = 32 AND NO_KAB =73";	
			}else{
				$sql .="SELECT 
							SUM(CASE WHEN UMUR >= 0 AND UMUR <= 4 THEN 1 ELSE 0 END) R1
                          , SUM(CASE WHEN UMUR >= 5 AND UMUR <= 9 THEN 1 ELSE 0 END) R2
                          , SUM(CASE WHEN UMUR >= 10 AND UMUR <= 14 THEN 1 ELSE 0 END) R3
                          , SUM(CASE WHEN UMUR >= 15 AND UMUR <= 19 THEN 1 ELSE 0 END) R4
                          , SUM(CASE WHEN UMUR >= 20 AND UMUR <= 24 THEN 1 ELSE 0 END) R5
                          , SUM(CASE WHEN UMUR >= 25 AND UMUR <= 29 THEN 1 ELSE 0 END) R6
                          , SUM(CASE WHEN UMUR >= 30 AND UMUR <= 34 THEN 1 ELSE 0 END) R7
                          , SUM(CASE WHEN UMUR >= 35 AND UMUR <= 39 THEN 1 ELSE 0 END) R8
                          , SUM(CASE WHEN UMUR >= 40 AND UMUR <= 44 THEN 1 ELSE 0 END) R9
                          , SUM(CASE WHEN UMUR >= 45 AND UMUR <= 49 THEN 1 ELSE 0 END) R10
                          , SUM(CASE WHEN UMUR >= 50 AND UMUR <= 54 THEN 1 ELSE 0 END) R11
                          , SUM(CASE WHEN UMUR >= 55 AND UMUR <= 59 THEN 1 ELSE 0 END) R12
                          , SUM(CASE WHEN UMUR >= 60 AND UMUR <= 64 THEN 1 ELSE 0 END) R13
                          , SUM(CASE WHEN UMUR >= 65 AND UMUR <= 69 THEN 1 ELSE 0 END) R14
                          , SUM(CASE WHEN UMUR >= 70 AND UMUR <= 74 THEN 1 ELSE 0 END) R15
                          , SUM(CASE WHEN UMUR >= 75  THEN 1 ELSE 0 END) R16
						  , COUNT(1) JUMLAH
					FROM KAMPUNG_GISA_DKB WHERE JENIS_KELAMIN = 1 AND 1=1 AND NO_PROP = 32 AND NO_KAB =73";
					if($no_kel == 0){    
					$sql .=" AND NO_KEC =".$no_kec." ";
						}else{
					$sql .=" AND NO_KEC =".$no_kec." and NO_KEL =".$no_kel." ";
						}
				}
				$r = DB::select($sql);
               return $r;
		}

		public function get_jumlah_laporan_umur_perempuan($no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT
							SUM(CASE WHEN UMUR >= 0 AND UMUR <= 4 THEN 1 ELSE 0 END) R1
                          , SUM(CASE WHEN UMUR >= 5 AND UMUR <= 9 THEN 1 ELSE 0 END) R2
                          , SUM(CASE WHEN UMUR >= 10 AND UMUR <= 14 THEN 1 ELSE 0 END) R3
                          , SUM(CASE WHEN UMUR >= 15 AND UMUR <= 19 THEN 1 ELSE 0 END) R4
                          , SUM(CASE WHEN UMUR >= 20 AND UMUR <= 24 THEN 1 ELSE 0 END) R5
                          , SUM(CASE WHEN UMUR >= 25 AND UMUR <= 29 THEN 1 ELSE 0 END) R6
                          , SUM(CASE WHEN UMUR >= 30 AND UMUR <= 34 THEN 1 ELSE 0 END) R7
                          , SUM(CASE WHEN UMUR >= 35 AND UMUR <= 39 THEN 1 ELSE 0 END) R8
                          , SUM(CASE WHEN UMUR >= 40 AND UMUR <= 44 THEN 1 ELSE 0 END) R9
                          , SUM(CASE WHEN UMUR >= 45 AND UMUR <= 49 THEN 1 ELSE 0 END) R10
                          , SUM(CASE WHEN UMUR >= 50 AND UMUR <= 54 THEN 1 ELSE 0 END) R11
                          , SUM(CASE WHEN UMUR >= 55 AND UMUR <= 59 THEN 1 ELSE 0 END) R12
                          , SUM(CASE WHEN UMUR >= 60 AND UMUR <= 64 THEN 1 ELSE 0 END) R13
                          , SUM(CASE WHEN UMUR >= 65 AND UMUR <= 69 THEN 1 ELSE 0 END) R14
                          , SUM(CASE WHEN UMUR >= 70 AND UMUR <= 74 THEN 1 ELSE 0 END) R15
                          , SUM(CASE WHEN UMUR >= 75  THEN 1 ELSE 0 END) R16
						  , COUNT(1) JUMLAH
					FROM KAMPUNG_GISA_DKB WHERE JENIS_KELAMIN = 2 AND 1=1 AND NO_PROP = 32 AND NO_KAB =73";	
			}else{
				$sql .="SELECT
							SUM(CASE WHEN UMUR >= 0 AND UMUR <= 4 THEN 1 ELSE 0 END) R1
                          , SUM(CASE WHEN UMUR >= 5 AND UMUR <= 9 THEN 1 ELSE 0 END) R2
                          , SUM(CASE WHEN UMUR >= 10 AND UMUR <= 14 THEN 1 ELSE 0 END) R3
                          , SUM(CASE WHEN UMUR >= 15 AND UMUR <= 19 THEN 1 ELSE 0 END) R4
                          , SUM(CASE WHEN UMUR >= 20 AND UMUR <= 24 THEN 1 ELSE 0 END) R5
                          , SUM(CASE WHEN UMUR >= 25 AND UMUR <= 29 THEN 1 ELSE 0 END) R6
                          , SUM(CASE WHEN UMUR >= 30 AND UMUR <= 34 THEN 1 ELSE 0 END) R7
                          , SUM(CASE WHEN UMUR >= 35 AND UMUR <= 39 THEN 1 ELSE 0 END) R8
                          , SUM(CASE WHEN UMUR >= 40 AND UMUR <= 44 THEN 1 ELSE 0 END) R9
                          , SUM(CASE WHEN UMUR >= 45 AND UMUR <= 49 THEN 1 ELSE 0 END) R10
                          , SUM(CASE WHEN UMUR >= 50 AND UMUR <= 54 THEN 1 ELSE 0 END) R11
                          , SUM(CASE WHEN UMUR >= 55 AND UMUR <= 59 THEN 1 ELSE 0 END) R12
                          , SUM(CASE WHEN UMUR >= 60 AND UMUR <= 64 THEN 1 ELSE 0 END) R13
                          , SUM(CASE WHEN UMUR >= 65 AND UMUR <= 69 THEN 1 ELSE 0 END) R14
                          , SUM(CASE WHEN UMUR >= 70 AND UMUR <= 74 THEN 1 ELSE 0 END) R15
                          , SUM(CASE WHEN UMUR >= 75  THEN 1 ELSE 0 END) R16
						  , COUNT(1) JUMLAH
					FROM KAMPUNG_GISA_DKB WHERE JENIS_KELAMIN = 2 AND 1=1 AND NO_PROP = 32 AND NO_KAB =73";
					if($no_kel == 0){    
					$sql .=" AND NO_KEC =".$no_kec." ";
						}else{
					$sql .=" AND NO_KEC =".$no_kec." and NO_KEL =".$no_kel." ";
						}
				}
				$r = DB::select($sql);
               return $r;
		}
		
		public function get_jumlah_laporan_umur_lansia($no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT
					SUM(CASE WHEN UMUR >= 60 AND UMUR <= 64 THEN 1 ELSE 0 END) R1
                          , SUM(CASE WHEN UMUR >= 65 AND UMUR <= 69 THEN 1 ELSE 0 END) R2
                          , SUM(CASE WHEN UMUR >= 70 AND UMUR <= 74 THEN 1 ELSE 0 END) R3
                          , SUM(CASE WHEN UMUR >= 75 AND UMUR <= 79 THEN 1 ELSE 0 END) R4
                          , SUM(CASE WHEN UMUR >= 80 AND UMUR <= 84 THEN 1 ELSE 0 END) R5
                          , SUM(CASE WHEN UMUR >= 85 AND UMUR <= 89 THEN 1 ELSE 0 END) R6
                          , SUM(CASE WHEN UMUR >= 90 AND UMUR <= 94 THEN 1 ELSE 0 END) R7
                          , SUM(CASE WHEN UMUR >= 95 AND UMUR <= 99 THEN 1 ELSE 0 END) R8
                          , SUM(CASE WHEN UMUR >= 100  THEN 1 ELSE 0 END) R9
						  , COUNT(1) JUMLAH
					FROM KAMPUNG_GISA_DKB WHERE  1=1 AND NO_PROP = 32 AND NO_KAB =73";	
			}else{
				$sql .="SELECT
					SUM(CASE WHEN UMUR >= 60 AND UMUR <= 64 THEN 1 ELSE 0 END) R1
                          , SUM(CASE WHEN UMUR >= 65 AND UMUR <= 69 THEN 1 ELSE 0 END) R2
                          , SUM(CASE WHEN UMUR >= 70 AND UMUR <= 74 THEN 1 ELSE 0 END) R3
                          , SUM(CASE WHEN UMUR >= 75 AND UMUR <= 79 THEN 1 ELSE 0 END) R4
                          , SUM(CASE WHEN UMUR >= 80 AND UMUR <= 84 THEN 1 ELSE 0 END) R5
                          , SUM(CASE WHEN UMUR >= 85 AND UMUR <= 89 THEN 1 ELSE 0 END) R6
                          , SUM(CASE WHEN UMUR >= 90 AND UMUR <= 94 THEN 1 ELSE 0 END) R7
                          , SUM(CASE WHEN UMUR >= 95 AND UMUR <= 99 THEN 1 ELSE 0 END) R8
                          , SUM(CASE WHEN UMUR >= 100  THEN 1 ELSE 0 END) R9
						  , COUNT(1) JUMLAH
					FROM KAMPUNG_GISA_DKB WHERE  1=1 AND NO_PROP = 32 AND NO_KAB =73";
					if($no_kel == 0){    
					$sql .=" AND NO_KEC =".$no_kec." ";
						}else{
					$sql .=" AND NO_KEC =".$no_kec." and NO_KEL =".$no_kel." ";
						}
				}
				$r = DB::select($sql);
               return $r;
		}
		public function get_jumlah_laporan_umur_lansia_laki_laki($no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT 
					SUM(CASE WHEN UMUR >= 60 AND UMUR <= 64 THEN 1 ELSE 0 END) R1
                          , SUM(CASE WHEN UMUR >= 65 AND UMUR <= 69 THEN 1 ELSE 0 END) R2
                          , SUM(CASE WHEN UMUR >= 70 AND UMUR <= 74 THEN 1 ELSE 0 END) R3
                          , SUM(CASE WHEN UMUR >= 75 AND UMUR <= 79 THEN 1 ELSE 0 END) R4
                          , SUM(CASE WHEN UMUR >= 80 AND UMUR <= 84 THEN 1 ELSE 0 END) R5
                          , SUM(CASE WHEN UMUR >= 85 AND UMUR <= 89 THEN 1 ELSE 0 END) R6
                          , SUM(CASE WHEN UMUR >= 90 AND UMUR <= 94 THEN 1 ELSE 0 END) R7
                          , SUM(CASE WHEN UMUR >= 95 AND UMUR <= 99 THEN 1 ELSE 0 END) R8
                          , SUM(CASE WHEN UMUR >= 100  THEN 1 ELSE 0 END) R9
						  , COUNT(1) JUMLAH
					FROM KAMPUNG_GISA_DKB WHERE JENIS_KELAMIN = 1 AND 1=1 AND NO_PROP = 32 AND NO_KAB =73";	
			}else{
				$sql .="SELECT 
					SUM(CASE WHEN UMUR >= 60 AND UMUR <= 64 THEN 1 ELSE 0 END) R1
                          , SUM(CASE WHEN UMUR >= 65 AND UMUR <= 69 THEN 1 ELSE 0 END) R2
                          , SUM(CASE WHEN UMUR >= 70 AND UMUR <= 74 THEN 1 ELSE 0 END) R3
                          , SUM(CASE WHEN UMUR >= 75 AND UMUR <= 79 THEN 1 ELSE 0 END) R4
                          , SUM(CASE WHEN UMUR >= 80 AND UMUR <= 84 THEN 1 ELSE 0 END) R5
                          , SUM(CASE WHEN UMUR >= 85 AND UMUR <= 89 THEN 1 ELSE 0 END) R6
                          , SUM(CASE WHEN UMUR >= 90 AND UMUR <= 94 THEN 1 ELSE 0 END) R7
                          , SUM(CASE WHEN UMUR >= 95 AND UMUR <= 99 THEN 1 ELSE 0 END) R8
                          , SUM(CASE WHEN UMUR >= 100  THEN 1 ELSE 0 END) R9
						  , COUNT(1) JUMLAH
					FROM KAMPUNG_GISA_DKB WHERE JENIS_KELAMIN = 1 AND 1=1 AND NO_PROP = 32 AND NO_KAB =73";
					if($no_kel == 0){    
					$sql .=" AND NO_KEC =".$no_kec." ";
						}else{
					$sql .=" AND NO_KEC =".$no_kec." and NO_KEL =".$no_kel." ";
						}
				}
				$r = DB::select($sql);
               return $r;
		}

		public function get_jumlah_laporan_umur_lansia_perempuan($no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT
					SUM(CASE WHEN UMUR >= 60 AND UMUR <= 64 THEN 1 ELSE 0 END) R1
                          , SUM(CASE WHEN UMUR >= 65 AND UMUR <= 69 THEN 1 ELSE 0 END) R2
                          , SUM(CASE WHEN UMUR >= 70 AND UMUR <= 74 THEN 1 ELSE 0 END) R3
                          , SUM(CASE WHEN UMUR >= 75 AND UMUR <= 79 THEN 1 ELSE 0 END) R4
                          , SUM(CASE WHEN UMUR >= 80 AND UMUR <= 84 THEN 1 ELSE 0 END) R5
                          , SUM(CASE WHEN UMUR >= 85 AND UMUR <= 89 THEN 1 ELSE 0 END) R6
                          , SUM(CASE WHEN UMUR >= 90 AND UMUR <= 94 THEN 1 ELSE 0 END) R7
                          , SUM(CASE WHEN UMUR >= 95 AND UMUR <= 99 THEN 1 ELSE 0 END) R8
                          , SUM(CASE WHEN UMUR >= 100  THEN 1 ELSE 0 END) R9
						  , COUNT(1) JUMLAH
					FROM KAMPUNG_GISA_DKB WHERE JENIS_KELAMIN = 2 AND 1=1 AND NO_PROP = 32 AND NO_KAB =73";	
			}else{
				$sql .="SELECT
					SUM(CASE WHEN UMUR >= 60 AND UMUR <= 64 THEN 1 ELSE 0 END) R1
                          , SUM(CASE WHEN UMUR >= 65 AND UMUR <= 69 THEN 1 ELSE 0 END) R2
                          , SUM(CASE WHEN UMUR >= 70 AND UMUR <= 74 THEN 1 ELSE 0 END) R3
                          , SUM(CASE WHEN UMUR >= 75 AND UMUR <= 79 THEN 1 ELSE 0 END) R4
                          , SUM(CASE WHEN UMUR >= 80 AND UMUR <= 84 THEN 1 ELSE 0 END) R5
                          , SUM(CASE WHEN UMUR >= 85 AND UMUR <= 89 THEN 1 ELSE 0 END) R6
                          , SUM(CASE WHEN UMUR >= 90 AND UMUR <= 94 THEN 1 ELSE 0 END) R7
                          , SUM(CASE WHEN UMUR >= 95 AND UMUR <= 99 THEN 1 ELSE 0 END) R8
                          , SUM(CASE WHEN UMUR >= 100  THEN 1 ELSE 0 END) R9
						  , COUNT(1) JUMLAH
					FROM KAMPUNG_GISA_DKB WHERE JENIS_KELAMIN = 2 AND 1=1 AND NO_PROP = 32 AND NO_KAB =73";
					if($no_kel == 0){    
					$sql .=" AND NO_KEC =".$no_kec." ";
						}else{
					$sql .=" AND NO_KEC =".$no_kec." and NO_KEL =".$no_kel." ";
						}
				}
				$r = DB::select($sql);
               return $r;
		}
		public function get_data_surat_datang_ln($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL, '$tgl' AS TANGGAL, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH, CASE WHEN B.JUMLAH_ANGGOTA IS NULL THEN 0 ELSE B.JUMLAH_ANGGOTA END AS JUMLAH_ANGGOTA
					FROM SETUP_KEC A LEFT JOIN
					(SELECT 
					    C.NO_KEC
					    , C.NAMA_KEC
					    , COUNT(DISTINCT(A.NO_SKDLN)) AS JUMLAH
             			, COUNT(A.NO_SKDLN) AS JUMLAH_ANGGOTA
              			FROM SKDLN_HEADER A INNER JOIN SKDLN_DETAIL B ON A.NO_SKDLN = B.NO_SKDLN INNER JOIN SETUP_KEC C
					    ON A.NO_PROP = C.NO_PROP 
					    AND A.NO_KAB = C.NO_KAB 
					    AND A.NO_KEC = C.NO_KEC 
              			WHERE 1=1 
        				AND A.CREATED_DATE >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND A.CREATED_DATE < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1
					    AND
					    C.NO_PROP = 32 AND C.NO_KAB = 73
					    GROUP BY C.NO_KEC, C.NAMA_KEC
					    ORDER BY C.NO_KEC, C.NAMA_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL, '$tgl' AS TANGGAL, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH, CASE WHEN B.JUMLAH_ANGGOTA IS NULL THEN 0 ELSE B.JUMLAH_ANGGOTA END AS JUMLAH_ANGGOTA
					FROM SETUP_KEL A LEFT JOIN
					(SELECT 
					    C.NO_KEL
					    , C.NAMA_KEL
					    , COUNT(DISTINCT(A.NO_SKDLN)) AS JUMLAH
             			, COUNT(A.NO_SKDLN) AS JUMLAH_ANGGOTA
              			FROM SKDLN_HEADER A INNER JOIN SKDLN_DETAIL B ON A.NO_SKDLN = B.NO_SKDLN INNER JOIN SETUP_KEL C
					    ON A.NO_PROP = C.NO_PROP 
					    AND A.NO_KAB = C.NO_KAB 
					    AND A.NO_KEC = C.NO_KEC 
					    AND A.NO_KEL = C.NO_KEL 
              			WHERE 1=1 
        				AND A.CREATED_DATE >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND A.CREATED_DATE < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1";
						if($no_kel == 0){    
					$sql .=" AND C.NO_PROP = 32 AND C.NO_KAB = 73 AND C.NO_KEC =".$no_kec."
						    GROUP BY C.NO_KEL, C.NAMA_KEL
						    ORDER BY C.NO_KEL, C.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND C.NO_PROP = 32 AND C.NO_KAB = 73 AND C.NO_KEC =".$no_kec." AND C.NO_KEL =".$no_kel."
						    GROUP BY C.NO_KEL, C.NAMA_KEL
						    ORDER BY C.NO_KEL, C.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::connection('db222')->select($sql);
			return $r;


		}

		public function get_data_surat_pindah_ln($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL, '$tgl' AS TANGGAL, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH, CASE WHEN B.JUMLAH_ANGGOTA IS NULL THEN 0 ELSE B.JUMLAH_ANGGOTA END AS JUMLAH_ANGGOTA
					FROM SETUP_KEC A LEFT JOIN
					(SELECT 
					    C.NO_KEC
					    , C.NAMA_KEC
					    , COUNT(DISTINCT(A.NO_SKPLN)) AS JUMLAH
             			, COUNT(A.NO_SKPLN) AS JUMLAH_ANGGOTA
              			FROM SKPLN_HEADER A INNER JOIN SKPLN_DETAIL B ON A.NO_SKPLN = B.NO_SKPLN INNER JOIN SETUP_KEC C
					    ON A.NO_PROP = C.NO_PROP 
					    AND A.NO_KAB = C.NO_KAB 
					    AND A.NO_KEC = C.NO_KEC 
              			WHERE 1=1 
        				AND A.CREATED_DATE >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND A.CREATED_DATE < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1
					    AND
					    C.NO_PROP = 32 AND C.NO_KAB = 73
					    GROUP BY C.NO_KEC, C.NAMA_KEC
					    ORDER BY C.NO_KEC, C.NAMA_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL, '$tgl' AS TANGGAL, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH, CASE WHEN B.JUMLAH_ANGGOTA IS NULL THEN 0 ELSE B.JUMLAH_ANGGOTA END AS JUMLAH_ANGGOTA
					FROM SETUP_KEL A LEFT JOIN
					(SELECT 
					    C.NO_KEL
					    , C.NAMA_KEL
					    , COUNT(DISTINCT(A.NO_SKPLN)) AS JUMLAH
             			, COUNT(A.NO_SKPLN) AS JUMLAH_ANGGOTA
              			FROM SKPLN_HEADER A INNER JOIN SKPLN_DETAIL B ON A.NO_SKPLN = B.NO_SKPLN INNER JOIN SETUP_KEL C
					    ON A.NO_PROP = C.NO_PROP 
					    AND A.NO_KAB = C.NO_KAB 
					    AND A.NO_KEC = C.NO_KEC 
					    AND A.NO_KEL = C.NO_KEL 
              			WHERE 1=1 
        				AND A.CREATED_DATE >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND A.CREATED_DATE < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1";
						if($no_kel == 0){    
					$sql .=" AND C.NO_PROP = 32 AND C.NO_KAB = 73 AND C.NO_KEC =".$no_kec."
						    GROUP BY C.NO_KEL, C.NAMA_KEL
						    ORDER BY C.NO_KEL, C.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND C.NO_PROP = 32 AND C.NO_KAB = 73 AND C.NO_KEC =".$no_kec." AND C.NO_KEL =".$no_kel."
						    GROUP BY C.NO_KEL, C.NAMA_KEL
						    ORDER BY C.NO_KEL, C.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::connection('db222')->select($sql);
			return $r;


		}

		public function mst_get_laporan_sisa_blangko($tgl_start,$tgl_end){
			$sql = "";
			$sql .="SELECT ID, CREATED_DT, TO_CHAR(CREATED_DT,'DD-MM-YYYY') TGL, VAL FROM SIAK_DASHBOARD A WHERE NOT EXISTS (SELECT 1 FROM SIAK_DASHBOARD_LOG B WHERE A.ID=B.ID AND TRUNC(A.CREATED_DT) = TRUNC(B.CREATED_DT)) 
			AND ID = 'GET_BLANGKO_OUT'
			AND A.CREATED_DT >= TO_DATE('$tgl_start','DD-MM-YYYY') AND A.CREATED_DT < TO_DATE('$tgl_end','DD-MM-YYYY')+1 
			UNION ALL
			SELECT ID, CREATED_DT, TO_CHAR(CREATED_DT,'DD-MM-YYYY') TGL, VAL FROM SIAK_DASHBOARD_LOG A WHERE NOT EXISTS (SELECT 1 FROM SIAK_DASHBOARD B WHERE A.ID=B.ID AND TRUNC(A.CREATED_DT) = TRUNC(B.CREATED_DT)) 
			AND ID = 'GET_BLANGKO_OUT' 
			AND A.CREATED_DT >= TO_DATE('$tgl_start','DD-MM-YYYY') AND A.CREATED_DT < TO_DATE('$tgl_end','DD-MM-YYYY')+1 
			ORDER BY CREATED_DT DESC";	
			$r = DB::select($sql);
			return $r;
		}

		public function mst_get_laporan_blangko_masuk($tgl_start,$tgl_end){
			$sql = "";
			$sql .="SELECT TANGGAL_MASUK, JUMLAH, CREATED_BY, CASE WHEN KET IS NULL THEN '-' ELSE KET END KET FROM SIAK_BLANGKO WHERE   TANGGAL_MASUK >= TO_DATE('$tgl_start','DD-MM-YYYY') AND TANGGAL_MASUK < TO_DATE('$tgl_end','DD-MM-YYYY')+1 ORDER BY TANGGAL_MASUK DESC";	
			$r = DB::select($sql);
			return $r;
		}

		public function mst_get_laporan_tte_kk($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
			$sql .="SELECT B.NO_KEC NO_WIL
        			, F5_GET_NAMA_KECAMATAN(B.NO_PROP, B.NO_KAB, B.NO_KEC) NAMA_WIL, '$tgl' TGL
                    ,SUM(CASE WHEN TRUNC(REQ_DATE) >= TRUNC(TO_DATE('$tgl_start','DD-MM-YYYY')) AND TRUNC(REQ_DATE) <= TRUNC(TO_DATE('$tgl_end','DD-MM-YYYY')) THEN 1 ELSE 0 END) REQUEST_N
                    ,SUM(CASE WHEN TRUNC(REQ_DATE) >= TRUNC(TO_DATE('$tgl_start','DD-MM-YYYY')) AND TRUNC(REQ_DATE) <= TRUNC(TO_DATE('$tgl_end','DD-MM-YYYY')) AND PEJABAT_PROCESS_DATE IS NOT NULL THEN 1 ELSE 0 END) APROVED_N
                    ,SUM(CASE WHEN TRUNC(REQ_DATE) >= TRUNC(TO_DATE('$tgl_start','DD-MM-YYYY')) AND TRUNC(REQ_DATE) <= TRUNC(TO_DATE('$tgl_end','DD-MM-YYYY')) AND PEJABAT_PROCESS_DATE IS NULL THEN 1 ELSE 0 END) THE_REST_N
                    ,SUM(CASE WHEN TRUNC(REQ_DATE) >= TRUNC(TO_DATE('$tgl_start','DD-MM-YYYY')) AND TRUNC(REQ_DATE) <= TRUNC(TO_DATE('$tgl_end','DD-MM-YYYY')) AND B.CERT_STATUS = 2  THEN 1 ELSE 0 END) APROVED_ORANGE
                    ,SUM(CASE WHEN TRUNC(REQ_DATE) >= TRUNC(TO_DATE('$tgl_start','DD-MM-YYYY')) AND TRUNC(REQ_DATE) <= TRUNC(TO_DATE('$tgl_end','DD-MM-YYYY')) AND B.CERT_STATUS = 3  THEN 1 ELSE 0 END) APROVED_BLUE
                    ,SUM(CASE WHEN TRUNC(REQ_DATE) >= TRUNC(TO_DATE('$tgl_start','DD-MM-YYYY')) AND TRUNC(REQ_DATE) <= TRUNC(TO_DATE('$tgl_end','DD-MM-YYYY')) AND B.CERT_STATUS = 9  THEN 1 ELSE 0 END) APROVED_RED
                    ,SUM(CASE WHEN TRUNC(REQ_DATE) >= TRUNC(TO_DATE('$tgl_start','DD-MM-YYYY')) AND TRUNC(REQ_DATE) <= TRUNC(TO_DATE('$tgl_end','DD-MM-YYYY')) AND B.CERT_STATUS NOT IN (1,2,3,9)  THEN 1 ELSE 0 END) APROVED_GREEN
                    ,COUNT(1) REQUEST_A
                    ,SUM(CASE WHEN PEJABAT_PROCESS_DATE IS NOT NULL THEN 1 ELSE 0 END) APROVED_A
                    ,SUM(CASE WHEN PEJABAT_PROCESS_DATE IS NULL THEN 1 ELSE 0 END) THE_REST_A
                    ,SUM(CASE WHEN B.CERT_STATUS = 2  THEN 1 ELSE 0 END) A_ORANGE
                    ,SUM(CASE WHEN B.CERT_STATUS = 3  THEN 1 ELSE 0 END) A_BLUE
                    ,SUM(CASE WHEN B.CERT_STATUS = 3  THEN 1 ELSE 0 END) A_RED
                    ,SUM(CASE WHEN B.CERT_STATUS NOT IN (1,2,3,9)  THEN 1 ELSE 0 END) A_GREEN
                     FROM  (SELECT  NO_DOC,NO_PROP, NO_KAB, NO_KEC, NO_KEL, PEJABAT_PROCESS_DATE, CERT_STATUS, REQ_DATE,REQ_BY FROM (SELECT  NO_DOC,NO_PROP, NO_KAB, NO_KEC, NO_KEL, CERT_STATUS,PEJABAT_PROCESS_DATE, REQ_DATE,REQ_BY, RANK() OVER (PARTITION BY NO_DOC ORDER BY REQ_DATE DESC,SEQN_ID DESC) RNK FROM BSRE_KARTU_KELUARGA ) WHERE RNK = 1) B 
                    GROUP BY B.NO_PROP, B.NO_KAB, B.NO_KEC ORDER BY B.NO_KEC";	
               }else{
               	$sql .="SELECT B.NO_KEL NO_WIL
        			, F5_GET_NAMA_KELURAHAN(B.NO_PROP, B.NO_KAB, B.NO_KEC, B.NO_KEL) NAMA_WIL, '$tgl' TGL
                    ,SUM(CASE WHEN TRUNC(REQ_DATE) >= TRUNC(TO_DATE('$tgl_start','DD-MM-YYYY')) AND TRUNC(REQ_DATE) <= TRUNC(TO_DATE('$tgl_end','DD-MM-YYYY')) THEN 1 ELSE 0 END) REQUEST_N
                    ,SUM(CASE WHEN TRUNC(REQ_DATE) >= TRUNC(TO_DATE('$tgl_start','DD-MM-YYYY')) AND TRUNC(REQ_DATE) <= TRUNC(TO_DATE('$tgl_end','DD-MM-YYYY')) AND PEJABAT_PROCESS_DATE IS NOT NULL THEN 1 ELSE 0 END) APROVED_N
                    ,SUM(CASE WHEN TRUNC(REQ_DATE) >= TRUNC(TO_DATE('$tgl_start','DD-MM-YYYY')) AND TRUNC(REQ_DATE) <= TRUNC(TO_DATE('$tgl_end','DD-MM-YYYY')) AND PEJABAT_PROCESS_DATE IS NULL THEN 1 ELSE 0 END) THE_REST_N
                    ,SUM(CASE WHEN TRUNC(REQ_DATE) >= TRUNC(TO_DATE('$tgl_start','DD-MM-YYYY')) AND TRUNC(REQ_DATE) <= TRUNC(TO_DATE('$tgl_end','DD-MM-YYYY')) AND B.CERT_STATUS = 2  THEN 1 ELSE 0 END) APROVED_ORANGE
                    ,SUM(CASE WHEN TRUNC(REQ_DATE) >= TRUNC(TO_DATE('$tgl_start','DD-MM-YYYY')) AND TRUNC(REQ_DATE) <= TRUNC(TO_DATE('$tgl_end','DD-MM-YYYY')) AND B.CERT_STATUS = 3  THEN 1 ELSE 0 END) APROVED_BLUE
                    ,SUM(CASE WHEN TRUNC(REQ_DATE) >= TRUNC(TO_DATE('$tgl_start','DD-MM-YYYY')) AND TRUNC(REQ_DATE) <= TRUNC(TO_DATE('$tgl_end','DD-MM-YYYY')) AND B.CERT_STATUS = 9  THEN 1 ELSE 0 END) APROVED_RED
                    ,SUM(CASE WHEN TRUNC(REQ_DATE) >= TRUNC(TO_DATE('$tgl_start','DD-MM-YYYY')) AND TRUNC(REQ_DATE) <= TRUNC(TO_DATE('$tgl_end','DD-MM-YYYY')) AND B.CERT_STATUS NOT IN (1,2,3,9)  THEN 1 ELSE 0 END) APROVED_GREEN
                    ,COUNT(1) REQUEST_A
                    ,SUM(CASE WHEN PEJABAT_PROCESS_DATE IS NOT NULL THEN 1 ELSE 0 END) APROVED_A
                    ,SUM(CASE WHEN PEJABAT_PROCESS_DATE IS NULL THEN 1 ELSE 0 END) THE_REST_A
                    ,SUM(CASE WHEN B.CERT_STATUS = 2  THEN 1 ELSE 0 END) A_ORANGE
                    ,SUM(CASE WHEN B.CERT_STATUS = 3  THEN 1 ELSE 0 END) A_BLUE
                    ,SUM(CASE WHEN B.CERT_STATUS = 3  THEN 1 ELSE 0 END) A_RED
                    ,SUM(CASE WHEN B.CERT_STATUS NOT IN (1,2,3,9)  THEN 1 ELSE 0 END) A_GREEN
                     FROM  (SELECT  NO_DOC,NO_PROP, NO_KAB, NO_KEC, NO_KEL, PEJABAT_PROCESS_DATE, CERT_STATUS, REQ_DATE,REQ_BY FROM (SELECT  NO_DOC,NO_PROP, NO_KAB, NO_KEC, NO_KEL, CERT_STATUS,PEJABAT_PROCESS_DATE, REQ_DATE,REQ_BY, RANK() OVER (PARTITION BY NO_DOC ORDER BY REQ_DATE DESC,SEQN_ID DESC) RNK FROM BSRE_KARTU_KELUARGA ) WHERE RNK = 1) B ";
                     if ($no_kel == 0){
                     	$sql .=" WHERE B.NO_KEC = $no_kec ";
                     }else{
                     	$sql .=" WHERE B.NO_KEC = $no_kec AND B.NO_KEL = $no_kel ";
                     }
                     $sql .="
                    GROUP BY B.NO_PROP, B.NO_KAB, B.NO_KEC, B.NO_KEL ORDER BY B.NO_KEL";	
               }
			$r = DB::select($sql);
			return $r;
		}

		public function mst_get_laporan_tte_perpindahan($tgl,$tgl_start,$tgl_end){
			$sql = "";
			$sql .="SELECT 
					B.NAMA_LGKP NAMA_PETUGAS, '$tgl' TGL
					,SUM(CASE WHEN TRUNC(REQ_DATE) >= TRUNC(TO_DATE('$tgl_start','DD-MM-YYYY')) AND TRUNC(REQ_DATE) <= TRUNC(TO_DATE('$tgl_end','DD-MM-YYYY')) THEN 1 ELSE 0 END) REQUEST_N
					,SUM(CASE WHEN TRUNC(REQ_DATE) >= TRUNC(TO_DATE('$tgl_start','DD-MM-YYYY')) AND TRUNC(REQ_DATE) <= TRUNC(TO_DATE('$tgl_end','DD-MM-YYYY')) AND PEJABAT_PROCESS_DATE IS NOT NULL THEN 1 ELSE 0 END) APROVED_N
					,SUM(CASE WHEN TRUNC(REQ_DATE) >= TRUNC(TO_DATE('$tgl_start','DD-MM-YYYY')) AND TRUNC(REQ_DATE) <= TRUNC(TO_DATE('$tgl_end','DD-MM-YYYY')) AND PEJABAT_PROCESS_DATE IS NULL THEN 1 ELSE 0 END) THE_REST_N
					,SUM(CASE WHEN TRUNC(REQ_DATE) >= TRUNC(TO_DATE('$tgl_start','DD-MM-YYYY')) AND TRUNC(REQ_DATE) <= TRUNC(TO_DATE('$tgl_end','DD-MM-YYYY')) AND CERT_STATUS IN 2  THEN 1 ELSE 0 END) APROVED_ORANGE
					,SUM(CASE WHEN TRUNC(REQ_DATE) >= TRUNC(TO_DATE('$tgl_start','DD-MM-YYYY')) AND TRUNC(REQ_DATE) <= TRUNC(TO_DATE('$tgl_end','DD-MM-YYYY')) AND CERT_STATUS = 3  THEN 1 ELSE 0 END) APROVED_BLUE
					,SUM(CASE WHEN TRUNC(REQ_DATE) >= TRUNC(TO_DATE('$tgl_start','DD-MM-YYYY')) AND TRUNC(REQ_DATE) <= TRUNC(TO_DATE('$tgl_end','DD-MM-YYYY')) AND CERT_STATUS = 9  THEN 1 ELSE 0 END) APROVED_RED
					,SUM(CASE WHEN TRUNC(REQ_DATE) >= TRUNC(TO_DATE('$tgl_start','DD-MM-YYYY')) AND TRUNC(REQ_DATE) <= TRUNC(TO_DATE('$tgl_end','DD-MM-YYYY')) AND CERT_STATUS NOT IN (1,2,3,9)  THEN 1 ELSE 0 END) APROVED_GREEN
					FROM (SELECT  A.SEQN_ID
                         , A.NO_DOC
                         , A.PEJABAT_NIP
                         , A.PEJABAT_NAMA_LGKP
                         , A.PEJABAT_JABATAN
                         , A.PEJABAT_PROCESS_DATE
                         , A.URL_DOKUMEN
                         , A.COUNT_PRINT
                         , A.SENT_STATUS
                         , A.SENT_DATE
                         , A.ACTIVE_STATUS
                         , A.ACTIVE_DATE
                         , A.CERT_STATUS
                         , A.NO_PROP
                         , A.NO_KAB
                         , A.REQ_BY
          , A.REQ_DATE FROM (SELECT  A.SEQN_ID
                         , A.NO_DOC
                         , A.PEJABAT_NIP
                         , A.PEJABAT_NAMA_LGKP
                         , A.PEJABAT_JABATAN
                         , A.PEJABAT_PROCESS_DATE
                         , A.URL_DOKUMEN
                         , A.COUNT_PRINT
                         , A.SENT_STATUS
                         , A.SENT_DATE
                         , A.ACTIVE_STATUS
                         , A.ACTIVE_DATE
                         , A.CERT_STATUS
                         , A.NO_PROP
                         , A.NO_KAB
                         , A.REQ_BY
          , A.REQ_DATE , RANK() OVER (PARTITION BY NO_DOC ORDER BY REQ_DATE DESC,SEQN_ID DESC) RNK FROM BSRE_PERPINDAHAN A ) A WHERE RNK = 1) A INNER JOIN T5_SIAK_USER B ON A.REQ_BY = B.USER_ID WHERE TRUNC(REQ_DATE) >= TRUNC(TO_DATE('$tgl_start','DD-MM-YYYY')) AND TRUNC(REQ_DATE) <= TRUNC(TO_DATE('$tgl_end','DD-MM-YYYY')) GROUP BY REQ_BY,NAMA_LGKP ORDER BY B.NAMA_LGKP";	
			$r = DB::connection('db222')->select($sql);
			return $r;
		}

		public function mst_get_laporan_tte_kelahiran($tgl,$tgl_start,$tgl_end){
			$sql = "";
			$sql .="SELECT 
					B.NAMA_LGKP NAMA_PETUGAS, '$tgl' TGL
					,SUM(CASE WHEN TRUNC(REQ_DATE) >= TRUNC(TO_DATE('$tgl_start','DD-MM-YYYY')) AND TRUNC(REQ_DATE) <= TRUNC(TO_DATE('$tgl_end','DD-MM-YYYY')) THEN 1 ELSE 0 END) REQUEST_N
					,SUM(CASE WHEN TRUNC(REQ_DATE) >= TRUNC(TO_DATE('$tgl_start','DD-MM-YYYY')) AND TRUNC(REQ_DATE) <= TRUNC(TO_DATE('$tgl_end','DD-MM-YYYY')) AND PEJABAT_PROCESS_DATE IS NOT NULL THEN 1 ELSE 0 END) APROVED_N
					,SUM(CASE WHEN TRUNC(REQ_DATE) >= TRUNC(TO_DATE('$tgl_start','DD-MM-YYYY')) AND TRUNC(REQ_DATE) <= TRUNC(TO_DATE('$tgl_end','DD-MM-YYYY')) AND PEJABAT_PROCESS_DATE IS NULL THEN 1 ELSE 0 END) THE_REST_N
					,SUM(CASE WHEN TRUNC(REQ_DATE) >= TRUNC(TO_DATE('$tgl_start','DD-MM-YYYY')) AND TRUNC(REQ_DATE) <= TRUNC(TO_DATE('$tgl_end','DD-MM-YYYY')) AND CERT_STATUS IN 2  THEN 1 ELSE 0 END) APROVED_ORANGE
					,SUM(CASE WHEN TRUNC(REQ_DATE) >= TRUNC(TO_DATE('$tgl_start','DD-MM-YYYY')) AND TRUNC(REQ_DATE) <= TRUNC(TO_DATE('$tgl_end','DD-MM-YYYY')) AND CERT_STATUS = 3  THEN 1 ELSE 0 END) APROVED_BLUE
					,SUM(CASE WHEN TRUNC(REQ_DATE) >= TRUNC(TO_DATE('$tgl_start','DD-MM-YYYY')) AND TRUNC(REQ_DATE) <= TRUNC(TO_DATE('$tgl_end','DD-MM-YYYY')) AND CERT_STATUS = 9  THEN 1 ELSE 0 END) APROVED_RED
					,SUM(CASE WHEN TRUNC(REQ_DATE) >= TRUNC(TO_DATE('$tgl_start','DD-MM-YYYY')) AND TRUNC(REQ_DATE) <= TRUNC(TO_DATE('$tgl_end','DD-MM-YYYY')) AND CERT_STATUS NOT IN (1,2,3,9)  THEN 1 ELSE 0 END) APROVED_GREEN
					FROM (SELECT  A.SEQN_ID
                         , A.NO_DOKUMEN
                         , A.PEJABAT_NIP
                         , A.PEJABAT_NAMA_LGKP
                         , A.PEJABAT_JABATAN
                         , A.PEJABAT_PROCESS_DATE
                         , A.URL_DOKUMEN
                         , A.COUNT_PRINT
                         , A.SENT_STATUS
                         , A.SENT_DATE
                         , A.ACTIVE_STATUS
                         , A.ACTIVE_DATE
                         , A.CERT_STATUS
                         , A.NO_PROV
                         , A.NO_KAB
                         , A.REQ_BY
          				 , A.REQ_DATE FROM (SELECT  A.SEQN_ID
                         , A.NO_DOKUMEN
                         , A.PEJABAT_NIP
                         , A.PEJABAT_NAMA_LGKP
                         , A.PEJABAT_JABATAN
                         , A.PEJABAT_PROCESS_DATE
                         , A.URL_DOKUMEN
                         , A.COUNT_PRINT
                         , A.SENT_STATUS
                         , A.SENT_DATE
                         , A.ACTIVE_STATUS
                         , A.ACTIVE_DATE
                         , A.CERT_STATUS
                         , A.NO_PROV
                         , A.NO_KAB
                         , A.REQ_BY
          , A.REQ_DATE , RANK() OVER (PARTITION BY NO_DOKUMEN ORDER BY REQ_DATE DESC,SEQN_ID DESC) RNK FROM BSRE_KELAHIRAN A ) A WHERE RNK = 1) A INNER JOIN T5_SIAK_USER B ON A.REQ_BY = B.USER_ID WHERE TRUNC(REQ_DATE) >= TRUNC(TO_DATE('$tgl_start','DD-MM-YYYY')) AND TRUNC(REQ_DATE) <= TRUNC(TO_DATE('$tgl_end','DD-MM-YYYY')) GROUP BY REQ_BY,NAMA_LGKP ORDER BY B.NAMA_LGKP";	
			$r = DB::connection('db222')->select($sql);
			return $r;
		}

		public function mst_get_laporan_tte_kematian($tgl,$tgl_start,$tgl_end){
			$sql = "";
			$sql .="SELECT 
					B.NAMA_LGKP NAMA_PETUGAS, '$tgl' TGL
					,SUM(CASE WHEN TRUNC(REQ_DATE) >= TRUNC(TO_DATE('$tgl_start','DD-MM-YYYY')) AND TRUNC(REQ_DATE) <= TRUNC(TO_DATE('$tgl_end','DD-MM-YYYY')) THEN 1 ELSE 0 END) REQUEST_N
					,SUM(CASE WHEN TRUNC(REQ_DATE) >= TRUNC(TO_DATE('$tgl_start','DD-MM-YYYY')) AND TRUNC(REQ_DATE) <= TRUNC(TO_DATE('$tgl_end','DD-MM-YYYY')) AND PEJABAT_PROCESS_DATE IS NOT NULL THEN 1 ELSE 0 END) APROVED_N
					,SUM(CASE WHEN TRUNC(REQ_DATE) >= TRUNC(TO_DATE('$tgl_start','DD-MM-YYYY')) AND TRUNC(REQ_DATE) <= TRUNC(TO_DATE('$tgl_end','DD-MM-YYYY')) AND PEJABAT_PROCESS_DATE IS NULL THEN 1 ELSE 0 END) THE_REST_N
					,SUM(CASE WHEN TRUNC(REQ_DATE) >= TRUNC(TO_DATE('$tgl_start','DD-MM-YYYY')) AND TRUNC(REQ_DATE) <= TRUNC(TO_DATE('$tgl_end','DD-MM-YYYY')) AND CERT_STATUS IN 2  THEN 1 ELSE 0 END) APROVED_ORANGE
					,SUM(CASE WHEN TRUNC(REQ_DATE) >= TRUNC(TO_DATE('$tgl_start','DD-MM-YYYY')) AND TRUNC(REQ_DATE) <= TRUNC(TO_DATE('$tgl_end','DD-MM-YYYY')) AND CERT_STATUS = 3  THEN 1 ELSE 0 END) APROVED_BLUE
					,SUM(CASE WHEN TRUNC(REQ_DATE) >= TRUNC(TO_DATE('$tgl_start','DD-MM-YYYY')) AND TRUNC(REQ_DATE) <= TRUNC(TO_DATE('$tgl_end','DD-MM-YYYY')) AND CERT_STATUS = 9  THEN 1 ELSE 0 END) APROVED_RED
					,SUM(CASE WHEN TRUNC(REQ_DATE) >= TRUNC(TO_DATE('$tgl_start','DD-MM-YYYY')) AND TRUNC(REQ_DATE) <= TRUNC(TO_DATE('$tgl_end','DD-MM-YYYY')) AND CERT_STATUS NOT IN (1,2,3,9)  THEN 1 ELSE 0 END) APROVED_GREEN
					FROM (SELECT  A.SEQN_ID
                         , A.NO_DOKUMEN
                         , A.PEJABAT_NIP
                         , A.PEJABAT_NAMA_LGKP
                         , A.PEJABAT_JABATAN
                         , A.PEJABAT_PROCESS_DATE
                         , A.URL_DOKUMEN
                         , A.COUNT_PRINT
                         , A.SENT_STATUS
                         , A.SENT_DATE
                         , A.ACTIVE_STATUS
                         , A.ACTIVE_DATE
                         , A.CERT_STATUS
                         , A.NO_PROV
                         , A.NO_KAB
                         , A.REQ_BY
          				 , A.REQ_DATE FROM (SELECT  A.SEQN_ID
                         , A.NO_DOKUMEN
                         , A.PEJABAT_NIP
                         , A.PEJABAT_NAMA_LGKP
                         , A.PEJABAT_JABATAN
                         , A.PEJABAT_PROCESS_DATE
                         , A.URL_DOKUMEN
                         , A.COUNT_PRINT
                         , A.SENT_STATUS
                         , A.SENT_DATE
                         , A.ACTIVE_STATUS
                         , A.ACTIVE_DATE
                         , A.CERT_STATUS
                         , A.NO_PROV
                         , A.NO_KAB
                         , A.REQ_BY
          , A.REQ_DATE , RANK() OVER (PARTITION BY NO_DOKUMEN ORDER BY REQ_DATE DESC,SEQN_ID DESC) RNK FROM BSRE_KEMATIAN A ) A WHERE RNK = 1) A INNER JOIN T5_SIAK_USER B ON A.REQ_BY = B.USER_ID WHERE TRUNC(REQ_DATE) >= TRUNC(TO_DATE('$tgl_start','DD-MM-YYYY')) AND TRUNC(REQ_DATE) <= TRUNC(TO_DATE('$tgl_end','DD-MM-YYYY')) GROUP BY REQ_BY,NAMA_LGKP ORDER BY B.NAMA_LGKP";	
			$r = DB::connection('db222')->select($sql);
			return $r;
		}
		public function mst_get_laporan_lintas_batas($no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEC A LEFT JOIN
					(SELECT 
						B.NO_KEC 
						,B.NAMA_KEC 
						, COUNT(1) JUMLAH
						FROM SETUP_KEC B LEFT JOIN SIAK_PLB A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC WHERE 1=1 AND A.NO_PROP = 32 AND A.NO_KAB =73 GROUP BY B.NO_KEC,B.NAMA_KEC ORDER BY B.NO_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEL A LEFT JOIN
					(SELECT 
						B.NO_KEL 
						,B.NAMA_KEL
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN SIAK_PLB A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC WHERE 1=1 ";
						if($no_kel == 0){    
					$sql .=" AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec." and  A.NO_KEL =".$no_kel."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::select($sql);
               return $r;
		}
		public function mst_get_laporan_petugas_khusus($no_kec,$no_kel){
			$sql = "";
			if($no_kec == 0){
				$sql .="SELECT A.NO_KEC AS NO_WIL, A.NAMA_KEC AS NAMA_WIL
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEC A LEFT JOIN
					(SELECT 
						B.NO_KEC 
						,B.NAMA_KEC 
						, COUNT(1) JUMLAH
						FROM SETUP_KEC B LEFT JOIN SIAK_PETUGAS_KHUSUS A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC WHERE 1=1 AND A.NO_PROP = 32 AND A.NO_KAB =73 GROUP BY B.NO_KEC,B.NAMA_KEC ORDER BY B.NO_KEC) B ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";	
			}else{
				$sql .="SELECT A.NO_KEL AS NO_WIL, A.NAMA_KEL AS NAMA_WIL
					, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
					FROM SETUP_KEL A LEFT JOIN
					(SELECT 
						B.NO_KEL 
						,B.NAMA_KEL
						, COUNT(1) JUMLAH
						FROM SETUP_KEL B LEFT JOIN SIAK_PETUGAS_KHUSUS A ON A.NO_PROP = B.NO_PROP AND A.NO_KAB = B.NO_KAB AND A.NO_KEC = B.NO_KEC WHERE 1=1 ";
						if($no_kel == 0){    
					$sql .=" AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
						}else{
					$sql .=" AND B.NO_PROP = 32 AND B.NO_KAB = 73 AND A.NO_KEC =".$no_kec." and  A.NO_KEL =".$no_kel."
						    GROUP BY B.NO_KEL, B.NAMA_KEL
						    ORDER BY B.NO_KEL, B.NAMA_KEL) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND A.NO_KEL =".$no_kel." ORDER BY A.NO_KEL";
						}
				}
				$r = DB::select($sql);
               return $r;
		}
		public function do_get_petugas_capil(){
			$sql = "SELECT A.USER_ID, A.NAMA_LGKP, CASE WHEN A.JENIS_KLMIN = 1 THEN 'LAKI-LAKI' ELSE 'PEREMPUAN' END JENIS_KLMIN , B.LEVEL_NAME , C.LEVEL_NAME GROUP_NAME
					FROM T5_SIAK_USER A 
					INNER JOIN T5_SIAK_LEVEL B ON A.USER_LEVEL = B.USER_LEVEL
					INNER JOIN T5_GROUP_LEVEL C ON B.GROUP_LEVEL = C.LEVEL_CODE
					WHERE A.USER_LEVEL  IN (2,3,13,14,15,18,26,27) ORDER BY A.USER_LEVEL, A.USER_ID";
			$r = DB::connection('db222')->select($sql);
            return $r;	
		}
		public function get_data_kpu($bln,$tipe_data,$kategori_data){
			if($tipe_data == 1){
				if($kategori_data ==1){
					$sql = "SELECT 
					TO_CHAR(A.NIK) NIK, A.NAMA_LGKP, A.JENIS_KLMIN, 
					A.NO_KEC,
					F5_GET_NAMA_KECAMATAN(32,73,A.NO_KEC) NAMA_KEC,
					A.NO_KEL ,
					F5_GET_NAMA_KELURAHAN(32,73,A.NO_KEC,A.NO_KEL) NAMA_KEL,
					CASE WHEN A.STAT_KWN= 1 THEN 'B' WHEN A.STAT_KWN = 2 THEN 'K' ELSE 'P' END STAT_KWN,
					CASE WHEN B.NIK IS NULL THEN 'B' ELSE 'S' END STAT_REKAM
					, UPPER(F5_GET_REF_WNI(A.PNYDNG_CCT, 701)) PNYDNG_CCT 
					FROM BIODATA_WNI A LEFT JOIN DEMOGRAPHICS_ALL B ON A.NIK = B.NIK WHERE TO_CHAR(ADD_MONTHS(A.TGL_LHR,+12*17),'MM/YYYY') = '$bln' 
					ORDER BY A.NO_KEC,A.NO_KEL";
				}else if($kategori_data ==2){
					$sql = "SELECT A.*, CASE WHEN B.NIK IS NULL THEN 'B' ELSE 'S' END STAT_REKAM, UPPER(F5_GET_REF_WNI(C.PNYDNG_CCT, 701)) PNYDNG_CCT  FROM 
						(SELECT MAX(TO_CHAR(NIK)) NIK, MAX(B.NAMA_LENGKAP) NAMA_LENGKAP
						, MAX(A.DEST_NO_KEC) NO_KEC
						, MAX(F5_GET_NAMA_KECAMATAN(32,73,A.DEST_NO_KEC)) NAMA_KEC
						, MAX(A.DEST_NO_KEL) NO_KEL
						, MAX(F5_GET_NAMA_KELURAHAN(32,73,A.DEST_NO_KEC,A.DEST_NO_KEL)) NAMA_KEL
						, MAX(B.JENIS_KLMIN) JENIS_KLMIN, MAX(TO_CHAR(TGL_LAHIR,'MM')) BLN , MAX(TO_CHAR(TGL_LAHIR,'YY')) THN
						, MAX (CASE WHEN STAT_KWN= 1 THEN 'B' WHEN STAT_KWN = 2 THEN 'K' ELSE 'P' END) STAT_KWN
						FROM PINDAH_HEADER A INNER JOIN PINDAH_DETAIL B ON A.NO_PINDAH = B.NO_PINDAH WHERE TO_CHAR(A.CREATED_DATE,'MM/YYYY') = '$bln' 
						AND A.DEST_NO_PROP = 32 AND A.DEST_NO_KAB = 73 AND A.DEST_NO_KEC <= 30 AND A.KLASIFIKASI_PINDAH > 3 AND TRUNC(MONTHS_BETWEEN(A.CREATED_DATE,B.TGL_LAHIR)/12) >= 17 
						GROUP BY NIK) A LEFT JOIN BIODATA_WNI C ON A.NIK = C.NIK LEFT JOIN DEMOGRAPHICS_ALL@DB5CETAK B ON A.NIK = B.NIK  ORDER BY A.NO_KEC,A.NO_KEL";
				}else if($kategori_data ==3){
					$sql = "SELECT TO_CHAR(A.NIK) NIK, A.NAMA_LGKP, A.JENIS_KLMIN
						, A.NO_KEC
						, F5_GET_NAMA_KECAMATAN(32,73,A.NO_KEC) NAMA_KEC
						, A.NO_KEL
						, F5_GET_NAMA_KELURAHAN(32,73,A.NO_KEC,A.NO_KEL) NAMA_KEL 
						, CASE WHEN A.STAT_KWN= 1 THEN 'B' WHEN A.STAT_KWN = 2 THEN 'K' ELSE 'P' END STAT_KWN
						, CASE WHEN B.NIK IS NULL THEN 'B' ELSE 'S' END STAT_REKAM
						, UPPER(F5_GET_REF_WNI(A.PNYDNG_CCT, 701)) PNYDNG_CCT
						FROM BIODATA_WNI A LEFT JOIN DEMOGRAPHICS_ALL@DB5CETAK B ON A.NIK = B.NIK WHERE A.NIK IN 
						(SELECT MAX(NIK) NIK FROM HISTORY_BIODATA_WNI_HEADER@DB222 A INNER JOIN HISTORY_BIODATA_WNI_DETAIL@DB222 B ON A.SEQ_ID_HEADER = B.SEQ_ID_HEADER 
						WHERE TO_CHAR(A.UPDATED_DATE,'MM/YYYY') = '$bln' AND B.VFIELD_NAME = 'JENIS PEKERJAAN' AND B.OLD_VALUE IN ('6','7') GROUP BY NIK) 
						  ORDER BY A.NO_KEC,A.NO_KEL";
				}else if($kategori_data ==4){
					$sql = "SELECT A.*, CASE WHEN B.NIK IS NULL THEN 'B' ELSE 'S' END STAT_REKAM, UPPER(F5_GET_REF_WNI(C.PNYDNG_CCT, 701)) PNYDNG_CCT FROM 
						(SELECT MAX(TO_CHAR(NIK)) NIK, MAX(B.NAMA_LENGKAP) NAMA_LENGKAP
						, MAX(A.FROM_NO_KEC) NO_KEC
						, MAX(F5_GET_NAMA_KECAMATAN(32,73,A.FROM_NO_KEC)) NAMA_KEC
						, MAX(A.FROM_NO_KEL) NO_KEL
						, MAX(F5_GET_NAMA_KELURAHAN(32,73,A.FROM_NO_KEC,A.FROM_NO_KEL)) NAMA_KEL
						,MAX(B.JENIS_KLMIN) JENIS_KLMIN, MAX(TO_CHAR(TGL_LAHIR,'MM')) BLN , MAX(TO_CHAR(TGL_LAHIR,'YY')) THN
						, MAX (CASE WHEN STAT_KWN= 1 THEN 'B' WHEN STAT_KWN = 2 THEN 'K' ELSE 'P' END) STAT_KWN
						FROM PINDAH_HEADER@DB222 A INNER JOIN PINDAH_DETAIL@DB222 B ON A.NO_PINDAH = B.NO_PINDAH WHERE TO_CHAR(A.CREATED_DATE,'MM/YYYY') = '$bln' 
						AND A.FROM_NO_PROP = 32 AND A.FROM_NO_KAB = 73  AND A.KLASIFIKASI_PINDAH > 3 AND TRUNC(MONTHS_BETWEEN(A.CREATED_DATE,B.TGL_LAHIR)/12) >= 17 
						GROUP BY NIK) A LEFT JOIN BIODATA_WNI_DELETE@DB222 C ON A.NIK = C.NIK LEFT JOIN DEMOGRAPHICS_ALL B ON A.NIK = B.NIK ORDER BY A.NO_KEC, A.NO_KEL";
				}else if($kategori_data ==5){
					$sql = "SELECT TO_CHAR(MATI_NIK) MATI_NIK, MATI_NAMA_LGKP, MATI_JNS_KELAMIN
						, A.ADM_NO_KEC
						, F5_GET_NAMA_KECAMATAN(32,73,A.ADM_NO_KEC) ADM_NAMA_KEC
						, A.ADM_NO_KEL
						, F5_GET_NAMA_KELURAHAN(32,73,A.ADM_NO_KEC,A.ADM_NO_KEL) ADM_NAMA_KEL 
						, CASE WHEN B.STAT_KWN= 1 THEN 'B' WHEN B.STAT_KWN = 2 THEN 'K' ELSE 'P' END STAT_KWN
						, CASE WHEN C.NIK IS NULL THEN 'B' ELSE 'S' END STAT_REKAM 
						, UPPER(F5_GET_REF_WNI(B.PNYDNG_CCT, 701)) PNYDNG_CCT 
						FROM CAPIL_MATI@DB222  A INNER JOIN BIODATA_WNI_DELETE@DB222 B ON TO_CHAR(B.NIK) = A.MATI_NIK 
						LEFT JOIN DEMOGRAPHICS_ALL@DB5CETAK C ON A.MATI_NIK = TO_CHAR(C.NIK)
						WHERE  TO_CHAR(ADM_AKTA_TGL,'MM/YYYY') = '$bln' AND MATI_NIK IS NOT NULL AND TRUNC(MONTHS_BETWEEN(A.ADM_AKTA_TGL,A.MATI_TGL_LAHIR)/12) >= 17 
						ORDER BY ADM_NO_KEC, ADM_NO_KEL";
				}else{
					$sql ="";
				}
			}else if($tipe_data == 2){
				if($kategori_data ==1){
					$sql = "SELECT 
							NO_KEC
							, NAMA_KEC
							, NO_KEL
							, NAMA_KEL
							, SUM(CASE WHEN STAT_KWN = 'B' THEN 1 ELSE 0 END) BELUM_KAWIN
							, SUM(CASE WHEN STAT_KWN = 'K' THEN 1 ELSE 0 END) SUDAH_KAWIN
							, SUM(CASE WHEN STAT_KWN = 'P' THEN 1 ELSE 0 END) PERNAH_KAWIN
							, SUM(CASE WHEN STAT_REKAM = 'B' THEN 1 ELSE 0 END) BELUM_REKAM
							, SUM(CASE WHEN STAT_REKAM = 'S' THEN 1 ELSE 0 END) SUDAH_REKAM
							, SUM(CASE WHEN PNYDNG_CCT <> '-' THEN 1 ELSE 0 END) DISABILITAS
							, COUNT(1) JUMLAH FROM (
							SELECT 
							TO_CHAR(A.NIK) NIK, A.NAMA_LGKP, A.JENIS_KLMIN, 
							A.NO_KEC,
							F5_GET_NAMA_KECAMATAN(32,73,A.NO_KEC) NAMA_KEC,
							A.NO_KEL ,
							F5_GET_NAMA_KELURAHAN(32,73,A.NO_KEC,A.NO_KEL) NAMA_KEL,
							CASE WHEN A.STAT_KWN= 1 THEN 'B' WHEN A.STAT_KWN = 2 THEN 'K' ELSE 'P' END STAT_KWN,
							CASE WHEN B.NIK IS NULL THEN 'B' ELSE 'S' END STAT_REKAM
							, UPPER(F5_GET_REF_WNI(A.PNYDNG_CCT, 701)) PNYDNG_CCT 
							FROM BIODATA_WNI A LEFT JOIN DEMOGRAPHICS_ALL B ON A.NIK = B.NIK WHERE TO_CHAR(ADD_MONTHS(A.TGL_LHR,+12*17),'MM/YYYY') = '$bln'  
							ORDER BY A.NO_KEC,A.NO_KEL
							) GROUP BY NO_KEC, NAMA_KEC, NO_KEL, NAMA_KEL ORDER BY NO_KEC, NO_KEL";
				}else if($kategori_data ==2){
					$sql = "SELECT 
							NO_KEC
							, NAMA_KEC
							, NO_KEL
							, NAMA_KEL
							, SUM(CASE WHEN STAT_KWN = 'B' THEN 1 ELSE 0 END) BELUM_KAWIN
							, SUM(CASE WHEN STAT_KWN = 'K' THEN 1 ELSE 0 END) SUDAH_KAWIN
							, SUM(CASE WHEN STAT_KWN = 'P' THEN 1 ELSE 0 END) PERNAH_KAWIN
							, SUM(CASE WHEN STAT_REKAM = 'B' THEN 1 ELSE 0 END) BELUM_REKAM
							, SUM(CASE WHEN STAT_REKAM = 'S' THEN 1 ELSE 0 END) SUDAH_REKAM
							, SUM(CASE WHEN PNYDNG_CCT <> '-' THEN 1 ELSE 0 END) DISABILITAS
							, COUNT(1) JUMLAH FROM (
							SELECT A.*, CASE WHEN B.NIK IS NULL THEN 'B' ELSE 'S' END STAT_REKAM, UPPER(F5_GET_REF_WNI(C.PNYDNG_CCT, 701)) PNYDNG_CCT  FROM 
							(SELECT MAX(TO_CHAR(NIK)) NIK, MAX(B.NAMA_LENGKAP) NAMA_LENGKAP
							, MAX(A.DEST_NO_KEC) NO_KEC
							, MAX(F5_GET_NAMA_KECAMATAN(32,73,A.DEST_NO_KEC)) NAMA_KEC
							, MAX(A.DEST_NO_KEL) NO_KEL
							, MAX(F5_GET_NAMA_KELURAHAN(32,73,A.DEST_NO_KEC,A.DEST_NO_KEL)) NAMA_KEL
							, MAX(B.JENIS_KLMIN) JENIS_KLMIN, MAX(TO_CHAR(TGL_LAHIR,'MM')) BLN , MAX(TO_CHAR(TGL_LAHIR,'YY')) THN
							, MAX (CASE WHEN STAT_KWN= 1 THEN 'B' WHEN STAT_KWN = 2 THEN 'K' ELSE 'P' END) STAT_KWN
							FROM PINDAH_HEADER A INNER JOIN PINDAH_DETAIL B ON A.NO_PINDAH = B.NO_PINDAH WHERE TO_CHAR(A.CREATED_DATE,'MM/YYYY') = '$bln' 
							AND A.DEST_NO_PROP = 32 AND A.DEST_NO_KAB = 73 AND A.DEST_NO_KEC <= 30 AND A.KLASIFIKASI_PINDAH > 3 AND TRUNC(MONTHS_BETWEEN(A.CREATED_DATE,B.TGL_LAHIR)/12) >= 17 
							GROUP BY NIK) A LEFT JOIN BIODATA_WNI C ON A.NIK = C.NIK LEFT JOIN DEMOGRAPHICS_ALL@DB5CETAK B ON A.NIK = B.NIK  ORDER BY A.NO_KEC,A.NO_KEL
							) GROUP BY NO_KEC, NAMA_KEC, NO_KEL, NAMA_KEL ORDER BY NO_KEC, NO_KEL";
				}else if($kategori_data ==3){
					$sql = "SELECT 
							NO_KEC
							, NAMA_KEC
							, NO_KEL
							, NAMA_KEL
							, SUM(CASE WHEN STAT_KWN = 'B' THEN 1 ELSE 0 END) BELUM_KAWIN
							, SUM(CASE WHEN STAT_KWN = 'K' THEN 1 ELSE 0 END) SUDAH_KAWIN
							, SUM(CASE WHEN STAT_KWN = 'P' THEN 1 ELSE 0 END) PERNAH_KAWIN
							, SUM(CASE WHEN STAT_REKAM = 'B' THEN 1 ELSE 0 END) BELUM_REKAM
							, SUM(CASE WHEN STAT_REKAM = 'S' THEN 1 ELSE 0 END) SUDAH_REKAM
							, SUM(CASE WHEN PNYDNG_CCT <> '-' THEN 1 ELSE 0 END) DISABILITAS
							, COUNT(1) JUMLAH FROM (
							SELECT TO_CHAR(A.NIK) NIK, A.NAMA_LGKP, A.JENIS_KLMIN
							, A.NO_KEC
							, F5_GET_NAMA_KECAMATAN(32,73,A.NO_KEC) NAMA_KEC
							, A.NO_KEL
							, F5_GET_NAMA_KELURAHAN(32,73,A.NO_KEC,A.NO_KEL) NAMA_KEL 
							, CASE WHEN A.STAT_KWN= 1 THEN 'B' WHEN A.STAT_KWN = 2 THEN 'K' ELSE 'P' END STAT_KWN
							, CASE WHEN B.NIK IS NULL THEN 'B' ELSE 'S' END STAT_REKAM
							, UPPER(F5_GET_REF_WNI(A.PNYDNG_CCT, 701)) PNYDNG_CCT
							FROM BIODATA_WNI A LEFT JOIN DEMOGRAPHICS_ALL@DB5CETAK B ON A.NIK = B.NIK WHERE A.NIK IN 
							(SELECT MAX(NIK) NIK FROM HISTORY_BIODATA_WNI_HEADER@DB222 A INNER JOIN HISTORY_BIODATA_WNI_DETAIL@DB222 B ON A.SEQ_ID_HEADER = B.SEQ_ID_HEADER 
							WHERE TO_CHAR(A.UPDATED_DATE,'MM/YYYY') = '$bln' AND B.VFIELD_NAME = 'JENIS PEKERJAAN' AND B.OLD_VALUE IN ('6','7') GROUP BY NIK) 
							  ORDER BY A.NO_KEC,A.NO_KEL
							  ) GROUP BY NO_KEC, NAMA_KEC, NO_KEL, NAMA_KEL ORDER BY NO_KEC, NO_KEL";
				}else if($kategori_data ==4){
					$sql = "SELECT 
							NO_KEC
							, NAMA_KEC
							, NO_KEL
							, NAMA_KEL
							, SUM(CASE WHEN STAT_KWN = 'B' THEN 1 ELSE 0 END) BELUM_KAWIN
							, SUM(CASE WHEN STAT_KWN = 'K' THEN 1 ELSE 0 END) SUDAH_KAWIN
							, SUM(CASE WHEN STAT_KWN = 'P' THEN 1 ELSE 0 END) PERNAH_KAWIN
							, SUM(CASE WHEN STAT_REKAM = 'B' THEN 1 ELSE 0 END) BELUM_REKAM
							, SUM(CASE WHEN STAT_REKAM = 'S' THEN 1 ELSE 0 END) SUDAH_REKAM
							, SUM(CASE WHEN PNYDNG_CCT <> '-' THEN 1 ELSE 0 END) DISABILITAS
							, COUNT(1) JUMLAH FROM (
							SELECT A.*, CASE WHEN B.NIK IS NULL THEN 'B' ELSE 'S' END STAT_REKAM, UPPER(F5_GET_REF_WNI(C.PNYDNG_CCT, 701)) PNYDNG_CCT FROM 
							(SELECT MAX(TO_CHAR(NIK)) NIK, MAX(B.NAMA_LENGKAP) NAMA_LENGKAP
							, MAX(A.FROM_NO_KEC) NO_KEC
							, MAX(F5_GET_NAMA_KECAMATAN(32,73,A.FROM_NO_KEC)) NAMA_KEC
							, MAX(A.FROM_NO_KEL) NO_KEL
							, MAX(F5_GET_NAMA_KELURAHAN(32,73,A.FROM_NO_KEC,A.FROM_NO_KEL)) NAMA_KEL
							,MAX(B.JENIS_KLMIN) JENIS_KLMIN, MAX(TO_CHAR(TGL_LAHIR,'MM')) BLN , MAX(TO_CHAR(TGL_LAHIR,'YY')) THN
							, MAX (CASE WHEN STAT_KWN= 1 THEN 'B' WHEN STAT_KWN = 2 THEN 'K' ELSE 'P' END) STAT_KWN
							FROM PINDAH_HEADER@DB222 A INNER JOIN PINDAH_DETAIL@DB222 B ON A.NO_PINDAH = B.NO_PINDAH WHERE TO_CHAR(A.CREATED_DATE,'MM/YYYY') = '$bln' 
							AND A.FROM_NO_PROP = 32 AND A.FROM_NO_KAB = 73  AND 
							A.KLASIFIKASI_PINDAH > 3 AND TRUNC(MONTHS_BETWEEN(A.CREATED_DATE,B.TGL_LAHIR)/12) >= 17 
							GROUP BY NIK) A LEFT JOIN BIODATA_WNI_DELETE@DB222 C ON A.NIK = C.NIK LEFT JOIN DEMOGRAPHICS_ALL B ON A.NIK = B.NIK ORDER BY A.NO_KEC, A.NO_KEL
							) GROUP BY NO_KEC, NAMA_KEC, NO_KEL, NAMA_KEL ORDER BY NO_KEC, NO_KEL";
				}else if($kategori_data ==5){
					$sql = "SELECT 
							NO_KEC
							, NAMA_KEC
							, NO_KEL
							, NAMA_KEL
							, SUM(CASE WHEN STAT_KWN = 'B' THEN 1 ELSE 0 END) BELUM_KAWIN
							, SUM(CASE WHEN STAT_KWN = 'K' THEN 1 ELSE 0 END) SUDAH_KAWIN
							, SUM(CASE WHEN STAT_KWN = 'P' THEN 1 ELSE 0 END) PERNAH_KAWIN
							, SUM(CASE WHEN STAT_REKAM = 'B' THEN 1 ELSE 0 END) BELUM_REKAM
							, SUM(CASE WHEN STAT_REKAM = 'S' THEN 1 ELSE 0 END) SUDAH_REKAM
							, SUM(CASE WHEN PNYDNG_CCT <> '-' THEN 1 ELSE 0 END) DISABILITAS
							, COUNT(1) JUMLAH FROM (
							SELECT TO_CHAR(MATI_NIK) MATI_NIK, MATI_NAMA_LGKP, MATI_JNS_KELAMIN
							, A.ADM_NO_KEC NO_KEC
							, F5_GET_NAMA_KECAMATAN(32,73,A.ADM_NO_KEC) NAMA_KEC
							, A.ADM_NO_KEL NO_KEL
							, F5_GET_NAMA_KELURAHAN(32,73,A.ADM_NO_KEC,A.ADM_NO_KEL) NAMA_KEL 
							, CASE WHEN B.STAT_KWN= 1 THEN 'B' WHEN B.STAT_KWN = 2 THEN 'K' ELSE 'P' END STAT_KWN
							, CASE WHEN C.NIK IS NULL THEN 'B' ELSE 'S' END STAT_REKAM 
							, UPPER(F5_GET_REF_WNI(B.PNYDNG_CCT, 701)) PNYDNG_CCT 
							FROM CAPIL_MATI@DB222  A INNER JOIN BIODATA_WNI_DELETE@DB222 B ON TO_CHAR(B.NIK) = A.MATI_NIK 
							LEFT JOIN DEMOGRAPHICS_ALL C ON A.MATI_NIK = TO_CHAR(C.NIK)
							WHERE  TO_CHAR(ADM_AKTA_TGL,'MM/YYYY') = '$bln' AND MATI_NIK IS NOT NULL AND TRUNC(MONTHS_BETWEEN(A.ADM_AKTA_TGL,A.MATI_TGL_LAHIR)/12) >= 17 
							ORDER BY ADM_NO_KEC, ADM_NO_KEL
							) GROUP BY NO_KEC, NAMA_KEC, NO_KEL, NAMA_KEL ORDER BY NO_KEC, NO_KEL";
				}else{
					$sql ="";
				}
			}else{
				$sql ="";
			}
			$r = DB::select($sql);
            return $r;	
		}
		
		public function all_get_dkb($val){
			$sql = "SELECT VAL, DKB, BIO_NAME, KK_NAME, CUTOFF, LAST_UPDATE, GISA FROM SIAK_MENU_DKB WHERE VAL = $val";
			$r = DB::select($sql);
            return $r[0];	
		}

}
