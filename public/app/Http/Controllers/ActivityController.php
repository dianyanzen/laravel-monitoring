<?php
namespace App\Http\Controllers;

use DB;
use Session;
use DateTime;
use PDF;
use Illuminate\Http\Request;
use App\Http\Controllers\SharedController as Shr;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class ActivityController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function Absensi(Request $request)
    {
        Shr::check_login();
        if (Session::get('S_USER_ID') != null)
        {
            $menu_id = 139;
            $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL') , $menu_id);
            if ($is_akses == 0)
            {
                return redirect()->route('404Notfound');
            }
            $menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
            $isakses_kec = Shr::get_give_kec();
            $isakses_kel = Shr::get_give_kel();
            $data = array(
                "stitle" => 'My Absensi',
                "menu" => $menu,
                "akses_kec" => $isakses_kec,
                "akses_kel" => $isakses_kel,
                "user_id" => $request->session()->get('S_USER_ID') ,
                "user_nik" => $request->session()->get('S_NIK') ,
                "user_nama_lgkp" => $request->session()->get('S_NAMA_LGKP') ,
                "user_nama_kantor" => $request->session()->get('S_NAMA_KANTOR') ,
                "user_nama_dpn" => $request->session()->get('S_NAMA_DPN') ,
                "user_no_kec" => $request->session()->get('S_NO_KEC') ,
                "user_no_kel" => $request->session()->get('S_NO_KEL') ,
                "user_level" => $request->session()->get('S_USER_LEVEL')
            );
            return view('Absensi/main', $data);
        }
        else
        {
            return redirect()->route('logout');
        }
    }
    public function Absensi_v(Request $request)
    {
        Shr::check_login();
        if (Session::get('S_USER_ID') != null) 
        {
            $menu_id = 333;
           $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
            if ($is_akses == 0){
                 return redirect()->route('404Notfound');
            }
            $menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
            $isakses_kec = Shr::get_give_kec();
            $isakses_kel = Shr::get_give_kel();
            $data = array(
                "stitle"=>'Verifikasi Daily Report',
                "mtitle"=>'Verifikasi Daily Report',
                "my_url"=>'Absensi_v',
                "type_tgl"=>'Aktivitas',
                "menu"=>$menu,
                "akses_kec"=>$isakses_kec,
                "akses_kel"=>$isakses_kel,
                "user_id"=>$request->session()->get('S_USER_ID'),
                "user_nik"=>$request->session()->get('S_NIK'),
                "user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
                "user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
                "user_level"=>$request->session()->get('S_USER_LEVEL'),
                "user_no_kec"=>$request->session()->get('S_NO_KEC'),
                "user_no_kel"=>$request->session()->get('S_NO_KEL'),
            );
            return view('Absensi_v/main',$data);
        }else{
            return redirect()->route('logout');
        }
    }
    public function Absensi_vr(Request $request)
    {
        Shr::check_login();
        if (Session::get('S_USER_ID') != null) 
        {
            $menu_id = 334;
           $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
            if ($is_akses == 0){
                 return redirect()->route('404Notfound');
            }
            $menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
            $isakses_kec = Shr::get_give_kec();
            $isakses_kel = Shr::get_give_kel();
            $data = array(
                "stitle"=>'Verifikasi Daily Report',
                "mtitle"=>'Verifikasi Daily Report',
                "my_url"=>'Absensi_vr',
                "type_tgl"=>'Aktivitas',
                "menu"=>$menu,
                "akses_kec"=>$isakses_kec,
                "akses_kel"=>$isakses_kel,
                "user_id"=>$request->session()->get('S_USER_ID'),
                "user_nik"=>$request->session()->get('S_NIK'),
                "user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
                "user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
                "user_level"=>$request->session()->get('S_USER_LEVEL'),
                "user_no_kec"=>$request->session()->get('S_NO_KEC'),
                "user_no_kel"=>$request->session()->get('S_NO_KEL'),
            );
            return view('Absensi_vr/main',$data);
        }else{
            return redirect()->route('logout');
        }
    }

    public function Absensi_ci(Request $request, $user_id)
    {
        Shr::check_login();
        if (Session::get('S_USER_ID') != null)
        {
            $menu_id = 139;
            $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL') , $menu_id);
            if ($is_akses == 0)
            {
                return redirect()->route('404Notfound');
            }
            if ($user_id != null)
            {
                $user_id = base64_decode($user_id);
                $r = $this->act_report_clock_in($user_id);
                if (count($r) > 0)
                {
                    $r = $r[0];
                    $data["user_id"] = $r->user_id;
                    $data["nama"] = $r->nama;
                    $data["tanggal"] = $r->tanggal;
                    $data["jam_masuk"] = $r->jam_masuk;
                    $data["jam_keluar"] = $r->jam_keluar;
                    $data["keterangan"] = $r->keterangan;
                    $data["ip_address"] = $r->ip_address;
                    $data["is_ci"] = 1;
                }
                else
                {
                    $data["user_id"] = $request->session()->get('S_USER_ID');
                    $data["nama"] = $request->session()->get('S_NAMA_LGKP');
                    $data["tanggal"] = date('d-m-Y');
                    $data["jam_masuk"] = date("H:i");
                    $data["jam_keluar"] = date("H:i");
                    $date1 = DateTime::createFromFormat('H:i', date("H:i"));
                    $date2 = DateTime::createFromFormat('H:i', "7:45");
                    if ($date1 > $date2)
                    {
                        $ket = 'Terlambat';
                    }
                    else
                    {
                        $ket = 'Tepat';
                    }
                    $data["keterangan"] = $ket;
                    $data["ip_address"] = $request->session()->get('S_IP_ADDRESS');
                    $data["is_ci"] = 0;

                }

                $menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
                $isakses_kec = Shr::get_give_kec();
                $isakses_kel = Shr::get_give_kel();
                $data = array(
                    "stitle" => 'My Absensi',
                    "user_id" => $request->session()->get('S_USER_ID') ,
                    "user_nik" => $request->session()->get('S_NIK') ,
                    "menu" => $menu,
                    "akses_kec" => $isakses_kec,
                    "akses_kel" => $isakses_kel,
                    "data" => $data,
                    "user_nama_lgkp" => $request->session()->get('S_NAMA_LGKP') ,
                    "user_nama_kantor" => $request->session()->get('S_NAMA_KANTOR') ,
                    "user_nama_dpn" => $request->session()->get('S_NAMA_DPN') ,
                    "user_level" => $request->session()->get('S_USER_LEVEL')
                );
                return view('Absensi_ci/main', $data);
            }
            else
            {
                return redirect()->route('logout');
            }
        }
        else
        {
            return redirect()
                ->route('logout');
        }

    }
    public function Absensi_co(Request $request, $user_id)
    {
        Shr::check_login();
        if (Session::get('S_USER_ID') != null)
        {
            $menu_id = 139;
            $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL') , $menu_id);
            if ($is_akses == 0)
            {
                return redirect()->route('404Notfound');
            }
            if ($user_id != null)
            {
                $user_id = base64_decode($user_id);
                $r = $this->act_report_clock_in($user_id);
                if (count($r) > 0)
                {
                    $r = $r[0];
                    $data["user_id"] = $r->user_id;
                    $data["nama"] = $r->nama;
                    $data["tanggal"] = $r->tanggal;
                    $data["jam_masuk"] = $r->jam_masuk;
                    $data["jam_keluar"] = date("H:i");
                    $data["keterangan"] = $r->keterangan;
                    $data["ip_address"] = $r->ip_address;
                    $data["is_ci"] = 1;
                }
                else
                {
                    $data["user_id"] = $request->session()->get('S_USER_ID');
                    $data["nama"] = $request->session()->get('S_NAMA_LGKP');
                    $data["tanggal"] = date('d-m-Y');
                    $data["jam_masuk"] = date("H:i");
                    $data["jam_keluar"] = date("H:i");

                    $date1 = DateTime::createFromFormat('H:i', date("H:i"));
                    $date2 = DateTime::createFromFormat('H:i', "8:00");
                    if ($date1 > $date2)
                    {
                        $ket = 'Terlambat';
                    }
                    else
                    {
                        $ket = 'Tepat';
                    }
                    $data["keterangan"] = $ket;
                    $data["ip_address"] = $request->session()->get('S_IP_ADDRESS');
                    $data["is_ci"] = 0;

                }

                $menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
                $isakses_kec = Shr::get_give_kec();
                $isakses_kel = Shr::get_give_kel();
                $data = array(
                    "stitle" => 'My Absensi',
                    "user_id" => $request->session()->get('S_USER_ID') ,
                    "user_nik" => $request->session()->get('S_NIK') ,
                    "menu" => $menu,
                    "akses_kec" => $isakses_kec,
                    "akses_kel" => $isakses_kel,
                    "data" => $data,
                    "user_nama_lgkp" => $request->session()->get('S_NAMA_LGKP') ,
                    "user_nama_kantor" => $request->session()->get('S_NAMA_KANTOR') ,
                    "user_nama_dpn" => $request->session()->get('S_NAMA_DPN') ,
                    "user_level" => $request->session()->get('S_USER_LEVEL')
                );
                return view('Absensi_co/main', $data);
            }
            else
            {
                return redirect()->route('logout');
            }
        }
        else
        {
            return redirect()
                ->route('logout');
        }

    }
    public function do_ci(Request $request)
    {
        if ($request->absen_user_id != null)
        {
            $absen_user_id = $request->absen_user_id;
            $absen_nama = $request->absen_nama;
            $absen_tanggal = $request->absen_tanggal;
            $absen_masuk = $request->absen_masuk;
            $absen_keluar = $request->absen_keluar;
            $absen_ket = $request->absen_ket;
            $absen_ip = $request->absen_ip;

            $r = $this->act_report_clock_in($absen_user_id);
            $j = $this->act_report_is_absen($absen_user_id);
            if (count($j) > 0)
            {
                $absen_nama = $j[0]->nama_lgkp;
                if (count($r) == 0)
                {
                    $this->act_do_clockin($absen_user_id, $absen_nama, $absen_tanggal, $absen_masuk, $absen_keluar, $absen_ket, $absen_ip);
                    $data["success"] = true;
                    $data["is_save"] = 0;
                    $data["message"] = "$absen_user_id a Telah Berhasil Clock In.";
                    return $data;
                }
                else
                {
                    $data["success"] = false;
                    $data["is_save"] = 1;
                    $data["message"] = "Anda Sudah Clock In";
                    return $data;
                }
            }
            else
            {
                $data["success"] = false;
                $data["is_save"] = 1;
                $data["message"] = "Anda Tidak Memiliki Hak Akses Untuk Absen";
                return $data;
            }
        }
        else
        {
            return redirect()->route('logout');
        }
    }
    public function do_co(Request $request)
    {
        if ($request->absen_user_id != null)
        {
            $absen_user_id = $request->absen_user_id;
            $absen_tanggal = $request->absen_tanggal;
            $absen_keluar = $request->absen_keluar;
            $r = $this->act_report_clock_in($absen_user_id);
            $j = $this->act_report_is_absen($absen_user_id);
            if (count($j) > 0)
            {
                $absen_nama = $j[0]->nama_lgkp;
                if (count($r) > 0)
                {
                    $this->act_do_clockout($absen_user_id, $absen_keluar, $absen_tanggal);
                    $data["success"] = true;
                    $data["is_save"] = 0;
                    $data["message"] = "$absen_user_id Jam Keluar Telah Di Update, Selamat Beristirahat.";
                    return $data;
                }
                else
                {
                    $data["success"] = false;
                    $data["is_save"] = 1;
                    $data["message"] = "Anda Harus Clock In Terlebih Dahulu";
                    return $data;
                }
            }
            else
            {
                $data["success"] = false;
                $data["is_save"] = 1;
                $data["message"] = "Anda Tidak Memiliki Hak Akses Untuk Absen";
                return $data;
            }
        }
        else
        {
            return redirect()->route('logout');
        }
    }
    public function rekap(Request $request)
    {
        Shr::check_login();
        if (Session::get('S_USER_ID') != null)
        {
            $menu_id = 139;
            $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL') , $menu_id);
            if ($is_akses == 0)
            {
                return redirect()->route('404Notfound');
            }
            $menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
            $isakses_kec = Shr::get_give_kec();
            $isakses_kel = Shr::get_give_kel();
            $data = array(
                "stitle" => 'Rekap My Absensi ',
                "mtitle" => 'Rekap My Absensi ',
                "my_url" => 'UserLevel',
                "type_tgl" => 'Tanggal',
                "menu" => $menu,
                "akses_kec" => $isakses_kec,
                "akses_kel" => $isakses_kel,
                "user_id" => $request->session()->get('S_USER_ID') ,
                "user_nik" => $request->session()->get('S_NIK') ,
                "user_nama_lgkp" => $request->session()->get('S_NAMA_LGKP') ,
                "user_nama_dpn" => $request->session()->get('S_NAMA_DPN') ,
                "user_level" => $request->session()->get('S_USER_LEVEL') ,
                "user_no_kec" => $request->session()->get('S_NO_KEC') ,
                "user_level" => $request->session()->get('S_USER_LEVEL')
            );
            return view('Rekap_absensi/main', $data);
        }
        else
        {
            return redirect()->route('logout');
        }
    }
    public function rekap_daily(Request $request)
    {
        Shr::check_login();
        if (Session::get('S_USER_ID') != null)
        {
            $menu_id = 139;
            $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL') , $menu_id);
            if ($is_akses == 0)
            {
                return redirect()->route('404Notfound');
            }
            $menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
            $isakses_kec = Shr::get_give_kec();
            $isakses_kel = Shr::get_give_kel();
            $data = array(
                "stitle" => 'Rekap My Daily Report ',
                "mtitle" => 'Rekap My Daily Report ',
                "my_url" => 'UserLevel',
                "type_tgl" => 'Tanggal',
                "menu" => $menu,
                "akses_kec" => $isakses_kec,
                "akses_kel" => $isakses_kel,
                "user_id" => $request->session()->get('S_USER_ID') ,
                "user_nik" => $request->session()->get('S_NIK') ,
                "user_nama_lgkp" => $request->session()->get('S_NAMA_LGKP') ,
                "user_nama_dpn" => $request->session()->get('S_NAMA_DPN') ,
                "user_level" => $request->session()->get('S_USER_LEVEL') ,
                "user_no_kec" => $request->session()->get('S_NO_KEC') ,
                "user_level" => $request->session()->get('S_USER_LEVEL')
            );
            return view('Rekap_activity/main', $data);
        }
        else
        {
            return redirect()->route('logout');
        }
    }
    public function Mepeling_monitoring(Request $request)
    {
        Shr::check_login();
        if (Session::get('S_USER_ID') != null)
        {
            $menu_id = 262;
            $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL') , $menu_id);
            if ($is_akses == 0)
            {
                return redirect()->route('404Notfound');
            }
            $menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
            $isakses_kec = Shr::get_give_kec();
            $isakses_kel = Shr::get_give_kel();
            $data = array(
                "stitle" => 'Monitoring Mepeling ',
                "mtitle" => 'Monitoring Mepeling ',
                "my_url" => 'MonitoringMepeling',
                "type_tgl" => 'Mepeling',
                "menu" => $menu,
                "akses_kec" => $isakses_kec,
                "akses_kel" => $isakses_kel,
                "user_id" => $request->session()->get('S_USER_ID') ,
                "user_nik" => $request->session()->get('S_NIK') ,
                "user_nama_lgkp" => $request->session()->get('S_NAMA_LGKP') ,
                "user_nama_dpn" => $request->session()->get('S_NAMA_DPN') ,
                "user_level" => $request->session()->get('S_USER_LEVEL') ,
                "user_no_kec" => $request->session()->get('S_NO_KEC') ,
                "user_level" => $request->session()->get('S_USER_LEVEL')
            );
            return view('Mepeling_monitoring/main', $data);
        }
        else
        {
            return redirect()->route('logout');
        }
    }
    public function Mepeling_wilayah(Request $request)
    {
        Shr::check_login();
        if (Session::get('S_USER_ID') != null)
        {
            $menu_id = 263;
            $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL') , $menu_id);
            if ($is_akses == 0)
            {
                return redirect()->route('404Notfound');
            }
            $menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
            $isakses_kec = Shr::get_give_kec();
            $isakses_kel = Shr::get_give_kel();
            $data = array(
                "stitle" => 'Mepeling Wilayah ',
                "mtitle" => 'Mepeling Wilayah ',
                "my_url" => 'WilayahMepeling',
                "type_tgl" => 'Mepeling',
                "menu" => $menu,
                "akses_kec" => $isakses_kec,
                "akses_kel" => $isakses_kel,
                "user_id" => $request->session()->get('S_USER_ID') ,
                "user_nik" => $request->session()->get('S_NIK') ,
                "user_nama_lgkp" => $request->session()->get('S_NAMA_LGKP') ,
                "user_nama_dpn" => $request->session()->get('S_NAMA_DPN') ,
                "user_level" => $request->session()->get('S_USER_LEVEL') ,
                "user_no_kec" => $request->session()->get('S_NO_KEC') ,
                "user_level" => $request->session()->get('S_USER_LEVEL')
            );
            return view('Mepeling_wilayah/main', $data);
        }
        else
        {
            return redirect()->route('logout');
        }
    }
    public function edit_wil_mpl(Request $request)
    {
        header("Content-Type: application/json", true);
        $no_wil = $request->no_wil;
        $user_id = $request->user_id;
        $r = $this->act_edit_wil_mpl($no_wil,$user_id);
        $output = array(
                    "message_type"=>1,
                    "message"=>"Your Account Has Been Changed Successfully! Thank you."
            );
           return $output;  
    }
    public function activate_mpl(Request $request)
    {
        header("Content-Type: application/json", true);
        $user_id = $request->user_id;
        $r = Shr::activate_user_siak($user_id);
        $output = array(
                    "message_type"=>1,
                    "message"=>"Your Account Has Been Activate Successfully! Thank you."
            );
           return $output;  
    }
    public function deactivate_mpl(Request $request)
    {
        header("Content-Type: application/json", true);
        $user_id = $request->user_id;
        $r = Shr::deactivate_user_siak($user_id);
        $output = array(
                    "message_type"=>1,
                    "message"=>"Your Account Has Been Deactivate Successfully! Thank you."
            );
           return $output;  
    }
    public function laporan_absensi(Request $request)
    {
        Shr::check_login();
        if (Session::get('S_USER_ID') != null)
        {
            $menu_id = 142;
            $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL') , $menu_id);
            if ($is_akses == 0)
            {
                return redirect()->route('404Notfound');
            }
            $menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
            $isakses_kec = Shr::get_give_kec();
            $isakses_kel = Shr::get_give_kel();
            $data = array(
                "stitle" => 'Rekap Laporan Absensi ',
                "mtitle" => 'Rekap Laporan Absensi ',
                "my_url" => 'laporan_absensi',
                "type_tgl" => 'Tanggal',
                "menu" => $menu,
                "akses_kec" => $isakses_kec,
                "akses_kel" => $isakses_kel,
                "user_id" => $request->session()->get('S_USER_ID') ,
                "user_nik" => $request->session()->get('S_NIK') ,
                "user_nama_lgkp" => $request->session()->get('S_NAMA_LGKP') ,
                "user_nama_dpn" => $request->session()->get('S_NAMA_DPN') ,
                "user_level" => $request->session()->get('S_USER_LEVEL') ,
                "user_no_kec" => $request->session()->get('S_NO_KEC') ,
                "user_level" => $request->session()->get('S_USER_LEVEL')
            );
            return view('rekap_laporan_absensi/main', $data);
        }
        else
        {
            return redirect()->route('logout');
        }
    }
    public function detail_absensi(Request $request, $id)
    {
        Shr::check_login();
        if (Session::get('S_USER_ID') != null)
        {
            $menu_id = 142;
            $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL') , $menu_id);
            if ($is_akses == 0)
            {
                return redirect()->route('404Notfound');
            }
            $user_id = base64_decode($id);
            $user_name = Shr::get_user_name($user_id);
            $menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
            $isakses_kec = Shr::get_give_kec();
            $isakses_kel = Shr::get_give_kel();
            $data = array(
                "stitle" => 'Rekap Absensi ' . $user_name,
                "mtitle" => 'Rekap Absensi ' . $user_name,
                "my_url" => 'Detail_absensi',
                "type_tgl" => 'Tanggal',
                "menu" => $menu,
                "akses_kec" => $isakses_kec,
                "akses_kel" => $isakses_kel,
                "check_id" => $user_id,
                "user_id" => $request->session()->get('S_USER_ID') ,
                "user_nik" => $request->session()->get('S_NIK') ,
                "user_nama_lgkp" => $request->session()->get('S_NAMA_LGKP') ,
                "user_nama_dpn" => $request->session()->get('S_NAMA_DPN') ,
                "user_level" => $request->session()->get('S_USER_LEVEL') ,
                "user_no_kec" => $request->session()->get('S_NO_KEC') ,
                "user_level" => $request->session()->get('S_USER_LEVEL')
            );
            return view('rekap_laporan_absensi_detail/main', $data);
        }
        else
        {
            return redirect()->route('logout');
        }
    }
    public function detail_activity(Request $request, $id)
    {
        Shr::check_login();
        if (Session::get('S_USER_ID') != null)
        {
            $menu_id = 142;
            $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL') , $menu_id);
            if ($is_akses == 0)
            {
                return redirect()->route('404Notfound');
            }
            $user_id = base64_decode($id);
            $menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
            $isakses_kec = Shr::get_give_kec();
            $isakses_kel = Shr::get_give_kel();
            $data = array(
                "stitle" => 'Rekap Activit ' . $user_id,
                "mtitle" => 'Rekap Activit ' . $user_id,
                "my_url" => 'Detail_activity',
                "type_tgl" => 'Tanggal',
                "menu" => $menu,
                "akses_kec" => $isakses_kec,
                "akses_kel" => $isakses_kel,
                "check_id" => $user_id,
                "user_id" => $request->session()->get('S_USER_ID') ,
                "user_nik" => $request->session()->get('S_NIK') ,
                "user_nama_lgkp" => $request->session()->get('S_NAMA_LGKP') ,
                "user_nama_dpn" => $request->session()->get('S_NAMA_DPN') ,
                "user_level" => $request->session()->get('S_USER_LEVEL') ,
                "user_no_kec" => $request->session()->get('S_NO_KEC') ,
                "user_level" => $request->session()->get('S_USER_LEVEL')
            );
            return view('rekap_laporan_activity_detail/main', $data);
        }
        else
        {
            return redirect()->route('logout');
        }
    }
    public function laporan_activity(Request $request)
    {
        Shr::check_login();
        if (Session::get('S_USER_ID') != null)
        {
            $menu_id = 143;
            $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL') , $menu_id);
            if ($is_akses == 0)
            {
                return redirect()->route('404Notfound');
            }
            $menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
            $isakses_kec = Shr::get_give_kec();
            $isakses_kel = Shr::get_give_kel();
            $data = array(
                "stitle" => 'Rekap Laporan Activity ',
                "mtitle" => 'Rekap Laporan Activity ',
                "my_url" => 'UserLevel',
                "type_tgl" => 'Tanggal',
                "menu" => $menu,
                "akses_kec" => $isakses_kec,
                "akses_kel" => $isakses_kel,
                "user_id" => $request->session()->get('S_USER_ID') ,
                "user_nik" => $request->session()->get('S_NIK') ,
                "user_nama_lgkp" => $request->session()->get('S_NAMA_LGKP') ,
                "user_nama_dpn" => $request->session()->get('S_NAMA_DPN') ,
                "user_level" => $request->session()->get('S_USER_LEVEL') ,
                "user_no_kec" => $request->session()->get('S_NO_KEC') ,
                "user_level" => $request->session()->get('S_USER_LEVEL')
            );
            return view('rekap_laporan_daily/main', $data);
        }
        else
        {
            return redirect()->route('logout');
        }
    }
    public function absensi_bulanan(Request $request)
    {
        Shr::check_login();
        if (Session::get('S_USER_ID') != null)
        {
            $menu_id = 144;
            $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL') , $menu_id);
            if ($is_akses == 0)
            {
                return redirect()->route('404Notfound');
            }
            $menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
            $isakses_kec = Shr::get_give_kec();
            $isakses_kel = Shr::get_give_kel();
            $data = array(
                "stitle" => 'Rekap Laporan Absensi Bulanan ',
                "mtitle" => 'Rekap Laporan Absensi Bulanan ',
                "my_url" => 'laporan_bulanan',
                "type_tgl" => 'Tanggal',
                "menu" => $menu,
                "akses_kec" => $isakses_kec,
                "akses_kel" => $isakses_kel,
                "user_id" => $request->session()->get('S_USER_ID') ,
                "user_nik" => $request->session()->get('S_NIK') ,
                "user_nama_lgkp" => $request->session()->get('S_NAMA_LGKP') ,
                "user_nama_dpn" => $request->session()->get('S_NAMA_DPN') ,
                "user_level" => $request->session()->get('S_USER_LEVEL') ,
                "user_no_kec" => $request->session()->get('S_NO_KEC') ,
                "user_level" => $request->session()->get('S_USER_LEVEL')
            );
            return view('rekap_bulanan_absensi/main', $data);
        }
        else
        {
            return redirect()->route('logout');
        }
    }
    public function get_daily_harian(Request $request)
    {
        Shr::check_login();
        if (Session::get('S_USER_ID') != null)
        {
            $menu_id = 144;
            $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL') , $menu_id);
            if ($is_akses == 0)
            {
                return redirect()->route('404Notfound');
            }
            $menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
            $isakses_kec = Shr::get_give_kec();
            $isakses_kel = Shr::get_give_kel();
            $data = array(
                "stitle" => 'Rekap Laporan Activity Harian ',
                "mtitle" => 'Rekap Laporan Activity Harian ',
                "my_url" => 'GetDaily',
                "type_tgl" => 'Tanggal',
                "menu" => $menu,
                "akses_kec" => $isakses_kec,
                "akses_kel" => $isakses_kel,
                "user_id" => $request->session()->get('S_USER_ID') ,
                "user_nik" => $request->session()->get('S_NIK') ,
                "user_nama_lgkp" => $request->session()->get('S_NAMA_LGKP') ,
                "user_nama_dpn" => $request->session()->get('S_NAMA_DPN') ,
                "user_level" => $request->session()->get('S_USER_LEVEL') ,
                "user_no_kec" => $request->session()->get('S_NO_KEC') ,
                "user_level" => $request->session()->get('S_USER_LEVEL')
            );
            return view('rekap_harian_activity/main', $data);
        }
        else
        {
            return redirect()->route('logout');
        }
    }
    public function get_daily_harian2(Request $request)
    {
        Shr::check_login();
        if (Session::get('S_USER_ID') != null)
        {
            $menu_id = 144;
            $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL') , $menu_id);
            if ($is_akses == 0)
            {
                return redirect()->route('404Notfound');
            }
            $menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
            $isakses_kec = Shr::get_give_kec();
            $isakses_kel = Shr::get_give_kel();
            $data = array(
                "stitle" => 'Rekap Laporan Activity Bulanan ',
                "mtitle" => 'Rekap Laporan Activity Bulanan ',
                "my_url" => 'GetDaily',
                "type_tgl" => 'Tanggal',
                "menu" => $menu,
                "akses_kec" => $isakses_kec,
                "akses_kel" => $isakses_kel,
                "user_id" => $request->session()->get('S_USER_ID') ,
                "user_nik" => $request->session()->get('S_NIK') ,
                "user_nama_lgkp" => $request->session()->get('S_NAMA_LGKP') ,
                "user_nama_dpn" => $request->session()->get('S_NAMA_DPN') ,
                "user_level" => $request->session()->get('S_USER_LEVEL') ,
                "user_no_kec" => $request->session()->get('S_NO_KEC') ,
                "user_level" => $request->session()->get('S_USER_LEVEL')
            );
            return view('rekap_harian_activity2/main', $data);
        }
        else
        {
            return redirect()->route('logout');
        }
    }
    public function get_rekap_data(Request $request)
    {
        $draw = intval($request->draw);
        $start = intval($request->start);
        $length = intval($request->length);
        $user_id = $request->user_id;
        $rekap_siak = $this->act_get_rekap_absensi($user_id);
        $data = array();
        $i = 0;
        foreach ($rekap_siak as $r)
        {
            $i++;
            $data[] = array(
                $i,
                $r->user_id,
                $r->nama_lgkp,
                $r->day,
                $r->jam_masuk,
                $r->jam_keluar,
                $r->keterangan

            );
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => count($rekap_siak) ,
            "recordsFiltered" => count($rekap_siak) ,
            "data" => $data
        );
        return $output;
        exit();
    }
    public function get_daily_ttd(Request $request)
    {
        $draw = intval($request->draw);
        $start = intval($request->start);
        $length = intval($request->length);
        $user_id = $request->user_id;
        $tgl = $request->tanggal;
        $tgl_start = substr($tgl, 0, 10);
        $tgl_end = substr($tgl, 13, 10);
        $rekap_daily = $this->act_rekap_daily_ttd($user_id, $tgl_start, $tgl_end);
        $data = array();
        $i = 0;
        foreach ($rekap_daily as $r)
        {
            $i++;
            $data[] = array(
                $i,
                $r->user_id,
                $r->tanggal,
                $r->description,
                $r->jumlah . ' Aktivitas'

            );
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => count($rekap_daily) ,
            "recordsFiltered" => count($rekap_daily) ,
            "data" => $data
        );
        return $output;
        exit();
    }
    public function get_rekap_bulan(Request $request)
    {
        $draw = intval($request->draw);
        $start = intval($request->start);
        $length = intval($request->length);
        $tgl = $request->tanggal;
        $rekap_siak = $this->act_get_rekap_absen_bulan($tgl);
        $data = array();
        $i = 0;
        foreach ($rekap_siak as $r)
        {
            $i++;
            $data[] = array(
                $i,
                $r->user_id,
                $r->nama,
                $r->bulan,
                $r->t1,
                $r->t2,
                $r->t3,
                $r->t4,
                $r->t5,
                $r->t6,
                $r->t7,
                $r->t8,
                $r->t9,
                $r->t10,
                $r->t11,
                $r->t12,
                $r->t13,
                $r->t14,
                $r->t15,
                $r->t16,
                $r->t17,
                $r->t18,
                $r->t19,
                $r->t20,
                $r->t21,
                $r->t22,
                $r->t23,
                $r->t24,
                $r->t25,
                $r->t26,
                $r->t27,
                $r->t28,
                $r->t29,
                $r->t30,
                $r->t31,
                $r->kehadiran,
                $r->tepat_waktu,
                $r->terlambat

            );
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => count($rekap_siak) ,
            "recordsFiltered" => count($rekap_siak) ,
            "data" => $data
        );
        return $output;
        exit();
    }
    public function get_rekap_daily(Request $request)
    {

        $draw = intval($request->draw);
        $start = intval($request->start);
        $length = intval($request->length);
        $user_id = $request->user_id;
        $rekap_daily = $this->act_daily_rekap($user_id);
        $data = array();
        $i = 0;
        foreach ($rekap_daily as $r)
        {
            $i++;
            $data[] = array(
                $i,
                $r->user_id,
                $r->tanggal,
                $r->activity,
                $r->description,
                $r->start_activity . '-' . $r->end_activity,
                $r->number_activity,
                $r->minute_activity,
                '<button type="button" class="btn btn-danger btn-xs" id="btn-delete" onclick="hapus(' . $r->daily_id . ');" style="margin-top: 5px;">Delete <i class="mdi mdi-delete-forever fa-fw"></i></button>'

            );
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => count($rekap_daily) ,
            "recordsFiltered" => count($rekap_daily) ,
            "data" => $data
        );
        return $output;
        exit();
    }
    public function get_rekap_daily_tgl(Request $request)
    {

        $draw = intval($request->draw);
        $start = intval($request->start);
        $length = intval($request->length);
        $user_id = $request->user_id;
        $tgl = $request->tanggal;
        $tgl_start = substr($tgl, 0, 10);
        $tgl_end = substr($tgl, 13, 10);
        $rekap_daily = $this->act_daily_rekap_tgl($user_id, $tgl_start, $tgl_end);
        $data = array();
        $i = 0;
        foreach ($rekap_daily as $r)
        {
            $i++;
            $data[] = array(
                $i,
                $r->user_id,
                $r->tanggal,
                $r->activity,
                $r->description,
                $r->start_activity . '-' . $r->end_activity,
                $r->number_activity,
                $r->minute_activity,
                '<button class="btn btn-danger btn-xs" onclick="hapus(' . $r->daily_id . ');">Delete <i class="mdi mdi-delete-forever fa-fw"></i></button>'

            );
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => count($rekap_daily) ,
            "recordsFiltered" => count($rekap_daily) ,
            "data" => $data
        );
        return $output;
        exit();
    }
    public function get_rekap_absen(Request $request)
    {

        $draw = intval($request->draw);
        $start = intval($request->start);
        $length = intval($request->length);
        $tgl = $request->tanggal;
        $rekap_daily = $this->act_rekap_absensi($tgl);
        $data = array();
        $i = 0;
        foreach ($rekap_daily as $r)
        {
            $i++;
            $data[] = array(
                $i,
                $r->user_id,
                $r->nama_lgkp,
                $r->keterangan,
                $r->jumlah_hadir . ' Hari',
                $r->tepat_waktu . ' Hari',
                $r->terlambat . ' Hari',
                $r->excuse . ' Hari',
                $r->minute_work . ' Menit',
                '<a target="_blank" href="LaporanAbsen/Lihat/' . base64_encode($r->user_id) . '"><button type="button" class="btn btn-info btn-xs" id="btn-detail" style="margin-top: 5px;">Detail <i class="mdi mdi-details fa-fw"></i></button></a>'

            );
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => count($rekap_daily) ,
            "recordsFiltered" => count($rekap_daily) ,
            "data" => $data
        );
        return $output;
        exit();
    }
    public function get_rekap_activity(Request $request)
    {

        $draw = intval($request->draw);
        $start = intval($request->start);
        $length = intval($request->length);
        $tgl = $request->tanggal;
        $rekap_daily = $this->act_rekap_daily($tgl);
        $data = array();
        $i = 0;
        foreach ($rekap_daily as $r)
        {
            $i++;
            $data[] = array(
                $i,
                $r->user_id,
                $r->nama_lgkp,
                $r->aktivitas . ' Kriteria',
                $r->task_per_aktivitas . ' Task',
                $r->jumlah_aktivitas . ' Aktivitas',
                '<button  class="btn btn-info btn-xs" onclick="lihat(\'' . base64_encode($r->user_id) . '\');"><i class="fa fa-desktop  fa-fw" id=></i> Detail</button>'

            );
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => count($rekap_daily) ,
            "recordsFiltered" => count($rekap_daily) ,
            "data" => $data
        );
        return $output;
        exit();
    }
    public function get_rekap_absen_detail(Request $request)
    {

        $draw = intval($request->draw);
        $start = intval($request->start);
        $length = intval($request->length);
        $tgl = $request->tanggal;
        $user_id = $request->user_id;
        $res = $this->act_get_rekap_absensi_detail($tgl, $user_id);
        $data = array();
        $i = 0;
        foreach ($res as $r)
        {
            $i++;
            $data[] = array(
                $i,
                $r->user_id,
                $r->nama_lgkp,
                $r->day,
                $r->jam_masuk,
                $r->jam_keluar,
                $r->keterangan

            );
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => count($res) ,
            "recordsFiltered" => count($res) ,
            "data" => $data
        );
        return $output;
        exit();
    }
    public function get_rekap_daily_detail(Request $request)
    {

        $draw = intval($request->draw);
        $start = intval($request->start);
        $length = intval($request->length);
        $tgl = $request->tanggal;
        $user_id = $request->user_id;
        $res = $this->act_get_rekap_activity_detail($tgl, $user_id);
        $data = array();
        $i = 0;
        foreach ($res as $r)
        {
            $i++;
            $data[] = array(
                $i,
                $r->user_id,
                $r->tanggal,
                $r->activity,
                $r->description,
                $r->start_activity . '-' . $r->end_activity,
                $r->number_activity

            );
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => count($res) ,
            "recordsFiltered" => count($res) ,
            "data" => $data
        );
        return $output;
        exit();
    }
    public function option_excuse(Request $request)
    {
        header("Content-Type: application/json", true);
        $r = $this->act_excuse_option();
        return $r;
    }
    public function option_daily(Request $request)
    {
        header("Content-Type: application/json", true);
        $user_id = $request->user_id;
        $r = $this->act_daily_option($user_id);
        return $r;
    }
    public function excuse(Request $request)
    {
        header("Content-Type: application/json", true);
        if ($request->get_in != null)
        {
            $user_id = $request->user_id;
            $tanggal_absen = $request->tanggal_absen;
            $r = $this->act_check_excuse($user_id, $tanggal_absen);
            if ($r > 0)
            {
                $r2 = $this->act_excuse_getdate($user_id, $tanggal_absen);
                $clock_in = $r2[0]->jam_masuk;
                $clock_out = $r2[0]->jam_keluar;
                $output = array(
                    "message_type" => 1,
                    "clock_in" => $clock_in,
                    "clock_out" => $clock_out,
                    "message" => '',
                    "sql" => ''
                );
            }
            else
            {
                $output = array(
                    "message_type" => 2,
                    "clock_in" => "00:00",
                    "clock_out" => "00:00",
                    "message" => "Maaf Pada Tanggal $tanggal_absen Anda Tidak Clock-In Atau Clock Out",
                    "sql" => "SQL"
                );
            }
            return $output;
        }
        else if ($request->get_date != null)
        {
            $tanggal_absen = $request->tanggal_absen;
            $r = $this->act_cekdate($tanggal_absen);
            if ($r > 0)
            {
                $output = array(
                    "message_type" => 1,
                    "message" => "Anda Tidak Bisa Mengajukan Excuse Lebih Dari Hari Ini"
                );
            }
            else
            {
                $output = array(
                    "message_type" => 2,
                    "message" => "Okay Dude"
                );
            }
            return $output;
        }
        else if ($request->get_out != null)
        {
            $user_id = $request->user_id;
            $tanggal_absen = $request->tanggal_absen;
            $r = $this->act_check_excuse($user_id, $tanggal_absen);
            if ($r > 0)
            {
                $r2 = $this->act_excuse_getdate($user_id, $tanggal_absen);
                $clock_in = $r2[0]->jam_masuk;
                $clock_out = $r2[0]->jam_keluar;
                $output = array(
                    "message_type" => 1,
                    "clock_in" => $clock_in,
                    "clock_out" => $clock_out,
                    "message" => '',
                    "sql" => ''
                );
            }
            else
            {
                $output = array(
                    "message_type" => 2,
                    "clock_in" => "00:00",
                    "clock_out" => "00:00",
                    "message" => "Maaf Pada Tanggal $tanggal_absen Anda Tidak Clock-In Atau Clock Out",
                    "sql" => "SQL"
                );
            }
            return $output;
        }
        else if ($request->get_absen != null)
        {
            $user_id = $request->user_id;
            $tanggal_absen = $request->tanggal_absen;
            $r = $this->act_check_excuse($user_id, $tanggal_absen);
            if ($r > 0)
            {
                $r2 = $this->act_excuse_getdate($user_id, $tanggal_absen);
                $clock_in = $r2[0]->jam_masuk;
                $clock_out = $r2[0]->jam_keluar;
                $output = array(
                    "message_type" => 1,
                    "clock_in" => $clock_in,
                    "clock_out" => $clock_out,
                    "message" => "Maaf Pada Tanggal $tanggal_absen Anda Telah Clock-In Atau Clock Out",
                    "sql" => ''
                );
            }
            else
            {
                $output = array(
                    "message_type" => 2,
                    "clock_in" => "08:00",
                    "clock_out" => "17:00",
                    "message" => "-",
                    "sql" => "-"
                );
            }
            return $output;
        }
        else if ($request->get_excuse != null)
        {
            $user_id = $request->user_id;
            $tanggal_absen = $request->tanggal_absen;
            $r = $this->act_check_excuse($user_id, $tanggal_absen);
            if ($r > 0)
            {
                $r2 = $this->act_excuse_getdate($user_id, $tanggal_absen);
                $clock_in = $r2[0]->jam_masuk;
                $clock_out = $r2[0]->jam_keluar;
                $output = array(
                    "message_type" => 1,
                    "clock_in" => $clock_in,
                    "clock_out" => $clock_out,
                    "message" => "Maaf Pada Tanggal $tanggal_absen Anda Telah Clock-In Atau Clock Out",
                    "sql" => ''
                );
            }
            else
            {
                $output = array(
                    "message_type" => 2,
                    "clock_in" => "08:00",
                    "clock_out" => "17:00",
                    "message" => "-",
                    "sql" => "-"
                );
            }
            return $output;
        }
        else if ($request->excuse_save != null)
        {
            $user_id = $request->user_id;
            $nama_lgkp = $request->nama_lgkp;
            $excuse_tanggal = $request->excuse_tanggal;
            $excuse_keterangan = $request->excuse_keterangan;
            $excuse_ci = $request->excuse_ci;
            $excuse_co = $request->excuse_co;
            $descrip = ucwords(strtolower($this->act_get_des_excuse($excuse_keterangan)));
            $j = $this->act_report_is_absen($user_id);
            if (count($j) > 0)
            {
                if ($excuse_keterangan == 1 || $excuse_keterangan == 4 || $excuse_keterangan == 5 || $excuse_keterangan == 6)
                {
                    $this->act_insert_excuse($user_id, $nama_lgkp, $excuse_tanggal, $excuse_ci, $excuse_co, $descrip, $request->session()->get('S_IP_ADDRESS'));
                    $output = array(
                        "message_type" => 1,
                        "message" => "Data Berhasil Di Insert",
                        "SQL" => ""
                    );
                }
                else if ($excuse_keterangan == 2 || $excuse_keterangan == 3)
                {
                    $this->act_update_excuse($user_id, $nama_lgkp, $excuse_tanggal, $excuse_ci, $excuse_co, $descrip, $request->session()->get('S_IP_ADDRESS'));
                    $output = array(
                        "message_type" => 1,
                        "message" => "Data Berhasil Di Update",
                        "SQL" => ""

                    );
                }
                else
                {
                    $output = array(
                        "message_type" => 2,
                        "message" => "Maaf Terjadi Kesalahan"
                    );
                }
            }
            else
            {
                $output = array(
                    "message_type" => 2,
                    "message" => "Maaf Anda Tidak Memiliki Hak Akses Untuk Absen"
                );
            }
            return $output;
        }
        else
        {
            return redirect()->route('logout');
        }
    }
    public function daily(Request $request)
    {
        header("Content-Type: application/json", true);
        if ($request->daily_save != null)
        {
            $aktivitas = $request->aktivitas;
            $tanggal_report = $request->tanggal_report;
            $jumlah = $request->jumlah;
            $daily_dari = $request->daily_dari;
            $daily_sampai = $request->daily_sampai;
            $daily_keterangan = $this->clean($request->daily_keterangan);
            $user_id = $request->user_id;
            $today = date('d-m-Y');
            $r = $this->act_check_daily($user_id, $aktivitas, $tanggal_report);
            if ($r == 0)
            {
                $this->act_insert_daily($aktivitas, $user_id, $daily_dari, $daily_sampai, $jumlah, $daily_keterangan, $tanggal_report);
                $output = array(
                    "message_type" => 1,
                    "aktivitas" => $aktivitas,
                    "message" => "Terima Kasih Sudah Mengisi Laporan Harian <span class='fa fa-smile-o'></span>"
                );
            }
            else
            {
                $output = array(
                    "message_type" => 2,
                    "aktivitas" => $aktivitas,
                    "message" => "Maaf Anda Tidak Bisa Mengisi Aktivitas Yang Sama Di Hari Yang Sama <span class='fa fa-meh-o'></span>"
                );
            }
            return $output;
        }
        else if ($request->get_date != null)
        {
            $tanggal_report = $request->tanggal_report;
            $r = $this->act_cekdate($tanggal_report);
            if ($r > 0)
            {
                $output = array(
                    "message_type" => 1,
                    "message" => "Anda Tidak Bisa Membuat Daily Report Lebih Dari Hari Ini"
                );
            }
            else
            {
                $output = array(
                    "message_type" => 2,
                    "message" => "Okay Dude"
                );
            }
            return $output;
        }
        else if ($request->do_delete != null)
        {
            $activity_delete = $request->activity_delete;
            $this->act_delete_daily($activity_delete);
            $output = array(
                "message_type" => 2,
                "aktivitas" => "",
                "message" => "Activitas Sudah Di Delete <span class='fa fa-smile-o'></span>"
            );
            return $output;
        }

    }
    public function delete_daily_id(Request $request)
    {
        header('Content-type: application/json');
        if ($request->daily_id != null)
        {
            $daily_id = $request->daily_id;
            $this->act_delete_daily($daily_id);
            $output = array(
                "message_type" => 1,
                "message" => "Aktivity Ini Telah Dihapus !"
            );
            return $output;
        }
    }
    public function get_daily_harian_table(Request $request)
    {
        header("Content-Type: application/json", true);
        $user_id = $request->user_id;
        $tgl = $request->tanggal;
        $tgl_start = substr($tgl, 0, 10);
        $tgl_end = substr($tgl, 13, 10);
        $r = $this->act_rekap_daily_ttd_table($user_id, $tgl_start, $tgl_end);
        return $r;
    }
    public function get_daily_harian_table2(Request $request)
    {
        header("Content-Type: application/json", true);
        $user_id = $request->user_id;
        $tgl = $request->tanggal;
        $r = $this->act_rekap_daily_ttd_table2($user_id, $tgl);
        return $r;
    }
    public function do_reset(Request $request){
        header('Content-type: application/json');
        if ($request->user_name != null){
            $user_name = $request->user_name;
            $old_password = md5($request->old_password);
            $new_password = md5($request->new_password);
            $this->lgn_change_pwd($user_name, $new_password);
            $output = array(
                    "message_type"=>1,
                    "message"=>"Your Password Has Been Changed Successfully! Thank you."
            );
           return $output;  
        }
    }

    public function get_rekap_mepeling_tgl(Request $request)
    {

        $draw = intval($request->draw);
        $start = intval($request->start);
        $length = intval($request->length);
        $tgl = $request->tanggal;
        $tgl_start = substr($tgl, 0, 10);
        $tgl_end = substr($tgl, 13, 10);
        $jns_seksi = $request->jns_seksi;
        $user_id = $request->user_id;
        if($jns_seksi == 1){
            $mepeling = $this->act_mepeling_lhr($user_id, $tgl_start, $tgl_end);
            $ket = ' Akta Kelahiran';
        }else{
            $mepeling = $this->act_mepeling_kmt($user_id, $tgl_start, $tgl_end);
            $ket = ' Akta Kematian';
        }
        
        $data = array();
        $i = 0;
        foreach ($mepeling as $r)
        {
            $i++;
            $data[] = array(
                $i,
                $r->nama_lgkp,
                $tgl,
                $r->nama_wil,
                $r->jml . $ket,
               
            );
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => count($mepeling) ,
            "recordsFiltered" => count($mepeling) ,
            "data" => $data
        );
        return $output;
        exit();
    }

    public function get_seting_user_mpl(Request $request)
    {
        $lvl = Shr::do_get_lvl_lhr_mpl();
        $draw = intval($request->draw);
        $start = intval($request->start);
        $length = intval($request->length);
        $mepeling = Shr::do_get_user_by_lvl($lvl);
        
        $data = array();
        $i = 0;
        foreach ($mepeling as $r)
        {
            $i++;
            $data[] = array(
                $i,
                $r->user_id,
                $r->nama_lgkp,
                $r->nama_prop,
                $r->nama_kab,
                $r->nama_kec,
                $r->level_name,
                ($r->status == 1) ? '<b class="text-success"> ACTIVE </b>' : '<b class="text-danger"> INACTIVE </b>',
                ($r->is_online == 'ONLINE') ? '<b class="text-success"> ONLINE </b>' : '<b class="text-danger"> OFFLINE </b>',
                ($r->status == 1) ? '<button type="button" class="btn btn-primary btn-xs" id="btn-ubah" onclick="akses(\''.$r->user_id.'\');" style="margin-top: 5px;">Ubah Wilayah <i class="mdi  mdi-pencil fa-fw"></i></button> &nbsp <button type="button" class="btn btn-danger btn-xs" id="btn-active" onclick="deactivate(\''.$r->user_id.'\');" style="margin-top: 5px;">Deactivate <i class="mdi  mdi-power fa-fw"></i></button>' : '<button type="button" class="btn btn-primary btn-xs" id="btn-ubah" onclick="akses(\''.$r->user_id.'\');" style="margin-top: 5px;">Ubah Wilayah <i class="mdi  mdi-pencil fa-fw"></i></button> &nbsp <button type="button" class="btn btn-success btn-xs" id="btn-deactive" onclick="activate(\''.$r->user_id.'\');" style="margin-top: 5px;">Activate <i class="mdi  mdi-power fa-fw"></i></button>'
               
            );
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => count($mepeling) ,
            "recordsFiltered" => count($mepeling) ,
            "data" => $data
        );
        return $output;
        exit();
    }
    public function salaman_batal(Request $request)
    {
        Shr::check_login();
        if (Session::get('S_USER_ID') != null)
        {
            $menu_id = 272;
            $is_akses = Shr::cek_is_akses($request->session()->get('S_USER_LEVEL') , $menu_id);
            if ($is_akses == 0)
            {
                redirect('404Notfound', 'refresh');
            }
            $menu = Shr::get_menu($request->session()->get('S_USER_LEVEL'));
            $isakses_kec = Shr::get_give_kec();
            $isakses_kel = Shr::get_give_kel();
            $data = array(
                "stitle" => 'Perbaikan Pembatalan Salaman',
                "mtitle" => 'Perbaikan Pembatalan Salaman',
                "my_url" => 'insert_akta',
                "type_tgl" => 'Tanggal',
                "menu" => $menu,
                "akses_kec" => $isakses_kec,
                "akses_kel" => $isakses_kel,
                "user_id" => $request->session()->get('S_USER_ID') ,
                "user_nik" => $request->session()->get('S_NIK') ,
                "user_nama_lgkp" => $request->session()->get('S_NAMA_LGKP') ,
                "user_nama_dpn" => $request->session()->get('S_NAMA_DPN') ,
                "user_no_kel" => $request->session()->get('S_NO_KEL') ,
                "user_no_kec" => $request->session()->get('S_NO_KEC') ,
                "user_level" => $request->session()->get('S_USER_LEVEL') ,
            );
            return view('Salaman_pembatalan/main', $data);
        }
        else
        {
            return redirect()->route('logout');
        }
    }
    public function get_salaman_batal(Request $request)
    {
        $draw = intval($request->draw);
        $start = intval($request->start);
        $length = intval($request->length);
        if (!empty($request->nik))
        {
            $nik = (!empty($request->nik)) ? $request->nik : '';
            $salaman = $this->act_get_salaman_batal($nik);
            $data = array();
            foreach ($salaman as $r)
            {
                $data[] = array(
                    '<span style="color: #fff">,</span>' . $r->id,
                    '<span style="color: #fff">,</span>' . $r->nik,
                    $r->nama_lgkp,
                    $r->telepon,
                    $r->email,
                    $r->verified_at,
                    $r->verified_by,
                    '<button type="button" class="btn btn-info waves-effect waves-light m-r-10" id="btn-perbaikan" onclick="on_edit(' . $r->id . ');">Perbaiki Status <i class="mdi  mdi-sync-alert fa-fw"></i></button>'
                );
            }
            $output = array(
                "draw" => $draw,
                "recordsTotal" => count($salaman) ,
                "recordsFiltered" => count($salaman) ,
                "data" => $data
            );
            return $output;
            exit();
        }
        else
        {
            $data = array();
            $output = array(
                "draw" => $draw,
                "recordsTotal" => 0,
                "recordsFiltered" => 0,
                "data" => $data
            );
            return $output;
            exit();
        }

    }
    public function edit_pengajuan_batal(Request $request)
    {
        header("Content-Type: application/json", true);
        $pengajuan_id = $request->pengajuan_id;
        $r = $this->act_edit_pengajuan_batal($pengajuan_id);
        $output = array(
                    "message_type"=>1,
                    "message"=>"Pengajuan Berhasil Di Perbaiki! Thank you."
            );
           return $output;  
    }
    public function do_pdf(Request $request)
    {
        if ($request->pdf_tanggal != null)
        {
            $pdf_userid = $request->session()->get('S_USER_ID');
            $tgl = $request->pdf_tanggal;
            $tgl_start = substr($tgl, 0, 10);
            $tgl_end = substr($tgl, 13, 10);

            $fileName = strtoupper($request->session()->get('S_USER_ID')) . '_activity_data_' . time() . '.xlsx';
            $activity_data = $this->act_rekap_daily_ttd_pdf($pdf_userid, $tgl_start, $tgl_end);
            $atasan = $this->act_get_atasan($pdf_userid);
            // set document information
            /*PDF::setcreator('Dian Yanzen');
            PDF::setauthor('Dian Yanzen');
            PDF::settitle('PDF Monitoring');
            PDF::setsubject('YZ PDF');
            PDF::setkeywords('YZ, PDF, Monitoring, DisdukCapil, Kota Bandung');
            // set auto page breaks
            PDF::setautopagebreak(true, PDF_MARGIN_BOTTOM);

            // set image scale factor
            PDF::setimagescale(PDF_IMAGE_SCALE_RATIO);

            // ---------------------------------------------------------
            // set font
            // set default header data
            PDF::setmargins(PDF_MARGIN_LEFT, 45, PDF_MARGIN_RIGHT);
            PDF::setheadermargin(PDF_MARGIN_HEADER);
            PDF::setfootermargin(PDF_MARGIN_FOOTER);
            // set font
            PDF::setfont('times', '', 12);
            PDF::addpage();*/
            // add a page
            $html = '
                <!-- EXAMPLE OF CSS STYLE -->
                <style>

                    table.first {
                        font-family: helvetica;
                        font-size: 7pt;
                        
                    }
                </style>
                <table class="first" cellpadding="0" cellspacing="5" border="0">
                 <tr>
                  <td width="50" align="left">Nama</td>
                  <td width="20" align="center">:</td>
                  <td width="140" align="left"><b>' . $request->session()->get('S_NAMA_LGKP') . '</b></td>
                  <td width="80" align="center"></td>
                  <td width="60" align="center"></td>
                  <td width="20" align="center"></td>
                  <td width="220" align="left"></td>
                 </tr>
                 <tr>
                  <td width="50" align="left">Nik</td>
                  <td width="20" align="center">:</td>
                  <td width="140" align="left">' . $request->session()->get('S_NIK') . '</td>
                  <td width="80" align="center"></td>
                  <td width="60" align="center"></td>
                  <td width="20" align="center"></td>
                  <td width="220" align="left"></td>
                 </tr>
                 <tr>
                  <td width="50" align="left">Perihal</td>
                  <td width="20" align="center">:</td>
                  <td width="210" align="left">Laporan Pelaksanaan Tugas</td>
                  <td width="10" align="center"></td>
                  <td width="60" align="right"></td>
                  <td width="20" align="center"></td>
                  <td width="220" align="left"></td>
                 </tr>
                 <tr>
                  <td width="50" align="left">Tanggal</td>
                  <td width="20" align="center">:</td>
                  <td width="210" align="left">' . $tgl . '</td>
                  <td width="10" align="center"></td>
                  <td width="60" align="right"></td>
                  <td width="20" align="center"></td>
                  <td width="220" align="left"></td>
                 </tr>
                </table>
                <br>
                <br>
                <table id="demo-foo-row-toggler" class="first" data-page-size="100" cellpadding="10" style="margin-left: auto; margin-right: auto; border: 1px solid #e4e7ea; width:700px;">
                                                        <thead>
                                            <tr>
                                                <th width="5%" style="border: 2px solid #e4e7ea; text-align: center;">No</th>
                                                <th width="15%" style="border: 2px solid #e4e7ea;text-align: center;">Tanggal</th>
                                                <th width="40%" style="border: 2px solid #e4e7ea;text-align: center;">Aktivitas</th>
                                                <th width="20%" style="border: 2px solid #e4e7ea;text-align: center;">Jumlah Aktivitas</th>
                                                <th width="20%" style="border: 2px solid #e4e7ea;text-align: center;">Paraf</th>
                                            </tr>
                                        </thead>
                                        <tbody id="my_data" style="border: 1px solid #e4e7ea;">
';
            
            $i = 0;
            foreach ($activity_data as $val)
            {
                // new style
                $i++;
                // QRCODE,H : QR-CODE Best error correction
                

                // print a line using Cell()
                // PDF::cell(0, 12, strtoupper($request->session()->get('S_USER_ID')).'_activity_data_'.time(), 0, 1, 'C');
                $html .= '

 <tr style="border: 1px solid #e4e7ea;">
<td width="5%" valign="center" style="border: 1px solid #e4e7ea; text-align: center;">' . $i . '</td>
<td width="15%" valign="center" style="border: 1px solid #e4e7ea; text-align: center;">' . $val->tanggal . '</td>
<td width="40%" valign="center" style="border: 1px solid #e4e7ea; text-align: left;">' . str_replace("//line", "<br/>", $val->desx) . '<br /></td>
<td width="20%" valign="center" style="border: 1px solid #e4e7ea; text-align: left;">' . str_replace("//line", "<br/>", $val->jumlah) . ' Aktivitas <br /></td>
<td width="20%" valign="center" style="border: 1px solid #e4e7ea; text-align: center;"></td>
</tr>
 
   
';
                
            }
            $html .= '
        </tbody>
                                    </table>
                                    <table class="first" cellpadding="0" cellspacing="5" border="0" style="margin-top : 10px;">

        
    </table>
';
            // PDF::writehtml($html, true, false, true, false, '');
            // //Close and output PDF document
            // PDF::output(strtoupper($request->session()->get('S_USER_ID')) . '_activity_data_' . time() . '.pdf', 'I');
            return  PDF::loadHTML($html)->setPaper('a4', 'portraits')
          ->setWarnings(false)
          ->stream(strtoupper($request->session()->get('S_USER_ID')).'_ACTIVITY_'.time().'.pdf');
        }
        else
        {
            return redirect()
                ->route('logout');
        }
    }
    public function do_pdf2(Request $request)
    {
        if ($request->pdf_tanggal != null)
        {
            $pdf_userid = $request->session()->get('S_USER_ID');
            $tgl = $request->pdf_tanggal;

            $fileName = strtoupper($request->session()->get('S_USER_ID')) . '_activity_data_' . time() . '.xlsx';
            $activity_data = $this->act_rekap_daily_ttd_pdf2($pdf_userid, $tgl);
            $atasan = $this->act_get_atasan($pdf_userid);
            // set document information
            /*PDF::setcreator('Dian Yanzen');
            PDF::setauthor('Dian Yanzen');
            PDF::settitle('PDF Monitoring');
            PDF::setsubject('YZ PDF');
            PDF::setkeywords('YZ, PDF, Monitoring, DisdukCapil, Kota Bandung');
            // set auto page breaks
            PDF::setautopagebreak(true, PDF_MARGIN_BOTTOM);

            // set image scale factor
            PDF::setimagescale(PDF_IMAGE_SCALE_RATIO);

            // ---------------------------------------------------------
            // set font
            // set default header data
            PDF::setmargins(PDF_MARGIN_LEFT, 45, PDF_MARGIN_RIGHT);
            PDF::setheadermargin(PDF_MARGIN_HEADER);
            PDF::setfootermargin(PDF_MARGIN_FOOTER);
            // set font
            PDF::setfont('times', '', 12);
            PDF::addpage();*/
            // add a page
            $html = '
                <!-- EXAMPLE OF CSS STYLE -->
                <style>

                    table.first {
                        font-family: helvetica;
                        font-size: 7pt;
                        
                    }
                </style>
                <table class="first" cellpadding="0" cellspacing="5" border="0">
                 <tr>
                  <td width="50" align="left">Nama</td>
                  <td width="20" align="center">:</td>
                  <td width="140" align="left"><b>' . $request->session()->get('S_NAMA_LGKP') . '</b></td>
                  <td width="80" align="center"></td>
                  <td width="60" align="center"></td>
                  <td width="20" align="center"></td>
                  <td width="220" align="left"></td>
                 </tr>
                 <tr>
                  <td width="50" align="left">Nik</td>
                  <td width="20" align="center">:</td>
                  <td width="140" align="left">' . $request->session()->get('S_NIK') . '</td>
                  <td width="80" align="center"></td>
                  <td width="60" align="center"></td>
                  <td width="20" align="center"></td>
                  <td width="220" align="left"></td>
                 </tr>
                 <tr>
                  <td width="50" align="left">Perihal</td>
                  <td width="20" align="center">:</td>
                  <td width="210" align="left">Laporan Pelaksanaan Tugas</td>
                  <td width="10" align="center"></td>
                  <td width="60" align="right"></td>
                  <td width="20" align="center"></td>
                  <td width="220" align="left"></td>
                 </tr>
                 <tr>
                  <td width="50" align="left">Bulan</td>
                  <td width="20" align="center">:</td>
                  <td width="210" align="left">' . $this->tgl_indo($tgl) . '</td>
                  <td width="10" align="center"></td>
                  <td width="60" align="right"></td>
                  <td width="20" align="center"></td>
                  <td width="220" align="left"></td>
                 </tr>
                </table>
                <br>
                <br>
                <table id="demo-foo-row-toggler" class="first" data-page-size="100" cellpadding="10" style="margin-left: auto; margin-right: auto; border: 1px solid #e4e7ea; width:700px;">
                                                        <thead>
                                            <tr>
                                                <th width="5%" style="border: 2px solid #e4e7ea; text-align: center;">No</th>
                                                <th width="15%" style="border: 2px solid #e4e7ea;text-align: center;">Tanggal</th>
                                                <th width="40%" style="border: 2px solid #e4e7ea;text-align: center;">Aktivitas</th>
                                                <th width="20%" style="border: 2px solid #e4e7ea;text-align: center;">Jumlah Aktivitas</th>
                                                <th width="20%" style="border: 2px solid #e4e7ea;text-align: center;">Paraf</th>
                                            </tr>
                                        </thead>
                                        <tbody id="my_data" style="border: 1px solid #e4e7ea;">';
            
            $i = 0;
            foreach ($activity_data as $val)
            {
                // new style
                $i++;
                // QRCODE,H : QR-CODE Best error correction
                

                // print a line using Cell()
                // PDF::cell(0, 12, strtoupper($request->session()->get('S_USER_ID')).'_activity_data_'.time(), 0, 1, 'C');
                $html .= '

 <tr style="border: 1px solid #e4e7ea;">
<td width="5%" valign="center" style="border: 1px solid #e4e7ea; text-align: center;">' . $i . '</td>
<td width="15%" valign="center" style="border: 1px solid #e4e7ea; text-align: center;">' . $val->tanggal . '</td>
<td width="40%" valign="center" style="border: 1px solid #e4e7ea; text-align: left;">' . str_replace("//line", "<br/>", $val->desx) . '<br /></td>
<td width="20%" valign="center" style="border: 1px solid #e4e7ea; text-align: left;">' . str_replace("//line", "<br/>", $val->jumlah) . ' Aktivitas <br /></td>
<td width="20%" valign="center" style="border: 1px solid #e4e7ea; text-align: center;"></td>
</tr>
 
   
';
                
            }
            $html .= '
        </tbody>
                                    </table>
                                    <table class="first" cellpadding="0" cellspacing="5" border="0" style="margin-top : 10px;">

        
    </table>
';
            // PDF::writehtml($html, true, false, true, false, '');
            // //Close and output PDF document
            // PDF::output(strtoupper($request->session()->get('S_USER_ID')) . '_activity_data_' . time() . '.pdf', 'I');
            return  PDF::loadHTML($html)->setPaper('a4', 'portraits')
          ->setWarnings(false)
          ->stream(strtoupper($request->session()->get('S_NAMA_LGKP')).' '.strtoupper($this->tgl_indo($tgl)).'.pdf');
        }
        else
        {
            return redirect()
                ->route('logout');
        }
    }
    public function clean($string)
    {
        $string = str_replace(' ', ' ', $string); // Replaces all spaces with hyphens.
        return preg_replace('/[^A-Za-z0-9\- ,.]/', '', $string); // Removes special chars.
        
    }
    public function act_is_online($user_id)
    {
        $sql = "SELECT A.USER_ID,CASE WHEN A.USER_SIAK IS NULL THEN '-' ELSE A.USER_SIAK END AS USER_SIAK,CASE WHEN TO_CHAR(B.LAST_ACTIVITY,'DD-MM-YYYY HH24:MI:SS') IS NULL THEN '-' ELSE TO_CHAR(B.LAST_ACTIVITY,'DD-MM-YYYY HH24:MI:SS') END AS MONEV_LAST, CASE WHEN B.IP_ADDRESS IS NULL THEN '-' ELSE B.IP_ADDRESS END AS MONEV_IP,CASE WHEN C.LAST_ACTIVITY IS NULL THEN '-' ELSE TO_CHAR(TO_DATE('1970-01-01','YYYY-MM-DD') + NUMTODSINTERVAL(TO_CHAR(C.LAST_ACTIVITY),'SECOND') + 7/24,'DD-MM-YYYY HH24:MI:SS') END AS SIAK_LAST,CASE WHEN C.IP_ADDRESS IS NULL THEN '-' ELSE C.IP_ADDRESS END AS  SIAK_IP FROM SIAK_USER_PLUS A LEFT JOIN SIAK_SESSION_PLUS B ON A.USER_ID = B.USER_ID LEFT JOIN T5_SESSION@DB222 C ON A.USER_SIAK = C.USER_ID WHERE A.USER_ID = '$user_id'  ORDER BY C.LAST_ACTIVITY DESC NULLS LAST";
        $r = DB::select($sql);
        return $r;
    }
    public function act_get_all_activity($user_id)
    {
        $sql = "SELECT A.USER_ID, E.NIK, E.NAMA_LGKP, TO_CHAR(A.ACTIVITY_DATE, 'HH24:MI')AS ACTIVITY_DATE, B.ACTIVITY_NAME AS P1,C.ACTIVITY_NAME AS P2, A.ACTIVITY_DESC AS P3 FROM T5_HIST_ACTIVITY@DB222 A INNER JOIN T5_HIST_ACTIVITY_REF@DB222 B ON A.ACTIVITY_TYPE = B.ACTIVITY_ID INNER JOIN T5_HIST_ACTIVITY_REF@DB222 C ON A.ACTIVITY_MOD = C.ACTIVITY_ID INNER JOIN SIAK_USER_PLUS E ON A.USER_ID = E.USER_SIAK WHERE (E.USER_LEVEL =1 OR E.USER_LEVEL =3) AND TRUNC(ACTIVITY_DATE) = TRUNC(SYSDATE) AND E.USER_ID = '$user_id' ORDER BY E.USER_ID,A.ACTIVITY_DATE DESC";
        $r = DB::select($sql);
        return $r;
    }
    public function act_get_recap_activity($user_id)
    {
        $sql = "SELECT COUNT(1) AS JML ,A.USER_ID, CONCAT(B.ACTIVITY_NAME,CONCAT(' ',C.ACTIVITY_NAME)) AS P1 FROM T5_HIST_ACTIVITY@DB222 A INNER JOIN T5_HIST_ACTIVITY_REF@DB222 B ON A.ACTIVITY_TYPE = B.ACTIVITY_ID INNER JOIN T5_HIST_ACTIVITY_REF@DB222 C ON A.ACTIVITY_MOD = C.ACTIVITY_ID INNER JOIN SIAK_USER_PLUS X ON A.USER_ID = X.USER_SIAK WHERE 
                TRUNC(ACTIVITY_DATE) = TRUNC(SYSDATE) AND X.USER_ID = '$user_id'
                GROUP BY CONCAT(B.ACTIVITY_NAME,CONCAT(' ',C.ACTIVITY_NAME)),A.USER_ID ORDER BY A.USER_ID,CONCAT(B.ACTIVITY_NAME,CONCAT(' ',C.ACTIVITY_NAME))";
        $r = DB::select($sql);
        return $r;
    }
    public function act_get_recap_activity_data($user_id, $tgl_start, $tgl_end)
    {
        $sql = "SELECT COUNT(1) AS JML ,X.USER_ID,X.NAMA_LGKP, CONCAT(B.ACTIVITY_NAME,CONCAT(' ',C.ACTIVITY_NAME)) AS P1 FROM T5_HIST_ACTIVITY@DB222 A INNER JOIN T5_HIST_ACTIVITY_REF@DB222 B ON A.ACTIVITY_TYPE = B.ACTIVITY_ID INNER JOIN T5_HIST_ACTIVITY_REF@DB222 C ON A.ACTIVITY_MOD = C.ACTIVITY_ID INNER JOIN SIAK_USER_PLUS X ON A.USER_ID = X.USER_SIAK WHERE 
                ACTIVITY_DATE >= TO_DATE('" . $tgl_start . "','DD-MM-YYYY') AND ACTIVITY_DATE < TO_DATE('" . $tgl_end . "','DD-MM-YYYY')+1
                AND X.USER_ID = '$user_id'
                GROUP BY CONCAT(B.ACTIVITY_NAME,CONCAT(' ',C.ACTIVITY_NAME)),X.USER_ID,X.NAMA_LGKP ORDER BY X.USER_ID,CONCAT(B.ACTIVITY_NAME,CONCAT(' ',C.ACTIVITY_NAME))";
        $r = DB::select($sql);
        return $r;
    }
    public function act_get_count_activity($user_id)
    {
        $sql = "SELECT E.USER_ID,COUNT(1) AS JML FROM T5_HIST_ACTIVITY@DB222 A INNER JOIN T5_HIST_ACTIVITY_REF@DB222 B ON A.ACTIVITY_TYPE = B.ACTIVITY_ID INNER JOIN T5_HIST_ACTIVITY_REF@DB222 C ON A.ACTIVITY_MOD = C.ACTIVITY_ID INNER JOIN SIAK_USER_PLUS E ON A.USER_ID = E.USER_SIAK WHERE E.USER_ID = '$user_id' AND TRUNC(ACTIVITY_DATE) = TRUNC(SYSDATE) GROUP BY E.USER_ID ORDER BY E.USER_ID";
        $r = DB::select($sql);
        if (!empty($r))
        {
            return $r[0]->jml;
        }
        else
        {
            return 0;
        }
    }
    public function act_get_all_bcard($user_id)
    {
        $sql = "SELECT E.USER_ID AS MONITORING_USER, CASE WHEN A.LAST_UPDATED_USERNAME IS NULL THEN A.CREATED_USERNAME ELSE A.LAST_UPDATED_USERNAME END AS USER_ID, E.NIK, E.NAMA_LGKP, TO_CHAR(A.PERSONALIZED_DATE, 'HH24:MI')AS ACTIVITY_DATE, 'PENCETAKAN' AS P1,'E-KTP' AS P2, CONCAT(CONCAT(CONCAT('NIK : ',A.NIK),'<br />'),(SELECT CONCAT(CONCAT(CONCAT('NIK : ',X.NAMA_LGKP),'<br />'),CONCAT('E-KTP STATUS CODE : ',X.CURRENT_STATUS_CODE)) FROM DEMOGRAPHICS@DB2 X WHERE A.NIK = X.NIK)) AS P3 FROM CARD_MANAGEMENT@DB5CETAK A INNER JOIN  SIAK_USER_PLUS E ON CASE WHEN UPPER(A.LAST_UPDATED_USERNAME) IS NULL THEN UPPER(A.CREATED_USERNAME) ELSE UPPER(A.LAST_UPDATED_USERNAME) END = UPPER(E.USER_BCARD)    WHERE E.USER_ID = '$user_id'  AND CASE WHEN TO_CHAR(LAST_UPDATE,'DD/MM/YYYY') IS NULL THEN TO_CHAR(PERSONALIZED_DATE,'DD/MM/YYYY') ELSE TO_CHAR(LAST_UPDATE,'DD/MM/YYYY') END =  TO_CHAR(SYSDATE,'DD/MM/YYYY')  ORDER BY A.PERSONALIZED_DATE DESC";
        $r = DB::select($sql);
        return $r;
    }
    public function act_get_count_bcard($user_id)
    {
        $sql = "SELECT E.USER_ID, COUNT(1) AS JML FROM CARD_MANAGEMENT@DB5CETAK A INNER JOIN  SIAK_USER_PLUS E ON CASE WHEN UPPER(A.LAST_UPDATED_USERNAME) IS NULL THEN UPPER(A.CREATED_USERNAME) ELSE UPPER(A.LAST_UPDATED_USERNAME) END = UPPER(E.USER_BCARD)  WHERE E.USER_ID = '$user_id'  AND CASE WHEN TO_CHAR(LAST_UPDATE,'DD/MM/YYYY') IS NULL THEN TO_CHAR(PERSONALIZED_DATE,'DD/MM/YYYY') ELSE TO_CHAR(LAST_UPDATE,'DD/MM/YYYY') END =  TO_CHAR(SYSDATE,'DD/MM/YYYY') GROUP BY E.USER_ID ORDER BY E.USER_ID DESC";
        $r = DB::select($sql);
        if (!empty($r))
        {
            return $r[0]->jml;
        }
        else
        {
            return 0;
        }

    }
    public function act_get_all($user_id)
    {
        $sql = "SELECT A.USER_ID, CASE WHEN B.JML IS NULL THEN 0 ELSE B.JML END ALL_SIAK, CASE WHEN C.JML IS NULL THEN 0 ELSE C.JML END ALL_REKAM,  CASE WHEN D.JML IS NULL THEN 0 ELSE D.JML END ALL_CETAK FROM SIAK_USER_PLUS A 
          LEFT JOIN
          (SELECT E.USER_ID, COUNT(1) AS JML FROM T5_HIST_ACTIVITY@DB222 A INNER JOIN T5_HIST_ACTIVITY_REF@DB222 B ON A.ACTIVITY_TYPE = B.ACTIVITY_ID INNER JOIN T5_HIST_ACTIVITY_REF@DB222 C ON A.ACTIVITY_MOD = C.ACTIVITY_ID INNER JOIN SIAK_USER_PLUS E ON A.USER_ID = E.USER_SIAK WHERE TO_CHAR(ACTIVITY_DATE,'MM/YYYY') = TO_CHAR(SYSDATE,'MM/YYYY') GROUP BY E.USER_ID) B ON A.USER_ID = B.USER_ID
          LEFT JOIN
          (SELECT E.USER_ID, COUNT(1) AS JML FROM DEMOGRAPHICS@DB221 A INNER JOIN  SIAK_USER_PLUS E ON A.CREATED_USERNAME = E.USER_BENROL WHERE TO_CHAR(CREATED,'MM/YYYY') = TO_CHAR(SYSDATE,'MM/YYYY') GROUP BY E.USER_ID) C ON A.USER_ID = C.USER_ID
          LEFT JOIN 
          (SELECT E.USER_ID, COUNT(1) AS JML FROM CARD_MANAGEMENT@DB5CETAK A INNER JOIN  SIAK_USER_PLUS E ON CASE WHEN UPPER(A.LAST_UPDATED_USERNAME) IS NULL THEN UPPER(A.CREATED_USERNAME) ELSE UPPER(A.LAST_UPDATED_USERNAME) END = UPPER(E.USER_BCARD)  WHERE CASE WHEN TO_CHAR(LAST_UPDATE,'MM/YYYY') IS NULL THEN TO_CHAR(PERSONALIZED_DATE,'MM/YYYY') ELSE TO_CHAR(LAST_UPDATE,'MM/YYYY') END =  TO_CHAR(SYSDATE,'MM/YYYY') GROUP BY E.USER_ID) D ON A.USER_ID = D.USER_ID WHERE A.USER_ID = '$user_id'  ORDER BY A.USER_ID";
        $r = DB::select($sql);
        return $r;
    }
    public function act_get_all_benroll($user_id)
    {
        $sql = "SELECT E.USER_ID AS MONITORING_USER,A.CREATED_USERNAME AS USER_ID, E.NIK, E.NAMA_LGKP, TO_CHAR(A.CREATED, 'HH24:MI')AS ACTIVITY_DATE, 'PEREKAMAN' AS P1,'E-KTP' AS P2, CONCAT(CONCAT(CONCAT('NIK : ',A.NIK),'<br />'),CONCAT(CONCAT(CONCAT('NAMA : ',A.NAMA_LGKP),'<br />'),CONCAT('E-KTP STATUS CODE : ',A.CURRENT_STATUS_CODE))) AS P3 FROM DEMOGRAPHICS@DB221 A INNER JOIN  SIAK_USER_PLUS E ON A.CREATED_USERNAME = E.USER_BENROL WHERE E.USER_ID = '$user_id'  AND TO_CHAR(CREATED,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') ORDER BY A.CREATED DESC";
        $r = DB::select($sql);
        return $r;
    }
    public function act_get_count_benroll($user_id)
    {
        $sql = "SELECT E.USER_ID,COUNT(1) AS JML FROM DEMOGRAPHICS@DB221 A INNER JOIN  SIAK_USER_PLUS E ON A.CREATED_USERNAME = E.USER_BENROL WHERE E.USER_ID = '$user_id' AND TO_CHAR(CREATED,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') GROUP BY E.USER_ID ORDER BY E.USER_ID DESC";
        $r = DB::select($sql);
        if (!empty($r))
        {
            return $r[0]->jml;
        }
        else
        {
            return 0;
        }
    }
    public function act_get_clockin($user_id)
    {
        $nowdate = date("d/m/Y");
        $sql = "SELECT A.USER_ID, CASE WHEN JAM_MASUK IS NULL THEN '-' ELSE JAM_MASUK END  AS JML FROM SIAK_USER_PLUS A LEFT JOIN  SIAK_ABSENSI B ON A.USER_ID = B.USER_ID AND TO_CHAR(B.TANGGAL,'DD/MM/YYYY') = '$nowdate' WHERE A.USER_ID = '$user_id'";
        $r = DB::select($sql);
        return $r[0]->jml;
    }
    public function act_get_clockout($user_id)
    {
        $nowdate = date("d/m/Y");
        $sql = "SELECT A.USER_ID, CASE WHEN JAM_KELUAR IS NULL THEN '-' ELSE JAM_KELUAR END  AS JML FROM SIAK_USER_PLUS A LEFT JOIN  SIAK_ABSENSI B ON A.USER_ID = B.USER_ID AND TO_CHAR(B.TANGGAL,'DD/MM/YYYY') = '$nowdate' WHERE A.USER_ID = '$user_id'";
        $r = DB::select($sql);
        return $r[0]->jml;
    }
    public function act_get_master_activity()
    {
        $nowdate = date("d/m/Y");
        $sql = "SELECT A.USER_ID,A.NAMA_LGKP, A.NAMA_KANTOR, A.NIK
                    , CASE WHEN B.PEREKAMAN IS NULL THEN 0 ELSE B.PEREKAMAN END AS PEREKAMAN  
                    , CASE WHEN C.PENCETAKAN IS NULL THEN 0 ELSE C.PENCETAKAN END AS PENCETAKAN  
                    , CASE WHEN D.SIAK IS NULL THEN 0 ELSE D.SIAK END AS SIAK
                    FROM SIAK_USER_PLUS@YZDB A 
                    LEFT JOIN (SELECT e.user_id, COUNT(1) AS PEREKAMAN FROM DEMOGRAPHICS@DB221 A INNER JOIN  SIAK_USER_PLUS@YZDB E ON A.CREATED_USERNAME = E.USER_BENROL WHERE E.USER_LEVEL =1 AND TO_CHAR(CREATED,'DD/MM/YYYY') = '$nowdate' group by e.user_id) B 
                    ON A.USER_ID = B.USER_ID
                    LEFT JOIN (SELECT E.USER_ID, COUNT(1) AS PENCETAKAN FROM CARD_MANAGEMENT@DB5CETAK A INNER JOIN  SIAK_USER_PLUS@YZDB E ON CASE WHEN UPPER(A.LAST_UPDATED_USERNAME) IS NULL THEN UPPER(A.CREATED_USERNAME) ELSE UPPER(A.LAST_UPDATED_USERNAME) END = UPPER(E.USER_BCARD)    WHERE E.USER_LEVEL =1  AND CASE WHEN TO_CHAR(A.LAST_UPDATE,'DD/MM/YYYY') IS NULL THEN TO_CHAR(A.PERSONALIZED_DATE,'DD/MM/YYYY') ELSE TO_CHAR(A.LAST_UPDATE,'DD/MM/YYYY') END =  '$nowdate' GROUP BY E.USER_ID) C
                    ON A.USER_ID = C.USER_ID
                    LEFT JOIN (SELECT E.USER_ID, COUNT(1) AS SIAK FROM T5_HIST_ACTIVITY A INNER JOIN T5_HIST_ACTIVITY_REF B ON A.ACTIVITY_TYPE = B.ACTIVITY_ID INNER JOIN T5_HIST_ACTIVITY_REF C ON A.ACTIVITY_MOD = C.ACTIVITY_ID INNER JOIN SIAK_USER_PLUS@YZDB E ON A.USER_ID = E.USER_SIAK WHERE E.USER_LEVEL =1 AND ACTIVITY_DATE >= TO_DATE('$nowdate','DD/MM/yyyy') AND ACTIVITY_DATE < TO_DATE('$nowdate','DD/MM/yyyy')+1 GROUP BY E.USER_ID) D
                    ON A.USER_ID = D.USER_ID
                    WHERE A.USER_LEVEL = 1 AND A.IS_SHOW = 1 ORDER BY TO_NUMBER(A.NO_KEC)";
        $r = DB::connection('db222')->select($sql);
        return $r;
    }
    public function act_get_master_activity_dinas()
    {
        $nowdate = date("d/m/Y");
        $sql = "SELECT A.USER_ID,A.NAMA_LGKP, A.NAMA_KANTOR, A.NIK
                    , CASE WHEN B.PEREKAMAN IS NULL THEN 0 ELSE B.PEREKAMAN END AS PEREKAMAN  
                    , CASE WHEN C.PENCETAKAN IS NULL THEN 0 ELSE C.PENCETAKAN END AS PENCETAKAN  
                    , CASE WHEN D.SIAK IS NULL THEN 0 ELSE D.SIAK END AS SIAK
                    FROM SIAK_USER_PLUS@YZDB A 
                    LEFT JOIN (SELECT e.user_id, COUNT(1) AS PEREKAMAN FROM DEMOGRAPHICS@DB221 A INNER JOIN  SIAK_USER_PLUS@YZDB E ON A.CREATED_USERNAME = E.USER_BENROL WHERE E.USER_LEVEL = 3 AND E.IS_MONITORING = 1 AND A.CREATED >= TO_DATE('$nowdate','DD/MM/yyyy') AND A.CREATED < TO_DATE('$nowdate','DD/MM/yyyy')+1 group by e.user_id) B 
                    ON A.USER_ID = B.USER_ID
                    LEFT JOIN (SELECT E.USER_ID, COUNT(1) AS PENCETAKAN FROM CARD_MANAGEMENT@DB5CETAK A INNER JOIN  SIAK_USER_PLUS@YZDB E ON CASE WHEN UPPER(A.LAST_UPDATED_USERNAME) IS NULL THEN UPPER(A.CREATED_USERNAME) ELSE UPPER(A.LAST_UPDATED_USERNAME) END = UPPER(E.USER_BCARD)    WHERE E.USER_LEVEL = 3 AND E.IS_MONITORING = 1  AND CASE WHEN TO_CHAR(A.LAST_UPDATE,'DD/MM/YYYY') IS NULL THEN TO_CHAR(A.PERSONALIZED_DATE,'DD/MM/YYYY') ELSE TO_CHAR(A.LAST_UPDATE,'DD/MM/YYYY') END =  TO_CHAR(SYSDATE,'DD/MM/YYYY') GROUP BY E.USER_ID) C
                    ON A.USER_ID = C.USER_ID
                    LEFT JOIN (SELECT E.USER_ID, COUNT(1) AS SIAK FROM T5_HIST_ACTIVITY A INNER JOIN T5_HIST_ACTIVITY_REF B ON A.ACTIVITY_TYPE = B.ACTIVITY_ID INNER JOIN T5_HIST_ACTIVITY_REF C ON A.ACTIVITY_MOD = C.ACTIVITY_ID INNER JOIN SIAK_USER_PLUS@YZDB E ON A.USER_ID = E.USER_SIAK WHERE E.USER_LEVEL = 3 AND E.IS_MONITORING = 1 AND A.ACTIVITY_DATE >= TO_DATE('$nowdate','DD/MM/yyyy') AND A.ACTIVITY_DATE < TO_DATE('$nowdate','DD/MM/yyyy')+1 GROUP BY E.USER_ID) D
                    ON A.USER_ID = D.USER_ID
                    WHERE A.USER_LEVEL = 3 AND A.IS_MONITORING = 1 ORDER BY TO_NUMBER(A.NO_KEC)";
        $r = DB::connection('db222')->select($sql);
        return $r;
    }
    public function act_get_master_absensi()
    {
        $sql = "SELECT USER_ID ,NAMA_LGKP ,NAMA_KANTOR ,NIK ,JAM_MASUK ,JAM_KELUAR FROM VW_SIAK_MASTER_ABSENSI";
        $r = DB::select($sql);
        return $r;
    }
    public function act_get_master_absensi_dinas()
    {
        $sql = "SELECT USER_ID ,NAMA_LGKP ,NAMA_KANTOR ,NIK ,JAM_MASUK ,JAM_KELUAR FROM VW_SIAK_MASTER_ABSENSI_DINAS";
        $r = DB::select($sql);
        return $r;
    }
    public function act_get_master_pengajuan()
    {
        $nowdate = date("d/m/Y");
        $sql = "SELECT USER_ID ,NAMA_LGKP ,NAMA_KANTOR ,NIK ,PENGAJUAN_B ,PENGAJUAN_N ,PENGAJUAN_Y FROM VW_SIAK_MASTER_PENGAJUAN";
        $r = DB::select($sql);
        return $r;
    }
    public function act_get_master_tte()
    {
        $sql = "SELECT USER_ID, NAMA_LGKP, NAMA_KANTOR, NIK, REQUEST_N, APROVED_N, THE_REST_N, REQUEST_A, APROVED_A, THE_REST_A,APROVED_ORANGE,APROVED_BLUE,APROVED_GREEN,APROVED_RED FROM VW_SIAK_TTE";
        $r = DB::select($sql);
        return $r;
    }
    public function act_get_master_tte_kelahiran()
    {
        $sql = "SELECT  NAMA_LGKP,  REQUEST_N, APROVED_N, THE_REST_N,APROVED_ORANGE,APROVED_BLUE,APROVED_GREEN,APROVED_RED FROM VW_SIAK_TTE_AKTA";
        $r = DB::select($sql);
        return $r;
    }
    public function act_get_master_tte_kematian()
    {
        $sql = "SELECT  NAMA_LGKP,  REQUEST_N, APROVED_N, THE_REST_N,APROVED_ORANGE,APROVED_BLUE,APROVED_GREEN,APROVED_RED FROM VW_SIAK_TTE_KEMATIAN";
        $r = DB::select($sql);
        return $r;
    }
    public function act_get_master_tte_perpindahan()
    {
        $sql = "SELECT  NAMA_LGKP,  REQUEST_N, APROVED_N, THE_REST_N,APROVED_ORANGE,APROVED_BLUE,APROVED_GREEN,APROVED_RED FROM VW_SIAK_TTE_PERPINDAHAN";
        $r = DB::select($sql);
        return $r;
    }
    public function act_get_master_pemanfaatan()
    {
        $sql = "SELECT 
                    A.INSTANSI
                    , CASE WHEN B.JML_HARI IS NULL THEN 0 ELSE B.JML_HARI END JML_HARI
                    , CASE WHEN B.JML_MINGGU IS NULL THEN 0 ELSE B.JML_MINGGU END JML_MINGGU
                    , CASE WHEN B.JML_BULAN IS NULL THEN 0 ELSE B.JML_BULAN END JML_BULAN
                    FROM
                    (SELECT INSTANSI, COUNT(1) JML, MAX(TANGGAL) TANGGAL FROM LOG_ACCESS_WS@DB5 GROUP BY INSTANSI) A LEFT JOIN  VW_SIAK_PEMANFAATAN B ON A.INSTANSI = B.INSTANSI ORDER BY A.INSTANSI";
        $r = DB::select($sql);
        return $r;
    }

    public function act_get_master_list()
    {
        $sql = "SELECT USER_ID, NAMA_LGKP, NAMA_KANTOR, NIK, PENGAJUAN_S, PRR_S FROM VW_SIAK_LIST";
        $r = DB::select($sql);
        return $r;
    }
    public function act_cek_daily_activity($tgl_start, $tgl_end, $user_id)
    {
        $sql = "SELECT USER_ID,(SELECT SIAK_USER_PLUS.NAMA_LGKP FROM SIAK_USER_PLUS WHERE SIAK_DAILY.USER_ID = SIAK_USER_PLUS.USER_ID ) AS NAMA, CASE WHEN (SELECT SIAK_ACTIVITY.ACTIVITY FROM SIAK_ACTIVITY WHERE SIAK_ACTIVITY.ACTIVITY_ID = SIAK_DAILY.ACTIVITY_ID) IS NULL THEN 'LAINNYA' ELSE (SELECT SIAK_ACTIVITY.ACTIVITY FROM SIAK_ACTIVITY WHERE SIAK_ACTIVITY.ACTIVITY_ID = SIAK_DAILY.ACTIVITY_ID) END AS AKTIVITAS,UPPER(DESCRIPTION) KETERANGAN,  TO_CHAR(CREATED_DT,'DD-MM-YYYY') AS TGL_AKTIVITAS,START_ACTIVITY AS DARI,END_ACTIVITY AS SAMPAI,NUMBER_ACTIVITY AS JUMLAH_AKTIVITAS FROM SIAK_DAILY WHERE USER_ID = '$user_id' AND CREATED_DT >= TO_DATE('$tgl_start','DD-MM-YYYY') AND CREATED_DT < TO_DATE('$tgl_end','DD-MM-YYYY') +1 ORDER BY USER_ID,CREATED_DT DESC";
        $r = DB::select($sql);
        return $r;
    }
    public function act_cek_count_daily_activity($tgl_start, $tgl_end, $user_id)
    {
        $sql = "SELECT COUNT(1) AS JML FROM SIAK_DAILY WHERE USER_ID = '$user_id' AND CREATED_DT >= TO_DATE('$tgl_start','DD-MM-YYYY') AND CREATED_DT < TO_DATE('$tgl_end','DD-MM-YYYY') +1";
        $r = DB::select($sql);
        return $r[0]->jml;
    }
    public function act_cek_daily_absensi($user_id)
    {
        $sql = "SELECT A.USER_ID, A.NAMA_LGKP,TO_CHAR(B.TANGGAL,'DD-MM-YYYY') AS TANGGAL, B.HARI, B.JAM_MASUK, B.JAM_KELUAR,CONCAT(TRUNC(24*(to_date(CONCAT(TO_CHAR(B.TANGGAL,'DD-MM-YYYY'),CONCAT(' ',B.JAM_KELUAR)), 'DD-MM-YYYY hh24:mi') - to_date(CONCAT(TO_CHAR(B.TANGGAL,'DD-MM-YYYY'),CONCAT(' ',B.JAM_MASUK)), 'DD-MM-YYYY hh24:mi')),0),' Jam') AS JAM_KERJA FROM SIAK_USER_PLUS A INNER JOIN
                    (SELECT 
                      A.TANGGAL
                      , CASE 
                      WHEN TO_CHAR (A.TANGGAL, 'DY') = 'SAT' THEN 'SABTU'
                      WHEN TO_CHAR (A.TANGGAL, 'DY') = 'SUN' THEN 'MINGGU'
                      WHEN TO_CHAR (A.TANGGAL, 'DY') = 'MON' THEN 'SENIN'
                      WHEN TO_CHAR (A.TANGGAL, 'DY') = 'TUE' THEN 'SELASA'
                      WHEN TO_CHAR (A.TANGGAL, 'DY') = 'WED' THEN 'RABU'
                      WHEN TO_CHAR (A.TANGGAL, 'DY') = 'THU' THEN 'KAMIS'
                      WHEN TO_CHAR (A.TANGGAL, 'DY') = 'FRI' THEN 'JUMAT'
                      ELSE '' END AS HARI
                      , '$user_id' AS USER_ID
                      , CASE WHEN JAM_MASUK IS NULL THEN '-' ELSE JAM_MASUK END AS JAM_MASUK
                      , CASE WHEN JAM_KELUAR IS NULL THEN '-' ELSE JAM_KELUAR END AS JAM_KELUAR
                       FROM (SELECT TRUNC(SYSDATE, 'YYYY') + LEVEL - 1 AS TANGGAL   FROM DUAL CONNECT BY TRUNC(TRUNC(SYSDATE, 'YYYY') + LEVEL - 1, 'YYYY') = TRUNC(SYSDATE, 'YYYY')) A LEFT JOIN (select USER_ID,TANGGAL, NAMA, JAM_MASUK, JAM_KELUAR, KETERANGAN FROM SIAK_ABSENSI WHERE USER_ID ='$user_id') B ON A.TANGGAL = B.TANGGAL WHERE A.TANGGAL >= TO_DATE('02-02-2018','DD-MM-YYYY') AND A.TANGGAL <= TRUNC(SYSDATE)+1 ) B ON A.USER_ID = B.USER_ID  ORDER BY B.TANGGAL";
        $r = DB::select($sql);
        return $r;
    }
    public function act_cek_count_daily_absensi($user_id)
    {
        $sql = "SELECT COUNT(1) AS JML FROM SIAK_USER_PLUS A INNER JOIN
                    (SELECT 
                      A.TANGGAL
                      , CASE 
                      WHEN TO_CHAR (A.TANGGAL, 'DY') = 'SAT' THEN 'SABTU'
                      WHEN TO_CHAR (A.TANGGAL, 'DY') = 'SUN' THEN 'MINGGU'
                      WHEN TO_CHAR (A.TANGGAL, 'DY') = 'MON' THEN 'SENIN'
                      WHEN TO_CHAR (A.TANGGAL, 'DY') = 'TUE' THEN 'SELASA'
                      WHEN TO_CHAR (A.TANGGAL, 'DY') = 'WED' THEN 'RABU'
                      WHEN TO_CHAR (A.TANGGAL, 'DY') = 'THU' THEN 'KAMIS'
                      WHEN TO_CHAR (A.TANGGAL, 'DY') = 'FRI' THEN 'JUMAT'
                      ELSE '' END AS HARI
                      , '$user_id' AS USER_ID
                      , CASE WHEN JAM_MASUK IS NULL THEN '-' ELSE JAM_MASUK END AS JAM_MASUK
                      , CASE WHEN JAM_KELUAR IS NULL THEN '-' ELSE JAM_KELUAR END AS JAM_KELUAR
                       FROM (SELECT TRUNC(SYSDATE, 'YYYY') + LEVEL - 1 AS TANGGAL   FROM DUAL CONNECT BY TRUNC(TRUNC(SYSDATE, 'YYYY') + LEVEL - 1, 'YYYY') = TRUNC(SYSDATE, 'YYYY')) A LEFT JOIN (select USER_ID,TANGGAL, NAMA, JAM_MASUK, JAM_KELUAR, KETERANGAN FROM SIAK_ABSENSI WHERE USER_ID ='$user_id') B ON A.TANGGAL = B.TANGGAL WHERE A.TANGGAL >= TO_DATE('02-02-2018','DD-MM-YYYY') AND A.TANGGAL <= TRUNC(SYSDATE)+1 ) B ON A.USER_ID = B.USER_ID  ORDER BY B.TANGGAL";
        $r = DB::select($sql);
        return $r[0]->jml;
    }
    public function act_get_user($user_id)
    {
        $sql = "SELECT USER_ID ,USER_PWD ,NAMA_LGKP ,NIK ,TMPT_LHR ,TGL_LHR ,JENIS_KLMIN ,TELP ,USER_SIAK ,USER_BCARD ,USER_BENROL FROM VW_SIAK_USER WHERE USER_ID = '$user_id'";
        $r = DB::select($sql);
        return $r;
    }

    public function act_report_clock_in($user_id)
    {
        $sql = "SELECT SEQ_ID, USER_ID, NAMA, TO_CHAR(TANGGAL,'DD-MM-YYYY') TANGGAL, JAM_MASUK, JAM_KELUAR, IP_ADDRESS, CI_FROM, KETERANGAN FROM SIAK_ABSENSI WHERE USER_ID = '$user_id' AND TRUNC(TANGGAL) = TRUNC(SYSDATE)";
        $r = DB::select($sql);
        return $r;
    }
    public function act_report_is_absen($user_id)
    {
        $sql = "SELECT USER_ID, NAMA_LGKP FROM SIAK_USER_PLUS WHERE USER_ID = '$user_id' and IS_ABSEN =1";
        $r = DB::select($sql);
        return $r;
    }
    public function act_get_rekap_absensi($user_id)
    {
        $sql = "SELECT 
                      TO_CHAR(A.DAY,'DD-MM-YYYY') DAY
                    , TO_CHAR(A.DAY, 'd') DOW
                    , '$user_id' USER_ID
                    , (SELECT NAMA_LGKP FROM SIAK_USER_PLUS WHERE USER_ID = '$user_id') NAMA_LGKP
                    , CASE WHEN B.JAM_MASUK IS NULL THEN '-' ELSE B.JAM_MASUK END JAM_MASUK 
                    , CASE WHEN B.JAM_KELUAR IS NULL THEN '-' ELSE B.JAM_KELUAR END JAM_KELUAR
                    , CASE WHEN B.KETERANGAN IS NULL THEN '-' ELSE B.KETERANGAN END KETERANGAN
                FROM (SELECT TRUNC(SYSDATE, 'MM') + LEVEL - 1 AS DAY FROM DUAL CONNECT BY TRUNC(TRUNC(SYSDATE, 'MM') + LEVEL - 1, 'MM') = TRUNC(SYSDATE, 'MM')) A
                LEFT JOIN (SELECT USER_ID,NAMA,JAM_MASUK, JAM_KELUAR, KETERANGAN, TANGGAL FROM SIAK_ABSENSI WHERE USER_ID = '$user_id') B ON TRUNC(A.DAY) = TRUNC(B.TANGGAL) ORDER BY A.DAY";
        $r = DB::select($sql);
        return $r;
    }
    public function act_do_clockin($absen_user_id, $absen_nama, $absen_tanggal, $absen_masuk, $absen_keluar, $absen_ket, $absen_ip)
    {
        $sql = "INSERT INTO SIAK_ABSENSI (SEQ_ID,USER_ID, NAMA, TANGGAL, JAM_MASUK, JAM_KELUAR, KETERANGAN, CI_FROM,IP_ADDRESS) VALUES (CONCAT(TO_CHAR(TO_DATE('$absen_tanggal','DD-MM-YYYY'),'DDMMYYYY'),'-$absen_user_id-" . time() . "'),'$absen_user_id','$absen_nama',TO_DATE('$absen_tanggal','DD-MM-YYYY'),'$absen_masuk','$absen_keluar','$absen_ket','MONITORING','$absen_ip')";
        $r = DB::insert($sql);
        return $r;
    }
    public function act_do_clockout($absen_user_id, $absen_keluar, $absen_tanggal)
    {
        $sql = "UPDATE SIAK_ABSENSI SET JAM_KELUAR = '$absen_keluar' WHERE USER_ID = '$absen_user_id' AND TANGGAL = TO_DATE('$absen_tanggal','DD-MM-YYYY')";
        $r = DB::update($sql);
        return $r;
    }

    public function act_check_excuse($user_id, $tanggal_absen)
    {
        $sql = "SELECT COUNT(USER_ID) AS JML FROM SIAK_ABSENSI WHERE USER_ID = '$user_id' AND TO_CHAR(TANGGAL,'DD/MM/YYYY') = '$tanggal_absen'";
        $r = DB::select($sql);
        return $r[0]->jml;
    }
    public function act_excuse_getdate($user_id, $tanggal_absen)
    {
        $sql = "SELECT  JAM_MASUK, JAM_KELUAR FROM SIAK_ABSENSI WHERE USER_ID = '$user_id' AND TO_CHAR(TANGGAL,'DD/MM/YYYY') = '$tanggal_absen'";
        $r = DB::select($sql);
        return $r;
    }
    public function act_cekdate($tanggal_absen)
    {
        $sql = "SELECT COUNT(1) AS JML FROM DUAL WHERE TO_DATE(TO_CHAR(SYSDATE,'DD/MM/YYYY'),'DD/MM/YYYY') < TO_DATE('$tanggal_absen','DD/MM/YYYY')";
        $r = DB::select($sql);
        return $r[0]->jml;
    }
    public function act_get_des_excuse($excuse_keterangan)
    {
        $sql = "SELECT EXCUSE_DESCRIPTION AS DESCRIP FROM SIAK_MASTER_EXCUSE WHERE EXCUSE_ID = $excuse_keterangan";
        $r = DB::select($sql);
        return $r[0]->descrip;
    }
    public function act_insert_excuse($user_id, $nama_lgkp, $excuse_tanggal, $excuse_ci, $excuse_co, $descrip, $ipaddress)
    {
        $sql = "INSERT INTO SIAK_ABSENSI (SEQ_ID,USER_ID, NAMA, TANGGAL, JAM_MASUK, JAM_KELUAR, KETERANGAN, CI_FROM,IP_ADDRESS) VALUES (CONCAT(TO_CHAR(TO_DATE('$excuse_tanggal','DD/MM/YYYY'),'DDMMYYYY'),'-excs-$user_id-" . time() . "'),'$user_id','$nama_lgkp',TO_DATE('$excuse_tanggal','DD/MM/YYYY'),'$excuse_ci','$excuse_co','$descrip','EXCUSE','$ipaddress')";
        $r = DB::insert($sql);
        return $r;
    }
    public function act_update_excuse($user_id, $nama_lgkp, $excuse_tanggal, $excuse_ci, $excuse_co, $descrip, $ipaddress)
    {
        $sql = "UPDATE SIAK_ABSENSI A SET A.JAM_MASUK = '$excuse_ci', A.JAM_KELUAR = '$excuse_co', A.KETERANGAN = '$descrip' WHERE A.USER_ID = '$user_id' AND A.TANGGAL = TO_DATE('$excuse_tanggal','DD/MM/YYYY') AND NOT EXISTS (SELECT 1 FROM SIAK_MASTER_EXCUSE B WHERE UPPER(A.KETERANGAN) = B.EXCUSE_DESCRIPTION)";
        $r = DB::update($sql);
        return $r;
    }
    public function act_excuse_option()
    {
        $sql = "SELECT EXCUSE_ID, EXCUSE_DESCRIPTION FROM SIAK_MASTER_EXCUSE ORDER BY EXCUSE_ID";
        $r = DB::select($sql);
        return $r;
    }
    public function act_daily_option($user_id)
    {
        $sql = "SELECT A.ACTIVITY_ID, A.ACTIVITY, A.MINUTE FROM SIAK_ACTIVITY A INNER JOIN SIAK_USER_PLUS B ON A.USER_LEVEL = B.ACTIVITY_LEVEL WHERE B.USER_ID = '$user_id' ORDER BY A.ACTIVITY_ID";
        $r = DB::select($sql);
        return $r;
    }
    public function act_check_daily($user_id, $aktivitas, $tanggal_report)
    {
        $sql = "SELECT COUNT(DAILY_ID) AS JML FROM SIAK_DAILY WHERE ACTIVITY_ID = $aktivitas AND USER_ID = '$user_id' AND TO_CHAR(CREATED_DT,'DD/MM/YYYY') = '$tanggal_report'";
        $r = DB::select($sql);
        return $r[0]->jml;
    }
    public function act_insert_daily($aktivitas, $user_id, $daily_dari, $daily_sampai, $jumlah, $daily_keterangan, $tanggal_report)
    {
        $sql = "INSERT INTO SIAK_DAILY (DAILY_ID,ACTIVITY_ID,USER_ID,START_ACTIVITY,END_ACTIVITY,NUMBER_ACTIVITY,DESCRIPTION,CREATED_BY,CREATED_DT, IS_VALID) VALUES (DAILY_SEQ.NEXTVAL,$aktivitas,'$user_id','$daily_dari','$daily_sampai',$jumlah,UPPER('$daily_keterangan'),'SYSTEM',TO_DATE('$tanggal_report','DD/MM/YYYY'),0)";
        $r = DB::insert($sql);
        return $r;
    }
    public function act_delete_daily($activity_delete)
    {
        $sql = "DELETE FROM SIAK_DAILY WHERE DAILY_ID = $activity_delete";
        $r = DB::delete($sql);
        return $r;
    }
    public function act_daily_rekap($user_id)
    {
        $sql = "SELECT ROWNUM, DAILY_ID, USER_ID, NAMA_DPN, DESCRIPTION, TANGGAL, START_ACTIVITY, END_ACTIVITY, CASE WHEN ACTIVITY IS NULL THEN 'LAINYA' ELSE ACTIVITY END ACTIVITY, NUMBER_ACTIVITY, MINUTE_ACTIVITY FROM  (SELECT A.DAILY_ID, A.USER_ID, C.NAMA_DPN, UPPER(A.DESCRIPTION) AS DESCRIPTION, TO_CHAR(A.CREATED_DT,'DD-MM-YYYY') AS TANGGAL, A.START_ACTIVITY, A.END_ACTIVITY, B.ACTIVITY, A.NUMBER_ACTIVITY, CONCAT((A.NUMBER_ACTIVITY * CASE WHEN B.MINUTE IS NULL THEN 10 ELSE B.MINUTE END),' Menit') MINUTE_ACTIVITY  FROM SIAK_DAILY A  INNER JOIN SIAK_USER_PLUS C ON A.USER_ID = C.USER_ID LEFT JOIN SIAK_ACTIVITY B ON A.ACTIVITY_ID = B.ACTIVITY_ID WHERE A.USER_ID = 'user_id' ORDER BY A.CREATED_DT DESC) WHERE ROWNUM <=250";
        $r = DB::select($sql);
        return $r;
    }
    public function act_daily_rekap_tgl($user_id, $tgl_start, $tgl_end)
    {
        $sql = "SELECT ROWNUM, DAILY_ID, USER_ID, NAMA_DPN, DESCRIPTION, TANGGAL, START_ACTIVITY, END_ACTIVITY, ACTIVITY, NUMBER_ACTIVITY, MINUTE_ACTIVITY FROM  (SELECT A.DAILY_ID, A.USER_ID, C.NAMA_DPN, UPPER(A.DESCRIPTION) AS DESCRIPTION, TO_CHAR(A.CREATED_DT,'DD-MM-YYYY') AS TANGGAL, A.START_ACTIVITY, A.END_ACTIVITY, CASE WHEN B.ACTIVITY IS NULL THEN 'LAINNYA' ELSE B.ACTIVITY END ACTIVITY, A.NUMBER_ACTIVITY, CONCAT((A.NUMBER_ACTIVITY * CASE WHEN B.MINUTE IS NULL THEN 10 ELSE B.MINUTE END),' Menit') MINUTE_ACTIVITY  FROM SIAK_DAILY A  INNER JOIN SIAK_USER_PLUS C ON A.USER_ID = C.USER_ID LEFT JOIN SIAK_ACTIVITY B ON A.ACTIVITY_ID = B.ACTIVITY_ID WHERE A.USER_ID = '$user_id' AND A.CREATED_DT >= TO_DATE('$tgl_start','DD-MM-YYYY') AND A.CREATED_DT < TO_DATE('$tgl_end','DD-MM-YYYY')+1 ORDER BY A.CREATED_DT DESC)";
        $r = DB::select($sql);
        return $r;
    }
    public function act_rekap_absensi($tgl)
    {
        $sql = "";
        $sql .= "SELECT A.USER_ID
          , A.NAMA_LGKP
                    , D.KETERANGAN
                    , CASE WHEN B.JUMLAH_HADIR IS NULL THEN 0 ELSE B.JUMLAH_HADIR END JUMLAH_HADIR
                    , CASE WHEN B.TEPAT_WAKTU IS NULL THEN 0 ELSE B.TEPAT_WAKTU END TEPAT_WAKTU
                    , CASE WHEN B.TERLAMBAT IS NULL THEN 0 ELSE B.TERLAMBAT END TERLAMBAT 
                    , CASE WHEN B.EXCUSE IS NULL THEN 0 ELSE B.EXCUSE END EXCUSE 
                    , CASE WHEN B.MINUTE_WORK IS NULL THEN 0 ELSE B.MINUTE_WORK END MINUTE_WORK 
                    FROM SIAK_USER_PLUS A 
                    LEFT JOIN SIAK_LEVEL D ON A.USER_LEVEL = D.LEVEL_ID
                    LEFT JOIN
                    (SELECT A.USER_ID
                        , COUNT(B.TANGGAL) AS JUMLAH_HADIR
                        , SUM(CASE WHEN UPPER(B.KETERANGAN) = 'TEPAT' OR  UPPER(B.KETERANGAN) = 'TEPAT WAKTU' THEN 1 ELSE 0 END) AS TEPAT_WAKTU  
                        , SUM(CASE WHEN UPPER(B.KETERANGAN) = 'TERLAMBAT' THEN 1 ELSE 0 END) AS TERLAMBAT
                        , SUM(CASE WHEN UPPER(B.KETERANGAN) <> 'TEPAT WAKTU' AND UPPER(B.KETERANGAN) <> 'TERLAMBAT' AND UPPER(B.KETERANGAN) <> 'TEPAT' THEN 1 ELSE 0 END) AS EXCUSE
                        , SUM((TO_DATE(JAM_KELUAR,'HH24:MI') - TO_DATE(JAM_MASUK,'HH24:MI'))*24*60) AS MINUTE_WORK
                    FROM SIAK_USER_PLUS A LEFT JOIN SIAK_ABSENSI B ON A.USER_ID = B.USER_ID WHERE 1=1 AND ";
        if ($tgl != '')
        {
            $sql .= " TO_CHAR(B.TANGGAL,'MM-YYYY') = '$tgl' ";
        }
        else
        {
            $sql .= " TO_CHAR(B.TANGGAL,'MM-YYYY') = TO_CHAR(SYSDATE,'MM-YYYY') ";
        }
        $sql .= " AND IS_ABSEN = 1 GROUP BY A.USER_ID
                     ) B ON A.USER_ID = B.USER_ID WHERE 1=1 AND IS_ABSEN =1 ORDER BY UPPER(A.USER_ID) ";
        $r = DB::select($sql);
        return $r;
    }
    public function act_rekap_daily($tgl)
    {
        $sql = "";
        $sql .= "SELECT A.USER_ID
                    , A.NAMA_LGKP
                    , CASE WHEN B.AKTIVITAS IS NULL THEN 0 ELSE B.AKTIVITAS END AKTIVITAS
                    , CASE WHEN B.JUMLAH_AKTIVITAS IS NULL THEN 0 ELSE B.JUMLAH_AKTIVITAS END JUMLAH_AKTIVITAS
                    , CASE WHEN B.TASK_PER_AKTIVITAS IS NULL THEN 0 ELSE B.TASK_PER_AKTIVITAS END TASK_PER_AKTIVITAS 
                    FROM SIAK_USER_PLUS A LEFT JOIN
                    (
                    SELECT A.USER_ID
                        , COUNT(DISTINCT(ACTIVITY_ID)) AS AKTIVITAS 
                        , COUNT(DAILY_ID) JUMLAH_AKTIVITAS
                        , SUM(B.NUMBER_ACTIVITY) TASK_PER_AKTIVITAS 
                    FROM SIAK_USER_PLUS A INNER JOIN SIAK_DAILY B 
                    ON A.USER_ID = B.USER_ID WHERE IS_ABSEN = 1 AND ";
        if ($tgl != '')
        {
            $sql .= " TO_CHAR(B.CREATED_DT,'MM-YYYY') = '$tgl' ";
        }
        else
        {
            $sql .= " TO_CHAR(B.CREATED_DT,'MM-YYYY') = TO_CHAR(SYSDATE,'MM-YYYY') ";
        }
        $sql .= " GROUP BY A.USER_ID) B ON A.USER_ID = B.USER_ID
                        WHERE 1=1 AND IS_ABSEN =1 ORDER BY UPPER(A.USER_ID) ";

        $r = DB::select($sql);
        return $r;
    }
    public function act_get_rekap_absensi_detail($tgl, $user_id)
    {
        $sql = "SELECT 
                      TO_CHAR(A.DAY,'DD-MM-YYYY') DAY
                    , TO_CHAR(A.DAY, 'd') DOW
                    , '$user_id' USER_ID
                    , (SELECT NAMA_LGKP FROM SIAK_USER_PLUS WHERE USER_ID = '$user_id') NAMA_LGKP
                    , CASE WHEN B.JAM_MASUK IS NULL THEN '-' ELSE B.JAM_MASUK END JAM_MASUK 
                    , CASE WHEN B.JAM_KELUAR IS NULL THEN '-' ELSE B.JAM_KELUAR END JAM_KELUAR
                    , CASE WHEN B.KETERANGAN IS NULL THEN '-' ELSE B.KETERANGAN END KETERANGAN
                FROM (SELECT TRUNC(TO_DATE('$tgl','MM-YYYY'), 'MM') + LEVEL - 1 AS DAY FROM DUAL CONNECT BY TRUNC(TRUNC(TO_DATE('$tgl','MM-YYYY'), 'MM') + LEVEL - 1, 'MM') = TRUNC(TO_DATE('$tgl','MM-YYYY'), 'MM')) A
                LEFT JOIN (SELECT USER_ID,NAMA,JAM_MASUK, JAM_KELUAR, KETERANGAN, TANGGAL FROM SIAK_ABSENSI WHERE USER_ID = '$user_id') B ON TRUNC(A.DAY) = TRUNC(B.TANGGAL) ORDER BY A.DAY";
        $r = DB::select($sql);
        return $r;
    }
    public function act_get_rekap_activity_detail($tgl, $user_id)
    {
        $sql = "SELECT ROWNUM, DAILY_ID, USER_ID, NAMA_DPN, DESCRIPTION, TANGGAL, START_ACTIVITY, END_ACTIVITY, ACTIVITY, NUMBER_ACTIVITY FROM  (SELECT A.DAILY_ID, A.USER_ID, C.NAMA_DPN, UPPER(A.DESCRIPTION) AS DESCRIPTION, TO_CHAR(A.CREATED_DT,'DD-MM-YYYY') AS TANGGAL, A.START_ACTIVITY, A.END_ACTIVITY, B.ACTIVITY, A.NUMBER_ACTIVITY  FROM SIAK_DAILY A INNER JOIN SIAK_ACTIVITY B ON A.ACTIVITY_ID = B.ACTIVITY_ID INNER JOIN SIAK_USER_PLUS C ON A.USER_ID = C.USER_ID WHERE A.USER_ID = '$user_id' AND
                TO_CHAR(A.CREATED_DT,'MM-YYYY') = '$tgl'
                 ORDER BY A.CREATED_DT DESC)";
        $r = DB::select($sql);
        return $r;
    }
    public function act_rekap_daily_ttd($user_id, $tgl_start, $tgl_end)
    {
        $sql = "SELECT TO_CHAR(A.CREATED_DT,'DD-MM-YYYY') TANGGAL, USER_ID
            , LISTAGG(CASE WHEN B.ACTIVITY = 'LAINNYA' THEN CONCAT('- ',UPPER(A.DESCRIPTION)) ELSE CONCAT('- ',B.ACTIVITY) END, ' </BR>') WITHIN GROUP (ORDER BY TRUNC(A.CREATED_DT)) AS DESCRIPTION
            , LISTAGG(A.NUMBER_ACTIVITY, ' Aktivitas </BR>') WITHIN GROUP (ORDER BY TRUNC(A.CREATED_DT)) AS JUMLAH
            FROM SIAK_DAILY A INNER JOIN SIAK_ACTIVITY B ON A.ACTIVITY_ID = B.ACTIVITY_ID WHERE USER_ID = '$user_id' AND A.CREATED_DT >= TO_DATE('$tgl_start','DD-MM-YYYY') AND A.CREATED_DT < TO_DATE('$tgl_end','DD-MM-YYYY')+1 GROUP BY TO_CHAR(A.CREATED_DT,'DD-MM-YYYY'),USER_ID ORDER BY TO_CHAR(A.CREATED_DT,'DD-MM-YYYY') DESC,USER_ID";
        $r = DB::select($sql);
        return $r;
    }
    public function act_rekap_daily_ttd_table($user_id, $tgl_start, $tgl_end)
    {
        $sql = "SELECT 
            TRUNC(A.CREATED_DT) TGL
            ,TO_CHAR(A.CREATED_DT,'DD-MM-YYYY') TANGGAL, USER_ID
            , LISTAGG(CASE WHEN B.ACTIVITY = 'LAINNYA' THEN CONCAT('- ',UPPER(A.DESCRIPTION)) ELSE CONCAT('- ',B.ACTIVITY) END, ' </BR>') WITHIN GROUP (ORDER BY TRUNC(A.CREATED_DT),A.DAILY_ID) AS DESCRIPTION
            , LISTAGG(A.NUMBER_ACTIVITY, ' Aktivitas </BR>') WITHIN GROUP (ORDER BY TRUNC(A.CREATED_DT),A.DAILY_ID) AS JUMLAH
            FROM SIAK_DAILY A INNER JOIN SIAK_ACTIVITY B ON A.ACTIVITY_ID = B.ACTIVITY_ID WHERE USER_ID = '$user_id' AND A.CREATED_DT >= TO_DATE('$tgl_start','DD-MM-YYYY') AND A.CREATED_DT < TO_DATE('$tgl_end','DD-MM-YYYY')+1 GROUP BY TO_CHAR(A.CREATED_DT,'DD-MM-YYYY'),TRUNC(A.CREATED_DT),USER_ID ORDER BY TRUNC(A.CREATED_DT) DESC,USER_ID";
        $r = DB::select($sql);
        return $r;
    }
    public function act_rekap_daily_ttd_table2($user_id, $tgl)
    {
        $sql = "SELECT 
            TRUNC(A.CREATED_DT) TGL
            ,TO_CHAR(A.CREATED_DT,'DD-MM-YYYY') TANGGAL, USER_ID
            , LISTAGG(CASE WHEN B.ACTIVITY = 'LAINNYA' THEN CONCAT('- ',UPPER(A.DESCRIPTION)) ELSE CONCAT('- ',B.ACTIVITY) END, ' </BR>') WITHIN GROUP (ORDER BY TRUNC(A.CREATED_DT),A.DAILY_ID) AS DESCRIPTION
            , LISTAGG(A.NUMBER_ACTIVITY, ' Aktivitas </BR>') WITHIN GROUP (ORDER BY TRUNC(A.CREATED_DT),A.DAILY_ID) AS JUMLAH
            FROM SIAK_DAILY A INNER JOIN SIAK_ACTIVITY B ON A.ACTIVITY_ID = B.ACTIVITY_ID WHERE USER_ID = '$user_id' AND TO_CHAR(A.CREATED_DT,'MM-YYYY') = '$tgl' GROUP BY TO_CHAR(A.CREATED_DT,'DD-MM-YYYY'),TRUNC(A.CREATED_DT),USER_ID ORDER BY TRUNC(A.CREATED_DT) DESC,USER_ID";
        $r = DB::select($sql);
        return $r;
    }
    public function act_rekap_daily_ttd_pdf2($user_id, $tgl)
    {
        $sql = "SELECT 
            TRUNC(A.CREATED_DT) TGL
            ,TO_CHAR(A.CREATED_DT,'DD-MM-YYYY') TANGGAL, USER_ID
            , LISTAGG(CASE WHEN B.ACTIVITY = 'LAINNYA' THEN CONCAT('- ',UPPER(A.DESCRIPTION)) ELSE CONCAT('- ',B.ACTIVITY) END, '//line') WITHIN GROUP (ORDER BY TRUNC(A.CREATED_DT),A.DAILY_ID) AS DESX
            , LISTAGG(A.NUMBER_ACTIVITY, ' Aktivitas //line') WITHIN GROUP (ORDER BY TRUNC(A.CREATED_DT),A.DAILY_ID) AS JUMLAH
            FROM SIAK_DAILY A INNER JOIN SIAK_ACTIVITY B ON A.ACTIVITY_ID = B.ACTIVITY_ID WHERE USER_ID = '$user_id' AND TO_CHAR(A.CREATED_DT,'MM-YYYY') = '$tgl' AND TRUNC(A.CREATED_DT) <= TRUNC(SYSDATE) GROUP BY TO_CHAR(A.CREATED_DT,'DD-MM-YYYY'),TRUNC(A.CREATED_DT),USER_ID ORDER BY TRUNC(A.CREATED_DT) DESC,USER_ID";
        $r = DB::select($sql);
        return $r;
    }
    public function act_rekap_daily_ttd_pdf($user_id, $tgl_start, $tgl_end)
    {
        $sql = "SELECT 
            TRUNC(A.CREATED_DT) TGL
            ,TO_CHAR(A.CREATED_DT,'DD-MM-YYYY') TANGGAL, USER_ID
            , LISTAGG(CASE WHEN B.ACTIVITY = 'LAINNYA' THEN CONCAT('- ',UPPER(A.DESCRIPTION)) ELSE CONCAT('- ',B.ACTIVITY) END, '//line') WITHIN GROUP (ORDER BY TRUNC(A.CREATED_DT),A.DAILY_ID) AS DESX
            , LISTAGG(A.NUMBER_ACTIVITY, ' Aktivitas //line') WITHIN GROUP (ORDER BY TRUNC(A.CREATED_DT),A.DAILY_ID) AS JUMLAH
            FROM SIAK_DAILY A INNER JOIN SIAK_ACTIVITY B ON A.ACTIVITY_ID = B.ACTIVITY_ID WHERE USER_ID = '$user_id' AND A.CREATED_DT >= TO_DATE('$tgl_start','DD-MM-YYYY') AND A.CREATED_DT < TO_DATE('$tgl_end','DD-MM-YYYY')+1 GROUP BY TO_CHAR(A.CREATED_DT,'DD-MM-YYYY'),TRUNC(A.CREATED_DT),USER_ID ORDER BY TRUNC(A.CREATED_DT) DESC,USER_ID";
        $r = DB::select($sql);
        return $r;
    }
    public function act_get_atasan($user_id)
    {
        $sql = "SELECT 
                      A.USER_ID
                      , A.NAMA_LGKP
                      , CASE WHEN C.NAMA IS NULL THEN '' ELSE C.NAMA END NAMA_SATU
                      , CASE WHEN C.NIP IS NULL THEN '' ELSE TO_CHAR(C.NIP) END NIP_SATU
                      , CASE WHEN C.JABATAN IS NULL THEN '' ELSE C.JABATAN END JABATAN_SATU 
                      , CASE WHEN D.NAMA IS NULL THEN '' ELSE D.NAMA END NAMA_DUA
                      , CASE WHEN D.NIP IS NULL THEN '' ELSE TO_CHAR(D.NIP) END NIP_DUA
                      , CASE WHEN D.JABATAN IS NULL THEN '' ELSE D.JABATAN END JABATAN_DUA
                    FROM SIAK_USER_PLUS A 
                    LEFT JOIN SIAK_ATASAN B ON A.USER_ID = B.USER_ID 
                    LEFT JOIN SIAK_PEJABAT C ON B.ATASAN_SATU= C.PEJABAT_ID
                    LEFT JOIN SIAK_PEJABAT D ON B.ATASAN_DUA= D.PEJABAT_ID WHERE A.USER_ID = '$user_id'";
        $r = DB::select($sql);
        return $r[0];
    }
    public function act_get_rekap_absen_bulan($tgl)
    {
        $sql = "SELECT 
                        USER_ID USER_ID
                        , NAMA_LGKP NAMA
                        , '$tgl' BULAN
                        , CASE WHEN TGL_01 = 1 THEN 'HADIR' ELSE '-' END AS T1
                        , CASE WHEN TGL_02 = 1 THEN 'HADIR' ELSE '-' END AS T2
                        , CASE WHEN TGL_03 = 1 THEN 'HADIR' ELSE '-' END AS T3
                        , CASE WHEN TGL_04 = 1 THEN 'HADIR' ELSE '-' END AS T4
                        , CASE WHEN TGL_05 = 1 THEN 'HADIR' ELSE '-' END AS T5
                        , CASE WHEN TGL_06 = 1 THEN 'HADIR' ELSE '-' END AS T6
                        , CASE WHEN TGL_07 = 1 THEN 'HADIR' ELSE '-' END AS T7
                        , CASE WHEN TGL_08 = 1 THEN 'HADIR' ELSE '-' END AS T8
                        , CASE WHEN TGL_09 = 1 THEN 'HADIR' ELSE '-' END AS T9
                        , CASE WHEN TGL_10 = 1 THEN 'HADIR' ELSE '-' END AS T10
                        , CASE WHEN TGL_11 = 1 THEN 'HADIR' ELSE '-' END AS T11
                        , CASE WHEN TGL_12 = 1 THEN 'HADIR' ELSE '-' END AS T12
                        , CASE WHEN TGL_13 = 1 THEN 'HADIR' ELSE '-' END AS T13
                        , CASE WHEN TGL_14 = 1 THEN 'HADIR' ELSE '-' END AS T14
                        , CASE WHEN TGL_15 = 1 THEN 'HADIR' ELSE '-' END AS T15
                        , CASE WHEN TGL_16 = 1 THEN 'HADIR' ELSE '-' END AS T16
                        , CASE WHEN TGL_17 = 1 THEN 'HADIR' ELSE '-' END AS T17
                        , CASE WHEN TGL_18 = 1 THEN 'HADIR' ELSE '-' END AS T18
                        , CASE WHEN TGL_19 = 1 THEN 'HADIR' ELSE '-' END AS T19
                        , CASE WHEN TGL_20 = 1 THEN 'HADIR' ELSE '-' END AS T20
                        , CASE WHEN TGL_21 = 1 THEN 'HADIR' ELSE '-' END AS T21
                        , CASE WHEN TGL_22 = 1 THEN 'HADIR' ELSE '-' END AS T22
                        , CASE WHEN TGL_23 = 1 THEN 'HADIR' ELSE '-' END AS T23
                        , CASE WHEN TGL_24 = 1 THEN 'HADIR' ELSE '-' END AS T24
                        , CASE WHEN TGL_25 = 1 THEN 'HADIR' ELSE '-' END AS T25
                        , CASE WHEN TGL_26 = 1 THEN 'HADIR' ELSE '-' END AS T26
                        , CASE WHEN TGL_27 = 1 THEN 'HADIR' ELSE '-' END AS T27
                        , CASE WHEN TGL_28 = 1 THEN 'HADIR' ELSE '-' END AS T28
                        , CASE WHEN TGL_29 = 1 THEN 'HADIR' ELSE '-' END AS T29
                        , CASE WHEN TGL_30 = 1 THEN 'HADIR' ELSE '-' END AS T30
                        , CASE WHEN TGL_31 = 1 THEN 'HADIR' ELSE '-' END AS T31
                        , KEHADIRAN
                        , TEPAT_WAKTU TEPAT_WAKTU
                        , TERLAMBAT TERLAMBAT
                        FROM
                        (SELECT 
                        A.USER_ID,
                        A.NAMA_LGKP,
                        SUM( CASE WHEN TO_CHAR(B.TANGGAL,'DD-MM-YYYY') = '01-$tgl' AND (UPPER(KETERANGAN) <> 'IZIN' AND UPPER(KETERANGAN) <> 'SAKIT') THEN 1 ELSE 0 END) AS TGL_01,
                        SUM( CASE WHEN TO_CHAR(B.TANGGAL,'DD-MM-YYYY') = '02-$tgl' AND (UPPER(KETERANGAN) <> 'IZIN' AND UPPER(KETERANGAN) <> 'SAKIT') THEN 1 ELSE 0 END) AS TGL_02,
                        SUM( CASE WHEN TO_CHAR(B.TANGGAL,'DD-MM-YYYY') = '03-$tgl' AND (UPPER(KETERANGAN) <> 'IZIN' AND UPPER(KETERANGAN) <> 'SAKIT') THEN 1 ELSE 0 END) AS TGL_03,
                        SUM( CASE WHEN TO_CHAR(B.TANGGAL,'DD-MM-YYYY') = '04-$tgl' AND (UPPER(KETERANGAN) <> 'IZIN' AND UPPER(KETERANGAN) <> 'SAKIT') THEN 1 ELSE 0 END) AS TGL_04,
                        SUM( CASE WHEN TO_CHAR(B.TANGGAL,'DD-MM-YYYY') = '05-$tgl' AND (UPPER(KETERANGAN) <> 'IZIN' AND UPPER(KETERANGAN) <> 'SAKIT') THEN 1 ELSE 0 END) AS TGL_05,
                        SUM( CASE WHEN TO_CHAR(B.TANGGAL,'DD-MM-YYYY') = '06-$tgl' AND (UPPER(KETERANGAN) <> 'IZIN' AND UPPER(KETERANGAN) <> 'SAKIT') THEN 1 ELSE 0 END) AS TGL_06,
                        SUM( CASE WHEN TO_CHAR(B.TANGGAL,'DD-MM-YYYY') = '07-$tgl' AND (UPPER(KETERANGAN) <> 'IZIN' AND UPPER(KETERANGAN) <> 'SAKIT') THEN 1 ELSE 0 END) AS TGL_07,
                        SUM( CASE WHEN TO_CHAR(B.TANGGAL,'DD-MM-YYYY') = '08-$tgl' AND (UPPER(KETERANGAN) <> 'IZIN' AND UPPER(KETERANGAN) <> 'SAKIT') THEN 1 ELSE 0 END) AS TGL_08,
                        SUM( CASE WHEN TO_CHAR(B.TANGGAL,'DD-MM-YYYY') = '09-$tgl' AND (UPPER(KETERANGAN) <> 'IZIN' AND UPPER(KETERANGAN) <> 'SAKIT') THEN 1 ELSE 0 END) AS TGL_09,
                        SUM( CASE WHEN TO_CHAR(B.TANGGAL,'DD-MM-YYYY') = '10-$tgl' AND (UPPER(KETERANGAN) <> 'IZIN' AND UPPER(KETERANGAN) <> 'SAKIT') THEN 1 ELSE 0 END) AS TGL_10,
                        SUM( CASE WHEN TO_CHAR(B.TANGGAL,'DD-MM-YYYY') = '11-$tgl' AND (UPPER(KETERANGAN) <> 'IZIN' AND UPPER(KETERANGAN) <> 'SAKIT') THEN 1 ELSE 0 END) AS TGL_11,
                        SUM( CASE WHEN TO_CHAR(B.TANGGAL,'DD-MM-YYYY') = '12-$tgl' AND (UPPER(KETERANGAN) <> 'IZIN' AND UPPER(KETERANGAN) <> 'SAKIT') THEN 1 ELSE 0 END) AS TGL_12,
                        SUM( CASE WHEN TO_CHAR(B.TANGGAL,'DD-MM-YYYY') = '13-$tgl' AND (UPPER(KETERANGAN) <> 'IZIN' AND UPPER(KETERANGAN) <> 'SAKIT') THEN 1 ELSE 0 END) AS TGL_13,
                        SUM( CASE WHEN TO_CHAR(B.TANGGAL,'DD-MM-YYYY') = '14-$tgl' AND (UPPER(KETERANGAN) <> 'IZIN' AND UPPER(KETERANGAN) <> 'SAKIT') THEN 1 ELSE 0 END) AS TGL_14,
                        SUM( CASE WHEN TO_CHAR(B.TANGGAL,'DD-MM-YYYY') = '15-$tgl' AND (UPPER(KETERANGAN) <> 'IZIN' AND UPPER(KETERANGAN) <> 'SAKIT') THEN 1 ELSE 0 END) AS TGL_15,
                        SUM( CASE WHEN TO_CHAR(B.TANGGAL,'DD-MM-YYYY') = '16-$tgl' AND (UPPER(KETERANGAN) <> 'IZIN' AND UPPER(KETERANGAN) <> 'SAKIT') THEN 1 ELSE 0 END) AS TGL_16,
                        SUM( CASE WHEN TO_CHAR(B.TANGGAL,'DD-MM-YYYY') = '17-$tgl' AND (UPPER(KETERANGAN) <> 'IZIN' AND UPPER(KETERANGAN) <> 'SAKIT') THEN 1 ELSE 0 END) AS TGL_17,
                        SUM( CASE WHEN TO_CHAR(B.TANGGAL,'DD-MM-YYYY') = '18-$tgl' AND (UPPER(KETERANGAN) <> 'IZIN' AND UPPER(KETERANGAN) <> 'SAKIT') THEN 1 ELSE 0 END) AS TGL_18,
                        SUM( CASE WHEN TO_CHAR(B.TANGGAL,'DD-MM-YYYY') = '19-$tgl' AND (UPPER(KETERANGAN) <> 'IZIN' AND UPPER(KETERANGAN) <> 'SAKIT') THEN 1 ELSE 0 END) AS TGL_19,
                        SUM( CASE WHEN TO_CHAR(B.TANGGAL,'DD-MM-YYYY') = '20-$tgl' AND (UPPER(KETERANGAN) <> 'IZIN' AND UPPER(KETERANGAN) <> 'SAKIT') THEN 1 ELSE 0 END) AS TGL_20,
                        SUM( CASE WHEN TO_CHAR(B.TANGGAL,'DD-MM-YYYY') = '21-$tgl' AND (UPPER(KETERANGAN) <> 'IZIN' AND UPPER(KETERANGAN) <> 'SAKIT') THEN 1 ELSE 0 END) AS TGL_21,
                        SUM( CASE WHEN TO_CHAR(B.TANGGAL,'DD-MM-YYYY') = '22-$tgl' AND (UPPER(KETERANGAN) <> 'IZIN' AND UPPER(KETERANGAN) <> 'SAKIT') THEN 1 ELSE 0 END) AS TGL_22,
                        SUM( CASE WHEN TO_CHAR(B.TANGGAL,'DD-MM-YYYY') = '23-$tgl' AND (UPPER(KETERANGAN) <> 'IZIN' AND UPPER(KETERANGAN) <> 'SAKIT') THEN 1 ELSE 0 END) AS TGL_23,
                        SUM( CASE WHEN TO_CHAR(B.TANGGAL,'DD-MM-YYYY') = '24-$tgl' AND (UPPER(KETERANGAN) <> 'IZIN' AND UPPER(KETERANGAN) <> 'SAKIT') THEN 1 ELSE 0 END) AS TGL_24,
                        SUM( CASE WHEN TO_CHAR(B.TANGGAL,'DD-MM-YYYY') = '25-$tgl' AND (UPPER(KETERANGAN) <> 'IZIN' AND UPPER(KETERANGAN) <> 'SAKIT') THEN 1 ELSE 0 END) AS TGL_25,
                        SUM( CASE WHEN TO_CHAR(B.TANGGAL,'DD-MM-YYYY') = '26-$tgl' AND (UPPER(KETERANGAN) <> 'IZIN' AND UPPER(KETERANGAN) <> 'SAKIT') THEN 1 ELSE 0 END) AS TGL_26,
                        SUM( CASE WHEN TO_CHAR(B.TANGGAL,'DD-MM-YYYY') = '27-$tgl' AND (UPPER(KETERANGAN) <> 'IZIN' AND UPPER(KETERANGAN) <> 'SAKIT') THEN 1 ELSE 0 END) AS TGL_27,
                        SUM( CASE WHEN TO_CHAR(B.TANGGAL,'DD-MM-YYYY') = '28-$tgl' AND (UPPER(KETERANGAN) <> 'IZIN' AND UPPER(KETERANGAN) <> 'SAKIT') THEN 1 ELSE 0 END) AS TGL_28,
                        SUM( CASE WHEN TO_CHAR(B.TANGGAL,'DD-MM-YYYY') = '29-$tgl' AND (UPPER(KETERANGAN) <> 'IZIN' AND UPPER(KETERANGAN) <> 'SAKIT') THEN 1 ELSE 0 END) AS TGL_29,
                        SUM( CASE WHEN TO_CHAR(B.TANGGAL,'DD-MM-YYYY') = '30-$tgl' AND (UPPER(KETERANGAN) <> 'IZIN' AND UPPER(KETERANGAN) <> 'SAKIT') THEN 1 ELSE 0 END) AS TGL_30,
                        SUM( CASE WHEN TO_CHAR(B.TANGGAL,'DD-MM-YYYY') = '31-$tgl' AND (UPPER(KETERANGAN) <> 'IZIN' AND UPPER(KETERANGAN) <> 'SAKIT') THEN 1 ELSE 0 END) AS TGL_31,
                        SUM( CASE WHEN TO_CHAR(B.TANGGAL,'MM-YYYY') = '$tgl' AND KETERANGAN IS NOT NULL THEN 1 ELSE 0 END) AS KEHADIRAN,
                        SUM( CASE WHEN TO_CHAR(B.TANGGAL,'MM-YYYY') = '$tgl' AND KETERANGAN <> 'Terlambat' THEN 1 ELSE 0 END) AS TEPAT_WAKTU,
                        SUM( CASE WHEN TO_CHAR(B.TANGGAL,'MM-YYYY') = '$tgl' AND KETERANGAN = 'Terlambat' THEN 1 ELSE 0 END) AS TERLAMBAT
                        FROM SIAK_USER_PLUS A
                        LEFT JOIN SIAK_ABSENSI B ON A.USER_ID =B.USER_ID
                        WHERE ABSENSI_CHECKING = 1 
                        GROUP BY A.USER_ID,A.NAMA_LGKP
                        ORDER BY UPPER(A.USER_ID))";
        $r = DB::select($sql);
        return $r;
    }

    //DETAIL KK
    public function act_rekap_detail_kk($user_id, $tgl_start, $tgl_end, $no_kec = 0, $no_kel = 0)
    {
        $sql = "";
        $sql .= "SELECT A.NO_KK, A.NAMA_KEP, B.USER_ID, TO_CHAR(A.PRINTED_DATE,'DD-MM-YYYY') TGL_CETAK,CASE WHEN C.REQ_DATE IS NULL THEN '-' ELSE TO_CHAR(C.REQ_DATE,'DD-MM-YYYY') END TGL_REQ, A.NO_KEC, A.NO_KEL FROM T5_SEQN_PRINT_KK A INNER JOIN SIAK_USER_PLUS B ON A.PRINTED_BY = B.USER_SIAK LEFT JOIN 
                 (
                            SELECT 
                            NO_DOC
                            , CERT_STATUS
                            , REQ_DATE
                            , REQ_BY
                            , SEQN_ID
                            , URL_DOKUMEN
                            , PEJABAT_PROCCESS_BY
                            , PEJABAT_PROCESS_DATE  
                          FROM 
                          (
                            SELECT 
                            NO_DOC
                            , CERT_STATUS
                            , REQ_DATE
                            , REQ_BY
                            , SEQN_ID
                            , URL_DOKUMEN
                            , PEJABAT_PROCCESS_BY
                            , PEJABAT_PROCESS_DATE 
                            , RANK() OVER (PARTITION BY NO_DOC ORDER BY REQ_DATE DESC,SEQN_ID DESC) RNK FROM BSRE_KARTU_KELUARGA) 
                            WHERE RNK = 1) C ON A.NO_KK = C.NO_DOC 
                WHERE B.USER_ID = '$user_id' AND A.PRINTED_DATE >= TO_DATE('$tgl_start','DD-MM-YYYY') AND A.PRINTED_DATE < TO_DATE('$tgl_end','DD-MM-YYYY')+1";
        if ($no_kec != 0)
        {
            $sql .= " AND A.NO_KEC = $no_kec ";
        }
        if ($no_kel != 0)
        {
            $sql .= " AND A.NO_KEL = $no_kel ";
        }
        $sql .= "ORDER BY A.PRINTED_DATE DESC";
        $r = DB::select($sql);
        return $r;
    }
    public function act_rekap_detail_kk_pdf($user_id, $tgl_start, $tgl_end, $no_kec = 0, $no_kel = 0)
    {
        $sql = "";
        $sql .= "SELECT A.NO_KK, A.NAMA_KEP, B.USER_ID, TO_CHAR(A.PRINTED_DATE,'DD-MM-YYYY') TGL_CETAK,CASE WHEN C.REQ_DATE IS NULL THEN '-' ELSE TO_CHAR(C.REQ_DATE,'DD-MM-YYYY') END TGL_REQ, A.NO_KEC, A.NO_KEL FROM T5_SEQN_PRINT_KK A INNER JOIN SIAK_USER_PLUS B ON A.PRINTED_BY = B.USER_SIAK LEFT JOIN 
                 (
                            SELECT 
                            NO_DOC
                            , CERT_STATUS
                            , REQ_DATE
                            , REQ_BY
                            , SEQN_ID
                            , URL_DOKUMEN
                            , PEJABAT_PROCCESS_BY
                            , PEJABAT_PROCESS_DATE  
                          FROM 
                          (
                            SELECT 
                            NO_DOC
                            , CERT_STATUS
                            , REQ_DATE
                            , REQ_BY
                            , SEQN_ID
                            , URL_DOKUMEN
                            , PEJABAT_PROCCESS_BY
                            , PEJABAT_PROCESS_DATE 
                            , RANK() OVER (PARTITION BY NO_DOC ORDER BY REQ_DATE DESC,SEQN_ID DESC) RNK FROM BSRE_KARTU_KELUARGA) 
                            WHERE RNK = 1) C ON A.NO_KK = C.NO_DOC 
                WHERE B.USER_ID = '$user_id' AND A.PRINTED_DATE >= TO_DATE('$tgl_start','DD-MM-YYYY') AND A.PRINTED_DATE < TO_DATE('$tgl_end','DD-MM-YYYY')+1";
        if ($no_kec != 0)
        {
            $sql .= " AND A.NO_KEC = $no_kec ";
        }
        if ($no_kel != 0)
        {
            $sql .= " AND A.NO_KEL = $no_kel ";
        }
        $sql .= "ORDER BY A.PRINTED_DATE DESC";
        $r = DB::select($sql);
        return $r;
    }
    // REKAP KEDATANGAN
    public function act_rekap_kedatangan($tgl_start, $tgl_end, $no_kec = 0, $no_kel = 0)
    {
        $sql = "";
        $sql .= "SELECT 
                        F5_GET_NAMA_KECAMATAN(A.NO_PROP,A.NO_KAB, A.NO_KEC) KEC
                        , F5_GET_NAMA_KELURAHAN(A.NO_PROP,A.NO_KAB, A.NO_KEC, A.NO_KEL) KEL
                        , CASE WHEN A.MK_RW IS NULL AND A.NK_RW IS NULL THEN 0 WHEN A.MK_RW IS NULL THEN A.NK_RW ELSE A.MK_RW END RW
                        , CASE WHEN A.MK_RT IS NULL AND A.NK_RT IS NULL THEN 0 WHEN A.MK_RT IS NULL THEN A.NK_RT ELSE A.MK_RT END RT
                        , CASE WHEN A.MK_ALAMAT IS NULL AND A.NK_ALAMAT IS NULL THEN '-' WHEN A.MK_ALAMAT IS NULL THEN A.NK_ALAMAT ELSE A.MK_ALAMAT END ALAMAT 
                        , A.NO_DATANG
                        , A.NO_PINDAH
                        , F5_GET_NAMA_PROVINSI(A.SRC_PROP) PROP_ASAL
                        , F5_GET_NAMA_KABUPATEN(A.SRC_PROP,A.SRC_KAB) KAB_ASAL
                        , F5_GET_NAMA_KECAMATAN(A.SRC_PROP,A.SRC_KAB, A.SRC_KEC) KEC_ASAL
                        , F5_GET_NAMA_KELURAHAN(A.SRC_PROP,A.SRC_KAB, A.SRC_KEC, A.SRC_KEL) KEL_ASAL
                        , TO_CHAR(A.NO_KK) NO_KK
                        , TO_CHAR(B.NIK) NIK_PEMOHON
                        , B.NAMA_LENGKAP NAMA_PEMOHON
                        , TO_CHAR(A.CREATED_DATE,'DD-MM-YYYY') TGL
                        , TO_CHAR(A.CREATED_DATE,'HH24:MM:SS') JAM
                    FROM DATANG_HEADER A INNER JOIN DATANG_DETAIL B ON A.NO_DATANG = B.NO_DATANG WHERE 
                    A.CREATED_DATE >= TO_DATE('" . $tgl_start . "','DD-MM-YYYY') AND A.CREATED_DATE < TO_DATE('" . $tgl_end . "','DD-MM-YYYY')+1 
                    AND KLASIFIKASI_PINDAH >= 4 AND A.NO_PROP = 32 AND A.NO_KAB = 73";
        if ($no_kec != 0)
        {
            $sql .= " AND A.NO_KEC = $no_kec ";
        }
        if ($no_kel != 0)
        {
            $sql .= " AND A.NO_KEL = $no_kel ";
        }
        $sql .= " ORDER BY A.CREATED_DATE DESC";
        $r = DB::connection('db222')->select($sql);
        return $r;
    }
    public function act_rekap_detail_kk_bulan($user_id, $bln, $no_kec = 0, $no_kel = 0, $sql_kel = '')
    {
        if ($no_kec != 0)
        {
            $sql = "SELECT * FROM
                    (
                       SELECT TO_CHAR(A.PRINTED_DATE,'DD-MM-YYYY') PRINTED_DATE,A.NO_KK, F5_GET_NAMA_KELURAHAN(A.NO_PROP,A.NO_KAB,A.NO_KEC,A.NO_KEL) NAMA_KEL
                        FROM T5_SEQN_PRINT_KK A INNER JOIN SIAK_USER_PLUS B ON A.PRINTED_BY = B.USER_SIAK WHERE TO_CHAR(A.PRINTED_DATE,'MM-YYYY') = '$bln' AND A.NO_KEC = $no_kec AND B.USER_ID ='$user_id'
                    )
                    PIVOT (COUNT(NO_KK) FOR (NAMA_KEL) IN ($sql_kel))
                    ORDER BY PRINTED_DATE";
        }
        else
        {
            $sql = "SELECT * FROM
                    (
                       SELECT TO_CHAR(A.PRINTED_DATE,'DD-MM-YYYY') PRINTED_DATE,A.NO_KK, F5_GET_NAMA_KECAMATAN(A.NO_PROP,A.NO_KAB,A.NO_KEC) NAMA_KEC
                        FROM T5_SEQN_PRINT_KK A INNER JOIN SIAK_USER_PLUS B ON A.PRINTED_BY = B.USER_SIAK WHERE TO_CHAR(A.PRINTED_DATE,'MM-YYYY') = '$bln' AND B.USER_ID ='$user_id'
                    )
                    PIVOT (COUNT(NO_KK) FOR (NAMA_KEC) IN ($sql_kel))
                    ORDER BY PRINTED_DATE";
        }

        $r = DB::select($sql);
        return $r;
    }
    public function act_rekap_kk($user_id, $tgl_start, $tgl_end, $no_kec = 0)
    {
        if ($no_kec != 0)
        {
            $sql = "SELECT 
                      A.NO_KEL NO_WIL
                      , F5_GET_NAMA_KELURAHAN(A.NO_PROP,A.NO_KAB,A.NO_KEC,A.NO_KEL) NAMA_WIL
                      , COUNT(1) JML 
                      FROM T5_SEQN_PRINT_KK A 
                      INNER JOIN SIAK_USER_PLUS B ON A.PRINTED_BY = B.USER_SIAK 
                      WHERE B.USER_ID = '$user_id' AND A.NO_KEC = $no_kec 
                      AND A.PRINTED_DATE >= TO_DATE('$tgl_start','DD-MM-YYYY') AND A.PRINTED_DATE < TO_DATE('$tgl_end','DD-MM-YYYY')+1
                      GROUP BY A.NO_PROP, A.NO_KAB, A.NO_KEC, A.NO_KEL ORDER BY A.NO_PROP, A.NO_KAB, A.NO_KEC, A.NO_KEL";
        }
        else
        {
            $sql = "SELECT 
                      A.NO_KEC NO_WIL
                      , F5_GET_NAMA_KECAMATAN(A.NO_PROP,A.NO_KAB,A.NO_KEC) NAMA_WIL
                      , COUNT(1) JML 
                      FROM T5_SEQN_PRINT_KK A 
                      INNER JOIN SIAK_USER_PLUS B ON A.PRINTED_BY = B.USER_SIAK 
                      WHERE B.USER_ID = '$user_id' 
                      AND A.PRINTED_DATE >= TO_DATE('$tgl_start','DD-MM-YYYY') AND A.PRINTED_DATE < TO_DATE('$tgl_end','DD-MM-YYYY')+1
                      GROUP BY A.NO_PROP, A.NO_KAB, A.NO_KEC ORDER BY A.NO_PROP, A.NO_KAB, A.NO_KEC";
        }

        $r = DB::select($sql);
        return $r;
    }
    public function act_mepeling_lhr($user_id, $tgl_start, $tgl_end)
    {
        $sql = "SELECT B.NAMA_LGKP, F5_GET_NAMA_KECAMATAN(32,73,A.ADM_NO_KEC) NAMA_WIL, COUNT(1) JML FROM CAPIL_LAHIR A INNER JOIN T5_SIAK_USER B ON A.USER_ID = B.USER_ID
                WHERE A.USER_ID = '$user_id' AND A.ADM_TGL_ENTRY >= TO_DATE('$tgl_start','DD-MM-YYYY') AND A.ADM_TGL_ENTRY < TO_DATE('$tgl_end','DD-MM-YYYY')+1
                GROUP BY B.NAMA_LGKP, A.ADM_NO_KEC ORDER BY A.ADM_NO_KEC";
        $r = DB::connection('db222')->select($sql);
        return $r;
    }
    public function act_mepeling_kmt($user_id, $tgl_start, $tgl_end)
    {
        $sql = "SELECT B.NAMA_LGKP, F5_GET_NAMA_KECAMATAN(32,73,A.ADM_NO_KEC) NAMA_WIL, COUNT(1) JML FROM CAPIL_MATI A INNER JOIN T5_SIAK_USER B ON A.USER_ID = B.USER_ID
                WHERE A.USER_ID = '$user_id' AND A.ADM_TGL_ENTRY >= TO_DATE('$tgl_start','DD-MM-YYYY') AND A.ADM_TGL_ENTRY < TO_DATE('$tgl_end','DD-MM-YYYY')+1
                GROUP BY B.NAMA_LGKP, A.ADM_NO_KEC ORDER BY A.ADM_NO_KEC";
        $r = DB::connection('db222')->select($sql);
        return $r;
    }
    public function act_get_pivot_kel($no_kec = 0)
    {
        $sql = "SELECT TO_CHAR(WM_CONCAT(NO_KEL)) VAL FROM SETUP_KEL WHERE NO_PROP=32 AND NO_KAB = 73 AND NO_KEC =$no_kec GROUP BY NO_KEC";
        $r = DB::select($sql);
        return $r[0]->val;
    }
    public function act_get_pivot_kel_str($no_kec = 0)
    {
        if ($no_kec != 0)
        {
            $sql = "SELECT TO_CHAR(WM_CONCAT(CONCAT(CONCAT(CONCAT(CONCAT('''',NAMA_KEL),''''),' AS C'),ROWNUM))) VAL FROM (SELECT NO_PROP, NO_KAB, NO_KEC, NO_KEL, NAMA_KEL FROM SETUP_KEL ORDER BY NO_KEL) WHERE NO_PROP=32 AND NO_KAB = 73 AND NO_KEC =$no_kec GROUP BY NO_KEC";
        }
        else
        {
            $sql = "SELECT TO_CHAR(WM_CONCAT(CONCAT(CONCAT(CONCAT(CONCAT('''',NAMA_KEC),''''),' AS C'),ROWNUM))) VAL FROM (SELECT NO_PROP, NO_KAB, NO_KEC, NAMA_KEC FROM SETUP_KEC ORDER BY NO_KEC) WHERE NO_PROP=32 AND NO_KAB = 73  GROUP BY NO_KAB";
        }

        $r = DB::select($sql);
        return $r[0]->val;
    }
    public function act_get_jum_kel($no_kec = 0)
    {
        if ($no_kec != 0)
        {
            $sql = "SELECT NAMA_KEL NAMA_WIL FROM SETUP_KEL WHERE NO_PROP=32 AND NO_KAB = 73 AND NO_KEC =$no_kec ORDER BY NO_KEL";
        }
        else
        {
            $sql = "SELECT NAMA_KEC NAMA_WIL FROM SETUP_KEC WHERE NO_PROP=32 AND NO_KAB = 73 ORDER BY NO_KEC";
        }
        $r = DB::select($sql);
        return $r;
    }
    public function lgn_change_pwd($user_id, $new_password)
        {
                $sql = "UPDATE SIAK_USER_PLUS SET USER_PWD = '$new_password'  WHERE USER_ID = '$user_id'";
                $r = DB::update($sql);
        }
    public function act_edit_wil_mpl($no_wil,$user_id)
        {
            if($no_wil != 0){
                $sql = "UPDATE T5_SIAK_USER SET NO_KEC = $no_wil , USER_LEVEL = 29  WHERE USER_ID = '$user_id'";
            }else{
                $sql = "UPDATE T5_SIAK_USER SET NO_KEC = 0 , USER_LEVEL = 13  WHERE USER_ID = '$user_id'";
            }
                $r = DB::connection('db222')->update($sql);
        }

    public function act_get_salaman_batal($nik)
    {
        $sql = "SELECT A.ID, A.NIK, A.NAMA_LGKP, CASE WHEN B.TELEPON IS NULL THEN '-' ELSE B.TELEPON END TELEPON, CASE WHEN B.EMAIL IS NULL THEN '-' ELSE B.EMAIL END EMAIL, CASE WHEN A.VERIFIED_AT IS NULL THEN '-' ELSE TO_CHAR(A.VERIFIED_AT,'DD-MM-YYYY HH24:MI') END VERIFIED_AT, CASE WHEN A.VERIFIED_BY IS NULL THEN '-' ELSE  A.VERIFIED_BY END VERIFIED_BY FROM PENGAJUAN A INNER JOIN USERS B ON A.NIK = B.NIK WHERE A.PROC_STAT =2  AND A.NIK = $nik";
        $r = DB::connection('webdb')->select($sql);
        return $r;
    }

    public function act_edit_pengajuan_batal($pengajuan_id)
    {
        $sql = "UPDATE PENGAJUAN SET PROC_STAT = 3, STAT_BERKAS = NULL WHERE ID = $pengajuan_id AND PROC_STAT = 2";
        $r = DB::connection('webdb')->update($sql);
        $sql = "UPDATE NOTIFICATION A SET A.DELETED_AT = SYSDATE WHERE EXISTS (SELECT 1 FROM PENGAJUAN B WHERE A.NIK = B.NIK AND ID = $pengajuan_id)";
        $r = DB::connection('webdb')->update($sql);
        return $r;
    }

    public function tgl_indo($tanggal){
    $bulan = array (
        1 =>   'Januari',
        'Februari',
        'Maret',
        'April',
        'Mei',
        'Juni',
        'Juli',
        'Agustus',
        'September',
        'Oktober',
        'November',
        'Desember'
    );
    $pecahkan = explode('-', $tanggal);
    
    // variabel pecahkan 0 = tanggal
    // variabel pecahkan 1 = bulan
    // variabel pecahkan 2 = tahun
 
    return  $bulan[ (int)$pecahkan[0] ] . ' ' . $pecahkan[1];
    }

    public function get_absensi_v(Request $request)
    {

        $draw = intval($request->draw);
        $start = intval($request->start);
        $length = intval($request->length);
        $rekap_pengajuan = $this->ept_get_absensi_v();
        $data = array();
        $i = 0;
        foreach ($rekap_pengajuan as $r)
        {
            if ($r->user_id == Session::get('S_USER_ID')){
                $aksi = '<button type="button" class="btn btn-info btn-rounded m-t-10 mb-2" disabled>
                      Waiting Approval
                    </button>
                    ';
            }else{
                $aksi = '
                <button class="btn btn-success btn-circle" id="btn-proses" onclick="terima('.$r->daily_id.',\''.str_replace("'", "", $r->nama_lgkp).'\');" ><i class="fa fa-check"></i></button>
                    &nbsp <button type="button" class="btn btn-danger btn-circle" id="btn-pesan" onclick="tolak('.$r->daily_id.');" ><i class="fa fa-close"></i></button>
                    ';
            }
            
            $i++;
            $data[] = array(
                "no" => $i,
                "user_id" => $r->user_id,
                "nama_lgkp" => $r->nama_lgkp,
                "activity" => $r->activity,
                "description" => $r->description,
                "created_dt" => $r->created_dt,
                "aksi" => $aksi
                );
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => count($rekap_pengajuan) ,
            "recordsFiltered" => count($rekap_pengajuan) ,
            "data" => $data
        );
        return $output;
        exit();
    }

    public function get_absensi_vr(Request $request)
    {

        $draw = intval($request->draw);
        $start = intval($request->start);
        $length = intval($request->length);
        $user_id = $request->user_id;
        $stat_ver = $request->stat_ver;
        $tgl = $request->tgl;
        $tgl_start = substr($tgl, 0, 10);
        $tgl_end = substr($tgl,13, 10);
        $rekap_pengajuan = $this->ept_get_absensi_vr($tgl_start,$tgl_end,$user_id,$stat_ver);

        $data = array();
        $i = 0;
        foreach ($rekap_pengajuan as $r)
        {
            if ($r->is_valid == 0){
                $aksi = '<button type="button" class="btn btn-info btn m-t-10 mb-2">
                      Menunggu
                    </button>';
            }else if ($r->is_valid == 1){
               $aksi = '<button type="button" class="btn btn-success btn m-t-10 mb-2">
                      Disetujui
                    </button>';
            }else {
                $aksi = '<button type="button" onclick="gettolak('.$r->daily_id.');" class="btn btn-danger btn m-t-10 mb-2">
                      DiTolak
                    </button>';
            }
            
            $i++;
            $data[] = array(
                "no" => $i,
                "user_id" => $r->user_id,
                "nama_lgkp" => $r->nama_lgkp,
                "activity" => $r->activity,
                "description" => $r->description,
                "created_dt" => $r->created_dt,
                "aksi" => $aksi
                );
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => count($rekap_pengajuan) ,
            "recordsFiltered" => count($rekap_pengajuan) ,
            "data" => $data
        );
        return $output;
        exit();
    }
    public function get_daily_acc(Request $request){
        header('Content-type: application/json');
        if ($request->daily_id != null){
            $daily_id = $request->daily_id;
            $this->ept_get_daily_acc($daily_id);
            $output = array(
                    "message_type"=>1,
                    "message"=>"Aktifitas Ini Telah Diverifikasi !"
            );
            return $output; 
        }
    }
    public function get_daily_tlk(Request $request){
        header('Content-type: application/json');
        if ($request->daily_id != null){
            $daily_id = $request->daily_id;
            $isi_pesan = $request->isi_pesan;
            $this->ept_insert_daily_tlk($daily_id,$isi_pesan);
            $this->ept_get_daily_tlk($daily_id);
            $output = array(
                    "message_type"=>1,
                    "message"=>"Aktifitas Ini Telah Diverifikasi !"
            );
            return $output; 
        }
    }
    public function get_tolak(Request $request){
        header('Content-type: application/json');
        if ($request->daily_id != null){
            $daily_id = $request->daily_id;
            $data = $this->ept_get_tolak($daily_id);
            $output = array(
                    "message_type"=>1,
                    "message"=>$data
            );
            return $output; 
        }
    }

    public function ept_get_absensi_v()
    {
        header("Content-Type: application/json", true);
         $sql = "SELECT A.DAILY_ID,A.USER_ID, B.NAMA_LGKP,C.ACTIVITY,A.DESCRIPTION, TO_CHAR(A.CREATED_DT,'DD-MM-YYYY') CREATED_DT, A.IS_VALID FROM SIAK_DAILY A INNER JOIN SIAK_ACTIVITY C ON A.ACTIVITY_ID = C.ACTIVITY_ID INNER JOIN SIAK_USER_PLUS B ON A.USER_ID = B.USER_ID WHERE A.IS_VALID = 0  AND (B.VERIVIKATOR IN ('".Session::get('S_USER_ID')."') OR A.USER_ID = '".Session::get('S_USER_ID')."')  ORDER BY A.CREATED_DT
         ";
         $r = DB::select($sql);
        return $r;
    }

    public function ept_get_absensi_vr($tgl_start,$tgl_end,$user_id,$stat_ver)
    {
        header("Content-Type: application/json", true);
         $sql = "";
         $sql .= "SELECT A.DAILY_ID,A.USER_ID, B.NAMA_LGKP,C.ACTIVITY,A.DESCRIPTION, TO_CHAR(A.CREATED_DT,'DD-MM-YYYY') CREATED_DT, A.IS_VALID FROM SIAK_DAILY A INNER JOIN SIAK_ACTIVITY C ON A.ACTIVITY_ID = C.ACTIVITY_ID INNER JOIN SIAK_USER_PLUS B ON A.USER_ID = B.USER_ID 
            WHERE 1=1 AND  A.CREATED_DT >= TO_DATE('".$tgl_start."','DD-MM-YYYY') AND A.CREATED_DT < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1 ";

         if($stat_ver <> 3){
            $sql .="AND A.IS_VALID = '$stat_ver' ";   
         }

         if($user_id <> '0'){
            $sql .="AND  A.USER_ID = '$user_id' ";   
         }
         $sql .="ORDER BY A.CREATED_DT";
         $r = DB::select($sql);
        return $r;
    }
    public function ept_get_daily_acc($id){
            $sql = "UPDATE SIAK_DAILY SET IS_VALID = 1 WHERE DAILY_ID =  $id";
            $r = DB::update($sql);
            return $r;
    }
    public function ept_get_daily_tlk($id){
            $sql = "UPDATE SIAK_DAILY SET IS_VALID = 2 WHERE DAILY_ID =  $id";
            $r = DB::update($sql);
            return $r;
    }
    public function ept_get_tolak($id){
            $sql = "SELECT DESCRIPTION FROM SIAK_DAILY_EXCUSE WHERE DAILY_ID =  $id";
            $r = DB::select($sql);
             return $r[0]->description;
    }
    public function ept_insert_daily_tlk($daily_id,$isi_pesan)
    {
        $sql = "INSERT INTO SIAK_DAILY_EXCUSE (DAILY_ID, DESCRIPTION, CREATED_BY, CREATED_DT) VALUES ($daily_id,UPPER('$isi_pesan'),'".Session::get('S_USER_ID')."',SYSDATE)";
        $r = DB::insert($sql);
        return $r;
    }

    
}

