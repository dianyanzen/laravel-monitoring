<?php

namespace App\Http\Controllers;

use DB;
use Session;
use DateTime;
use PDF;
use Excel;
use App\User; 
use App\Message;
use App\Events\NewMessageNotification;
use Jenssegers\Date\Date;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Http\Controllers\SharedController as Shr;
use App\Http\Controllers\AndroidApiController as AndrApi;
use GuzzleHttp\Client;




use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class EpuntenBackController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function cek_skts(Request $request)
    {
        Shr::check_login();
        if (Session::get('S_USER_ID') != null) 
        {
            $menu_id = 310;
            $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
            if ($is_akses == 0){
                 return redirect()->route('404Notfound');
            }
            $menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
            $isakses_kec = Shr::get_give_kec();
            $isakses_kel = Shr::get_give_kel();
          if($request->nik != null || $request->no_kk != null || $request->nama_lgkp != null || $request->tgl_lhr != null){
            $nik = $request->nik;
                if (substr($nik, 0, 1) === ','){
                    $nik = ltrim($nik, ',');
                }
          if (substr($nik, -1, 1) === ','){
            $nik = rtrim($nik, ',');
          }
                $no_kk = $request->no_kk;
                if (substr($no_kk, 0, 1) === ','){
                    $no_kk = ltrim($no_kk, ',');
                }
          if (substr($no_kk, -1, 1) === ','){
            $no_kk = rtrim($no_kk, ',');
          }
          
                $nama_lgkp = $request->nama_lgkp;
                $tgl_lhr = $request->tgl_lhr;
                $no_kec = $request->no_kec;
                $no_kel = $request->no_kel;
                $r = $this->scr_cek_data_skts($nik,$no_kk,$nama_lgkp,$tgl_lhr,$no_kec,$no_kel);
                $data = array(
                    "stitle"=>'Cek Pengajuan',
                    "mtitle"=>'Cek Pengajuan',
                    "my_url"=>'cek_skts',
                    "type_tgl"=>'Cetak',
                    "data"=>$r,
                    "jumlah"=>count($r),
                    "menu"=>$menu,
                    "akses_kec"=>$isakses_kec,
                    "akses_kel"=>$isakses_kel,
                    "user_id"=>$request->session()->get('S_USER_ID'),
                    "user_nik"=>$request->session()->get('S_NIK'),
                    "user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
                    "user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
                    "user_level"=>$request->session()->get('S_USER_LEVEL'),
                    "user_no_kec"=>$request->session()->get('S_NO_KEC'),
                    "user_no_kel"=>$request->session()->get('S_NO_KEL'),
                    "user_level"=>$request->session()->get('S_USER_LEVEL')
                );
                }else{
                $data = array(
                    "stitle"=>'Cek Pengajuan',
                    "mtitle"=>'Cek Pengajuan',
                    "my_url"=>'cek_skts',
                    "type_tgl"=>'Pengajuan',
                    "menu"=>$menu,
                    "akses_kec"=>$isakses_kec,
                    "akses_kel"=>$isakses_kel,
                    "user_id"=>$request->session()->get('S_USER_ID'),
                    "user_nik"=>$request->session()->get('S_NIK'),
                    "user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
                    "user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
                    "user_level"=>$request->session()->get('S_USER_LEVEL'),
                    "user_no_kec"=>$request->session()->get('S_NO_KEC'),
                    "user_no_kel"=>$request->session()->get('S_NO_KEL'),
                    "user_level"=>$request->session()->get('S_USER_LEVEL')
                );
            }
            return view('ceknik_skts/main',$data);
        }else{
            return redirect()->route('logout');
        }
    }

    public function epunten_chat(Request $request)
    {
        Shr::check_login();
        if (Session::get('S_USER_ID') != null) 
        {
            $menu_id = 284;
           $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
            if ($is_akses == 0){
                 return redirect()->route('404Notfound');
            }
            $menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
            $isakses_kec = Shr::get_give_kec();
            $isakses_kel = Shr::get_give_kel();
            $data = array(
                "stitle"=>'Chat Message',
                "mtitle"=>'Chat Message',
                "my_url"=>'chat',
                "type_tgl"=>'chat_date',
                "menu"=>$menu,
                "akses_kec"=>$isakses_kec,
                "akses_kel"=>$isakses_kel,
                "user_id"=>$request->session()->get('S_USER_ID'),
                "user_nik"=>$request->session()->get('S_NIK'),
                "user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
                "user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
                "user_level"=>$request->session()->get('S_USER_LEVEL'),
                "user_no_kec"=>$request->session()->get('S_NO_KEC'),
                "user_no_kel"=>$request->session()->get('S_NO_KEL'),
            );
            return view('epunten_chat/main',$data);
        }else{
            return redirect()->route('logout');
        }
    }

    public function epunten_cetak(Request $request)
    {
        Shr::check_login();
        if (Session::get('S_USER_ID') != null) 
        {
            $menu_id = 284;
           $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
            if ($is_akses == 0){
                 return redirect()->route('404Notfound');
            }
            $menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
            $isakses_kec = Shr::get_give_kec();
            $isakses_kel = Shr::get_give_kel();
            $data = array(
                "stitle"=>'Cetak Kartu',
                "mtitle"=>'Cetak Kartu',
                "my_url"=>'cetak',
                "type_tgl"=>'chat_date',
                "menu"=>$menu,
                "akses_kec"=>$isakses_kec,
                "akses_kel"=>$isakses_kel,
                "user_id"=>$request->session()->get('S_USER_ID'),
                "user_nik"=>$request->session()->get('S_NIK'),
                "user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
                "user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
                "user_level"=>$request->session()->get('S_USER_LEVEL'),
                "user_no_kec"=>$request->session()->get('S_NO_KEC'),
                "user_no_kel"=>$request->session()->get('S_NO_KEL'),
            );
            return view('epunten_cetak/main',$data);
        }else{
            return redirect()->route('logout');
        }
    }

    public function epunten_chat_warga(Request $request)
    {
        Shr::check_login();
        if (Session::get('S_USER_ID') != null) 
        {
            $menu_id = 323;
           $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
            if ($is_akses == 0){
                 return redirect()->route('404Notfound');
            }
            $menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
            $isakses_kec = Shr::get_give_kec();
            $isakses_kel = Shr::get_give_kel();
            $data = array(
                "stitle"=>'Chat Message',
                "mtitle"=>'Chat Message',
                "my_url"=>'chat',
                "type_tgl"=>'chat_date',
                "menu"=>$menu,
                "akses_kec"=>$isakses_kec,
                "akses_kel"=>$isakses_kel,
                "user_id"=>$request->session()->get('S_USER_ID'),
                "user_nik"=>$request->session()->get('S_NIK'),
                "user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
                "user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
                "user_level"=>$request->session()->get('S_USER_LEVEL'),
                "user_no_kec"=>$request->session()->get('S_NO_KEC'),
                "user_no_kel"=>$request->session()->get('S_NO_KEL'),
            );
            return view('epunten_chat_warga/main',$data);
        }else{
            return redirect()->route('logout');
        }
    }

    public function skts_baru(Request $request)
    {
        Shr::check_login();
        if (Session::get('S_USER_ID') != null) 
        {
            $menu_id = 296;
           $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
            if ($is_akses == 0){
                 return redirect()->route('404Notfound');
            }
            $menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
            $isakses_kec = Shr::get_give_kec();
            $isakses_kel = Shr::get_give_kel();
            $data = array(
                "stitle"=>'Pengajuan SKTS Baru',
                "mtitle"=>'Pengajuan SKTS Baru',
                "my_url"=>'delete_req',
                "type_tgl"=>'Pengajuan',
                "menu"=>$menu,
                "akses_kec"=>$isakses_kec,
                "akses_kel"=>$isakses_kel,
                "user_id"=>$request->session()->get('S_USER_ID'),
                "user_nik"=>$request->session()->get('S_NIK'),
                "user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
                "user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
                "user_level"=>$request->session()->get('S_USER_LEVEL'),
                "user_no_kec"=>$request->session()->get('S_NO_KEC'),
                "user_no_kel"=>$request->session()->get('S_NO_KEL'),
            );
            return view('epunten_skts_baru/main',$data);
        }else{
            return redirect()->route('logout');
        }
    }

    public function skts_acc_dokumen(Request $request)
    {
        Shr::check_login();
        if (Session::get('S_USER_ID') != null) 
        {
            $menu_id = 317;
           $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
            if ($is_akses == 0){
                 return redirect()->route('404Notfound');
            }
            $menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
            $isakses_kec = Shr::get_give_kec();
            $isakses_kel = Shr::get_give_kel();
            $data = array(
                "stitle"=>'Pengajuan SKTS Acc Dokumen',
                "mtitle"=>'Pengajuan SKTS Acc Dokumen',
                "my_url"=>'delete_req',
                "type_tgl"=>'Pengajuan',
                "menu"=>$menu,
                "akses_kec"=>$isakses_kec,
                "akses_kel"=>$isakses_kel,
                "user_id"=>$request->session()->get('S_USER_ID'),
                "user_nik"=>$request->session()->get('S_NIK'),
                "user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
                "user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
                "user_level"=>$request->session()->get('S_USER_LEVEL'),
                "user_no_kec"=>$request->session()->get('S_NO_KEC'),
                "user_no_kel"=>$request->session()->get('S_NO_KEL'),
            );
            return view('epunten_skts_acc_dokumen/main',$data);
        }else{
            return redirect()->route('logout');
        }
    }

    public function skts_melengkapi(Request $request)
    {
        Shr::check_login();
        if (Session::get('S_USER_ID') != null) 
        {
            $menu_id = 318;
           $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
            if ($is_akses == 0){
                 return redirect()->route('404Notfound');
            }
            $menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
            $isakses_kec = Shr::get_give_kec();
            $isakses_kel = Shr::get_give_kel();
            $data = array(
                "stitle"=>'Pengajuan SKTS Melengkapi',
                "mtitle"=>'Pengajuan SKTS Melengkapi',
                "my_url"=>'delete_req',
                "type_tgl"=>'Pengajuan',
                "menu"=>$menu,
                "akses_kec"=>$isakses_kec,
                "akses_kel"=>$isakses_kel,
                "user_id"=>$request->session()->get('S_USER_ID'),
                "user_nik"=>$request->session()->get('S_NIK'),
                "user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
                "user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
                "user_level"=>$request->session()->get('S_USER_LEVEL'),
                "user_no_kec"=>$request->session()->get('S_NO_KEC'),
                "user_no_kel"=>$request->session()->get('S_NO_KEL'),
            );
            return view('epunten_skts_melengkapi/main',$data);
        }else{
            return redirect()->route('logout');
        }
    }

    public function skts_pembaruan(Request $request)
    {
        Shr::check_login();
        if (Session::get('S_USER_ID') != null) 
        {
            $menu_id = 309;
           $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
            if ($is_akses == 0){
                 return redirect()->route('404Notfound');
            }
            $menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
            $isakses_kec = Shr::get_give_kec();
            $isakses_kel = Shr::get_give_kel();
            $data = array(
                "stitle"=>'Pengajuan SKTS Pembaruan',
                "mtitle"=>'Pengajuan SKTS Pembaruan',
                "my_url"=>'delete_req',
                "type_tgl"=>'Pengajuan',
                "menu"=>$menu,
                "akses_kec"=>$isakses_kec,
                "akses_kel"=>$isakses_kel,
                "user_id"=>$request->session()->get('S_USER_ID'),
                "user_nik"=>$request->session()->get('S_NIK'),
                "user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
                "user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
                "user_level"=>$request->session()->get('S_USER_LEVEL'),
                "user_no_kec"=>$request->session()->get('S_NO_KEC'),
                "user_no_kel"=>$request->session()->get('S_NO_KEL'),
            );
            return view('epunten_skts_pembaruan/main',$data);
        }else{
            return redirect()->route('logout');
        }
    }
    public function skts_arsip(Request $request)
    {
        Shr::check_login();
        if (Session::get('S_USER_ID') != null) 
        {
            $menu_id = 297;
           $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
            if ($is_akses == 0){
                 return redirect()->route('404Notfound');
            }
            $menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
            $isakses_kec = Shr::get_give_kec();
            $isakses_kel = Shr::get_give_kel();
            $data = array(
                "stitle"=>'Pengajuan SKTS Arsip',
                "mtitle"=>'Pengajuan SKTS Arsip',
                "my_url"=>'delete_req',
                "type_tgl"=>'Pengajuan',
                "menu"=>$menu,
                "akses_kec"=>$isakses_kec,
                "akses_kel"=>$isakses_kel,
                "user_id"=>$request->session()->get('S_USER_ID'),
                "user_nik"=>$request->session()->get('S_NIK'),
                "user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
                "user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
                "user_level"=>$request->session()->get('S_USER_LEVEL'),
                "user_no_kec"=>$request->session()->get('S_NO_KEC'),
                "user_no_kel"=>$request->session()->get('S_NO_KEL'),
            );
            return view('epunten_skts_arsip/main',$data);
        }else{
            return redirect()->route('logout');
        }
    }
    public function skts_batal(Request $request)
    {
        Shr::check_login();
        if (Session::get('S_USER_ID') != null) 
        {
            $menu_id = 298;
           $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
            if ($is_akses == 0){
                 return redirect()->route('404Notfound');
            }
            $menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
            $isakses_kec = Shr::get_give_kec();
            $isakses_kel = Shr::get_give_kel();
            $data = array(
                "stitle"=>'Pengajuan SKTS Batal',
                "mtitle"=>'Pengajuan SKTS Batal',
                "my_url"=>'delete_req',
                "type_tgl"=>'Pengajuan',
                "menu"=>$menu,
                "akses_kec"=>$isakses_kec,
                "akses_kel"=>$isakses_kel,
                "user_id"=>$request->session()->get('S_USER_ID'),
                "user_nik"=>$request->session()->get('S_NIK'),
                "user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
                "user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
                "user_level"=>$request->session()->get('S_USER_LEVEL'),
                "user_no_kec"=>$request->session()->get('S_NO_KEC'),
                "user_no_kel"=>$request->session()->get('S_NO_KEL'),
            );
            return view('epunten_skts_batal/main',$data);
        }else{
            return redirect()->route('logout');
        }
    }

    public function datang_baru(Request $request)
    {
        Shr::check_login();
        if (Session::get('S_USER_ID') != null) 
        {
            $menu_id = 299;
           $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
            if ($is_akses == 0){
                 return redirect()->route('404Notfound');
            }
            $menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
            $isakses_kec = Shr::get_give_kec();
            $isakses_kel = Shr::get_give_kel();
            $data = array(
                "stitle"=>'Pengajuan Kedatangan Baru',
                "mtitle"=>'Pengajuan Kedatangan Baru',
                "my_url"=>'delete_req',
                "type_tgl"=>'Pengajuan',
                "menu"=>$menu,
                "akses_kec"=>$isakses_kec,
                "akses_kel"=>$isakses_kel,
                "user_id"=>$request->session()->get('S_USER_ID'),
                "user_nik"=>$request->session()->get('S_NIK'),
                "user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
                "user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
                "user_level"=>$request->session()->get('S_USER_LEVEL'),
                "user_no_kec"=>$request->session()->get('S_NO_KEC'),
                "user_no_kel"=>$request->session()->get('S_NO_KEL'),
            );
            return view('epunten_datang_baru/main',$data);
        }else{
            return redirect()->route('logout');
        }
    }

    public function datang_acc_dokumen(Request $request)
    {
        Shr::check_login();
        if (Session::get('S_USER_ID') != null) 
        {
            $menu_id = 319;
           $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
            if ($is_akses == 0){
                 return redirect()->route('404Notfound');
            }
            $menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
            $isakses_kec = Shr::get_give_kec();
            $isakses_kel = Shr::get_give_kel();
            $data = array(
                "stitle"=>'Pengajuan Datang Acc Dokumen',
                "mtitle"=>'Pengajuan Datang Acc Dokumen',
                "my_url"=>'delete_req',
                "type_tgl"=>'Pengajuan',
                "menu"=>$menu,
                "akses_kec"=>$isakses_kec,
                "akses_kel"=>$isakses_kel,
                "user_id"=>$request->session()->get('S_USER_ID'),
                "user_nik"=>$request->session()->get('S_NIK'),
                "user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
                "user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
                "user_level"=>$request->session()->get('S_USER_LEVEL'),
                "user_no_kec"=>$request->session()->get('S_NO_KEC'),
                "user_no_kel"=>$request->session()->get('S_NO_KEL'),
            );
            return view('epunten_datang_acc_dokumen/main',$data);
        }else{
            return redirect()->route('logout');
        }
    }

    public function datang_melengkapi(Request $request)
    {
        Shr::check_login();
        if (Session::get('S_USER_ID') != null) 
        {
            $menu_id = 320;
           $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
            if ($is_akses == 0){
                 return redirect()->route('404Notfound');
            }
            $menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
            $isakses_kec = Shr::get_give_kec();
            $isakses_kel = Shr::get_give_kel();
            $data = array(
                "stitle"=>'Pengajuan Datang Melengkapi',
                "mtitle"=>'Pengajuan Datang Melengkapi',
                "my_url"=>'delete_req',
                "type_tgl"=>'Pengajuan',
                "menu"=>$menu,
                "akses_kec"=>$isakses_kec,
                "akses_kel"=>$isakses_kel,
                "user_id"=>$request->session()->get('S_USER_ID'),
                "user_nik"=>$request->session()->get('S_NIK'),
                "user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
                "user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
                "user_level"=>$request->session()->get('S_USER_LEVEL'),
                "user_no_kec"=>$request->session()->get('S_NO_KEC'),
                "user_no_kel"=>$request->session()->get('S_NO_KEL'),
            );
            return view('epunten_datang_melengkapi/main',$data);
        }else{
            return redirect()->route('logout');
        }
    }
    public function datang_arsip(Request $request)
    {
        Shr::check_login();
        if (Session::get('S_USER_ID') != null) 
        {
            $menu_id = 300;
           $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
            if ($is_akses == 0){
                 return redirect()->route('404Notfound');
            }
            $menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
            $isakses_kec = Shr::get_give_kec();
            $isakses_kel = Shr::get_give_kel();
            $data = array(
                "stitle"=>'Pengajuan Datang Arsip',
                "mtitle"=>'Pengajuan Datang Arsip',
                "my_url"=>'delete_req',
                "type_tgl"=>'Pengajuan',
                "menu"=>$menu,
                "akses_kec"=>$isakses_kec,
                "akses_kel"=>$isakses_kel,
                "user_id"=>$request->session()->get('S_USER_ID'),
                "user_nik"=>$request->session()->get('S_NIK'),
                "user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
                "user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
                "user_level"=>$request->session()->get('S_USER_LEVEL'),
                "user_no_kec"=>$request->session()->get('S_NO_KEC'),
                "user_no_kel"=>$request->session()->get('S_NO_KEL'),
            );
            return view('epunten_datang_arsip/main',$data);
        }else{
            return redirect()->route('logout');
        }
    }
    public function datang_batal(Request $request)
    {
        Shr::check_login();
        if (Session::get('S_USER_ID') != null) 
        {
            $menu_id = 301;
           $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
            if ($is_akses == 0){
                 return redirect()->route('404Notfound');
            }
            $menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
            $isakses_kec = Shr::get_give_kec();
            $isakses_kel = Shr::get_give_kel();
            $data = array(
                "stitle"=>'Pengajuan Datang Batal',
                "mtitle"=>'Pengajuan Datang Batal',
                "my_url"=>'delete_req',
                "type_tgl"=>'Pengajuan',
                "menu"=>$menu,
                "akses_kec"=>$isakses_kec,
                "akses_kel"=>$isakses_kel,
                "user_id"=>$request->session()->get('S_USER_ID'),
                "user_nik"=>$request->session()->get('S_NIK'),
                "user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
                "user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
                "user_level"=>$request->session()->get('S_USER_LEVEL'),
                "user_no_kec"=>$request->session()->get('S_NO_KEC'),
                "user_no_kel"=>$request->session()->get('S_NO_KEL'),
            );
            return view('epunten_datang_batal/main',$data);
        }else{
            return redirect()->route('logout');
        }
    }

    public function wna_baru(Request $request)
    {
        Shr::check_login();
        if (Session::get('S_USER_ID') != null) 
        {
            $menu_id = 302;
           $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
            if ($is_akses == 0){
                 return redirect()->route('404Notfound');
            }
            $menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
            $isakses_kec = Shr::get_give_kec();
            $isakses_kel = Shr::get_give_kel();
            $data = array(
                "stitle"=>'Pengajuan Wna Baru',
                "mtitle"=>'Pengajuan Wna Baru',
                "my_url"=>'delete_req',
                "type_tgl"=>'Pengajuan',
                "menu"=>$menu,
                "akses_kec"=>$isakses_kec,
                "akses_kel"=>$isakses_kel,
                "user_id"=>$request->session()->get('S_USER_ID'),
                "user_nik"=>$request->session()->get('S_NIK'),
                "user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
                "user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
                "user_level"=>$request->session()->get('S_USER_LEVEL'),
                "user_no_kec"=>$request->session()->get('S_NO_KEC'),
                "user_no_kel"=>$request->session()->get('S_NO_KEL'),
            );
            return view('epunten_wna_baru/main',$data);
        }else{
            return redirect()->route('logout');
        }
    }
    public function wna_acc_dokumen(Request $request)
    {
        Shr::check_login();
        if (Session::get('S_USER_ID') != null) 
        {
            $menu_id = 321;
           $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
            if ($is_akses == 0){
                 return redirect()->route('404Notfound');
            }
            $menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
            $isakses_kec = Shr::get_give_kec();
            $isakses_kel = Shr::get_give_kel();
            $data = array(
                "stitle"=>'Pengajuan Wna Acc Dokumen',
                "mtitle"=>'Pengajuan Wna Acc Dokumen',
                "my_url"=>'delete_req',
                "type_tgl"=>'Pengajuan',
                "menu"=>$menu,
                "akses_kec"=>$isakses_kec,
                "akses_kel"=>$isakses_kel,
                "user_id"=>$request->session()->get('S_USER_ID'),
                "user_nik"=>$request->session()->get('S_NIK'),
                "user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
                "user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
                "user_level"=>$request->session()->get('S_USER_LEVEL'),
                "user_no_kec"=>$request->session()->get('S_NO_KEC'),
                "user_no_kel"=>$request->session()->get('S_NO_KEL'),
            );
            return view('epunten_wna_acc_dokumen/main',$data);
        }else{
            return redirect()->route('logout');
        }
    }

    public function wna_melengkapi(Request $request)
    {
        Shr::check_login();
        if (Session::get('S_USER_ID') != null) 
        {
            $menu_id = 322;
           $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
            if ($is_akses == 0){
                 return redirect()->route('404Notfound');
            }
            $menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
            $isakses_kec = Shr::get_give_kec();
            $isakses_kel = Shr::get_give_kel();
            $data = array(
                "stitle"=>'Pengajuan Wna Melengkapi',
                "mtitle"=>'Pengajuan Wna Melengkapi',
                "my_url"=>'delete_req',
                "type_tgl"=>'Pengajuan',
                "menu"=>$menu,
                "akses_kec"=>$isakses_kec,
                "akses_kel"=>$isakses_kel,
                "user_id"=>$request->session()->get('S_USER_ID'),
                "user_nik"=>$request->session()->get('S_NIK'),
                "user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
                "user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
                "user_level"=>$request->session()->get('S_USER_LEVEL'),
                "user_no_kec"=>$request->session()->get('S_NO_KEC'),
                "user_no_kel"=>$request->session()->get('S_NO_KEL'),
            );
            return view('epunten_wna_melengkapi/main',$data);
        }else{
            return redirect()->route('logout');
        }
    }
    public function wna_arsip(Request $request)
    {
        Shr::check_login();
        if (Session::get('S_USER_ID') != null) 
        {
            $menu_id = 303;
           $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
            if ($is_akses == 0){
                 return redirect()->route('404Notfound');
            }
            $menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
            $isakses_kec = Shr::get_give_kec();
            $isakses_kel = Shr::get_give_kel();
            $data = array(
                "stitle"=>'Pengajuan Wna Arsip',
                "mtitle"=>'Pengajuan Wna Arsip',
                "my_url"=>'delete_req',
                "type_tgl"=>'Pengajuan',
                "menu"=>$menu,
                "akses_kec"=>$isakses_kec,
                "akses_kel"=>$isakses_kel,
                "user_id"=>$request->session()->get('S_USER_ID'),
                "user_nik"=>$request->session()->get('S_NIK'),
                "user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
                "user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
                "user_level"=>$request->session()->get('S_USER_LEVEL'),
                "user_no_kec"=>$request->session()->get('S_NO_KEC'),
                "user_no_kel"=>$request->session()->get('S_NO_KEL'),
            );
            return view('epunten_wna_arsip/main',$data);
        }else{
            return redirect()->route('logout');
        }
    }
    public function wna_batal(Request $request)
    {
        Shr::check_login();
        if (Session::get('S_USER_ID') != null) 
        {
            $menu_id = 304;
           $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
            if ($is_akses == 0){
                 return redirect()->route('404Notfound');
            }
            $menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
            $isakses_kec = Shr::get_give_kec();
            $isakses_kel = Shr::get_give_kel();
            $data = array(
                "stitle"=>'Pengajuan Wna Batal',
                "mtitle"=>'Pengajuan Wna Batal',
                "my_url"=>'delete_req',
                "type_tgl"=>'Pengajuan',
                "menu"=>$menu,
                "akses_kec"=>$isakses_kec,
                "akses_kel"=>$isakses_kel,
                "user_id"=>$request->session()->get('S_USER_ID'),
                "user_nik"=>$request->session()->get('S_NIK'),
                "user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
                "user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
                "user_level"=>$request->session()->get('S_USER_LEVEL'),
                "user_no_kec"=>$request->session()->get('S_NO_KEC'),
                "user_no_kel"=>$request->session()->get('S_NO_KEL'),
            );
            return view('epunten_wna_batal/main',$data);
        }else{
            return redirect()->route('logout');
        }
    }

    public function epunten_user(Request $request)
    {
        Shr::check_login();
        if (Session::get('S_USER_ID') != null) 
        {
            $menu_id = 313;
           $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
            if ($is_akses == 0){
                 return redirect()->route('404Notfound');
            }
            $menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
            $isakses_kec = Shr::get_give_kec();
            $isakses_kel = Shr::get_give_kel();
            $data = array(
                "stitle"=>'Pengaturan Pengguna',
                "mtitle"=>'Pengaturan Pengguna',
                "my_url"=>'user',
                "type_tgl"=>'Pengajuan',
                "menu"=>$menu,
                "akses_kec"=>$isakses_kec,
                "akses_kel"=>$isakses_kel,
                "user_id"=>$request->session()->get('S_USER_ID'),
                "user_nik"=>$request->session()->get('S_NIK'),
                "user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
                "user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
                "user_level"=>$request->session()->get('S_USER_LEVEL'),
                "user_no_kec"=>$request->session()->get('S_NO_KEC'),
                "user_no_kel"=>$request->session()->get('S_NO_KEL'),
            );
            return view('epunten_user/main',$data);
        }else{
            return redirect()->route('logout');
        }
    }

    

    public function get_skts_baru(Request $request)
    {

        $draw = intval($request->draw);
        $start = intval($request->start);
        $length = intval($request->length);
        $rekap_pengajuan = $this->ept_get_skts_baru();
        $data = array();
        $i = 0;
        foreach ($rekap_pengajuan as $r)
        {
             $img = '';
            if (!empty($r->img_15)){
                if (strpos($r->img_15, 'pdf') !== false) {
                    $img .= '
                    <div class="row">
                        <div class="col-md-12">
                            <div class="white-box">
                            <embed src="'.url('/').'/'.$r->img_15.'" type="application/pdf"   height="500px" width="100%">
                        </div>
                        </div>
                    </div>';
                }
            }  
            $img .= '<div class="row">
                    <div class="col-md-12">
                        <div class="white-box">
                            <h3 class="box-title">Berkas Pengajuan</h3>
                            <div id="gallery">
                                <div id="gallery-content">
                                    <div id="gallery-content-center">';
            if (!empty($r->img_1)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 1" href="'.url('/').'/'.$r->img_1.'"><img src="'.url('/').'/'.$r->img_thumb_1.'" alt=""></a>';
            }
            if (!empty($r->img_2)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 2" href="'.url('/').'/'.$r->img_2.'"><img src="'.url('/').'/'.$r->img_thumb_2.'" alt=""></a>';
            }
            if (!empty($r->img_3)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 3" href="'.url('/').'/'.$r->img_3.'"><img src="'.url('/').'/'.$r->img_thumb_3.'" alt=""></a>';
            }
            if (!empty($r->img_4)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 4" href="'.url('/').'/'.$r->img_4.'"><img src="'.url('/').'/'.$r->img_thumb_4.'" alt=""></a>';
            }
            if (!empty($r->img_5)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 5" href="'.url('/').'/'.$r->img_5.'"><img src="'.url('/').'/'.$r->img_thumb_5.'" alt=""></a>';
            }
            if (!empty($r->img_6)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 6" href="'.url('/').'/'.$r->img_6.'"><img src="'.url('/').'/'.$r->img_thumb_6.'" alt=""></a>';
            }
            if (!empty($r->img_7)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 7" href="'.url('/').'/'.$r->img_7.'"><img src="'.url('/').'/'.$r->img_thumb_7.'" alt=""></a>';
            }
            if (!empty($r->img_8)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 8" href="'.url('/').'/'.$r->img_8.'"><img src="'.url('/').'/'.$r->img_thumb_8.'" alt=""></a>';
            }
            if (!empty($r->img_9)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 9" href="'.url('/').'/'.$r->img_9.'"><img src="'.url('/').'/'.$r->img_thumb_9.'" alt=""></a>';
            }
            if (!empty($r->img_10)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 10" href="'.url('/').'/'.$r->img_10.'"><img src="'.url('/').'/'.$r->img_thumb_10.'" alt=""></a>';
            }
            if (!empty($r->img_11)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 11" href="'.url('/').'/'.$r->img_11.'"><img src="'.url('/').'/'.$r->img_thumb_11.'" alt=""></a>';
            }
            if (!empty($r->img_12)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 12" href="'.url('/').'/'.$r->img_12.'"><img src="'.url('/').'/'.$r->img_thumb_12.'" alt=""></a>';
            }
            if (!empty($r->img_13)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 13" href="'.url('/').'/'.$r->img_13.'"><img src="'.url('/').'/'.$r->img_thumb_13.'" alt=""></a>';
            }
            if (!empty($r->img_14)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 14" href="'.url('/').'/'.$r->img_14.'"><img src="'.url('/').'/'.$r->img_thumb_14.'" alt=""></a>';
            }
            if (!empty($r->img_15)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 15" href="'.url('/').'/'.$r->img_15.'"><img src="'.url('/').'/'.$r->img_thumb_15.'" alt=""></a>';
            }

            if ($r->jum_anggota > 1){
               $img .='</div>
                        </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                         
                    <h3 class="box-title">Anggota Keluarga</h3>';
                     $rekap_pengajuan_anggota = $this->ept_get_skts_anggota($r->no_kk);
                     foreach ($rekap_pengajuan_anggota as $ra)
                        {
                        $img .='<div class="col-md-6">';
                         if (!empty($ra->img_1)){
                           
                                $img .='<div id="gallery">
                                    <div id="gallery-content">
                                        <div id="gallery-content-center">
                                            <a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$ra->nama_lgkp.' 1" href="'.url('/').'/'.$ra->img_1.'"><img src="'.url('/').'/'.$ra->img_thumb_1.'" alt=""></a>
                                        </div>
                                        </div>
                                        </div>
                                        ';
                            }else{
                                $img .='<div id="gallery">
                                    <div id="gallery-content">
                                        <div id="gallery-content-center">
                                            <a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan kosong 1" href="'.url('/').'/assets/plugins/images/calming-cat.gif"><img src="'.url('/').'/assets/plugins/images/calming-cat.gif" alt="" width="300" height="300"></a>
                                        </div>
                                        </div>
                                        </div>
                                        ';
                            }

                            $img .='<div class="clearfix"></div>
                            <h4 >NIK : '.$ra->nik.'</h4>
                                         <h4 >NAMA LENGKAP : '.$ra->nama_lgkp.'</h4>
                                         <h4 >STATUS HUBUNGAN KELUARGA : '.$ra->stat_hbkel_desc.'</h4>
                                         </div>
                            ';
                        }

                    $img .='</div>
                </div>';

                  
             }else{
               $img .='</div>
                        </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    </div>
                </div>';
            }
            

            
            $i++;
            if ($r->jum_anggota > 1){
                $detail_style = '';
             }else{
                $detail_style = 'style="display:none !important"';
             }
            $data[] = array(
                "no" => $i,
                "nik" => $r->nik,
                "nama_lgkp" => $r->nama_lgkp,
                "telepon" => $r->telepon,
                "email" => $r->email,
                "created_at" => $r->created_at,
                "img" => $img,
                "aksi" => '
                    <button type="button" class="btn btn-info btn-circle"  id="btn-edit" onclick="edit('.$r->id.');" ><i class="fa fa-pencil"></i></button> 
                    &nbsp <button type="button" class="btn btn-danger btn-circle" id="btn-hapus" onclick="hapus('.$r->id.',\''.str_replace("'", "", $r->nama_lgkp).'\','.$r->nik.');" ><i class="fa fa-trash-o"></i></button>
                    &nbsp <button type="button" class="btn btn-info btn-circle" id="btn-print" onclick="print_data('.$r->id.');" ><i class="fa  fa-print"></i></button>
                    &nbsp <button type="button" class="btn btn-warning btn-circle" id="btn-pesan" onclick="pesan('.$r->id.','.$r->nik.');" ><i class="fa fa-envelope"></i></button>
                    &nbsp <button type="button" class="btn btn-success btn-circle" id="btn-proses" onclick="proses('.$r->id.',\''.str_replace("'", "", $r->nama_lgkp).'\','.$r->nik.');" ><i class="fa fa-check"></i></button>
                    &nbsp <button type="button" class="btn btn-warning btn-circle" id="btn-detail" '.$detail_style.' ><i class="fa fa-plus-square"></i></button>
                    '
                );
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => count($rekap_pengajuan) ,
            "recordsFiltered" => count($rekap_pengajuan) ,
            "data" => $data
        );
        return $output;
        exit();
    }
    public function get_skts_acc_dokumen(Request $request)
    {

        $draw = intval($request->draw);
        $start = intval($request->start);
        $length = intval($request->length);
        $rekap_pengajuan = $this->ept_get_skts_acc_dokumen();
        $data = array();
        $i = 0;
        foreach ($rekap_pengajuan as $r)
        {
             $img = '';
            if (!empty($r->img_15)){
                if (strpos($r->img_15, 'pdf') !== false) {
                    $img .= '
                    <div class="row">
                        <div class="col-md-12">
                            <div class="white-box">
                            <embed src="'.url('/').'/'.$r->img_15.'" type="application/pdf"   height="500px" width="100%">
                        </div>
                        </div>
                    </div>';
                }
            }  
            $img .= '<div class="row">
                    <div class="col-md-12">
                        <div class="white-box">
                            <h3 class="box-title">Berkas Pengajuan</h3>
                            <div id="gallery">
                                <div id="gallery-content">
                                    <div id="gallery-content-center">';
            if (!empty($r->img_1)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 1" href="'.url('/').'/'.$r->img_1.'"><img src="'.url('/').'/'.$r->img_thumb_1.'" alt=""></a>';
            }
            if (!empty($r->img_2)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 2" href="'.url('/').'/'.$r->img_2.'"><img src="'.url('/').'/'.$r->img_thumb_2.'" alt=""></a>';
            }
            if (!empty($r->img_3)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 3" href="'.url('/').'/'.$r->img_3.'"><img src="'.url('/').'/'.$r->img_thumb_3.'" alt=""></a>';
            }
            if (!empty($r->img_4)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 4" href="'.url('/').'/'.$r->img_4.'"><img src="'.url('/').'/'.$r->img_thumb_4.'" alt=""></a>';
            }
            if (!empty($r->img_5)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 5" href="'.url('/').'/'.$r->img_5.'"><img src="'.url('/').'/'.$r->img_thumb_5.'" alt=""></a>';
            }
            if (!empty($r->img_6)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 6" href="'.url('/').'/'.$r->img_6.'"><img src="'.url('/').'/'.$r->img_thumb_6.'" alt=""></a>';
            }
            if (!empty($r->img_7)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 7" href="'.url('/').'/'.$r->img_7.'"><img src="'.url('/').'/'.$r->img_thumb_7.'" alt=""></a>';
            }
            if (!empty($r->img_8)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 8" href="'.url('/').'/'.$r->img_8.'"><img src="'.url('/').'/'.$r->img_thumb_8.'" alt=""></a>';
            }
            if (!empty($r->img_9)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 9" href="'.url('/').'/'.$r->img_9.'"><img src="'.url('/').'/'.$r->img_thumb_9.'" alt=""></a>';
            }
            if (!empty($r->img_10)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 10" href="'.url('/').'/'.$r->img_10.'"><img src="'.url('/').'/'.$r->img_thumb_10.'" alt=""></a>';
            }
            if (!empty($r->img_11)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 11" href="'.url('/').'/'.$r->img_11.'"><img src="'.url('/').'/'.$r->img_thumb_11.'" alt=""></a>';
            }
            if (!empty($r->img_12)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 12" href="'.url('/').'/'.$r->img_12.'"><img src="'.url('/').'/'.$r->img_thumb_12.'" alt=""></a>';
            }
            if (!empty($r->img_13)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 13" href="'.url('/').'/'.$r->img_13.'"><img src="'.url('/').'/'.$r->img_thumb_13.'" alt=""></a>';
            }
            if (!empty($r->img_14)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 14" href="'.url('/').'/'.$r->img_14.'"><img src="'.url('/').'/'.$r->img_thumb_14.'" alt=""></a>';
            }
            if (!empty($r->img_15)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 15" href="'.url('/').'/'.$r->img_15.'"><img src="'.url('/').'/'.$r->img_thumb_15.'" alt=""></a>';
            }
             if ($r->jum_anggota > 1){
               $img .='</div>
                        </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                         
                    <h3 class="box-title">Anggota Keluarga</h3>';
                     $rekap_pengajuan_anggota = $this->ept_get_skts_anggota($r->no_kk);
                     foreach ($rekap_pengajuan_anggota as $ra)
                        {
                        $img .='<div class="col-md-6">';
                         if (!empty($ra->img_1)){
                           
                                $img .='<div id="gallery">
                                    <div id="gallery-content">
                                        <div id="gallery-content-center">
                                            <a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$ra->nama_lgkp.' 1" href="'.url('/').'/'.$ra->img_1.'"><img src="'.url('/').'/'.$ra->img_thumb_1.'" alt=""></a>
                                        </div>
                                        </div>
                                        </div>
                                        ';
                            }else{
                                $img .='<div id="gallery">
                                    <div id="gallery-content">
                                        <div id="gallery-content-center">
                                            <a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan kosong 1" href="'.url('/').'/assets/plugins/images/calming-cat.gif"><img src="'.url('/').'/assets/plugins/images/calming-cat.gif" alt="" width="300" height="300"></a>
                                        </div>
                                        </div>
                                        </div>
                                        ';
                            }

                            $img .='<div class="clearfix"></div>
                            <h4 >NIK : '.$ra->nik.'</h4>
                                         <h4 >NAMA LENGKAP : '.$ra->nama_lgkp.'</h4>
                                         <h4 >STATUS HUBUNGAN KELUARGA : '.$ra->stat_hbkel_desc.'</h4>
                                         </div>
                            ';
                        }

                    $img .='</div>
                </div>';

                  
             }else{
               $img .='</div>
                        </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    </div>
                </div>';
            }
            

            
            $i++;
            if ($r->jum_anggota > 1){
                $detail_style = '';
             }else{
                $detail_style = 'style="display:none !important"';
             }
            $i++;
            $data[] = array(
                "no" => $i,
                "nik" => $r->nik,
                "nama_lgkp" => $r->nama_lgkp,
                "telepon" => $r->telepon,
                "email" => $r->email,
                "created_at" => $r->created_at,
                "img" => $img,
                "aksi" => '
                    <button type="button" class="btn btn-info btn-circle"  id="btn-edit" onclick="edit('.$r->id.');" ><i class="fa fa-pencil"></i></button> 
                    &nbsp <button type="button" class="btn btn-danger btn-circle" id="btn-hapus" onclick="hapus('.$r->id.',\''.str_replace("'", "", $r->nama_lgkp).'\','.$r->nik.');" ><i class="fa fa-trash-o"></i></button>
                    &nbsp <button type="button" class="btn btn-info btn-circle" id="btn-print" onclick="print_data('.$r->id.');" ><i class="fa  fa-print"></i></button>
                    &nbsp <button type="button" class="btn btn-warning btn-circle" id="btn-pesan" onclick="pesan('.$r->id.','.$r->nik.');" ><i class="fa fa-envelope"></i></button>
                    &nbsp <a target="_blank" href="'.url('/').'/Epunten/cetak" class="btn btn-success btn-circle" id="btn-proses" onclick="" ><i class="fa fa-check"></i></a>
                    &nbsp <button type="button" class="btn btn-warning btn-circle" id="btn-detail" '.$detail_style.' ><i class="fa fa-plus-square"></i></button>
                    '
                );
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => count($rekap_pengajuan) ,
            "recordsFiltered" => count($rekap_pengajuan) ,
            "data" => $data
        );
        return $output;
        exit();
    }

    public function get_skts_arsip(Request $request)
    {

        $draw = intval($request->draw);
        $start = intval($request->start);
        $length = intval($request->length);
        $rekap_pengajuan = $this->ept_get_skts_arsip();
        $data = array();
        $i = 0;
        foreach ($rekap_pengajuan as $r)
        {
             $img = '';
            if (!empty($r->img_15)){
                if (strpos($r->img_15, 'pdf') !== false) {
                    $img .= '
                    <div class="row">
                        <div class="col-md-12">
                            <div class="white-box">
                            <embed src="'.url('/').'/'.$r->img_15.'" type="application/pdf"   height="500px" width="100%">
                        </div>
                        </div>
                    </div>';
                }
            }  
            $img .= '<div class="row">
                    <div class="col-md-12">
                        <div class="white-box">
                            <h3 class="box-title">Berkas Pengajuan</h3>
                            <div id="gallery">
                                <div id="gallery-content">
                                    <div id="gallery-content-center">';
            if (!empty($r->img_1)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 1" href="'.url('/').'/'.$r->img_1.'"><img src="'.url('/').'/'.$r->img_thumb_1.'" alt=""></a>';
            }
            if (!empty($r->img_2)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 2" href="'.url('/').'/'.$r->img_2.'"><img src="'.url('/').'/'.$r->img_thumb_2.'" alt=""></a>';
            }
            if (!empty($r->img_3)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 3" href="'.url('/').'/'.$r->img_3.'"><img src="'.url('/').'/'.$r->img_thumb_3.'" alt=""></a>';
            }
            if (!empty($r->img_4)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 4" href="'.url('/').'/'.$r->img_4.'"><img src="'.url('/').'/'.$r->img_thumb_4.'" alt=""></a>';
            }
            if (!empty($r->img_5)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 5" href="'.url('/').'/'.$r->img_5.'"><img src="'.url('/').'/'.$r->img_thumb_5.'" alt=""></a>';
            }
            if (!empty($r->img_6)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 6" href="'.url('/').'/'.$r->img_6.'"><img src="'.url('/').'/'.$r->img_thumb_6.'" alt=""></a>';
            }
            if (!empty($r->img_7)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 7" href="'.url('/').'/'.$r->img_7.'"><img src="'.url('/').'/'.$r->img_thumb_7.'" alt=""></a>';
            }
            if (!empty($r->img_8)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 8" href="'.url('/').'/'.$r->img_8.'"><img src="'.url('/').'/'.$r->img_thumb_8.'" alt=""></a>';
            }
            if (!empty($r->img_9)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 9" href="'.url('/').'/'.$r->img_9.'"><img src="'.url('/').'/'.$r->img_thumb_9.'" alt=""></a>';
            }
            if (!empty($r->img_10)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 10" href="'.url('/').'/'.$r->img_10.'"><img src="'.url('/').'/'.$r->img_thumb_10.'" alt=""></a>';
            }
            if (!empty($r->img_11)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 11" href="'.url('/').'/'.$r->img_11.'"><img src="'.url('/').'/'.$r->img_thumb_11.'" alt=""></a>';
            }
            if (!empty($r->img_12)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 12" href="'.url('/').'/'.$r->img_12.'"><img src="'.url('/').'/'.$r->img_thumb_12.'" alt=""></a>';
            }
            if (!empty($r->img_13)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 13" href="'.url('/').'/'.$r->img_13.'"><img src="'.url('/').'/'.$r->img_thumb_13.'" alt=""></a>';
            }
            if (!empty($r->img_14)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 14" href="'.url('/').'/'.$r->img_14.'"><img src="'.url('/').'/'.$r->img_thumb_14.'" alt=""></a>';
            }
            if (!empty($r->img_15)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 15" href="'.url('/').'/'.$r->img_15.'"><img src="'.url('/').'/'.$r->img_thumb_15.'" alt=""></a>';
            }
             if ($r->jum_anggota > 1){
               $img .='</div>
                        </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                         
                    <h3 class="box-title">Anggota Keluarga</h3>';
                     $rekap_pengajuan_anggota = $this->ept_get_skts_anggota($r->no_kk);
                     foreach ($rekap_pengajuan_anggota as $ra)
                        {
                        $img .='<div class="col-md-6">';
                         if (!empty($ra->img_1)){
                           
                                $img .='<div id="gallery">
                                    <div id="gallery-content">
                                        <div id="gallery-content-center">
                                            <a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$ra->nama_lgkp.' 1" href="'.url('/').'/'.$ra->img_1.'"><img src="'.url('/').'/'.$ra->img_thumb_1.'" alt=""></a>
                                        </div>
                                        </div>
                                        </div>
                                        ';
                            }else{
                                $img .='<div id="gallery">
                                    <div id="gallery-content">
                                        <div id="gallery-content-center">
                                            <a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan kosong 1" href="'.url('/').'/assets/plugins/images/calming-cat.gif"><img src="'.url('/').'/assets/plugins/images/calming-cat.gif" alt="" width="300" height="300"></a>
                                        </div>
                                        </div>
                                        </div>
                                        ';
                            }

                            $img .='<div class="clearfix"></div>
                            <h4 >NIK : '.$ra->nik.'</h4>
                                         <h4 >NAMA LENGKAP : '.$ra->nama_lgkp.'</h4>
                                         <h4 >STATUS HUBUNGAN KELUARGA : '.$ra->stat_hbkel_desc.'</h4>
                                         </div>
                            ';
                        }

                    $img .='</div>
                </div>';

                  
             }else{
               $img .='</div>
                        </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    </div>
                </div>';
            }
            

            
            $i++;
            if ($r->jum_anggota > 1){
                $detail_style = '';
             }else{
                $detail_style = 'style="display:none !important"';
             }
            $i++;
            $data[] = array(
                "no" => $i,
                "nik" => $r->nik,
                "nama_lgkp" => $r->nama_lgkp,
                "telepon" => $r->telepon,
                "email" => $r->email,
                "created_at" => $r->created_at,
                "img" => $img,
                "aksi" => '
                    <button type="button" class="btn btn-info btn-circle"  id="btn-edit" onclick="edit('.$r->id.');" ><i class="fa fa-pencil"></i></button> 
                    &nbsp <button type="button" class="btn btn-danger btn-circle" id="btn-hapus" onclick="hapus('.$r->id.',\''.str_replace("'", "", $r->nama_lgkp).'\','.$r->nik.');" ><i class="fa fa-trash-o"></i></button>
                    &nbsp <button type="button" class="btn btn-info btn-circle" id="btn-print" onclick="print_data('.$r->id.');" ><i class="fa  fa-print"></i></button>
                    &nbsp <button type="button" class="btn btn-warning btn-circle" id="btn-detail" '.$detail_style.' ><i class="fa fa-plus-square"></i></button>
                    '
                );
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => count($rekap_pengajuan) ,
            "recordsFiltered" => count($rekap_pengajuan) ,
            "data" => $data
        );
        return $output;
        exit();
    }
    public function get_skts_batal(Request $request)
    {

        $draw = intval($request->draw);
        $start = intval($request->start);
        $length = intval($request->length);
        $rekap_pengajuan = $this->ept_get_skts_batal();
        $data = array();
        $i = 0;
        foreach ($rekap_pengajuan as $r)
        {
            $img = '';
            if (!empty($r->img_15)){
                if (strpos($r->img_15, 'pdf') !== false) {
                    $img .= '
                    <div class="row">
                        <div class="col-md-12">
                            <div class="white-box">
                            <embed src="'.url('/').'/'.$r->img_15.'" type="application/pdf"   height="500px" width="100%">
                        </div>
                        </div>
                    </div>';
                }
            }  
            $img .= '<div class="row">
                    <div class="col-md-12">
                        <div class="white-box">
                            <h3 class="box-title">Berkas Pengajuan</h3>
                            <div id="gallery">
                                <div id="gallery-content">
                                    <div id="gallery-content-center">';
            if (!empty($r->img_1)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 1" href="'.url('/').'/'.$r->img_1.'"><img src="'.url('/').'/'.$r->img_thumb_1.'" alt=""></a>';
            }
            if (!empty($r->img_2)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 2" href="'.url('/').'/'.$r->img_2.'"><img src="'.url('/').'/'.$r->img_thumb_2.'" alt=""></a>';
            }
            if (!empty($r->img_3)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 3" href="'.url('/').'/'.$r->img_3.'"><img src="'.url('/').'/'.$r->img_thumb_3.'" alt=""></a>';
            }
            if (!empty($r->img_4)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 4" href="'.url('/').'/'.$r->img_4.'"><img src="'.url('/').'/'.$r->img_thumb_4.'" alt=""></a>';
            }
            if (!empty($r->img_5)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 5" href="'.url('/').'/'.$r->img_5.'"><img src="'.url('/').'/'.$r->img_thumb_5.'" alt=""></a>';
            }
            if (!empty($r->img_6)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 6" href="'.url('/').'/'.$r->img_6.'"><img src="'.url('/').'/'.$r->img_thumb_6.'" alt=""></a>';
            }
            if (!empty($r->img_7)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 7" href="'.url('/').'/'.$r->img_7.'"><img src="'.url('/').'/'.$r->img_thumb_7.'" alt=""></a>';
            }
            if (!empty($r->img_8)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 8" href="'.url('/').'/'.$r->img_8.'"><img src="'.url('/').'/'.$r->img_thumb_8.'" alt=""></a>';
            }
            if (!empty($r->img_9)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 9" href="'.url('/').'/'.$r->img_9.'"><img src="'.url('/').'/'.$r->img_thumb_9.'" alt=""></a>';
            }
            if (!empty($r->img_10)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 10" href="'.url('/').'/'.$r->img_10.'"><img src="'.url('/').'/'.$r->img_thumb_10.'" alt=""></a>';
            }
            if (!empty($r->img_11)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 11" href="'.url('/').'/'.$r->img_11.'"><img src="'.url('/').'/'.$r->img_thumb_11.'" alt=""></a>';
            }
            if (!empty($r->img_12)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 12" href="'.url('/').'/'.$r->img_12.'"><img src="'.url('/').'/'.$r->img_thumb_12.'" alt=""></a>';
            }
            if (!empty($r->img_13)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 13" href="'.url('/').'/'.$r->img_13.'"><img src="'.url('/').'/'.$r->img_thumb_13.'" alt=""></a>';
            }
            if (!empty($r->img_14)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 14" href="'.url('/').'/'.$r->img_14.'"><img src="'.url('/').'/'.$r->img_thumb_14.'" alt=""></a>';
            }
            if (!empty($r->img_15)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 15" href="'.url('/').'/'.$r->img_15.'"><img src="'.url('/').'/'.$r->img_thumb_15.'" alt=""></a>';
            }
             if ($r->jum_anggota > 1){
               $img .='</div>
                        </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                         
                    <h3 class="box-title">Anggota Keluarga</h3>';
                     $rekap_pengajuan_anggota = $this->ept_get_skts_anggota($r->no_kk);
                     foreach ($rekap_pengajuan_anggota as $ra)
                        {
                        $img .='<div class="col-md-6">';
                         if (!empty($ra->img_1)){
                           
                                $img .='<div id="gallery">
                                    <div id="gallery-content">
                                        <div id="gallery-content-center">
                                            <a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$ra->nama_lgkp.' 1" href="'.url('/').'/'.$ra->img_1.'"><img src="'.url('/').'/'.$ra->img_thumb_1.'" alt=""></a>
                                        </div>
                                        </div>
                                        </div>
                                        ';
                            }else{
                                $img .='<div id="gallery">
                                    <div id="gallery-content">
                                        <div id="gallery-content-center">
                                            <a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan kosong 1" href="'.url('/').'/assets/plugins/images/calming-cat.gif"><img src="'.url('/').'/assets/plugins/images/calming-cat.gif" alt="" width="300" height="300"></a>
                                        </div>
                                        </div>
                                        </div>
                                        ';
                            }

                            $img .='<div class="clearfix"></div>
                            <h4 >NIK : '.$ra->nik.'</h4>
                                         <h4 >NAMA LENGKAP : '.$ra->nama_lgkp.'</h4>
                                         <h4 >STATUS HUBUNGAN KELUARGA : '.$ra->stat_hbkel_desc.'</h4>
                                         </div>
                            ';
                        }

                    $img .='</div>
                </div>';

                  
             }else{
               $img .='</div>
                        </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    </div>
                </div>';
            }
            

            
            $i++;
            if ($r->jum_anggota > 1){
                $detail_style = '';
             }else{
                $detail_style = 'style="display:none !important"';
             }
            $i++;
            $data[] = array(
                "no" => $i,
                "nik" => $r->nik,
                "nama_lgkp" => $r->nama_lgkp,
                "telepon" => $r->telepon,
                "email" => $r->email,
                "created_at" => $r->created_at,
                "img" => $img,
                "aksi" => '
                    <button type="button" class="btn btn-info btn-circle"  id="btn-edit" onclick="edit('.$r->id.');" ><i class="fa fa-pencil"></i></button> 
                    &nbsp <button type="button" class="btn btn-danger btn-circle" id="btn-hapus" onclick="hapus('.$r->id.',\''.str_replace("'", "", $r->nama_lgkp).'\','.$r->nik.');" ><i class="fa fa-trash-o"></i></button>
                    &nbsp <button type="button" class="btn btn-info btn-circle" id="btn-print" onclick="print_data('.$r->id.');" ><i class="fa  fa-print"></i></button>
                    &nbsp <button type="button" class="btn btn-warning btn-circle" id="btn-detail" '.$detail_style.' ><i class="fa fa-plus-square"></i></button>
                    '
                );
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => count($rekap_pengajuan) ,
            "recordsFiltered" => count($rekap_pengajuan) ,
            "data" => $data
        );
        return $output;
        exit();
    }

    public function get_skts_melengkapi(Request $request)
    {

        $draw = intval($request->draw);
        $start = intval($request->start);
        $length = intval($request->length);
        $rekap_pengajuan = $this->ept_get_skts_melengkapi();
        $data = array();
        $i = 0;
        foreach ($rekap_pengajuan as $r)
        {
             $img = '';
            if (!empty($r->img_15)){
                if (strpos($r->img_15, 'pdf') !== false) {
                    $img .= '
                    <div class="row">
                        <div class="col-md-12">
                            <div class="white-box">
                            <embed src="'.url('/').'/'.$r->img_15.'" type="application/pdf"   height="500px" width="100%">
                        </div>
                        </div>
                    </div>';
                }
            }  
            $img .= '<div class="row">
                    <div class="col-md-12">
                        <div class="white-box">
                            <h3 class="box-title">Berkas Pengajuan</h3>
                            <div id="gallery">
                                <div id="gallery-content">
                                    <div id="gallery-content-center">';
            if (!empty($r->img_1)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 1" href="'.url('/').'/'.$r->img_1.'"><img src="'.url('/').'/'.$r->img_thumb_1.'" alt=""></a>';
            }
            if (!empty($r->img_2)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 2" href="'.url('/').'/'.$r->img_2.'"><img src="'.url('/').'/'.$r->img_thumb_2.'" alt=""></a>';
            }
            if (!empty($r->img_3)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 3" href="'.url('/').'/'.$r->img_3.'"><img src="'.url('/').'/'.$r->img_thumb_3.'" alt=""></a>';
            }
            if (!empty($r->img_4)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 4" href="'.url('/').'/'.$r->img_4.'"><img src="'.url('/').'/'.$r->img_thumb_4.'" alt=""></a>';
            }
            if (!empty($r->img_5)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 5" href="'.url('/').'/'.$r->img_5.'"><img src="'.url('/').'/'.$r->img_thumb_5.'" alt=""></a>';
            }
            if (!empty($r->img_6)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 6" href="'.url('/').'/'.$r->img_6.'"><img src="'.url('/').'/'.$r->img_thumb_6.'" alt=""></a>';
            }
            if (!empty($r->img_7)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 7" href="'.url('/').'/'.$r->img_7.'"><img src="'.url('/').'/'.$r->img_thumb_7.'" alt=""></a>';
            }
            if (!empty($r->img_8)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 8" href="'.url('/').'/'.$r->img_8.'"><img src="'.url('/').'/'.$r->img_thumb_8.'" alt=""></a>';
            }
            if (!empty($r->img_9)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 9" href="'.url('/').'/'.$r->img_9.'"><img src="'.url('/').'/'.$r->img_thumb_9.'" alt=""></a>';
            }
            if (!empty($r->img_10)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 10" href="'.url('/').'/'.$r->img_10.'"><img src="'.url('/').'/'.$r->img_thumb_10.'" alt=""></a>';
            }
            if (!empty($r->img_11)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 11" href="'.url('/').'/'.$r->img_11.'"><img src="'.url('/').'/'.$r->img_thumb_11.'" alt=""></a>';
            }
            if (!empty($r->img_12)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 12" href="'.url('/').'/'.$r->img_12.'"><img src="'.url('/').'/'.$r->img_thumb_12.'" alt=""></a>';
            }
            if (!empty($r->img_13)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 13" href="'.url('/').'/'.$r->img_13.'"><img src="'.url('/').'/'.$r->img_thumb_13.'" alt=""></a>';
            }
            if (!empty($r->img_14)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 14" href="'.url('/').'/'.$r->img_14.'"><img src="'.url('/').'/'.$r->img_thumb_14.'" alt=""></a>';
            }
            if (!empty($r->img_15)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 15" href="'.url('/').'/'.$r->img_15.'"><img src="'.url('/').'/'.$r->img_thumb_15.'" alt=""></a>';
            }
             if ($r->jum_anggota > 1){
               $img .='</div>
                        </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                         
                    <h3 class="box-title">Anggota Keluarga</h3>';
                     $rekap_pengajuan_anggota = $this->ept_get_skts_anggota($r->no_kk);
                     foreach ($rekap_pengajuan_anggota as $ra)
                        {
                        $img .='<div class="col-md-6">';
                         if (!empty($ra->img_1)){
                           
                                $img .='<div id="gallery">
                                    <div id="gallery-content">
                                        <div id="gallery-content-center">
                                            <a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$ra->nama_lgkp.' 1" href="'.url('/').'/'.$ra->img_1.'"><img src="'.url('/').'/'.$ra->img_thumb_1.'" alt=""></a>
                                        </div>
                                        </div>
                                        </div>
                                        ';
                            }else{
                                $img .='<div id="gallery">
                                    <div id="gallery-content">
                                        <div id="gallery-content-center">
                                            <a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan kosong 1" href="'.url('/').'/assets/plugins/images/calming-cat.gif"><img src="'.url('/').'/assets/plugins/images/calming-cat.gif" alt="" width="300" height="300"></a>
                                        </div>
                                        </div>
                                        </div>
                                        ';
                            }

                            $img .='<div class="clearfix"></div>
                            <h4 >NIK : '.$ra->nik.'</h4>
                                         <h4 >NAMA LENGKAP : '.$ra->nama_lgkp.'</h4>
                                         <h4 >STATUS HUBUNGAN KELUARGA : '.$ra->stat_hbkel_desc.'</h4>
                                         </div>
                            ';
                        }

                    $img .='</div>
                </div>';

                  
             }else{
               $img .='</div>
                        </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    </div>
                </div>';
            }
            

            
            $i++;
            if ($r->jum_anggota > 1){
                $detail_style = '';
             }else{
                $detail_style = 'style="display:none !important"';
             }
            $i++;
            $data[] = array(
                "no" => $i,
                "nik" => $r->nik,
                "nama_lgkp" => $r->nama_lgkp,
                "telepon" => $r->telepon,
                "email" => $r->email,
                "created_at" => $r->created_at,
                "img" => $img,
                "aksi" => '
                     <button type="button" class="btn btn-info btn-circle"  id="btn-edit" onclick="edit('.$r->id.');" ><i class="fa fa-pencil"></i></button> 
                    &nbsp <button type="button" class="btn btn-danger btn-circle" id="btn-hapus" onclick="hapus('.$r->id.',\''.str_replace("'", "", $r->nama_lgkp).'\','.$r->nik.');" ><i class="fa fa-trash-o"></i></button>
                    &nbsp <button type="button" class="btn btn-info btn-circle" id="btn-print" onclick="print_data('.$r->id.');" ><i class="fa  fa-print"></i></button>
                    &nbsp <button type="button" class="btn btn-warning btn-circle" id="btn-pesan" onclick="pesan('.$r->id.','.$r->nik.');" ><i class="fa fa-envelope"></i></button>
                    &nbsp <button type="button" class="btn btn-success btn-circle" id="btn-proses" onclick="proses('.$r->id.',\''.str_replace("'", "", $r->nama_lgkp).'\','.$r->nik.');" ><i class="fa fa-check"></i></button>
                    &nbsp <button type="button" class="btn btn-warning btn-circle" id="btn-detail" '.$detail_style.' ><i class="fa fa-plus-square"></i></button>
                    '
                );
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => count($rekap_pengajuan) ,
            "recordsFiltered" => count($rekap_pengajuan) ,
            "data" => $data
        );
        return $output;
        exit();
    }

    public function get_skts_pembaruan(Request $request)
    {

        $draw = intval($request->draw);
        $start = intval($request->start);
        $length = intval($request->length);
        $rekap_pengajuan = $this->ept_get_skts_pembaruan();
        $data = array();
        $i = 0;
        foreach ($rekap_pengajuan as $r)
        {
             $img = '';
            if (!empty($r->img_15)){
                if (strpos($r->img_15, 'pdf') !== false) {
                    $img .= '
                    <div class="row">
                        <div class="col-md-12">
                            <div class="white-box">
                            <embed src="'.url('/').'/'.$r->img_15.'" type="application/pdf"   height="500px" width="100%">
                        </div>
                        </div>
                    </div>';
                }
            }  
            $img .= '<div class="row">
                    <div class="col-md-12">
                        <div class="white-box">
                            <h3 class="box-title">Berkas Pengajuan</h3>
                            <div id="gallery">
                                <div id="gallery-content">
                                    <div id="gallery-content-center">';
            if (!empty($r->img_1)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 1" href="'.url('/').'/'.$r->img_1.'"><img src="'.url('/').'/'.$r->img_thumb_1.'" alt=""></a>';
            }
            if (!empty($r->img_2)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 2" href="'.url('/').'/'.$r->img_2.'"><img src="'.url('/').'/'.$r->img_thumb_2.'" alt=""></a>';
            }
            if (!empty($r->img_3)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 3" href="'.url('/').'/'.$r->img_3.'"><img src="'.url('/').'/'.$r->img_thumb_3.'" alt=""></a>';
            }
            if (!empty($r->img_4)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 4" href="'.url('/').'/'.$r->img_4.'"><img src="'.url('/').'/'.$r->img_thumb_4.'" alt=""></a>';
            }
            if (!empty($r->img_5)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 5" href="'.url('/').'/'.$r->img_5.'"><img src="'.url('/').'/'.$r->img_thumb_5.'" alt=""></a>';
            }
            if (!empty($r->img_6)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 6" href="'.url('/').'/'.$r->img_6.'"><img src="'.url('/').'/'.$r->img_thumb_6.'" alt=""></a>';
            }
            if (!empty($r->img_7)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 7" href="'.url('/').'/'.$r->img_7.'"><img src="'.url('/').'/'.$r->img_thumb_7.'" alt=""></a>';
            }
            if (!empty($r->img_8)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 8" href="'.url('/').'/'.$r->img_8.'"><img src="'.url('/').'/'.$r->img_thumb_8.'" alt=""></a>';
            }
            if (!empty($r->img_9)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 9" href="'.url('/').'/'.$r->img_9.'"><img src="'.url('/').'/'.$r->img_thumb_9.'" alt=""></a>';
            }
            if (!empty($r->img_10)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 10" href="'.url('/').'/'.$r->img_10.'"><img src="'.url('/').'/'.$r->img_thumb_10.'" alt=""></a>';
            }
            if (!empty($r->img_11)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 11" href="'.url('/').'/'.$r->img_11.'"><img src="'.url('/').'/'.$r->img_thumb_11.'" alt=""></a>';
            }
            if (!empty($r->img_12)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 12" href="'.url('/').'/'.$r->img_12.'"><img src="'.url('/').'/'.$r->img_thumb_12.'" alt=""></a>';
            }
            if (!empty($r->img_13)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 13" href="'.url('/').'/'.$r->img_13.'"><img src="'.url('/').'/'.$r->img_thumb_13.'" alt=""></a>';
            }
            if (!empty($r->img_14)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 14" href="'.url('/').'/'.$r->img_14.'"><img src="'.url('/').'/'.$r->img_thumb_14.'" alt=""></a>';
            }
            if (!empty($r->img_15)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 15" href="'.url('/').'/'.$r->img_15.'"><img src="'.url('/').'/'.$r->img_thumb_15.'" alt=""></a>';
            }
             if ($r->jum_anggota > 1){
               $img .='</div>
                        </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                         
                    <h3 class="box-title">Anggota Keluarga</h3>';
                     $rekap_pengajuan_anggota = $this->ept_get_skts_anggota($r->no_kk);
                     foreach ($rekap_pengajuan_anggota as $ra)
                        {
                        $img .='<div class="col-md-6">';
                         if (!empty($ra->img_1)){
                           
                                $img .='<div id="gallery">
                                    <div id="gallery-content">
                                        <div id="gallery-content-center">
                                            <a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$ra->nama_lgkp.' 1" href="'.url('/').'/'.$ra->img_1.'"><img src="'.url('/').'/'.$ra->img_thumb_1.'" alt=""></a>
                                        </div>
                                        </div>
                                        </div>
                                        ';
                            }else{
                                $img .='<div id="gallery">
                                    <div id="gallery-content">
                                        <div id="gallery-content-center">
                                            <a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan kosong 1" href="'.url('/').'/assets/plugins/images/calming-cat.gif"><img src="'.url('/').'/assets/plugins/images/calming-cat.gif" alt="" width="300" height="300"></a>
                                        </div>
                                        </div>
                                        </div>
                                        ';
                            }

                            $img .='<div class="clearfix"></div>
                            <h4 >NIK : '.$ra->nik.'</h4>
                                         <h4 >NAMA LENGKAP : '.$ra->nama_lgkp.'</h4>
                                         <h4 >STATUS HUBUNGAN KELUARGA : '.$ra->stat_hbkel_desc.'</h4>
                                         </div>
                            ';
                        }

                    $img .='</div>
                </div>';

                  
             }else{
               $img .='</div>
                        </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    </div>
                </div>';
            }
            

            
            $i++;
            if ($r->jum_anggota > 1){
                $detail_style = '';
             }else{
                $detail_style = 'style="display:none !important"';
             }
            $i++;
            $data[] = array(
                "no" => $i,
                "nik" => $r->nik,
                "nama_lgkp" => $r->nama_lgkp,
                "telepon" => $r->telepon,
                "email" => $r->email,
                "created_at" => $r->created_at,
                "tgl_berlaku" => $r->tgl_berlaku,
                "img" => $img,
                "aksi" => '
                     <button type="button" class="btn btn-info btn-circle"  id="btn-edit" onclick="edit('.$r->id.');" ><i class="fa fa-pencil"></i></button> 
                    &nbsp <button type="button" class="btn btn-danger btn-circle" id="btn-hapus" onclick="hapus('.$r->id.',\''.str_replace("'", "", $r->nama_lgkp).'\','.$r->nik.');" ><i class="fa fa-trash-o"></i></button>
                    &nbsp <button type="button" class="btn btn-info btn-circle" id="btn-print" onclick="print_data('.$r->id.');" ><i class="fa  fa-print"></i></button>
                    &nbsp <button type="button" class="btn btn-warning btn-circle" id="btn-pesan" onclick="pesan('.$r->id.','.$r->nik.');" ><i class="fa fa-envelope"></i></button>
                    &nbsp <button type="button" class="btn btn-warning btn-circle" id="btn-detail" '.$detail_style.' ><i class="fa fa-plus-square"></i></button>
                    '
                );
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => count($rekap_pengajuan) ,
            "recordsFiltered" => count($rekap_pengajuan) ,
            "data" => $data
        );
        return $output;
        exit();
    }

    public function get_user_edit(request $request){
        header('Content-type: application/json');
        $pengajuan = $request->id_peng;
        return $this->ept_get_user_edit($pengajuan);
    }

    public function get_skts_edit(request $request){
        header('Content-type: application/json');
        $pengajuan = $request->id_peng;
        return $this->ept_get_skts_edit($pengajuan);
    }

    
    public function get_skts_delete(Request $request){
        header('Content-type: application/json');
        if ($request->id_peng != null){
            $pengajuan = $request->id_peng;
            $nik = $request->nik;
            $this->ept_get_skts_delete($pengajuan);
            $this->ept_get_notif_delete($nik);
            $output = array(
                    "message_type"=>1,
                    "message"=>"Pengajuan Ini Telah Dihapus !"
            );
            return $output; 
        }
    }

    public function get_skts_acc(Request $request){
        header('Content-type: application/json');
        if ($request->id_peng != null){
            $pengajuan = $request->id_peng;
            $nik = $request->nik;
            $notif = 'Pengajuan anda selesai diproses, informasi jadwal pengambilan kartu SKTS akan diinformasikan setelah proses pencetakan';
            $jenis_layanan = 1;
            $ntf = 'pemberitahuan';
            $jenis = 'PENGAJUAN';
            $from_user_id = Session::get('S_NIK');
            // AndrApi::sendnotification($jenis,$pengajuan,$jenis_layanan,$nik,$notif,$from_user_id,$ntf);
            $this->ept_get_skts_acc($pengajuan);
            $output = array(
                    "message_type"=>1,
                    "message"=>"Pengajuan Ini Telah Dipublish !"
            );
            return $output; 
        }
    }
    public function get_skts_beres(Request $request){
        header('Content-type: application/json');
        if ($request->id_peng != null){
            $pengajuan = $request->id_peng;
            $nik = $request->nik;
            $notif = 'SKTS Anda dalam proses pencetakan, Silahkan menunggu jadwal pengambilan.';
            $jenis_layanan = 1;
            $ntf = 'pemberitahuan';
            $jenis = 'PENGAJUAN';
            $from_user_id = Session::get('S_NIK');
            AndrApi::sendnotification($jenis,$pengajuan,$jenis_layanan,$nik,$notif,$ntf,$from_user_id);
            $this->ept_get_skts_beres($pengajuan);
            $output = array(
                    "message_type"=>1,
                    "message"=>"Pengajuan Ini Telah Dipublish !"
            );
            return $output; 
        }
    }
    public function get_skts_tolak(Request $request){
        header('Content-type: application/json');
        if ($request->id_peng != null){
            $pengajuan = $request->id_peng;
            $notif = $request->isi_pesan;
            $jenis_layanan = $request->jenis_layanan_tolak;
            $nik = $request->nik_tolak;
            $ntf = 'pemberitahuan';
            $jenis = 'PENGAJUAN';
            $from_user_id = Session::get('S_NIK');
            $nama_lgkp = Session::get('S_NAMA_LGKP');
            AndrApi::sendnotification($jenis,$pengajuan,$jenis_layanan,$nik,$notif,$ntf,$from_user_id);
            AndrApi::chat_send_api($from_user_id,$nik,$nama_lgkp,$notif);
            $this->ept_get_skts_tolak($pengajuan);
            $output = array(
                    "message_type"=>1,
                    "message"=>"Pengajuan Ini Telah Ditolak !"
            );
            return $output; 
        }
    }

    public function get_datang_baru(Request $request)
    {

        $draw = intval($request->draw);
        $start = intval($request->start);
        $length = intval($request->length);
        $rekap_pengajuan = $this->ept_get_datang_baru($request->session()->get('S_NO_KEC'));
        $data = array();
        $i = 0;
        foreach ($rekap_pengajuan as $r)
        {
             $img = '';
            if (!empty($r->img_15)){
                if (strpos($r->img_15, 'pdf') !== false) {
                    $img .= '
                    <div class="row">
                        <div class="col-md-12">
                            <div class="white-box">
                            <embed src="'.url('/').'/'.$r->img_15.'" type="application/pdf"   height="500px" width="100%">
                        </div>
                        </div>
                    </div>';
                }
            } 
            $img .= '
                <div class="row">
                    <div class="col-md-12">
                        <div class="white-box">
                            <h3 class="box-title">Berkas Pengajuan</h3>
                            <div id="gallery">
                                <div id="gallery-content">
                                    <div id="gallery-content-center">';
            if (!empty($r->img_1)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 1" href="'.url('/').'/'.$r->img_1.'"><img src="'.url('/').'/'.$r->img_thumb_1.'" alt=""></a>';
            }
            if (!empty($r->img_2)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 2" href="'.url('/').'/'.$r->img_2.'"><img src="'.url('/').'/'.$r->img_thumb_2.'" alt=""></a>';
            }
            if (!empty($r->img_3)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 3" href="'.url('/').'/'.$r->img_3.'"><img src="'.url('/').'/'.$r->img_thumb_3.'" alt=""></a>';
            }
            if (!empty($r->img_4)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 4" href="'.url('/').'/'.$r->img_4.'"><img src="'.url('/').'/'.$r->img_thumb_4.'" alt=""></a>';
            }
            if (!empty($r->img_5)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 5" href="'.url('/').'/'.$r->img_5.'"><img src="'.url('/').'/'.$r->img_thumb_5.'" alt=""></a>';
            }
            if (!empty($r->img_6)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 6" href="'.url('/').'/'.$r->img_6.'"><img src="'.url('/').'/'.$r->img_thumb_6.'" alt=""></a>';
            }
            if (!empty($r->img_7)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 7" href="'.url('/').'/'.$r->img_7.'"><img src="'.url('/').'/'.$r->img_thumb_7.'" alt=""></a>';
            }
            
            if (!empty($r->img_9)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 9" href="'.url('/').'/'.$r->img_9.'"><img src="'.url('/').'/'.$r->img_thumb_9.'" alt=""></a>';
            }
            if (!empty($r->img_10)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 10" href="'.url('/').'/'.$r->img_10.'"><img src="'.url('/').'/'.$r->img_thumb_10.'" alt=""></a>';
            }
            if (!empty($r->img_11)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 11" href="'.url('/').'/'.$r->img_11.'"><img src="'.url('/').'/'.$r->img_thumb_11.'" alt=""></a>';
            }
            if (!empty($r->img_12)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 12" href="'.url('/').'/'.$r->img_12.'"><img src="'.url('/').'/'.$r->img_thumb_12.'" alt=""></a>';
            }
            if (!empty($r->img_13)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 13" href="'.url('/').'/'.$r->img_13.'"><img src="'.url('/').'/'.$r->img_thumb_13.'" alt=""></a>';
            }
            if (!empty($r->img_14)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 14" href="'.url('/').'/'.$r->img_14.'"><img src="'.url('/').'/'.$r->img_thumb_14.'" alt=""></a>';
            }
            if (!empty($r->img_15)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 15" href="'.url('/').'/'.$r->img_15.'"><img src="'.url('/').'/'.$r->img_thumb_15.'" alt=""></a>';
            }
            $img .='</div>
                        </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    </div>
                </div>';
            $i++;
            $data[] = array(
                "no" => $i,
                "nik" => $r->nik,
                "no_skpwni" => $r->no_skpwni,
                "nama_lgkp" => $r->nama_lgkp,
                "telepon" => $r->telepon,
                "email" => $r->email,
                "created_at" => $r->created_at,
                "img" => $img,
                "aksi" => '
                    <button type="button" class="btn btn-info btn-circle"  id="btn-edit" onclick="edit('.$r->id.');" ><i class="fa fa-search"></i></button> 
                    &nbsp <button type="button" class="btn btn-danger btn-circle" id="btn-hapus" onclick="hapus('.$r->id.',\''.str_replace("'", "", $r->nama_lgkp).'\','.$r->nik.');" ><i class="fa fa-trash-o"></i></button>
                    &nbsp <button type="button" class="btn btn-info btn-circle" id="btn-print" onclick="print_data('.$r->id.');" ><i class="fa  fa-print"></i></button>
                    &nbsp <button type="button" class="btn btn-warning btn-circle" id="btn-pesan" onclick="pesan('.$r->id.','.$r->nik.');" ><i class="fa fa-envelope"></i></button>
                    &nbsp <button type="button" class="btn btn-success btn-circle" id="btn-proses" onclick="proses('.$r->id.',\''.str_replace("'", "", $r->nama_lgkp).'\','.$r->nik.');" ><i class="fa fa-check"></i></button>
                    '
                );
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => count($rekap_pengajuan) ,
            "recordsFiltered" => count($rekap_pengajuan) ,
            "data" => $data
        );
        return $output;
        exit();
    }
    public function get_datang_acc_dokumen(Request $request)
    {

        $draw = intval($request->draw);
        $start = intval($request->start);
        $length = intval($request->length);
        $rekap_pengajuan = $this->ept_get_datang_acc_dokumen($request->session()->get('S_NO_KEC'));
        $data = array();
        $i = 0;
        foreach ($rekap_pengajuan as $r)
        {
             $img = '';
            if (!empty($r->img_15)){
                if (strpos($r->img_15, 'pdf') !== false) {
                    $img .= '
                    <div class="row">
                        <div class="col-md-12">
                            <div class="white-box">
                            <embed src="'.url('/').'/'.$r->img_15.'" type="application/pdf"   height="500px" width="100%">
                        </div>
                        </div>
                    </div>';
                }
            }  
            $img .= '<div class="row">
                    <div class="col-md-12">
                        <div class="white-box">
                            <h3 class="box-title">Berkas Pengajuan</h3>
                            <div id="gallery">
                                <div id="gallery-content">
                                    <div id="gallery-content-center">';
            if (!empty($r->img_1)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 1" href="'.url('/').'/'.$r->img_1.'"><img src="'.url('/').'/'.$r->img_thumb_1.'" alt=""></a>';
            }
            if (!empty($r->img_2)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 2" href="'.url('/').'/'.$r->img_2.'"><img src="'.url('/').'/'.$r->img_thumb_2.'" alt=""></a>';
            }
            if (!empty($r->img_3)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 3" href="'.url('/').'/'.$r->img_3.'"><img src="'.url('/').'/'.$r->img_thumb_3.'" alt=""></a>';
            }
            if (!empty($r->img_4)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 4" href="'.url('/').'/'.$r->img_4.'"><img src="'.url('/').'/'.$r->img_thumb_4.'" alt=""></a>';
            }
            if (!empty($r->img_5)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 5" href="'.url('/').'/'.$r->img_5.'"><img src="'.url('/').'/'.$r->img_thumb_5.'" alt=""></a>';
            }
            if (!empty($r->img_6)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 6" href="'.url('/').'/'.$r->img_6.'"><img src="'.url('/').'/'.$r->img_thumb_6.'" alt=""></a>';
            }
            if (!empty($r->img_7)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 7" href="'.url('/').'/'.$r->img_7.'"><img src="'.url('/').'/'.$r->img_thumb_7.'" alt=""></a>';
            }
            if (!empty($r->img_8)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 8" href="'.url('/').'/'.$r->img_8.'"><img src="'.url('/').'/'.$r->img_thumb_8.'" alt=""></a>';
            }
            if (!empty($r->img_9)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 9" href="'.url('/').'/'.$r->img_9.'"><img src="'.url('/').'/'.$r->img_thumb_9.'" alt=""></a>';
            }
            if (!empty($r->img_10)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 10" href="'.url('/').'/'.$r->img_10.'"><img src="'.url('/').'/'.$r->img_thumb_10.'" alt=""></a>';
            }
            if (!empty($r->img_11)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 11" href="'.url('/').'/'.$r->img_11.'"><img src="'.url('/').'/'.$r->img_thumb_11.'" alt=""></a>';
            }
            if (!empty($r->img_12)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 12" href="'.url('/').'/'.$r->img_12.'"><img src="'.url('/').'/'.$r->img_thumb_12.'" alt=""></a>';
            }
            if (!empty($r->img_13)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 13" href="'.url('/').'/'.$r->img_13.'"><img src="'.url('/').'/'.$r->img_thumb_13.'" alt=""></a>';
            }
            if (!empty($r->img_14)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 14" href="'.url('/').'/'.$r->img_14.'"><img src="'.url('/').'/'.$r->img_thumb_14.'" alt=""></a>';
            }
            if (!empty($r->img_15)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 15" href="'.url('/').'/'.$r->img_15.'"><img src="'.url('/').'/'.$r->img_thumb_15.'" alt=""></a>';
            }
            $img .='</div>
                        </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    </div>
                </div>';
            $i++;
            if ($r->cert_status == '0' || $r->cert_status == '1'){ 
            $cert_status = "BELUM DISERTIVIKASI";
            $cert_style = 'style="display:none !important"';
             }else if ($r->cert_status == '2'){ 
            $cert_status = "PROSES PENERBITAN";
            $cert_style = 'style="display:none !important"';
             }else if  ($r->cert_status == '3'){ 
             $cert_status = "BELUM DIPUBLISH ";
             $cert_style = 'style="display:none !important"';
             }else if  ($r->cert_status == '9'){ 
            $cert_status = "BELUM DIVERIVIKASI";
            $cert_style = 'style="display:none !important"';
             }else if  ($r->cert_status == '-'){ 
             $cert_status = "-";
             $cert_style = 'style="display:none !important"';
             }else{ 
              $cert_status = "SUDAH DIPUBLISH";
              $cert_style = '';
             }
             if ($r->no_kk == '-'){
                $bsre_style = 'style="display:none !important"';
             }else{
                $bsre_style = '';
             }
            $data[] = array(
                "no" => $i,
                "nik" => $r->nik,
                "no_kk" => $r->no_kk,
                "cert_status" => $cert_status,
                "no_skpwni" => $r->no_skpwni,
                "nama_lgkp" => $r->nama_lgkp,
                "telepon" => $r->telepon,
                "email" => $r->email,
                "created_at" => $r->created_at,
                "img" => $img,
                "aksi" => '
                    <button type="button" class="btn btn-info btn-circle"  id="btn-edit" onclick="edit('.$r->id.');" ><i class="fa fa-search"></i></button> 
                    &nbsp <button type="button" class="btn btn-danger btn-circle" id="btn-hapus" onclick="hapus('.$r->id.',\''.str_replace("'", "", $r->nama_lgkp).'\','.$r->nik.');" ><i class="fa fa-trash-o"></i></button>
                    &nbsp <button type="button" class="btn btn-info btn-circle" id="btn-print" onclick="print_data('.$r->id.');" ><i class="fa  fa-print"></i></button>
                    &nbsp <button type="button" class="btn btn-warning btn-circle" id="btn-pesan" onclick="pesan('.$r->id.','.$r->nik.');" ><i class="fa fa-envelope"></i></button>
                    &nbsp <button type="button" class="btn btn-info btn-circle" id="btn-ajukan" '.$bsre_style.' onclick="ajukan('.$r->id.','.$r->no_kk.','.$r->nik.',\''.$r->email.'\',\''.$r->telepon.'\');" ><i class="fa fa-rocket"></i></button>
                    &nbsp <button type="button" class="btn btn-success btn-circle" id="btn-proses" '.$cert_style.' onclick="beres('.$r->id.',\''.$r->nama_lgkp.'\','.$r->nik.');" ><i class="fa fa-check"></i></button>' 
                );
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => count($rekap_pengajuan) ,
            "recordsFiltered" => count($rekap_pengajuan) ,
            "data" => $data
        );
        return $output;
        exit();
    }

    public function get_datang_arsip(Request $request)
    {

        $draw = intval($request->draw);
        $start = intval($request->start);
        $length = intval($request->length);
        $rekap_pengajuan = $this->ept_get_datang_arsip($request->session()->get('S_NO_KEC'));
        $data = array();
        $i = 0;
        foreach ($rekap_pengajuan as $r)
        {
             $img = '';
            if (!empty($r->img_15)){
                if (strpos($r->img_15, 'pdf') !== false) {
                    $img .= '
                    <div class="row">
                        <div class="col-md-12">
                            <div class="white-box">
                            <embed src="'.url('/').'/'.$r->img_15.'" type="application/pdf"   height="500px" width="100%">
                        </div>
                        </div>
                    </div>';
                }
            }  
            $img .= '<div class="row">
                    <div class="col-md-12">
                        <div class="white-box">
                            <h3 class="box-title">Berkas Pengajuan</h3>
                            <div id="gallery">
                                <div id="gallery-content">
                                    <div id="gallery-content-center">';
            if (!empty($r->img_1)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 1" href="'.url('/').'/'.$r->img_1.'"><img src="'.url('/').'/'.$r->img_thumb_1.'" alt=""></a>';
            }
            if (!empty($r->img_2)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 2" href="'.url('/').'/'.$r->img_2.'"><img src="'.url('/').'/'.$r->img_thumb_2.'" alt=""></a>';
            }
            if (!empty($r->img_3)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 3" href="'.url('/').'/'.$r->img_3.'"><img src="'.url('/').'/'.$r->img_thumb_3.'" alt=""></a>';
            }
            if (!empty($r->img_4)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 4" href="'.url('/').'/'.$r->img_4.'"><img src="'.url('/').'/'.$r->img_thumb_4.'" alt=""></a>';
            }
            if (!empty($r->img_5)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 5" href="'.url('/').'/'.$r->img_5.'"><img src="'.url('/').'/'.$r->img_thumb_5.'" alt=""></a>';
            }
            if (!empty($r->img_6)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 6" href="'.url('/').'/'.$r->img_6.'"><img src="'.url('/').'/'.$r->img_thumb_6.'" alt=""></a>';
            }
            if (!empty($r->img_7)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 7" href="'.url('/').'/'.$r->img_7.'"><img src="'.url('/').'/'.$r->img_thumb_7.'" alt=""></a>';
            }
            if (!empty($r->img_8)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 8" href="'.url('/').'/'.$r->img_8.'"><img src="'.url('/').'/'.$r->img_thumb_8.'" alt=""></a>';
            }
            if (!empty($r->img_9)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 9" href="'.url('/').'/'.$r->img_9.'"><img src="'.url('/').'/'.$r->img_thumb_9.'" alt=""></a>';
            }
            if (!empty($r->img_10)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 10" href="'.url('/').'/'.$r->img_10.'"><img src="'.url('/').'/'.$r->img_thumb_10.'" alt=""></a>';
            }
            if (!empty($r->img_11)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 11" href="'.url('/').'/'.$r->img_11.'"><img src="'.url('/').'/'.$r->img_thumb_11.'" alt=""></a>';
            }
            if (!empty($r->img_12)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 12" href="'.url('/').'/'.$r->img_12.'"><img src="'.url('/').'/'.$r->img_thumb_12.'" alt=""></a>';
            }
            if (!empty($r->img_13)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 13" href="'.url('/').'/'.$r->img_13.'"><img src="'.url('/').'/'.$r->img_thumb_13.'" alt=""></a>';
            }
            if (!empty($r->img_14)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 14" href="'.url('/').'/'.$r->img_14.'"><img src="'.url('/').'/'.$r->img_thumb_14.'" alt=""></a>';
            }
            if (!empty($r->img_15)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 15" href="'.url('/').'/'.$r->img_15.'"><img src="'.url('/').'/'.$r->img_thumb_15.'" alt=""></a>';
            }
            $img .='</div>
                        </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    </div>
                </div>';
            $i++;
            $data[] = array(
                "no" => $i,
                "nik" => $r->nik,
                "no_skpwni" => $r->no_skpwni,
                "nama_lgkp" => $r->nama_lgkp,
                "telepon" => $r->telepon,
                "email" => $r->email,
                "created_at" => $r->created_at,
                "img" => $img,
                "aksi" => '
                    <button type="button" class="btn btn-info btn-circle"  id="btn-edit" onclick="edit('.$r->id.');" ><i class="fa fa-search"></i></button> 
                    &nbsp <button type="button" class="btn btn-danger btn-circle" id="btn-hapus" onclick="hapus('.$r->id.',\''.str_replace("'", "", $r->nama_lgkp).'\','.$r->nik.');" ><i class="fa fa-trash-o"></i></button>
                    &nbsp <button type="button" class="btn btn-info btn-circle" id="btn-print" onclick="print_data('.$r->id.');" ><i class="fa  fa-print"></i></button>
                    '
                );
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => count($rekap_pengajuan) ,
            "recordsFiltered" => count($rekap_pengajuan) ,
            "data" => $data
        );
        return $output;
        exit();
    }
    public function get_datang_batal(Request $request)
    {

        $draw = intval($request->draw);
        $start = intval($request->start);
        $length = intval($request->length);
        $rekap_pengajuan = $this->ept_get_datang_batal($request->session()->get('S_NO_KEC'));
        $data = array();
        $i = 0;
        foreach ($rekap_pengajuan as $r)
        {
             $img = '';
            if (!empty($r->img_15)){
                if (strpos($r->img_15, 'pdf') !== false) {
                    $img .= '
                    <div class="row">
                        <div class="col-md-12">
                            <div class="white-box">
                            <embed src="'.url('/').'/'.$r->img_15.'" type="application/pdf"   height="500px" width="100%">
                        </div>
                        </div>
                    </div>';
                }
            }  
            $img .= '<div class="row">
                    <div class="col-md-12">
                        <div class="white-box">
                            <h3 class="box-title">Berkas Pengajuan</h3>
                            <div id="gallery">
                                <div id="gallery-content">
                                    <div id="gallery-content-center">';
            if (!empty($r->img_1)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 1" href="'.url('/').'/'.$r->img_1.'"><img src="'.url('/').'/'.$r->img_thumb_1.'" alt=""></a>';
            }
            if (!empty($r->img_2)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 2" href="'.url('/').'/'.$r->img_2.'"><img src="'.url('/').'/'.$r->img_thumb_2.'" alt=""></a>';
            }
            if (!empty($r->img_3)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 3" href="'.url('/').'/'.$r->img_3.'"><img src="'.url('/').'/'.$r->img_thumb_3.'" alt=""></a>';
            }
            if (!empty($r->img_4)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 4" href="'.url('/').'/'.$r->img_4.'"><img src="'.url('/').'/'.$r->img_thumb_4.'" alt=""></a>';
            }
            if (!empty($r->img_5)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 5" href="'.url('/').'/'.$r->img_5.'"><img src="'.url('/').'/'.$r->img_thumb_5.'" alt=""></a>';
            }
            if (!empty($r->img_6)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 6" href="'.url('/').'/'.$r->img_6.'"><img src="'.url('/').'/'.$r->img_thumb_6.'" alt=""></a>';
            }
            if (!empty($r->img_7)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 7" href="'.url('/').'/'.$r->img_7.'"><img src="'.url('/').'/'.$r->img_thumb_7.'" alt=""></a>';
            }
            if (!empty($r->img_8)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 8" href="'.url('/').'/'.$r->img_8.'"><img src="'.url('/').'/'.$r->img_thumb_8.'" alt=""></a>';
            }
            if (!empty($r->img_9)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 9" href="'.url('/').'/'.$r->img_9.'"><img src="'.url('/').'/'.$r->img_thumb_9.'" alt=""></a>';
            }
            if (!empty($r->img_10)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 10" href="'.url('/').'/'.$r->img_10.'"><img src="'.url('/').'/'.$r->img_thumb_10.'" alt=""></a>';
            }
            if (!empty($r->img_11)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 11" href="'.url('/').'/'.$r->img_11.'"><img src="'.url('/').'/'.$r->img_thumb_11.'" alt=""></a>';
            }
            if (!empty($r->img_12)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 12" href="'.url('/').'/'.$r->img_12.'"><img src="'.url('/').'/'.$r->img_thumb_12.'" alt=""></a>';
            }
            if (!empty($r->img_13)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 13" href="'.url('/').'/'.$r->img_13.'"><img src="'.url('/').'/'.$r->img_thumb_13.'" alt=""></a>';
            }
            if (!empty($r->img_14)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 14" href="'.url('/').'/'.$r->img_14.'"><img src="'.url('/').'/'.$r->img_thumb_14.'" alt=""></a>';
            }
            if (!empty($r->img_15)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 15" href="'.url('/').'/'.$r->img_15.'"><img src="'.url('/').'/'.$r->img_thumb_15.'" alt=""></a>';
            }
            $img .='</div>
                        </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel panel-info block4">
                            <div class="panel-heading"> <button type="button" class="btn btn-info btn-circle"><i class="fa fa-calendar"></i> </button>'.$r->tgl_notif.'
                            <br>
                            <button type="button" class="btn btn-info btn-circle"><i class="fa  fa-play"></i> </button>'.$r->message.' 
                                <div class="pull-right">  </div>
                            </div>
                        </div>
                    </div>
                </div>';
            $i++;
            $data[] = array(
                "no" => $i,
                "nik" => $r->nik,
                "no_skpwni" => $r->no_skpwni,
                "nama_lgkp" => $r->nama_lgkp,
                "telepon" => $r->telepon,
                "email" => $r->email,
                "created_at" => $r->created_at,
                "img" => $img,
                "aksi" => '
                    <button type="button" class="btn btn-info btn-circle"  id="btn-edit" onclick="edit('.$r->id.');" ><i class="fa fa-search"></i></button> 
                    &nbsp <button type="button" class="btn btn-danger btn-circle" id="btn-hapus" onclick="hapus('.$r->id.',\''.str_replace("'", "", $r->nama_lgkp).'\','.$r->nik.');" ><i class="fa fa-trash-o"></i></button>
                    &nbsp <button type="button" class="btn btn-info btn-circle" id="btn-print" onclick="print_data('.$r->id.');" ><i class="fa  fa-print"></i></button>
                    '
                );
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => count($rekap_pengajuan) ,
            "recordsFiltered" => count($rekap_pengajuan) ,
            "data" => $data
        );
        return $output;
        exit();
    }
    
    public function get_datang_melengkapi(Request $request)
    {

        $draw = intval($request->draw);
        $start = intval($request->start);
        $length = intval($request->length);
        $rekap_pengajuan = $this->ept_get_datang_melengkapi($request->session()->get('S_NO_KEC'));
        $data = array();
        $i = 0;
        foreach ($rekap_pengajuan as $r)
        {
             $img = '';
            if (!empty($r->img_15)){
                if (strpos($r->img_15, 'pdf') !== false) {
                    $img .= '
                    <div class="row">
                        <div class="col-md-12">
                            <div class="white-box">
                            <embed src="'.url('/').'/'.$r->img_15.'" type="application/pdf"   height="500px" width="100%">
                        </div>
                        </div>
                    </div>';
                }
            }  
            $img .= '<div class="row">
                    <div class="col-md-12">
                        <div class="white-box">
                            <h3 class="box-title">Berkas Pengajuan</h3>
                            <div id="gallery">
                                <div id="gallery-content">
                                    <div id="gallery-content-center">';
            if (!empty($r->img_1)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 1" href="'.url('/').'/'.$r->img_1.'"><img src="'.url('/').'/'.$r->img_thumb_1.'" alt=""></a>';
            }
            if (!empty($r->img_2)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 2" href="'.url('/').'/'.$r->img_2.'"><img src="'.url('/').'/'.$r->img_thumb_2.'" alt=""></a>';
            }
            if (!empty($r->img_3)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 3" href="'.url('/').'/'.$r->img_3.'"><img src="'.url('/').'/'.$r->img_thumb_3.'" alt=""></a>';
            }
            if (!empty($r->img_4)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 4" href="'.url('/').'/'.$r->img_4.'"><img src="'.url('/').'/'.$r->img_thumb_4.'" alt=""></a>';
            }
            if (!empty($r->img_5)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 5" href="'.url('/').'/'.$r->img_5.'"><img src="'.url('/').'/'.$r->img_thumb_5.'" alt=""></a>';
            }
            if (!empty($r->img_6)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 6" href="'.url('/').'/'.$r->img_6.'"><img src="'.url('/').'/'.$r->img_thumb_6.'" alt=""></a>';
            }
            if (!empty($r->img_7)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 7" href="'.url('/').'/'.$r->img_7.'"><img src="'.url('/').'/'.$r->img_thumb_7.'" alt=""></a>';
            }
            if (!empty($r->img_8)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 8" href="'.url('/').'/'.$r->img_8.'"><img src="'.url('/').'/'.$r->img_thumb_8.'" alt=""></a>';
            }
            if (!empty($r->img_9)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 9" href="'.url('/').'/'.$r->img_9.'"><img src="'.url('/').'/'.$r->img_thumb_9.'" alt=""></a>';
            }
            if (!empty($r->img_10)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 10" href="'.url('/').'/'.$r->img_10.'"><img src="'.url('/').'/'.$r->img_thumb_10.'" alt=""></a>';
            }
            if (!empty($r->img_11)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 11" href="'.url('/').'/'.$r->img_11.'"><img src="'.url('/').'/'.$r->img_thumb_11.'" alt=""></a>';
            }
            if (!empty($r->img_12)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 12" href="'.url('/').'/'.$r->img_12.'"><img src="'.url('/').'/'.$r->img_thumb_12.'" alt=""></a>';
            }
            if (!empty($r->img_13)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 13" href="'.url('/').'/'.$r->img_13.'"><img src="'.url('/').'/'.$r->img_thumb_13.'" alt=""></a>';
            }
            if (!empty($r->img_14)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 14" href="'.url('/').'/'.$r->img_14.'"><img src="'.url('/').'/'.$r->img_thumb_14.'" alt=""></a>';
            }
            if (!empty($r->img_15)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 15" href="'.url('/').'/'.$r->img_15.'"><img src="'.url('/').'/'.$r->img_thumb_15.'" alt=""></a>';
            }
            $img .='</div>
                        </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel panel-info block4">
                            <div class="panel-heading"> <button type="button" class="btn btn-info btn-circle"><i class="fa fa-calendar"></i> </button>'.$r->tgl_notif.'
                            <br>
                            <button type="button" class="btn btn-info btn-circle"><i class="fa  fa-play"></i> </button>'.$r->message.' 
                                <div class="pull-right">  </div>
                            </div>
                        </div>
                    </div>
                </div>';
            $i++;
            $data[] = array(
                "no" => $i,
                "nik" => $r->nik,
                "no_skpwni" => $r->no_skpwni,
                "nama_lgkp" => $r->nama_lgkp,
                "telepon" => $r->telepon,
                "email" => $r->email,
                "created_at" => $r->created_at,
                "img" => $img,
                "aksi" => '
                     <button type="button" class="btn btn-info btn-circle"  id="btn-edit" onclick="edit('.$r->id.');" ><i class="fa fa-search"></i></button> 
                    &nbsp <button type="button" class="btn btn-danger btn-circle" id="btn-hapus" onclick="hapus('.$r->id.',\''.str_replace("'", "", $r->nama_lgkp).'\','.$r->nik.');" ><i class="fa fa-trash-o"></i></button>
                    &nbsp <button type="button" class="btn btn-info btn-circle" id="btn-print" onclick="print_data('.$r->id.');" ><i class="fa  fa-print"></i></button>
                    &nbsp <button type="button" class="btn btn-warning btn-circle" id="btn-pesan" onclick="pesan('.$r->id.','.$r->nik.');" ><i class="fa fa-envelope"></i></button>
                    &nbsp <button type="button" class="btn btn-success btn-circle" id="btn-proses" onclick="proses('.$r->id.',\''.str_replace("'", "", $r->nama_lgkp).'\','.$r->nik.');" ><i class="fa fa-check"></i></button>
                    '
                );
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => count($rekap_pengajuan) ,
            "recordsFiltered" => count($rekap_pengajuan) ,
            "data" => $data
        );
        return $output;
        exit();
    }

    public function get_datang_edit(request $request){
        header('Content-type: application/json');
        $pengajuan = $request->id_peng;
        return $this->ept_get_datang_edit($pengajuan);
    }

    
    public function get_datang_delete(Request $request){
        header('Content-type: application/json');
        if ($request->id_peng != null){
            $pengajuan = $request->id_peng;
            $nik = $request->nik;
            $this->ept_get_datang_delete($pengajuan);
            $this->ept_get_notif_delete($nik);
            $output = array(
                    "message_type"=>1,
                    "message"=>"Pengajuan Ini Telah Dihapus !"
            );
            return $output; 
        }
    }

    public function get_datang_acc(Request $request){
        header('Content-type: application/json');
        if ($request->id_peng != null){
            $pengajuan = $request->id_peng;
            $nik = $request->nik;
            $notif = 'Dokumen Kedatangan Anda Telah Selesai Diverifikasi, Menunggu Proses Publish.';
            $jenis_layanan = 1;
            $ntf = 'pemberitahuan';
            $jenis = 'PENGAJUAN';
            $from_user_id = Session::get('S_NIK');
            // AndrApi::sendnotification($jenis,$pengajuan,$jenis_layanan,$nik,$notif,$ntf,$from_user_id);
            $this->ept_get_datang_acc($pengajuan);
            $output = array(
                    "message_type"=>1,
                    "message"=>"Pengajuan Ini Telah Dipublish !"
            );
            return $output; 
        }
    }
    public function get_datang_beres(Request $request){
        header('Content-type: application/json');
        if ($request->id_peng != null){
            $pengajuan = $request->id_peng;
            $nik = $request->nik;
            $notif = 'Kartu Keluarga telah selesai. Anda dapat mendownload Kartu Keluarga pada menu Dokumen di halaman utama atau Email Kepala Keluarga. Gunakan kertas HVS ukuran A4 80 Gram untuk melakukan pencetakan. Silahkan mengajukan pencetakan KTP-EL melalui <a href="https://play.google.com/store/apps/details?id=gov.disdukcapilkotabdg.pemuda">aplikasi Pemuda</a>';
            $jenis_layanan = 1;
            $ntf = 'pemberitahuan';
            $jenis = 'PENGAJUAN';
            $from_user_id = Session::get('S_NIK');
            AndrApi::sendnotification($jenis,$pengajuan,$jenis_layanan,$nik,$notif,$ntf,$from_user_id);
            $this->ept_get_datang_beres($pengajuan);
            $this->ept_push_berkas_kk($nik);
            $output = array(
                    "message_type"=>1,
                    "message"=>"Pengajuan Ini Telah Dipublish !"
            );
            return $output; 
        }
    }
    public function push_berkas_kk(Request $request){
        header('Content-type: application/json');
        if ($request->nik != null){
            $nik = $request->nik;
            $no_kk = $request->no_kk;
            $email = $request->email;
            $telepon = $request->telepon;
            $this->ept_push_berkas_kk($nik);
            $this->ept_update_kk($no_kk,$email,$telepon);
            AndrApi::insert_bsre_kk_epunten($no_kk);
            exec("redis-cli del lumen_cache:dok_bsre_".$nik);
            $output = array(
                    "message_type"=>1,
                    "message"=>"Sudah Di Push !"
            );
            return $output; 
        }
    }
    public function push_berkas_sktt(Request $request){
        header('Content-type: application/json');
        if ($request->nik != null){
            $nik = $request->nik;
            $this->ept_push_berkas_sktt($nik);
            $output = array(
                    "message_type"=>1,
                    "message"=>"Sudah Di Push !"
            );
            return $output; 
        }
    }
    public function get_datang_tolak(Request $request){
        header('Content-type: application/json');
        if ($request->id_peng != null){
            $pengajuan = $request->id_peng;
            $notif = $request->isi_pesan;
            $jenis_layanan = $request->jenis_layanan_tolak;
            $nik = $request->nik_tolak;
            $ntf = 'pemberitahuan';
            $jenis = 'PENGAJUAN';
            $from_user_id = Session::get('S_NIK');
            $nama_lgkp = Session::get('S_NAMA_LGKP');
            AndrApi::sendnotification($jenis,$pengajuan,$jenis_layanan,$nik,$notif,$ntf,$from_user_id);
            AndrApi::chat_send_api($from_user_id,$nik,$nama_lgkp,$notif);
            $this->ept_get_datang_tolak($pengajuan);
            $output = array(
                    "message_type"=>1,
                    "message"=>"Pengajuan Ini Telah Ditolak !"
            );
            return $output; 
        }
    }
    public function get_wna_baru(Request $request)
    {

        $draw = intval($request->draw);
        $start = intval($request->start);
        $length = intval($request->length);
        $rekap_pengajuan = $this->ept_get_wna_baru();
        $data = array();
        $i = 0;
        foreach ($rekap_pengajuan as $r)
        {
             $img = '';
            if (!empty($r->img_15)){
                if (strpos($r->img_15, 'pdf') !== false) {
                    $img .= '
                    <div class="row">
                        <div class="col-md-12">
                            <div class="white-box">
                            <embed src="'.url('/').'/'.$r->img_15.'" type="application/pdf"   height="500px" width="100%">
                        </div>
                        </div>
                    </div>';
                }
            }  
            $img .= '<div class="row">
                    <div class="col-md-12">
                        <div class="white-box">
                            <h3 class="box-title">Berkas Pengajuan</h3>
                            <div id="gallery">
                                <div id="gallery-content">
                                    <div id="gallery-content-center">';
            if (!empty($r->img_1)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 1" href="'.url('/').'/'.$r->img_1.'"><img src="'.url('/').'/'.$r->img_thumb_1.'" alt=""></a>';
            }
            if (!empty($r->img_2)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 2" href="'.url('/').'/'.$r->img_2.'"><img src="'.url('/').'/'.$r->img_thumb_2.'" alt=""></a>';
            }
            if (!empty($r->img_3)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 3" href="'.url('/').'/'.$r->img_3.'"><img src="'.url('/').'/'.$r->img_thumb_3.'" alt=""></a>';
            }
            if (!empty($r->img_4)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 4" href="'.url('/').'/'.$r->img_4.'"><img src="'.url('/').'/'.$r->img_thumb_4.'" alt=""></a>';
            }
            if (!empty($r->img_5)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 5" href="'.url('/').'/'.$r->img_5.'"><img src="'.url('/').'/'.$r->img_thumb_5.'" alt=""></a>';
            }
            if (!empty($r->img_6)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 6" href="'.url('/').'/'.$r->img_6.'"><img src="'.url('/').'/'.$r->img_thumb_6.'" alt=""></a>';
            }
            if (!empty($r->img_7)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 7" href="'.url('/').'/'.$r->img_7.'"><img src="'.url('/').'/'.$r->img_thumb_7.'" alt=""></a>';
            }
            if (!empty($r->img_8)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 8" href="'.url('/').'/'.$r->img_8.'"><img src="'.url('/').'/'.$r->img_thumb_8.'" alt=""></a>';
            }
            if (!empty($r->img_9)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 9" href="'.url('/').'/'.$r->img_9.'"><img src="'.url('/').'/'.$r->img_thumb_9.'" alt=""></a>';
            }
            if (!empty($r->img_10)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 10" href="'.url('/').'/'.$r->img_10.'"><img src="'.url('/').'/'.$r->img_thumb_10.'" alt=""></a>';
            }
            if (!empty($r->img_11)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 11" href="'.url('/').'/'.$r->img_11.'"><img src="'.url('/').'/'.$r->img_thumb_11.'" alt=""></a>';
            }
            if (!empty($r->img_12)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 12" href="'.url('/').'/'.$r->img_12.'"><img src="'.url('/').'/'.$r->img_thumb_12.'" alt=""></a>';
            }
            if (!empty($r->img_13)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 13" href="'.url('/').'/'.$r->img_13.'"><img src="'.url('/').'/'.$r->img_thumb_13.'" alt=""></a>';
            }
            if (!empty($r->img_14)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 14" href="'.url('/').'/'.$r->img_14.'"><img src="'.url('/').'/'.$r->img_thumb_14.'" alt=""></a>';
            }
            if (!empty($r->img_15)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 15" href="'.url('/').'/'.$r->img_15.'"><img src="'.url('/').'/'.$r->img_thumb_15.'" alt=""></a>';
            }
            $img .='</div>
                        </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    </div>
                </div>';
            $i++;
            $data[] = array(
                "no" => $i,
                "nik" => $r->nik,
                "nama_lgkp" => $r->nama_lgkp,
                "telepon" => $r->telepon,
                "email" => $r->email,
                "created_at" => $r->created_at,
                "dok_imgr_desc" => ($r->dok_imgr_desc == null) ? '-' : $r->dok_imgr_desc,
                "img" => $img,
                "aksi" => '
                    <button type="button" class="btn btn-info btn-circle"  id="btn-edit" onclick="edit('.$r->id.');" ><i class="fa fa-search"></i></button> 
                    &nbsp <button type="button" class="btn btn-danger btn-circle" id="btn-hapus" onclick="hapus('.$r->id.',\''.str_replace("'", "", $r->nama_lgkp).'\','.$r->nik.');" ><i class="fa fa-trash-o"></i></button>
                    &nbsp <button type="button" class="btn btn-info btn-circle" id="btn-print" onclick="print_data('.$r->id.');" ><i class="fa  fa-print"></i></button>
                    &nbsp <button type="button" class="btn btn-warning btn-circle" id="btn-pesan" onclick="pesan('.$r->id.','.$r->nik.');" ><i class="fa fa-envelope"></i></button>
                    &nbsp <button type="button" class="btn btn-success btn-circle" id="btn-proses" onclick="proses('.$r->id.',\''.str_replace("'", "", $r->nama_lgkp).'\','.$r->nik.');" ><i class="fa fa-check"></i></button>
                    '
                );
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => count($rekap_pengajuan) ,
            "recordsFiltered" => count($rekap_pengajuan) ,
            "data" => $data
        );
        return $output;
        exit();
    }
    public function get_wna_acc_dokumen(Request $request)
    {

        $draw = intval($request->draw);
        $start = intval($request->start);
        $length = intval($request->length);
        $rekap_pengajuan = $this->ept_get_wna_acc_dokumen();
        $data = array();
        $i = 0;
        foreach ($rekap_pengajuan as $r)
        {
             $img = '';
            if (!empty($r->img_15)){
                if (strpos($r->img_15, 'pdf') !== false) {
                    $img .= '
                    <div class="row">
                        <div class="col-md-12">
                            <div class="white-box">
                            <embed src="'.url('/').'/'.$r->img_15.'" type="application/pdf"   height="500px" width="100%">
                        </div>
                        </div>
                    </div>';
                }
            }  
            $img .= '<div class="row">
                    <div class="col-md-12">
                        <div class="white-box">
                            <h3 class="box-title">Berkas Pengajuan</h3>
                            <div id="gallery">
                                <div id="gallery-content">
                                    <div id="gallery-content-center">';
            if (!empty($r->img_1)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 1" href="'.url('/').'/'.$r->img_1.'"><img src="'.url('/').'/'.$r->img_thumb_1.'" alt=""></a>';
            }
            if (!empty($r->img_2)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 2" href="'.url('/').'/'.$r->img_2.'"><img src="'.url('/').'/'.$r->img_thumb_2.'" alt=""></a>';
            }
            if (!empty($r->img_3)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 3" href="'.url('/').'/'.$r->img_3.'"><img src="'.url('/').'/'.$r->img_thumb_3.'" alt=""></a>';
            }
            if (!empty($r->img_4)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 4" href="'.url('/').'/'.$r->img_4.'"><img src="'.url('/').'/'.$r->img_thumb_4.'" alt=""></a>';
            }
            if (!empty($r->img_5)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 5" href="'.url('/').'/'.$r->img_5.'"><img src="'.url('/').'/'.$r->img_thumb_5.'" alt=""></a>';
            }
            if (!empty($r->img_6)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 6" href="'.url('/').'/'.$r->img_6.'"><img src="'.url('/').'/'.$r->img_thumb_6.'" alt=""></a>';
            }
            if (!empty($r->img_7)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 7" href="'.url('/').'/'.$r->img_7.'"><img src="'.url('/').'/'.$r->img_thumb_7.'" alt=""></a>';
            }
            if (!empty($r->img_8)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 8" href="'.url('/').'/'.$r->img_8.'"><img src="'.url('/').'/'.$r->img_thumb_8.'" alt=""></a>';
            }
            if (!empty($r->img_9)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 9" href="'.url('/').'/'.$r->img_9.'"><img src="'.url('/').'/'.$r->img_thumb_9.'" alt=""></a>';
            }
            if (!empty($r->img_10)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 10" href="'.url('/').'/'.$r->img_10.'"><img src="'.url('/').'/'.$r->img_thumb_10.'" alt=""></a>';
            }
            if (!empty($r->img_11)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 11" href="'.url('/').'/'.$r->img_11.'"><img src="'.url('/').'/'.$r->img_thumb_11.'" alt=""></a>';
            }
            if (!empty($r->img_12)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 12" href="'.url('/').'/'.$r->img_12.'"><img src="'.url('/').'/'.$r->img_thumb_12.'" alt=""></a>';
            }
            if (!empty($r->img_13)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 13" href="'.url('/').'/'.$r->img_13.'"><img src="'.url('/').'/'.$r->img_thumb_13.'" alt=""></a>';
            }
            if (!empty($r->img_14)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 14" href="'.url('/').'/'.$r->img_14.'"><img src="'.url('/').'/'.$r->img_thumb_14.'" alt=""></a>';
            }
            if (!empty($r->img_15)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 15" href="'.url('/').'/'.$r->img_15.'"><img src="'.url('/').'/'.$r->img_thumb_15.'" alt=""></a>';
            }
            $img .='</div>
                        </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    </div>
                </div>';
            $i++;
            $data[] = array(
                "no" => $i,
                "nik" => $r->nik,
                "nama_lgkp" => $r->nama_lgkp,
                "telepon" => $r->telepon,
                "email" => $r->email,
                "created_at" => $r->created_at,
                "dok_imgr_desc" => ($r->dok_imgr_desc == null) ? '-' : $r->dok_imgr_desc,
                "img" => $img,
                "aksi" => '
                    <button type="button" class="btn btn-info btn-circle"  id="btn-edit" onclick="edit('.$r->id.');" ><i class="fa fa-search"></i></button> 
                    &nbsp <button type="button" class="btn btn-danger btn-circle" id="btn-hapus" onclick="hapus('.$r->id.',\''.str_replace("'", "", $r->nama_lgkp).'\','.$r->nik.');" ><i class="fa fa-trash-o"></i></button>
                    &nbsp <button type="button" class="btn btn-info btn-circle" id="btn-print" onclick="print_data('.$r->id.');" ><i class="fa  fa-print"></i></button>
                    &nbsp <button type="button" class="btn btn-warning btn-circle" id="btn-pesan" onclick="pesan('.$r->id.','.$r->nik.');" ><i class="fa fa-envelope"></i></button>
                    &nbsp <button type="button" class="btn btn-success btn-circle" id="btn-proses" onclick="beres('.$r->id.',\''.$r->nama_lgkp.'\','.$r->nik.','.$r->dok_imgr.');" ><i class="fa fa-check"></i></button>
                    '
                );
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => count($rekap_pengajuan) ,
            "recordsFiltered" => count($rekap_pengajuan) ,
            "data" => $data
        );
        return $output;
        exit();
    }

    public function get_wna_arsip(Request $request)
    {

        $draw = intval($request->draw);
        $start = intval($request->start);
        $length = intval($request->length);
        $rekap_pengajuan = $this->ept_get_wna_arsip();
        $data = array();
        $i = 0;
        foreach ($rekap_pengajuan as $r)
        {
             $img = '';
            if (!empty($r->img_15)){
                if (strpos($r->img_15, 'pdf') !== false) {
                    $img .= '
                    <div class="row">
                        <div class="col-md-12">
                            <div class="white-box">
                            <embed src="'.url('/').'/'.$r->img_15.'" type="application/pdf"   height="500px" width="100%">
                        </div>
                        </div>
                    </div>';
                }
            }  
            $img .= '<div class="row">
                    <div class="col-md-12">
                        <div class="white-box">
                            <h3 class="box-title">Berkas Pengajuan</h3>
                            <div id="gallery">
                                <div id="gallery-content">
                                    <div id="gallery-content-center">';
            if (!empty($r->img_1)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 1" href="'.url('/').'/'.$r->img_1.'"><img src="'.url('/').'/'.$r->img_thumb_1.'" alt=""></a>';
            }
            if (!empty($r->img_2)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 2" href="'.url('/').'/'.$r->img_2.'"><img src="'.url('/').'/'.$r->img_thumb_2.'" alt=""></a>';
            }
            if (!empty($r->img_3)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 3" href="'.url('/').'/'.$r->img_3.'"><img src="'.url('/').'/'.$r->img_thumb_3.'" alt=""></a>';
            }
            if (!empty($r->img_4)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 4" href="'.url('/').'/'.$r->img_4.'"><img src="'.url('/').'/'.$r->img_thumb_4.'" alt=""></a>';
            }
            if (!empty($r->img_5)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 5" href="'.url('/').'/'.$r->img_5.'"><img src="'.url('/').'/'.$r->img_thumb_5.'" alt=""></a>';
            }
            if (!empty($r->img_6)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 6" href="'.url('/').'/'.$r->img_6.'"><img src="'.url('/').'/'.$r->img_thumb_6.'" alt=""></a>';
            }
            if (!empty($r->img_7)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 7" href="'.url('/').'/'.$r->img_7.'"><img src="'.url('/').'/'.$r->img_thumb_7.'" alt=""></a>';
            }
            if (!empty($r->img_8)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 8" href="'.url('/').'/'.$r->img_8.'"><img src="'.url('/').'/'.$r->img_thumb_8.'" alt=""></a>';
            }
            if (!empty($r->img_9)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 9" href="'.url('/').'/'.$r->img_9.'"><img src="'.url('/').'/'.$r->img_thumb_9.'" alt=""></a>';
            }
            if (!empty($r->img_10)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 10" href="'.url('/').'/'.$r->img_10.'"><img src="'.url('/').'/'.$r->img_thumb_10.'" alt=""></a>';
            }
            if (!empty($r->img_11)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 11" href="'.url('/').'/'.$r->img_11.'"><img src="'.url('/').'/'.$r->img_thumb_11.'" alt=""></a>';
            }
            if (!empty($r->img_12)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 12" href="'.url('/').'/'.$r->img_12.'"><img src="'.url('/').'/'.$r->img_thumb_12.'" alt=""></a>';
            }
            if (!empty($r->img_13)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 13" href="'.url('/').'/'.$r->img_13.'"><img src="'.url('/').'/'.$r->img_thumb_13.'" alt=""></a>';
            }
            if (!empty($r->img_14)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 14" href="'.url('/').'/'.$r->img_14.'"><img src="'.url('/').'/'.$r->img_thumb_14.'" alt=""></a>';
            }
            if (!empty($r->img_15)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 15" href="'.url('/').'/'.$r->img_15.'"><img src="'.url('/').'/'.$r->img_thumb_15.'" alt=""></a>';
            }
            $img .='</div>
                        </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    </div>
                </div>';
            $i++;
            $data[] = array(
                "no" => $i,
                "nik" => $r->nik,
                "nama_lgkp" => $r->nama_lgkp,
                "telepon" => $r->telepon,
                "email" => $r->email,
                "created_at" => $r->created_at,
                "dok_imgr_desc" => ($r->dok_imgr_desc == null) ? '-' : $r->dok_imgr_desc,
                "img" => $img,
                "aksi" => '
                    <button type="button" class="btn btn-info btn-circle"  id="btn-edit" onclick="edit('.$r->id.');" ><i class="fa fa-search"></i></button> 
                    &nbsp <button type="button" class="btn btn-danger btn-circle" id="btn-hapus" onclick="hapus('.$r->id.',\''.str_replace("'", "", $r->nama_lgkp).'\','.$r->nik.');" ><i class="fa fa-trash-o"></i></button>
                    &nbsp <button type="button" class="btn btn-info btn-circle" id="btn-print" onclick="print_data('.$r->id.');" ><i class="fa  fa-print"></i></button>
                    '
                );
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => count($rekap_pengajuan) ,
            "recordsFiltered" => count($rekap_pengajuan) ,
            "data" => $data
        );
        return $output;
        exit();
    }
    public function get_wna_batal(Request $request)
    {

        $draw = intval($request->draw);
        $start = intval($request->start);
        $length = intval($request->length);
        $rekap_pengajuan = $this->ept_get_wna_batal();
        $data = array();
        $i = 0;
        foreach ($rekap_pengajuan as $r)
        {
             $img = '';
            if (!empty($r->img_15)){
                if (strpos($r->img_15, 'pdf') !== false) {
                    $img .= '
                    <div class="row">
                        <div class="col-md-12">
                            <div class="white-box">
                            <embed src="'.url('/').'/'.$r->img_15.'" type="application/pdf"   height="500px" width="100%">
                        </div>
                        </div>
                    </div>';
                }
            }  
            $img .= '<div class="row">
                    <div class="col-md-12">
                        <div class="white-box">
                            <h3 class="box-title">Berkas Pengajuan</h3>
                            <div id="gallery">
                                <div id="gallery-content">
                                    <div id="gallery-content-center">';
            if (!empty($r->img_1)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 1" href="'.url('/').'/'.$r->img_1.'"><img src="'.url('/').'/'.$r->img_thumb_1.'" alt=""></a>';
            }
            if (!empty($r->img_2)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 2" href="'.url('/').'/'.$r->img_2.'"><img src="'.url('/').'/'.$r->img_thumb_2.'" alt=""></a>';
            }
            if (!empty($r->img_3)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 3" href="'.url('/').'/'.$r->img_3.'"><img src="'.url('/').'/'.$r->img_thumb_3.'" alt=""></a>';
            }
            if (!empty($r->img_4)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 4" href="'.url('/').'/'.$r->img_4.'"><img src="'.url('/').'/'.$r->img_thumb_4.'" alt=""></a>';
            }
            if (!empty($r->img_5)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 5" href="'.url('/').'/'.$r->img_5.'"><img src="'.url('/').'/'.$r->img_thumb_5.'" alt=""></a>';
            }
            if (!empty($r->img_6)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 6" href="'.url('/').'/'.$r->img_6.'"><img src="'.url('/').'/'.$r->img_thumb_6.'" alt=""></a>';
            }
            if (!empty($r->img_7)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 7" href="'.url('/').'/'.$r->img_7.'"><img src="'.url('/').'/'.$r->img_thumb_7.'" alt=""></a>';
            }
            if (!empty($r->img_8)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 8" href="'.url('/').'/'.$r->img_8.'"><img src="'.url('/').'/'.$r->img_thumb_8.'" alt=""></a>';
            }
            if (!empty($r->img_9)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 9" href="'.url('/').'/'.$r->img_9.'"><img src="'.url('/').'/'.$r->img_thumb_9.'" alt=""></a>';
            }
            if (!empty($r->img_10)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 10" href="'.url('/').'/'.$r->img_10.'"><img src="'.url('/').'/'.$r->img_thumb_10.'" alt=""></a>';
            }
            if (!empty($r->img_11)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 11" href="'.url('/').'/'.$r->img_11.'"><img src="'.url('/').'/'.$r->img_thumb_11.'" alt=""></a>';
            }
            if (!empty($r->img_12)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 12" href="'.url('/').'/'.$r->img_12.'"><img src="'.url('/').'/'.$r->img_thumb_12.'" alt=""></a>';
            }
            if (!empty($r->img_13)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 13" href="'.url('/').'/'.$r->img_13.'"><img src="'.url('/').'/'.$r->img_thumb_13.'" alt=""></a>';
            }
            if (!empty($r->img_14)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 14" href="'.url('/').'/'.$r->img_14.'"><img src="'.url('/').'/'.$r->img_thumb_14.'" alt=""></a>';
            }
            if (!empty($r->img_15)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 15" href="'.url('/').'/'.$r->img_15.'"><img src="'.url('/').'/'.$r->img_thumb_15.'" alt=""></a>';
            }
            $img .='</div>
                        </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel panel-info block4">
                            <div class="panel-heading"> <button type="button" class="btn btn-info btn-circle"><i class="fa fa-calendar"></i> </button>'.$r->tgl_notif.'
                            <br>
                            <button type="button" class="btn btn-info btn-circle"><i class="fa  fa-play"></i> </button>'.$r->message.' 
                                <div class="pull-right">  </div>
                            </div>
                        </div>
                    </div>
                </div>';
            $i++;
            $data[] = array(
                "no" => $i,
                "nik" => $r->nik,
                "nama_lgkp" => $r->nama_lgkp,
                "telepon" => $r->telepon,
                "email" => $r->email,
                "dok_imgr_desc" => ($r->dok_imgr_desc == null) ? '-' : $r->dok_imgr_desc,
                "created_at" => $r->created_at,
                "img" => $img,
                "aksi" => '
                    <button type="button" class="btn btn-info btn-circle"  id="btn-edit" onclick="edit('.$r->id.');" ><i class="fa fa-search"></i></button> 
                    &nbsp <button type="button" class="btn btn-danger btn-circle" id="btn-hapus" onclick="hapus('.$r->id.',\''.str_replace("'", "", $r->nama_lgkp).'\','.$r->nik.');" ><i class="fa fa-trash-o"></i></button>
                    &nbsp <button type="button" class="btn btn-info btn-circle" id="btn-print" onclick="print_data('.$r->id.');" ><i class="fa  fa-print"></i></button>
                    '
                );
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => count($rekap_pengajuan) ,
            "recordsFiltered" => count($rekap_pengajuan) ,
            "data" => $data
        );
        return $output;
        exit();
    }
    
    public function get_wna_melengkapi(Request $request)
    {

        $draw = intval($request->draw);
        $start = intval($request->start);
        $length = intval($request->length);
        $rekap_pengajuan = $this->ept_get_wna_melengkapi();
        $data = array();
        $i = 0;
        foreach ($rekap_pengajuan as $r)
        {
             $img = '';
            if (!empty($r->img_15)){
                if (strpos($r->img_15, 'pdf') !== false) {
                    $img .= '
                    <div class="row">
                        <div class="col-md-12">
                            <div class="white-box">
                            <embed src="'.url('/').'/'.$r->img_15.'" type="application/pdf"   height="500px" width="100%">
                        </div>
                        </div>
                    </div>';
                }
            }  
            $img .= '<div class="row">
                    <div class="col-md-12">
                        <div class="white-box">
                            <h3 class="box-title">Berkas Pengajuan</h3>
                            <div id="gallery">
                                <div id="gallery-content">
                                    <div id="gallery-content-center">';
            if (!empty($r->img_1)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 1" href="'.url('/').'/'.$r->img_1.'"><img src="'.url('/').'/'.$r->img_thumb_1.'" alt=""></a>';
            }
            if (!empty($r->img_2)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 2" href="'.url('/').'/'.$r->img_2.'"><img src="'.url('/').'/'.$r->img_thumb_2.'" alt=""></a>';
            }
            if (!empty($r->img_3)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 3" href="'.url('/').'/'.$r->img_3.'"><img src="'.url('/').'/'.$r->img_thumb_3.'" alt=""></a>';
            }
            if (!empty($r->img_4)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 4" href="'.url('/').'/'.$r->img_4.'"><img src="'.url('/').'/'.$r->img_thumb_4.'" alt=""></a>';
            }
            if (!empty($r->img_5)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 5" href="'.url('/').'/'.$r->img_5.'"><img src="'.url('/').'/'.$r->img_thumb_5.'" alt=""></a>';
            }
            if (!empty($r->img_6)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 6" href="'.url('/').'/'.$r->img_6.'"><img src="'.url('/').'/'.$r->img_thumb_6.'" alt=""></a>';
            }
            if (!empty($r->img_7)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 7" href="'.url('/').'/'.$r->img_7.'"><img src="'.url('/').'/'.$r->img_thumb_7.'" alt=""></a>';
            }
            if (!empty($r->img_8)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 8" href="'.url('/').'/'.$r->img_8.'"><img src="'.url('/').'/'.$r->img_thumb_8.'" alt=""></a>';
            }
            if (!empty($r->img_9)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 9" href="'.url('/').'/'.$r->img_9.'"><img src="'.url('/').'/'.$r->img_thumb_9.'" alt=""></a>';
            }
            if (!empty($r->img_10)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 10" href="'.url('/').'/'.$r->img_10.'"><img src="'.url('/').'/'.$r->img_thumb_10.'" alt=""></a>';
            }
            if (!empty($r->img_11)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 11" href="'.url('/').'/'.$r->img_11.'"><img src="'.url('/').'/'.$r->img_thumb_11.'" alt=""></a>';
            }
            if (!empty($r->img_12)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 12" href="'.url('/').'/'.$r->img_12.'"><img src="'.url('/').'/'.$r->img_thumb_12.'" alt=""></a>';
            }
            if (!empty($r->img_13)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 13" href="'.url('/').'/'.$r->img_13.'"><img src="'.url('/').'/'.$r->img_thumb_13.'" alt=""></a>';
            }
            if (!empty($r->img_14)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 14" href="'.url('/').'/'.$r->img_14.'"><img src="'.url('/').'/'.$r->img_thumb_14.'" alt=""></a>';
            }
            if (!empty($r->img_15)){
                $img .= '<a class="col-md-3" data-magnify="gallery" data-caption="Pengajuan '.$r->nama_lgkp.' 15" href="'.url('/').'/'.$r->img_15.'"><img src="'.url('/').'/'.$r->img_thumb_15.'" alt=""></a>';
            }
            $img .='</div>
                        </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel panel-info block4">
                            <div class="panel-heading"> <button type="button" class="btn btn-info btn-circle"><i class="fa fa-calendar"></i> </button>'.$r->tgl_notif.'
                            <br>
                            <button type="button" class="btn btn-info btn-circle"><i class="fa  fa-play"></i> </button>'.$r->message.' 
                                <div class="pull-right">  </div>
                            </div>
                        </div>
                    </div>
                </div>';
            $i++;
            $data[] = array(
                "no" => $i,
                "nik" => $r->nik,
                "nama_lgkp" => $r->nama_lgkp,
                "telepon" => $r->telepon,
                "email" => $r->email,
                "dok_imgr_desc" => ($r->dok_imgr_desc == null) ? '-' : $r->dok_imgr_desc,
                "created_at" => $r->created_at,
                "img" => $img,
                "aksi" => '
                     <button type="button" class="btn btn-info btn-circle"  id="btn-edit" onclick="edit('.$r->id.');" ><i class="fa fa-search"></i></button> 
                    &nbsp <button type="button" class="btn btn-danger btn-circle" id="btn-hapus" onclick="hapus('.$r->id.',\''.str_replace("'", "", $r->nama_lgkp).'\','.$r->nik.');" ><i class="fa fa-trash-o"></i></button>
                    &nbsp <button type="button" class="btn btn-info btn-circle" id="btn-print" onclick="print_data('.$r->id.');" ><i class="fa  fa-print"></i></button>
                    &nbsp <button type="button" class="btn btn-warning btn-circle" id="btn-pesan" onclick="pesan('.$r->id.','.$r->nik.');" ><i class="fa fa-envelope"></i></button>
                    &nbsp <button type="button" class="btn btn-success btn-circle" id="btn-proses" onclick="proses('.$r->id.',\''.str_replace("'", "", $r->nama_lgkp).'\','.$r->nik.');" ><i class="fa fa-check"></i></button>
                    '
                );
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => count($rekap_pengajuan) ,
            "recordsFiltered" => count($rekap_pengajuan) ,
            "data" => $data
        );
        return $output;
        exit();
    }

    public function get_wna_edit(request $request){
        header('Content-type: application/json');
        $pengajuan = $request->id_peng;
        return $this->ept_get_wna_edit($pengajuan);
    }

    
    public function get_wna_delete(Request $request){
        header('Content-type: application/json');
        if ($request->id_peng != null){
            $pengajuan = $request->id_peng;
            $nik = $request->nik;
            $this->ept_get_wna_delete($pengajuan);
            $this->ept_get_notif_delete($nik);
            $output = array(
                    "message_type"=>1,
                    "message"=>"Pengajuan Ini Telah Dihapus !"
            );
            return $output; 
        }
    }

    public function get_wna_acc(Request $request){
        header('Content-type: application/json');
        if ($request->id_peng != null){
            $pengajuan = $request->id_peng;
            $nik = $request->nik;
            $notif = 'Dokumen wna Anda Telah Selesai Diverifikasi, Menunggu Proses Publish.';
            $jenis_layanan = 1;
            $ntf = 'pemberitahuan';
            $jenis = 'PENGAJUAN';
            $from_user_id = Session::get('S_NIK');
            // AndrApi::sendnotification($jenis,$pengajuan,$jenis_layanan,$nik,$notif,$ntf,$from_user_id);
            $this->ept_get_wna_acc($pengajuan);
            $output = array(
                    "message_type"=>1,
                    "message"=>"Pengajuan Ini Telah Dipublish !"
            );
            return $output; 
        }
    }
    public function get_wna_beres(Request $request){
        header('Content-type: application/json');
        if ($request->id_peng != null){
            $pengajuan = $request->id_peng;
            $nik = $request->nik;
            $jenis_dok = $request->jenis_dok;
            if($jenis_dok == 1 ){
                $user = User::where('nik', $nik)->get();
                $telepon=$user[0]->telepon;
                $email=$user[0]->email;
                $android_token=$user[0]->android_token;
                $nama_lgkp=$user[0]->name;
                $to_user_id=$user[0]->id;
                $now = Carbon::now();
                $query=DB::connection('webpunten')->select("select * from (select tgl_pengambilan,jam,urutan from pengambilan order by tgl_pengambilan desc, tgl_pengambilan)where rownum = 1");
                if($query){
                  $tglPengambilan = Date::parse($query[0]->tgl_pengambilan)->format('Y-m-d H:i');
                  $tglPengambilan = Date::parse($tglPengambilan)->addMinutes(10);
                  $jam= Date::parse($tglPengambilan)->format('H:i');
                  $urutan=$query[0]->urutan;
                  $hari = $this->hariId(Date::parse($tglPengambilan)->format('l'));
                  if($urutan>=50){
                    $urutan=1;
                    $tglPengambilan = $tglPengambilan->addDays(1);
                    $jam='08:00';
                    $hari = $this->hariId(Date::parse($tglPengambilan)->format('l'));
                  }
                  elseif (($urutan < 50) && ($tglPengambilan->format('Y-m-d') <= Carbon::now()->format('Y-m-d'))) {
                    $urutan=1;
                    $tglPengambilan = $tglPengambilan->addDays(1);
                    $jam='08:00';
                    $hari = $this->hariId(Date::parse($tglPengambilan)->format('l'));
                  }
                  else{
                    $urutan=$urutan+1;
                  }
                  if( (Carbon::createFromFormat('H:i', $jam) > Carbon::createFromFormat('H:i', '11:30')) && (Carbon::createFromFormat('H:i', $jam) < Carbon::createFromFormat('H:i', '13:00')) && ($hari == "JUMAT") ) {
                    $jam='13:00';
                  }
                  elseif(Carbon::createFromFormat('H:i', $jam) > Carbon::createFromFormat('H:i', '15:00')){
                    $urutan=1;
                    $tglPengambilan = $tglPengambilan->addDays(1);
                    $jam='08:00';
                    $hari = $this->hariId(Date::parse($tglPengambilan)->format('l'));
                  }
                  else if ((Carbon::createFromFormat('H:i', $jam) > Carbon::createFromFormat('H:i', '12:00') ) && (Carbon::createFromFormat('H:i', $jam) < Carbon::createFromFormat('H:i', '13:00') )) {
                    $jam='13:00';
                  }
                }
                if(!$query){
                  $urutan=1;
                  $jam='08:00';
                  $tglPengambilan = $now->addDays(1);
                  $hari = $this->hariId(Date::parse($tglPengambilan)->format('l'));
                }
                if($tglPengambilan <= $now){
                  $tglPengambilan = $now->addDays(1);
                  $hari = $this->hariId(Date::parse($tglPengambilan)->format('l'));
                }
                if ($hari=='SABTU'){
                  $tglPengambilan = $tglPengambilan->addDays(2);
                }
                if ($hari=='MINGGU'){
                  $tglPengambilan = $tglPengambilan->addDays(1);
                }
                $tglPengambilan=Date::parse($tglPengambilan)->format('Y-m-d');
                $query=DB::connection('webpunten')->select("select * from HARI_LIBUR where TANGGAL_LIBUR = TO_DATE('".$tglPengambilan."','yyyy-MM-dd')");
                if($query){
                  $jumlah=$query[0]->jumlah_libur;
                  //$tglPengambilan = $tglPengambilan->addDays($jumlah);
                  // $tglPengambilan = Carbon::createFromFormat('Y-m-d', $tglPengambilan)->addDays($jumlah);
                  $tglPengambilan = Carbon::createFromFormat('Y-m-d', $tglPengambilan)->addDays($jumlah);
                  $tglPengambilan = Date::parse($tglPengambilan)->format('Y-m-d');
                }
                //$tglPengambilan=Date::parse($tglPengambilan)->format('Y-m-d');
                $hari = $this->hariId(Date::parse($tglPengambilan)->format('l'));
                if (strpos($nama_lgkp, '\'') !== false) {
                  $query=DB::connection('webpunten')->insert("insert into pengambilan (nik,nama_lgkp,tgl_pengambilan,urutan,hari,jam,jenis_dok) values (".$nik.",q'[".$nama_lgkp."]',TO_DATE('".$tglPengambilan." ".$jam."','yyyy-MM-dd hh24:mi'),".$urutan.",'".$hari."','".$jam."','SKTT')");
                } else {
                  $query=DB::connection('webpunten')->insert("insert into pengambilan (nik,nama_lgkp,tgl_pengambilan,urutan,hari,jam,jenis_dok) values (".$nik.",'".$nama_lgkp."',TO_DATE('".$tglPengambilan." ".$jam."','yyyy-MM-dd hh24:mi'),".$urutan.",'".$hari."','".$jam."','SKTT')");
                }
                $subjek = 'Pengajuan Pembuatan SKTT';
                $tglPengambilan=Date::parse($tglPengambilan)->format('d-m-Y');
                  $notif = "Dokumen SKTT Anda dapat diambil pada :
Hari/Tanggal : ".$hari.", ".$tglPengambilan."
Jam : ".$jam."
Urutan : ".$urutan."
Loket : C
* Dengan membawa berkas yang telah di upload.";
                    $ntf = 'pemberitahuan';
                    $from_user_id = 'DISDUKCAPIL KOTA BANDUNG';
                    // AndrApi::sendnotification(null,3,null,$nik,$notif,$ntf,$from_user_id);
                  if ($telepon != null){
                    $pesan = "Jadwal pengambilan SKTT\n".$hari.", ".$tglPengambilan."\nJam : ".$jam."\nUrutan : ".$urutan."\nLoket : C\n* Bawa semua berkas asli untuk verifikasi data jika melakukan pengajuan online.";
                    $query=DB::connection('webpunten')->insert("Insert into OUTBOX@SMSDISDUK
                    (TELEPON, ISI_PESAN, TANGGAL_SMS, SERVER)
                    Values
                    ('".$telepon."', '".$pesan."', TO_DATE('".date('m/d/Y h:i:s')."', 'MM/DD/YYYY HH24:MI:SS'), '1')");
                    $this->pushData("by-me", $telepon, $pesan);
                    $this->kirimSMS($telepon, $pesan);
                  }
                        
            }else{
                $notif = 'Dokumen Anda telah selesai dan dapat didownload pada menu Dokumen dihalaman utama atau Email Kepala Keluarga. Gunakan kertas HVS ukuran A4 80 Gram untuk melakukan pencetakan. Silahkan Anda datang ke Kantor Dissdukcapil untuk melakukan perekaman KTP-EL dengan memperlihatkan notifikasi ini kepada petugas Loket C.';    
            }
            
            $jenis_layanan = 1;
            $ntf = 'pemberitahuan';
            $jenis = 'PENGAJUAN';
            $from_user_id = Session::get('S_NIK');
            AndrApi::sendnotification($jenis,$pengajuan,$jenis_layanan,$nik,$notif,$ntf,$from_user_id);
            $this->ept_get_wna_beres($pengajuan);
            $output = array(
                    "message_type"=>1,
                    "message"=>"Pengajuan Ini Telah Dipublish !"
            );
            return $output; 
        }
    }
    public function get_wna_tolak(Request $request){
        header('Content-type: application/json');
        if ($request->id_peng != null){
            $pengajuan = $request->id_peng;
            $notif = $request->isi_pesan;
            $jenis_layanan = $request->jenis_layanan_tolak;
            $nik = $request->nik_tolak;
            $ntf = 'pemberitahuan';
            $jenis = 'PENGAJUAN';
            $from_user_id = Session::get('S_NIK');
            $nama_lgkp = Session::get('S_NAMA_LGKP');
            AndrApi::sendnotification($jenis,$pengajuan,$jenis_layanan,$nik,$notif,$ntf,$from_user_id);
            AndrApi::chat_send_api($from_user_id,$nik,$nama_lgkp,$notif);
            $this->ept_get_wna_tolak($pengajuan);
            $output = array(
                    "message_type"=>1,
                    "message"=>"Pengajuan Ini Telah Ditolak !"
            );
            return $output; 
        }
    }

    public function ept_get_skts_baru()
    {
        header("Content-Type: application/json", true);
         $sql = "SELECT A.ID, A.NIK, A.NO_KK, A.NAMA_LGKP,CASE WHEN A.TELEPON IS NULL THEN '-' ELSE A.TELEPON END TELEPON,CASE WHEN A.EMAIL IS NULL THEN '-' ELSE A.EMAIL END EMAIL, TO_CHAR(A.CREATED_AT,'DD-MM-YYYY HH24:MI:SS') CREATED_AT, IMG_1 ,IMG_THUMB_1 ,IMG_2 ,IMG_THUMB_2 ,IMG_3 ,IMG_THUMB_3 ,IMG_4 ,IMG_THUMB_4 ,IMG_5 ,IMG_THUMB_5 ,IMG_6 ,IMG_THUMB_6 ,IMG_7 ,IMG_THUMB_7 ,IMG_8 ,IMG_THUMB_8 ,IMG_9 ,IMG_THUMB_9 ,IMG_10 ,IMG_THUMB_10 ,IMG_11 ,IMG_THUMB_11 ,IMG_12 ,IMG_THUMB_12 ,IMG_13 ,IMG_THUMB_13 ,IMG_14 ,IMG_THUMB_14 ,IMG_15 ,IMG_THUMB_15, JUM_ANGGOTA
         FROM PENGAJUANSKTS A 
         WHERE A.PROC_STAT =1 AND IMG_2 IS NOT NULL AND TRUNC(A.TGL_BERLAKU) > TRUNC(SYSDATE)
         ";
         $r = DB::connection('webpunten')->select($sql);
        return $r;
    }

    public function ept_get_skts_anggota($no_kk)
    {
        header("Content-Type: application/json", true);
         $sql = "SELECT A.ID, A.NIK, A.NO_KK, A.NAMA_LGKP,CASE WHEN A.TELEPON IS NULL THEN '-' ELSE A.TELEPON END TELEPON,CASE WHEN A.EMAIL IS NULL THEN '-' ELSE A.EMAIL END EMAIL,UPPER(F5_GET_REF_WNI(A.STAT_HBKEL, 301)) STAT_HBKEL_DESC, TO_CHAR(A.CREATED_AT,'DD-MM-YYYY HH24:MI:SS') CREATED_AT, IMG_1 ,IMG_THUMB_1 ,IMG_2 ,IMG_THUMB_2 ,IMG_3 ,IMG_THUMB_3 ,IMG_4 ,IMG_THUMB_4 ,IMG_5 ,IMG_THUMB_5 ,IMG_6 ,IMG_THUMB_6 ,IMG_7 ,IMG_THUMB_7 ,IMG_8 ,IMG_THUMB_8 ,IMG_9 ,IMG_THUMB_9 ,IMG_10 ,IMG_THUMB_10 ,IMG_11 ,IMG_THUMB_11 ,IMG_12 ,IMG_THUMB_12 ,IMG_13 ,IMG_THUMB_13 ,IMG_14 ,IMG_THUMB_14 ,IMG_15 ,IMG_THUMB_15, JUM_ANGGOTA
         FROM PENGAJUANSKTS A 
         WHERE IMG_2 IS NULL AND NO_KK = $no_kk  AND TRUNC(A.TGL_BERLAKU) > TRUNC(SYSDATE)
         ";
         $r = DB::connection('webpunten')->select($sql);
        return $r;
    }

    public function ept_get_skts_acc_dokumen()
    {
        header("Content-Type: application/json", true);
         $sql = "SELECT A.ID, A.NIK, A.NO_KK, A.NAMA_LGKP,CASE WHEN A.TELEPON IS NULL THEN '-' ELSE A.TELEPON END TELEPON,CASE WHEN A.EMAIL IS NULL THEN '-' ELSE A.EMAIL END EMAIL, TO_CHAR(A.CREATED_AT,'DD-MM-YYYY HH24:MI:SS') CREATED_AT, IMG_1 ,IMG_THUMB_1 ,IMG_2 ,IMG_THUMB_2 ,IMG_3 ,IMG_THUMB_3 ,IMG_4 ,IMG_THUMB_4 ,IMG_5 ,IMG_THUMB_5 ,IMG_6 ,IMG_THUMB_6 ,IMG_7 ,IMG_THUMB_7 ,IMG_8 ,IMG_THUMB_8 ,IMG_9 ,IMG_THUMB_9 ,IMG_10 ,IMG_THUMB_10 ,IMG_11 ,IMG_THUMB_11 ,IMG_12 ,IMG_THUMB_12 ,IMG_13 ,IMG_THUMB_13 ,IMG_14 ,IMG_THUMB_14 ,IMG_15 ,IMG_THUMB_15 , JUM_ANGGOTA
         FROM PENGAJUANSKTS A
         WHERE A.PROC_STAT =3 AND IMG_2 IS NOT NULL  AND TRUNC(A.TGL_BERLAKU) > TRUNC(SYSDATE)
         ";
         $r = DB::connection('webpunten')->select($sql);
        return $r;
    }

    public function ept_get_skts_arsip()
    {
        header("Content-Type: application/json", true);
         $sql = "SELECT A.ID, A.NIK, A.NO_KK, A.NAMA_LGKP,CASE WHEN A.TELEPON IS NULL THEN '-' ELSE A.TELEPON END TELEPON,CASE WHEN A.EMAIL IS NULL THEN '-' ELSE A.EMAIL END EMAIL, TO_CHAR(A.CREATED_AT,'DD-MM-YYYY HH24:MI:SS') CREATED_AT, IMG_1 ,IMG_THUMB_1 ,IMG_2 ,IMG_THUMB_2 ,IMG_3 ,IMG_THUMB_3 ,IMG_4 ,IMG_THUMB_4 ,IMG_5 ,IMG_THUMB_5 ,IMG_6 ,IMG_THUMB_6 ,IMG_7 ,IMG_THUMB_7 ,IMG_8 ,IMG_THUMB_8 ,IMG_9 ,IMG_THUMB_9 ,IMG_10 ,IMG_THUMB_10 ,IMG_11 ,IMG_THUMB_11 ,IMG_12 ,IMG_THUMB_12 ,IMG_13 ,IMG_THUMB_13 ,IMG_14 ,IMG_THUMB_14 ,IMG_15 ,IMG_THUMB_15 , JUM_ANGGOTA
         FROM PENGAJUANSKTS A
         WHERE A.PROC_STAT >3 AND IMG_2 IS NOT NULL  AND TRUNC(A.TGL_BERLAKU) > TRUNC(SYSDATE)
         ";
         $r = DB::connection('webpunten')->select($sql);
        return $r;
    }

    public function ept_get_skts_batal()
    {
        header("Content-Type: application/json", true);
         $sql = "SELECT A.ID, A.NIK, A.NO_KK, A.NAMA_LGKP,CASE WHEN A.TELEPON IS NULL THEN '-' ELSE A.TELEPON END TELEPON,CASE WHEN A.EMAIL IS NULL THEN '-' ELSE A.EMAIL END EMAIL, TO_CHAR(A.CREATED_AT,'DD-MM-YYYY HH24:MI:SS') CREATED_AT, IMG_1 ,IMG_THUMB_1 ,IMG_2 ,IMG_THUMB_2 ,IMG_3 ,IMG_THUMB_3 ,IMG_4 ,IMG_THUMB_4 ,IMG_5 ,IMG_THUMB_5 ,IMG_6 ,IMG_THUMB_6 ,IMG_7 ,IMG_THUMB_7 ,IMG_8 ,IMG_THUMB_8 ,IMG_9 ,IMG_THUMB_9 ,IMG_10 ,IMG_THUMB_10 ,IMG_11 ,IMG_THUMB_11 ,IMG_12 ,IMG_THUMB_12 ,IMG_13 ,IMG_THUMB_13 ,IMG_14 ,IMG_THUMB_14 ,IMG_15 ,IMG_THUMB_15,B.MESSAGE,TO_CHAR(B.CREATED_AT,'DD-MM-YYYY HH24:MI:SS') TGL_NOTIF, JUM_ANGGOTA
         FROM PENGAJUANSKTS@DBEPUNTEN A LEFT JOIN (SELECT ID,NIK,TO_USER_ID,FROM_USER_ID,MESSAGE,CREATED_AT,PENGAJUAN_ID FROM (SELECT ID,NIK,TO_USER_ID,FROM_USER_ID,MESSAGE,CREATED_AT,PENGAJUAN_ID, RANK() OVER (PARTITION BY PENGAJUAN_ID ORDER BY  CREATED_AT DESC, ID DESC) RNK FROM NOTIFICATION) WHERE RNK = 1) B ON A.ID =B.PENGAJUAN_ID
         WHERE A.PROC_STAT = 2 AND A.STAT_BERKAS IS NOT NULL AND IMG_2 IS NOT NULL  AND TRUNC(A.TGL_BERLAKU) > TRUNC(SYSDATE)
         ";
         $r = DB::select($sql);
        return $r;
    }

    public function ept_get_skts_melengkapi()
    {
        header("Content-Type: application/json", true);
         $sql = "SELECT A.ID, A.NIK, A.NO_KK, A.NAMA_LGKP,CASE WHEN A.TELEPON IS NULL THEN '-' ELSE A.TELEPON END TELEPON,CASE WHEN A.EMAIL IS NULL THEN '-' ELSE A.EMAIL END EMAIL, TO_CHAR(A.CREATED_AT,'DD-MM-YYYY HH24:MI:SS') CREATED_AT, IMG_1 ,IMG_THUMB_1 ,IMG_2 ,IMG_THUMB_2 ,IMG_3 ,IMG_THUMB_3 ,IMG_4 ,IMG_THUMB_4 ,IMG_5 ,IMG_THUMB_5 ,IMG_6 ,IMG_THUMB_6 ,IMG_7 ,IMG_THUMB_7 ,IMG_8 ,IMG_THUMB_8 ,IMG_9 ,IMG_THUMB_9 ,IMG_10 ,IMG_THUMB_10 ,IMG_11 ,IMG_THUMB_11 ,IMG_12 ,IMG_THUMB_12 ,IMG_13 ,IMG_THUMB_13 ,IMG_14 ,IMG_THUMB_14 ,IMG_15 ,IMG_THUMB_15,B.MESSAGE,TO_CHAR(B.CREATED_AT,'DD-MM-YYYY HH24:MI:SS') TGL_NOTIF, JUM_ANGGOTA
         FROM PENGAJUANSKTS@DBEPUNTEN A LEFT JOIN (SELECT ID,NIK,TO_USER_ID,FROM_USER_ID,MESSAGE,CREATED_AT,PENGAJUAN_ID FROM (SELECT ID,NIK,TO_USER_ID,FROM_USER_ID,MESSAGE,CREATED_AT,PENGAJUAN_ID, RANK() OVER (PARTITION BY PENGAJUAN_ID ORDER BY  CREATED_AT DESC, ID DESC) RNK FROM NOTIFICATION) WHERE RNK = 1) B ON A.ID =B.PENGAJUAN_ID
         WHERE A.PROC_STAT = 2 AND A.STAT_BERKAS IS NULL AND IMG_2 IS NOT NULL  AND TRUNC(A.TGL_BERLAKU) > TRUNC(SYSDATE)
         ";
         $r = DB::select($sql);
        return $r;
    }

    public function ept_get_skts_pembaruan()
    {
        header("Content-Type: application/json", true);
         $sql = "SELECT A.ID, A.NIK, A.NO_KK, A.NAMA_LGKP,CASE WHEN A.TELEPON IS NULL THEN '-' ELSE A.TELEPON END TELEPON,CASE WHEN A.EMAIL IS NULL THEN '-' ELSE A.EMAIL END EMAIL, TO_CHAR(A.CREATED_AT,'DD-MM-YYYY HH24:MI:SS') CREATED_AT, TO_CHAR(A.TGL_BERLAKU,'DD-MM-YYYY HH24:MI:SS') TGL_BERLAKU, IMG_1 ,IMG_THUMB_1 ,IMG_2 ,IMG_THUMB_2 ,IMG_3 ,IMG_THUMB_3 ,IMG_4 ,IMG_THUMB_4 ,IMG_5 ,IMG_THUMB_5 ,IMG_6 ,IMG_THUMB_6 ,IMG_7 ,IMG_THUMB_7 ,IMG_8 ,IMG_THUMB_8 ,IMG_9 ,IMG_THUMB_9 ,IMG_10 ,IMG_THUMB_10 ,IMG_11 ,IMG_THUMB_11 ,IMG_12 ,IMG_THUMB_12 ,IMG_13 ,IMG_THUMB_13 ,IMG_14 ,IMG_THUMB_14 ,IMG_15 ,IMG_THUMB_15,B.MESSAGE,TO_CHAR(B.CREATED_AT,'DD-MM-YYYY HH24:MI:SS') TGL_NOTIF, JUM_ANGGOTA
         FROM PENGAJUANSKTS@DBEPUNTEN A LEFT JOIN (SELECT ID,NIK,TO_USER_ID,FROM_USER_ID,MESSAGE,CREATED_AT,PENGAJUAN_ID FROM (SELECT ID,NIK,TO_USER_ID,FROM_USER_ID,MESSAGE,CREATED_AT,PENGAJUAN_ID, RANK() OVER (PARTITION BY PENGAJUAN_ID ORDER BY  CREATED_AT DESC, ID DESC) RNK FROM NOTIFICATION) WHERE RNK = 1) B ON A.ID =B.PENGAJUAN_ID
         WHERE  TRUNC(A.TGL_BERLAKU) < TRUNC(SYSDATE)
         ";
         $r = DB::select($sql);
        return $r;
    }

    public function ept_get_skts_edit($id)
    {
        header("Content-Type: application/json", true);
         $sql = "SELECT A.ID, A.NIK, A.PROC_STAT, A.STAT_BERKAS, A.VERIFIED_AT, A.VERIFIED_BY, A.CREATED_AT, A.UPDATED_AT, A.DELETED_AT, A.DELETED_BY, A.NO_SKTS, A.NAMA_LGKP, A.TMPT_LHR, TO_CHAR(A.TGL_LHR,'YYYY-MM-DD') TGL_LHR, A.JENIS_KLMIN, A.AGAMA, A.STAT_KWN, A.GOL_DRH, A.PENDIDIKAN, A.PEKERJAAN, A.SRC_PROV, F5_GET_NAMA_PROVINSI(A.SRC_PROV) NAMA_SRC_PROV, A.SRC_KAB, F5_GET_NAMA_KABUPATEN(A.SRC_PROV,A.SRC_KAB) NAMA_SRC_KAB, A.SRC_KEC, F5_GET_NAMA_KECAMATAN(A.SRC_PROV,A.SRC_KAB,A.SRC_KEC) NAMA_SRC_KEC, A.SRC_KEL, F5_GET_NAMA_KELURAHAN(A.SRC_PROV,A.SRC_KAB,A.SRC_KEC,A.SRC_KEL) NAMA_SRC_KEL,  A.SRC_ALAMAT, A.SRC_RT, A.SRC_RW, A.PROP,F5_GET_NAMA_PROVINSI(A.PROP) NAMA_PROP, A.KAB,F5_GET_NAMA_KABUPATEN(A.PROP,A.KAB) NAMA_KAB, A.KEC,F5_GET_NAMA_KECAMATAN(A.PROP,A.KAB,A.KEC) NAMA_KEC, A.KEL, F5_GET_NAMA_KELURAHAN(A.PROP,A.KAB,A.KEC,A.KEL) NAMA_KEL, A.ALAMAT, A.NO_RT, A.NO_RW, A.STATUS, A.TGL_BERLAKU, A.JENIS_LAYANAN, A.NO_KK, A.STAT_HBKEL, A.ALASAN_PINDAH, A.JANGKA_WAKTU, A.JUM_ANGGOTA, A.TGL_DATANG, CASE WHEN A.TELEPON IS NULL THEN '-' ELSE A.TELEPON END TELEPON,CASE WHEN A.EMAIL IS NULL THEN '-' ELSE A.EMAIL END EMAIL, TO_CHAR(A.CREATED_AT,'DD-MM-YYYY HH24:MI:SS') CREATED_AT, IMG_1 ,IMG_THUMB_1 ,IMG_2 ,IMG_THUMB_2 ,IMG_3 ,IMG_THUMB_3 ,IMG_4 ,IMG_THUMB_4,IMG_5 ,IMG_THUMB_5, A.STAT_HBKEL, A.ALASAN_PINDAH, A.JUM_ANGGOTA, A.JANGKA_WAKTU, TO_CHAR(A.TGL_DATANG,'YYYY-MM-DD') TGL_DATANG
         FROM PENGAJUANSKTS A
         WHERE 1=1 AND A.ID = $id 
         ";
         $r = DB::connection('webpunten')->select($sql);
        return $r;
    }

    public function ept_get_user_edit($id)
    {
        header("Content-Type: application/json", true);
         $sql = "SELECT A.ID, A.NIK, A.NO_KK, A.NAME NAMA_LGKP, A.EMAIL, A.TELEPON FROM USERS A WHERE 1=1 AND A.ID = $id ";
         $r = DB::select($sql);
        return $r;
    }

    public function edit_data_user(Request $request)
    {
         header('Content-type: application/json');
        if ($request->id_peng != null){
            $id = $request->id_peng;
            $name = strtoupper($request->nama_lgkp);
            $email = $request->email;
            $telepon = $request->telepon;
            $pass = $request->pass;
            $this->ept_edit_data_user($id,$name,$email,$telepon,$pass);
            $output = array(
                    "message_type"=>1,
                    "message"=>"User Berhasil Dirubah !"
            );
            return $output; 
        }
       
    }

    public function change_nik(Request $request)
    {
         header('Content-type: application/json');
        if ($request->nik_lama != null){
            $nik_lama = $request->nik_lama;
            $nik_baru = $request->nik_baru;
            $get_nik = $this->ept_get_nik($nik_baru);
            if($get_nik > 0){
                $output = array(
                    "message_type"=>0,
                    "message"=>"Nik Baru Sudah Terdaftar Dan Aktif !"
                );
            }else{
                $this->ept_change_nik($nik_lama,$nik_baru);
                $output = array(
                        "message_type"=>1,
                        "message"=>"User Berhasil Dirubah !"
                );
            }
            
            return $output; 
        }
       
    }

    public function do_reset_user(Request $request)
    {
         header('Content-type: application/json');
        if ($request->id_peng != null){
            $id = $request->id_peng;
            $this->ept_do_reset_user($id);
            $output = array(
                    "message_type"=>1,
                    "message"=>"Password Berhasil Direset !"
            );
            return $output; 
        }
       
    }

    public function do_edit_psss(Request $request)
    {
         header('Content-type: application/json');
        if ($request->id_peng != null){
            $id = $request->id_peng;
            $pass = $request->pass;
            $this->ept_do_edit_psss($id,$pass);
            $output = array(
                    "message_type"=>1,
                    "message"=>"Password Berhasil Di Rubah !"
            );
            return $output; 
        }
       
    }

    public function do_delete_user(Request $request)
    {
         header('Content-type: application/json');
        if ($request->id_peng != null){
            $id = $request->id_peng;
            $this->ept_do_delete_user($id);
            $output = array(
                    "message_type"=>1,
                    "message"=>"User Berhasil Dihapus !"
            );
            return $output; 
        }
       
    }

    
    

    
    public function ept_get_skts_delete($id){
            $sql = "DELETE FROM PENGAJUANSKTS WHERE NO_KK IN (SELECT NO_KK FROM PENGAJUANSKTS WHERE ID = $id)";
            $r = DB::connection('webpunten')->delete($sql);
            return $r;
    }

    public function ept_get_skts_tolak($id){
            $nik = Session::get('S_NIK');
            $sql = "UPDATE PENGAJUANSKTS SET STAT_BERKAS = 1, PROC_STAT = 2, UPDATED_AT = SYSDATE, VERIFIED_AT = SYSDATE, VERIFIED_BY = '$nik' WHERE NO_KK IN (SELECT NO_KK FROM PENGAJUANSKTS WHERE ID = $id)";
            $r = DB::connection('webpunten')->update($sql);
               return $r;
    }
    public function ept_get_skts_acc($id){
            $nik = Session::get('S_NIK');
            $sql = "UPDATE PENGAJUANSKTS SET STAT_BERKAS = NULL, PROC_STAT = 3, UPDATED_AT = SYSDATE, VERIFIED_AT = SYSDATE, VERIFIED_BY = '$nik' WHERE NO_KK IN (SELECT NO_KK FROM PENGAJUANSKTS WHERE ID = $id)";
            $r = DB::connection('webpunten')->update($sql);
               return $r;
    }
    public function ept_get_skts_beres($id){
            $nik = Session::get('S_NIK');
            $sql = "UPDATE PENGAJUANSKTS SET STAT_BERKAS = NULL, PROC_STAT = 3, UPDATED_AT = SYSDATE, VERIFIED_AT = SYSDATE, VERIFIED_BY = '$nik' WHERE NO_KK IN (SELECT NO_KK FROM PENGAJUANSKTS WHERE ID = $id)";
            $r = DB::connection('webpunten')->update($sql);
               return $r;
    }
    public function ept_update_cetak($nik){
            $sql = "UPDATE PENGAJUANSKTS SET PROC_STAT = 4 WHERE NO_KK IN( SELECT NO_KK FROM PENGAJUANSKTS WHERE PROC_STAT = 4) AND PROC_STAT <> 4 AND NIK = $nik";
            $r = DB::connection('webpunten')->update($sql);
               return $r;
    }

    public function ept_get_datang_baru($no_kec)
    {
        header("Content-Type: application/json", true);
         $sql = "SELECT A.ID, A.NIK, A.NAMA_LGKP,CASE WHEN A.TELEPON IS NULL THEN '-' ELSE A.TELEPON END TELEPON,CASE WHEN A.EMAIL IS NULL THEN '-' ELSE A.EMAIL END EMAIL, TO_CHAR(A.CREATED_AT,'DD-MM-YYYY HH24:MI:SS') CREATED_AT, IMG_1 ,IMG_THUMB_1 ,IMG_2 ,IMG_THUMB_2 ,IMG_3 ,IMG_THUMB_3 ,IMG_4 ,IMG_THUMB_4 ,IMG_5 ,IMG_THUMB_5 ,IMG_6 ,IMG_THUMB_6 ,IMG_7 ,IMG_THUMB_7 ,IMG_8 ,IMG_THUMB_8 ,IMG_9 ,IMG_THUMB_9 ,IMG_10 ,IMG_THUMB_10 ,IMG_11 ,IMG_THUMB_11 ,IMG_12 ,IMG_THUMB_12 ,IMG_13 ,IMG_THUMB_13 ,IMG_14 ,IMG_THUMB_14 ,IMG_15 ,IMG_THUMB_15 ,NO_SKPWNI
         FROM PENGAJUANDATANG A 
         WHERE A.PROC_STAT =1 
         ";
         if ($no_kec != 0){
            $sql .= " AND PROP = 32 AND KAB = 73 AND KEC = $no_kec";
         }
         $r = DB::connection('webpunten')->select($sql);
        return $r;
    }

    public function ept_get_datang_acc_dokumen($no_kec)
    {
        header("Content-Type: application/json", true);
         $sql = "SELECT A.ID, A.NIK, A.NAMA_LGKP,CASE WHEN A.TELEPON IS NULL THEN '-' ELSE A.TELEPON END TELEPON,CASE WHEN A.EMAIL IS NULL THEN '-' ELSE A.EMAIL END EMAIL,CASE WHEN B.NO_KK IS NULL THEN '-' ELSE TO_CHAR(B.NO_KK) END NO_KK, TO_CHAR(A.CREATED_AT,'DD-MM-YYYY HH24:MI:SS') CREATED_AT, IMG_1 ,IMG_THUMB_1 ,IMG_2 ,IMG_THUMB_2 ,IMG_3 ,IMG_THUMB_3 ,IMG_4 ,IMG_THUMB_4 ,IMG_5 ,IMG_THUMB_5 ,IMG_6 ,IMG_THUMB_6 ,IMG_7 ,IMG_THUMB_7 ,IMG_8 ,IMG_THUMB_8 ,IMG_9 ,IMG_THUMB_9 ,IMG_10 ,IMG_THUMB_10 ,IMG_11 ,IMG_THUMB_11 ,IMG_12 ,IMG_THUMB_12 ,IMG_13 ,IMG_THUMB_13 ,IMG_14 ,IMG_THUMB_14 ,IMG_15 ,IMG_THUMB_15 ,NO_SKPWNI, CASE WHEN C.CERT_STATUS IS NULL THEN '-'  ELSE TO_CHAR(C.CERT_STATUS) END CERT_STATUS
         FROM PENGAJUANDATANG A LEFT JOIN BIODATA_WNI@DB222 B ON A.NIK = B.NIK
         LEFT JOIN 
                          (
                            SELECT 
                            NO_DOC
                            , CERT_STATUS
                            , REQ_DATE
                            , REQ_BY
                            , SEQN_ID
                            , URL_DOKUMEN
                            , PEJABAT_PROCCESS_BY
                            , PEJABAT_PROCESS_DATE  
                          FROM 
                          (
                            SELECT 
                            NO_DOC
                            , CERT_STATUS
                            , REQ_DATE
                            , REQ_BY
                            , SEQN_ID
                            , URL_DOKUMEN
                            , PEJABAT_PROCCESS_BY
                            , PEJABAT_PROCESS_DATE 
                            , RANK() OVER (PARTITION BY NO_DOC ORDER BY REQ_DATE DESC,SEQN_ID DESC) RNK FROM BSRE_KARTU_KELUARGA@DB222) 
                            WHERE RNK = 1) C ON B.NO_KK = C.NO_DOC
         WHERE A.PROC_STAT =3 
         ";
         if ($no_kec != 0){
            $sql .= " AND PROP = 32 AND KAB = 73 AND KEC = $no_kec";
         }
         $r = DB::connection('webpunten')->select($sql);
        return $r;
    }

    public function ept_get_datang_arsip($no_kec)
    {
        header("Content-Type: application/json", true);
         $sql = "SELECT A.ID, A.NIK, A.NAMA_LGKP,CASE WHEN A.TELEPON IS NULL THEN '-' ELSE A.TELEPON END TELEPON,CASE WHEN A.EMAIL IS NULL THEN '-' ELSE A.EMAIL END EMAIL, TO_CHAR(A.CREATED_AT,'DD-MM-YYYY HH24:MI:SS') CREATED_AT, IMG_1 ,IMG_THUMB_1 ,IMG_2 ,IMG_THUMB_2 ,IMG_3 ,IMG_THUMB_3 ,IMG_4 ,IMG_THUMB_4 ,IMG_5 ,IMG_THUMB_5 ,IMG_6 ,IMG_THUMB_6 ,IMG_7 ,IMG_THUMB_7 ,IMG_8 ,IMG_THUMB_8 ,IMG_9 ,IMG_THUMB_9 ,IMG_10 ,IMG_THUMB_10 ,IMG_11 ,IMG_THUMB_11 ,IMG_12 ,IMG_THUMB_12 ,IMG_13 ,IMG_THUMB_13 ,IMG_14 ,IMG_THUMB_14 ,IMG_15 ,IMG_THUMB_15 ,NO_SKPWNI
         FROM PENGAJUANDATANG A
         WHERE A.PROC_STAT >3  AND IMG_2 IS NOT NULL
         ";
         if ($no_kec != 0){
            $sql .= " AND PROP = 32 AND KAB = 73 AND KEC = $no_kec";
         }
         $r = DB::connection('webpunten')->select($sql);
        return $r;
    }

    public function ept_get_datang_batal($no_kec)
    {
        header("Content-Type: application/json", true);
         $sql = "SELECT A.ID, A.NIK, A.NAMA_LGKP,CASE WHEN A.TELEPON IS NULL THEN '-' ELSE A.TELEPON END TELEPON,CASE WHEN A.EMAIL IS NULL THEN '-' ELSE A.EMAIL END EMAIL, TO_CHAR(A.CREATED_AT,'DD-MM-YYYY HH24:MI:SS') CREATED_AT, IMG_1 ,IMG_THUMB_1 ,IMG_2 ,IMG_THUMB_2 ,IMG_3 ,IMG_THUMB_3 ,IMG_4 ,IMG_THUMB_4 ,IMG_5 ,IMG_THUMB_5 ,IMG_6 ,IMG_THUMB_6 ,IMG_7 ,IMG_THUMB_7 ,IMG_8 ,IMG_THUMB_8 ,IMG_9 ,IMG_THUMB_9 ,IMG_10 ,IMG_THUMB_10 ,IMG_11 ,IMG_THUMB_11 ,IMG_12 ,IMG_THUMB_12 ,IMG_13 ,IMG_THUMB_13 ,IMG_14 ,IMG_THUMB_14 ,IMG_15 ,IMG_THUMB_15,B.MESSAGE,TO_CHAR(B.CREATED_AT,'DD-MM-YYYY HH24:MI:SS') TGL_NOTIF,NO_SKPWNI
         FROM PENGAJUANDATANG@DBEPUNTEN A LEFT JOIN (SELECT ID,NIK,TO_USER_ID,FROM_USER_ID,MESSAGE,CREATED_AT,PENGAJUAN_ID FROM (SELECT ID,NIK,TO_USER_ID,FROM_USER_ID,MESSAGE,CREATED_AT,PENGAJUAN_ID, RANK() OVER (PARTITION BY PENGAJUAN_ID ORDER BY  CREATED_AT DESC, ID DESC) RNK FROM NOTIFICATION) WHERE RNK = 1) B ON A.ID =B.PENGAJUAN_ID
         WHERE A.PROC_STAT = 2 AND A.STAT_BERKAS IS NOT NULL AND IMG_2 IS NOT NULL
         ";
         if ($no_kec != 0){
            $sql .= " AND PROP = 32 AND KAB = 73 AND KEC = $no_kec";
         }
         $r = DB::select($sql);
        return $r;
    }

    public function ept_get_datang_melengkapi($no_kec)
    {
        header("Content-Type: application/json", true);
         $sql = "SELECT A.ID, A.NIK, A.NAMA_LGKP,CASE WHEN A.TELEPON IS NULL THEN '-' ELSE A.TELEPON END TELEPON,CASE WHEN A.EMAIL IS NULL THEN '-' ELSE A.EMAIL END EMAIL, TO_CHAR(A.CREATED_AT,'DD-MM-YYYY HH24:MI:SS') CREATED_AT, IMG_1 ,IMG_THUMB_1 ,IMG_2 ,IMG_THUMB_2 ,IMG_3 ,IMG_THUMB_3 ,IMG_4 ,IMG_THUMB_4 ,IMG_5 ,IMG_THUMB_5 ,IMG_6 ,IMG_THUMB_6 ,IMG_7 ,IMG_THUMB_7 ,IMG_8 ,IMG_THUMB_8 ,IMG_9 ,IMG_THUMB_9 ,IMG_10 ,IMG_THUMB_10 ,IMG_11 ,IMG_THUMB_11 ,IMG_12 ,IMG_THUMB_12 ,IMG_13 ,IMG_THUMB_13 ,IMG_14 ,IMG_THUMB_14 ,IMG_15 ,IMG_THUMB_15,B.MESSAGE,TO_CHAR(B.CREATED_AT,'DD-MM-YYYY HH24:MI:SS') TGL_NOTIF,NO_SKPWNI,AKTA_LHR,NO_AKTA_LHR
         FROM PENGAJUANDATANG@DBEPUNTEN A LEFT JOIN (SELECT ID,NIK,TO_USER_ID,FROM_USER_ID,MESSAGE,CREATED_AT,PENGAJUAN_ID FROM (SELECT ID,NIK,TO_USER_ID,FROM_USER_ID,MESSAGE,CREATED_AT,PENGAJUAN_ID, RANK() OVER (PARTITION BY PENGAJUAN_ID ORDER BY  CREATED_AT DESC, ID DESC) RNK FROM NOTIFICATION) WHERE RNK = 1) B ON A.ID =B.PENGAJUAN_ID
         WHERE A.PROC_STAT = 2 AND A.STAT_BERKAS IS NULL AND IMG_2 IS NOT NULL
         ";
         if ($no_kec != 0){
            $sql .= " AND PROP = 32 AND KAB = 73 AND KEC = $no_kec";
         }
         $r = DB::select($sql);
        return $r;
    }

    public function ept_get_datang_edit($id)
    {
        header("Content-Type: application/json", true);
         $sql = "SELECT A.ID, A.NIK, A.PROC_STAT, A.STAT_BERKAS, A.VERIFIED_AT, A.VERIFIED_BY, A.CREATED_AT, A.UPDATED_AT, A.DELETED_AT, A.DELETED_BY, A.NO_DATANG, A.NAMA_LGKP, A.TMPT_LHR, TO_CHAR(A.TGL_LHR,'YYYY-MM-DD') TGL_LHR, A.JENIS_KLMIN, A.AGAMA, A.STAT_KWN, A.GOL_DRH, A.PENDIDIKAN, A.PEKERJAAN, A.SRC_PROV, F5_GET_NAMA_PROVINSI(A.SRC_PROV) NAMA_SRC_PROV, A.SRC_KAB, F5_GET_NAMA_KABUPATEN(A.SRC_PROV,A.SRC_KAB) NAMA_SRC_KAB, A.SRC_KEC, F5_GET_NAMA_KECAMATAN(A.SRC_PROV,A.SRC_KAB,A.SRC_KEC) NAMA_SRC_KEC, A.SRC_KEL, F5_GET_NAMA_KELURAHAN(A.SRC_PROV,A.SRC_KAB,A.SRC_KEC,A.SRC_KEL) NAMA_SRC_KEL,  A.SRC_ALAMAT, A.SRC_RT, A.SRC_RW, A.PROP,F5_GET_NAMA_PROVINSI(A.PROP) NAMA_PROP, A.KAB,F5_GET_NAMA_KABUPATEN(A.PROP,A.KAB) NAMA_KAB, A.KEC,F5_GET_NAMA_KECAMATAN(A.PROP,A.KAB,A.KEC) NAMA_KEC, A.KEL, F5_GET_NAMA_KELURAHAN(A.PROP,A.KAB,A.KEC,A.KEL) NAMA_KEL, A.ALAMAT, A.NO_RT, A.NO_RW, A.STATUS, A.TGL_BERLAKU, A.JENIS_LAYANAN, A.NO_KK, A.STAT_HBKEL, A.ALASAN_PINDAH, A.JANGKA_WAKTU, A.JUM_ANGGOTA, A.TGL_DATANG, CASE WHEN A.TELEPON IS NULL THEN '-' ELSE A.TELEPON END TELEPON,CASE WHEN A.EMAIL IS NULL THEN '-' ELSE A.EMAIL END EMAIL, TO_CHAR(A.CREATED_AT,'DD-MM-YYYY HH24:MI:SS') CREATED_AT, IMG_1 ,IMG_THUMB_1 ,IMG_2 ,IMG_THUMB_2 ,IMG_3 ,IMG_THUMB_3 ,IMG_4 ,IMG_THUMB_4,IMG_5 ,IMG_THUMB_5,IMG_6 ,IMG_THUMB_6,IMG_6 ,IMG_THUMB_6,IMG_7 ,IMG_THUMB_7, A.STAT_HBKEL, A.ALASAN_PINDAH, A.JUM_ANGGOTA, A.JANGKA_WAKTU, TO_CHAR(A.TGL_DATANG,'YYYY-MM-DD') TGL_DATANG, A.NO_SKPWNI, A.AKTA_LHR, A.NO_AKTA_LHR
         FROM PENGAJUANDATANG A
         WHERE 1=1 AND A.ID = $id 
         ";
         $r = DB::connection('webpunten')->select($sql);
        return $r;
    }

    
    public function ept_get_datang_delete($id){
            $sql = "DELETE FROM PENGAJUANDATANG WHERE NO_KK IN (SELECT NO_KK FROM PENGAJUANDATANG WHERE ID = $id)";
            $r = DB::connection('webpunten')->delete($sql);
            return $r;
    }

    public function ept_get_datang_tolak($id){
            $nik = Session::get('S_NIK');
            $sql = "UPDATE PENGAJUANDATANG SET STAT_BERKAS = 1, PROC_STAT = 2, UPDATED_AT = SYSDATE, VERIFIED_AT = SYSDATE, VERIFIED_BY = '$nik' WHERE NO_KK IN (SELECT NO_KK FROM PENGAJUANDATANG WHERE ID = $id)";
            $r = DB::connection('webpunten')->update($sql);
               return $r;
    }
    public function ept_get_datang_acc($id){
            $nik = Session::get('S_NIK');
            $sql = "UPDATE PENGAJUANDATANG SET STAT_BERKAS = NULL, PROC_STAT = 3, UPDATED_AT = SYSDATE, VERIFIED_AT = SYSDATE, VERIFIED_BY = '$nik' WHERE NO_KK IN (SELECT NO_KK FROM PENGAJUANDATANG WHERE ID = $id)";
            $r = DB::connection('webpunten')->update($sql);
               return $r;
    }
    public function ept_get_datang_beres($id){
            $nik = Session::get('S_NIK');
            $sql = "UPDATE PENGAJUANDATANG SET STAT_BERKAS = NULL, PROC_STAT = 4, UPDATED_AT = SYSDATE, VERIFIED_AT = SYSDATE, VERIFIED_BY = '$nik' WHERE NO_KK IN (SELECT NO_KK FROM PENGAJUANDATANG WHERE ID = $id)";
            $r = DB::connection('webpunten')->update($sql);
               return $r;
    }

    public function ept_get_wna_baru()
    {
        header("Content-Type: application/json", true);
         $sql = "SELECT A.ID, A.NIK, A.NAMA_LGKP ||' '|| A.NAMA_FAM NAMA_LGKP,CASE WHEN A.TELEPON IS NULL THEN '-' ELSE A.TELEPON END TELEPON,CASE WHEN A.EMAIL IS NULL THEN '-' ELSE A.EMAIL END EMAIL, TO_CHAR(A.CREATED_AT,'DD-MM-YYYY HH24:MI:SS') CREATED_AT, IMG_1 ,IMG_THUMB_1 ,IMG_2 ,IMG_THUMB_2 ,IMG_3 ,IMG_THUMB_3 ,IMG_4 ,IMG_THUMB_4 ,IMG_5 ,IMG_THUMB_5 ,IMG_6 ,IMG_THUMB_6 ,IMG_7 ,IMG_THUMB_7 ,IMG_8 ,IMG_THUMB_8 ,IMG_9 ,IMG_THUMB_9 ,IMG_10 ,IMG_THUMB_10 ,IMG_11 ,IMG_THUMB_11 ,IMG_12 ,IMG_THUMB_12 ,IMG_13 ,IMG_THUMB_13 ,IMG_14 ,IMG_THUMB_14 ,IMG_15 ,IMG_THUMB_15  , UPPER(F5_GET_REF_WNA(DOK_IMGR, 501)) DOK_IMGR_DESC ,DOK_IMGR
         FROM PENGAJUANWNA A
         WHERE A.PROC_STAT =1  AND A.IMG_1 IS NOT NULL AND IMG_2 IS NOT NULL
         ";
         $r = DB::connection('webpunten')->select($sql);
        return $r;
    }

    public function ept_get_wna_acc_dokumen()
    {
        header("Content-Type: application/json", true);
         $sql = "SELECT A.ID, A.NIK, A.NAMA_LGKP ||' '|| A.NAMA_FAM NAMA_LGKP,CASE WHEN A.TELEPON IS NULL THEN '-' ELSE A.TELEPON END TELEPON,CASE WHEN A.EMAIL IS NULL THEN '-' ELSE A.EMAIL END EMAIL, TO_CHAR(A.CREATED_AT,'DD-MM-YYYY HH24:MI:SS') CREATED_AT, IMG_1 ,IMG_THUMB_1 ,IMG_2 ,IMG_THUMB_2 ,IMG_3 ,IMG_THUMB_3 ,IMG_4 ,IMG_THUMB_4 ,IMG_5 ,IMG_THUMB_5 ,IMG_6 ,IMG_THUMB_6 ,IMG_7 ,IMG_THUMB_7 ,IMG_8 ,IMG_THUMB_8 ,IMG_9 ,IMG_THUMB_9 ,IMG_10 ,IMG_THUMB_10 ,IMG_11 ,IMG_THUMB_11 ,IMG_12 ,IMG_THUMB_12 ,IMG_13 ,IMG_THUMB_13 ,IMG_14 ,IMG_THUMB_14 ,IMG_15 ,IMG_THUMB_15  , UPPER(F5_GET_REF_WNA(DOK_IMGR, 501)) DOK_IMGR_DESC ,DOK_IMGR
         FROM PENGAJUANWNA A
         WHERE A.PROC_STAT =3 AND A.IMG_1 IS NOT NULL AND IMG_2 IS NOT NULL
         ";
         $r = DB::connection('webpunten')->select($sql);
        return $r;
    }

    public function ept_get_wna_arsip()
    {
        header("Content-Type: application/json", true);
         $sql = "SELECT A.ID, A.NIK, A.NAMA_LGKP ||' '|| A.NAMA_FAM NAMA_LGKP,CASE WHEN A.TELEPON IS NULL THEN '-' ELSE A.TELEPON END TELEPON,CASE WHEN A.EMAIL IS NULL THEN '-' ELSE A.EMAIL END EMAIL, TO_CHAR(A.CREATED_AT,'DD-MM-YYYY HH24:MI:SS') CREATED_AT, IMG_1 ,IMG_THUMB_1 ,IMG_2 ,IMG_THUMB_2 ,IMG_3 ,IMG_THUMB_3 ,IMG_4 ,IMG_THUMB_4 ,IMG_5 ,IMG_THUMB_5 ,IMG_6 ,IMG_THUMB_6 ,IMG_7 ,IMG_THUMB_7 ,IMG_8 ,IMG_THUMB_8 ,IMG_9 ,IMG_THUMB_9 ,IMG_10 ,IMG_THUMB_10 ,IMG_11 ,IMG_THUMB_11 ,IMG_12 ,IMG_THUMB_12 ,IMG_13 ,IMG_THUMB_13 ,IMG_14 ,IMG_THUMB_14 ,IMG_15 ,IMG_THUMB_15  , UPPER(F5_GET_REF_WNA(DOK_IMGR, 501)) DOK_IMGR_DESC ,DOK_IMGR
         FROM PENGAJUANWNA A
         WHERE A.PROC_STAT >3 AND A.IMG_1 IS NOT NULL AND IMG_2 IS NOT NULL
         ";
         $r = DB::connection('webpunten')->select($sql);
        return $r;
    }

    public function ept_get_wna_batal()
    {
        header("Content-Type: application/json", true);
         $sql = "SELECT A.ID, A.NIK, A.NAMA_LGKP ||' '|| A.NAMA_FAM NAMA_LGKP,CASE WHEN A.TELEPON IS NULL THEN '-' ELSE A.TELEPON END TELEPON,CASE WHEN A.EMAIL IS NULL THEN '-' ELSE A.EMAIL END EMAIL, TO_CHAR(A.CREATED_AT,'DD-MM-YYYY HH24:MI:SS') CREATED_AT, IMG_1 ,IMG_THUMB_1 ,IMG_2 ,IMG_THUMB_2 ,IMG_3 ,IMG_THUMB_3 ,IMG_4 ,IMG_THUMB_4 ,IMG_5 ,IMG_THUMB_5 ,IMG_6 ,IMG_THUMB_6 ,IMG_7 ,IMG_THUMB_7 ,IMG_8 ,IMG_THUMB_8 ,IMG_9 ,IMG_THUMB_9 ,IMG_10 ,IMG_THUMB_10 ,IMG_11 ,IMG_THUMB_11 ,IMG_12 ,IMG_THUMB_12 ,IMG_13 ,IMG_THUMB_13 ,IMG_14 ,IMG_THUMB_14 ,IMG_15 ,IMG_THUMB_15,B.MESSAGE,TO_CHAR(B.CREATED_AT,'DD-MM-YYYY HH24:MI:SS') TGL_NOTIF , UPPER(F5_GET_REF_WNA(DOK_IMGR, 501)) DOK_IMGR_DESC ,DOK_IMGR
         FROM PENGAJUANWNA@DBEPUNTEN A INNER JOIN (SELECT ID,NIK,TO_USER_ID,FROM_USER_ID,MESSAGE,CREATED_AT,PENGAJUAN_ID FROM (SELECT ID,NIK,TO_USER_ID,FROM_USER_ID,MESSAGE,CREATED_AT,PENGAJUAN_ID, RANK() OVER (PARTITION BY PENGAJUAN_ID ORDER BY  CREATED_AT DESC, ID DESC) RNK FROM NOTIFICATION) WHERE RNK = 1) B ON A.ID =B.PENGAJUAN_ID
         WHERE A.PROC_STAT = 2 AND A.STAT_BERKAS IS NOT NULL AND A.IMG_1 IS NOT NULL AND IMG_2 IS NOT NULL
         ";
         $r = DB::select($sql);
        return $r;
    }

    public function ept_get_wna_melengkapi()
    {
        header("Content-Type: application/json", true);
         $sql = "SELECT A.ID, A.NIK, A.NAMA_LGKP ||' '|| A.NAMA_FAM NAMA_LGKP,CASE WHEN A.TELEPON IS NULL THEN '-' ELSE A.TELEPON END TELEPON,CASE WHEN A.EMAIL IS NULL THEN '-' ELSE A.EMAIL END EMAIL, TO_CHAR(A.CREATED_AT,'DD-MM-YYYY HH24:MI:SS') CREATED_AT, IMG_1 ,IMG_THUMB_1 ,IMG_2 ,IMG_THUMB_2 ,IMG_3 ,IMG_THUMB_3 ,IMG_4 ,IMG_THUMB_4 ,IMG_5 ,IMG_THUMB_5 ,IMG_6 ,IMG_THUMB_6 ,IMG_7 ,IMG_THUMB_7 ,IMG_8 ,IMG_THUMB_8 ,IMG_9 ,IMG_THUMB_9 ,IMG_10 ,IMG_THUMB_10 ,IMG_11 ,IMG_THUMB_11 ,IMG_12 ,IMG_THUMB_12 ,IMG_13 ,IMG_THUMB_13 ,IMG_14 ,IMG_THUMB_14 ,IMG_15 ,IMG_THUMB_15,B.MESSAGE,TO_CHAR(B.CREATED_AT,'DD-MM-YYYY HH24:MI:SS') TGL_NOTIF , UPPER(F5_GET_REF_WNA(DOK_IMGR, 501)) DOK_IMGR_DESC ,DOK_IMGR
         FROM PENGAJUANWNA@DBEPUNTEN A INNER JOIN (SELECT ID,NIK,TO_USER_ID,FROM_USER_ID,MESSAGE,CREATED_AT,PENGAJUAN_ID FROM (SELECT ID,NIK,TO_USER_ID,FROM_USER_ID,MESSAGE,CREATED_AT,PENGAJUAN_ID, RANK() OVER (PARTITION BY PENGAJUAN_ID ORDER BY  CREATED_AT DESC, ID DESC) RNK FROM NOTIFICATION) WHERE RNK = 1) B ON A.ID =B.PENGAJUAN_ID
         WHERE A.PROC_STAT = 2 AND A.STAT_BERKAS IS NULL AND A.IMG_1 IS NOT NULL AND IMG_2 IS NOT NULL
         ";
         $r = DB::select($sql);
        return $r;
    }

    public function ept_get_wna_edit($id)
    {
        header("Content-Type: application/json", true);
         $sql = "SELECT ID ,NIK
              ,IMG_1 ,IMG_THUMB_1 ,IMG_2 ,IMG_THUMB_2 ,IMG_3 ,IMG_THUMB_3 ,IMG_4 ,IMG_THUMB_4 ,IMG_5 ,IMG_THUMB_5
              ,IMG_6 ,IMG_THUMB_6 ,IMG_7 ,IMG_THUMB_7 ,IMG_8 ,IMG_THUMB_8 ,IMG_9 ,IMG_THUMB_9 ,IMG_10 ,IMG_THUMB_10
              ,IMG_11 ,IMG_THUMB_11 ,IMG_12 ,IMG_THUMB_12 ,IMG_13 ,IMG_THUMB_13 ,IMG_14 ,IMG_THUMB_14 ,IMG_15 ,IMG_THUMB_15
              ,PROC_STAT ,STAT_BERKAS ,VERIFIED_AT ,VERIFIED_BY ,CREATED_AT ,UPDATED_AT ,DELETED_AT ,DELETED_BY ,NO_WNA,NAMA_LGKP ,TMPT_LHR
              ,TO_CHAR(TGL_LHR ,'DD-MM-YYYY') TGL_LHR
              ,JENIS_KLMIN ,AGAMA ,STAT_KWN ,GOL_DRH ,PENDIDIKAN ,PEKERJAAN
              , SRC_PROV ,SRC_KAB ,SRC_KEC ,SRC_KEL
              , F5_GET_NAMA_PROVINSI(SRC_PROV) SRC_NAMA_PROV
              , F5_GET_NAMA_KABUPATEN(SRC_PROV,SRC_KAB) SRC_NAMA_KAB
              , F5_GET_NAMA_KECAMATAN(SRC_PROV,SRC_KAB ,SRC_KEC) SRC_NAMA_KEC
              , F5_GET_NAMA_KELURAHAN(SRC_PROV,SRC_KAB ,SRC_KEC ,SRC_KEL ) SRC_NAMA_KEL
              , SRC_ALAMAT ,SRC_RT ,SRC_RW ,KEC ,KEL
              , F5_GET_NAMA_KECAMATAN(32,73,KEC) NAMA_KEC
              , F5_GET_NAMA_KELURAHAN(32,73,KEC ,KEL ) NAMA_KEL
              , UPPER(F5_GET_REF_WNA(F5_TO_NUMBER(JENIS_KLMIN,7), 801)) JENIS_KLMIN_DESC
              , UPPER(F5_GET_REF_WNA(F5_TO_NUMBER(AGAMA,7), 901)) AGAMA_DESC
              , UPPER(F5_GET_REF_WNA(PEKERJAAN, 201)) PEKERJAAN_DESC
              , UPPER(F5_GET_REF_WNA(PENDIDIKAN, 101)) PENDIDIKAN_DESC
              , UPPER(F5_GET_REF_WNA(STAT_KWN, 701)) STAT_KWN_DESC
              , UPPER(F5_GET_REF_WNA(GOL_DRH, 401)) GOL_DRH_DESC
              , UPPER(F5_GET_REF_NEGARA(KWRNGRN)) KWRNGRN_DESC
              , KWRNGRN KWRNGRN
              ,ALAMAT ,NO_RT ,NO_RW ,STATUS,TELEPON,EMAIL
              , STAT_HBKEL, UPPER(F5_GET_REF_WNA(STAT_HBKEL, 301)) STAT_HBKEL_DESC, JANGKA_WAKTU, JUM_ANGGOTA, TO_CHAR(TGL_DATANG,'DD-MM-YYYY') TGL_DATANG, ALASAN_PINDAH, UPPER(F5_GET_REF_WNA(ALASAN_PINDAH, 601)) ALASAN_PINDAH_DESC
              ,NO_PASPOR
              ,TO_CHAR(TGL_PASPOR,'DD-MM-YYYY') TGL_PASPOR
              ,TO_CHAR(TGL_AKH_PASPOR,'DD-MM-YYYY') TGL_AKH_PASPOR
              ,NAMA_SPSOR
              ,TIPE_SPSOR
              ,DOK_IMGR
              , UPPER(F5_GET_REF_WNA(DOK_IMGR, 501)) DOK_IMGR_DESC
              ,NO_DOK
              ,TMPT_DTBIT
              ,TO_CHAR(TGL_DTBIT,'DD-MM-YYYY') TGL_DTBIT
              ,TO_CHAR(TGL_AKH_DOK,'DD-MM-YYYY') TGL_AKH_DOK
              ,TO_CHAR(TGL_STAY,'DD-MM-YYYY') TGL_STAY
              ,TO_CHAR(TGL_PICKUP,'DD-MM-YYYY') TGL_PICKUP
              ,TO_CHAR(TGL_DOC_IMIGRASI,'DD-MM-YYYY') TGL_DOC_IMIGRASI
              ,TO_CHAR(TGL_BERLAKU,'DD-MM-YYYY') TGL_BERLAKU  ,JENIS_LAYANAN,
              'WNA' JENIS_LAYANAN_DESC
              FROM PENGAJUANWNA WHERE 1=1 AND ID = $id 
         ";
         $r = DB::connection('webpunten')->select($sql);
        return $r;
    }

    
    public function ept_get_wna_delete($id){
            $sql = "DELETE FROM PENGAJUANWNA WHERE NO_KK IN (SELECT NO_KK FROM PENGAJUANDATANG WHERE ID = $id)";
            $r = DB::connection('webpunten')->delete($sql);
            return $r;
    }

    public function ept_get_wna_tolak($id){
            $nik = Session::get('S_NIK');
            $sql = "UPDATE PENGAJUANWNA SET STAT_BERKAS = 1, PROC_STAT = 2, UPDATED_AT = SYSDATE, VERIFIED_AT = SYSDATE, VERIFIED_BY = '$nik' WHERE NO_KK IN (SELECT NO_KK FROM PENGAJUANWNA WHERE ID = $id)";
            $r = DB::connection('webpunten')->update($sql);
               return $r;
    }
    public function ept_get_wna_acc($id){
            $nik = Session::get('S_NIK');
            $sql = "UPDATE PENGAJUANWNA SET STAT_BERKAS = NULL, PROC_STAT = 3, UPDATED_AT = SYSDATE, VERIFIED_AT = SYSDATE, VERIFIED_BY = '$nik' WHERE NO_KK IN (SELECT NO_KK FROM PENGAJUANWNA WHERE ID = $id)";
            $r = DB::connection('webpunten')->update($sql);
               return $r;
    }
    public function ept_get_wna_beres($id){
            $nik = Session::get('S_NIK');
            $sql = "UPDATE PENGAJUANWNA SET STAT_BERKAS = NULL, PROC_STAT = 4, UPDATED_AT = SYSDATE, VERIFIED_AT = SYSDATE, VERIFIED_BY = '$nik' WHERE NO_KK IN (SELECT NO_KK FROM PENGAJUANWNA WHERE ID = $id)";
            $r = DB::connection('webpunten')->update($sql);
               return $r;
    }
    

   

    public function ept_get_notif_delete($nik){
            $sql = "DELETE FROM NOTIFICATION WHERE NIK = $nik";
            $r = DB::delete($sql);
            return $r;
    }


    public function get_kontak(Request $request)
    {
        $nik = Session::get('S_NIK');
        $kontak = '';
        if($nik != null);{
        $get_kontak = $this->ept_get_kontak($nik);
            foreach ($get_kontak as $r)
            {
                $is_sent = ($r->type_msg == 'Sent') ? '<i class="mdi mdi-check-all"></i> ' : null;
                $jml = ($r->jml > 0) ? '<span class="label label-rouded label-success pull-right notiv_mini">'.$r->jml.'</span>' : null;
                $kontak .= '<li>
                                    <a href="javascript:void(0)" onclick="chat_user('.$r->nik.',\''.$r->nama_lgkp.'\',\''.$r->is_color.'\',\''.$r->is_active.'\');">
                                    <img src="'.url('/').'/assets/upload/pp/'.$r->nik.'.jpg" alt="user-img" onerror="this.onerror=null;this.src=\''.url('/').'//assets/plugins/images/calming-cat.gif\';" class="img-circle" style="width: 60px;"> 
                                    <b style="color: black!important">'.$r->nama_lgkp.'</b><span class="pull-right" style="color: rgba(0,0,0,0.45)!important">'.$r->created_at.'</span><br> <small class="'.$r->is_color.'">'.$r->is_active.'</small><br>
                                    <small class="text-muted">'.$is_sent.'</small><b style="color: black!important">'.$r->text.'</b>'.$jml.'</a>
                                </li>
                        ';
            }
        }
        return $kontak;
        exit();
    }


    public function get_kontak_warga(Request $request)
    {
        $nik = Session::get('S_NIK');
        $kontak = '';
        if($nik != null);{
        $get_kontak = $this->ept_get_kontak_warga($nik);
            foreach ($get_kontak as $r)
            {
                $is_sent = ($r->type_msg == 'Sent') ? '<i class="mdi mdi-check-all"></i> ' : null;
                $jml = ($r->jml > 0) ? '<span class="label label-rouded label-success pull-right notiv_mini">'.$r->jml.'</span>' : null;
                $kontak .= '<li>
                                    <a href="javascript:void(0)" onclick="chat_user_warga('.$r->nik.',\''.$r->nama_lgkp.'\',\''.$r->is_color.'\',\''.$r->is_active.'\',\''.$r->pic.'\');">';
                if($r->pic != null){
                    $kontak .= '<img src="'.url('/').'/'.$r->pic.'" alt="user-img" onerror="this.onerror=null;this.src=\''.url('/').'//assets/plugins/images/calming-cat.gif\';" class="img-circle" style="width: 60px;">';
                }else{
                    $kontak .= '<img src="'.url('/').'/assets/plugins/images/calming-cat.gif" alt="user-img" onerror="this.onerror=null;this.src=\''.url('/').'//assets/plugins/images/calming-cat.gif\';" class="img-circle" style="width: 60px;">';
                }
                
                $kontak .= '<b style="color: black!important">'.$r->nama_lgkp.'</b><span class="pull-right" style="color: rgba(0,0,0,0.45)!important">'.$r->created_at.'</span><br> <small class="'.$r->is_color.'">'.$r->is_active.'</small><br>
                                    <small class="text-muted">'.$is_sent.'</small><b style="color: black!important">'.$r->text.'</b>'.$jml.'</a>
                                </li>
                        ';
            }
        }
        return $kontak;
        exit();
    }


    public function get_kontak_byname(Request $request)
    {
        $nik = Session::get('S_NIK');
        $kontak = '';
        $nama = strtoupper($request->nama);
        if($nik != null);
        {
        $get_kontak = $this->ept_get_kontak_byname($nik,$nama);
            foreach ($get_kontak as $r)
            {
                $is_sent = ($r->type_msg == 'Sent') ? '<i class="mdi mdi-check-all"></i> ' : null;
                $jml = ($r->jml > 0) ? '<span class="label label-rouded label-success pull-right notiv_mini">'.$r->jml.'</span>' : null;
                $kontak .= '<li>
                                    <a href="javascript:void(0)" onclick="chat_user('.$r->nik.',\''.$r->nama_lgkp.'\',\''.$r->is_color.'\',\''.$r->is_active.'\');">
                                    <img src="'.url('/').'/assets/upload/pp/'.$r->nik.'.jpg" alt="user-img" onerror="this.onerror=null;this.src=\''.url('/').'//assets/plugins/images/calming-cat.gif\';" class="img-circle" style="width: 60px;"> 
                                    <b style="color: black!important">'.$r->nama_lgkp.'</b><span class="pull-right" style="color: rgba(0,0,0,0.45)!important">'.$r->created_at.'</span><br> <small class="'.$r->is_color.'">'.$r->is_active.'</small><br>
                                    <small class="text-muted">'.$is_sent.'</small><b style="color: black!important">'.$r->text.'</b>'.$jml.'</a>
                                </li>
                        ';
            }
        }

        return $kontak;
        exit();
    }


    public function get_kontak_warga_byname(Request $request)
    {
        $nik = Session::get('S_NIK');
        $kontak = '';
        $nama = strtoupper($request->nama);
        if($nik != null);{
        $get_kontak = $this->ept_get_kontak_warga_byname($nik,$nama);
            foreach ($get_kontak as $r)
            {
                $is_sent = ($r->type_msg == 'Sent') ? '<i class="mdi mdi-check-all"></i> ' : null;
                $jml = ($r->jml > 0) ? '<span class="label label-rouded label-success pull-right notiv_mini">'.$r->jml.'</span>' : null;
                $kontak .= '<li>
                                    <a href="javascript:void(0)" onclick="chat_user_warga('.$r->nik.',\''.$r->nama_lgkp.'\',\''.$r->is_color.'\',\''.$r->is_active.'\',\''.$r->pic.'\');">';
                if($r->pic != null){
                    $kontak .= '<img src="'.url('/').'/'.$r->pic.'" alt="user-img" onerror="this.onerror=null;this.src=\''.url('/').'//assets/plugins/images/calming-cat.gif\';" class="img-circle" style="width: 60px;">';
                }else{
                    $kontak .= '<img src="'.url('/').'/assets/plugins/images/calming-cat.gif" alt="user-img" onerror="this.onerror=null;this.src=\''.url('/').'//assets/plugins/images/calming-cat.gif\';" class="img-circle" style="width: 60px;">';
                }
                
                $kontak .= '<b style="color: black!important">'.$r->nama_lgkp.'</b><span class="pull-right" style="color: rgba(0,0,0,0.45)!important">'.$r->created_at.'</span><br> <small class="'.$r->is_color.'">'.$r->is_active.'</small><br>
                                    <small class="text-muted">'.$is_sent.'</small><b style="color: black!important">'.$r->text.'</b>'.$jml.'</a>
                                </li>
                        ';
            }
        }
        return $kontak;
        exit();
    }


    public function get_kontak_notif(Request $request)
    {
        $nik = Session::get('S_NIK');
        $kontak = '';
        $nama = strtoupper($request->nama);
        if($nik != null);{
        $count_notif = $this->ept_get_message_count($nik);
            if($count_notif > 0){
                $get_kontak = $this->ept_get_kontak_withchat($nik);
                foreach ($get_kontak as $r)
                {
                    $kontak .= '<a href="'.url('/').'/Chat">
                                        <div class="user-img"> <img src="'.url('/').'/assets/upload/pp/'.$r->nik.'.jpg" alt="user-img" alt="user" class="img-circle" onerror="this.onerror=null;this.src=\''.url('/').'//assets/plugins/images/calming-cat.gif\';"> <span class="profile-status online pull-right"></span> </div>
                                        <div class="mail-contnet">
                                            <h5>'.$r->nama_lgkp.'</h5> <span class="mail-desc">'.$r->text.'</span> <span class="time">'.$r->created_at.'</span> </div>
                                </a>
                            ';
                }
            }
        }
        $output = array(
                    "message_type"=>$count_notif,
                    "data"=>$kontak
            );
        return $output; 
        exit();
    }


    public function get_pesan(Request $request)
    {
        $mynik = Session::get('S_NIK');
        $tonik = $request->nik;
        $pesan = '';
        $this->ept_read_pesan($mynik,$tonik);
        if($tonik != null);{
        $get_pesan = $this->ept_get_pesan($mynik,$tonik);
            foreach ($get_pesan as $r)
            {

                if($r->text == 'PIC'){
                    $pesan .= '<li class="'.$r->source.'">
                                    <div class="chat-body">
                                        <div class="chat-text">
                                            <p><div class="user-img"> <img src="'.url('/').'/'.$r->img.'" alt="user-img" alt="user" class="img" onerror="this.onerror=null;this.src=\''.url('/').'//assets/plugins/images/calming-cat.gif\';"> <span class="profile-status online pull-right"></span> </div></p> <b>'.$r->jam.'</b> </div>
                                    </div>
                                </li>
                        ';
                }else{
                    $pesan .= '<li class="'.$r->source.'">
                                    <div class="chat-body">
                                        <div class="chat-text">
                                            <p>'.$r->text.'</p> <b>'.$r->jam.'</b> </div>
                                    </div>
                                </li>
                        ';
                }
                
            }
        }
        return $pesan;
        exit();
    }


    public function ept_get_message_count($nik)
    {

        $sql ="SELECT COUNT(DISTINCT(FROM_ID)) JML FROM MESSAGES WHERE TO_ID = $nik AND READ = 0";
        $r = DB::select($sql);
        return $r[0]->jml;

    }

    public function ept_get_kontak($nik)
    {

        $sql ="SELECT A.USER_ID, INITCAP(A.NAMA_LGKP) NAMA_LGKP, A.NIK
            , CASE WHEN B.IS_ACTIVE IS NOT NULL AND TRUNC(B.LAST_ACTIVITY) = TRUNC(SYSDATE) THEN 'Online' ELSE 'Offline' END IS_ACTIVE
            , CASE WHEN B.IS_ACTIVE IS NOT NULL AND TRUNC(B.LAST_ACTIVITY) = TRUNC(SYSDATE) THEN 'text-success' ELSE 'text-muted' END IS_COLOR
            , TYPE_MSG
            , CASE WHEN C.TEXT IS NULL THEN NULL
               WHEN LENGTH(C.TEXT) >= 20 THEN SUBSTR(C.TEXT,0,20) || ' ...'
                 ELSE C.TEXT END TEXT
            , CASE WHEN C.READ IS NULL THEN '0' ELSE C.READ END READ
            , CASE WHEN C.JML IS NULL THEN 0 ELSE C.JML END JML
            , CASE 
                WHEN C.CREATED_AT IS NULL THEN NULL 
                WHEN TRUNC(C.CREATED_AT) = TRUNC(SYSDATE) THEN  TO_CHAR(C.CREATED_AT,'HH24:MI') 
                WHEN TRUNC(C.CREATED_AT) = TRUNC(SYSDATE-1) THEN  'Yesterday' 
                WHEN TRUNC(C.CREATED_AT) < TRUNC(SYSDATE-1) AND TRUNC(C.CREATED_AT) > TRUNC(SYSDATE-7)  THEN  TO_CHAR(C.CREATED_AT,'Day') 
                ELSE TO_CHAR(C.CREATED_AT,'DD/MM/YYYY') 
              END CREATED_AT
            FROM SIAK_USER_PLUS A 
            LEFT JOIN SIAK_SESSION_PLUS B ON A.USER_ID = B.USER_ID 
            LEFT JOIN 
                (SELECT ID, TYPE_MSG, SOURCE_NIK, SOURCE_MSG, DELETED_FROM, DELETED_TO, TEXT, READ, CREATED_AT, UPDATED_AT, OPR_READ, (SELECT COUNT(1) FROM MESSAGES X WHERE X.READ = 0 AND X.FROM_ID = A.SOURCE_NIK AND X.TO_ID = $nik ) JML FROM(  
                SELECT  ID, TYPE_MSG, SOURCE_NIK, SOURCE_MSG, DELETED_FROM, DELETED_TO, TEXT, READ, CREATED_AT, UPDATED_AT, OPR_READ, RANK() OVER (PARTITION BY SOURCE_MSG ORDER BY CREATED_AT DESC,UPDATED_AT DESC, ID DESC) RNK FROM 
                (SELECT ID, TYPE_MSG, SOURCE_NIK, SOURCE_MSG, DELETED_FROM, DELETED_TO, TEXT, READ, CREATED_AT, UPDATED_AT, OPR_READ, RNK FROM 
                (SELECT ID, 'Receive' TYPE_MSG, FROM_ID SOURCE_NIK, FROM_ID||TO_ID SOURCE_MSG, DELETED_FROM, DELETED_TO, TEXT, READ, CREATED_AT, UPDATED_AT, OPR_READ, RANK() OVER (PARTITION BY FROM_ID ORDER BY CREATED_AT DESC,UPDATED_AT DESC, ID DESC) RNK FROM MESSAGES WHERE TO_ID = $nik) 
                UNION ALL
                (SELECT ID, 'Sent' TYPE_MSG, TO_ID SOURCE_NIK,TO_ID||FROM_ID SOURCE_MSG, DELETED_FROM, DELETED_TO, TEXT, READ, CREATED_AT, UPDATED_AT, OPR_READ, RANK() OVER (PARTITION BY TO_ID ORDER BY CREATED_AT DESC,UPDATED_AT DESC, ID DESC) RNK FROM MESSAGES WHERE FROM_ID = $nik))) A WHERE RNK =1) C ON A.NIK = C.SOURCE_NIK
            WHERE A.NIK <> $nik
            ORDER BY C.CREATED_AT DESC NULLS LAST, NAMA_LGKP";
            $r = DB::select($sql);
            return $r;

    }


    public function ept_get_kontak_withchat($nik)
    {

        $sql ="SELECT A.USER_ID, INITCAP(A.NAMA_LGKP) NAMA_LGKP, A.NIK
            , CASE WHEN B.IS_ACTIVE IS NOT NULL AND TRUNC(B.LAST_ACTIVITY) = TRUNC(SYSDATE) THEN 'Online' ELSE 'Offline' END IS_ACTIVE
            , CASE WHEN B.IS_ACTIVE IS NOT NULL AND TRUNC(B.LAST_ACTIVITY) = TRUNC(SYSDATE) THEN 'text-success' ELSE 'text-muted' END IS_COLOR
            , TYPE_MSG
            , CASE WHEN C.TEXT IS NULL THEN NULL
               WHEN LENGTH(C.TEXT) >= 20 THEN SUBSTR(C.TEXT,0,20) || ' ...'
                 ELSE C.TEXT END TEXT
            , CASE WHEN C.READ IS NULL THEN '0' ELSE C.READ END READ
            , CASE WHEN C.JML IS NULL THEN 0 ELSE C.JML END JML
            , CASE 
                WHEN C.CREATED_AT IS NULL THEN NULL 
                WHEN TRUNC(C.CREATED_AT) = TRUNC(SYSDATE) THEN  TO_CHAR(C.CREATED_AT,'HH24:MI') 
                WHEN TRUNC(C.CREATED_AT) = TRUNC(SYSDATE-1) THEN  'Yesterday' 
                WHEN TRUNC(C.CREATED_AT) < TRUNC(SYSDATE-1) AND TRUNC(C.CREATED_AT) > TRUNC(SYSDATE-7)  THEN  TO_CHAR(C.CREATED_AT,'Day') 
                ELSE TO_CHAR(C.CREATED_AT,'DD/MM/YYYY') 
              END CREATED_AT
            FROM SIAK_USER_PLUS A 
            INNER JOIN SIAK_SESSION_PLUS B ON A.USER_ID = B.USER_ID 
            INNER JOIN 
                (SELECT ID, TYPE_MSG, SOURCE_NIK, SOURCE_MSG, DELETED_FROM, DELETED_TO, TEXT, READ, CREATED_AT, UPDATED_AT, OPR_READ, (SELECT COUNT(1) FROM MESSAGES X WHERE X.READ = 0 AND X.FROM_ID = A.SOURCE_NIK AND X.TO_ID = $nik ) JML FROM(  
                SELECT  ID, TYPE_MSG, SOURCE_NIK, SOURCE_MSG, DELETED_FROM, DELETED_TO, TEXT, READ, CREATED_AT, UPDATED_AT, OPR_READ, RANK() OVER (PARTITION BY SOURCE_MSG ORDER BY CREATED_AT DESC,UPDATED_AT DESC, ID DESC) RNK FROM 
                (SELECT ID, TYPE_MSG, SOURCE_NIK, SOURCE_MSG, DELETED_FROM, DELETED_TO, TEXT, READ, CREATED_AT, UPDATED_AT, OPR_READ, RNK FROM 
                (SELECT ID, 'Receive' TYPE_MSG, FROM_ID SOURCE_NIK, FROM_ID||TO_ID SOURCE_MSG, DELETED_FROM, DELETED_TO, TEXT, READ, CREATED_AT, UPDATED_AT, OPR_READ, RANK() OVER (PARTITION BY FROM_ID ORDER BY CREATED_AT DESC,UPDATED_AT DESC, ID DESC) RNK FROM MESSAGES WHERE TO_ID = $nik) 
                )) A WHERE RNK =1) C ON A.NIK = C.SOURCE_NIK
            WHERE A.NIK <> $nik AND READ = 0
            ORDER BY C.CREATED_AT DESC NULLS LAST, NAMA_LGKP";
            $r = DB::select($sql);
            return $r;

    }


    public function ept_get_kontak_warga($nik)
    {

        $sql ="SELECT A.ID USER_ID, INITCAP(A.NAME) NAMA_LGKP, A.NIK, A.PIC
            ,  'Online' IS_ACTIVE
            ,  'text-success' IS_COLOR
            , TYPE_MSG
            , CASE WHEN C.TEXT IS NULL THEN NULL
               WHEN LENGTH(C.TEXT) >= 20 THEN SUBSTR(C.TEXT,0,20) || ' ...'
                 ELSE C.TEXT END TEXT
            , CASE WHEN C.READ IS NULL THEN '0' ELSE C.READ END READ
            , CASE WHEN C.JML IS NULL THEN 0 ELSE C.JML END JML
            , CASE 
                WHEN C.CREATED_AT IS NULL THEN NULL 
                WHEN TRUNC(C.CREATED_AT) = TRUNC(SYSDATE) THEN  TO_CHAR(C.CREATED_AT,'HH24:MI') 
                WHEN TRUNC(C.CREATED_AT) = TRUNC(SYSDATE-1) THEN  'Yesterday' 
                WHEN TRUNC(C.CREATED_AT) < TRUNC(SYSDATE-1) AND TRUNC(C.CREATED_AT) > TRUNC(SYSDATE-7)  THEN  TO_CHAR(C.CREATED_AT,'Day') 
                ELSE TO_CHAR(C.CREATED_AT,'DD/MM/YYYY') 
              END CREATED_AT
            FROM USERS A  
            INNER JOIN 
                (SELECT ID, TYPE_MSG, SOURCE_NIK, SOURCE_MSG, DELETED_FROM, DELETED_TO, TEXT, READ, CREATED_AT, UPDATED_AT, OPR_READ, (SELECT COUNT(1) FROM MESSAGES X WHERE X.READ = 0 AND X.FROM_ID = A.SOURCE_NIK AND X.TO_ID = $nik ) JML FROM(  
                SELECT  ID, TYPE_MSG, SOURCE_NIK, SOURCE_MSG, DELETED_FROM, DELETED_TO, TEXT, READ, CREATED_AT, UPDATED_AT, OPR_READ, RANK() OVER (PARTITION BY SOURCE_MSG ORDER BY CREATED_AT DESC,UPDATED_AT DESC, ID DESC) RNK FROM 
                (SELECT ID, TYPE_MSG, SOURCE_NIK, SOURCE_MSG, DELETED_FROM, DELETED_TO, TEXT, READ, CREATED_AT, UPDATED_AT, OPR_READ, RNK FROM 
                (SELECT ID, 'Receive' TYPE_MSG, FROM_ID SOURCE_NIK, FROM_ID||TO_ID SOURCE_MSG, DELETED_FROM, DELETED_TO, TEXT, READ, CREATED_AT, UPDATED_AT, OPR_READ, RANK() OVER (PARTITION BY FROM_ID ORDER BY CREATED_AT DESC,UPDATED_AT DESC, ID DESC) RNK FROM MESSAGES WHERE TO_ID = $nik) 
                UNION ALL
                (SELECT ID, 'Sent' TYPE_MSG, TO_ID SOURCE_NIK,TO_ID||FROM_ID SOURCE_MSG, DELETED_FROM, DELETED_TO, TEXT, READ, CREATED_AT, UPDATED_AT, OPR_READ, RANK() OVER (PARTITION BY TO_ID ORDER BY CREATED_AT DESC,UPDATED_AT DESC, ID DESC) RNK FROM MESSAGES WHERE FROM_ID = $nik))) A WHERE RNK =1) C ON A.NIK = C.SOURCE_NIK
            WHERE A.NIK <> $nik
            ORDER BY C.CREATED_AT DESC NULLS LAST, NAMA_LGKP";
            $r = DB::select($sql);
            return $r;

    }


    public function ept_get_kontak_byname($nik,$nama)
    {

        $sql ="SELECT A.USER_ID, INITCAP(A.NAMA_LGKP) NAMA_LGKP, A.NIK
            , CASE WHEN B.IS_ACTIVE IS NOT NULL AND TRUNC(B.LAST_ACTIVITY) = TRUNC(SYSDATE) THEN 'Online' ELSE 'Offline' END IS_ACTIVE
            , CASE WHEN B.IS_ACTIVE IS NOT NULL AND TRUNC(B.LAST_ACTIVITY) = TRUNC(SYSDATE) THEN 'text-success' ELSE 'text-muted' END IS_COLOR
            , TYPE_MSG
            , CASE WHEN C.TEXT IS NULL THEN NULL
               WHEN LENGTH(C.TEXT) >= 20 THEN SUBSTR(C.TEXT,0,20) || ' ...'
                 ELSE C.TEXT END TEXT
            , CASE WHEN C.READ IS NULL THEN '0' ELSE C.READ END READ
            , CASE WHEN C.JML IS NULL THEN 0 ELSE C.JML END JML
            , CASE 
                WHEN C.CREATED_AT IS NULL THEN NULL 
                WHEN TRUNC(C.CREATED_AT) = TRUNC(SYSDATE) THEN  TO_CHAR(C.CREATED_AT,'HH24:MI') 
                WHEN TRUNC(C.CREATED_AT) = TRUNC(SYSDATE-1) THEN  'Yesterday' 
                WHEN TRUNC(C.CREATED_AT) < TRUNC(SYSDATE-1) AND TRUNC(C.CREATED_AT) > TRUNC(SYSDATE-7)  THEN  TO_CHAR(C.CREATED_AT,'Day') 
                ELSE TO_CHAR(C.CREATED_AT,'DD/MM/YYYY') 
              END CREATED_AT
            FROM SIAK_USER_PLUS A 
            LEFT JOIN SIAK_SESSION_PLUS B ON A.USER_ID = B.USER_ID 
            LEFT JOIN 
                (SELECT ID, TYPE_MSG, SOURCE_NIK, SOURCE_MSG, DELETED_FROM, DELETED_TO, TEXT, READ, CREATED_AT, UPDATED_AT, OPR_READ, (SELECT COUNT(1) FROM MESSAGES X WHERE X.READ = 0 AND X.FROM_ID = A.SOURCE_NIK AND X.TO_ID = $nik ) JML FROM(  
                SELECT  ID, TYPE_MSG, SOURCE_NIK, SOURCE_MSG, DELETED_FROM, DELETED_TO, TEXT, READ, CREATED_AT, UPDATED_AT, OPR_READ, RANK() OVER (PARTITION BY SOURCE_MSG ORDER BY CREATED_AT DESC,UPDATED_AT DESC, ID DESC) RNK FROM 
                (SELECT ID, TYPE_MSG, SOURCE_NIK, SOURCE_MSG, DELETED_FROM, DELETED_TO, TEXT, READ, CREATED_AT, UPDATED_AT, OPR_READ, RNK FROM 
                (SELECT ID, 'Receive' TYPE_MSG, FROM_ID SOURCE_NIK, FROM_ID||TO_ID SOURCE_MSG, DELETED_FROM, DELETED_TO, TEXT, READ, CREATED_AT, UPDATED_AT, OPR_READ, RANK() OVER (PARTITION BY FROM_ID ORDER BY CREATED_AT DESC,UPDATED_AT DESC, ID DESC) RNK FROM MESSAGES WHERE TO_ID = $nik) 
                UNION ALL
                (SELECT ID, 'Sent' TYPE_MSG, TO_ID SOURCE_NIK,TO_ID||FROM_ID SOURCE_MSG, DELETED_FROM, DELETED_TO, TEXT, READ, CREATED_AT, UPDATED_AT, OPR_READ, RANK() OVER (PARTITION BY TO_ID ORDER BY CREATED_AT DESC,UPDATED_AT DESC, ID DESC) RNK FROM MESSAGES WHERE FROM_ID = $nik))) A WHERE RNK =1) C ON A.NIK = C.SOURCE_NIK
            WHERE A.NIK <> $nik AND A.NAMA_LGKP LIKE '$nama%'
            ORDER BY C.CREATED_AT DESC NULLS LAST, NAMA_LGKP";
            $r = DB::select($sql);
            return $r;

    }


    public function ept_get_kontak_warga_byname($nik,$nama)
    {

        $sql ="SELECT A.ID USER_ID, INITCAP(A.NAME) NAMA_LGKP, A.NIK, A.PIC
            ,  'Online' IS_ACTIVE
            ,  'text-success' IS_COLOR
            , TYPE_MSG
            , CASE WHEN C.TEXT IS NULL THEN NULL
               WHEN LENGTH(C.TEXT) >= 20 THEN SUBSTR(C.TEXT,0,20) || ' ...'
                 ELSE C.TEXT END TEXT
            , CASE WHEN C.READ IS NULL THEN '0' ELSE C.READ END READ
            , CASE WHEN C.JML IS NULL THEN 0 ELSE C.JML END JML
            , CASE 
                WHEN C.CREATED_AT IS NULL THEN NULL 
                WHEN TRUNC(C.CREATED_AT) = TRUNC(SYSDATE) THEN  TO_CHAR(C.CREATED_AT,'HH24:MI') 
                WHEN TRUNC(C.CREATED_AT) = TRUNC(SYSDATE-1) THEN  'Yesterday' 
                WHEN TRUNC(C.CREATED_AT) < TRUNC(SYSDATE-1) AND TRUNC(C.CREATED_AT) > TRUNC(SYSDATE-7)  THEN  TO_CHAR(C.CREATED_AT,'Day') 
                ELSE TO_CHAR(C.CREATED_AT,'DD/MM/YYYY') 
              END CREATED_AT
            FROM USERS A  
            INNER JOIN 
                (SELECT ID, TYPE_MSG, SOURCE_NIK, SOURCE_MSG, DELETED_FROM, DELETED_TO, TEXT, READ, CREATED_AT, UPDATED_AT, OPR_READ, (SELECT COUNT(1) FROM MESSAGES X WHERE X.READ = 0 AND X.FROM_ID = A.SOURCE_NIK AND X.TO_ID = $nik ) JML FROM(  
                SELECT  ID, TYPE_MSG, SOURCE_NIK, SOURCE_MSG, DELETED_FROM, DELETED_TO, TEXT, READ, CREATED_AT, UPDATED_AT, OPR_READ, RANK() OVER (PARTITION BY SOURCE_MSG ORDER BY CREATED_AT DESC,UPDATED_AT DESC, ID DESC) RNK FROM 
                (SELECT ID, TYPE_MSG, SOURCE_NIK, SOURCE_MSG, DELETED_FROM, DELETED_TO, TEXT, READ, CREATED_AT, UPDATED_AT, OPR_READ, RNK FROM 
                (SELECT ID, 'Receive' TYPE_MSG, FROM_ID SOURCE_NIK, FROM_ID||TO_ID SOURCE_MSG, DELETED_FROM, DELETED_TO, TEXT, READ, CREATED_AT, UPDATED_AT, OPR_READ, RANK() OVER (PARTITION BY FROM_ID ORDER BY CREATED_AT DESC,UPDATED_AT DESC, ID DESC) RNK FROM MESSAGES WHERE TO_ID = $nik) 
                UNION ALL
                (SELECT ID, 'Sent' TYPE_MSG, TO_ID SOURCE_NIK,TO_ID||FROM_ID SOURCE_MSG, DELETED_FROM, DELETED_TO, TEXT, READ, CREATED_AT, UPDATED_AT, OPR_READ, RANK() OVER (PARTITION BY TO_ID ORDER BY CREATED_AT DESC,UPDATED_AT DESC, ID DESC) RNK FROM MESSAGES WHERE FROM_ID = $nik))) A WHERE RNK =1) C ON A.NIK = C.SOURCE_NIK
            WHERE A.NIK <> $nik AND A.NAME LIKE '$nama%'
            ORDER BY C.CREATED_AT DESC NULLS LAST, NAMA_LGKP";
            $r = DB::select($sql);
            return $r;

    }


    public function ept_get_pesan($mynik,$tonik)
    {

        $sql ="SELECT 
                    CASE WHEN FROM_ID = $mynik THEN 'odd' ELSE NULL END SOURCE
                    , TEXT
                    , IMG
                    , TO_CHAR(CREATED_AT,'DD/MM/YYYY') TGL
                    , TO_CHAR(CREATED_AT,'HH24:MI') JAM 
                FROM MESSAGES WHERE 
                TO_ID || FROM_ID = ".$tonik."".$mynik." 
                OR TO_ID || FROM_ID = ".$mynik."".$tonik." 
                ORDER BY CREATED_AT";
            $r = DB::select($sql);
            return $r;

    }


    public function ept_read_pesan($mynik,$tonik)
    {

        $sql ="UPDATE MESSAGES SET READ = 1 WHERE TO_ID =$mynik AND FROM_ID = $tonik";
            $r = DB::update($sql);

    }


    public function chat_event(Request $request)
    {

        header('Content-type: application/json');
        if ($request->text != null){
            $ntf = 'pesan';
            $text = $request->text;
            $from_id = Session::get('S_NIK');
            $nama_lgkp = Session::get('S_NAMA_LGKP');
            $to_id = $request->to_id;
            $img = '';
            AndrApi::chat_event($text, $from_id, $to_id, $nama_lgkp,$img);
            $output = array(
                    "message_type"=>1,
                    "message"=>"Pesan Terkirim !"
            );
            return $output; 
        }
       
    }

    public function ept_push_berkas_kk($nik){
            $sql = "SELECT NIK FROM BIODATA_WNI WHERE STAT_HBKEL = 1 AND NO_KK IN (SELECT NO_KK FROM BIODATA_WNI WHERE NIK = $nik)";
            $get_nik = DB::connection('db222')->select($sql);
            $isnik = $get_nik[0]->nik;
            $sql = "INSERT INTO DOKUMEN_BSRE@WEBDB (OWNER_NIK
                    , NO_DOK
                    , NAMA_LGKP
                    , NAMA_DOK
                    , URL_DOK
                    , ACTIVATED_AT)
                    SELECT A.NIK, A.NO_KK NO_DOK, A.NAMA_LGKP, 'KARTU KELUARGA' NAMA_DOK, B.URL_DOKUMEN URL_DOK, B.ACTIVE_DATE ACTIVATED_AT 
                    FROM BIODATA_WNI A 
                    LEFT JOIN (
                            SELECT 
                            NO_DOC
                            , CERT_STATUS
                            , REQ_DATE
                            , REQ_BY
                            , SEQN_ID
                            , URL_DOKUMEN
                            , PEJABAT_PROCCESS_BY
                            , PEJABAT_PROCESS_DATE  
                            , ACTIVE_DATE  
                          FROM 
                          (
                            SELECT 
                            NO_DOC
                            , CERT_STATUS
                            , REQ_DATE
                            , REQ_BY
                            , SEQN_ID
                            , URL_DOKUMEN
                            , PEJABAT_PROCCESS_BY
                            , PEJABAT_PROCESS_DATE 
                            , ACTIVE_DATE 
                            , RANK() OVER (PARTITION BY NO_DOC ORDER BY REQ_DATE DESC,SEQN_ID DESC) RNK FROM BSRE_KARTU_KELUARGA) 
                            WHERE RNK = 1) B ON A.NO_KK = B.NO_DOC
                    WHERE NIK = $isnik AND B.CERT_STATUS = 4 AND NOT EXISTS(SELECT 1 FROM DOKUMEN_BSRE@WEBDB C 
                    WHERE A.NIK = C.OWNER_NIK AND C.NAMA_DOK = 'KARTU KELUARGA')";
            $r = DB::connection('db222')->insert($sql);
            return $r;
    }

    public function ept_update_kk($no_kk,$email,$telepon){
            $sql = "UPDATE DATA_KELUARGA SET SMS_PHONE = '$telepon', EMAIL = '$email' WHERE NO_KK = $no_kk";
            $r = DB::connection('db222')->update($sql);
            return $r;
    }

    public function ept_push_berkas_sktt($nik){
            $sql = "INSERT INTO DOKUMEN_BSRE@WEBDB (OWNER_NIK
                    , NO_DOK
                    , NAMA_LGKP
                    , NAMA_DOK
                    , URL_DOK
                    , ACTIVATED_AT)
                    SELECT A.NIK, A.NO_KK NO_DOK, A.NAMA_PERTMA ||' '||A.NAMA_KLRGA NAMA_LGKP, 'BIODATA SKTT' NAMA_DOK, B.URL_DOKUMEN URL_DOK, B.ACTIVE_DATE ACTIVATED_AT FROM BIODATA_WNA A 
                    LEFT JOIN (
                            SELECT 
                            NO_DOC
                            , CERT_STATUS
                            , REQ_DATE
                            , REQ_BY
                            , SEQN_ID
                            , URL_DOKUMEN
                            , PEJABAT_PROCCESS_BY
                            , PEJABAT_PROCESS_DATE  
                            , ACTIVE_DATE  
                          FROM 
                          (
                            SELECT 
                            NO_DOC
                            , CERT_STATUS
                            , REQ_DATE
                            , REQ_BY
                            , SEQN_ID
                            , URL_DOKUMEN
                            , PEJABAT_PROCCESS_BY
                            , PEJABAT_PROCESS_DATE 
                            , ACTIVE_DATE 
                            , RANK() OVER (PARTITION BY NO_DOC ORDER BY REQ_DATE DESC,SEQN_ID DESC) RNK FROM BSRE_BIODATA_SKTT) 
                            WHERE RNK = 1) B ON A.NIK = B.NO_DOC
                    WHERE NIK = $nik AND B.CERT_STATUS > 3 AND NOT EXISTS(SELECT 1 FROM DOKUMEN_BSRE@WEBDB C WHERE A.NIK = C.OWNER_NIK AND C.NAMA_DOK = 'BIODATA SKTT')";
            $r = DB::connection('db222')->insert($sql);
            return $r;
    }


    public function get_nik_cetak(request $request){
        header('Content-type: application/json');
        $nik = $request->nik;
        $this->ept_update_cetak($nik);
        return $this->ept_get_nik_cetak($nik);
    }
    
    public function ept_get_nik_cetak($nik){
            $sql = "SELECT  
                    ROWNUM RN
                    , ROWNUM POSISI
                    , A.NO_SKTS 
                    , A.NIK
                    , A.NAMA_LGKP
                    , A.TMPT_LHR
                    , TO_CHAR(A.TGL_LHR ,'DD-MM-YYYY') TGL_LHR
                    , UPPER(F5_GET_REF_WNI(F5_TO_NUMBER(A.JENIS_KLMIN,7), 801)) JENIS_KLMIN
                    , A.IMG_1 PIC
                    , UPPER(F5_GET_REF_WNI(F5_TO_NUMBER(A.AGAMA,7), 501)) AGAMA
                    , UPPER(F5_GET_REF_WNI(A.STAT_KWN, 601)) STAT_KWN
                    , UPPER(F5_GET_REF_WNI(A.GOL_DRH, 401)) GOL_DRH
                    , UPPER(F5_GET_REF_WNI(A.PENDIDIKAN, 101)) PENDIDIKAN
                    , UPPER(F5_GET_REF_WNI(A.PEKERJAAN, 201)) PEKERJAAN
                    , F5_GET_NAMA_PROVINSI(A.SRC_PROV) NAMA_PROP
                    , F5_GET_NAMA_KABUPATEN(A.SRC_PROV,A.SRC_KAB) NAMA_KAB
                    , F5_GET_NAMA_KECAMATAN(A.SRC_PROV,A.SRC_KAB ,A.SRC_KEC) NAMA_KEC
                    , F5_GET_NAMA_KELURAHAN(A.SRC_PROV,A.SRC_KAB ,A.SRC_KEC ,A.SRC_KEL ) NAMA_KEL
                    , A.SRC_ALAMAT
                    , A.SRC_RT 
                    , A.SRC_RW
                    , F5_GET_NAMA_PROVINSI(A.PROP) PROP
                    , F5_GET_NAMA_KABUPATEN(A.PROP,A.KAB) KAB
                    , F5_GET_NAMA_KECAMATAN(32,73,A.KEC) KEC 
                    , F5_GET_NAMA_KELURAHAN(32,73,A.KEC,A.KEL) KEL
                    , A.ALAMAT
                    , A.NO_RT
                    , A.NO_RW
                    , A.TELEPON
                    , A.EMAIL
                    , A.PROC_STAT STATUS
                    , TO_CHAR(A.TGL_BERLAKU,'DD-MM-YYYY') TGL_BERLAKU
                    , (SELECT COUNT(1) JML FROM HIST_CETAK B WHERE B.NIK = A.NIK) COUNT_CETAK
                    FROM PENGAJUANSKTS A WHERE PROC_STAT >=3 AND NIK = $nik";
            $r = DB::connection('webpunten')->select($sql);
            return $r;
    }

    public function pengambilan(Request $request)
    {
        Shr::check_login();
        if (Session::get('S_USER_ID') != null) 
        {
            $menu_id = 307;
            $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
            if ($is_akses == 0){
                 return redirect()->route('404Notfound');
            }
            $menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
            $isakses_kec = Shr::get_give_kec();
            $isakses_kel = Shr::get_give_kel();
            $data = array(
                "stitle"=>'Pengambilan',
                "mtitle"=>'Pengambilan',
                "my_url"=>'UserLevel',
                "type_tgl"=>'Tanggal',
                "menu"=>$menu,
                "akses_kec"=>$isakses_kec,
                "akses_kel"=>$isakses_kel,
                "user_id"=>$request->session()->get('S_USER_ID'),
                "user_nik"=>$request->session()->get('S_NIK'),
                "user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
                "user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
                "user_level"=>$request->session()->get('S_USER_LEVEL'),
                "user_no_kec"=>$request->session()->get('S_NO_KEC'),
                "user_no_kel"=>$request->session()->get('S_NO_KEL'),
                "user_level"=>$request->session()->get('S_USER_LEVEL')
            );
            return view('Epunten_pengambilan/main',$data);
        }else{
            return redirect()->route('logout');
        }
        
    }

    public function pengambilan_log(Request $request)
    {
        Shr::check_login();
        if (Session::get('S_USER_ID') != null) 
        {
            $menu_id = 308;
            $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
            if ($is_akses == 0){
                 return redirect()->route('404Notfound');
            }
            $menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
            $isakses_kec = Shr::get_give_kec();
            $isakses_kel = Shr::get_give_kel();
            $data = array(
                "stitle"=>'Pengambilan Log ',
                "mtitle"=>'Pengambilan Log ',
                "my_url"=>'UserLevel',
                "type_tgl"=>'Tanggal',
                "menu"=>$menu,
                "akses_kec"=>$isakses_kec,
                "akses_kel"=>$isakses_kel,
                "user_id"=>$request->session()->get('S_USER_ID'),
                "user_nik"=>$request->session()->get('S_NIK'),
                "user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
                "user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
                "user_level"=>$request->session()->get('S_USER_LEVEL'),
                "user_no_kec"=>$request->session()->get('S_NO_KEC'),
                "user_no_kel"=>$request->session()->get('S_NO_KEL'),
                "user_level"=>$request->session()->get('S_USER_LEVEL')
            );
            return view('Epunten_pengambilan_log/main',$data);
        }else{
            return redirect()->route('logout');
        }
        
    }

    public function get_data_pengambilan(Request $request)
     {
          $draw = intval($request->draw);
          $start = intval($request->start);
          $length = intval($request->length);
          $rekap_pengambilan = $this->ept_rekap_pengambilan();
          $data = array();
          $i = 0;
          foreach($rekap_pengambilan as $r) {
                $i ++;
               $data[] = array(
                    $i,
                    $r->nik,
                    $r->nama_lgkp,
                    $r->tgl_pengambilan,
                    $r->urutan,
                    $r->jam,
                    $r->hari,
                    $r->jenis_dok
                   
               );
          }

          $output = array(
               "draw" => $draw,
                 "recordsTotal" => count($rekap_pengambilan),
                 "recordsFiltered" => count($rekap_pengambilan),
                 "data" => $data
            );
          return $output;
    }
    
    public function get_data_pengambilan_log(Request $request)
     {
          $draw = intval($request->draw);
          $start = intval($request->start);
          $length = intval($request->length);
          $rekap_pengambilan = $this->ept_rekap_pengambilan_log();
          $data = array();
          $i = 0;
          foreach($rekap_pengambilan as $r) {
                $i ++;
               $data[] = array(
                    $i,
                    $r->nik,
                    $r->nama_lgkp,
                    $r->tgl_pengambilan,
                    $r->urutan,
                    $r->jam,
                    $r->hari,
                    $r->jenis_dok
                   
               );
          }

          $output = array(
               "draw" => $draw,
                 "recordsTotal" => count($rekap_pengambilan),
                 "recordsFiltered" => count($rekap_pengambilan),
                 "data" => $data
            );
          return $output;
    }
     public function ept_rekap_pengambilan()
    {
        header("Content-Type: application/json", true);
         $sql = "SELECT NIK, NAMA_LGKP,TO_CHAR(TGL_PENGAMBILAN,'DD-MM-YYYY HH24:MI:SS') TGL_PENGAMBILAN, URUTAN, JAM, HARI, JENIS_DOK FROM PENGAMBILAN WHERE TRUNC(TGL_PENGAMBILAN) >= TRUNC(SYSDATE)  ORDER BY TGL_PENGAMBILAN";
         $r = DB::connection('webpunten')->select($sql);
        return $r;
    }
     public function ept_rekap_pengambilan_log()
    {
        header("Content-Type: application/json", true);
         $sql = "SELECT NIK, NAMA_LGKP,TO_CHAR(TGL_PENGAMBILAN,'DD-MM-YYYY HH24:MI:SS') TGL_PENGAMBILAN, URUTAN, JAM, HARI, JENIS_DOK FROM PENGAMBILAN WHERE TRUNC(TGL_PENGAMBILAN) < TRUNC(SYSDATE)  ORDER BY TGL_PENGAMBILAN";
         $r = DB::connection('webpunten')->select($sql);
        return $r;
    }


     public function get_user_epunten()
    {
        header("Content-Type: application/json", true);
         $sql = "SELECT NIK, NAMA_LGKP,TO_CHAR(TGL_PENGAMBILAN,'DD-MM-YYYY HH24:MI:SS') TGL_PENGAMBILAN, URUTAN, JAM, HARI, JENIS_DOK FROM PENGAMBILAN WHERE TRUNC(TGL_PENGAMBILAN) < TRUNC(SYSDATE)  ORDER BY TGL_PENGAMBILAN";
         $r = DB::connection('webpunten')->select($sql);
        return $r;
    }

    public function get_user_list(Request $request)
     {
          $draw = intval($request->draw);
          $start = intval($request->start);
          $length = intval($request->length);
          $user_data = $this->ept_get_user_list();
          $data = array();
          $i = 0;
          foreach($user_data as $r) {
                $i ++;
               $data[] = array(
                    $i,
                    $r->nik,
                    $r->no_kk,
                    $r->nama_lgkp,
                    $r->email,
                    $r->telepon,
                    '<button type="button" class="btn btn-primary btn-xs" id="btn-edit" onclick="edit('.$r->id.');" style="margin-top: 5px;">Edit <i class="mdi  mdi-tooltip-edit fa-fw"></i></button> &nbsp <button type="button" class="btn btn-info btn-xs" id="btn-edit-nik" onclick="change_nik('.$r->id.',\''.$r->nik.'\');" style="margin-top: 5px;">Rubah Nik <i class="mdi  mdi-tooltip-edit fa-fw"></i></button> &nbsp <button type="button" class="btn btn-warning btn-xs" id="btn-ubah" onclick="on_reset(\''.$r->nama_lgkp.'\','.$r->id.');" style="margin-top: 5px;">Reset Password <i class="mdi  mdi-sync fa-fw"></i></button> &nbsp <button type="button" class="btn btn-danger btn-xs" id="btn-delete" onclick="hapus('.$r->id.',\''.$r->nama_lgkp.'\');" style="margin-top: 5px;">Delete <i class="mdi mdi-delete-forever fa-fw"></i></button>'
               );
          }

          $output = array(
               "draw" => $draw,
                 "recordsTotal" => count($user_data),
                 "recordsFiltered" => count($user_data),
                 "data" => $data
            );
          return $output;
          exit();
        }

        function ept_get_user_list(){
            $sql ="SELECT A.ID, A.NIK, A.NO_KK, A.NAME NAMA_LGKP, A.EMAIL, A.TELEPON FROM USERS A LEFT JOIN SIAK_USER_PLUS B ON A.NIK = B.NIK  ORDER BY B.NAMA_LGKP, A.ID";
            $r = DB::select($sql);
            return $r;
        }

        function ept_edit_data_user($id,$name,$email,$telepon,$pass){
            $sql= "UPDATE USERS SET NAME = '$name', EMAIL = '$email', TELEPON = '$telepon'
            WHERE ID = $id";
            $r = DB::update($sql);
            if($pass !=''){
                $pass = bcrypt($pass);
                $api_token = Str::random(100);
                $verivication_code = Str::random(5);
                $rmbr_token = Str::random(10);
                 $sql= "UPDATE USERS SET PASSWORD = '$pass', API_TOKEN = '$api_token', REMEMBER_TOKEN = '$rmbr_token', VERIFICATION_CODE = '$rmbr_token' WHERE ID = $id";
                $r = DB::update($sql);
            }
            return $r;
        }

        public function ept_get_nik($nik_baru)
        {

            $sql ="SELECT COUNT(1) JML FROM USERS WHERE NIK = $nik_baru";
            $r = DB::select($sql);
            return $r[0]->jml;

        }

        function ept_change_nik($nik_lama,$nik_baru){
            $sql= "UPDATE USERS SET NIK = '$nik_baru'
            WHERE NIK = '$nik_lama'";
            $r = DB::update($sql);

            $sql= "UPDATE NOTIFICATION SET NIK = '$nik_baru'
            WHERE NIK = '$nik_lama'";
            $r = DB::update($sql);

            $sql= "UPDATE MESSAGES SET FROM_ID = '$nik_baru'
            WHERE FROM_ID = '$nik_lama'";
            $r = DB::update($sql);

            $sql= "UPDATE MESSAGES SET TO_ID = '$nik_baru'
            WHERE TO_ID = '$nik_lama'";
            $r = DB::update($sql);

            $sql= "UPDATE PENGAJUANDATANG SET NIK = '$nik_baru'
            WHERE NIK = '$nik_lama'";
            $r = DB::connection('webpunten')->update($sql);

            $sql= "UPDATE PENGAJUANWNA SET NIK = '$nik_baru'
            WHERE NIK = '$nik_lama'";
            $r = DB::connection('webpunten')->update($sql);

            $sql= "UPDATE PENGAJUANSKTS SET NIK = '$nik_baru'
            WHERE NIK = '$nik_lama'";
            $r = DB::connection('webpunten')->update($sql);

            return $r;
        }

        function ept_do_reset_user($id){
                $pass = bcrypt('123456');
                $api_token = Str::random(100);
                $verivication_code = Str::random(5);
                $rmbr_token = Str::random(10);
                 $sql= "UPDATE USERS SET PASSWORD = '$pass', API_TOKEN = '$api_token', REMEMBER_TOKEN = '$rmbr_token', VERIFICATION_CODE = '$rmbr_token' WHERE ID = $id";
                $r = DB::update($sql);
            return $r;
        }

        function ept_do_edit_psss($id,$pass){
                $pass = bcrypt('$pass');
                $api_token = Str::random(100);
                $verivication_code = Str::random(5);
                $rmbr_token = Str::random(10);
                 $sql= "UPDATE USERS SET PASSWORD = '$pass', API_TOKEN = '$api_token', REMEMBER_TOKEN = '$rmbr_token', VERIFICATION_CODE = '$rmbr_token' WHERE ID = $id";
                $r = DB::update($sql);
            return $r;
        }

        function ept_do_delete_user($id){
                $sql= "DELETE FROM USERS WHERE ID = $id";
                $r = DB::delete($sql);
            return $r;
        }

        public function Laporan_cetak_skts(Request $request)
        {
            Shr::check_login();
            if (Session::get('S_USER_ID') != null) 
            {
                $menu_id = 305;
                $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
                if ($is_akses == 0){
                     return redirect()->route('404Notfound');
                }
                $menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
                $isakses_kec = Shr::get_give_kec();
                $isakses_kel = Shr::get_give_kel();
                if($request->tanggal != null){
                $tgl = $request->tanggal;
                $tgl_start = substr($tgl, 0, 10);
                $tgl_end = substr($tgl,13, 10);
                $no_kec = $request->no_kec;
                $no_kel = $request->no_kel;
                $title_wil = '';
                if($no_kec == 0){
                    $no_wil = 'No Kec';
                    $kode_wil = 'Nama Kecamatan';
                }else{
                    $no_wil = 'No Kel';
                    $kode_wil = 'Nama Kelurahan';
                }
                if($no_kec != 0 && $no_kel == 0){
                    $title_wil = ' Kecamatan '.ucfirst(strtolower(Shr::do_get_nama_kec($no_kec)));
                }else if($no_kec != 0 && $no_kel != 0){
                    $title_wil = ' Kelurahan '.ucfirst(strtolower(Shr::do_get_nama_kel($no_kec,$no_kel)));
                }
                $r = $this->ept_get_lap_skts($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel);
                $data = array(
                    "stitle"=>'Laporan Cetak Skts'.$title_wil.' Tanggal '.$tgl,
                    "mtitle"=>'Laporan Cetak Skts',
                    "my_url"=>'jumlah_cetak',
                    "type_tgl"=>'Pencetakan',
                    "data"=>$r,
                    "jumlah"=>count($r),
                    "menu"=>$menu,
                    "akses_kec"=>$isakses_kec,
                    "akses_kel"=>$isakses_kel,
                    "user_id"=>$request->session()->get('S_USER_ID'),
                    "user_nik"=>$request->session()->get('S_NIK'),
                    "user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
                    "user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
                    "user_no_kel"=>$request->session()->get('S_NO_KEL'),
                    "user_no_kec"=>$request->session()->get('S_NO_KEC'),
                    "user_level"=>$request->session()->get('S_USER_LEVEL'),
                    "no_wil"=>$no_wil,
                    "kode_wil"=>$kode_wil
                );
                }else{
                $data = array(
                    "stitle"=>'Laporan Cetak Skts',
                    "mtitle"=>'Laporan Cetak Skts',
                    "my_url"=>'jumlah_cetak',
                    "type_tgl"=>'Pencetakan',
                    "menu"=>$menu,
                    "akses_kec"=>$isakses_kec,
                    "akses_kel"=>$isakses_kel,
                    "user_id"=>$request->session()->get('S_USER_ID'),
                    "user_nik"=>$request->session()->get('S_NIK'),
                    "user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
                    "user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
                    "user_no_kel"=>$request->session()->get('S_NO_KEL'),
                    "user_no_kec"=>$request->session()->get('S_NO_KEC'),
                    "user_level"=>$request->session()->get('S_USER_LEVEL'),
                    "no_wil"=>'No Kec',
                    "kode_wil"=>'Nama Kecamatan'
                );
                }
                return view('Laporan/main',$data);
            }else{
                return redirect()->route('logout');
            }
        }
        public function Laporan_cetak_detail_skts(Request $request)
        {
            Shr::check_login();
            if (Session::get('S_USER_ID') != null) 
            {
                $menu_id = 306;
                 $is_akses = Shr::cek_is_akses(Session::get('S_USER_LEVEL'),$menu_id);
                if ($is_akses == 0){
                     return redirect()->route('404Notfound');
                }
                $menu = Shr::get_menu(Session::get('S_USER_LEVEL'));
                $isakses_kec = Shr::get_give_kec();
                $isakses_kel = Shr::get_give_kel();
                if($request->no_kec != null){
                $tgl = $request->tanggal;
                $tgl_start = substr($tgl, 0, 10);
                $tgl_end = substr($tgl,13, 10);
                $no_kec = $request->no_kec;
                $no_kel = $request->no_kel;
                $r = $this->ept_get_lap_skts_detail($tgl_start,$tgl_end,$no_kec,$no_kel);
                $data = array(
                    "stitle"=>'List Pencetakan Skts',
                    "mtitle"=>'List Pencetakan Skts '.$tgl,
                    "my_url"=>'jumlah_cetal_detail',
                    "type_tgl"=>'Tanggal Cetak',
                    "data"=>$r,
                    "jumlah"=>count($r),
                    "menu"=>$menu,
                    "akses_kec"=>$isakses_kec,
                    "akses_kel"=>$isakses_kel,
                    "user_id"=>$request->session()->get('S_USER_ID'),
                    "user_nik"=>$request->session()->get('S_NIK'),
                    "user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
                    "user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
                    "user_level"=>$request->session()->get('S_USER_LEVEL'),
                    "user_no_kec"=>$request->session()->get('S_NO_KEC'),
                    "user_no_kel"=>$request->session()->get('S_NO_KEL'),
                    "user_level"=>$request->session()->get('S_USER_LEVEL')
                );
                }else{
                $data = array(
                    "stitle"=>'List Pencetakan Skts',
                    "mtitle"=>'List Pencetakan Skts',
                    "my_url"=>'jumlah_cetal_detail',
                    "type_tgl"=>'Tanggal Cetak',
                    "menu"=>$menu,
                    "akses_kec"=>$isakses_kec,
                    "akses_kel"=>$isakses_kel,
                    "user_id"=>$request->session()->get('S_USER_ID'),
                    "user_nik"=>$request->session()->get('S_NIK'),
                    "user_nama_lgkp"=>$request->session()->get('S_NAMA_LGKP'),
                    "user_nama_dpn"=>$request->session()->get('S_NAMA_DPN'),
                    "user_level"=>$request->session()->get('S_USER_LEVEL'),
                    "user_no_kec"=>$request->session()->get('S_NO_KEC'),
                    "user_no_kel"=>$request->session()->get('S_NO_KEL'),
                    "user_level"=>$request->session()->get('S_USER_LEVEL')
                );
                }
                return view('Epunten_laporan_cetak_skts/main',$data);
            }else{
                return redirect()->route('logout');
            }
        }

        public function ept_get_lap_skts($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel){
            $sql = "";
            if($no_kec == 0){
                $sql .="SELECT 
                        A.NO_KEC AS NO_WIL
                        , A.NAMA_KEC AS NAMA_WIL
                        , '$tgl' AS TANGGAL
                        , CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
                    FROM SETUP_KEC@DB222 A LEFT JOIN
                    (SELECT 
                        KEC NO_KEC
                        , F5_GET_NAMA_KECAMATAN(32,73,KEC) NAMA_KEC
                        , COUNT(1) JUMLAH 
                    FROM HIST_CETAK A INNER JOIN PENGAJUANSKTS B ON A.NIK = B.NIK 
                    WHERE PROP = 32 AND KAB = 73  
                        AND A.CREATED_AT >= TO_DATE('".$tgl_start."','DD-MM-YYYY') 
                        AND A.CREATED_AT < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1
                    GROUP BY KEC ORDER BY KEC) B 
                    ON A.NO_KEC = B.NO_KEC WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 ORDER BY A.NO_KEC";  
            }else{
                $sql .="SELECT 
                            A.NO_KEL AS NO_WIL
                            , A.NAMA_KEL AS NAMA_WIL
                            , '$tgl' AS TANGGAL
                            , CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH
                        FROM SETUP_KEL A LEFT JOIN
                        (SELECT 
                            KEC NO_KEC
                            , F5_GET_NAMA_KECAMATAN(32,73,KEC) NAMA_KEC
                            , KEL NO_KEL
                            , F5_GET_NAMA_KELURAHAN(32,73,KEC,KEL) NAMA_KEL
                            , COUNT(1) JUMLAH 
                        FROM HIST_CETAK A 
                        INNER JOIN PENGAJUANSKTS B ON A.NIK = B.NIK 
                        WHERE PROP = 32 AND KAB = 73  
                        AND A.CREATED_AT >= TO_DATE('".$tgl_start."','DD-MM-YYYY') 
                        AND A.CREATED_AT < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1
                            ";
                        if($no_kel == 0){    
                    $sql .=" AND PROP = 32 AND KAB = 73 AND KEC =".$no_kec."
                            GROUP BY KEC,KEL
                            ORDER BY KEC,KEL
                            ) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." ORDER BY A.NO_KEL";
                        }else{
                    $sql .=" AND PROP = 32 AND KAB = 73 AND KEC =".$no_kec." AND KEL = ".$no_keL."
                            GROUP BY KEC,KEL
                            ORDER BY KEC,KEL
                            ) B ON A.NO_KEL = B.NO_KEL WHERE A.NO_PROP = 32 AND A.NO_KAB = 73 AND A.NO_KEC =".$no_kec." AND KEL = ".$no_keL." ORDER BY A.NO_KEL";
                        }
                }
                
            $r = DB::connection('webpunten')->select($sql);
            return $r;
        }
        public function ept_get_lap_skts_detail($tgl_start,$tgl_end,$no_kec,$no_kel){
            $sql = "SELECT 
                        A.NIK
                        , A.PROP NO_PROP
                        , A.KAB NO_KAB
                        , A.KEC NO_KEC
                        , A.KEL NO_KEL
                        , A.NAMA_LGKP
                        , A.TMPT_LHR
                        , TO_CHAR(A.TGL_LHR,'DD-MM-YYYY') TGL_LHR
                        , TRUNC(MONTHS_BETWEEN(B.PRINTED_DATE,A.TGL_LHR)/12) UMUR
                        , CASE WHEN A.JENIS_KLMIN = 1 THEN 'LAKI-LAKI' ELSE 'PEREMPUAN' END AS JENIS_KLMIN
                        , A.NO_SKTS
                        , B.PRINTED_BY
                        , TO_CHAR(B.PRINTED_DATE,'DD-MM-YYYY') PRINTED_DATE
                        , F5_GET_NAMA_KECAMATAN(A.PROP,A.KAB,A.KEC) NAMA_KEC
                        , F5_GET_NAMA_KELURAHAN(A.PROP,A.KAB,A.KEC,A.KEL) NAMA_KEL
                        , LPAD(TO_CHAR(A.NO_RT), 3, '0') AS RT
                        , LPAD(TO_CHAR(A.NO_RW), 3, '0') AS RW
                        , A.ALAMAT 
                    FROM  PENGAJUANSKTS A
                    INNER JOIN 
                        (
                            SELECT 
                                NIK
                                , PRINTED_DATE
                                , PRINTED_BY
                                , SEQN_ID  FROM 
                                (
                                    SELECT NIK
                                    , CREATED_AT PRINTED_DATE
                                    , PRINTED_BY PRINTED_BY
                                    , ID SEQN_ID
                                    , RANK() OVER 
                                        (
                                            PARTITION BY NIK 
                                            ORDER BY 
                                            CREATED_AT DESC
                                            , ID DESC
                                        ) 
                                    RNK FROM HIST_CETAK
                                )  WHERE RNK = 1
                        ) B ON A.NIK = B.NIK 
                    WHERE 1=1
                    AND A.PROP = 32 AND A.KAB =73
                    AND B.PRINTED_DATE >= TO_DATE('$tgl_start','DD-MM-YYYY') AND B.PRINTED_DATE < TO_DATE('$tgl_end','DD-MM-YYYY') +1 
                    ";
            if($no_kec != 0){
                $sql .= " AND A.KEC = $no_kec";
            }
            if($no_kel != 0){
                $sql .= " AND A.KEL = $no_kel";
            } 
            $sql .=" ORDER BY B.PRINTED_DATE DESC, A.KEC, A.KEL, A.NO_RW, A.NO_RT, A.NAMA_LGKP";
            $r = DB::connection('webpunten')->select($sql);
            return $r;
        }

        public function scr_cek_data_skts($nik = '',$no_kk = '',$nama_lgkp = '',$tgl_lhr = '',$no_kec = 0,$no_kel = 0){
            $sql = "";
            $sql .= " SELECT 
            A.NIK
            , A.ID
            , A.NO_KK
            , A.NAMA_LGKP
            , A.TMPT_LHR
            , TO_CHAR(A.TGL_LHR,'DD-MM-YYYY') AS TGL_LHR
            , UPPER(F5_GET_REF_WNI(F5_TO_NUMBER(A.JENIS_KLMIN,7), 801)) JENIS_KLMIN
            , A.ALAMAT
            , A.NO_SKTS
            , A.PROC_STAT
            , LPAD(TO_CHAR(A.NO_RT), 3, '0') AS RT
            , LPAD(TO_CHAR(A.NO_RW), 3, '0') AS RW
            , F5_GET_NAMA_KECAMATAN(A.PROP,A.KAB,A.KEC) NAMA_KEC
            , F5_GET_NAMA_KELURAHAN(A.PROP,A.KAB,A.KEC,A.KEL) NAMA_KEL
            , UPPER(F5_GET_REF_WNI(F5_TO_NUMBER(A.AGAMA,7), 501)) AGAMA
            , UPPER(F5_GET_REF_WNI(A.PEKERJAAN, 201)) JENIS_PKRJN
            , UPPER(F5_GET_REF_WNI(A.STAT_KWN, 601)) STAT_KWN
            , CASE WHEN to_char(B.TGL_CETAK,'DD-MM-YYYY') IS NULL THEN '-' ELSE  to_char(B.TGL_CETAK,'DD-MM-YYYY') END  KTP_DT  
            , CASE WHEN B.PRINTED_BY IS NULL THEN '-' ELSE  'Opr_'||B.PRINTED_BY END  KTP_BY
            , CASE WHEN TO_CHAR(A.CREATED_AT,'DD-MM-YYYY') IS NULL THEN '-' ELSE TO_CHAR(A.CREATED_AT,'DD-MM-YYYY') END REQ_DATE
            , CASE WHEN TO_CHAR(A.TGL_BERLAKU,'DD-MM-YYYY') IS NULL THEN '-' ELSE TO_CHAR(A.TGL_BERLAKU,'DD-MM-YYYY') END REQ_BY
            , CASE WHEN A.PROC_STAT IS NULL THEN '-' WHEN  A.PROC_STAT = 1 THEN 'Pengajuan Baru' WHEN  A.PROC_STAT = 2 AND A.STAT_BERKAS IS NOT NULL THEN 'Pengajuan Batal' WHEN  A.PROC_STAT = 2 AND A.STAT_BERKAS IS NULL THEN 'Pengajuan Melengkapi' WHEN  A.PROC_STAT = 3 THEN 'Pengajuan Acc Dokumen' WHEN  A.PROC_STAT = 4 THEN 'Pengajuan Selesai' ELSE TO_CHAR(A.PROC_STAT) END STEP_PROC  
                         FROM PENGAJUANSKTS A LEFT JOIN 
            (SELECT  ID,NIK,NAMA_LGKP, TGL_CETAK, PRINTED_BY, CREATED_AT  FROM (SELECT ID,NIK,NAMA_LGKP, TGL_CETAK, PRINTED_BY, CREATED_AT, RANK() OVER (PARTITION BY NIK ORDER BY TGL_CETAK DESC,CREATED_AT DESC, ID DESC) RNK FROM HIST_CETAK)  WHERE RNK = 1) B ON A.NIK = B.NIK
            WHERE 1=1 ";
            if($no_kec != 0 ){ 
                $sql .= " AND A.KEC = $no_kec ";
            } 
            if($no_kel != 0 ){ 
                $sql .= " AND A.KEL = $no_kel ";
            } 
            if($nik != '' || $nik != null || !empty($nik)){ 
                if(strlen($nik) <= 17){
                $sql .= " AND A.NIK = $nik ";   
                }else{
                $sql .= " AND A.NIK IN ($nik) ";    
                }
                
            } 
            if($no_kk != '' || $no_kk != null || !empty($no_kk)){ 
                if(strlen($no_kk) <= 17){
                $sql .= " AND A.NO_KK = $no_kk ";
                }else{
                $sql .= " AND A.NO_KK IN ($no_kk) ";
                }
            }
            if($nama_lgkp != '' || $nama_lgkp != null || !empty($nama_lgkp)){ 
                $sql .= " AND A.NAMA_LGKP LIKE UPPER('$nama_lgkp') ";
            }
            if($tgl_lhr != '' || $tgl_lhr != null || !empty($tgl_lhr)){ 
                $sql .= " AND TO_CHAR(A.TGL_LHR,'DD-MM-YYYY') = '$tgl_lhr' ";
            } 
            $sql .= " ORDER BY A.NO_KK, A.STAT_HBKEL ";
            $r = DB::connection('webpunten')->select($sql);
            return $r;
        }

        public function pushData($jenis, $telepon, $pesan)
        {
          $client = new Client();
          $socketUrl = env("SOCKET_URL", null);
          if($socketUrl != null) {
            $result = $client->request('POST', $socketUrl, [
              'body' => json_encode([
                'jenis' => $jenis,
                'telepon' => $telepon,
                'isi_pesan' => $pesan,
                'tanggal' => date('d-M-Y h:i:s'),
                'waktu' => date('Ymdhis'),
              ], JSON_PRESERVE_ZERO_FRACTION),
              'headers' => ['Content-Type' => 'application/json']
            ]);
          }
        }

        public function kirimSMS($telepon, $pesan)
        {
          $client = new Client();
          $socketUrl = env("E_SPASI_URL", null);
          if($socketUrl != null) {
            $result = $client->request('POST', $socketUrl, [
              'body' => json_encode([
                'telepon' => $telepon,
                'pesan' => $pesan
              ], JSON_PRESERVE_ZERO_FRACTION),
              'headers' => ['Content-Type' => 'application/json']
            ]);
          }
        }
       

        public function hariId($hari)
      {
        if($hari == 'Sunday') {
          return "MINGGU";
        } else if($hari == 'Monday') {
          return "SENIN";
        }
        else if($hari == 'Tuesday') {
          return "SELASA";
        }
        else if($hari == 'Wednesday') {
          return "RABU";
        }
        else if($hari == 'Thursday') {
          return "KAMIS";
        }
        else if($hari == 'Friday') {
          return "JUMAT";
        }
        else if($hari == 'Saturday') {
          return "SABTU";
        } else {
          return $hari;
        }
    }
}
