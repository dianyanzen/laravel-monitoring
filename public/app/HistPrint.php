<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HistPrint extends Model
{	
	protected $connection= 'webpunten';
    public $table = 'WEBPUNTEN.HIST_CETAK';
    protected $guarded  = ['id'];
     protected $fillable = [
        'nik',
        'nama_lgkp',
        'tgl_cetak',
        'printed_by'
    ];
    protected $dates = [
    'created_at',
    'updated_at',
    'tgl_cetak',
    // your other new column
    ];
    // public function fromContact()
    // {
    //     return $this->hasOne(User::class, 'id', 'from_id');
    // }
}
