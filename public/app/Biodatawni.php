<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Biodatawni extends Model
{

	protected $connection= 'db222';

    protected $table = "biodata_wni";

    protected $fillable = ['nama_lgkp','tmpt_lhr','tgl_lhr'];
}