<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pengajuanwna extends Model
{
  use SoftDeletes;
  protected $connection= 'webpunten';
  public $table = 'WEBPUNTEN.PENGAJUANWNA';
  protected $guarded  = ['id'];
  protected $fillable = [
    'nik',
    'img_1',
    'img_thumb_1',
    'img_2',
    'img_thumb_2',
    'img_3',
    'img_thumb_3',
    'img_4',
    'img_thumb_4',
    'img_5',
    'img_thumb_5',
    'img_6',
    'img_thumb_6',
    'img_7',
    'img_thumb_7',
    'img_8',
    'img_thumb_8',
    'img_9',
    'img_thumb_9',
    'img_10',
    'img_thumb_10',
    'img_11',
    'img_thumb_11',
    'img_12',
    'img_thumb_12',
    'img_13',
    'img_thumb_13',
    'img_14',
    'img_thumb_14',
    'img_15',
    'img_thumb_15',
    'proc_stat',
    'stat_berkas',
    
    'verified_by',
    'deleted_by',
    'no_skts',
    'nama_lgkp',
    'tmpt_lhr',
    'kwrngrn',
    
    'jenis_klmin',
    'agama',
    'stat_kwn',
    'gol_drh',
    'pendidikan',
    'pekerjaan',
    'src_prov',
    'src_kab',
    'src_kec',
    'src_kel',
    'src_alamat',
    'src_rt',
    'src_rw',
    'prop',
    'kab',
    'kec',
    'kel',
    'alamat',
    'no_rt',
    'no_rw',
    'status',
    'jenis_layanan',
    'no_paspor',
    'no_dok',
    'dok_imgr',
    'no_kk',
    'stat_hbkel',
    'alasan_pindah',
    'tipe_spsor',
    'nama_spsor',
    
    'jum_anggota', 
    'email',
    'telepon', 
    'nama_fam', 
  ];
  protected $dates = [
    'jangka_waktu',
    'verified_at',
    'tgl_lhr',
    'created_at',
    'updated_at',
    'tgl_berlaku',
    'tgl_datang',
    'deleted_at',
    'verified_at',
    'tgl_paspor',
    'tgl_akh_paspor',
    'tgl_doc_imigrasi',
    'tgl_akh_doc_imgr',
    'tgl_stay',
    'tgl_permit',
    // your other new column
  ];

    public function notif()
    {
      return $this->hasMany(Notification::class,'nik','nik');
    }
    public function user()
    {
        return $this->belongsTo(User::class, 'pengajuan_id');
    }

}
