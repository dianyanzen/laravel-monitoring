<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Concerns\registerEvents;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Events\BeforeWriting;
use Maatwebsite\Excel\Events\BeforeSheet;
use Maatwebsite\Excel\Events\AfterSheet;

class BiodatawniExport implements FromArray, WithHeadings, WithEvents
{
	use  RegistersEventListeners;
    protected $biodata;

    public function __construct(array $biodata)
    {
        $this->biodata = $biodata;
    }

    public function headings(): array
    {
        return [
            'NIK',
            'NO KK',
            'Nama Lengkap',
            'Nama Kepala Keluarga',
            'Umur',
            'Tempat Lahir',
            'Tanggal Lahir',
            'Jenis  Kelamin',
            'Alamat',
            'RT',
            'RW',
            'Kecamatan',
            'Kelurahan',
            'Agama',
            'Pekerjaan',
            'Status Kawin',
            'Stat Hubkel',
            'Kode Pos',
        ];
    }
    public static function beforeExport(BeforeExport $event)
    {
        //
    }

    public static function beforeWriting(BeforeWriting $event)
    {
        //
    }

    public static function beforeSheet(BeforeSheet $event)
    {
        //
    }

    public static function afterSheet(AfterSheet $event)
    {

    }
    public function array(): array
    {
        return $this->biodata;
    }
}
