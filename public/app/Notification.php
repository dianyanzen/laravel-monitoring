<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Notification extends Model
{
    //protected $guarded = [];
    // use SoftDeletes;
    // protected $table = 'notification';
    public $table = 'NOTIFICATION';
    protected $guarded  = ['id'];
     protected $fillable = [
        'type',
        'pengajuan_id',
        'nik',
        'from_user_id',
        'to_user_id',
        'message',
        'seen_at',
        'flag_read',
    ];
    protected $dates = [
    'created_at',
    'updated_at',
    'deleted_at',
    'seen_at',
    // your other new column
    ];
    public function fromContact()
    {
        return $this->hasOne(User::class, 'id', 'from_id');
    }
}

   