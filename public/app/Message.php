<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{


    protected $table = "messages";

    protected $fillable = ['ID','from_id','deleted_from','to_id','deleted_to','text','read','created_at','updated_at','opr_read'];
}