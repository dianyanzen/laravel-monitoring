<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('login/yzdb', 'LoginController@check_yzdb');
Route::get('login/db222', 'LoginController@check_db222');
Route::get('login/db221', 'LoginController@check_db221');
Route::get('login/db2', 'LoginController@check_db2');
Route::get('/', 'LoginController@index')->name('home');
Route::get('/404', function () {
    return view('error.main');
})->name('404Notfound');
Route::get('Android_api/get_data_suket','AndroidApiController@get_data_suket');
Route::get('Listen','AndroidApiController@listen');

//Login
Route::get('login/dologin','LoginController@dologin');
Route::post('login/dologin','LoginController@dologin');
Route::get('login/dologout','LoginController@dologout');
Route::get('logout','LoginController@logout')->name('logout');

// Api Shared
Route::post('Shared_api/get_will','SharedController@get_will');
Route::post('Shared_api/get_one_kecamatan','SharedController@get_one_kecamatan');
Route::post('Shared_api/get_kecamatan','SharedController@get_kecamatan');
Route::post('Shared_api/get_kelurahan','SharedController@get_kelurahan');
Route::post('Shared_api/get_rw','SharedController@get_rw');
Route::post('Shared_api/get_rt','SharedController@get_rt');
Route::post('Shared_api/get_master_menu','SharedController@get_master_menu');
Route::post('Shared_api/get_master_dkb','SharedController@get_master_dkb');
Route::post('Shared_api/get_master_usia','SharedController@get_master_usia');
Route::post('Shared_api/get_master_cakupan','SharedController@get_master_cakupan');
Route::post('Shared_api/get_user_daily','SharedController@get_user_daily');
Route::post('Shared_api/get_user_cpltte','SharedController@get_user_cpltte');
Route::post('Shared_api/get_master_provinsi','SharedController@get_master_provinsi');
Route::post('Shared_api/get_master_kabupaten','SharedController@get_master_kabupaten');
Route::post('Shared_api/get_master_kecamatan','SharedController@get_master_kecamatan');
Route::post('Shared_api/get_master_kelurahan','SharedController@get_master_kelurahan');
Route::post('Shared_api/get_default_pass','SharedController@get_default_pass');
Route::post('Shared_api/get_capil_blk_opt','SharedController@get_capil_blk_opt');

Route::post('Shared_api/get_gisa_kecamatan','SharedController@get_gisa_kecamatan');
Route::post('Shared_api/get_gisa_kelurahan','SharedController@get_gisa_kelurahan');
Route::post('Shared_api/get_gisa_rw','SharedController@get_gisa_rw');
Route::post('Shared_api/get_gisa_rt','SharedController@get_gisa_rt');

Route::post('Shared_api/gisa_get_one_kecamatan','SharedController@gisa_get_one_kecamatan');
Route::post('Shared_api/gisa_get_kelurahan','SharedController@gisa_get_kelurahan');
Route::post('Shared_api/gisa_get_rw','SharedController@gisa_get_rw');
Route::post('Shared_api/gisa_get_rt','SharedController@gisa_get_rt');

Route::post('Shared_api/get_user_lhr','SharedController@get_user_lhr');
Route::get('Shared_api/get_user_lhr','SharedController@get_user_lhr');
Route::post('Shared_api/get_user_kmt','SharedController@get_user_kmt');
Route::post('Shared_api/get_user_lhr_mpl','SharedController@get_user_lhr_mpl');

//Dashboard 
Route::get('Dashboard','LoginController@index');
Route::get('Dashboard/Userktp','DashboardController@Userktpel')->name('cetak');
Route::get('Dashboard/Userktp/{user_bcard}','DashboardController@UserBcard');
Route::get('Dashboard/Userktp/{user_bcard}/{tanggal}','DashboardController@UserBcardTanggal');
Route::get('Dashboard/Ktpel','DashboardController@ktpel');
Route::get('Dashboard/Biodata','DashboardController@Biodata');
Route::get('Dashboard/Mobilitas','DashboardController@Mobilitas');
Route::get('Dashboard/Capil','DashboardController@Capil');
Route::get('Dashboard/Graph','DashboardController@dashboard_graph');

Route::post('Dashboard_ktpel/get_data_rkm','AndroidApiController@get_data_rkm');
Route::post('Dashboard_ktpel/get_data_ctk','AndroidApiController@get_data_ctk');
Route::post('Dashboard_ktpel/get_data_kia','AndroidApiController@get_data_kia');
Route::post('Dashboard_ktpel/get_data_suket','AndroidApiController@get_data_suket_dashboard');
Route::post('Dashboard_biodata/get_data','AndroidApiController@dashboard_biodata');
Route::post('Dashboard_mobilitas/get_data','AndroidApiController@dashboard_mobilitas');
Route::post('Dashboard_mobilitas/get_data_detail','AndroidApiController@dashboard_mobilitas_detail');
Route::post('Dashboard_capil/get_data','AndroidApiController@dashboard_capil');

//Dashboard Main api
Route::post('dashboard/get_dashboard_time','AndroidApiController@get_dashboard_time');
Route::post('dashboard/update_prr','AndroidApiController@update_prr');
Route::post('dashboard/update_sfe','AndroidApiController@update_sfe');
Route::post('dashboard/get_time_m3','AndroidApiController@get_time_m3');
Route::post('dashboard/get_time_m4','AndroidApiController@get_time_m4');
Route::post('dashboard/get_dashboard_m1','AndroidApiController@get_dashboard_m1');
Route::post('dashboard/get_dashboard_m2','AndroidApiController@get_dashboard_m2');
Route::post('dashboard/get_dashboard_m3','AndroidApiController@get_dashboard_m3');
Route::post('dashboard/get_dashboard_m4','AndroidApiController@get_dashboard_m4');
Route::post('dashboard/get_dashboard_m5','AndroidApiController@get_dashboard_m5');
Route::post('dashboard/get_dashboard_m6','AndroidApiController@get_dashboard_m6');
Route::post('dashboard/get_dashboard_m7','AndroidApiController@get_dashboard_m7');
Route::post('dashboard/get_dashboard_m8','AndroidApiController@get_dashboard_m8');
Route::post('dashboard/get_dashboard_kk','AndroidApiController@get_dashboard_kk');
Route::post('dashboard/get_dashboard_kia','AndroidApiController@get_dashboard_kia');
Route::post('dashboard/get_dashboard_nik','AndroidApiController@get_dashboard_nik');
Route::post('dashboard/get_dashboard_nik','AndroidApiController@get_dashboard_nik');
Route::post('dashboard/get_dashboard_akta_lu','AndroidApiController@get_dashboard_akta_lu');
Route::post('dashboard/get_dashboard_akta_lt','AndroidApiController@get_dashboard_akta_lt');
Route::post('dashboard/get_dashboard_akta_mt','AndroidApiController@get_dashboard_akta_mt');
Route::post('dashboard/get_dashboard_akta_kwn','AndroidApiController@get_dashboard_akta_kwn');
Route::post('dashboard/get_dashboard_akta_cry','AndroidApiController@get_dashboard_akta_cry');
Route::post('dashboard/get_dashboard_pdh_akab','AndroidApiController@get_dashboard_pdh_akab');
Route::post('dashboard/get_dashboard_pdh_akec','AndroidApiController@get_dashboard_pdh_akec');
Route::post('dashboard/get_dashboard_pdh_dkec','AndroidApiController@get_dashboard_pdh_dkec');
Route::post('dashboard/get_dashboard_dtg_akab','AndroidApiController@get_dashboard_dtg_akab');
Route::post('dashboard/get_dashboard_dtg_akec','AndroidApiController@get_dashboard_dtg_akec');
Route::post('dashboard/get_dashboard_dtg_dkec','AndroidApiController@get_dashboard_dtg_dkec');

// Detail Dashboard
Route::get('Detail/gdert','DashboardController@gdert');
Route::get('Detail/gdery','DashboardController@gdery');
Route::get('Detail/gdect','DashboardController@gdect');
Route::get('Detail/gdecy','DashboardController@gdecy');
Route::get('Detail/gdest','DashboardController@gdest');
Route::get('Detail/gdesy','DashboardController@gdesy');
Route::get('Detail/gdbkt','DashboardController@gdbkt');
Route::get('Detail/gdbky','DashboardController@gdbky');
Route::get('Detail/gdbbt','DashboardController@gdbbt');
Route::get('Detail/gdbby','DashboardController@gdbby');
Route::get('Detail/gdmdt','DashboardController@gdmdt');
Route::get('Detail/gdmdy','DashboardController@gdmdy');
Route::get('Detail/gdmpt','DashboardController@gdmpt');
Route::get('Detail/gdmpy','DashboardController@gdmpy');
Route::get('Detail/gdclut','DashboardController@gdclut');
Route::get('Detail/gdcluy','DashboardController@gdcluy');
Route::get('Detail/gdcltt','DashboardController@gdcltt');
Route::get('Detail/gdclty','DashboardController@gdclty');
Route::get('Detail/gdckt','DashboardController@gdckt');
Route::get('Detail/gdcky','DashboardController@gdcky');
Route::get('Detail/gdcct','DashboardController@gdcct');
Route::get('Detail/gdccy','DashboardController@gdccy');
Route::get('Detail/gdcmt','DashboardController@gdcmt');
Route::get('Detail/gdcmy','DashboardController@gdcmy');
Route::get('Detail/gdsjrt','DashboardController@gdsjrt');
Route::get('Detail/gdsjry','DashboardController@gdsjry');
Route::get('Detail/gdsjnrt','DashboardController@gdsjnrt');
Route::get('Detail/gdsjnry','DashboardController@gdsjnry');
Route::get('Detail/gdsjdrt','DashboardController@gdsjdrt');
Route::get('Detail/gdsjdry','DashboardController@gdsjdry');
Route::get('Detail/gdsjct','DashboardController@gdsjct');
Route::get('Detail/gdsjcy','DashboardController@gdsjcy');
Route::get('Detail/gdsjbt','DashboardController@gdsjbt');
Route::get('Detail/gdsjby','DashboardController@gdsjby');
Route::get('Detail/gdrsbiot','DashboardController@gdrsbiot');
Route::get('Detail/gdrsbioy','DashboardController@gdrsbioy');
Route::get('Detail/gdrsproct','DashboardController@gdrsproct');
Route::get('Detail/gdrsprocy','DashboardController@gdrsprocy');
Route::get('Detail/gdrsadjt','DashboardController@gdrsadjt');
Route::get('Detail/gdrsadjy','DashboardController@gdrsadjy');
Route::get('Detail/gdrsefact','DashboardController@gdrsefact');
Route::get('Detail/gdrsefacy','DashboardController@gdrsefacy');
Route::get('Detail/gdrssfet','DashboardController@gdrssfet');
Route::get('Detail/gdrssfey','DashboardController@gdrssfey');
Route::get('Detail/gdrsprrt','DashboardController@gdrsprrt');
Route::get('Detail/gdrsprry','DashboardController@gdrsprry');
Route::get('Detail/gdrsdurect','DashboardController@gdrsdurect');
Route::get('Detail/gdrsdurecy','DashboardController@gdrsdurecy');
Route::get('Detail/gdrscapt','DashboardController@gdrscapt');
Route::get('Detail/gdrscapy','DashboardController@gdrscapy');

// Activity
Route::get('Master/Tte','MasterController@tte_kk');
Route::get('Master/TteDetail','MasterController@tte_detail');
Route::get('Master/TteKelahiran','MasterController@tte_kelahiran');
Route::get('Master/TteKematian','MasterController@tte_kematian');
Route::get('Master/TtePerpindahan','MasterController@tte_perpindahan');
Route::get('Master/TtePerkawinan','MasterController@tte_perkawinan');
Route::get('Master/TtePerceraian','MasterController@tte_perceraian');
Route::get('Master/TtePengesahanAnak','MasterController@tte_pengesahananak');
Route::get('Master/TtePengakuanAnak','MasterController@tte_pengakuananak');
Route::get('Master/Salaman','MasterController@mst_salaman');
Route::get('Master/EpuntenDatang','MasterController@mst_edatang');
Route::get('Master/Activity','MasterController@master_activity');
Route::get('Master/List','MasterController@master_list');
Route::get('Master/ListDownload','MasterController@master_list_download');
Route::get('Master/ListSuket','MasterController@master_list_suket');
Route::get('Master/Request','MasterController@master_request');
Route::get('Master/Absensi','MasterController@master_absensi');
Route::get('Master/Contact','MasterController@master_contact');
Route::get('Master/MyActivity','MasterController@activity');
Route::get('ActivityRekap','MasterController@activity_rekap');
Route::get('KK/Detail','MasterController@kk_detail');
Route::get('KK/Bulanan','MasterController@kk_bulanan');
Route::post('KK/Bulanan','MasterController@kk_bulanan');
Route::get('KK/Rekap','MasterController@kk_Rekap');
Route::get('ActivityKedatangan','MasterController@activitykedatangan');
Route::get('ActivityPerpindahan','MasterController@activityPerpindahan');
Route::get('Activity/setting','MasterController@Activity_setting');
Route::get('Activity/absensi_daily','MasterController@absensi_daily');
Route::get('Activity/activity_daily','MasterController@activity_daily');
Route::post('Activity/do_save','MasterController@Activity_do_save');
Route::post('DeleteSuket','NikController@Delete_suket');


Route::get('KK/Pdf_detail','MasterController@kk_pdf_detail');

// Api Activity
Route::post('Activity/get_data_first','MasterController@myactivity_data_first');
Route::post('Activity/get_data_second','MasterController@myactivity_data_second');
Route::post('Activity/get_data_third','MasterController@myactivity_data_third');
Route::post('Activity/get_data_four','MasterController@myactivity_data_four');
Route::post('Activity/siak_data','MasterController@myactivity_siak_data');
Route::post('Activity/bcard_data','MasterController@myactivity_bcard_data');
Route::post('Activity/benroll_data','MasterController@myactivity_benroll_data');
Route::post('Activity/get_rekap_data','MasterController@myactivity_get_rekap_data');
Route::post('Activity/get_rekap_detail_kk','MasterController@myactivity_get_rekap_detail_kk');
Route::post('Activity/rekap_kk_data','MasterController@myactivity_get_rekap_kk_data');
Route::post('Activity/get_rekap_kedatangan','MasterController@myactivity_get_rekap_kedatangan');
Route::post('Activity/get_rekap_perpindahan','MasterController@myactivity_get_rekap_perpindahan');
Route::post('Activity/absensi_daily','MasterController@absensi_daily');
Route::post('Activity/activity_daily','MasterController@activity_daily');


//Siak Restore
Route::get('Siak/Restore','SiakController@restore_bio_wni');
Route::post('Siak/Restore','SiakController@restore_bio_wni');

Route::get('Siak/Dewa_kematian','SiakController@Dewa_kematian');
Route::post('Siak/Dewa_kematian','SiakController@Dewa_kematian');
Route::post('Siak/update_dewa_kematian','SiakController@update_dewa_kematian');

Route::get('Siak/restore_bio_wni','SiakController@restore_bio_wni');

// Siak Penyesuaian Nik
Route::get('Siak/Penyesuaian','SiakController@repair_bio_wni');
Route::post('Siak/Penyesuaian','SiakController@repair_bio_wni');

Route::get('Siak/PerbaikanData','SiakController@do_perbaikan_data');
Route::get('Siak/ListPerbaikan','SiakController@list_perbaikan_data');
Route::get('Siak/repair_local_biometric','SiakController@repair_local_biometric');
Route::post('Siak/repair_local_biometric','SiakController@repair_local_biometric');


Route::get('Siak/delete_biometric_cetak','SiakController@delete_biometric_cetak');
Route::get('Siak/delete_biometric_rekam','SiakController@delete_biometric_rekam');
Route::get('Siak/PerbaikanData','SiakController@do_perbaikan_data');
Route::get('Siak/ListPerbaikan','SiakController@list_perbaikan_data');

Route::get('Siak/restore_biodata_wni','SiakController@restore_biodata_wni');
Route::get('Siak/restore_biodata_pindah_wni','SiakController@restore_biodata_pindah_wni');
Route::get('Siak/delete_biodata_wni','SiakController@delete_biodata_wni');
Route::get('Siak/get_js_kk','SiakController@get_js_kk');
Route::get('Siak/cek_no_kk_baru','SiakController@cek_no_kk_baru');
Route::get('Siak/change_no_kk','SiakController@change_no_kk');
Route::get('Siak/change_stat_hbkel','SiakController@change_stat_hbkel');
Route::get('Siak/change_stat_hbkel2','SiakController@change_stat_hbkel2');
Route::get('Siak/repair_biodata_wni','SiakController@repair_biodata_wni');
Route::post('Shared_api/get_ref_biodata_delete','SharedController@get_ref_biodata_delete');

// Api Helpdesk
Route::post('Siak/request_pending','SiakController@do_request_pending');
Route::post('Siak/request_success','SiakController@do_request_success');
Route::post('Siak/request_reject','SiakController@do_request_reject');
Route::post('Siak/send_request','SiakController@send_request');
Route::post('Siak/list_request_pending','SiakController@do_list_request_pending');
Route::post('Siak/list_request_success','SiakController@do_list_request_success');
Route::post('Siak/list_request_reject','SiakController@do_list_request_reject');
Route::post('Siak/get_request','SiakController@do_get_request');
Route::post('Siak/send_request_aprove','SiakController@send_request_aprove');
Route::post('Siak/send_request_reject','SiakController@send_request_reject');
Route::post('Siak/check_ganda','SiakController@check_ganda');

// Api Menu Helpdesk
Route::post('Setting/get_helpdesk_option','AndroidApiController@get_helpdesk_option');

//check ktpel
Route::post('Check/Ktpel','NikController@Nik_cetak');
Route::get('Check/Ktpel','NikController@Nik_cetak');
// Route::get('Check/Ktpel/Pdf','NikController@nik_pdf');
Route::get('Check/Ktpel/Pdf','NikController@nik_pdf_ppdb');

//check ktpel WNA
Route::post('Check/KtpelWna','NikController@Nikwna_cetak');
Route::get('Check/KtpelWna','NikController@Nikwna_cetak');
Route::get('Check/KtpelWna/Pdf','NikController@nikwna_pdf');
// Route::get('Check/Ktpel/Export','NikController@nik_export');
Route::post('Check/Ktpel/Export','NikController@nik_export');
Route::get('Check/Ktpel/Push','NikController@do_push');
Route::get('Check/Duplicate','NikController@Nik_duplicate');
Route::post('Check/Duplicate','NikController@Nik_duplicate');
Route::get('Check/ListDuplicate','NikController@Nik_duplicate');
Route::post('Check/ListDuplicate','NikController@Nik_duplicate');
Route::get('Check/History','NikController@Nik_history');
Route::post('Check/History','NikController@Nik_history');
Route::get('Check/KK','NikController@Nik_kk');
Route::get('Check/KK/Push','NikController@kkdo_push');
Route::post('Check/KK','NikController@Nik_kk');
Route::get('Check/KKRoket','NikController@Nik_kkroket');
Route::post('Check/KKRoket','NikController@Nik_kkroket');
Route::get('Check/Prr','NikController@Nik_prr');
Route::post('Check/Prr','NikController@Nik_prr');
Route::get('Check/KiaBalita','NikController@Nik_kia_balita');
Route::post('Check/KiaBalita','NikController@Nik_kia_balita');
Route::get('Check/KiaPengajuan','NikController@Nik_kia_pengajuan');
Route::post('Check/KiaPengajuan','NikController@Nik_kia_pengajuan');
Route::get('Check/Cetak_kk','NikController@Cetak_kk');
Route::post('Check/Cetak_kk','NikController@Cetak_kk');
Route::get('Check/Nik_baru','NikController@Nik_baru');
Route::post('Check/Nik_baru','NikController@Nik_baru');
Route::get('Check/Rekam','NikController@Nik_srekam');
Route::post('Check/Rekam','NikController@Nik_srekam');
Route::get('Check/Cetak','NikController@Nik_scetak');
Route::post('Check/Cetak','NikController@Nik_scetak');
Route::get('Check/Sfe','NikController@Nik_list_sfe');
Route::post('Check/Sfe','NikController@Nik_list_sfe');
Route::get('Check/ListCetakSuket','NikController@Nik_list_cetak_suket');
Route::post('Check/ListCetakSuket','NikController@Nik_list_cetak_suket');
Route::get('Check/Failure','NikController@Nik_list_failure');
Route::post('Check/Failure','NikController@Nik_list_failure');
Route::get('Check/NotRecord','NikController@Nik_belum_rekam');
Route::post('Check/NotRecord','NikController@Nik_belum_rekam');
Route::get('Check/Beginner','NikController@Nik_pemula');
Route::post('Check/Beginner','NikController@Nik_pemula');
Route::get('Check/Suket','NikController@Nik_prsuket');
Route::post('Check/Suket','NikController@Nik_prsuket');
Route::get('Check/Kia','NikController@Nik_kia');
Route::post('Check/Kia','NikController@Nik_kia');
Route::get('Check/Skts','NikController@Nik_skts');
Route::post('Check/Skts','NikController@Nik_skts');

Route::get('Check/FclPrr','NikController@Fcl_prr');
Route::post('Check/FclPrr','NikController@Fcl_prr');
Route::get('Check/FclCetak','NikController@Fcl_scetak');
Route::post('Check/FclCetak','NikController@Fcl_scetak');
Route::get('Check/FclRekam','NikController@Fcl_srekam');
Route::post('Check/FclRekam','NikController@Fcl_srekam');




// Cetak Dinas Input
Route::get('Cetak_dinas/input_cetak','BlangkoController@input_cetak_dinas');
Route::post('Cetak_dinas/get_ludo','BlangkoController@get_ludo');
Route::get('Cetak_dinas/input_ludo','BlangkoController@input_ludo_dinas');
Route::get('Cetak_dinas/cek_cetak_dinas','BlangkoController@cek_cetak_dinas');
Route::post('Cetak_dinas/cek_cetak_dinas','BlangkoController@cek_cetak_dinas');
Route::get('Cetak_dinas/rekap_cetak_dinas','BlangkoController@rekap_cetak_dinas');
Route::post('Cetak_dinas/rekap_cetak_dinas','BlangkoController@rekap_cetak_dinas');
Route::get('Blangko/blangko_in','BlangkoController@blangko_in');
Route::get('Blangko/blangko_out','BlangkoController@blangko_out');
Route::get('Blangko/blangko_rekap','BlangkoController@blangko_rekap');
Route::post('Blangko/blangko_rekap','BlangkoController@blangko_rekap');

Route::get('Cetak_dinas/input_pengambilan_kia','BlangkoController@input_kia_dinas');
Route::post('Cetak_dinas/do_save_kia','BlangkoController@do_save_kia');
Route::post('Cetak_dinas/get_nik_kia','BlangkoController@get_nik_kia');
Route::post('Cetak_dinas/get_idpengajuan_kia','BlangkoController@get_idpengajuan_kia');
Route::post('Cetak_dinas/get_seqn_print_kia','BlangkoController@get_seqn_print_kia');
Route::post('Cetak_dinas/get_seqn_print_kia_id','BlangkoController@get_seqn_print_kia_id');
Route::post('Cetak_dinas/get_delivery_kia','BlangkoController@get_delivery_kia');
Route::post('Cetak_dinas/get_delivery_kia_id','BlangkoController@get_delivery_kia_id');

Route::get('Blangko/blangko_pemusnahan','BlangkoController@blangko_pemusnahan');
Route::get('Blangko/blangko_gudang','BlangkoController@blangko_gudang');
Route::get('Blangko/blangko_gudang_out','BlangkoController@blangko_gudang_out');
Route::get('Blangko/blangko_gudang_rekap','BlangkoController@blangko_gudang_rekap');
Route::post('Blangko/blangko_gudang_rekap','BlangkoController@blangko_gudang_rekap');



Route::post('Cetak_dinas/do_save_ludo','BlangkoController@do_save_ludo');
Route::post('Cetak_dinas/get_nik','BlangkoController@get_nik');
Route::post('Cetak_dinas/do_save_cetak','BlangkoController@do_save_cetak');
Route::post('Blangko/get_data','BlangkoController@get_data');
Route::post('Blangko/get_allin_data','BlangkoController@get_allin_data');
Route::post('Blangko/get_gudang_data','BlangkoController@get_gudang_data');
Route::post('Blangko/get_dafduk_data','BlangkoController@get_dafduk_data');
Route::post('Blangko/get_pemusnahan_data','BlangkoController@get_pemusnahan_data');
Route::post('Blangko/get_all_data','BlangkoController@get_all_data');
Route::post('Blangko/do_save','BlangkoController@do_save');
Route::post('Blangko/do_save_dis','BlangkoController@do_save_dis');
Route::post('Blangko/doin_save','BlangkoController@doin_save');
Route::post('Blangko/doin_save_gudang','BlangkoController@doin_save_gudang');
Route::post('Blangko/doin_distribusi_gudang','BlangkoController@doin_distribusi_gudang');
Route::post('Blangko/doin_distribusi_pemusnahan','BlangkoController@doin_distribusi_pemusnahan');


Route::get('BlangkoCapil/blangko_in','BlangkoController@blangko_capil_in');
Route::get('BlangkoCapil/blangko_out','BlangkoController@blangko_capil_out');
Route::get('BlangkoCapil/blangko_rekap','BlangkoController@blangko_capil_rekap');
Route::get('BlangkoCapil/blangko_rekap','BlangkoController@blangko_capil_rekap');
Route::post('BlangkoCapil/blangko_rekap','BlangkoController@blangko_capil_rekap');

Route::post('BlangkoCapil/get_allin_capil_data','BlangkoController@get_allin_capil_data');
Route::post('BlangkoCapil/get_allin_capil_data_out','BlangkoController@get_allin_capil_data_out');
Route::post('BlangkoCapil/get_capil_data','BlangkoController@get_capil_data');
Route::post('BlangkoCapil/doin_save_capil','BlangkoController@doin_save_capil');
Route::post('BlangkoCapil/do_save','BlangkoController@do_save_capil_out');

Route::post('Blangko/get_hist_dist','BlangkoController@get_hist_distribusi');
Route::post('Blangko/get_histkec_dist','BlangkoController@get_hist_distribusikec');

// Activity
Route::get('Report/Absensi','ActivityController@Absensi');
Route::get('Report/Absensi_v','ActivityController@Absensi_v');
Route::get('Report/Absensi_vr','ActivityController@Absensi_vr');
Route::post('Report/get_absensi_v','ActivityController@get_absensi_v');
Route::post('Report/get_absensi_vr','ActivityController@get_absensi_vr');
Route::post('Report/get_daily_acc','ActivityController@get_daily_acc');
Route::post('Report/get_daily_tlk','ActivityController@get_daily_tlk');
Route::post('Report/get_tolak','ActivityController@get_tolak');
Route::get('Report/RekapDaily','ActivityController@Rekap_daily');
Route::get('Report/Rekap','ActivityController@Rekap');
Route::get('Report/Doci/{user_id}','ActivityController@Absensi_ci');
Route::get('Report/Doco/{user_id}','ActivityController@Absensi_co');
Route::get('Report/GetDaily','ActivityController@Get_daily_harian2');
// Route::get('Report/GetDaily','ActivityController@Get_daily_harian');
Route::get('Report/GetDaily2','ActivityController@Get_daily_harian2');
Route::get('Report/LaporanAbsen','ActivityController@Laporan_absensi');
Route::get('Report/LaporanDaily','ActivityController@Laporan_activity');
Route::get('Report/LaporanDaily/Lihat/{user_id}','ActivityController@Detail_activity');
Route::get('Report/AbsensiBulanan','ActivityController@Absensi_bulanan');
Route::get('Report/LaporanAbsen/Lihat/{id}','ActivityController@Detail_absensi');

// Route::post('Activity/get_data_first','ActivityController@get_data_first');

Route::post('Report/option_excuse','ActivityController@option_excuse');
Route::post('Report/option_daily','ActivityController@option_daily');
Route::post('Report/daily','ActivityController@daily');
Route::post('Report/excuse','ActivityController@excuse');
Route::post('Report/GetDaily/Pdf','ActivityController@do_pdf');
Route::post('Report/GetDaily/Pdf2','ActivityController@do_pdf2');
Route::post('Report/do_ci','ActivityController@do_ci');
Route::post('Report/do_co','ActivityController@do_co');
Route::post('Report/get_daily_harian_table','ActivityController@get_daily_harian_table');
Route::post('Report/get_daily_harian_table2','ActivityController@get_daily_harian_table2');
Route::post('Report/get_rekap_activity','ActivityController@get_rekap_activity');
Route::post('Report/get_rekap_daily_detail','ActivityController@get_rekap_daily_detail');
Route::post('Report/get_rekap_bulan','ActivityController@get_rekap_bulan');
Route::post('Report/get_rekap_daily','ActivityController@get_rekap_daily');
Route::post('Report/get_rekap_daily_tgl','ActivityController@get_rekap_daily_tgl');
Route::post('Report/get_rekap_data','ActivityController@get_rekap_data');
Route::post('Report/get_rekap_absen','ActivityController@get_rekap_absen');
Route::post('Report/get_rekap_absen_detail','ActivityController@get_rekap_absen_detail');
Route::post('Report/delete_daily_id','ActivityController@delete_daily_id');


Route::get('Search_req/uktp_f','SearchController@uktp_f');
Route::post('Search_req/uktp_f','SearchController@uktp_f');
Route::get('Search_req/dktp_f','SearchController@dktp_f');
Route::post('Search_req/dktp_f','SearchController@dktp_f');
Route::get('Search_req/uktp','SearchController@uktp');
Route::post('Search_req/uktp','SearchController@uktp');
Route::get('Search_req/dktp','SearchController@dktp');
Route::post('Search_req/dktp','SearchController@dktp');
Route::get('Search_req/req_bio','SearchController@req_bio');
Route::get('Search_req/req_bio2','SearchController@req_bio2');
Route::post('Search_req/req_bio','SearchController@req_bio');
Route::get('Search_req/usuket','SearchController@usuket');
Route::post('Search_req/usuket','SearchController@usuket');
Route::get('Search_req/dsuket','SearchController@dsuket');
Route::post('Search_req/dsuket','SearchController@dsuket');
Route::get('Search_req/delete_req','SearchController@delete_req')->name('delete_req');
Route::post('Search_req/delete_req','SearchController@delete_req');
Route::get('Search_req/Do_delete','SearchController@Do_delete');
Route::post('Search_req/Do_delete','SearchController@Do_delete');
Route::get('Request/Tte/Capil_Waiting','SearchController@uttelhr');
Route::post('Request/Tte/Capil_Waiting','SearchController@uttelhr');
Route::get('Request/Tte/Capil_Done','SearchController@dttelhr');
Route::post('Request/Tte/Capil_Done','SearchController@dttelhr');
Route::get('Request/Tte/Dafduk_Waiting','SearchController@uttekk');
Route::post('Request/Tte/Dafduk_Waiting','SearchController@uttekk');
Route::get('Request/Tte/Dafduk_Done','SearchController@dttekk');
Route::post('Request/Tte/Dafduk_Done','SearchController@dttekk');
Route::get('Request/PengajuanKK/Wait','SearchController@upkk');
Route::post('Request/PengajuanKK/Wait','SearchController@upkk');
Route::get('Request/PengajuanKK/Done','SearchController@dpkk');
Route::post('Request/PengajuanKK/Done','SearchController@dpkk');

Route::get('Laporan_rekam','LaporanController@Laporan_rekam');
Route::post('Laporan_rekam','LaporanController@Laporan_rekam');
Route::get('Laporan_cetak','LaporanController@Laporan_cetak');
Route::post('Laporan_cetak','LaporanController@Laporan_cetak');
Route::get('Laporan_suket','LaporanController@Laporan_suket');
Route::post('Laporan_suket','LaporanController@Laporan_suket');
Route::get('Laporan_kk','LaporanController@Laporan_kk');
Route::post('Laporan_kk','LaporanController@Laporan_kk');
Route::get('Laporan_biodata','LaporanController@Laporan_biodata');
Route::post('Laporan_biodata','LaporanController@Laporan_biodata');
Route::get('Laporan_surat_pindah','LaporanController@Laporan_surat_pindah');
Route::post('Laporan_surat_pindah','LaporanController@Laporan_surat_pindah');
Route::get('Laporan_surat_pindah_kec','LaporanController@Laporan_surat_pindah_kec');
Route::post('Laporan_surat_pindah_kec','LaporanController@Laporan_surat_pindah_kec');
Route::get('Laporan_surat_pindah_kel','LaporanController@Laporan_surat_pindah_kel');
Route::post('Laporan_surat_pindah_kel','LaporanController@Laporan_surat_pindah_kel');
Route::get('Laporan_surat_datang','LaporanController@Laporan_surat_datang');
Route::post('Laporan_surat_datang','LaporanController@Laporan_surat_datang');
Route::get('Laporan_surat_datang_kec','LaporanController@Laporan_surat_datang_kec');
Route::post('Laporan_surat_datang_kec','LaporanController@Laporan_surat_datang_kec');
Route::get('Laporan_surat_datang_kel','LaporanController@Laporan_surat_datang_kel');
Route::post('Laporan_surat_datang_kel','LaporanController@Laporan_surat_datang_kel');
Route::get('Laporan_kia','LaporanController@Laporan_kia');
Route::post('Laporan_kia','LaporanController@Laporan_kia');
Route::get('LaporanDafduk/Skpln','LaporanController@Laporan_surat_pindah_ln');
Route::post('LaporanDafduk/Skpln','LaporanController@Laporan_surat_pindah_ln');
Route::get('LaporanDafduk/Skdln','LaporanController@Laporan_surat_datang_ln');
Route::post('LaporanDafduk/Skdln','LaporanController@Laporan_surat_datang_ln');
Route::get('LaporanDafduk/BlangkoEktp','LaporanController@BlangkoEktp');
Route::post('LaporanDafduk/BlangkoEktp','LaporanController@BlangkoEktp');
Route::get('LaporanDafduk/BlangkoEktpMasuk','LaporanController@BlangkoEktpMasuk');
Route::post('LaporanDafduk/BlangkoEktpMasuk','LaporanController@BlangkoEktpMasuk');

Route::get('Laporan_kelahiran','LaporanController@Laporan_kelahiran');
Route::post('Laporan_kelahiran','LaporanController@Laporan_kelahiran');
Route::get('Laporan_perceraian','LaporanController@Laporan_perceraian');
Route::post('Laporan_perceraian','LaporanController@Laporan_perceraian');
Route::get('Laporan_perkawinan','LaporanController@Laporan_perkawinan');
Route::post('Laporan_perkawinan','LaporanController@Laporan_perkawinan');
Route::get('Laporan_kematian','LaporanController@Laporan_kematian');
Route::post('Laporan_kematian','LaporanController@Laporan_kematian');
Route::get('Laporan_kematian_asing','LaporanController@Laporan_kematian_asing');
Route::post('Laporan_kematian_asing','LaporanController@Laporan_kematian_asing');

Route::get('Master_laporan','LaporanController@Master_laporan');
Route::post('Master_laporan','LaporanController@Master_laporan');
Route::get('Master_laporan_usia','LaporanController@Master_laporan_usia');
Route::post('Master_laporan_usia','LaporanController@Master_laporan_usia');
Route::get('Master_cakupan','LaporanController@Master_cakupan');
Route::post('Master_cakupan','LaporanController@Master_cakupan');

Route::get('Laporan_bln_rekam','LaporanController@Laporan_bln_rekam');
Route::post('Laporan_bln_rekam','LaporanController@Laporan_bln_rekam');
Route::get('Laporan_bln_cetak','LaporanController@Laporan_bln_cetak');
Route::post('Laporan_bln_cetak','LaporanController@Laporan_bln_cetak');
Route::get('Laporan_bln_kia','LaporanController@Laporan_bln_kia');
Route::post('Laporan_bln_kia','LaporanController@Laporan_bln_kia');
Route::get('Laporan_bln_kk','LaporanController@Laporan_bln_kk');
Route::post('Laporan_bln_kk','LaporanController@Laporan_bln_kk');
Route::get('Laporan_bln_suket','LaporanController@Laporan_bln_suket');
Route::post('Laporan_bln_suket','LaporanController@Laporan_bln_suket');

Route::get('Laporan_ktp','LaporanController@Laporan_ktp');
Route::post('Laporan_ktp','LaporanController@Laporan_ktp');
Route::get('Laporan_sisa_suket','LaporanController@Laporan_sisa_suket');
Route::post('Laporan_sisa_suket','LaporanController@Laporan_sisa_suket');

Route::get('Grafik_rekam','LaporanController@Grafik_rekam');
Route::post('Grafik_rekam','LaporanController@Grafik_rekam');
Route::get('Grafik_cetak','LaporanController@Grafik_cetak');
Route::post('Grafik_cetak','LaporanController@Grafik_cetak');
Route::get('Grafik_suket','LaporanController@Grafik_suket');
Route::post('Grafik_suket','LaporanController@Grafik_suket');
Route::get('Grafik_kk','LaporanController@Grafik_kk');
Route::post('Grafik_kk','LaporanController@Grafik_kk');
Route::get('Grafik_biodata','LaporanController@Grafik_biodata');
Route::post('Grafik_biodata','LaporanController@Grafik_biodata');
Route::get('Grafik_surat_pindah','LaporanController@Grafik_surat_pindah');
Route::post('Grafik_surat_pindah','LaporanController@Grafik_surat_pindah');
Route::get('Grafik_surat_pindah_kec','LaporanController@Grafik_surat_pindah_kec');
Route::post('Grafik_surat_pindah_kec','LaporanController@Grafik_surat_pindah_kec');
Route::get('Grafik_surat_pindah_kel','LaporanController@Grafik_surat_pindah_kel');
Route::post('Grafik_surat_pindah_kel','LaporanController@Grafik_surat_pindah_kel');
Route::get('Grafik_surat_datang','LaporanController@Grafik_surat_datang');
Route::post('Grafik_surat_datang','LaporanController@Grafik_surat_datang');
Route::get('Grafik_surat_datang_kec','LaporanController@Grafik_surat_datang_kec');
Route::post('Grafik_surat_datang_kec','LaporanController@Grafik_surat_datang_kec');
Route::get('Grafik_surat_datang_kel','LaporanController@Grafik_surat_datang_kel');
Route::post('Grafik_surat_datang_kel','LaporanController@Grafik_surat_datang_kel');
Route::get('Grafik_kia','LaporanController@Grafik_kia');
Route::post('Grafik_kia','LaporanController@Grafik_kia');

Route::get('Grafik_kelahiran','LaporanController@Grafik_kelahiran');
Route::post('Grafik_kelahiran','LaporanController@Grafik_kelahiran');
Route::get('Grafik_perceraian','LaporanController@Grafik_perceraian');
Route::post('Grafik_perceraian','LaporanController@Grafik_perceraian');
Route::get('Grafik_perkawinan','LaporanController@Grafik_perkawinan');
Route::post('Grafik_perkawinan','LaporanController@Grafik_perkawinan');
Route::get('Grafik_kematian','LaporanController@Grafik_kematian');
Route::post('Grafik_kematian','LaporanController@Grafik_kematian');

Route::get('Grafik_master_laporan','LaporanController@Grafik_master_laporan');
Route::post('Grafik_master_laporan','LaporanController@Grafik_master_laporan');

Route::get('Antrian/list_antrian','AntrianController@list_antrian');
Route::get('Antrian/cek_antrian','AntrianController@cek_antrian');
Route::get('Antrian/cek_antrian_log','AntrianController@cek_antrian_log');
Route::get('Antrian/cek_nohp','AntrianController@cek_nohp');
Route::get('Antrian/cek_kepakta','AntrianController@cek_kepakta');
Route::get('Antrian/insert_akta','AntrianController@insert_akta');
Route::get('Antrian/insert_biodata','AntrianController@insert_biodata');


Route::post('Antrian/edit_akta_sms','AntrianController@edit_akta_sms');

Route::post('Antrian/get_antrian','AntrianController@get_antrian');
Route::post('Antrian/get_loket','AntrianController@get_loket');
Route::post('Antrian/get_antrian_today','AntrianController@get_antrian_today');
Route::post('Antrian/get_antrian_tomorrow','AntrianController@get_antrian_tomorrow');
Route::post('Antrian/get_cek_antrian','AntrianController@get_cek_antrian');
Route::post('Antrian/get_cek_antrianlog','AntrianController@get_cek_antrianlog');
Route::post('Antrian/get_cek_nohp','AntrianController@get_cek_nohp');
Route::post('Antrian/get_cek_antrianakta','AntrianController@get_cek_antrianakta');
Route::post('Antrian/get_edit_antrianakta','AntrianController@get_edit_antrianakta');
Route::post('Antrian/get_cek_antrian_biodata','AntrianController@get_cek_antrian_biodata');

Route::post('Antrian/do_delete_akta','AntrianController@do_delete_akta');
Route::post('Antrian/do_delete_antrian','AntrianController@do_delete_antrian');
Route::post('Antrian/do_insert_biodata','AntrianController@do_insert_biodata');
Route::post('Antrian/do_delete_phone','AntrianController@do_delete_phone');



Route::get('Setting/get_group_tree_one','SettingController@get_group_tree_one');
Route::get('Setting/edit_tree_one','SettingController@edit_tree_one');

Route::get('Setting/UserLevel','SettingController@user_level');
Route::get('Setting/User','SettingController@user');
Route::get('Setting/ActivityId','SettingController@User_activity');
Route::get('Setting/MenuMaster','SettingController@menu_master');

Route::get('Setting/Helpdesk','SettingController@set_helpdesk');
Route::get('Setting/Helpdesk/Add','SettingController@set_helpdesk_add');
Route::get('Setting/Helpdesk/Change/{id}','SettingController@set_helpdesk_edit');
Route::get('Setting/Helpdesk/Delete/{id}','SettingController@set_helpdesk_delete');
Route::get('Setting/Helpdesk/Lihat/{id}','SettingController@set_helpdesk_see');

Route::get('Setting/MenuUser/Lihat/{id}','SettingController@see_menu_user');
Route::get('Setting/MenuUser/Change/{id}','SettingController@edit_menu_user');

Route::get('Setting/MenuUser','SettingController@menu_user');

Route::get('Setting/User/Add','SettingController@user_add');
Route::get('Setting/User/Lihat/{id}','SettingController@see_user');
Route::get('Setting/User/Change/{id}','SettingController@change_user');
Route::get('Setting/User/Edit/{id}','SettingController@edit_user');
Route::get('Setting/User/Delete/{id}','SettingController@delete_user');

Route::get('Setting/User_id/Add','SettingController@userid_add');
Route::get('Setting/User_id/Lihat/{id}','SettingController@see_user_id');
Route::get('Setting/User_id/Edit/{id}','SettingController@edit_user_id');

Route::get('Setting/ActivityId/Add','SettingController@activity_add');
Route::get('Setting/ActivityId/Edit/{id}','SettingController@edit_activity');

Route::get('Setting/App','SettingController@seting_app');
Route::get('Setting/Pejabat','SettingController@seting_pejabat');
Route::get('Setting/Pejabat/Add','SettingController@add_pejabat');
Route::get('Setting/Pejabat/Edit/{id}','SettingController@edit_pejabat');


Route::post('Setting/get_group','SettingController@get_group');
Route::post('Setting/menu_settting','SettingController@menu_settting');
Route::post('Setting/get_user_level','SettingController@get_user_level');
Route::post('Setting/get_level','SettingController@get_level');
Route::post('Setting/get_user_list','SettingController@get_user_list');
Route::post('Setting/get_group_user','SettingController@get_group_user');
Route::post('Setting/get_user_activity','SettingController@get_user_activity');
Route::post('Setting/get_user_helpdesk','SettingController@get_user_helpdesk');
Route::post('Setting/get_parent','SettingController@get_parent');
Route::post('Setting/get_child','SettingController@get_child');
Route::post('Setting/do_akses','SettingController@do_akses');
Route::post('Setting/update_lvl','SettingController@update_lvl');
Route::post('Setting/check_lvl','SettingController@check_lvl');
Route::post('Setting/save_lvl','SettingController@save_lvl');
Route::post('Setting/delete_lvl','SettingController@delete_lvl');
Route::post('Setting/get_atasan','SettingController@get_atasan');
Route::post('Setting/get_group_lvl','SettingController@get_group_lvl');
Route::post('Setting/check_userid','SettingController@check_userid');
Route::post('Setting/add_userid','SettingController@add_userid');
Route::post('Setting/edit_userid','SettingController@edit_userid');
Route::post('Activity/do_reset','ActivityController@do_reset');
Route::post('Setting/delete_user_id','SettingController@delete_user_id');
Route::post('Setting/save_activity','SettingController@save_activity');
Route::post('Setting/update_activity','SettingController@update_activity');
Route::post('Setting/delete_activity_id','SettingController@delete_activity_id');
Route::post('Setting/max_helpdesk','SettingController@max_helpdesk');
Route::post('Setting/save_helpdesk','SettingController@save_helpdesk');
Route::post('Setting/update_helpdesk','SettingController@update_helpdesk');
Route::post('Setting/delete_helpdesk','SettingController@delete_helpdesk');
Route::post('Setting/do_edit_menu_id','SettingController@do_edit_menu_id');
Route::post('Setting/edit_setting','SettingController@edit_setting');
Route::post('Setting/get_pejabat_list','SettingController@get_pejabat_list');
Route::post('Setting/save_pejabat','SettingController@save_pejabat');
Route::post('Setting/edit_pejabat_id','SettingController@edit_pejabat_id');
Route::post('Setting/delete_pejabat','SettingController@delete_pejabat');

Route::get('Barang/MasterBarang','BarangController@Master_barang');
Route::get('Barang/InputMasuk','BarangController@Master_barang_in');
Route::get('Barang/InputKeluar','BarangController@Master_barang_out');
Route::get('Barang/MasterBarang/Edit/{id}','BarangController@edit_master_barang');
Route::get('Barang/MasterBarang_in/Edit/{id}','BarangController@edit_master_barang_in');
Route::get('Barang/CekBarang','BarangController@Status_barang');
Route::get('Barang/Laporan','BarangController@Laporan_barang');

Route::get('Barang/MasterAset','BarangController@Master_aset');
Route::get('Barang/InputAset','BarangController@Master_aset_in');

Route::post('Barang/get_master_barang','BarangController@get_master_barang');
Route::post('Barang/insert_master_barang','BarangController@insert_master_barang');
Route::post('Barang/delete_master_barang','BarangController@delete_master_barang');
Route::post('Barang/get_master_aset','BarangController@get_master_aset');
Route::post('Barang/insert_master_aset','BarangController@insert_master_aset');
Route::post('Barang/get_modal_data','BarangController@get_modal_data');
Route::post('Barang/do_edit_master_aset','BarangController@do_edit_master_aset');
Route::post('Barang/delete_master_aset','BarangController@delete_master_aset');
Route::post('Barang/get_barang','BarangController@get_barang');
Route::post('Barang/get_master_barang_in','BarangController@get_master_barang_in');
Route::post('Barang/insert_master_in_barang','BarangController@insert_master_in_barang');
Route::post('Barang/insert_master_out_barang','BarangController@insert_master_out_barang');
Route::post('Barang/get_master_barang_out','BarangController@get_master_barang_out');
Route::post('Barang/do_edit_master_barang','BarangController@do_edit_master_barang');
Route::post('Barang/do_edit_master_barang_in','BarangController@do_edit_master_barang_in');
Route::post('Barang/delete_master_in_barang','BarangController@delete_master_in_barang');
Route::post('Barang/delete_master_out_barang','BarangController@delete_master_out_barang');
Route::post('Barang/get_master_barang_status','BarangController@get_master_barang_status');
Route::post('Barang/get_master_barang_laporan','BarangController@get_master_barang_laporan');

Route::get('Gisa','GisaController@Gisa');
Route::post('Gisa','GisaController@Gisa');
Route::get('gisa','GisaController@Gisa');
Route::post('gisa','GisaController@Gisa');
Route::get('gisa/cek_detail','GisaController@cek_detail');
Route::post('gisa/cek_detail','GisaController@cek_detail');
Route::get('gisa/do_edit','GisaController@do_edit');
Route::post('gisa/do_edit','GisaController@do_edit');

Route::get('Aprove','GisaController@Aprove_kk');
Route::post('Aprove','GisaController@Aprove_kk');

Route::get('LaporanDafduk/Wajibktp','LaporanController@Laporan_dafduk_wktp');
Route::post('LaporanDafduk/Wajibktp','LaporanController@Laporan_dafduk_wktp');
Route::get('LaporanDafduk/IdentitasTunggal','LaporanController@Laporan_dafduk_identitas_tunggal');
Route::post('LaporanDafduk/IdentitasTunggal','LaporanController@Laporan_dafduk_identitas_tunggal');
Route::get('LaporanDafduk/Kitap','LaporanController@Laporan_dafduk_kitap');
Route::post('LaporanDafduk/Kitap','LaporanController@Laporan_dafduk_kitap');
Route::get('LaporanDafduk/Kitas','LaporanController@Laporan_dafduk_kitas');
Route::post('LaporanDafduk/Kitas','LaporanController@Laporan_dafduk_kitas');
Route::get('LaporanDafduk/KtpOA','LaporanController@Laporan_dafduk_ktp_oa');
Route::post('LaporanDafduk/KtpOA','LaporanController@Laporan_dafduk_ktp_oa');
Route::get('LaporanDafduk/BlangkoEktp','LaporanController@BlangkoEktp');
Route::post('LaporanDafduk/BlangkoEktp','LaporanController@BlangkoEktp');
Route::get('LaporanDafduk/TTEkk','LaporanController@TTE_kk');
Route::post('LaporanDafduk/TTEkk','LaporanController@TTE_kk');
Route::get('LaporanDafduk/TTEperpindahan','LaporanController@TTE_perpindahan');
Route::post('LaporanDafduk/TTEperpindahan','LaporanController@TTE_perpindahan');
Route::get('LaporanDafduk/LintasBatas','LaporanController@Laporan_dafduk_lintas_batas');
Route::post('LaporanDafduk/LintasBatas','LaporanController@Laporan_dafduk_lintas_batas');
Route::get('LaporanDafduk/PetugasKhusus','LaporanController@Laporan_dafduk_petugas_khusus');
Route::post('LaporanDafduk/PetugasKhusus','LaporanController@Laporan_dafduk_petugas_khusus');

Route::get('LaporanCapil/PemilikAkta','LaporanController@Laporan_capil_pemilik_akta');
Route::post('LaporanCapil/PemilikAkta','LaporanController@Laporan_capil_pemilik_akta');
Route::get('LaporanCapil/U18','LaporanController@Laporan_capil_pemilik_akta_018');
Route::post('LaporanCapil/U18','LaporanController@Laporan_capil_pemilik_akta_018');
Route::get('LaporanCapil/StatKwn','LaporanController@Laporan_capil_pemilik_akta_perkawinan');
Route::post('LaporanCapil/StatKwn','LaporanController@Laporan_capil_pemilik_akta_perkawinan');
Route::get('LaporanCapil/KawinCampuran','LaporanController@Laporan_perkawinan_campuran');
Route::post('LaporanCapil/KawinCampuran','LaporanController@Laporan_perkawinan_campuran');
Route::get('LaporanCapil/StatCrai','LaporanController@Laporan_capil_pemilik_akta_perceraian');
Route::post('LaporanCapil/StatCrai','LaporanController@Laporan_capil_pemilik_akta_perceraian');
Route::get('LaporanCapil/PengesahanAnak','LaporanController@Laporan_pengesahan_anak');
Route::post('LaporanCapil/PengesahanAnak','LaporanController@Laporan_pengesahan_anak');
Route::get('LaporanCapil/PengangkatanAnak','LaporanController@Laporan_pengangkatan_anak');
Route::post('LaporanCapil/PengangkatanAnak','LaporanController@Laporan_pengangkatan_anak');
Route::get('LaporanCapil/PengakuanAnak','LaporanController@Laporan_pengakuan_anak');
Route::post('LaporanCapil/PengakuanAnak','LaporanController@Laporan_pengakuan_anak');
Route::get('LaporanCapil/PerubahanNama','LaporanController@Laporan_perubahan_nama');
Route::post('LaporanCapil/PerubahanNama','LaporanController@Laporan_perubahan_nama');
Route::get('LaporanCapil/KonversiWniWna','LaporanController@Laporan_wni_wna');
Route::post('LaporanCapil/KonversiWniWna','LaporanController@Laporan_wni_wna');
Route::get('LaporanCapil/KonversiWnaWni','LaporanController@Laporan_wna_wni');
Route::post('LaporanCapil/KonversiWnaWni','LaporanController@Laporan_wna_wni');
Route::get('LaporanCapil/KewarganegaraanGanda','LaporanController@Laporan_warga_ganda');
Route::post('LaporanCapil/KewarganegaraanGanda','LaporanController@Laporan_warga_ganda');
Route::get('LaporanCapil/PeristiwaPenting','LaporanController@Laporan_pristiwa_penting');
Route::post('LaporanCapil/PeristiwaPenting','LaporanController@Laporan_pristiwa_penting');
Route::get('LaporanCapil/PembetulanAkta','LaporanController@Laporan_pembetulan_akta');
Route::post('LaporanCapil/PembetulanAkta','LaporanController@Laporan_pembetulan_akta');
Route::get('LaporanCapil/PembatalanAkta','LaporanController@Laporan_pembatalan_akta');
Route::post('LaporanCapil/PembatalanAkta','LaporanController@Laporan_pembatalan_akta');
Route::get('LaporanCapil/AktaLN','LaporanController@Laporan_lahir_ln');
Route::post('LaporanCapil/AktaLN','LaporanController@Laporan_lahir_ln');
Route::get('LaporanCapil/Kutipan2x','LaporanController@Laporan_lahir_bakak');
Route::post('LaporanCapil/Kutipan2x','LaporanController@Laporan_lahir_bakak');
Route::get('LaporanCapil/PetugasCapil','LaporanController@Petugas_capil');
Route::post('LaporanCapil/get_petugas_capil','LaporanController@get_petugas_capil');
Route::get('LaporanCapil/TTEKelahiran','LaporanController@TTE_kelahiran');
Route::post('LaporanCapil/TTEKelahiran','LaporanController@TTE_kelahiran');
Route::get('LaporanCapil/TTEKematian','LaporanController@TTE_kematian');
Route::post('LaporanCapil/TTEKematian','LaporanController@TTE_kematian');
Route::get('Capil/MonitoringMepeling','ActivityController@Mepeling_monitoring');
Route::get('Capil/WilayahMepeling','ActivityController@Mepeling_wilayah');
Route::post('Capil/get_rekap_mepeling_tgl','ActivityController@get_rekap_mepeling_tgl');
Route::get('Capil/get_rekap_mepeling_tgl','ActivityController@get_rekap_mepeling_tgl');
Route::post('Capil/get_seting_user_mpl','ActivityController@get_seting_user_mpl');
Route::post('Capil/edit_wil_mpl','ActivityController@edit_wil_mpl');
Route::post('Capil/activate_mpl','ActivityController@activate_mpl');
Route::post('Capil/deactivate_mpl','ActivityController@deactivate_mpl');

Route::get('Capil/SalamanBatal','ActivityController@salaman_batal');
Route::post('Capil/get_salaman_batal','ActivityController@get_salaman_batal');
Route::post('Capil/edit_pengajuan_batal','ActivityController@edit_pengajuan_batal');

Route::get('Check/InputLintasBatas','NikController@input_lintas_batas');
Route::get('Check/CekLintasBatas','NikController@cek_lintas_batas');
Route::post('Check/CekLintasBatas','NikController@cek_lintas_batas');
Route::get('Check/CekLintasBatas/Lihat/{id}','NikController@cek_lintas_batas_lihat');
Route::get('Check/CekLintasBatas/Edit/{id}','NikController@cek_lintas_batas_edit');
Route::post('Plb/get_nik','NikController@plb_get_nik');
Route::post('Plb/do_save','NikController@plb_do_save');
Route::post('Plb/do_edit','NikController@plb_do_edit');
Route::post('Plb/do_delete','NikController@plb_do_delete');

Route::get('Check/InputPetugasKhusus','NikController@input_petugas_khusus');
Route::get('Check/CekPetugasKhusus','NikController@cek_petugas_khusus');
Route::post('Check/CekPetugasKhusus','NikController@cek_petugas_khusus');
Route::get('Check/CekPetugasKhusus/Lihat/{id}','NikController@cek_petugas_khusus_lihat');
Route::get('Check/CekPetugasKhusus/Edit/{id}','NikController@cek_petugas_khusus_edit');

Route::post('Ph/get_nik','NikController@ph_get_nik');
Route::post('Ph/do_save','NikController@ph_do_save');
Route::post('Ph/do_edit','NikController@ph_do_edit');
Route::post('Ph/do_delete','NikController@ph_do_delete');




Route::get('ReqKtpPushBio','SearchController@req_ektp_pushbio')->name('req_pushbio');
Route::get('ReqKtpPerubahan','SearchController@req_ektp_perubahan')->name('req_perubahan');
Route::get('ReqKtpRusak','SearchController@req_ektp_rusak')->name('req_rusak');
Route::get('ReqKtpHilang','SearchController@req_ektp_hilang')->name('req_hilang');
Route::get('InputKehilangan','SearchController@req_ektp_detail_hilang')->name('detail_hilang');
Route::post('ListPengajuan','SearchController@list_pengajuan');

Route::post('ListHilang','SearchController@list_hilang');
Route::post('ListPengajuanBatal','SearchController@list_pengajuan_ditolak');
Route::post('Req/Ektp','SearchController@do_ajukan')->name('req_perubahan');
Route::post('Pengajuan/update_hilang','SearchController@edit_kehilangan');

Route::post('ListPengajuanPindahan','SearchController@list_pengajuan_pindahan');
Route::post('ListPengajuanPindahanBatal','SearchController@list_pengajuan_pindahan_ditolak');

// Public
Route::get('Api/Listen','ApiController@listen');
Route::get('Api/login','ApiController@login');
Route::get('fotocetak','AndroidApiController@fotocetak');
Route::get('fotorekam','AndroidApiController@fotorekam');
// Route::get('Api/login','ApiController@login');

// Epunten
// Baru
Route::get('Epunten/skts/baru','EpuntenBackController@skts_baru');
Route::post('ApiEpunten/get_skts_baru','EpuntenBackController@get_skts_baru');
Route::post('ApiEpunten/get_skts_edit','EpuntenBackController@get_skts_edit');
Route::post('ApiEpunten/get_skts_delete','EpuntenBackController@get_skts_delete');
Route::post('ApiEpunten/get_skts_tolak','EpuntenBackController@get_skts_tolak');
Route::post('ApiEpunten/get_skts_acc','EpuntenBackController@get_skts_acc');

// Arsip
Route::get('Epunten/skts/arsip','EpuntenBackController@skts_arsip');
Route::post('ApiEpunten/get_skts_arsip','EpuntenBackController@get_skts_arsip');
Route::post('ApiEpunten/get_skts_edit_arsip','EpuntenBackController@get_skts_edit_arsip');
Route::post('ApiEpunten/get_skts_delete_arsip','EpuntenBackController@get_skts_delete_arsip');

//Batal
Route::get('Epunten/skts/batal','EpuntenBackController@skts_batal');
Route::post('ApiEpunten/get_skts_batal','EpuntenBackController@get_skts_batal');
Route::post('ApiEpunten/get_skts_edit_batal','EpuntenBackController@get_skts_edit_batal');
Route::post('ApiEpunten/get_skts_delete_batal','EpuntenBackController@get_skts_delete_batal');

//Acc Dokumen
Route::get('Epunten/skts/acc_dokumen','EpuntenBackController@skts_acc_dokumen');
Route::post('ApiEpunten/get_skts_acc_dokumen','EpuntenBackController@get_skts_acc_dokumen');
Route::post('ApiEpunten/get_skts_edit_acc_dokumen','EpuntenBackController@get_skts_edit_acc_dokumen');
Route::post('ApiEpunten/get_skts_delete_acc_dokumen','EpuntenBackController@get_skts_delete_acc_dokumen');
Route::post('ApiEpunten/get_skts_beres','EpuntenBackController@get_skts_beres');

//Melengkapi
Route::get('Epunten/skts/melengkapi','EpuntenBackController@skts_melengkapi');
Route::post('ApiEpunten/get_skts_melengkapi','EpuntenBackController@get_skts_melengkapi');
Route::post('ApiEpunten/get_skts_edit_melengkapi','EpuntenBackController@get_skts_edit_melengkapi');
Route::post('ApiEpunten/get_skts_delete_melengkapi','EpuntenBackController@get_skts_delete_melengkapi');
Route::post('ApiEpunten/get_skts_acc_melengkapi','EpuntenBackController@get_skts_acc_melengkapi');

//Pembaruan
Route::get('Epunten/skts/pembaruan','EpuntenBackController@skts_pembaruan');
Route::post('ApiEpunten/get_skts_pembaruan','EpuntenBackController@get_skts_pembaruan');
Route::post('ApiEpunten/get_skts_edit_pembaruan','EpuntenBackController@get_skts_edit_pembaruan');
Route::post('ApiEpunten/get_skts_delete_pembaruan','EpuntenBackController@get_skts_delete_pembaruan');
Route::post('ApiEpunten/get_skts_acc_pembaruan','EpuntenBackController@get_skts_acc_pembaruan');

// Baru
Route::get('Epunten/datang/baru','EpuntenBackController@datang_baru');
Route::post('ApiEpunten/get_datang_baru','EpuntenBackController@get_datang_baru');
Route::post('ApiEpunten/get_datang_edit','EpuntenBackController@get_datang_edit');
Route::post('ApiEpunten/get_datang_delete','EpuntenBackController@get_datang_delete');
Route::post('ApiEpunten/get_datang_tolak','EpuntenBackController@get_datang_tolak');
Route::post('ApiEpunten/get_datang_acc','EpuntenBackController@get_datang_acc');

// Arsip
Route::get('Epunten/datang/arsip','EpuntenBackController@datang_arsip');
Route::post('ApiEpunten/get_datang_arsip','EpuntenBackController@get_datang_arsip');
Route::post('ApiEpunten/get_datang_edit_arsip','EpuntenBackController@get_datang_edit_arsip');
Route::post('ApiEpunten/get_datang_delete_arsip','EpuntenBackController@get_datang_delete_arsip');

//Batal
Route::get('Epunten/datang/batal','EpuntenBackController@datang_batal');
Route::post('ApiEpunten/get_datang_batal','EpuntenBackController@get_datang_batal');
Route::post('ApiEpunten/get_datang_edit_batal','EpuntenBackController@get_datang_edit_batal');
Route::post('ApiEpunten/get_datang_delete_batal','EpuntenBackController@get_datang_delete_batal');

//Acc Dokumen
Route::get('Epunten/datang/acc_dokumen','EpuntenBackController@datang_acc_dokumen');
Route::post('ApiEpunten/get_datang_acc_dokumen','EpuntenBackController@get_datang_acc_dokumen');
Route::post('ApiEpunten/get_datang_edit_acc_dokumen','EpuntenBackController@get_datang_edit_acc_dokumen');
Route::post('ApiEpunten/get_datang_delete_acc_dokumen','EpuntenBackController@get_datang_delete_acc_dokumen');
Route::post('ApiEpunten/get_datang_beres','EpuntenBackController@get_datang_beres');

//Melengkapi
Route::get('Epunten/datang/melengkapi','EpuntenBackController@datang_melengkapi');
Route::post('ApiEpunten/get_datang_melengkapi','EpuntenBackController@get_datang_melengkapi');
Route::post('ApiEpunten/get_datang_edit_melengkapi','EpuntenBackController@get_datang_edit_melengkapi');
Route::post('ApiEpunten/get_datang_delete_melengkapi','EpuntenBackController@get_datang_delete_melengkapi');
Route::post('ApiEpunten/get_datang_acc_melengkapi','EpuntenBackController@get_datang_acc_melengkapi');

// Baru
Route::get('Epunten/wna/baru','EpuntenBackController@wna_baru');
Route::post('ApiEpunten/get_wna_baru','EpuntenBackController@get_wna_baru');
Route::post('ApiEpunten/get_wna_edit','EpuntenBackController@get_wna_edit');
Route::post('ApiEpunten/get_wna_delete','EpuntenBackController@get_wna_delete');
Route::post('ApiEpunten/get_wna_tolak','EpuntenBackController@get_wna_tolak');
Route::post('ApiEpunten/get_wna_acc','EpuntenBackController@get_wna_acc');

// Arsip
Route::get('Epunten/wna/arsip','EpuntenBackController@wna_arsip');
Route::post('ApiEpunten/get_wna_arsip','EpuntenBackController@get_wna_arsip');
Route::post('ApiEpunten/get_wna_edit_arsip','EpuntenBackController@get_wna_edit_arsip');
Route::post('ApiEpunten/get_wna_delete_arsip','EpuntenBackController@get_wna_delete_arsip');

//Batal
Route::get('Epunten/wna/batal','EpuntenBackController@wna_batal');
Route::post('ApiEpunten/get_wna_batal','EpuntenBackController@get_wna_batal');
Route::post('ApiEpunten/get_wna_edit_batal','EpuntenBackController@get_wna_edit_batal');
Route::post('ApiEpunten/get_wna_delete_batal','EpuntenBackController@get_wna_delete_batal');

//Acc Dokumen
Route::get('Epunten/wna/acc_dokumen','EpuntenBackController@wna_acc_dokumen');
Route::post('ApiEpunten/get_wna_acc_dokumen','EpuntenBackController@get_wna_acc_dokumen');
Route::post('ApiEpunten/get_wna_edit_acc_dokumen','EpuntenBackController@get_wna_edit_acc_dokumen');
Route::post('ApiEpunten/get_wna_delete_acc_dokumen','EpuntenBackController@get_wna_delete_acc_dokumen');
Route::post('ApiEpunten/get_wna_beres','EpuntenBackController@get_wna_beres');

//Melengkapi
Route::get('Epunten/wna/melengkapi','EpuntenBackController@wna_melengkapi');
Route::post('ApiEpunten/get_wna_melengkapi','EpuntenBackController@get_wna_melengkapi');
Route::post('ApiEpunten/get_wna_edit_melengkapi','EpuntenBackController@get_wna_edit_melengkapi');
Route::post('ApiEpunten/get_wna_delete_melengkapi','EpuntenBackController@get_wna_delete_melengkapi');
Route::post('ApiEpunten/get_wna_acc_melengkapi','EpuntenBackController@get_wna_acc_melengkapi');



Route::get('Chat','EpuntenBackController@epunten_chat');
Route::get('Epunten/chat','EpuntenBackController@epunten_chat_warga');
Route::get('Epunten/cetak','EpuntenBackController@epunten_cetak');
// Route::get('test','EpuntenBackController@chat_event');
Route::post('ApiEpunten/get_kontak','EpuntenBackController@get_kontak');
Route::post('ApiEpunten/get_kontak_byname','EpuntenBackController@get_kontak_byname');
Route::post('ApiEpunten/get_kontak_warga','EpuntenBackController@get_kontak_warga');
Route::post('ApiEpunten/get_kontak_warga_byname','EpuntenBackController@get_kontak_warga_byname');
Route::post('ApiEpunten/get_pesan','EpuntenBackController@get_pesan');
Route::post('ApiEpunten/chat_event','EpuntenBackController@chat_event');
Route::post('ApiEpunten/get_kontak_notif','EpuntenBackController@get_kontak_notif');
Route::post('ApiEpunten/get_user_edit','EpuntenBackController@get_user_edit');



Route::get('Epunten/skts/pengambilan','EpuntenBackController@pengambilan');
Route::get('Epunten/skts/pengambilan_log','EpuntenBackController@pengambilan_log');
Route::post('pengambilan/get_data_pengambilan','EpuntenBackController@get_data_pengambilan');
Route::post('pengambilan/get_data_pengambilan_log','EpuntenBackController@get_data_pengambilan_log');

Route::get('Epunten/seting/user','EpuntenBackController@epunten_user');
Route::get('Epunten/jumlah_cetak','EpuntenBackController@Laporan_cetak_skts');
Route::post('Epunten/jumlah_cetak','EpuntenBackController@Laporan_cetak_skts');
Route::get('Epunten/jumlah_cetal_detail','EpuntenBackController@Laporan_cetak_detail_skts');
Route::post('Epunten/jumlah_cetal_detail','EpuntenBackController@Laporan_cetak_detail_skts');
// Route::get('change_password','EpuntenBackController@change_password');
Route::post('Epunten/change_nik','EpuntenBackController@change_nik');
Route::post('Epunten/edit_data_user','EpuntenBackController@edit_data_user');
Route::post('Epunten/do_reset','EpuntenBackController@do_reset_user');
Route::post('Epunten/do_delete_user','EpuntenBackController@do_delete_user');


Route::get('Epunten/skts/cek_skts','EpuntenBackController@cek_skts');
Route::post('Epunten/skts/cek_skts','EpuntenBackController@cek_skts');



Route::get('Kpu/Data','LaporanController@get_kpu');
Route::post('Kpu/Data','LaporanController@get_kpu');


Route::get('LaporanPerpindahan','MasterController@laporanperpindahan')->name('laporanpindah');
Route::get('LaporanKedatangan','MasterController@laporankedatangan')->name('laporandatang');

Route::post('Activity/get_laporan_kedatangan','MasterController@mylaporan_get_rekap_kedatangan');
Route::post('Activity/get_laporan_perpindahan','MasterController@mylaporan_get_rekap_perpindahan');


// Siak Terpusat

// Route::get('Pengajuan_ld/pengajuan_ld','BlangkoController@input_cetak_dinas');
Route::get('Pengajuan_ld','SearchController@req_ektp_ld')->name('req_ld');
Route::post('Pengajuan_ld/get_nik_ld','SearchController@get_nik_ld');
Route::post('Req/do_ajukan_ld','SearchController@do_ajukan_ld');

Route::get('Search_req/uktpld','SearchController@uktpld');
Route::post('Search_req/uktpld','SearchController@uktpld');
Route::get('Search_req/dktpld','SearchController@dktpld');
Route::post('Search_req/dktpld','SearchController@dktpld');