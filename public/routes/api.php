<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => 'auth:api'], function(){
	Route::post('details', 'EpuntenController@webdetails');
	Route::post('epunten','EpuntenController@store');
	Route::post('epunten2','EpuntenController@store2');
	Route::post('epunten3','EpuntenController@store3');
	Route::post('reepunten','EpuntenController@re_store');
	Route::post('re_edit','EpuntenController@re_edit');
	Route::post('epunten_update','EpuntenController@restore');
	Route::post('cek_pengajuan','EpuntenController@cek_pengajuan');
	Route::post('cek_pengajuan_kk','EpuntenController@cek_pengajuan_kk');
	Route::post('update_pengajuan','EpuntenController@update_pengajuan');
	Route::post('cek_notif','AndroidApiController@notif');
	Route::post('read_notif','EpuntenController@read_notif');
	Route::post('cek_user_petugas','EpuntenController@cek_user_petugas');
	Route::post('update_pengajuan','EpuntenController@update_pengajuan');
	Route::post('get_kk_epunten','EpuntenController@get_kk_epunten');
});

	Route::prefix('andro')->group(function () {
	Route::post('details', 'EpuntenController@webdetails');
	Route::post('hapus-notif', 'EpuntenController@hapus_notif');
	Route::post('epunten','AndroidEpuntenController@store');
	Route::post('epunten_admin','AndroidEpuntenController@store_admin');
	Route::post('epunten2','AndroidEpuntenController@store2');
	Route::post('epunten3','AndroidEpuntenController@store3');
	Route::post('epunten_array','AndroidEpuntenController@store_array');
	Route::post('reepunten','AndroidEpuntenController@re_store');
	Route::post('re_edit','AndroidEpuntenController@re_edit');
	Route::post('re_edit_admin','AndroidEpuntenController@re_edit_admin');
	Route::post('epunten_update','AndroidEpuntenController@restore');
	Route::post('cek_pengajuan','AndroidEpuntenController@cek_pengajuan');
	Route::post('cek_pengajuan_kk','AndroidEpuntenController@cek_pengajuan_kk');
	Route::post('update_pengajuan','AndroidEpuntenController@update_pengajuan');
	Route::post('cek_notif','AndroidApiController@notif');
	Route::post('cek_berkas','AndroidEpuntenController@cek_berkas');
	Route::post('read_notif','AndroidEpuntenController@read_notif');
	Route::post('cek_user_petugas','AndroidEpuntenController@cek_user_petugas');
	Route::post('update_pengajuan','AndroidEpuntenController@update_pengajuan');
	Route::post('get_kk_epunten','AndroidEpuntenController@get_kk_epunten');
});
	Route::prefix('androv2')->group(function () {
	Route::post('details', 'EpuntenV2Controller@webdetails');
	Route::post('hapus-notif', 'EpuntenV2Controller@hapus_notif');
	Route::post('epunten','AndroidEpuntenV2Controller@store');
	Route::post('epunten_admin','AndroidEpuntenV2Controller@store_admin');
	Route::post('epunten2','AndroidEpuntenV2Controller@store2');
	Route::post('epunten3','AndroidEpuntenV2Controller@store3');
	Route::post('epunten_array','AndroidEpuntenV2Controller@store_array');
	Route::post('reepunten','AndroidEpuntenV2Controller@re_store');
	Route::post('re_edit','AndroidEpuntenV2Controller@re_edit');
	Route::post('re_edit_admin','AndroidEpuntenV2Controller@re_edit_admin');
	Route::post('epunten_update','AndroidEpuntenV2Controller@restore');
	Route::post('cek_pengajuan','AndroidEpuntenV2Controller@cek_pengajuan');
	Route::post('cek_pengajuan_kk','AndroidEpuntenV2Controller@cek_pengajuan_kk');
	Route::post('update_pengajuan','AndroidEpuntenV2Controller@update_pengajuan');
	Route::post('cek_notif','AndroidApiV2Controller@notif');
	Route::post('cek_berkas','AndroidEpuntenV2Controller@cek_berkas');
	Route::post('read_notif','AndroidEpuntenV2Controller@read_notif');
	Route::post('cek_user_petugas','AndroidEpuntenV2Controller@cek_user_petugas');
	Route::post('update_pengajuan','AndroidEpuntenV2Controller@update_pengajuan');
	Route::post('get_kk_epunten','AndroidEpuntenV2Controller@get_kk_epunten');
});


Route::get('/404', function () {
    return view('error_api.main');
})->name('404Notfound');


Route::post('epunten_array','AndroidEpuntenController@store_array');
Route::post('dummy', 'EpuntenController@dummy');
Route::post('total_user', 'EpuntenController@total_user');

Route::post('all_active_chat', 'AndroidApiController@all_active_chat');
Route::post('all_kontak', 'AndroidApiController@all_kontak');
Route::post('get_message', 'AndroidApiController@get_message');
Route::post('chat_send', 'AndroidApiController@chat_send');


Route::post('epunten_versi', 'AndroidApiController@epunten_versi');
Route::post('epunten_persyaratan', 'AndroidApiController@epunten_persyaratan');


Route::post('details', 'EpuntenController@webdetails');
Route::post('login', 'EpuntenController@weblogin');
Route::post('logout', 'EpuntenController@weblogout');
Route::post('register', 'EpuntenController@webregister');
Route::post('reset_password', 'EpuntenController@reset_password');

// Master 
Route::post('cek-nik','AndroidApiController@cek_nik');
Route::post('get-kk','AndroidApiController@get_data_kk');
Route::post('get-nik','AndroidApiController@get_data_nik');

Route::post('get_warga_bandung','AndroidApiController@get_data_suket');
Route::post('mst_prop','SharedController@get_webdb_provinsi');
Route::post('mst_kab','SharedController@get_webdb_kabupaten');
Route::post('mst_kec','SharedController@get_webdb_kecamatan');
Route::post('mst_kel','SharedController@get_webdb_kelurahan');
Route::post('mst_wna','AndroidApiController@mst_wna');
Route::post('cek_negara','AndroidApiController@cek_negara');
Route::post('cek_sisa_skts','AndroidApiController@cek_sisa_skts');
Route::post('cek_sisa_wna','AndroidApiController@cek_sisa_wna');
Route::post('cek_sisa_datang','AndroidApiController@cek_sisa_datang');

// Epunten Combobox
Route::post('ektp','AndroidApiController@get_master_combobox');
Route::post('ektp-kab','AndroidApiController@get_master_combobox_kab');
Route::post('ektp-kec','AndroidApiController@get_master_combobox_kec');
Route::post('ektp-kec2','AndroidApiController@get_master_combobox_kec2');
Route::post('ektp-kel','AndroidApiController@get_master_combobox_kel');
Route::post('ektp-kel2','AndroidApiController@get_master_combobox_kel2');
Route::post('ektp-dest-kel','AndroidApiController@get_master_combobox_dest_kel');


Route::post('sendnotification','AndroidApiController@sendnotification');
Route::post('print','AndroidEpuntenController@print');
Route::post('get_nik_cetak','EpuntenBackController@get_nik_cetak');



Route::post('insert_bsre_kk','EpuntenBackController@push_berkas_kk');
Route::post('insert_bsre_sktt','EpuntenBackController@push_berkas_sktt');
Route::post('edit_password','EpuntenBackController@do_edit_psss');

Route::post('push_berkas_kk','AndroidApiController@push_berkas_kk');


Route::post('get_user_list','EpuntenBackController@get_user_list');
// Route::post('epunten_test','AndroidEpuntenController@epunten_test');

// Route::post('get_token','AndroidApiController@get_token');



Route::post('update_tte_roket','AndroidApiController@update_tte_roket');
Route::post('update_siak_pindah','AndroidApiController@update_siak_pindah');
Route::post('update_anjungan_kia','AndroidApiController@update_anjungan_kia');
Route::post('update_req_done_ktp','AndroidApiController@update_req_done_ktp');
Route::post('update_clean_siak','AndroidApiController@update_clean_siak');
Route::post('update_kepala_keluarga','AndroidApiController@update_kepala_keluarga');



// Satpol

Route::post('get-ceknik','AndroidApiController@get_cek_data_nik');
Route::post('do-blokir','AndroidApiController@insert_nik_blokir');
Route::post('do-unblokir','AndroidApiController@insert_nik_unblokir');

Route::post('report_user','AndroidApiController@get_report_user');



