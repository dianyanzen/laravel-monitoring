<div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $stitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                        <ol class="breadcrumb">
                           <li class="text-info"><?php echo $stitle; ?></li>
                        </ol>
                    </div>
                </div>
                <!-- .row -->
               <div class="row">
                <div class="row">
                    <?php   if (!empty($data)){
                            foreach($data as $row){?>
                    <div class="col-md-3 col-xs-12">
                        <div class="white-box">
                            <div class="user-bg"> <img width="100%" alt="user" src="{{ url('/') }}/assets/plugins/images/wallpaper2.jpg">
                                <div class="overlay-box">
                                    <div class="user-content">
                                        <img src="{{ url('/') }}/assets/plugins/images/pemkot.png" class=" thumb-lg" alt="user-img">
                                        <h4 class="text-white"><?php echo $row->layanan; ?></h4>
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="user-btm-box">
                                <div class="col-md-4 col-sm-4 text-center">
                                    <p class="text-info"><b>Request<br>Today</b></p>
                                    <h1><?php echo $row->request_n; ?></h1> 
                                </div>
                                <div class="col-md-4 col-sm-4 text-center">
                                    <p class="text-info"><b>Aproved<br>Today</b></p>
                                    <h1><?php echo $row->aproved_n; ?></h1> 
                                </div>
                                <div class="col-md-4 col-sm-4 text-center">
                                    <p class="text-info"><b>Waiting <br>Today</b></p>
                                    <h1><?php echo $row->the_rest_n; ?></h1> 
                                </div>
                                <div class="col-md-3 col-sm-3 text-center">
                                    <p class="text-info"><b>Belum<br>Verifikasi</b></p>
                                    <h1><?php echo $row->aproved_red; ?></h1> 
                                </div>
                                <div class="col-md-3 col-sm-3 text-center">
                                    <p class="text-info"><b>Proses<br>Penerbitan</b></p>
                                    <h1><?php echo $row->aproved_orange; ?></h1> 
                                </div>
                                <div class="col-md-3 col-sm-3 text-center">
                                    <p class="text-info"><b>Belum<br>Dipublish</b></p>
                                    <h1><?php echo $row->aproved_blue; ?></h1> 
                                </div>
                                <div class="col-md-3 col-sm-3 text-center">
                                    <p class="text-info"><b>Sudah<br>Dipublish</b></p>
                                    <h1><?php echo $row->aproved_green; ?></h1> 
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                    <?php } ?>
                    
                </div>
                   
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- /.container-fluid -->
         @include('shared.footer_detail')