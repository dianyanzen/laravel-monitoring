<!DOCTYPE html>
<html lang="en">
@include('shared.head')
<body class="fix-header">
    <div id="wrapper">
            @include('shared.nav_top')
            @include('shared/nav_side')
            @include('Laporan_capil_tte_kematian/content')
    </div>
     
    @include('shared/footer')
    
    @include('Laporan_capil_tte_kematian/javascript')
</body>
</html>