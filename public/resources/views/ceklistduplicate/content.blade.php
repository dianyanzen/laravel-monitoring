 <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $stitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                        <ol class="breadcrumb">
                            <li class="active"><?php echo $mtitle; ?></li>
                        </ol>
                    </div>
                </div>
                 <div class="row">
                <form name ="get_form" action="<?php echo $my_url; ?>" method="post">
					@csrf
                    <div class="col-sm-12">
                        <div class="white-box">
                            <div class="row">
                                <div class="col-lg-3">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_nik" type="checkbox" checked disabled="true">
                                            <label for="cb_nik"> Nik</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3">         
                                    <div class="form-group">
                                        <input class="form-control" type="text" id="nik" name="nik" onkeypress="return isNumberKey(event)" onchange="onlyNum()" /></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-3">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_kec" type="checkbox" checked disabled="true">
                                            <label for="cb_kec"> Kecamatan</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3">         
                                    <div class="form-group" <?php if (!empty($is_colnotwil)){echo 'style="display: none!important;"'; }?>>
                                        <select class="form-control select2" name="no_kec" id="no_kec">
                                         <option  value="0">-- Pilih Kecamatan --</option>
                                        </select>
                                    </div>
                                </div>
                                </div>
                                <div class="row">
                                <div class="col-lg-3">         
                                    <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_kel" type="checkbox" checked disabled="true">
                                            <label for="cb_kel"> Kelurahan</label>
                                        </div>
                                </div>
                                <div class="col-lg-3">         
                                    <div class="form-group" <?php if (!empty($is_colnotwil)){echo 'style="display: none!important;"'; }?>>
                                        <select class="form-control select2" name="no_kel" id="no_kel">
                                        <option value="0">-- Pilih Kelurahan --</option>  
                                    </select>
                           
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <button type="submit" class="btn btn-info waves-effect waves-light m-r-10 pull-right" id="btn-filter" onclick="on_serach();" >Search <i class="mdi  mdi-magnify fa-fw"></i></button>
                                  <button type="button" class="btn btn-invert waves-effect waves-light m-r-10 pull-right" id="btn-reset" onclick="on_clear();">Clear <i class="mdi  mdi-delete fa-fw"></i></button>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
                <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">Pencarian Nik Duplicate</div>
                            <div class="panel-wrapper collapse in">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th class="text-center">#</th>
                                            <th>Langkah -Langkah</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td align="center">1</td>
                                            <td><b style="font-weight:bold !important; color:#292961;">Isi Nik Apabila Ingin Mencari Nik Duplicate, (Dapat Mencari Lebih Dari Satu Data Dengan Menambahkan Koma)</b></td>
                                        </tr>
                                        <tr>
                                            <td align="center">2</td>
                                            <td><b style="font-weight:bold !important; color:#292961;">Kosongkan Nik, Dan Pilih Kecamatan Apabila Ingin Memunculkan List Perkecamatan Atau Perkelurahan</b></td>
                                        </tr>
                                        <tr>
                                            <td align="center">3</td>
                                            <td><B>Sekian Dan Terima Kasih, Kalau Bingung Bisa Menanyakan Kepada Team IT</B></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                            </div>
                <?php if (!empty($data)){ ?>
                <div class="row">
                <div class="col-md-12">
                        <div class="white-box">
                            <h3 class="box-title"><?php echo $mtitle; ?></h3>
                            <div class="scrollable">
                                <div class="table-responsive">
                                    <table id="mytable" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                   <thead>
                                    
                                        <tr>
                                            <th width="10%" style="text-align: center;">No Kec</th>
                                            <th width="20%" style="text-align: center;">Nik Duplicate</th>
                                            <th width="20%" style="text-align: center;">Nik Single</th>
                                            <th width="20%" style="text-align: center;">Nama Lengkap</th>
                                            <th width="15%" style="text-align: center;">Status E-KTP</th>
                                            <th width="15%" style="text-align: center;">Tanggal Rekam</th>
                                            <th width="15%" style="text-align: center;">Alamat</th>
                                            <th width="15%" style="text-align: center;">RW</th>
                                            <th width="15%" style="text-align: center;">RT</th>
                                            <th width="15%" style="text-align: center;">Kode Pos</th>
                                            <th width="10%" style="text-align: center;">Nama Kecamatan</th>
                                            <th width="10%" style="text-align: center;">Nama Kelurahan</th>

                                        </tr>
                                       
                                    </thead>
                                    <tbody id="show_data">
                                       <?php
                                        
                                           foreach($data as $row){?>
                                             <tr>
                                                <td width="10%" style="text-align: center;"><?php echo $row->no_kec ;?></td>
                                                <td width="20%" style="text-align: center;"><span style="color: #fff">,</span><?php echo $row->nik ;?></td>
                                                <td width="20%" style="text-align: center;"><span style="color: #fff">,</span><?php echo $row->nik_single ;?></td>
                                                <td width="20%" style="text-align: center;"><?php echo $row->nama_lgkp ;?></td>
                                                <td width="15%" style="text-align: center;"><?php echo $row->status_ktp ;?></td>
                                                <td width="15%" style="text-align: center;"><?php echo $row->tgl_rekam ;?></td>
                                                <td width="15%" style="text-align: center;"><?php echo $row->alamat ;?></td>
                                                <td width="15%" style="text-align: center;"><?php echo $row->no_rw ;?></td>
                                                <td width="15%" style="text-align: center;"><?php echo $row->no_rt ;?></td>
                                                <td width="15%" style="text-align: center;"><?php echo $row->kode_pos ;?></td>
                                                <td width="10%" style="text-align: center;"><?php echo $row->nama_kec ;?></td>
                                                <td width="10%" style="text-align: center;"><?php echo $row->nama_kel ;?></td>
                                            </tr>
                                          
                                          <?php } ?>
                                       
                                        
                                    </tbody>
                                    
                                        <tfoot id="my_foot">
                                            <tr>
                                            <?php if (!empty($jumlah)){ ?>
                                            <th width="80%" colspan="11" style="text-align: center;">Jumlah</th>
                                            <th width="20%"  style="text-align: left;"><?php echo number_format(htmlentities($jumlah),0,',','.');?></th>
                                            <?php } ?>
                                        </tr>

                                            </tfoot>
                                </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <?php if (empty($data)){ ?>
                <div class="row">
                <div class="col-md-12">
                        <div class="white-box">
                            <h3 class="box-title"><?php echo $mtitle; ?></h3>
                            <?php if (!empty($data_duplicate)){ 
                                $niks = array();
                                foreach($data_duplicate as $row){
                                    $niks[] = $row->nik;
                                    }
                                ?>
                            <?php } ?>
                            <div class="scrollable">
                                <div class="table-responsive">
                                    <table id="demo-foo-row-toggler" class="table m-t-30 table-hover contact-list color-table info-table" data-page-size="100" style="margin-left: 1px; margin-right: 1px;">
                                        <thead>
                                            <tr>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">NO</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">NIK DUPLICATE</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">NIK SINGLE / MULTIPLE NIK'S</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">NAMA DUPLICATE</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">ALAMAT DUPLICATE</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">STATUS KTP DUPLICATE</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">PROVINSI DUPLICATE</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">KABUPATEN DUPLICATE</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">KECAMATAN DUPLICATE</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">KELURAHAN DUPLICATE</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">TANGGAL REKAM DUPLICATE</th>
                                                
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">NAMA SINGLE</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">ALAMAT SINGLE</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">STATUS KTP SINGLE</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">PROVINSI SINGLE</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">KABUPATEN SINGLE</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">KECAMATAN SINGLE</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">KELURAHAN SINGLE</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">TANGGAL REKAM SINGLE</th>
                                            </tr>
                                        </thead>
                                        <tbody id='my_data' style="border: 1px solid #e4e7ea;">
                                             <?php
                                        if (!empty($data_duplicate)){
                                            $i = 0;
                                           foreach($data_duplicate as $row){
                                            $i++;
                                            ?>
                                            <tr style="border: 1px solid #e4e7ea;">
                                                <td valign="center" style="border: 1px solid #e4e7ea;"><?php echo $i ;?></td>
                                                <td valign="center" style="border: 1px solid #e4e7ea;"><span style="color: #fff">,</span><span style="color: #4b8df8 !important"><?php echo $row->nik ;?></span></td>
                                                <td valign="center" style="border: 1px solid #e4e7ea;"><span style="color: #fff !important">,</span><span style="color: #cc1d1d !important"><?php echo $row->nik_single ;?></span></td>
                                                <td valign="center" style="border: 1px solid #e4e7ea;"><?php echo $row->nama_lgkp ;?></td>
                                                <td valign="center" style="border: 1px solid #e4e7ea;"><?php echo $row->alamat ;?></td>
                                                <td valign="center" style="border: 1px solid #e4e7ea;"><?php echo $row->current_status_code ;?></td>
                                                <td valign="center" style="border: 1px solid #e4e7ea;"><?php echo $row->nama_prop ;?></td>
                                                <td valign="center" style="border: 1px solid #e4e7ea;"><?php echo $row->nama_kab ;?></td>
                                                <td valign="center" style="border: 1px solid #e4e7ea;"><?php echo $row->nama_kec ;?></td>
                                                <td valign="center" style="border: 1px solid #e4e7ea;"><?php echo $row->nama_kel ;?></td>
                                                <td valign="center" style="border: 1px solid #e4e7ea;"><?php echo $row->tgl_rekam ;?></td>
                                                <td valign="center" style="border: 1px solid #e4e7ea;"><?php echo $row->nama_lgkp_single ;?></td>
                                                <td valign="center" style="border: 1px solid #e4e7ea;"><?php echo $row->alamat_single ;?></td>
                                                <td valign="center" style="border: 1px solid #e4e7ea;"><?php echo $row->current_status_code_single ;?></td>
                                                <td valign="center" style="border: 1px solid #e4e7ea;"><?php echo $row->nama_prop_single ;?></td>
                                                <td valign="center" style="border: 1px solid #e4e7ea;"><?php echo $row->nama_kab_single ;?></td>
                                                <td valign="center" style="border: 1px solid #e4e7ea;"><?php echo $row->nama_kec_single ;?></td>
                                                <td valign="center" style="border: 1px solid #e4e7ea;"><?php echo $row->nama_kel_single ;?></td>
                                                <td valign="center" style="border: 1px solid #e4e7ea;"><?php echo $row->tgl_rekam_single ;?></td>
                                                
                                            </tr>

                                            <?php } }else{ ?>
                                            
                                                <tr>
                                                <td colspan="19" style="text-align: center;">Apabila Data Duplicate Tidak Di Temukan, Silahkan Hubungi HelpDesk :))</td>
                                            </tr>
                                            <?php }?>

                                        </tbody>
                                        <tfoot>
                                            <tr style="border: 0px solid black;">
                                                
                                                <td colspan="19" style="border: 0px solid black;">
                                                    <div class="text-left">
                                                        <ul class="pagination"> </ul>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tfoot>
                                        <tfoot id="my_foot">
                                            
                                                <?php if (!empty($jumlah)){ ?>
                                                <tr>
                                                <th colspan="19" style="text-align: left;">Total : <?php echo number_format(htmlentities($jumlah),0,',','.');?> Data</th>
                                                </tr>
                                                <?php }else{ ?>
                                                <tr>
                                                <th colspan="19" style="text-align: left;">Total : 0 Data</th>
                                                </tr>
                                                <?php } ?>
                                            </tfoot>

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php }?>
                 </div>
       @include('shared.footer_detail')