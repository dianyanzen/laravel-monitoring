 <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $stitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                        <ol class="breadcrumb">
                           <li class="text-info"><?php echo $stitle; ?></li>
                        </ol>
                    </div>
                </div>
                 <div class="row">
     
                <form name ="get_form" action="<?php echo $my_url; ?>" method="post">
                    @csrf
                    <div class="col-sm-12">
                        <div class="white-box">
                            
                              
                                <?php if ($my_url != 'Push'){ ?>
                                    <div class="row">
                                <div class="col-lg-3">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_kec" type="checkbox" checked disabled="true">
                                            <label for="cb_kec"> Kecamatan</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3">         
                                    <div class="form-group" <?php if (!empty($is_colnotwil)){echo 'style="display: none!important;"'; }?>>
                                        <select class="form-control select2" name="no_kec" id="no_kec">
                                         <option  value="0">-- Pilih Kecamatan --</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-3">         
                                    <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_kel" type="checkbox" checked disabled="true">
                                            <label for="cb_kel"> Kelurahan</label>
                                        </div>
                                </div>
                                <div class="col-lg-3">         
                                    <div class="form-group" <?php if (!empty($is_colnotwil)){echo 'style="display: none!important;"'; }?>>
                                        <select class="form-control select2" name="no_kel" id="no_kel">
                                        <option value="0">-- Pilih Kelurahan --</option>  
                                    </select>
                           
                                    </div>
                                </div>
                            </div>
                                <div class="row">
                                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_nik" type="checkbox" checked disabled="true">
                                            <label for="cb_nik"> Nik</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">         
                                    <div class="form-group">
                                        <input class="form-control" type="text" id="nik" name="nik" onkeypress="return isNumberKey(event)" onchange="onlyNum()" /></div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_nik" type="checkbox" checked disabled="true">
                                            <label for="cb_nik"> No KK</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">         
                                    <div class="form-group">
                                        <input class="form-control" type="text" id="no_kk" name="no_kk" onkeypress="return isNumberKey(event)" onchange="onlyNum()" /></div>
                                </div>
                                </div>
                                <div class="row">
                                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_nik" type="checkbox" checked disabled="true">
                                            <label for="cb_nik"> Nama Lengkap</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">         
                                    <div class="form-group">
                                        <input class="form-control" type="text" id="nama_lgkp" name="nama_lgkp" /></div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_tgl_lhr" type="checkbox" checked disabled="true">
                                            <label for="cb_tgl_lhr"> Tanggal Lahir</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">         
                                    <div class="form-group">
                                         <input type="text" name="tgl_lhr" class="form-control" id="tgl_lhr" value="" placeholder="DD-MM-YYYY" readonly></div>
                                </div>
                                </div>
                                <?php } ?>
                                
                            
                           <?php if ($my_url == 'Push'){ ?>
                            <?php if (!empty($data)){ ?>
                            <div class="row">
                                  <button type="button" class="btn btn-info waves-effect waves-light m-r-10 pull-right" id="btn-reset" onclick="openTab('{{ url('/') }}/Check/Ktpel');">Back <i class="mdi  mdi-backspace fa-fw"></i></button>
                                  
                            </div>
                            <?php }else{ ?>
                            <div class="row">
                                <button type="submit" name="do_search" class="btn btn-info waves-effect waves-light m-r-10 pull-right" id="btn-filter" onclick="on_serach();" >Push <i class="mdi  mdi-near-me fa-fw"></i></button>
                                   <button type="button" class="btn btn-info waves-effect waves-light m-r-10 pull-right" id="btn-reset" onclick="openTab('{{ url('/') }}/Check/Ktpel');">Back <i class="mdi  mdi-backspace fa-fw"></i></button>
                                  
                            </div>
                            <?php } ?> 
                           <?php }else{ ?>
                           <div class="row">
                                <button type="submit" name="do_search" class="btn btn-info waves-effect waves-light m-r-10 pull-right" id="btn-filter" onclick="on_serach();" >Search <i class="mdi  mdi-magnify fa-fw"></i></button>
                                  <button type="button" class="btn btn-invert waves-effect waves-light m-r-10 pull-right" id="btn-reset" onclick="on_clear();">Clear <i class="mdi  mdi-delete fa-fw"></i></button>
                                  
                            </div>
                           <?php } ?>
                            
                        </div>
                    </div>
                    </form>
                </div>
                 <?php if ($my_url == 'Push'){ ?>
                            <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">Status Yang Dapat Di Push Ulang Oleh Monitoring</div>
                            <div class="panel-wrapper collapse in">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th class="text-center">#</th>
                                            <th>Status Code</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td align="center">1</td>
                                            <td>ENROLL_FAILURE_AT_REGIONAL</td>
                                        </tr>
                                        <tr>
                                            <td align="center">2</td>
                                            <td>PACKET_RETRY</td>
                                        </tr>
                                        <tr>
                                            <td align="center">3</td>
                                            <td>RECEIVED_AT_CENTRAL</td>
                                        </tr>
                                         <tr>
                                            <td align="center">4</td>
                                            <td><b style="font-weight:bold !important; color:#292961;">PROCESSING *</b></td>
                                        </tr>
                                         <tr>
                                            <td align="center">5</td>
                                            <td><b style="font-weight:bold !important; color:#292961;">SENT_FOR_ENROLLMENT *</b></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">Apabila Status E-Ktp Kembali Seperti Awal, Coba Lagi Next Time</div>
                            <div class="panel-wrapper collapse in">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th class="text-center">#</th>
                                            <th>Status Code</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td align="center">6</td>
                                            <td><b style="font-weight:bold !important; color:#292961;">ENROLL_FAILURE_AT_CENTRAL *</b></td>
                                        </tr>
                                        <tr>
                                            <td align="center">7</td>
                                            <td>SEARCH_FAILURE_AT_CENTRAL</td>
                                        </tr>
                                        <tr>
                                            <td align="center">8</td>
                                            <td><b style="font-weight:bold !important; color:#292961;">ADJUDICATE_RECORD *</b></td>
                                        </tr>
                                         <tr>
                                            <td align="center">9</td>
                                            <td>ADJUDICATE_IN_PROCESS</td>
                                        </tr>
                                         <tr>
                                            <td align="center">10</td>
                                            <td>SENT_FOR_DEDUP</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                            </div>
                            <?php } ?>
                <div class="row">
                <div class="col-md-12">
                        <div class="white-box">
                            <h3 class="box-title"><?php echo $stitle; ?></h3>
                            <?php if (!empty($data)){ 
                                $niks = array();
                                foreach($data as $row){
                                    $niks[] = $row->nik;
                                    }
                                ?>
                                <div class="row">
                                <div class="col-md-4">
                                <div class="button-box">
                                        <button type="submit" onclick="do_export();" class="btn btn-info waves-effect waves-light"> <i class="mdi mdi-file-excel-box m-r-5" ></i> <span>Export</span></button>
                                    <!-- 
                                        <button type="submit" onclick="do_pdf();" class="btn btn-info waves-effect waves-light"> <i class="mdi mdi-file-excel-box m-r-5"></i> <span>PPDB PDF</span></button> -->
                                    
                                    <form target="_blank" id="export_data" name ="get_form" action="<?php echo $my_url; ?>/Export" method="post">
                                        @csrf
                                        <input type="hidden" name="niks" id="export_niks" value="<?php echo implode(',',$niks); ?>">
                                    </form>
                                    <form target="_blank" id="pdf_data" name ="get_form" action="<?php echo $my_url; ?>/Pdf" method="get">
                                        <input type="hidden" id="pdf_niks" name="niks" value="<?php echo implode(',',$niks); ?>">
                                    </form>
                                </div>
                                </div>
                                </div>
                            <?php } ?>
                            <div class="scrollable">
                                <div class="table-responsive">
                                    <table id="demo-foo-row-toggler" class="table m-t-30 table-hover contact-list color-table info-table" data-page-size="100" style="margin-left: 1px; margin-right: 1px;">
                                        <thead>
                                            <tr>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">NO</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">NAMA </th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">NIK</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">LAHIR</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">STATUS KTP</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">CETAK KTP</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">DISTRIBUSI KTP</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">PENGAJUAN</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">NOMOR KK</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">NAMA KEC</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">NAMA KEL</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">ALAMAT</th>
                                            </tr>
                                        </thead>
                                        <tbody id='my_data' style="border: 1px solid #e4e7ea;">
                                             <?php
                                        if (!empty($data)){
                                            $i = 0;
                                           foreach($data as $row){
                                            $i++;
                                            ?>
                                            <tr style="border: 1px solid #e4e7ea;">
                                                <td valign="center" style="border: 1px solid #e4e7ea;"><?php echo $i ;?></td>
                                                <td valign="center" style="border: 1px solid #e4e7ea;">
                                                   
                                                            <img src='http://10.32.73.222:8080/Siak/<?php echo $row->path ;?>/<?php echo $row->nik ;?>.jpg'  alt="" width="36" class="img-circle">
                                                          <?php echo $row->nama_lgkp ;?>
                                                </td>
                                                <td valign="center" style="border: 1px solid #e4e7ea; text-align: center;"><?php echo $row->nik ;?>
                                                     
                                                </td>
                                                <td valign="center" style="border: 1px solid #e4e7ea; text-align: center;"><?php echo $row->tmpt_lhr ;?><br><?php echo $row->tgl_lhr ;?>
                                                     
                                                </td>
                                                
                                                <td valign="center" style="border: 1px solid #e4e7ea; text-align: center;"><?php echo $row->current_status_code ;?><br />
                                                <?php if ($row->current_status_code == 'ENROLL_FAILURE_AT_REGIONAL' || $row->current_status_code == 'PACKET_RETRY' || $row->current_status_code == 'RECEIVED_AT_CENTRAL' || $row->current_status_code == 'PROCESSING' || $row->current_status_code == 'SENT_FOR_ENROLLMENT' || $row->current_status_code == 'ENROLL_FAILURE_AT_CENTRAL' || $row->current_status_code == 'SEARCH_FAILURE_AT_CENTRAL' || $row->current_status_code == 'ADJUDICATE_RECORD' || $row->current_status_code == 'ADJUDICATE_IN_PROCESS' || $row->current_status_code == 'SENT_FOR_DEDUP'){ ?>
                                                <?php if ($my_url != 'Push'){ ?>
                                                <?php if (!empty($data)){ ?>
                                                <button type="button" class="btn btn-info btn-xs" id="btn-repush" onclick="openInNewTab('{{ url('/') }}/Check/Ktpel/Push?nik=<?php echo $row->nik ;?>');" style="margin-top: 5px;">Re-Push <i class="mdi  mdi-near-me fa-fw"></i></button>
                                                <?php } ?>
                                                <?php } ?>
                                                <?php } ?>
                                                </td>
                                                <td valign="center" style="border: 1px solid #e4e7ea;">
                                                    <i class="mdi mdi-account-circle text-white" style="float: none; color: #292961 !important;"></i><span style="float: none; color: #292961 !important;"> {{ ($row->ktp_by == '32730000yanzen' || $row->ktp_by == '32730000dian') ? '32730000dns' : $row->ktp_by }}</span><br>
                                                    <i class="mdi mdi-label text-white" style="float: none; color: #292961 !important;"></i><span style="float: none; color: #292961 !important;"> <?php echo $row->ktp_dt ;?></span><br>
                                                    <?php if ($row->jumlah_cetak != 0){ ?>
                                                    <i class="mdi mdi-printer-alert text-white" style="float: none; color: #292961 !important;"></i><span style="float: none; color: #292961 !important;"> Dicetak Sebanyak <?php echo $row->jumlah_cetak ;?> Kali</span>
                                                    <?php } ?>
                                                </td>
                                                <td valign="center" style="border: 1px solid #e4e7ea;">
                                                    <i class="mdi mdi-account-circle text-white" style="float: none; color: #292961 !important;"></i><span style="float: none; color: #292961 !important;"> <?php echo $row->dis_by ;?></span><br>
                                                    <i class="mdi mdi-label text-white" style="float: none; color: #292961 !important;"></i><span style="float: none; color: #292961 !important;"> <?php echo $row->dis_dt ;?></span>
                                                </td>
                                                <td valign="center" style="border: 1px solid #e4e7ea;">
                                                    <i class="mdi mdi-account-circle text-white" style="float: none; color: #292961 !important;"></i><span style="float: none; color: #292961 !important;"> <?php echo $row->req_by ;?></span><br>
                                                    <i class="mdi mdi-label text-white" style="float: none; color: #292961 !important;"></i><span style="float: none; color: #292961 !important;"> <?php echo $row->req_date ;?></span><br>
                                                    <i class="mdi mdi-verified text-white" style="float: none; color: #292961 !important;"></i><span style="float: none; color: #292961 !important;"> <?php echo $row->proc_by ;?></span><br>
                                                    <i class="mdi mdi-spotify text-white" style="float: none; color: #292961 !important;"></i><span style="float: none; color: #292961 !important;"> <?php echo $row->step_proc ;?></span>
                                                </td>
                                                <td valign="center" style="border: 1px solid #e4e7ea; text-align: center;"><?php echo $row->no_kk ;?></td>
                                                <td valign="center" style="border: 1px solid #e4e7ea;"><?php echo $row->nama_kec ;?></td>
                                                <td valign="center" style="border: 1px solid #e4e7ea;"><?php echo $row->nama_kel ;?></td>
                                                <td valign="center" style="border: 1px solid #e4e7ea;"><?php echo $row->alamat ;?><br>RT. <?php echo $row->rt ;?> RW. <?php echo $row->rw ;?><br>KODE POS : <?php echo $row->kode_pos ;?></td>
                                            </tr>

                                            <?php } }else{ ?>
                                            
                                                <tr>
                                                <td colspan="12" style="text-align: center;">No data available</td>
                                            </tr>
                                            <?php }?>

                                        </tbody>
                                        <tfoot>
                                            <tr style="border: 0px solid black;">
                                                
                                                <td colspan="12" style="border: 0px solid black;">
                                                    <div class="text-left">
                                                        <ul class="pagination"> </ul>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tfoot>
                                        <tfoot id="my_foot">
                                            
                                                <?php if (!empty($data)){ ?>
                                                <tr>
                                                <th colspan="12" style="text-align: left;">Total : <?php echo $i ;?> Data</th>
                                                </tr>
                                                <?php }else{ ?>
                                                <tr>
                                                <th colspan="12" style="text-align: left;">Total : 0 Data</th>
                                                </tr>
                                                <?php } ?>
                                            </tfoot>

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                 </div>
       @include('shared.footer_detail')

       