<style type="text/css">
    .notiv_mini {
        text-align: center!important;
        background-color: #06d755!important;
        color: #fff!important;
        font-weight: 600!important;
        font-size: 12px!important;
        border-radius: 600px!important;
        padding: 4px 7px 3px!important;
    }
</style>
<div id="page-wrapper">
            <div class="container-fluid">
               <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $mtitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                
                        <ol class="breadcrumb">
                            <li class="active"><?php echo $mtitle; ?></li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>

                <!-- .chat-row -->
                <div class="chat-main-box">
                    <!-- .chat-left-panel -->
                    <div class="chat-left-aside">
                        <div class="open-panel"><i class="ti-angle-right"></i></div>
                        <div class="chat-left-inner">
                            <ul class="chatonline style-none" id="kontak">
                                
                                
                                <li class="p-20"></li>
                            </ul>
                        </div>
                    </div>
                    <!-- .chat-left-panel -->
                    <!-- .chat-right-panel -->
                    <div class="chat-right-aside">
                        <div class="chat-main-header">
                            <div class="p-20 b-b">
                                <h3 class="box-title"><img  id="chat_image" src="" alt="user-img" onerror="this.onerror=null;this.src='{{ url('/') }}//assets/plugins/images/calming-cat.gif';" class="img-circle" style="width: 40px;display: none" > 
                                    <span id="chat_title">Chat Message</h3> </div>
                        </div>
                        <div class="chat-box">
                            <ul class="chat-list slimscroll p-t-30" id="pesan">
                            </ul>
                            <div class="row send-chat-box">
                                <div class="col-sm-12">
                                    <textarea class="form-control" placeholder="Type your message" id="isi_pesan"></textarea>
                                    <div class="custom-send">
                                        <input type="hidden" name="to_id" id="to_id">
                                        <button class="btn btn-danger btn-rounded" onclick="kirim_pesan();" type="button">Send</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- .chat-right-panel -->
                </div>
                <!-- /.chat-row -->
               
               

            </div>
       @include('shared.footer_detail')