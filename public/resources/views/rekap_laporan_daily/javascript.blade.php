 <script>
   function lihat(data) {
            block_screen();
            var url = "{{ url('/') }}/";
            url += "Report/LaporanDaily/Lihat/";
            url += data;
            var win = window.location.replace(url);
            win.focus();
        }
    function on_clear() {
        $('#my_data').empty();
        $('#my_data').append('<tr><td colspan="4" style="text-align: center;" valign="center">No data available in table</td></td>');
        $('#my_foot').html('<tr><th width="80%" colspan="3" style="text-align: center;">Jumlah</th><th width="10%" style="text-align: right;">0</th></td>');
        $('select[name="no_kec"]').val("0").trigger("change");
        $('select[name="no_kel"]').empty();
        $('select[name="no_kel"]').append('<option value="0">-- Pilih Kelurahan --</option>');
        $('select[name="no_kel"]').val("0").trigger("change");
    }
    function on_serach(){
        $('#my_data').html('<tr><td colspan="4" style="text-align: center;" valign="center">Waiting For Generate Data</td></td>');
        $('#my_foot').html('<tr><th width="80%" colspan="3" style="text-align: center;">Jumlah</th><th width="10%" style="text-align: right;">0</th></td>');
        $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff'
            },
            baseZ: 2000
        }); 
    }
    jQuery('#tanggal').datepicker({
        format: 'mm-yyyy',
        viewMode : "months",
        minViewMode : "months",               
        autoclose: true,
        todayHighlight: true,
        });
    function on_back() {
            block_screen();
            var url = "{{ url('/') }}/";
            url += "Report/Absensi";
            var win = window.location.replace(url);
            win.focus();
        }  
    jQuery(document).ready(function() {
           get_table();
           $('#tanggal').on('change', function() {
            var tgl = $(this).val();
            get_table_tgl(tgl);
        });
        });
        $('#daily-list').DataTable({
         "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });

    function get_table() {
        $('#daily-list').DataTable({
                destroy: true,
                 "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                 "columnDefs": [
                    {"className": "dt-left", "targets": "_all"}
                  ],
                "pageLength" : 50,
                "ajax": {
                 "url": BASE_URL+"Report/get_rekap_activity",
                "type": "post",
                "data": {
                "_token": "{{ csrf_token() }}",
                "tanggal":  $("#tanggal").val(),
                }
                }
            });
    }
    function get_table_tgl(tgl) {
        $('#daily-list').DataTable({
                destroy: true,
                 "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                 "columnDefs": [
                    {"className": "dt-left", "targets": "_all"}
                  ],
                "pageLength" : 50,
                "ajax": {
                 "url": BASE_URL+"Report/get_rekap_activity",
                "type": "post",
                "data": {
                "_token": "{{ csrf_token() }}",
                "tanggal":  $("#tanggal").val(),
                }
                }
            });
    }
    function on_serach(user_id) {
        $('#daily-list').DataTable({
                destroy: true,
                 "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                 "columnDefs": [
                    {"className": "dt-left", "targets": "_all"}
                  ],
                "pageLength" : 50,
                "ajax": {
                 "url": BASE_URL+"Report/get_rekap_activity",
                "type": "post",
                "data": {
                "_token": "{{ csrf_token() }}",
                "tanggal":  $("#tanggal").val(),
            
                }
                }
            });
    }
    function block_screen(){
        
        $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff'
            },
            baseZ: 2000
        }); 
    }
    function unblock_screen(){
       $.unblockUI();
    }
    </script>