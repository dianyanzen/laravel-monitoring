<script>
        function on_back() {
            block_screen();
            var url = "{{ url('/') }}/";
            url += "Setting/Helpdesk";
            var win = window.location.replace(url);
            win.focus();
        }
        function on_save() {
            if (validationdaily()){
                do_save();
            }
        }
        function validationdaily() {
        var helpdesk_id = $("#helpdesk_id");
            if (helpdesk_id.val().length == 0) {                
                  swal("Warning!", "Helpdesk Id Cannot Be Empty !", "warning");  
                 return false;
            }
        var helpdesk_nm = $("#helpdesk_nm");
            if (helpdesk_nm.val().length == 0) {                
                  swal("Warning!", "Helpdesk Name Cannot Be Empty !", "warning");  
                 return false;
            }
            return true;
        }
        function do_save(){
            $.ajax({
                    type: "post",
                    url: BASE_URL+"Setting/update_helpdesk",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        helpdesk_id : $("#helpdesk_id").val(),
                        helpdesk_nm : $("#helpdesk_nm").val()

                    },
                    beforeSend:
                    function () {
                         block_screen();
                    },
                    success: function (data) {
                        console.log(data);
                        swal("Success!", data.message, "success");
                    },
                    error:
                    function (data) {
                        unblock_screen();
                        swal("Error!", "Ooops!, Please Try Again, Something Went Wrong", "error"); 

                    },
                    complete:
                    function (response) {
                        unblock_screen();
                        on_back();
                    }
                });
        }
        function block_screen(){
        
        $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff'
            },
            baseZ: 2000
        }); 
    }
    function unblock_screen(){
       $.unblockUI();
    }
    </script>