     <script>
        function on_back() {
            block_screen();
            var url = "{{ url('/') }}/";
            url += "Check/CekPetugasKhusus";
            var win = window.location.replace(url);
            win.focus();
        }
     
    
    function block_screen(){
        
        $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff'
            },
            baseZ: 2000
        }); 
    }
    function unblock_screen(){
       $.unblockUI();
    }
    </script>
   