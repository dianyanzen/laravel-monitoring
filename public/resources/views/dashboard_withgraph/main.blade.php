<!DOCTYPE html>
<html lang="en">
@include('shared.head')
<body class="fix-header">
    <div id="wrapper">
            @include('shared.nav_top')
            @include('shared/nav_side')
            @include('dashboard_withgraph/content')
    </div>
    @include('shared/footer')
    @include('dashboard_withgraph/javascript')
</body>
</html>