    <script src="{{ url('/') }}/assets/plugins/bower_components/owl.carousel/owl.carousel.min.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/owl.carousel/owl.custom.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/flot/jquery.flot.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/Minimal-Gauge-chart/js/cmGauge.js"></script>
    <script>
         $(document).ready(function () {
        //ktpel section
        var chart_ktpel = new CanvasJS.Chart("chart_ktpel", {
            title: {
                text: "Detail Status Rekam Ktp-El"
            },
            axisY: {
                title: "",
                suffix: ""
            },
            data: [{
                type: "column", 
                yValueFormatString: "#,### ",
                indexLabel: "{y}",
                dataPoints: [
                    { label: "Perekaman Ktp-El", y: 0 },
                    { label: "Pencetakan KTP-EL", y: 0 },
                    { label: "Print Ready Record", y: 0 },
                    { label: "Sent For Enrollment", y: 0 },
                    { label: "PR Suket", y: 0 },
                    { label: "Pengeluaran Blangko", y: 0 },
                    { label: "Sisa Blangko E-Ktp", y: 0 },
                    { label: "Duplicate Record", y: 0 },
                    
                ]
            }]
        });

        function updateChart_ktpel(data,jumlah,title_data) {
            var boilerColor, deltaY, yVal;
            var dps = chart_ktpel.options.data[0].dataPoints;
            dps[data] = {label: title_data, y: jumlah, color: '#292961'}; 
            chart_ktpel.options.data[0].dataPoints = dps; 
            chart_ktpel.render();
        };

       
        function get_data_ktpel(){
             $.ajax({
                type: "post",
                url: BASE_URL+"dashboard/get_dashboard_m1",
                data: {"_token": "{{ csrf_token() }}",},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                    updateChart_ktpel(0,parseInt(data.perekaman_today),"Perekaman Ktp-El");
                },
                error:
                function (data) {
                    updateChart_ktpel(0,0,"Perekaman Ktp-El");

                },
                complete:
                function (response) {
                   
                }
            });
             $.ajax({
                type: "post",
                url: BASE_URL+"dashboard/get_dashboard_m2",
                data: {"_token": "{{ csrf_token() }}",},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                     updateChart_ktpel(1,parseInt(data.pencetakan_today),"Pencetakan KTP-EL");
                },
                error:
                function (data) {
                    updateChart_ktpel(1,0,"Pencetakan KTP-EL");

                },
                complete:
                function (response) {
                   
                  
                }
            });
             $.ajax({
                type: "post",
                url: BASE_URL+"dashboard/get_dashboard_m3",
                data: {"_token": "{{ csrf_token() }}",},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                     updateChart_ktpel(2,parseInt(data.sisa_prr),"Print Ready Record");
                },
                error:
                function (data) {
                    updateChart_ktpel(2,0,"Print Ready Record");

                },
                complete:
                function (response) {
                   
                  
                }
            });
             $.ajax({
                type: "post",
                url: BASE_URL+"dashboard/get_dashboard_m4",
                data: {"_token": "{{ csrf_token() }}",},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                     updateChart_ktpel(3,parseInt(data.sisa_sfe),"Sent For Enrollment");
                },
                error:
                function (data) {
                    updateChart_ktpel(3,0,"Sent For Enrollment");

                },
                complete:
                function (response) {
                   
                  
                }
            });
             $.ajax({
                type: "post",
                url: BASE_URL+"dashboard/get_dashboard_m5",
                data: {"_token": "{{ csrf_token() }}",},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                     updateChart_ktpel(4,parseInt(data.sisa_suket),"PR Suket");
                },
                error:
                function (data) {
                    updateChart_ktpel(4,0,"PR Suket");

                },
                complete:
                function (response) {
                   
                  
                }
            });
             $.ajax({
                type: "post",
                url: BASE_URL+"dashboard/get_dashboard_m6",
                data: {"_token": "{{ csrf_token() }}",},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                     updateChart_ktpel(5,parseInt(data.blangko_out),"Sisa Blangko E-Ktp");
                },
                error:
                function (data) {
                    updateChart_ktpel(5,0,"Sisa Blangko E-Ktp");

                },
                complete:
                function (response) {
                   
                  
                }
            });
             $.ajax({
                type: "post",
                url: BASE_URL+"dashboard/get_dashboard_m7",
                data: {"_token": "{{ csrf_token() }}",},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                     updateChart_ktpel(6,parseInt(data.duplicate),"Duplicate Record");
                },
                error:
                function (data) {
                    updateChart_ktpel(6,0,"Duplicate Record");

                },
                complete:
                function (response) {
                   
                  
                }
            });
             $.ajax({
                type: "post",
                url: BASE_URL+"dashboard/get_dashboard_m8",
                data: {"_token": "{{ csrf_token() }}",},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                     updateChart_ktpel(7,parseInt(data.sisa_blangko),"Pengeluaran Blangko");
                },
                error:
                function (data) {
                    updateChart_ktpel(7,0,"Pengeluaran Blangko");

                },
                complete:
                function (response) {
                   
                  
                }
            });

        }
       
        get_data_ktpel();
        //dafduk section
         var chart_dafduk = new CanvasJS.Chart("chart_dafduk", {
            title: {
                text: "Detail Pelayanan Pendaftaran Penduduk"
            },
            axisY: {
                title: "",
                suffix: ""
            },
            data: [{
                type: "column", 
                yValueFormatString: "#,### ",
                indexLabel: "{y}",
                dataPoints: [
                    { label: "Pencetakan KK", y: 0 },
                    { label: "Pencetakan Kia", y: 0 },
                    { label: "Kepindahan Antar Kab", y: 0 },
                    { label: "Kepindahan Antar Kec", y: 0 },
                    { label: "Kepindahan Dalam Kec", y: 0 },
                    { label: "Kedatangan Antar Kab", y: 0 },
                    { label: "Kedatangan Antar Kec", y: 0 },
                    { label: "Kedatangan Dalam Kec", y: 0 },
                    { label: "Nik Baru", y: 0 },
                    
                ]
            }]
        });
            function updateChart_dafduk(data,jumlah,title_data) {
            var boilerColor, deltaY, yVal;
            var dps = chart_dafduk.options.data[0].dataPoints;
            dps[data] = {label: title_data, y: jumlah, color: '#292961'}; 
            chart_dafduk.options.data[0].dataPoints = dps; 
            chart_dafduk.render();
        };
        function get_data_dafduk(){
            $.ajax({
                type: "post",
                url: BASE_URL+"dashboard/get_dashboard_kk",
                data: {"_token": "{{ csrf_token() }}",},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                            updateChart_dafduk(0,parseInt(data.pen_kk),"Pencetakan KK");
                },
                error:
                function (data) {
                            updateChart_dafduk(0,0,"Pencetakan KK");

                },
                complete:
                function (response) {
                   
                }
            });
            $.ajax({
                type: "post",
                url: BASE_URL+"dashboard/get_dashboard_kia",
                data: {"_token": "{{ csrf_token() }}",},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                            updateChart_dafduk(1,parseInt(data.pen_kia),"Pencetakan Kia");
                },
                error:
                function (data) {
                            updateChart_dafduk(1,0,"Pencetakan Kia");

                },
                complete:
                function (response) {
                   
                }
            });
            $.ajax({
                type: "post",
                url: BASE_URL+"dashboard/get_dashboard_pdh_akab",
                data: {"_token": "{{ csrf_token() }}",},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                            updateChart_dafduk(2,parseInt(data.pen_pindah_akab),"Kepindahan Antar Kab");
                },
                error:
                function (data) {
                            updateChart_dafduk(2,0,"Kepindahan Antar Kab");

                },
                complete:
                function (response) {
                   
                }
            });
            $.ajax({
                type: "post",
                url: BASE_URL+"dashboard/get_dashboard_pdh_akec",
                data: {"_token": "{{ csrf_token() }}",},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                            updateChart_dafduk(3,parseInt(data.pen_pindah_akec),"Kepindahan Antar Kec");
                },
                error:
                function (data) {
                            updateChart_dafduk(3,0,"Kepindahan Antar Kec");

                },
                complete:
                function (response) {
                   
                }
            });
            $.ajax({
                type: "post",
                url: BASE_URL+"dashboard/get_dashboard_pdh_dkec",
                data: {"_token": "{{ csrf_token() }}",},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                            updateChart_dafduk(4,parseInt(data.pen_pindah_dkec),"Kepindahan Dalam Kec");
                },
                error:
                function (data) {
                            updateChart_dafduk(4,0,"Kepindahan Dalam Kec");

                },
                complete:
                function (response) {
                   
                }
            });
            $.ajax({
                type: "post",
                url: BASE_URL+"dashboard/get_dashboard_dtg_akab",
                data: {"_token": "{{ csrf_token() }}",},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                            updateChart_dafduk(5,parseInt(data.pen_datang_akab),"Kedatangan Antar Kab");
                },
                error:
                function (data) {
                            updateChart_dafduk(5,0,"Kedatangan Antar Kab");

                },
                complete:
                function (response) {
                   
                }
            });
            $.ajax({
                type: "post",
                url: BASE_URL+"dashboard/get_dashboard_dtg_akec",
                data: {"_token": "{{ csrf_token() }}",},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                            updateChart_dafduk(6,parseInt(data.pen_datang_akec),"Kedatangan Antar Kec");
                },
                error:
                function (data) {
                            updateChart_dafduk(6,0,"Kedatangan Antar Kec");

                },
                complete:
                function (response) {
                   
                }
            });
            $.ajax({
                type: "post",
                url: BASE_URL+"dashboard/get_dashboard_dtg_dkec",
                data: {"_token": "{{ csrf_token() }}",},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                            updateChart_dafduk(7,parseInt(data.pen_datang_dkec),"Kedatangan Dalam Kec");
                },
                error:
                function (data) {
                            updateChart_dafduk(7,0,"Kedatangan Dalam Kec");

                },
                complete:
                function (response) {
                   
                }
            });
            $.ajax({
                type: "post",
                url: BASE_URL+"dashboard/get_dashboard_nik",
                data: {"_token": "{{ csrf_token() }}",},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                            updateChart_dafduk(8,parseInt(data.pen_nik_baru),"Nik Baru");
                },
                error:
                function (data) {
                            updateChart_dafduk(8,0,"Nik Baru");

                },
                complete:
                function (response) {
                   
                }
            });
        }
         get_data_dafduk();
         //capil section
         var chart_capil = new CanvasJS.Chart("chart_capil", {
            title: {
                text: "Detail Pelayanan Pencatatan Sipil"
            },
            axisY: {
                title: "",
                suffix: ""
            },
            data: [{
                type: "column", 
                yValueFormatString: "#,### ",
                indexLabel: "{y}",
                dataPoints: [
                    { label: "Akta Kelahiran Umum", y: 0 },
                    { label: "Akta Kelahiran Terlambat", y: 0 },
                    { label: "Akta Kematian", y: 0 },
                    { label: "Akta Perkawinan", y: 0 },
                    { label: "Akta Perceraian", y: 0 },
                    
                    
                ]
            }]
        });
            function updateChart_capil(data,jumlah,title_data) {
            var boilerColor, deltaY, yVal;
            var dps = chart_capil.options.data[0].dataPoints;
            dps[data] = {label: title_data, y: jumlah, color: '#292961'}; 
            chart_capil.options.data[0].dataPoints = dps; 
            chart_capil.render();
        };
        function get_data_capil(){
            $.ajax({
                type: "post",
                url: BASE_URL+"dashboard/get_dashboard_akta_lu",
                data: {"_token": "{{ csrf_token() }}",},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                            updateChart_capil(0,parseInt(data.pen_lahir_lu),"Akta Kelahiran Umum");
                },
                error:
                function (data) {
                            updateChart_capil(0,0,"Akta Kelahiran Umum");

                },
                complete:
                function (response) {
                   
                }
            });
            $.ajax({
                type: "post",
                url: BASE_URL+"dashboard/get_dashboard_akta_lt",
                data: {"_token": "{{ csrf_token() }}",},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                            updateChart_capil(1,parseInt(data.pen_lahir_lt),"Akta Kelahiran Terlambat");
                },
                error:
                function (data) {


                },
                complete:
                function (response) {
                   
                }
            });
            $.ajax({
                type: "post",
                url: BASE_URL+"dashboard/get_dashboard_akta_mt",
                data: {"_token": "{{ csrf_token() }}",},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                            updateChart_capil(2,parseInt(data.pen_mati),"KepindahanAkta Kematian");
                },
                error:
                function (data) {


                },
                complete:
                function (response) {
                   
                }
            });
            $.ajax({
                type: "post",
                url: BASE_URL+"dashboard/get_dashboard_akta_kwn",
                data: {"_token": "{{ csrf_token() }}",},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                            updateChart_capil(3,parseInt(data.pen_kawin),"Akta Perkawinan");
                },
                error:
                function (data) {


                },
                complete:
                function (response) {
                   
                }
            });
            $.ajax({
                type: "post",
                url: BASE_URL+"dashboard/get_dashboard_akta_cry",
                data: {"_token": "{{ csrf_token() }}",},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                            updateChart_capil(4,parseInt(data.pen_cerai),"Akta Perceraian");
                },
                error:
                function (data) {

                },
                complete:
                function (response) {
                   
                }
            });
        }
         get_data_capil();
         var chart_piak = new CanvasJS.Chart("chart_piak", {
            title: {
                text: "Pengelola Informasi Administrasi Kependudukan"
            },
            axisY: {
                title: "",
                suffix: ""
            },
            data: [{
                type: "column", 
                yValueFormatString: "#,### ",
                indexLabel: "{y}",
                dataPoints: [
                    { label: "Perangkat Lunak", y: 0 },
                    { label: "Perangkat Keras", y: 0 },
                    { label: "Jarkomdat", y: 0 },
                    { label: "Perangkat Pendukung", y: 0 },
                    { label: "Hak Akses Siak", y: 0 },
                    { label: "Hak Akses Bcard", y: 0 },
                    { label: "Hak Akses Benroll", y: 0 },
                    
                    
                ]
            }]
        });
            function updateChart_piak(data,jumlah,title_data) {
            var boilerColor, deltaY, yVal;
            var dps = chart_piak.options.data[0].dataPoints;
            dps[data] = {label: title_data, y: jumlah, color: '#292961'}; 
            chart_piak.options.data[0].dataPoints = dps; 
            chart_piak.render();
        };
         updateChart_piak(0,0,"Perangkat Lunak");
         updateChart_piak(1,0,"Perangkat Keras");
         updateChart_piak(2,0,"Jarkomdat");
         updateChart_piak(3,0,"Perangkat Pendukung");
         updateChart_piak(4,0,"Hak Akses Siak");
         updateChart_piak(5,0,"Hak Akses Bcard");
         updateChart_piak(5,0,"Hak Akses Benroll");
        get_data_piak();
        });
       
        </script>
    <script type="text/javascript">
    $(document).ready(function () {
    var interval = setInterval(function() {
        var momentNow = moment();
        $('#date-part').html(momentNow.format('DD-MM-YYYY'));
        $('#time-part').html(momentNow.format('HH:mm:ss'));
    }, 100);
       
    });
   function do_ktpl(){
            window.location.href = "{{ url('/') }}/Dashboard_ktpel";
    }
    function do_bio(){
            window.location.href = "{{ url('/') }}/Dashboard_biodata";
    }
    function do_mobi(){
            window.location.href = "{{ url('/') }}/Dashboard_mobilitas";
    }
    function do_capil(){
            window.location.href = "{{ url('/') }}/Dashboard_capil";
    }
    function get_data_m1() {
       
        }
    </script>   
    