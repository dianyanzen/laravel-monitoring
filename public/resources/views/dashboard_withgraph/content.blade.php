 <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $stitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                        <ol class="breadcrumb">
                           <li class="text-info"><?php echo $stitle; ?></li>
                        </ol>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-lg-12">
                        <div class="white-box p-b-0">
                            <div class="row">
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12" >
                                    <h2 class="font-medium m-t-0">Dashboard Ktp-El</h2>
                                    <h5 class="text-muted m-t-0">Tanggal <span id="date-part"></span> <span id="time-part"></span></h5>
                                </div>
                            </div>
                           <div id="chart_ktpel" style="height: 350px; max-width: auto; margin: 0px auto;"></div>
                           
                            <div class="row" style="margin-top: 10px !important">
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12" >
                                    <h2 class="font-medium m-t-0">Dashboard Dafduk</h2>
                                </div>
                            </div>
                            <div id="chart_dafduk" style="height: 350px; max-width: auto; margin: 0px auto;"></div>
                           
                     
                            <div class="row" style="margin-top: 10px !important">
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12" >
                                    <h2 class="font-medium m-t-0">Dashboard Capil</h2>
                                </div>
                            </div>
                           <div id="chart_capil" style="height: 350px; max-width: auto; margin: 0px auto;"></div>

                           <div class="row" style="margin-top: 10px !important">
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12" >
                                    <h2 class="font-medium m-t-0">Dashboard Piak</h2>
                                </div>
                            </div>
                           <div id="chart_piak" style="height: 350px; max-width: auto; margin: 0px auto;"></div>
                          
                        </div>
                    </div>
                 
                </div>
        
            
               
                 </div>
       @include('shared.footer_detail')