 <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $stitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                        <ol class="breadcrumb">
                           <li class="text-info"><?php echo $stitle; ?></li>
                        </ol>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                        <div class="white-box">
                            <div class="row">
                                <div class="col-xs-6">
                                    <h2 class="font-medium m-t-0">Pengecekan Log Antrian</h2>
                                </div>
                            </div>
                            <div class="row" style="margin-top:  5px">
                                <div class="col-md-6 col-lg-6 col-xs-12 col-sm-6">         
                                     <div class="form-group">
                                            <label for="antrian_nik">Nik</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control" id="antrian_nik" onkeypress="return isNumberKey(event)" maxlength="16"> 
                                                <div class="input-group-addon"><i class="ti-user"></i></div>
                                            </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-6 col-xs-12 col-sm-6">         
                                     <div class="form-group">
                                            <label for="antrian_tlp">Nomor Telepon : (+62)</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control" id="antrian_tlp" value="+62" onkeypress="return isNumberKey(event)"  maxlength="16"> 
                                                <div class="input-group-addon"><i class="ti-mobile"></i></div>
                                            </div>

                                    </div>
                                </div>
                        </div>
                        <div class="row">
                                <button type="button" class="btn btn-info waves-effect waves-light m-r-10 pull-right" id="btn-search" onclick="get_table();" >Tampilkan <i class="mdi mdi-magnify fa-fw"></i></button>
                                <button type="button" class="btn btn-invert waves-effect waves-light m-r-10 pull-right" id="btn-reset" onclick="on_clear();">Clear <i class="mdi  mdi-delete fa-fw"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                <div class="col-md-12">
                        <div class="white-box">
                            <h3 class="box-title"><?php echo $mtitle; ?></h3>
                            <div class="scrollable">
                                <div class="table-responsive">
                                    <table id="antrian-list" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                   <thead>
                                    
                                        <tr>
                                            <th width="10%" style="text-align: center;">No</th>
                                            <th width="15%" style="text-align: center;">Nik</th>
                                            <th width="15%" style="text-align: center;">Nama</th>
                                            <th width="15%" style="text-align: center;">Kode Pelayanan</th>
                                            <th width="10%" style="text-align: center;">Urutan</th>
                                            <th width="15%" style="text-align: center;">Tanggal</th>
                                            <th width="10%" style="text-align: center;">Jam</th>
                                            <th width="10%" style="text-align: center;">Hari</th>
                                            <th width="10%" style="text-align: center;">Telepon</th>

                                        </tr>
                                       
                                    </thead>
                                    <tbody id="show_data">
                                       
                                        
                                    </tbody>
                                    
                                </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
       @include('shared.footer_detail')