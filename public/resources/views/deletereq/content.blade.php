 <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $stitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                        <ol class="breadcrumb">
                           <li class="text-info"><?php echo $stitle; ?></li>
                        </ol>
                    </div>
                </div>
                 <div class="row">
                <form name ="get_form" action="<?php echo $my_url; ?>" method="post">
                    @csrf
                    <div class="col-sm-12">
                        <div class="white-box">
                            <div class="row">
                                <div class="col-lg-3">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_nik" type="checkbox" checked disabled="true">
                                            <label for="cb_nik"> Nik</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3">         
                                    <div class="form-group">
                                        <input class="form-control" type="text" id="nik" name="nik" onkeypress="return isNumberKey(event)" onchange="onlyNum()" /></div>
                                </div>
                            </div>
                            <div class="row">
                                <button type="submit" name="do_search" class="btn btn-info waves-effect waves-light m-r-10 pull-right" id="btn-filter" onclick="on_serach();" >Search <i class="mdi  mdi-magnify fa-fw"></i></button>
                                  <button type="button" class="btn btn-invert waves-effect waves-light m-r-10 pull-right" id="btn-reset" onclick="on_clear();">Clear <i class="mdi  mdi-delete fa-fw"></i></button>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
                <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">Cara Penghapusan Pengajuan Bermasalah</div>
                            <div class="panel-wrapper collapse in">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th class="text-center">#</th>
                                            <th>Langkah -Langkah</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td align="center">1</td>
                                            <td>Masukan Nik-Nik Yang Akan Dihapus (Bisa Mengguanakn Comma), Maksimal 1000 Nik Dalam Satu Kali Transaksi</td>
                                        </tr>
                                        <tr>
                                            <td align="center">2</td>
                                            <td>Pastikan Nik-Nik Yang Akan Dihapus Dari Pengajuan Adalah Nik Nik Yang Bermasalah</td>
                                        </tr>
                                        <tr>
                                            <td align="center">3</td>
                                            <td>Setelah Nik-Nik Yang Akan Dihapus Muncul Di Dalam Table Klik Tombol Hapus Pengajuan</td>
                                        </tr>
                                         <tr>
                                            <td align="center">4</td>
                                            <td><b style="font-weight:bold !important; color:#292961;">Monitoring Akan Menghapus Semua Nik Yang Ada Didalam Tabel Ini, Jadi Pastikan Nik Yang Muncul Di Tabel Benar Benar Layak Hapus</b></td>
                                        </tr>
                                         <tr>
                                            <td align="center">5</td>
                                            <td><b style="font-weight:bold !important; color:#292961;">Setelah Penghapusan Pengajuan Selesai Tabel Akan Kembali Kosong, Dan Dengan Otomatis Nik Nik Yang Tadi Ada Didalam Table, Sudah Terhapus Dari Pengajuan</b></td>
                                        </tr>
                                        <tr>
                                            <td align="center">6</td>
                                            <td><B>Sekian Dan Terima Kasih, Kalau Bingung Bisa Tanyakan Kepada Team IT</B></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                            </div>
                <div class="row">
                <div class="col-md-12">
                        <div class="white-box">
                            <h3 class="box-title"><?php echo $mtitle; ?></h3>
                            <?php if (!empty($data)){ 
                                $niks = array();
                                foreach($data as $row){
                                    $niks[] = $row->nik;
                                    }
                                ?>
                                <div class="row">
                                <div class="col-md-1">
                                <form  name ="get_form" action="Do_delete" method="post" >
                                    @csrf
                                    <input type="hidden" name="niks" value="<?php echo implode(',',$niks); ?>">
                                <button type="submit" class="btn btn-info waves-effect waves-light"> <i class="mdi mdi-delete-circle m-r-5"></i> <span>Hapus Pengajuan</span></button>
                                </form>
                                </div>
                                </div>
                            <?php } ?>
                            <div class="scrollable">
                                <div class="table-responsive">
                                    <table id="demo-foo-row-toggler" class="table m-t-30 table-hover contact-list color-table info-table" data-page-size="100" style="margin-left: 1px; margin-right: 1px;">
                                        <thead>
                                            <tr>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">NO</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">NIK</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">NAMA LENGKAP</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">STATUS KTP</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">REQ DATE</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">REQ BY</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">PRINT DATE</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">PRINT BY</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">STATUS PENGAJUAN</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">KETERANGAN PENGAJUAN</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">NAMA KECAMATAN</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">NAMA KELURAHAN</th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody id='my_data' style="border: 1px solid #e4e7ea;">
                                             <?php
                                        if (!empty($data)){
                                            $i = 0;
                                           foreach($data as $row){
                                            $i++;
                                            ?>
                                            <tr style="border: 1px solid #e4e7ea;">
                                                <td valign="center" style="border: 1px solid #e4e7ea;"><?php echo $i ;?></td>
                                                <td valign="center" style="border: 1px solid #e4e7ea;"><span style="color: #fff">,</span><?php echo $row->nik ;?></td>
                                                <td valign="center" style="border: 1px solid #e4e7ea;"><?php echo $row->nama_lengkap ;?></td>
                                                <td valign="center" style="border: 1px solid #e4e7ea;"><?php echo $row->current_status_code ;?></td>
                                                <td valign="center" style="border: 1px solid #e4e7ea;"><?php echo $row->req_date ;?></td>
                                                <td valign="center" style="border: 1px solid #e4e7ea;"><?php echo $row->req_by ;?></td>
                                                <td valign="center" style="border: 1px solid #e4e7ea;"><?php echo $row->printed_date ;?></td>
                                                <td valign="center" style="border: 1px solid #e4e7ea;"><?php echo $row->printed_by ;?></td>
                                                <td valign="center" style="border: 1px solid #e4e7ea;"><?php echo $row->status_pengajuan ;?></td>
                                                <td valign="center" style="border: 1px solid #e4e7ea;"><?php echo $row->keterangan_status ;?></td>
                                                <td valign="center" style="border: 1px solid #e4e7ea;"><?php echo $row->nama_kec ;?></td>
                                                <td valign="center" style="border: 1px solid #e4e7ea;"><?php echo $row->nama_kel ;?></td>
                                                
                                            </tr>

                                            <?php } }else{ ?>
                                            
                                                <tr>
                                                <td colspan="19" style="text-align: center;">Mohon Benar Benar Cek Terlebih Dahulu Sebelum Di Delete Dari Pengajuan :))</td>
                                            </tr>
                                            <?php }?>

                                        </tbody>
                                        <tfoot>
                                            <tr style="border: 0px solid black;">
                                                
                                                <td colspan="19" style="border: 0px solid black;">
                                                    <div class="text-left">
                                                        <ul class="pagination"> </ul>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tfoot>
                                        <tfoot id="my_foot">
                                            
                                                <?php if (!empty($data)){ ?>
                                                <tr>
                                                <th colspan="11" style="text-align: left;">Total : <?php echo number_format(htmlentities($i),0,',','.');?> Data</th>
                                                </tr>
                                                <?php }else{ ?>
                                                <tr>
                                                <th colspan="11" style="text-align: left;">Total : 0 Data</th>
                                                </tr>
                                                <?php } ?>
                                            </tfoot>

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                 </div>
       @include('shared.footer_detail')