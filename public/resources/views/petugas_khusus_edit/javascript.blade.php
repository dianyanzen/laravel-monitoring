     <script>
        function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }

     function on_edit(){
        if (validationdaily()){
          do_edit();
          // alert("Suceess");
        }
  
        
    }
    function validationdaily() {
        var ph = $("#txt_ph");
        var nik = $("#txt_nik");
        console.log(nik);
            if (nik.val().length != 16) {                
                  swal("Warning!", "Input Nik Harus 16 Digit !", "warning");  
                 return false;
            }
            if (ph.val().length == 0) {                
                  swal("Warning!", "Nomor Ph Tidak Boleh Kosong !", "warning");  
                 return false;
            }
           
          
            return true;
       
        }
        function do_edit(){
            $.ajax({
                    type: "post",
                    url: BASE_URL+"Ph/do_edit",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        ph : $("#txt_ph").val(),
                        nik : $("#txt_nik").val(),
                       
                    },
                    beforeSend:
                    function () {
                         block_screen();
                    },
                    success: function (data) {
                        console.log(data);
                        if (data.success){ 
                        swal("Success!", data.message, "success");
                        }else{
                        swal("Warning!", data.message, "warning");
                        }
                    },
                    error:
                    function (data) {
                        unblock_screen();
                        swal("Error!", "Ooops!, Please Try Again, Something Went Wrong", "error"); 

                    },
                    complete:
                    function (response) {
                        unblock_screen();
                        on_back();
                    }
                });
        }
    function on_back() {
            block_screen();
            var url = "{{ url('/') }}/";
            url += "Check/CekPetugasKhusus";
            var win = window.location.replace(url);
            win.focus();
        }
     
    
    function block_screen(){
        
        $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff'
            },
            baseZ: 2000
        }); 
    }
    function unblock_screen(){
       $.unblockUI();
    }
    </script>
   