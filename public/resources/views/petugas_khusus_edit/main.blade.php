<!DOCTYPE html>
<html lang="en">
@include('shared.head')
<body class="fix-header">
    <div id="wrapper">
            @include('shared.nav_top')
            @include('shared/nav_side')
            @include('petugas_khusus_edit/content')
    </div>
     
    @include('shared/footer')
    
    @include('petugas_khusus_edit/javascript')
</body>
</html>