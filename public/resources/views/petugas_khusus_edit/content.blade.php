 <style type="text/css">
     textarea {
  resize: none;
  height: 155px !important;
}
 </style>
 <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $stitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                        <ol class="breadcrumb">
                            <li class="active"><?php echo $mtitle; ?></li>
                        </ol>
                    </div>
                </div>
                 <div class="row">
                <!-- <form name ="get_form" action="<?php echo $my_url; ?>" method="post"> -->
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="white-box">
                            <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_ph" type="checkbox" checked disabled="true">
                                            <label for="cb_ph"> No Petugas Khusus</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">         
                                    <div class="form-group">
                                        <input class="form-control" id="txt_ph" type="text" value="<?php if (!empty($data)){echo $data[0]->no_ph; }?>"></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_nik" type="checkbox" checked disabled="true">
                                            <label for="cb_nik"> Nik</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">         
                                    <div class="form-group">
                                        <input class="form-control" id="txt_nik" type="text" maxlength="16" value="<?php if (!empty($data)){echo $data[0]->nik; }?>" readonly></div>
                                </div>
                            </div>
                            <div class="row">
                               
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_nama" type="checkbox" checked disabled="true" >
                                            <label for="cb_nama"> Nama</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">         
                                    <div class="form-group">
                                        <input class="form-control" id="txt_nama" type="text"  value="<?php if (!empty($data)){echo $data[0]->nama_lgkp; }?>" readonly></div>
                                </div>
                            </div>
                            <div class="row">
                               
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_tmpt_lhr" type="checkbox" checked disabled="true" >
                                            <label for="cb_tmpt_lhr"> Tempat Lahir</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">         
                                    <div class="form-group">
                                        <input class="form-control" id="txt_tmpt_lhr" type="text"  value="<?php if (!empty($data)){echo $data[0]->tmpt_lhr; }?>" readonly></div>
                                </div>
                            </div>
                            <div class="row">
                               
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_tgl_lhr" type="checkbox" checked disabled="true" >
                                            <label for="cb_tgl_lhr"> Tanggal Lahir</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">         
                                    <div class="form-group">
                                        <input class="form-control" id="txt_tgl_lhr" type="text"  value="<?php if (!empty($data)){echo $data[0]->tgl_lhr; }?>" readonly>
                                        <input class="form-control" id="txt_umur" type="text" style="display: none"></div>
                                </div>
                            </div>
                            
                            <div class="row">
                               
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_jenis_klmin" type="checkbox" checked disabled="true" >
                                            <label for="cb_jenis_klmin"> Jenis Kelamin</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">         
                                    <div class="form-group">
                                        <input class="form-control" id="txt_jenis_klmin" type="text"  value="<?php if (!empty($data)){echo $data[0]->jenis_klmin; }?>" readonly></div>
                                </div>
                            </div>


                            <div class="row">
                               
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_jenis_pkrjn" type="checkbox" checked disabled="true" >
                                            <label for="cb_jenis_pkrjn"> Jenis Pekerjaan</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">         
                                    <div class="form-group">
                                        <input class="form-control" id="txt_jenis_pkrjn" type="text"  value="<?php if (!empty($data)){echo $data[0]->jenis_pkrjn; }?>" readonly></div>
                                </div>
                            </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_prop" type="checkbox" checked disabled="true">
                                            <label for="cb_prop"> Provinsi</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">         
                                    <div class="form-group">
                                        <select class="form-control select2" name="no_prop" id="no_prop" disabled="true">
                                         <option  value="<?php if (!empty($data)){echo $data[0]->no_prop; }?>" selected><?php if (!empty($data)){echo $data[0]->nama_prop; }?></option>
                                        </select>
                                    </div>
                                </div>
                                </div>
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_kab" type="checkbox" checked disabled="true">
                                            <label for="cb_kab"> Kabupaten</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">         
                                    <div class="form-group">
                                        <select class="form-control select2" name="no_kab" id="no_kab" disabled="true">
                                         <option  value="<?php if (!empty($data)){echo $data[0]->no_kab; }?>" selected><?php if (!empty($data)){echo $data[0]->nama_kab; }?></option>
                                        </select>
                                    </div>
                                </div>
                                </div>
                                <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_kec" type="checkbox" checked disabled="true">
                                            <label for="cb_kec"> Kecamatan</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">         
                                    <div class="form-group">
                                        <select class="form-control select2" name="no_kec" id="no_kec" disabled="true">
                                         <option  value="<?php if (!empty($data)){echo $data[0]->no_kec; }?>" selected><?php if (!empty($data)){echo $data[0]->nama_kec; }?></option>
                                        </select>
                                    </div>
                                </div>
                                </div>
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_kel" type="checkbox" checked disabled="true">
                                            <label for="cb_kel"> Kelurahan</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">         
                                    <div class="form-group">
                                        <select class="form-control select2" name="no_kel" id="no_kel" disabled="true">
                                         <option  value="<?php if (!empty($data)){echo $data[0]->no_kel; }?>" selected><?php if (!empty($data)){echo $data[0]->nama_kel; }?></option>
                                        </select>
                                    </div>
                                </div>
                                </div>
                                <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_alamat" type="checkbox" checked disabled="true">
                                            <label for="cb_alamat"> Alamat</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">         
                                    <div class="form-group">
                                        <input class="form-control" id="txt_alamat" type="text" value="<?php if (!empty($data)){echo $data[0]->alamat; }?>" readonly></div>
                                </div>
                                </div>
                                <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_rt" type="checkbox" checked disabled="true">
                                            <label for="cb_rt"> RT</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">         
                                    <div class="form-group">
                                        <input class="form-control" id="txt_rt" type="text" value="<?php if (!empty($data)){echo $data[0]->rt; }?>" readonly></div>
                                </div>
                                </div>
                                <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_rw" type="checkbox" checked disabled="true">
                                            <label for="cb_rw"> RW</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">         
                                    <div class="form-group">
                                        <input class="form-control" id="txt_rw" type="text" value="<?php if (!empty($data)){echo $data[0]->rw; }?>" readonly></div>
                                </div>
                                </div>
                            </div>
                            </div>
                            <div class="row">
                                <button type="submit" id="btn_dosave" class="btn btn-info waves-effect waves-light m-r-10 pull-right" id="btn-filter" onclick="on_edit();" >Edit <i class="mdi mdi-lead-pencil fa-fw"></i></button>
                                  <button type="button" class="btn btn-invert waves-effect waves-light m-r-10 pull-right" id="btn-reset" onclick="on_back();">Back <i class="mdi  mdi-backspace fa-fw"></i></button>
                            </div>
                        
                        </div>
                    </div>
                    <!-- </form> -->
                
                </div>
              
                 </div>
       @include('shared.footer_detail')