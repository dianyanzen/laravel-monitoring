 <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $stitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                        <ol class="breadcrumb">
                           <li class="text-info"><?php echo $stitle; ?></li>
                        </ol>
                    </div>
                </div>
                 <div class="row">
                <form name ="get_form" action="<?php echo $my_url; ?>" method="post">
                    @csrf
                    <div class="col-sm-12">
                        <div class="white-box">
                            <div class="row">
                                <div class="col-lg-3">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_tgl" type="checkbox" checked disabled="true">
                                            <label for="cb_tgl"> Tanggal Request</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3">         
                                    <div class="form-group">
                                        <input class="form-control input-daterange-datepicker" type="text" id="tanggal" name="tanggal" /></div>
                                </div>
                            </div>
                           <div class="row">
                                <button type="submit" name="do_search" class="btn btn-info waves-effect waves-light m-r-10 pull-right" id="btn-filter" onclick="on_serach();" >Search <i class="mdi  mdi-magnify fa-fw"></i></button>
                                  <button type="button" class="btn btn-invert waves-effect waves-light m-r-10 pull-right" id="btn-reset" onclick="on_clear();">Clear <i class="mdi  mdi-delete fa-fw"></i></button>
                                  
                            </div>
                            
                        </div>
                    </div>
                    </form>
                </div>
                 
                <!-- <div class="row">
                <div class="col-md-12">
                        <div class="white-box">
                            <h3 class="box-title"><?php echo $stitle; ?></h3>
                            <div class="scrollable">
                                <div class="table-responsive">
                                    <table id="demo-foo-row-toggler" class="table m-t-30 table-hover contact-list color-table info-table" data-page-size="100" style="margin-left: 1px; margin-right: 1px;">
                                        <thead>
                                            <tr>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">NO</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">NAMA </th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">NIK</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">NO KEC</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">NAMA KEC</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">TANGGAL CETAK</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">REQUEST</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">RECEIVE</th>
                                            </tr>
                                        </thead>
                                        <tbody id='my_data' style="border: 1px solid #e4e7ea;">
                                             <?php
                                        if (!empty($data)){
                                            $i = 0;
                                           foreach($data as $row){
                                            $i++;
                                            ?>
                                            <tr style="border: 1px solid #e4e7ea;">
                                                <td valign="center" style="border: 1px solid #e4e7ea;"><?php echo $i ;?></td>
                                                <td valign="center" style="border: 1px solid #e4e7ea;"><?php echo $row->nama_lgkp ;?></td>
                                                <td valign="center" style="border: 1px solid #e4e7ea;"><?php echo $row->nik ;?></td>
                                                <td valign="center" style="border: 1px solid #e4e7ea;"><?php echo $row->no_kec ;?></td>
                                                <td valign="center" style="border: 1px solid #e4e7ea;"><?php echo $row->nama_kec ;?></td>
                                                <td valign="center" style="border: 1px solid #e4e7ea;"><?php echo $row->tgl_cetak ;?></td>
                                                <td valign="center" style="border: 1px solid #e4e7ea;"><?php echo $row->req_by ;?></td>
                                                <td valign="center" style="border: 1px solid #e4e7ea;"><?php echo $row->receive_by ;?></td>
                                            </tr>

                                            <?php } }else{ ?>
                                            
                                                <tr>
                                                <td colspan="10" style="text-align: center;">No data available</td>
                                            </tr>
                                            <?php }?>

                                        </tbody>
                                        <tfoot>
                                            <tr style="border: 0px solid black;">
                                                
                                                <td colspan="10" style="border: 0px solid black;">
                                                    <div class="text-left">
                                                        <ul class="pagination"> </ul>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tfoot>
                                        <tfoot id="my_foot">
                                            
                                                <?php if (!empty($jumlah)){ ?>
                                                <tr>
                                                <th colspan="10" style="text-align: left;">Total : <?php echo number_format(htmlentities($jumlah),0,',','.');?> Data</th>
                                                </tr>
                                                <?php }else{ ?>
                                                <tr>
                                                <th colspan="10" style="text-align: left;">Total : 0 Data</th>
                                                </tr>
                                                <?php } ?>
                                            </tfoot>

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> -->
                <div class="row">
                <div class="col-md-12">
                        <div class="white-box">
                            <h3 class="box-title"><?php echo $mtitle; ?></h3>
                            <div class="scrollable">
                                <div class="table-responsive">
                                    <table id="mytable" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                    <thead>
                                     
                                        <tr>
                                            <th width="10%" style="text-align: center;">No</th>
                                            <th width="15%" style="text-align: center;">Nama</th>
                                            <th width="20%" style="text-align: center;">Nik</th>
                                            <th width="15%" style="text-align: center;">No Kec</th>
                                            <th width="10%" style="text-align: center;">Nama Kec</th>
                                            <th width="10%" style="text-align: center;">Tanggal Cetak</th>
                                            <th width="10%" style="text-align: center;">Request</th>
                                            <th width="10%" style="text-align: center;">Receive</th>
                                            <th width="10%" style="text-align: center;">Petugas Cetak</th>
                                        </tr>
                                       
                                    </thead>
                                    <tbody id="show_data">
                                       <?php
                                        if (!empty($data)){
                                            $i = 0;
                                           foreach($data as $row){
                                            $i++;
                                            ?>
                                             <tr>
                                                <td width="10%" style="text-align: center;"><?php echo $i ;?></td>
                                                <td width="15%" style="text-align: center;"><?php echo $row->nama_lgkp ;?></td>
                                                <td width="20%" style="text-align: center;"><span style="color: #fff">,</span><?php echo $row->nik ;?></td>
                                                <td width="15%" style="text-align: center;"><?php echo $row->no_kec ;?></td>
                                                <td width="10%" style="text-align: center;"><?php echo $row->nama_kec ;?></td>
                                                <td width="10%" style="text-align: center;"><?php echo $row->tgl_cetak ;?></td>
                                                <td width="10%" style="text-align: center;"><?php echo $row->req_by ;?></td>
                                                <td width="10%" style="text-align: center;"><?php echo $row->receive_by ;?></td>
                                                <td width="10%" style="text-align: center;"><?php echo $row->created_by ;?></td>
                                            </tr>
                                          <?php } ?>
                                          <?php } ?>
                                       
                                        
                                    </tbody>
                                    
                                        <tfoot id="my_foot">
                                            <tr>
                                            <?php if (!empty($jumlah)){ ?>
                                            <th width="80%" colspan="8" style="text-align: center;">Jumlah</th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah),0,',','.');?></th>
                                            <?php } ?>
                                        </tr>

                                            </tfoot>
                                </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                 </div>
       @include('shared.footer_detail')