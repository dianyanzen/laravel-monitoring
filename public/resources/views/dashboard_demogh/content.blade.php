 <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $stitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                        <ol class="breadcrumb">
                           <li class="text-info"><?php echo $stitle; ?></li>
                        </ol>
                    </div>
                </div>
                <?php if($user_level == 5) { ?>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-lg-12">
                        <div class="white-box p-b-0">
                            <div class="row">
                                <div class="col-xs-8">
                                    <h2 class="font-medium m-t-0">Demographics KTP-EL</h2>
                                    <h5 class="text-muted m-t-0">Tanggal <?php 
                                        $date = date('Y-m-d');
                                        $newdate = strtotime ( '-1 day' , strtotime ( $date ) ) ;
                                        $newdate = date ( 'd-m-Y' , $newdate );
                                        echo $newdate;
                                    ?></h5>
                                </div>
                                <div class="col-xs-4">
                                    <div class="circle-md pull-right circle bg-info"><i class="icon-credit-card"></i></div>
                                </div>
                            </div>
                            <div class="row m-t-30 minus-margin">
                                <div class="col-sm-12 col-sm-6 b-t b-r">
                                    <ul class="expense-box">
                                        <li><i class="ti-headphone-alt text-info"></i>
                                            <div>
                                                <h2><span id="perekaman_today">0</span></h2>
                                                <h4>Perekaman KTP-EL</h4>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-sm-12 col-sm-6  b-t">
                                    <ul class="expense-box">
                                        <li><i class="ti-home text-info"></i>
                                            <div>
                                                <h2><span id="pencetakan_today">0</span></h2>
                                                <h4>Pencetakan KTP-EL</h4>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="row minus-margin">
                                <div class="col-sm-12 col-sm-6  b-t b-r">
                                    <ul class="expense-box">
                                        <li><i class="fa fa-paper-plane-o text-info"></i>
                                            <div>
                                                <h2><span id="sisa_prr">0</span></h2>
                                                <h4>Print Ready Record</h4>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-sm-12 col-sm-6  b-t">
                                    <ul class="expense-box">
                                        <li><i class="ti-shopping-cart text-info"></i>
                                            <div>
                                                <h2><span id="sisa_sfe">0</span></h2>
                                                <h4>Sent For Enrollment</h4>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-sm-12 col-sm-6  b-t">
                                    <ul class="expense-box">
                                        <li><i class="ti-unlink text-info"></i>
                                            <div>
                                                <h2><span id="sisa_failure">0</span></h2>
                                                <h4>Failure Or Adjudicate</h4>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                
                                <div class="col-sm-12 col-sm-6  b-t b-r">
                                    <ul class="expense-box">
                                        <li><i class="fa fa-print text-info"></i>
                                            <div>
                                                <h2><span id="sisa_suket">0</span></h2>
                                                <h4>Sisa Suket</h4>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-sm-12 col-sm-6  b-t b-r">
                                    <ul class="expense-box">
                                        <li><i class="fa fa-credit-card text-info"></i>
                                            <div>
                                                <h2><span id="blangko_out">0</span></h2>
                                                <h4>Pengeluaran Blangko</h4>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-sm-12 col-sm-6  b-t b-r">
                                    <ul class="expense-box">
                                        <li><i class="ti-drupal text-info"></i>
                                            <div>
                                                <h2><span id="sisa_blangko_yesterday">0</span></h2>
                                                <h4>Sisa Blangko</h4>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                 
                </div>
                <?php } ?>
                 </div>
       @include('shared.footer_detail')