<!DOCTYPE html>
<html lang="en">
@include('shared.head')
<body class="fix-header">
    <div id="wrapper">
            @include('shared.nav_top')
            @include('shared/nav_side')
            @include('pengajuan_ektp_perubahan/content')
    </div>
     
    @include('shared/footer')
    
    @include('pengajuan_ektp_perubahan/javascript')
</body>
</html>