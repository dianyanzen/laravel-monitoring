 <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $stitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                        <ol class="breadcrumb">
                           <li class="text-info"><?php echo $stitle; ?></li>
                        </ol>
                    </div>
                </div>
                 <div class="row">
                    <div class="col-sm-12">
                        <div class="white-box">
                            <div class="row">
                                <div class="col-lg-3">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_nik" type="checkbox" checked disabled="true">
                                            <label for="cb_nik"> Nik</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3">         
                                    <div class="form-group">
                                        <input class="form-control" type="text" id="nik" name="nik" onkeypress="return isNumberKey(event)" /></div>
                                        
                                </div>
                            </div>
                            <div class="row">
                                <button type="submit" name="do_search" class="btn btn-info waves-effect waves-light m-r-10 pull-right" id="btn-filter" onclick="on_serach();" >Search <i class="mdi  mdi-magnify fa-fw"></i></button>
                                  <button type="button" class="btn btn-invert waves-effect waves-light m-r-10 pull-right" id="btn-reset" onclick="on_clear();">Clear <i class="mdi  mdi-delete fa-fw"></i></button>
                            </div>
                            
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-12 col-xs-12">
                        <div class="white-box">
                        <ul class="nav nav-tabs tabs customtab">
                                    <li class="active tab">
                                        <a href="#request_vw" data-toggle="tab"> <span class="visible-xs"><i class="mdi mdi-auto-fix"></i></span> <span class="hidden-xs" style="color: #292961  !important">List Pengajuan</span> </a> 
                                    </li>
                                    <li class="tab">
                                        <a href="#requestbtl_vw" data-toggle="tab"> <span class="visible-xs"><i class="mdi mdi-file-excel"></i></span> <span class="hidden-xs" style="color: red !important">List Yang Di Tolak</span> </a> 
                                    </li>
                                </ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="tab-content" style="margin-top: 5px !important">
                    <div class="tab-pane active" id="request_vw">
                <div class="col-md-12">
                        <div class="white-box">
                            <h3 class="box-title"><?php echo $mtitle; ?></h3>
                            <div class="row">
                                <div class="col-md-1">
                                    <input type="hidden" name="niks" id="niks">
                                    <input type="hidden" name="jum_nik" id="jum_nik">
                                <button type="button" class="btn btn-success waves-effect waves-light" onclick="do_req();"> <i class="mdi mdi-send m-r-5" ></i> <span>Ajukan Pencetakan</span></button>
                                
                                </div>
                                <br>
                                <br>
                                <br>
                                </div>
                            <div class="scrollable">
                                <div class="table-responsive">
                                    <table id="pengajuan-list" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                   <thead>
                                    
                                         <tr>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">NO</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">NO KEC</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">NIK</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">NO KK</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">NAMA LENGKAP</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">STATUS E-KTP</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">TEMPAT TANGGAL LAHIR</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">ALAMAT</th>
                                                
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">REQ DATE</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">REQ BY</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">PRINT DATE</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">PRINT BY</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">ALASAN PENGAJUAN</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">STATUS PENGAJUAN</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">KETERANGAN PENGAJUAN</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">NAMA KECAMATAN</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">NAMA KELURAHAN</th>
                                                
                                            </tr>
                                       
                                    </thead>
                                    <tbody id="show_data">
                                       
                                        
                                    </tbody>
                                    
                                </table>
                                </div>

                            </div>
                        </div>
                    </div>
                    </div>
                    <div class="tab-pane" id="requestbtl_vw">
                             <div class="col-md-12">
                        <div class="white-box">
                            <div class="panel panel-default">
                            <div class="panel-heading">Request Yang Di Tolak</div>
                            <div class="panel-wrapper collapse in">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th class="text-center">#</th>
                                            <th>Informasi Penolakan Pengajuan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td align="center">1</td>
                                            <td>Status Ktp Yang Tidak Bisa Dicetak Tidak Dapat Diajukan</td>
                                        </tr>
                                        <tr>
                                            <td align="center">2</td>
                                            <td>Perekaman Pemula Tidak Perlu Diajukan, Langsung Sajah Tarik Di List PRR</td>
                                        </tr>
                                        <tr>
                                            <td align="center">3</td>
                                            <td>Informasi Terkait Data Rekam Kosong (Pindahan Luar Kota)</td>
                                        </tr>
                                         <tr>
                                            <td align="center">-</td>
                                            <td><b style="font-weight:bold !important; color:#292961;">Lakukan Request Biometric Terlebih Dahulu Untuk Menarik Data Perekaman Yang Kosong</b></td>
                                        </tr>
                                         <tr>
                                            <td align="center">-</td>
                                            <td><b style="font-weight:bold !important; color:#292961;">Setelah Request Biometric Dilakukan Menggunakan Konsol Masing Masing, Data Akan Terupdate dalam 5-30 Menit</b></td>
                                        </tr>
                                         <tr>
                                            <td align="center">-</td>
                                            <td><b style="font-weight:bold !important; color:#292961;">Setelah Data Memiliki Status E-Ktp Maka Data Siap Diajukan</b></td>
                                        </tr>
                                        <tr>
                                            <td align="center">4</td>
                                            <td><B>Sekian Dan Terima Kasih, Kalau Bingung Bisa Tanyakan Kepada Kepala Seksi Sistem Informasi Administrasi Kependudukan Dan Staffnya</B></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                            <div class="scrollable">
                                <div class="table-responsive">
                                    <table id="pengajuan-listbtl" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                   <thead>
                                    
                                         <tr>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">NO</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">NO KEC</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">NIK</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">NO KK</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">NAMA LENGKAP</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">TEMPAT TANGGAL LAHIR</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">ALAMAT</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">STATUS E-KTP</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">REQ DATE</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">REQ BY</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">PRINT DATE</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">PRINT BY</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">ALASAN PENGAJUAN</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">STATUS PENGAJUAN</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">KETERANGAN PENGAJUAN</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">NAMA KECAMATAN</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">NAMA KELURAHAN</th>
                                                
                                            </tr>
                                       
                                    </thead>
                                    <tbody id="show_data">
                                       
                                        
                                    </tbody>
                                    
                                </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>



                </div>
                 </div>

       @include('shared.footer_detail')