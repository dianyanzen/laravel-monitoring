<!DOCTYPE html>
<html lang="en">
@include('shared.head')
<body class="fix-header">
    <div id="wrapper">
            @include('shared.nav_top')
            @include('shared/nav_side')
            @include('blangko_kk_out/content')
    </div>
     
    @include('shared/footer')
    
    @include('blangko_kk_out/javascript')
</body>
</html>