<div id="page-wrapper">
            <div class="container-fluid">
               <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $mtitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                
                        <ol class="breadcrumb">
                            <li><a href="{{ url('/') }}/">Laporan</a></li>
                            <li class="active"><?php echo $mtitle; ?></li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- .row -->
                <div class="row">
                <form name ="get_form" action="<?php echo $my_url; ?>" method="post">
                    @csrf
                    <div class="col-sm-12">
                        <div class="white-box">
                            <div class="row">
                                <div class="col-lg-4">         
                                    <div class="form-group">
                                        <h3 class="box-title m-b-0"><b>Tanggal <?php echo $type_tgl; ?></b></h3>
                                        <input class="form-control input-daterange-datepicker" type="text" id="tanggal" name="tanggal"/> </div>
                                    
                                </div>
                                <div class="col-lg-4">         
                                    <div class="form-group" <?php if (!empty($is_colnotwil)){echo 'style="display: none!important;"'; }?>>
                                        <h3 class="box-title m-b-0"><b>Kecamatan</b></h3>
                                        <select class="form-control select2" name="no_kec" id="no_kec">
                                <option  value="0">-- Pilih Kecamatan --</option>
                            </select>
                            
                                    </div>
                                </div>
                                <div class="col-lg-4">         
                                    <div class="form-group" <?php if (!empty($is_colnotwil)){echo 'style="display: none!important;"'; }?>>
                                        <h3 class="box-title m-b-0"><b>Kelurahan</b></h3>
                                        <select class="form-control select2" name="no_kel" id="no_kel">
                                        <option value="0">-- Pilih Kelurahan --</option>  
                                    </select>
                           
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <button type="submit" class="btn btn-info waves-effect waves-light m-r-10 pull-right" id="btn-filter" onclick="on_serach();" >Search <i class="mdi  mdi-magnify fa-fw"></i></button>
                                  <button type="button" class="btn btn-invert waves-effect waves-light m-r-10 pull-right" id="btn-reset" onclick="on_clear();">Clear <i class="mdi  mdi-delete fa-fw"></i></button>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>

                <div class="row">
                <div class="col-sm-12">
                        <div class="white-box">
                            <h3 class="box-title m-b-0"><?php echo $stitle; ?></h3>
                            <p class="text-muted m-b-30">Dinas Kependudukan Dan Pencatatan Sipil Kota Bandung</p>
                            <div class="table-responsive">
                                <table id="mytable" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                   <thead>
                                        <tr>
                                            <?php if (!empty($is_mobilitas)){?>
                                            <th width="10%" style="text-align: right;"><?php echo $no_wil ;?></th>
                                            <th width="30%" style="text-align: left;"><?php echo $kode_wil ;?></th>
                                            <th width="30%" style="text-align: center;">Tanggal</th>
                                            <th width="15%" style="text-align: right;">Jumlah KK</th>
                                            <th width="15%" style="text-align: right;">Jumlah Anggota</th>
                                            <?php }else{ ?>
                                            <th width="10%" style="text-align: right;"><?php echo $no_wil ;?></th>
                                            <th width="40%" style="text-align: left;"><?php echo $kode_wil ;?></th>
                                            <th width="30%" style="text-align: center;">Tanggal</th>
                                            <th width="20%" style="text-align: right;">Jumlah</th>
                                            <?php } ?>
                                        </tr>
                                    </thead>
                                    <tbody id="my_data">
                                          <?php
                                        if (!empty($data)){
                                            $jumlah = 0;
                                            $jumlah_anggota = 0;
                                           foreach($data as $row){?>
                                             <tr>
                                                <?php if (!empty($is_mobilitas)){ ?>
                                                    <?php   
                                                        $jumlah = $jumlah + $row->jumlah;
                                                        $jumlah_anggota = $jumlah_anggota + $row->jumlah_anggota; 
                                                    ?>
                                                <td width="10%" style="text-align: right;"><?php echo $row->no_wil ;?></td>
                                                <td width="30%" style="text-align: left;"><?php echo $row->nama_wil ;?></td>
                                                <td width="30%" style="text-align: center;"><?php echo $row->tanggal ;?></td>
                                                <td width="15%" style="text-align: right;"><?php echo $row->jumlah ;?></td>
                                                <td width="15%" style="text-align: right;"><?php echo $row->jumlah_anggota ;?></td>
                                                <?php }else{ ?>
                                                    <?php   
                                                        $jumlah = $jumlah + $row->jumlah;
                                                    ?>
                                                <td width="10%" style="text-align: right;"><?php echo $row->no_wil ;?></td>
                                                <td width="40%" style="text-align: left;"><?php echo $row->nama_wil ;?></td>
                                                <td width="30%" style="text-align: center;"><?php echo $row->tanggal ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->jumlah ;?></td>
                                                <?php } ?>
                                            </tr>
                                         <?php }
                                         } ?>
                                    </tbody>
                                    <?php if (!empty($jumlah)){ ?>
                                    <tfoot id="my_foot">
                                         <tr>
                                            <?php if (!empty($is_mobilitas)){?>
                                            <th width="70%" colspan="3" style="text-align: center;">Jumlah</th>
                                            <th width="15%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah),0,',','.');?></th>
                                            <th width="15%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah_anggota),0,',','.');?></th>
                                            <?php }else{ ?>
                                            <th width="80%" colspan="3" style="text-align: center;">Jumlah</th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah),0,',','.');?></th>
                                            <?php } ?>
                                        </tr>
                                    </tfoot>
                                    <?php } ?>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
           @include('shared.footer_detail')
        </div>