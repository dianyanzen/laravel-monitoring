

<div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $stitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                        <ol class="breadcrumb">
                           <li class="text-info"><?php echo $stitle; ?></li>
                        </ol>
                    </div>
                </div>
                <!-- .row -->
                    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="ganda_modal" id="ganda_modal" aria-hidden="true" style="display: none;">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h4 class="modal-title" id="ganda_modal_text" style="text-align:center">Large modal</h4> </div>
                                <div class="modal-body" >
                                    <div class="row" id="ganda_modal_isi">
                                        

                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>
                 <div class="row">
                    <form name ="get_form" id="get_form" action="<?php echo $my_url; ?>" method="post">
                    @csrf
                    <div class="col-sm-12">
                        <div class="white-box">
                            <div class="row">
                                <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_nik" type="checkbox" checked disabled="true">
                                            <label for="cb_nik"> Nik</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">         
                                    <div class="form-group">
                                        <input class="form-control" type="text" id="nik" name="nik" maxlength="16" onkeypress="return isNumberKey(event)"/>
                                        <input type="hidden" id="no_kec" name="no_kec" value="<?php echo $user_no_kec; ?>" />
                                    </div>
                                </div>
                                </div>
                                <div class="row">
                                <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_no_kk" type="checkbox" checked disabled="true">
                                            <label for="cb_no_kk"> No KK</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">         
                                    <div class="form-group">
                                        <input class="form-control" type="text" id="no_kk" name="no_kk" maxlength="16" onkeypress="return isNumberKey(event)"/></div>
                                </div>
                                </div>
                            <div class="row">
                                <a class="btn btn-info waves-effect waves-light m-r-10 pull-right" id="btn-filter" onclick="get_data();" >Search <i class="mdi  mdi-magnify fa-fw"></i></a>
                                  <a  class="btn btn-info waves-effect waves-light m-r-10 pull-right" id="btn-reset" onclick="on_clear();">Clear <i class="mdi  mdi-delete fa-fw"></i></a>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>

                <?php if (!empty($data)){ ?>
                         <div class="row" id="all_content">
                    <div class="col-lg-12" id="all_<?php echo $data[0]->nik; ?>">
                        <div class="white-box">
                            <div class="">
                                <h2 class="m-b-0 m-t-0">Biodata Wni</h2>
                                <hr>
                                <div class="row el-element-overlay m-b-40">
                                    <div class="col-lg-4 col-md-4 col-sm-6">
                                    <table class="table">
                                        <tbody>
                                        <tr>
                                    <div class="white-box">
                                        <?php if ($data[0]->path != '-'){ ?>
                                    <?php $filename = 'http://10.32.73.222:8080/Siak/'.$data[0]->path.'/'.$data[0]->nik.'.jpg';
                                        if ($_SERVER['SERVER_NAME'] == '10.32.73.7') { ?>
                                        <div class="el-card-item">
                                            <div class="el-card-avatar el-overlay-1"> <img src="<?php echo $filename; ?>" />
                                                <div class="el-overlay">
                                                    <ul class="el-info">
                                                        <li><a class="btn default btn-outline image-popup-vertical-fit" href="<?php echo $filename; ?>"><i class="icon-magnifier"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                            <?php }else{ ?>
                                                <?php if ($data[0]->jenis_klmin == 'LAKI-LAKI'){ ?>
                                            <div class="el-card-item">
                                            <div class="el-card-avatar el-overlay-1"> <img src="{{ url('/') }}/assets/plugins/images/male-placeholder.jpg" />
                                                <div class="el-overlay">
                                                    <ul class="el-info">
                                                        <li><a class="btn default btn-outline image-popup-vertical-fit" href="{{ url('/') }}/assets/plugins/images/male-placeholder.jpg"><i class="icon-magnifier"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            </div>
                                            <?php }else{ ?>
                                            <div class="el-card-item">
                                            <div class="el-card-avatar el-overlay-1"> <img src="{{ url('/') }}/assets/plugins/images/female-placeholder.jpg" />
                                                <div class="el-overlay">
                                                    <ul class="el-info">
                                                        <li><a class="btn default btn-outline image-popup-vertical-fit" href="{{ url('/') }}/assets/plugins/images/female-placeholder.jpg"><i class="icon-magnifier"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            </div>
                                            <?php } ?> 

                                            <?php } ?> 
                                        <?php }else{ ?>
                                            <?php if ($data[0]->jenis_klmin == 'LAKI-LAKI'){ ?>
                                            <div class="el-card-item">
                                            <div class="el-card-avatar el-overlay-1"> <img src="{{ url('/') }}/assets/plugins/images/male-placeholder.jpg" />
                                                <div class="el-overlay">
                                                    <ul class="el-info">
                                                        <li><a class="btn default btn-outline image-popup-vertical-fit" href="{{ url('/') }}/assets/plugins/images/male-placeholder.jpg"><i class="icon-magnifier"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            </div>
                                            <?php }else{ ?>
                                            <div class="el-card-item">
                                            <div class="el-card-avatar el-overlay-1"> <img src="{{ url('/') }}/assets/plugins/images/female-placeholder.jpg" />
                                                <div class="el-overlay">
                                                    <ul class="el-info">
                                                        <li><a class="btn default btn-outline image-popup-vertical-fit" href="{{ url('/') }}/assets/plugins/images/female-placeholder.jpg"><i class="icon-magnifier"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            </div>
                                            <?php } ?> 
                                        <?php } ?> 
                                    
                                    </div>
                                    </tr>
                                    <?php if ($data[0]->ket == 'TIDAK AKTIF DI SIAK'){ ?>
                                          <?php if ($data[0]->current_status_code != 'DUPLICATE_RECORD'){ ?> 

                                            <?php if (empty($mati) && empty($pindah)){ ?>
                                            <tr>
                                                <td>
                                                    <?php if ($user_no_kec == 0 && ($user_level ==5 || $user_level ==6) ){ ?>
                                                    
                                                    <?php } ?>
                                                </td>
                                            </tr>
                                            
                                            <?php } else if (!empty($pindah)){ ?>
                                                <?php if ($pindah[0]->klasifikasi_pindah == 'ANTAR KECAMATAN' || $pindah[0]->klasifikasi_pindah == 'ANTAR DESA/KELURAHAN' || $pindah[0]->klasifikasi_pindah == 'DALAM SATU DESA/KELURAHAN' || ($pindah[0]->created_date < $data[0]->delete_dt && $data[0]->flag_status == 'AKTIF')){ ?>
                                            <tr>
                                                <td>
                                                    <?php if ($user_no_kec == 0 && ($user_level ==5 || $user_level ==6) ){ ?>
                                                    
                                                    <?php } ?>
                                                </td>
                                            </tr>
                                            <?php } ?> 
                                            <?php } ?> 
                                             <?php if ($data[0]->stat_hbkel != 'KEPALA KELUARGA' && empty($mati) && empty($pindah)){ ?> 
                                            <tr>
                                                <td>
                                                    <?php if ($user_no_kec == 0 && ($user_level ==5 || $user_level ==6) ){ ?>
                                                    
                                                    <?php } ?>
                                                </td>
                                            </tr> 
                                            <tr>
                                                <td>
                                                    <?php if ($user_no_kec == 0 && ($user_level ==5 || $user_level ==6) ){ ?>
                                                    
                                                    <?php } ?>
                                                </td>
                                            </tr>
                                            


                                            <?php } else if (!empty($pindah)) { ?>
                                                <?php if ($pindah[0]->klasifikasi_pindah == 'ANTAR KECAMATAN' || $pindah[0]->klasifikasi_pindah == 'ANTAR DESA/KELURAHAN' || $pindah[0]->klasifikasi_pindah == 'DALAM SATU DESA/KELURAHAN' || ($pindah[0]->created_date < $data[0]->delete_dt && $data[0]->flag_status == 'AKTIF')){ ?>
                                                    <tr>
                                                <td>
                                                    <?php if ($user_no_kec == 0 && ($user_level ==5 || $user_level ==6) ){ ?>
                                                    
                                                <?php } ?>
                                                </td>
                                            </tr>
                                                    <tr>
                                                <td>
                                                    <?php if ($user_no_kec == 0 && ($user_level ==5 || $user_level ==6) ){ ?>
                                                        
                                                <?php } ?>
                                                </td>
                                            </tr>
                                               
                                          <?php } ?>
                                          <?php } ?>
                                         <?php } ?> 
                                    <?php }else{ ?>
                                        
                                        <?php if ($data[0]->current_status_code == 'DUPLICATE_RECORD' || $data[0]->current_status_code == 'BELUM REKAM'){ ?> 
                                        <tr>
                                                <td>
                                                    <?php if ($user_no_kec == 0 && ($user_level ==5 || $user_level ==6) ){ ?>
                                                      
                                                    <?php } ?> 
                                                </td>
                                            </tr>
                                        <?php } ?>
                                        <tr>
                                                <td>
                                                    <?php if ($user_no_kec == 0 && ($user_level ==5 || $user_level ==6) ){ ?>
                                                    
                                                    <?php } ?>
                                                </td>
                                            </tr> 
                                            
                                    <?php } ?> 
                                    </tbody>
                                    </table>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-6">
                                        <table class="table">
                                                <tbody>
                                                    <tr>
                                                        <td width="250"><h4>NAMA LENGKAP</h4></td>
                                                        <td> <h4><?php echo $data[0]->nama_lgkp; ?></h4> </td>
                                                        <td><h4></h4></td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>NIK</h4></td>
                                                        <td> <h4><?php echo $data[0]->nik; ?></h4> </td>
                                                        <td><h4></h4></td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>NO KK</h4></td>
                                                        <td> <h4><?php echo $data[0]->no_kk; ?></h4> </td>
                                                        <td><h4></h4></td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>STAT HUBKEL</h4></td>
                                                        <td> <h4><?php echo $data[0]->stat_hbkel; ?></h4> </td>
                                                        <td><h4></h4></td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>TEMPAT LAHIR</h4></td>
                                                        <td> <h4><?php echo $data[0]->tmpt_lhr; ?></h4> </td>
                                                        <td><h4></h4></td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>TANGGAL LAHIR</h4></td>
                                                        <td> <h4><?php echo $data[0]->tgl_lhr; ?></h4> </td>
                                                        <td><h4></h4></td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>NO AKTA LAHIR</h4></td>
                                                        <td> <?php echo (!is_null($data[0]->no_akta_lhr)) ? '<h4 class="text-success">'.$data[0]->no_akta_lhr.'</h4>' : '<h4 class="text-danger">- No Akta Tidak Terdaftar -</h4>'; ?> </td>
                                                        <td><h4></h4></td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>JENIS KELAMIN</h4></td>
                                                        <td> <h4><?php echo $data[0]->jenis_klmin; ?></h4> </td>
                                                        <td><h4></h4></td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>KECAMATAN</h4></td>
                                                        <td> <h4><?php echo $data[0]->nama_kec; ?></h4> </td>
                                                        <td><h4></h4></td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>KELURAHAN</h4></td>
                                                        <td> <h4><?php echo $data[0]->nama_kel; ?></h4> </td>
                                                        <td><h4></h4></td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>STATUS PEREKAMAN</h4></td>
                                                        <?php if ($data[0]->current_status_code == 'ENROLL_FAILURE_AT_REGIONAL' || $data[0]->current_status_code == 'PACKET_RETRY' || $data[0]->current_status_code == 'RECEIVED_AT_CENTRAL' || $data[0]->current_status_code == 'PROCESSING' || $data[0]->current_status_code == 'SENT_FOR_ENROLLMENT' || $data[0]->current_status_code == 'ENROLL_FAILURE_AT_CENTRAL' || $data[0]->current_status_code == 'SEARCH_FAILURE_AT_CENTRAL' || $data[0]->current_status_code == 'ADJUDICATE_RECORD' || $data[0]->current_status_code == 'ADJUDICATE_IN_PROCESS' || $data[0]->current_status_code == 'SENT_FOR_DEDUP' || $data[0]->current_status_code == 'DUPLICATE_RECORD' || $data[0]->current_status_code == 'BELUM REKAM'){ ?>
                                                        <td> <h4 class="text-info"><b><?php echo $data[0]->current_status_code; ?></b></h4> </td>
                                                    <?php }else{ ?>
                                                        <td> <h4 class="text-info"><b><?php echo $data[0]->current_status_code; ?></b></h4> </td>
                                                    <?php } ?>
                                                        <td><h4></h4></td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>NAMA IBU</h4></td>
                                                        <td> <h4><?php echo $data[0]->nama_lgkp_ibu; ?></h4> </td>
                                                        <td><h4></h4></td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>NAMA AYAH</h4></td>
                                                        <td> <h4><?php echo $data[0]->nama_lgkp_ayah; ?></h4> </td>
                                                        <td><h4></h4></td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>KETERANGAN </h4></td>
                                                    <?php if ($data[0]->ket == 'TIDAK AKTIF DI SIAK'){ ?>
                                                        <td> <h4 class="text-danger"><b><?php echo $data[0]->ket; ?></b></h4> </td>
                                                    <?php }else{ ?>
                                                        <td> <h4 class="text-success"><b><?php echo $data[0]->ket; ?></b></h4> </td>
                                                    <?php } ?>
                                                        <td><h4></h4></td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>FLAG STATUS </h4></td>
                                                    <?php if ($data[0]->flag_status == 'TIDAK AKTIF'){ ?>
                                                        <td> <h4 class="text-danger"><b><?php echo $data[0]->flag_status; ?></b></h4> </td>
                                                    <?php }else{ ?>
                                                        <td> <h4 class="text-success"><b><?php echo $data[0]->flag_status; ?></b></h4> </td>
                                                    <?php } ?>
                                                        <td><h4></h4></td>
                                                    </tr>
                                                    <?php if ($data[0]->ket == 'TIDAK AKTIF DI SIAK'){ ?>
                                                         <?php if ($data[0]->alasan != '-'){ ?>
                                                        <tr>
                                                            <td><h4>ALASAN PENGHAPUSAN</h4></td>
                                                            <td> <h4 class="text-danger"><?php echo $data[0]->alasan; ?></h4> </td>
                                                            <td> <h4 class="text-danger"><?php echo $data[0]->desc_alasan; ?>
                                                            <?php if(stripos($data[0]->desc_alasan, 'GANDA DENGAN NIK :') !== FALSE){ echo '<br><button type="button" class="btn btn-info" id="btn-proses" onclick="cek_ganda('.$data[0]->nik.');" style="margin-top : 5px;"><i class="fa fa-search"></i> Detail</button>'; }?></h4> </td>

                                                        </tr>
                                                        <tr>
                                                            <td><h4>HISTORY PENGHAPUSAN</h4></td>
                                                            <td> <h4 class="text-danger"><?php echo $data[0]->created_by; ?></h4> </td>
                                                            <td> <h4 class="text-danger"><?php echo $data[0]->delete_dt; ?></h4> </td>

                                                        </tr>
                                                        <?php } ?>
                                                        <?php if (!empty($pindah)){ ?>
                                                        <tr>
                                                            <td><h4>HISTORY PINDAH</h4></td>
                                                            <?php if ($pindah[0]->klasifikasi_pindah == 'ANTAR KECAMATAN' || $pindah[0]->klasifikasi_pindah == 'ANTAR DESA/KELURAHAN' || $pindah[0]->klasifikasi_pindah == 'DALAM SATU DESA/KELURAHAN'){ ?>
                                                                <td> <h4 class="text-info"><?php echo $pindah[0]->no_pindah; ?></h4> </td>
                                                            <td> <h4 class="text-danger"><?php echo $pindah[0]->klasifikasi_pindah; ?></h4> </
                                                               <?php }else{ ?> 
                                                                <td> <h4 class="text-info"><?php echo $pindah[0]->no_pindah; ?></h4> </td>
                                                            <td> <h4 class="text-danger"><?php echo $pindah[0]->klasifikasi_pindah; ?></h4> </
                                                               <?php } ?> 
                                                            td>

                                                        </tr>
                                                        <tr>
                                                            <td><h4>DARI :</h4></td>
                                                            <td> <h4><?php echo $pindah[0]->dari_nama_provinsi; ?></h4> </td>
                                                            <td> <h4><?php echo $pindah[0]->dari_nama_kabupaten; ?></h4> </td>
                                                        </tr>
                                                        <tr>
                                                            <td><h4></h4></td>
                                                            <td> <h4><?php echo $pindah[0]->dari_nama_kecamatan; ?></h4> </td>
                                                            <td> <h4><?php echo $pindah[0]->dari_nama_kelurahan; ?></h4> </td>
                                                        </tr>
                                                        <tr>
                                                            <td><h4>KE :</h4></td>
                                                            <td> <h4><?php echo $pindah[0]->tujuan_nama_provinsi; ?></h4> </td>
                                                            <td> <h4><?php echo $pindah[0]->tujuan_nama_kabupaten; ?></h4> </td>
                                                        </tr>
                                                        <tr>
                                                            <td><h4></h4></td>
                                                            <td> <h4><?php echo $pindah[0]->tujuan_nama_kecamatan; ?></h4> </td>
                                                            <td> <h4><?php echo $pindah[0]->tujuan_nama_kelurahan; ?></h4> </td>
                                                        </tr>
                                                        <tr>
                                                            <td><h4>STATUS PINDAH :</h4></td>
                                                            <td> <h4 class="text-danger"><?php echo $pindah[0]->ket_pindah; ?></h4> </td>
                                                            <td> <h4></h4> </td>
                                                        </tr>
                                                        <?php } ?>
                                                    <?php } ?>
                                                    <?php if ($data[0]->ket == 'TIDAK AKTIF DI SIAK'){ ?>
                                                        <?php if (!empty($mati)){ ?>
                                                        <tr>
                                                            <td><h4>HISTORY KEMATIAN</h4></td>
                                                            <td colspan="2"> <h4 class="text-info"><?php echo $mati[0]->ket; ?></h4> </td>
                                                        </tr>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        

                                    </div>
                                    </div>
                                   
                            </div>
                        </div>
                    </div>
                    <?php if (!empty($data_kk)){ ?>
                        <div class="col-lg-12">
                            <div class="white-box">
                                <div class="table-responsive">
                                    <h2 class="m-b-0 m-t-0">Data Keluarga</h2>
                                <hr>
                                    <table class="table color-bordered-table info-bordered-table">
                                        <thead>
                                            <tr>
                                                        <th><h6 class="text-white" style="font-weight: bold !important;text-align: center;">NO</h6></th>
                                                        <th><h6 class="text-white" style="font-weight: bold !important;text-align: center;">NO KK</h6></th>
                                                        <th><h6 class="text-white" style="font-weight: bold !important;text-align: center;">NIK</h6></th>
                                                        <th><h6 class="text-white" style="font-weight: bold !important;text-align: center;">NAMA LENGKAP</h6></th>
                                                        <th><h6 class="text-white" style="font-weight: bold !important;text-align: center;">STAT HUBKEL</h6></th>
                                                        <th><h6 class="text-white" style="font-weight: bold !important;text-align: center;">TANGGAL LAHIR</h6></th>
                                                        <th><h6 class="text-white" style="font-weight: bold !important;text-align: center;">KECAMATAN</h6></th>
                                                        <th><h6 class="text-white" style="font-weight: bold !important;text-align: center;">KELURAHAN</h6></th>
                                                        <th><h6 class="text-white" style="font-weight: bold !important;text-align: center;">STATUS KTP</h6></th>
                                                        <th><h6 class="text-white" style="font-weight: bold !important;text-align: center;">FLAG STATUS</h6></th>
                                                        <th><h6 class="text-white" style="font-weight: bold !important;text-align: center;">KETERANGAN</h6></th>
                                                        <th><h6 class="text-white" style="font-weight: bold !important;text-align: center;">ACTION</h6></th>
                                            </tr>
                                        </thead>
                                        <tbody id="table_kk_<?php echo $data_kk[0]->no_kk; ?>">
                                            <?php 
                                                $i=0;
                                                foreach($data_kk as $row){ 
                                                $i++;    
                                            ?>
                                            <?php if($row->nik == $data[0]->nik){ ?>
                                            <tr>
                                                        <td><h6 class="text-info" style="text-align: center; color: #4b8df8 !important;"><?php echo $i ;?></h6></td>
                                                        <td><h6 class="text-info" style="text-align: center; color: #4b8df8 !important;"><?php echo $row->no_kk ;?></h6></td>
                                                        <td><h6 class="text-info" style="text-align: center; color: #4b8df8 !important;"><?php echo $row->nik ;?></h6></td>
                                                        <td><h6 class="text-info" style="text-align: center; color: #4b8df8 !important;"><?php echo $row->nama_lgkp ;?></h6></td>
                                                        <td><h6 class="text-info" style="text-align: center; color: #4b8df8 !important;"><?php echo $row->stat_hubkel ;?></h6></td>
                                                        <td><h6 class="text-info" style="text-align: center; color: #4b8df8 !important;"><?php echo $row->tgl_lhr ;?></h6></td>
                                                        <td><h6 class="text-info" style="text-align: center; color: #4b8df8 !important;"><?php echo $row->nama_kecamatan ;?></h6></td>
                                                        <td><h6 class="text-info" style="text-align: center; color: #4b8df8 !important;"><?php echo $row->nama_kelurahan ;?></h6></td>
                                                        <td><h6 class="text-info" style="text-align: center; color: #4b8df8 !important;"><?php echo $row->current_status_code ;?></h6></td>
                                                        <td><h6 class="text-info" style="text-align: center; color: #4b8df8 !important;"><?php echo $row->flag_status ;?></h6></td>
                                                        <?php if ($row->deleted_by == 'TIDAK AKTIF DI SIAK'){ ?>
                                                        <td><h6 style="text-align: center;" class="text-danger"><?php echo $row->deleted_by ;?></h6></td>
                                                        <?php }else{ ?>
                                                        <td><h6 style="text-align: center;" class="text-success"><?php echo $row->deleted_by ;?></h6></td>
                                                        <?php } ?>
                                                        <td><h6 class="text-info" style="text-align: center;"><a class="btn default btn-outline image-popup-vertical-fit" href="{{ url('/') }}/Siak/restore_bio_wni?nik=<?php echo $row->nik; ?>" onclick="on_fast_search('<?php echo $row->nik; ?>');"><i class="icon-magnifier"></i></a></h6></td>
                                            </tr>
                                            <?php }else{ ?>
                                                <tr>
                                                        <td><h6 style="text-align: center;"><?php echo $i ;?></h6></td>
                                                        <td><h6 style="text-align: center;"><?php echo $row->no_kk ;?></h6></td>
                                                        <td><h6 style="text-align: center;"><?php echo $row->nik ;?></h6></td>
                                                        <td><h6 style="text-align: center;"><?php echo $row->nama_lgkp ;?></h6></td>
                                                        <td><h6 style="text-align: center;"><?php echo $row->stat_hubkel ;?></h6></td>
                                                        <td><h6 style="text-align: center;"><?php echo $row->tgl_lhr ;?></h6></td>
                                                        <td><h6 style="text-align: center;"><?php echo $row->nama_kecamatan ;?></h6></td>
                                                        <td><h6 style="text-align: center;"><?php echo $row->nama_kelurahan ;?></h6></td>
                                                        <td><h6 style="text-align: center;"><?php echo $row->current_status_code ;?></h6></td>
                                                        <td><h6 style="text-align: center;"><?php echo $row->flag_status ;?></h6></td>
                                                        <?php if ($row->deleted_by == 'TIDAK AKTIF DI SIAK'){ ?>
                                                        <td><h6 style="text-align: center;" class="text-danger"><?php echo $row->deleted_by ;?></h6></td>
                                                        <?php }else{ ?>
                                                        <td><h6 style="text-align: center;" class="text-success"><?php echo $row->deleted_by ;?></h6></td>
                                                        <?php } ?>
                                                        <td><h6 class="text-info" style="text-align: center;"><a class="btn default btn-outline image-popup-vertical-fit" href="{{ url('/') }}/Siak/restore_bio_wni?nik=<?php echo $row->nik; ?>" onclick="on_fast_search('<?php echo $row->nik; ?>');"><i class="icon-magnifier"></i></a></h6></td>
                                            </tr>
                                            <?php } ?>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                        </div>
                    <?php } ?>        
                </div>
                <?php }else if (!empty($data_kk)){ ?>
                     <div class="col-lg-12">
                            <div class="white-box">
                                <div class="table-responsive">
                                    <h2 class="m-b-0 m-t-0">Data Keluarga</h2>
                                <hr>
                                    <table class="table color-bordered-table info-bordered-table">
                                        <thead>
                                            <tr>
                                                        <th><h6 class="text-white" style="font-weight: bold !important;text-align: center;">NO</h6></th>
                                                        <th><h6 class="text-white" style="font-weight: bold !important;text-align: center;">NO KK</h6></th>
                                                        <th><h6 class="text-white" style="font-weight: bold !important;text-align: center;">NIK</h6></th>
                                                        <th><h6 class="text-white" style="font-weight: bold !important;text-align: center;">NAMA LENGKAP</h6></th>
                                                        <th><h6 class="text-white" style="font-weight: bold !important;text-align: center;">STAT HUBKEL</h6></th>
                                                        <th><h6 class="text-white" style="font-weight: bold !important;text-align: center;">TANGGAL LAHIR</h6></th>
                                                        <th><h6 class="text-white" style="font-weight: bold !important;text-align: center;">KECAMATAN</h6></th>
                                                        <th><h6 class="text-white" style="font-weight: bold !important;text-align: center;">KELURAHAN</h6></th>
                                                        <th><h6 class="text-white" style="font-weight: bold !important;text-align: center;">STATUS KTP</h6></th>
                                                        <th><h6 class="text-white" style="font-weight: bold !important;text-align: center;">FLAG STATUS</h6></th>
                                                        <th><h6 class="text-white" style="font-weight: bold !important;text-align: center;">KETERANGAN</h6></th>
                                                        <th><h6 class="text-white" style="font-weight: bold !important;text-align: center;">ACTION</h6></th>
                                            </tr>
                                        </thead>
                                        <tbody id="table_kk_<?php echo $data_kk[0]->no_kk; ?>">
                                            <?php 
                                                $i=0;
                                                foreach($data_kk as $row){ 
                                                $i++;    
                                            ?>
                                                <tr>
                                                        <td><h6 style="text-align: center;"><?php echo $i ;?></h6></td>
                                                        <td><h6 style="text-align: center;"><?php echo $row->no_kk ;?></h6></td>
                                                        <td><h6 style="text-align: center;"><?php echo $row->nik ;?></h6></td>
                                                        <td><h6 style="text-align: center;"><?php echo $row->nama_lgkp ;?></h6></td>
                                                        <td><h6 style="text-align: center;"><?php echo $row->stat_hubkel ;?></h6></td>
                                                        <td><h6 style="text-align: center;"><?php echo $row->tgl_lhr ;?></h6></td>
                                                        <td><h6 style="text-align: center;"><?php echo $row->nama_kecamatan ;?></h6></td>
                                                        <td><h6 style="text-align: center;"><?php echo $row->nama_kelurahan ;?></h6></td>
                                                        <td><h6 style="text-align: center;"><?php echo $row->current_status_code ;?></h6></td>
                                                        <td><h6 style="text-align: center;"><?php echo $row->flag_status ;?></h6></td>
                                                        <?php if ($row->deleted_by == 'TIDAK AKTIF DI SIAK'){ ?>
                                                        <td><h6 style="text-align: center;" class="text-danger"><?php echo $row->deleted_by ;?></h6></td>
                                                        <?php }else{ ?>
                                                        <td><h6 style="text-align: center;" class="text-success"><?php echo $row->deleted_by ;?></h6></td>
                                                        <?php } ?>
                                                        <td><h6 class="text-info" style="text-align: center;"><a class="btn default btn-outline image-popup-vertical-fit" href="{{ url('/') }}/Siak/restore_bio_wni?nik=<?php echo $row->nik; ?>" onclick="on_fast_search('<?php echo $row->nik; ?>');"><i class="icon-magnifier"></i></a></h6></td>
                                            </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                        </div>
                        </div>
                <?php }else{ ?>
                <?php if (!empty($is_ada)){ ?>
                 <div class="row">
                    <div class="col-lg-12">
                        <div class="white-box">
                            <div class="">
                                <h2 class="m-b-0 m-t-0">Biodata Wni Tidak Ditemukan</h2>
                                <hr>
                                <div class="row el-element-overlay m-b-40">
                                    <div class="col-lg-4 col-md-4 col-sm-6">
                                    <div class="white-box">
                                        <div class="el-card-item">
                                            <div class="el-card-avatar el-overlay-1"> <img src="{{ url('/') }}/assets/plugins/images/calming-cat.gif" />
                                                <div class="el-overlay">
                                                    <ul class="el-info">
                                                        <li><a class="btn default btn-outline image-popup-vertical-fit" href="{{ url('/') }}/assets/plugins/images/calming-cat.gif"><i class="icon-magnifier"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            </div>
                                    </div>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-6">
                                        <table class="table">
                                                <tbody>
                                                    <tr>
                                                        <td width="250"><h4>NAMA LENGKAP</h4></td>
                                                        <td> <h4>-</h4> </td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>NIK</h4></td>
                                                        <td> <h4>-</h4> </td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>NO KK</h4></td>
                                                        <td> <h4>-</h4> </td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>TEMPAT LAHIR</h4></td>
                                                        <td> <h4>-</h4> </td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>TANGGAL LAHIR</h4></td>
                                                        <td> <h4>-</h4> </td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>JENIS KELAMIN</h4></td>
                                                        <td> <h4>-</h4> </td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>STATUS PEREKAMAN</h4></td>
                                                        
                                                        <td> <h4>-</h4> </td>
                                                    
                                                    </tr>
                                                    <tr>
                                                        <td><h4>NAMA IBU</h4></td>
                                                        
                                                        <td> <h4>-</h4> </td>
                                                    
                                                    </tr>
                                                    <tr>
                                                        <td><h4>NAMA AYAH</h4></td>
                                                        
                                                        <td> <h4>-</h4> </td>
                                                    
                                                    </tr>
                                                    <tr>
                                                        <td><h4>KETERANGAN</h4></td>
                                                        
                                                        <td> <h4>-</h4> </td>
                                                    
                                                    </tr>
                                                </tbody>
                                            </table>
                                        

                                    </div>
                                    </div>
                                   
                            </div>
                        </div>
                    </div>
                </div>

                <?php } ?>
                <?php } ?>
            <!-- /.container-fluid -->
         @include('shared.footer_detail')