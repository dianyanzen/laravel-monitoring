 <script>
    
    function on_clear() {
        $('#my_data').empty();
        $('#my_data').append('<tr><td colspan="4" style="text-align: center;" valign="center">No data available in table</td></td>');
        $('#my_foot').html('<tr><th width="80%" colspan="3" style="text-align: center;">Jumlah</th><th width="10%" style="text-align: right;">0</th></td>');
        $('select[name="no_kec"]').val("0").trigger("change");
        $('select[name="no_kel"]').empty();
        $('select[name="no_kel"]').append('<option value="0">-- Pilih Kelurahan --</option>');
        $('select[name="no_kel"]').val("0").trigger("change");
    }
    function on_serach(){
        $('#my_data').html('<tr><td colspan="32" style="text-align: center;" valign="center">Waiting For Generate Data</td></td>');
        $('#my_foot').html('<tr><th width="80%" colspan="3" style="text-align: center;">Jumlah</th><th width="10%" style="text-align: right;">0</th></td>');
        $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff'
            },
            baseZ: 2000
        }); 
    }
    jQuery(document).ready(function() {
            $(".select2").select2();
            $('.input-daterange-datepicker').daterangepicker({
                buttonClasses: ['btn', 'btn-sm'],
                applyClass: 'btn-info',
                cancelClass: 'btn-info',
                locale: {
                    format: 'DD-MM-YYYY'
                },
                startDate: '<?php echo date('d-m-Y');?>',
                endDate: '<?php echo date('d-m-Y');?>'
            });
           
        });
        $('#mytable').DataTable({
         "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        dom: 'Bfrtip',
        "pageLength" : 50,
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
    </script>