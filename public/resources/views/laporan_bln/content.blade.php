<div id="page-wrapper">
            <div class="container-fluid">
               <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $mtitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                
                        <ol class="breadcrumb">
                            <li><a href="{{ url('/') }}/">Laporan</a></li>
                            <li class="active"><?php echo $mtitle; ?></li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- .row -->
                <div class="row">
                <form name ="get_form" action="<?php echo $my_url; ?>" method="post">
                    @csrf
                    <div class="col-sm-12">
                        <div class="white-box">
                            <div class="row">
                                <div class="col-lg-12">         
                                    <div class="form-group">
                                        <h3 class="box-title m-b-0"><b>Bulan Laporan</b></h3>
                                        <!-- <input type="text" placeholder="" data-mask="99/99/9999" class="form-control">  -->
                                        <input class="form-control" type="text" id="bulan" data-mask="99/9999" name="bulan" onkeypress="return isNumberKey(event)" onchange="onlyNum()" />
                            </select>
                            
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <button type="submit" class="btn btn-info waves-effect waves-light m-r-10 pull-right" id="btn-filter" onclick="on_serach();" >Search <i class="mdi  mdi-magnify fa-fw"></i></button>
                                  <button type="button" class="btn btn-invert waves-effect waves-light m-r-10 pull-right" id="btn-reset" onclick="on_clear();">Clear <i class="mdi  mdi-delete fa-fw"></i></button>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
                <div class="row">
                <div class="col-sm-12">
                        <div class="white-box">
                            <h3 class="box-title m-b-0"><?php echo $stitle; ?></h3>
                            <p class="text-muted m-b-30">Dinas Kependudukan Dan Pencatatan Sipil Kota Bandung</p>
                            <div class="table-responsive">
                                <table id="mytable" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                   <thead>
                                        <tr>
                                            
                                            <th width="10%" style="text-align: right;">No</th>
                                            <th width="20%" style="text-align: right;">Tanggal</th>
                                            <th width="20%" style="text-align: right;">Sukasari</th>
                                            <th width="20%" style="text-align: right;">Coblong</th>
                                            <th width="20%" style="text-align: right;">Babakan Ciparay</th>
                                            <th width="20%" style="text-align: right;">Bojongloa Kaler</th>
                                            <th width="20%" style="text-align: right;">Andir</th>
                                            <th width="20%" style="text-align: right;">Cicendo</th>
                                            <th width="20%" style="text-align: right;">Sukajadi</th>
                                            <th width="20%" style="text-align: right;">Cidadap</th>
                                            <th width="20%" style="text-align: right;">Bandung Wetan</th>
                                            <th width="20%" style="text-align: right;">Astana Anyar</th>
                                            <th width="20%" style="text-align: right;">Regol</th>
                                            <th width="20%" style="text-align: right;">Batununggal</th>
                                            <th width="20%" style="text-align: right;">Lengkong</th>
                                            <th width="20%" style="text-align: right;">Cibeunying Kidul</th>
                                            <th width="20%" style="text-align: right;">Bandung Kulon</th>
                                            <th width="20%" style="text-align: right;">Kiaracondong</th>
                                            <th width="20%" style="text-align: right;">Bojongloa Kidul</th>
                                            <th width="20%" style="text-align: right;">Cibeunying Kaler</th>
                                            <th width="20%" style="text-align: right;">Sumur Bandung</th>
                                            <th width="20%" style="text-align: right;">Antapani</th>
                                            <th width="20%" style="text-align: right;">Bandung Kidul</th>
                                            <th width="20%" style="text-align: right;">Buahbatu</th>
                                            <th width="20%" style="text-align: right;">Rancasari</th>
                                            <th width="20%" style="text-align: right;">Arcamanik</th>
                                            <th width="20%" style="text-align: right;">Cibiru</th>
                                            <th width="20%" style="text-align: right;">Ujung Berung</th>
                                            <th width="20%" style="text-align: right;">Gedebage</th>
                                            <th width="20%" style="text-align: right;">Panyileukan</th>
                                            <th width="20%" style="text-align: right;">Cinambo</th>
                                            <th width="20%" style="text-align: right;">Mandalajati</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody id="my_data">
                                          <?php
                                        if (!empty($data)){
                                             $r1 = 0;
                                             $r2 = 0;
                                             $r3 = 0;
                                             $r4 = 0;
                                             $r5 = 0;
                                             $r6 = 0;
                                             $r7 = 0;
                                             $r8 = 0;
                                             $r9 = 0;
                                             $r10 = 0;
                                             $r11 = 0;
                                             $r12 = 0;
                                             $r13 = 0;
                                             $r14 = 0;
                                             $r15 = 0;
                                             $r16 = 0;
                                             $r17 = 0;
                                             $r18 = 0;
                                             $r19 = 0;
                                             $r20 = 0;
                                             $r21 = 0;
                                             $r22 = 0;
                                             $r23 = 0;
                                             $r24 = 0;
                                             $r25 = 0;
                                             $r26 = 0;
                                             $r27 = 0;
                                             $r28 = 0;
                                             $r29 = 0;
                                             $r30 = 0;
                                             $i = 0;
                                           foreach($data as $row){ $i = $i+ 1; ?>
                                            
                                             <tr>
                                                <?php   
                                                        $r1 = $r1 + $row->r1;
                                                        $r2 = $r2 + $row->r2;
                                                        $r3 = $r3 + $row->r3;
                                                        $r4 = $r4 + $row->r4;
                                                        $r5 = $r5 + $row->r5;
                                                        $r6 = $r6 + $row->r6;
                                                        $r7 = $r7 + $row->r7;
                                                        $r8 = $r8 + $row->r8;
                                                        $r9 = $r9 + $row->r9;
                                                        $r10 = $r10 + $row->r10;
                                                        $r11 = $r11 + $row->r11;
                                                        $r12 = $r12 + $row->r12;
                                                        $r13 = $r13 + $row->r13;
                                                        $r14 = $r14 + $row->r14; 
                                                        $r15 = $r15 + $row->r15; 
                                                        $r16 = $r16 + $row->r16; 
                                                        $r17 = $r17 + $row->r17; 
                                                        $r18 = $r18 + $row->r18; 
                                                        $r19 = $r19 + $row->r19; 
                                                        $r20 = $r20 + $row->r20; 
                                                        $r21 = $r21 + $row->r21; 
                                                        $r22 = $r22 + $row->r22; 
                                                        $r23 = $r23 + $row->r23; 
                                                        $r24 = $r24 + $row->r24; 
                                                        $r25 = $r25 + $row->r25; 
                                                        $r26 = $r26 + $row->r26; 
                                                        $r27 = $r27 + $row->r27; 
                                                        $r28 = $r28 + $row->r28; 
                                                        $r29 = $r29 + $row->r29; 
                                                        $r30 = $r30 + $row->r30; 
                                                    ?>
                                                <td width="10%" style="text-align: right;"><?php echo $i ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->day ;?></td>
                                                <th width="20%" style="text-align: right;"><?php echo $row->r1 ;?></th>
                                                <th width="20%" style="text-align: right;"><?php echo $row->r2 ;?></th>
                                                <th width="20%" style="text-align: right;"><?php echo $row->r3 ;?></th>
                                                <th width="20%" style="text-align: right;"><?php echo $row->r4 ;?></th>
                                                <th width="20%" style="text-align: right;"><?php echo $row->r5 ;?></th>
                                                <th width="20%" style="text-align: right;"><?php echo $row->r6 ;?></th>
                                                <th width="20%" style="text-align: right;"><?php echo $row->r7 ;?></th>
                                                <th width="20%" style="text-align: right;"><?php echo $row->r8 ;?></th>
                                                <th width="20%" style="text-align: right;"><?php echo $row->r9 ;?></th>
                                                <th width="20%" style="text-align: right;"><?php echo $row->r10 ;?></th>
                                                <th width="20%" style="text-align: right;"><?php echo $row->r11 ;?></th>
                                                <th width="20%" style="text-align: right;"><?php echo $row->r12 ;?></th>
                                                <th width="20%" style="text-align: right;"><?php echo $row->r13 ;?></th>
                                                <th width="20%" style="text-align: right;"><?php echo $row->r14 ;?></th>
                                                <th width="20%" style="text-align: right;"><?php echo $row->r15 ;?></th>
                                                <th width="20%" style="text-align: right;"><?php echo $row->r16 ;?></th>
                                                <th width="20%" style="text-align: right;"><?php echo $row->r17 ;?></th>
                                                <th width="20%" style="text-align: right;"><?php echo $row->r18 ;?></th>
                                                <th width="20%" style="text-align: right;"><?php echo $row->r19 ;?></th>
                                                <th width="20%" style="text-align: right;"><?php echo $row->r20 ;?></th>
                                                <th width="20%" style="text-align: right;"><?php echo $row->r21 ;?></th>
                                                <th width="20%" style="text-align: right;"><?php echo $row->r22 ;?></th>
                                                <th width="20%" style="text-align: right;"><?php echo $row->r23 ;?></th>
                                                <th width="20%" style="text-align: right;"><?php echo $row->r24 ;?></th>
                                                <th width="20%" style="text-align: right;"><?php echo $row->r25 ;?></th>
                                                <th width="20%" style="text-align: right;"><?php echo $row->r26 ;?></th>
                                                <th width="20%" style="text-align: right;"><?php echo $row->r27 ;?></th>
                                                <th width="20%" style="text-align: right;"><?php echo $row->r28 ;?></th>
                                                <th width="20%" style="text-align: right;"><?php echo $row->r29 ;?></th>
                                                <th width="20%" style="text-align: right;"><?php echo $row->r30 ;?></th>
                                                
                                            </tr>
                                         <?php }
                                         } ?>
                                    </tbody>
                                    <?php if (!empty($data)){ ?>
                                    <tfoot id="my_foot">
                                         <tr>
                                            <th width="30%" colspan="2" style="text-align: center;">Jumlah</th>
                                            <th width="15%" style="text-align: right;"><?php echo number_format(htmlentities($r1),0,',','.');?></th>
                                            <th width="15%" style="text-align: right;"><?php echo number_format(htmlentities($r2),0,',','.');?></th>
                                            <th width="15%" style="text-align: right;"><?php echo number_format(htmlentities($r3),0,',','.');?></th>
                                            <th width="15%" style="text-align: right;"><?php echo number_format(htmlentities($r4),0,',','.');?></th>
                                            <th width="15%" style="text-align: right;"><?php echo number_format(htmlentities($r5),0,',','.');?></th>
                                            <th width="15%" style="text-align: right;"><?php echo number_format(htmlentities($r6),0,',','.');?></th>
                                            <th width="15%" style="text-align: right;"><?php echo number_format(htmlentities($r7),0,',','.');?></th>
                                            <th width="15%" style="text-align: right;"><?php echo number_format(htmlentities($r8),0,',','.');?></th>
                                            <th width="15%" style="text-align: right;"><?php echo number_format(htmlentities($r9),0,',','.');?></th>
                                            <th width="15%" style="text-align: right;"><?php echo number_format(htmlentities($r10),0,',','.');?></th>
                                            <th width="15%" style="text-align: right;"><?php echo number_format(htmlentities($r11),0,',','.');?></th>
                                            <th width="15%" style="text-align: right;"><?php echo number_format(htmlentities($r12),0,',','.');?></th>
                                            <th width="15%" style="text-align: right;"><?php echo number_format(htmlentities($r13),0,',','.');?></th>
                                            <th width="15%" style="text-align: right;"><?php echo number_format(htmlentities($r14),0,',','.');?></th>
                                            <th width="15%" style="text-align: right;"><?php echo number_format(htmlentities($r15),0,',','.');?></th>
                                            <th width="15%" style="text-align: right;"><?php echo number_format(htmlentities($r16),0,',','.');?></th>
                                            <th width="15%" style="text-align: right;"><?php echo number_format(htmlentities($r17),0,',','.');?></th>
                                            <th width="15%" style="text-align: right;"><?php echo number_format(htmlentities($r18),0,',','.');?></th>
                                            <th width="15%" style="text-align: right;"><?php echo number_format(htmlentities($r19),0,',','.');?></th>
                                            <th width="15%" style="text-align: right;"><?php echo number_format(htmlentities($r20),0,',','.');?></th>
                                            <th width="15%" style="text-align: right;"><?php echo number_format(htmlentities($r21),0,',','.');?></th>
                                            <th width="15%" style="text-align: right;"><?php echo number_format(htmlentities($r22),0,',','.');?></th>
                                            <th width="15%" style="text-align: right;"><?php echo number_format(htmlentities($r23),0,',','.');?></th>
                                            <th width="15%" style="text-align: right;"><?php echo number_format(htmlentities($r24),0,',','.');?></th>
                                            <th width="15%" style="text-align: right;"><?php echo number_format(htmlentities($r25),0,',','.');?></th>
                                            <th width="15%" style="text-align: right;"><?php echo number_format(htmlentities($r26),0,',','.');?></th>
                                            <th width="15%" style="text-align: right;"><?php echo number_format(htmlentities($r27),0,',','.');?></th>
                                            <th width="15%" style="text-align: right;"><?php echo number_format(htmlentities($r28),0,',','.');?></th>
                                            <th width="15%" style="text-align: right;"><?php echo number_format(htmlentities($r29),0,',','.');?></th>
                                            <th width="15%" style="text-align: right;"><?php echo number_format(htmlentities($r30),0,',','.');?></th>
                                        </tr>
                                    </tfoot>
                                    <?php } ?>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
           @include('shared.footer_detail')
        </div>