
    <script src="{{ url('/') }}/assets/plugins/bower_components/owl.carousel/owl.carousel.min.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/owl.carousel/owl.custom.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/flot/jquery.flot.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/Minimal-Gauge-chart/js/cmGauge.js"></script>
    <!-- Footable -->
    <script src="{{ url('/') }}/assets/plugins/bower_components/footable/js/footable.all.min.js"></script>
    
    <!--FooTable init-->
    <script src="{{ url('/') }}/assets/js/footable-init.js"></script>
   
     <script>
        function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode != 44 && charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }
    $(document).ready(function($) {
        $('#cb_nik').change(function() {
            if ($('#cb_nik').is(':checked')) {
                $('#nik').attr("disabled",false);
                $('#nik').val("");
            }else{
                $('#nik').attr("disabled",true);
                $('#nik').val("");
            }
            
        });
        $('#nik').on('paste', function() {
          var $el = $(this);
          setTimeout(function() {
            $el.val(function(i, val) {
              return val.replace(/[^0-9,]/g, '')
            })
          })
        });
        $(document).on('keypress',function(e) {
            if(e.which == 13) {
                get_table();
            }
        });
        get_table();
    });
    function on_clear() {
        $('#nik').val("");
        $('#niks').val("");
        $('#jum_nik').val("");
        get_table();
    }
    function on_serach(){
        get_table();
    }
    function do_req(){
        if (validationdaily()){
             $.ajax({
                    type: "post",
                    url: BASE_URL+"Req/Ektp",
                    dataType: "json",
                    data: {
                      "_token": "{{ csrf_token() }}",
                        niks :  $('#niks').val(),
                        jum_nik :  $('#jum_nik').val(),
                        reqstatus :  2

                    },
                    beforeSend:
                    function () {
                         block_screen();
                    },
                    success: function (data) {
                        console.log(data);
                        swal("Success!", "Berhasil Mengajukan "+ $("#jum_nik").val() +" Data, Untuk Pencetakan E-Ktp Karena Rusak", "success");
                    },
                    error:
                    function (data) {
                        unblock_screen();
                        swal("Error!", "Ooops!, Please Try Again, Something Went Wrong", "error"); 

                    },
                    complete:
                    function (response) {
                        unblock_screen();
                        get_table();
                    }
                });
           
        }
    }

    function validationdaily(){
        var jum_nik = $("#jum_nik");
            if (jum_nik.val() == 0) {                
                  swal("Warning!", "Data Tidak Boleh Kosong !", "warning");  
                 return false;
            }
            if (jum_nik.val().length == 0) {                
                  swal("Warning!", "Data Tidak Boleh Kosong !", "warning");  
                 return false;
            }
            return true;
    }
    function get_table() {
       $('#pengajuan-list').DataTable({
                destroy: true,
                 "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                 "columnDefs": [
                    {"className": "dt-left", "targets": "_all"}
                  ],
                "pageLength" : 500,
                "ajax": {
                 "url": BASE_URL+"ListPengajuan",
                "type": "post",
                "data": {
                "_token": "{{ csrf_token() }}",
                "nik":  $("#nik").val(),
                },
                complete: function(data) {
                    var niks = '';
                    $.each(data.responseJSON.data, function(key, value) {
                        // console.log(value[2])
                        niks = niks+value[2]
                    });
                    // console.log(niks);
                    $('#niks').val(niks);
                    $('#jum_nik').val(data.responseJSON.recordsTotal);
                }

                }
            });
       $('#pengajuan-listbtl').DataTable({
                destroy: true,
                 "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                 "columnDefs": [
                    {"className": "dt-left", "targets": "_all"}
                  ],
                "pageLength" : 500,
                "ajax": {
                 "url": BASE_URL+"ListPengajuanBatal",
                "type": "post",
                "data": {
                "_token": "{{ csrf_token() }}",
                "nik":  $("#nik").val(),
                },
                complete: function(data) {
                    
                }

                }
            });
          
    }
    function block_screen(){
        
        $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff'
            },
            baseZ: 2000
        }); 

    }
       
    function unblock_screen(){
       $.unblockUI();
    }

       
    </script>