     <script>
        function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode != 44 && charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }
     $(document).bind('keypress', function(e) {
            if(e.keyCode==13){
                 $('#btn-search').trigger('click');
             }
        });
        $(document).ready(function() {
            $('#capil-list').DataTable({
                 "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                "pageLength" : 10,
                "ajax": {
                 "url": BASE_URL+"Capil/get_salaman_batal",
                "type": "post",
                "data": {
                  "_token": "{{ csrf_token() }}",
                    "nik":  $('#capil_nik').val()
                }
                }
            });

    });
    function get_table() {
        if (validationdaily()){
        block_screen();
        $('#capil-list').DataTable({
                destroy: true,
                 "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                 'columnDefs': [
                  {
                      "targets": 0, // your case first column
                      "className": "text-center",
                 },
                 {
                      "targets": 1,
                      "className": "text-center",
                 },
                 {
                      "targets": 2,
                      "className": "text-center",
                 },
                 {
                      "targets": 3,
                      "className": "text-center",
                 },
                 {
                      "targets": 4,
                      "className": "text-center",
                 },
                 {
                      "targets": 5,
                      "className": "text-center",
                 },
                 {
                      "targets": 6,
                      "className": "text-center",
                 }],
                "pageLength" : 50,
                "ajax": {
                 "url": BASE_URL+"Capil/get_salaman_batal",
                "type": "post",
                "data": {
                  "_token": "{{ csrf_token() }}",
                    "nik":  $('#capil_nik').val()
                },
                complete: function() {
                    cek_exists();
                }
                }
            });
        
        unblock_screen();
        }
    }
    function refresh_table() {
         $('#capil-list').DataTable({
                destroy: true,
                 "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                "pageLength" : 10,
                "ajax": {
                 "url": BASE_URL+"Capil/get_salaman_batal",
                "type": "post",
                "data": {
                  "_token": "{{ csrf_token() }}",
                    "nik":  $('#capil_nik').val()
                }
                }
            });
    }
    function cek_exists(){
        var totalRecords =  $("#capil-list").DataTable().page.info().recordsTotal;
        if (totalRecords ==0){
            if ($('#capil_nik').val().length > 0){
        swal("Warning!", "Nik : "+$('#capil_nik').val()+" \n Tidak Ditemukan !", "warning");  
            }
        }
        $('#capil_nik').val("");
    }
    function validationdaily() {
        var nik = $("#capil_nik");
            if (nik.val().length == 0 ) {                
                  swal("Warning!", "Nik Tidak Boleh Kosong !", "warning");  
                  on_clear();
                 return false;
            }
            return true;
       
        }
        
    function on_clear() {
        $('#capil_nik').val("");
        $('#capil-list').DataTable({
                destroy: true,
                 "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                "pageLength" : 50,
                "ajax": {
                 "url": BASE_URL+"Capil/get_salaman_batal",
                "type": "post",
                "data": {
                  "_token": "{{ csrf_token() }}",
                    "nik":  $('#capil_nik').val()
                }
                }
            });
    }
    function block_screen(){
        $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff'
            },
            baseZ: 2000
        }); 
    }
    function unblock_screen(){
       $.unblockUI();
    }
    jQuery(document).ready(function() {
        $(".select2").select2();
        $('#tanggal').val("<?php echo date('d-m-Y');?>");

        $("#mdl_ststus_akta").change(function(){
          var mdl_ststus_akta =  $("#mdl_ststus_akta").val();
            if (mdl_ststus_akta == 1){
              $("#mdl_no_akta_lhr").attr("disabled",true);
              $("#go_send").attr("disabled",true);
            }else{
              $("#mdl_no_akta_lhr").attr("disabled",false);
              $("#go_send").attr("disabled",false);
            }
        });
        });

       
    function on_edit(pengajuan_id){
        swal({   
            title: "Apakah Anda Yakin ?",   
            text: "Memperbaiki Pengajuan "+pengajuan_id+" ?",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Ya, Perbaiki Pengajuan",   
            cancelButtonText: "Tidak, Jangan Diperbaiki",   
            closeOnConfirm: false,   
            closeOnCancel: false 
            }, function(isConfirm){   
                if (isConfirm) {
                $.ajax({
                    type: "post",
                    url: BASE_URL+"Capil/edit_pengajuan_batal",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        pengajuan_id : pengajuan_id
                    },
                    beforeSend:
                    function () {
                         block_screen();
                    },
                    success: function (data) {
                        console.log(data);
                         if (data.message_type > 0){
                              swal("Berhasil !", "Pengajuan "+pengajuan_id+" Telah Diperbaiki !", "success");
                              refresh_table();
                        }else{
                            swal("Warning!", data.message, "warning");  
                        }
                    },
                    error:
                    function (data) {
                        unblock_screen();
                        swal("Error!", "Ooops!, Please Try Again, Something Went Wrong", "error"); 

                    },
                    complete:
                    function (response) {
                        unblock_screen();
                    }
                });
                     
                } else {     
                    swal("Cancelled", "Pengajuan "+pengajuan_id+" Tidak Jadi Diperbaiki :)", "error");   
                } 
            });
    }
   
    </script>