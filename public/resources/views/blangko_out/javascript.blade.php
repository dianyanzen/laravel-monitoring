     <script>
        function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode != 44 && charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }
      $("#txt_pengambilan").keydown(function (e) {
            if (e.keyCode == 13) {
             e.preventDefault();
             on_save();
            }
            
          });

        $("#txt_rusak").keydown(function (e) {
            if (e.keyCode == 13) {
             e.preventDefault();
             on_save();
            }
            
          });

        $("#txt_pengembalian").keydown(function (e) {
            if (e.keyCode == 13) {
             e.preventDefault();
             on_save();
            }
            
          });

        $("#txt_penerima").keydown(function (e) {
            if (e.keyCode == 13) {
             e.preventDefault();
             on_save_dis();
            }
            
          });
    $(document).ready(function() {

    console.log(init_kec);
    console.log(init_level);
     $.ajax({
                    type: "post",
                    url: BASE_URL+"Shared_api/get_will",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        no_kec : init_kec
                    },
                    beforeSend:
                    function () {
                        $('select[name="no_kec"]').attr("disabled",true);
                    },
                    success: function (data) {
                        console.log(data);
                       $('select[name="no_kec"]').empty();
                        $.each(data, function(key, value) {
                            $('select[name="no_kec"]').append('<option value="'+ value.no_wil +'">'+ value.nama_wil +'</option>');
                        });
                        $('select[name="no_kec"]').val(init_kec).trigger("change");
                        
                    },
                    error:
                    function (data) {
               

                    },
                    complete:
                    function (response) {
                        $('select[name="no_kec"]').attr("disabled",false);
                        get_all_data();
                    }
                });
            $('select[name="no_kec"]').on('change', function() {
                get_data();

            });
            $.ajax({
                    type: "post",
                    url: BASE_URL+"api/ektp-kec",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        no_prop : 32,
                        no_kab : 73,
                        no_kec : init_kec
                    },
                    beforeSend:
                    function () {
                        $('select[name="no_kec_dis"]').attr("disabled",true);
                    },
                    success: function (data) {
                        console.log(data);
                       $('select[name="no_kec_dis"]').empty();
                       <?php if ($user_no_kec == 0){ ?>
                       $('select[name="no_kec_dis"]').append('<option value="0">-- Pilih Kecamatan --</option>');
                       <?php }else if($akses_kec > 0){ ?>
                        $('select[name="no_kec_dis"]').append('<option value="0">-- Pilih Kecamatan --</option>');
                       <?php } ?>
                        $.each(data.kecamatan, function(key, value) {
                            $('select[name="no_kec_dis"]').append('<option value="'+ value.no_kec +'">'+ value.nama_kec +'</option>');
                        });
                        <?php if ($user_no_kec == 0){ ?>
                       $('select[name="no_kec_dis"]').val("0").trigger("change");
                       <?php }else{ ?>
                        $('select[name="no_kec_dis"]').val("<?php echo $user_no_kec; ?>").trigger("change");
                        <?php } ?>
                        
                    },
                    error:
                    function (data) {
               

                    },
                    complete:
                    function (response) {
                        $('select[name="no_kec_dis"]').attr("disabled",false);
                    }
                });
                do_get_hist();
                $('select[name="no_kec_dis"]').on('change', function() {

                        var no_kec = $(this).val();

                        if(no_kec != 0) {
                            
                            $.ajax({
                                type: "post",
                                url: BASE_URL+"Blangko/get_hist_dist",
                                dataType: "json",
                                data: {
                                    "_token": "{{ csrf_token() }}",
                                    no_prop : 32,
                                    no_kab : 73,
                                    no_kec : no_kec
                                },
                                beforeSend:
                                function () {
                                    $("#tbl_hist_dist tbody").empty();
                                },
                                success: function (data) {
                                    console.log(data);
                                    var i = 0;
                                      $.each(data, function(key, value) {
                                        i = i+1;
                                        $("#tbl_hist_dist tbody").append(' <tr><td align="center">'+ i +'</td><td>'+ value.nama_kec +'</td><td>'+ value.update_dt +'</td><td>'+ value.penerima +'</td></tr>');
                                    });
                                    
                                },
                                error:
                                function (data) {
                           

                                },
                                complete:
                                function (response) {
                                }
                            });
                        }else{
                            $("#tbl_hist_dist tbody").empty();
                        }

                    });

    });
    function get_data(){
        $.ajax({
                    type: "post",
                    url: BASE_URL+"Blangko/get_data",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        no_kec : $("#no_kec").val(),
                        tanggal : $("#tanggal").val()
                    },
                    beforeSend:
                    function () {
                        
                    },
                    success: function (data) {
                        console.log(data);
                        $('#txt_pengambilan').val(data.pengambilan);
                        $('#txt_rusak').val(data.rusak);
                        $('#txt_pengembalian').val(data.pengembalian);
                        
                    },
                    error:
                    function (data) {
               

                    },
                    complete:
                    function (response) {
                        // $('select[name="no_kec"]').attr("disabled",false);
                        setTimeout(function () { get_data_m3(); }, 500);
                    }
                });
    }
    function get_all_data(){
        $.ajax({
                    type: "post",
                    url: BASE_URL+"Blangko/get_all_data",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        tanggal : $("#tanggal").val()
                    },
                    beforeSend:
                    function () {
                        
                    },
                    success: function (data) {
                        console.log(data);
                        $('#lbl_pengambilan').text(data.pengambilan);
                        $('#lbl_rusak').text(data.rusak);
                        $('#lbl_pengembalian').text(data.pengembalian);
                        $('#lbl_total').text(data.total);
                    },
                    error:
                    function (data) {
               

                    },
                    complete:
                    function (response) {
                        // $('select[name="no_kec"]').attr("disabled",false);
                    }
                });
    }
    function get_data_m3() {
       $.ajax({
                type: "post",
                url: BASE_URL+"dashboard/get_dashboard_m3",
                data: {"_token": "{{ csrf_token() }}",},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                            $('#sisa_prr').html(data.sisa_prr);
                },
                error:
                function (data) {


                },
                complete:
                function (response) {
                   
                   setTimeout(function () { get_data_m4(); }, 500);
                }
            });
        }
    function get_data_m4() {
       $.ajax({
                type: "post",
                url: BASE_URL+"dashboard/get_dashboard_m4",
                data: {"_token": "{{ csrf_token() }}",},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                            $('#sisa_sfe').html(data.sisa_sfe);
                },
                error:
                function (data) {


                },
                complete:
                function (response) {
                   
                   // setTimeout(function () { get_time_m3(); }, 500);
                }
            });
        }
    function get_time_m3() {
       $.ajax({
                type: "post",
                url: BASE_URL+"dashboard/get_time_m3",
                data: {"_token": "{{ csrf_token() }}",},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                            $('#jam_prr').html(data.jam_prr);
                },
                error:
                function (data) {


                },
                complete:
                function (response) {
                   
                   setTimeout(function () { get_time_m4(); }, 500);
                }
            });
        }
    function get_time_m4() {
       $.ajax({
                type: "post",
                url: BASE_URL+"dashboard/get_time_m4",
                data: {"_token": "{{ csrf_token() }}",},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                            $('#jam_sfe').html(data.jam_sfe);
                },
                error:
                function (data) {


                },
                complete:
                function (response) {
                   
                   // setTimeout(function () { get_data_m3(); }, 500);
                }
            });
        }
    function do_get_hist() {
                        $.ajax({
                                type: "post",
                                url: BASE_URL+"Blangko/get_histkec_dist",
                                dataType: "json",
                                data: {
                                    "_token": "{{ csrf_token() }}",
                                   
                                },
                                beforeSend:
                                function () {
                                    $("#tbl_hist_dist_kec tbody").empty();
                                },
                                success: function (data) {
                                    console.log(data);
                                    var i = 0;
                                      $.each(data, function(key, value) {
                                        i = i+1;
                                        $("#tbl_hist_dist_kec tbody").append(' <tr><td align="center">'+ i +'</td><td>'+ value.nama_kec +'</td><td>'+ value.update_dt +'</td></tr>');
                                    });
                                    
                                },
                                error:
                                function (data) {
                           

                                },
                                complete:
                                function (response) {
                                }
                            });
        }
    function on_clear() {
        
        $('select[name="no_kec"]').val(init_kec).trigger("change");
        $('#txt_pengambilan').val("0");
        $('#txt_rusak').val("0");
        $('#txt_pengembalian').val("0");
        $('#tanggal').val("<?php echo date('d-m-Y');?>");
    }
     function on_save(){
        if (validationdaily()){
          do_save();
        }
    }

     function on_save_dis(){
        if (validationdaily_dis()){
          do_save_dis();
        }
    }
    function validationdaily() {
        var pengambilan = $("#txt_pengambilan");
        var rusak = $("#txt_rusak");
        var pengembalian = $("#txt_pengembalian");
        var total_kembali = parseInt(rusak.val()) + parseInt(pengembalian.val());
        console.log(total_kembali);
            if (pengambilan.val().length == 0) {                
                  swal("Warning!", "Pengembalian Tidak Boleh Kosong !", "warning");  
                 return false;
            }
            if (rusak.val().length == 0) {                
                  swal("Warning!", "Keping Rusak Tidak Boleh Kosong !", "warning");  
                 return false;
            }
            if (pengembalian.val().length == 0) {                
                  swal("Warning!", "Pengembalian Tidak Boleh Kosong !", "warning");  
                 return false;
            }
            if( parseInt(pengambilan.val()) < total_kembali) {
                swal("Error!", "Pengembalian Tidak Boleh Lebih Banyak Dari Pengambilan", "error"); 
             return false; 
            }
            if( parseInt(pengambilan.val()) < 0) {
                swal("Error!", "Tidak Boleh Mengambil Kurang Dari 0", "error"); 
              return false; 
            }
            if (init_kec != 0){
             console.log(init_kec);
             swal("Error!", "Hanya Operator Dinas Saja Yang Boleh Menginput Kendali Blangko", "error"); 
              return false;    
            }
            return true;
       
        }
        function validationdaily_dis() {
        var no_kec_dis = $("#no_kec_dis");
        var tanggal_dis = $("#tanggal_dis");
        var txt_penerima = $("#txt_penerima");
            if (no_kec_dis.val() == 0) {                
                  swal("Warning!", "Pilih Kecamatan !", "warning");  
                 return false;
            }
            if (tanggal_dis.val().length == 0) {                
                  swal("Warning!", "Tanggal Tidak Boleh Kosong !", "warning");  
                 return false;
            }
            if (txt_penerima.val().length == 0) {                
                  swal("Warning!", "Penerima Tidak Boleh Kosong !", "warning");  
                 return false;
            }
            return true;
       
        }
        function do_save(){
            $.ajax({
                    type: "post",
                    url: BASE_URL+"Blangko/do_save",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        no_kec : $("#no_kec").val(),
                        tanggal : $("#tanggal").val(),
                        pengambilan : $("#txt_pengambilan").val(),
                        rusak : $("#txt_rusak").val(),
                        pengembalian : $("#txt_pengembalian").val()
                    },
                    beforeSend:
                    function () {
                        block_screen();
                    },
                    success: function (data) {
                        console.log(data);
                        swal("Success!", data.message, "success");
                    },
                    error:
                    function (data) {
                        unblock_screen();
                        swal("Error!", "Ooops!, Please Try Again, Something Went Wrong", "error"); 

                    },
                    complete:
                    function (response) {
                        unblock_screen();
                        get_all_data();
                    }
                });
        }
        function do_save_dis(){
            $.ajax({
                    type: "post",
                    url: BASE_URL+"Blangko/do_save_dis",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        no_kec : $("#no_kec_dis").val(),
                        tanggal : $("#tanggal_dis").val(),
                        penerima : $("#txt_penerima").val(),
                    },
                    beforeSend:
                    function () {
                        block_screen();
                    },
                    success: function (data) {
                         console.log(data);
                        if (!data.success){
                            swal("Error!", data.message, "error");
                        }else{
                            swal("Success!", data.message, "success");
                        }
                    },
                    error:
                    function (data) {
                        unblock_screen();
                        swal("Error!", "Ooops!, Please Try Again, Something Went Wrong", "error"); 

                    },
                    complete:
                    function (response) {
                        unblock_screen();
                        $('select[name="no_kec_dis"]').val("0").trigger("change");
                        do_get_hist();
                    }
                });
        }
        function update_prr(){
            $.ajax({
                    type: "post",
                    url: BASE_URL+"dashboard/update_prr",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}"
                    },
                    beforeSend:
                    function () {
                        block_screen();
                    },
                    success: function (data) {
                        console.log(data);
                        swal("Success!", data.message, "success");
                    },
                    error:
                    function (data) {
                        unblock_screen();
                        swal("Error!", "Ooops!, Please Try Again, Something Went Wrong", "error"); 

                    },
                    complete:
                    function (response) {
                        unblock_screen();
                        get_data_m3();
                    }
                });
        }
        function update_sfe(){
            $.ajax({
                    type: "post",
                    url: BASE_URL+"dashboard/update_sfe",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}"
                    },
                    beforeSend:
                    function () {
                        block_screen();
                    },
                    success: function (data) {
                        console.log(data);
                        swal("Success!", data.message, "success");
                    },
                    error:
                    function (data) {
                        unblock_screen();
                        swal("Error!", "Ooops!, Please Try Again, Something Went Wrong", "error"); 

                    },
                    complete:
                    function (response) {
                        unblock_screen();
                        get_data_m3();
                    }
                });
        }
     function block_screen(){
        
        $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff'
            },
            baseZ: 2000
        }); 
    }
    function unblock_screen(){
       $.unblockUI();
    }
    jQuery(document).ready(function() {
            $(".select2").select2();
            $('.input-daterange-datepicker').daterangepicker({
                buttonClasses: ['btn', 'btn-sm'],
                applyClass: 'btn-info',
                cancelClass: 'btn-info',
                locale: {
                    format: 'DD-MM-YYYY'
                },
                startDate: '<?php echo date('d-m-Y');?>',
                endDate: '<?php echo date('d-m-Y');?>'
            });
             $('#mytable').DataTable({
                 "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
        $('#tanggal').val("<?php echo date('d-m-Y');?>");
        $('#txt_pengambilan').val("0");
        $('#txt_rusak').val("0");
        $('#txt_pengembalian').val("0");
        });
    </script>
   