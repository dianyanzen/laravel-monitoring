 <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $stitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                        <ol class="breadcrumb">
                            <li class="active"><?php echo $mtitle; ?></li>
                        </ol>
                    </div>
                </div>
                 <div class="row">
                <!-- <form name ="get_form" action="<?php echo $my_url; ?>" method="post"> -->
                    <div class="col-md-12 col-xs-12">
                        <div class="white-box">
                        <ul class="nav nav-tabs tabs customtab">
                                    <li class="active tab">
                                        <a href="#master_vw" data-toggle="tab"> <span class="visible-xs"><i class="mdi mdi-auto-fix"></i></span> <span class="hidden-xs" style="color: blue !important">Pengeluaran Blangko E-Ktp</span> </a> 
                                    </li>
                                    <li class="tab">
                                        <a href="#dis_vw" data-toggle="tab"> <span class="visible-xs"><i class="mdi mdi-arrow-up-box"></i></span> <span class="hidden-xs" style="color: blue !important">Distribusi Keping</span> </a> 
                                    </li>
                                    <li class="tab">
                                        <a href="#prr_vw" data-toggle="tab"> <span class="visible-xs"><i class="mdi mdi-arrow-up-box"></i></span> <span class="hidden-xs" style="color: blue !important">Update Dashboard Monitoring</span> </a> 
                                    </li>
                                
                                   
                                </ul>
                        </div>
                            </div>
                            <div class="tab-content">
                                <div class="tab-pane active" id="master_vw">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div class="white-box">
                            <div class="row">
                                <div class="col-lg-6">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_kec" type="checkbox" checked disabled="true">
                                            <label for="cb_kec"> Request</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">         
                                    <div class="form-group" <?php if (!empty($is_colnotwil)){echo 'style="display: none!important;"'; }?>>
                                        <select class="form-control select2" name="no_kec" id="no_kec">
                                         <option  value="0">DINAS</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_tgl" type="checkbox" checked disabled="true">
                                            <label for="cb_tgl"> Tanggal</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">         
                                    <div class="form-group">
                                        <input class="form-control" id="tanggal" placeholder="dd/mm/yyyy" type="text" value="<?php echo date("d-m-Y") ?>"></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_tgl" type="checkbox" checked disabled="true">
                                            <label for="cb_tgl"> Pengambilan</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">         
                                    <div class="form-group">
                                        <input class="form-control" type="text" id="txt_pengambilan" value="0" onkeypress="return isNumberKey(event)" ></div>
                                </div>
                                 <div class="col-lg-6">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_tgl" type="checkbox" checked disabled="true">
                                            <label for="cb_tgl"> Rusak</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">         
                                    <div class="form-group">
                                        <input class="form-control" id="txt_rusak" type="text" value="0" onkeypress="return isNumberKey(event)" ></div>
                                </div>
                                 <div class="col-lg-6">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_tgl" type="checkbox" checked disabled="true">
                                            <label for="cb_tgl"> Pengembalian</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">         
                                    <div class="form-group">
                                        <input class="form-control" type="text" id="txt_pengembalian" value="0" onkeypress="return isNumberKey(event)"></div>
                                </div>
                                <div class="col-lg-3">         
                                    <div class="form-group">
                                        <!-- <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_nik" type="checkbox">
                                            <label for="cb_nik"> Nik</label>
                                        </div> -->
                                    </div>
                                </div>
                                <div class="col-lg-3">         
                                    <!-- <div class="form-group">
                                        <input class="form-control" type="text" id="nik" name="nik" disabled="true" onkeypress="return isNumberKey(event)" onchange="onlyNum()" /></div> -->
                                </div>
                            </div>
                            <div class="row">
                                <button type="submit" class="btn btn-info waves-effect waves-light m-r-10 pull-right" id="btn-filter" onclick="on_save();" >Save <i class="mdi  mdi-content-save fa-fw"></i></button>
                                  <button type="button" class="btn btn-invert waves-effect waves-light m-r-10 pull-right" id="btn-reset" onclick="on_clear();">Clear <i class="mdi  mdi-delete fa-fw"></i></button>
                            </div>
                        </div>
                    </div>
                    <!-- </form> -->
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">Kendali Blangko</div>
                            <div class="panel-wrapper collapse in">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th class="text-center">#</th>
                                            <th>Keterangan</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td align="center">1</td>
                                            <td>Request</td>
                                            <td>Total Blangko</td>
                                        </tr>
                                        <tr>
                                            <td align="center">2</td>
                                            <td>Tanggal</td>
                                            <td><?php echo date("d-m-Y") ?></td>
                                        </tr>
                                        <tr>
                                            <td align="center">3</td>
                                            <td>Pengambilan</td>
                                            <td><span id="lbl_pengambilan">0</span></td>
                                        </tr>
                                         <tr>
                                            <td align="center">4</td>
                                            <td>Rusak</td>
                                            <td><span id="lbl_rusak">0</span></td>
                                        </tr>
                                         <tr>
                                            <td align="center">5</td>
                                            <td>Pengembalian</td>
                                            <td><span id="lbl_pengembalian">0</span></td>
                                        </tr>
                                         <tr>
                                            <td align="center">6</td>
                                            <td>Yang Dibawa</td>
                                            <td><span id="lbl_total">0</span></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="tab-pane" id="dis_vw">
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <div class="white-box">
                                <div class="row">
                                    <div class="col-lg-6">         
                                        <div class="form-group">
                                            <div class="checkbox checkbox-info align-bottom">
                                                <input id="cb_kec" type="checkbox" checked disabled="true">
                                                <label for="cb_kec"> Kecamatan</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">         
                                        <div class="form-group" <?php if (!empty($is_colnotwil)){echo 'style="display: none!important;"'; }?>>
                                            <select class="form-control select2" name="no_kec_dis" id="no_kec_dis">
                                             <option  value="0">-- Pilih Kecamatan --</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">         
                                        <div class="form-group">
                                            <div class="checkbox checkbox-info align-bottom">
                                                <input id="cb_tgl_dis" type="checkbox" checked disabled="true">
                                                <label for="cb_tgl_dis"> Tanggal</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">         
                                        <div class="form-group">
                                            <input class="form-control" id="tanggal_dis" placeholder="dd/mm/yyyy" type="text" value="<?php echo date("d-m-Y") ?>"></div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">         
                                        <div class="form-group">
                                            <div class="checkbox checkbox-info align-bottom">
                                                <input id="cb_penerima_dis" type="checkbox" checked disabled="true">
                                                <label for="cb_penerima_dis"> Penerima Keping</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">         
                                        <div class="form-group">
                                            <input class="form-control" type="text" id="txt_penerima" value="" style="text-transform:uppercase"></div>
                                    </div>
                                    
                                    <div class="col-lg-3">         
                                        <div class="form-group">
                                            <!-- <div class="checkbox checkbox-info align-bottom">
                                                <input id="cb_nik" type="checkbox">
                                                <label for="cb_nik"> Nik</label>
                                            </div> -->
                                        </div>
                                    </div>
                                    <div class="col-lg-3">         
                                        <!-- <div class="form-group">
                                            <input class="form-control" type="text" id="nik" name="nik" disabled="true" onkeypress="return isNumberKey(event)" onchange="onlyNum()" /></div> -->
                                    </div>
                                </div>
                                <div class="row">
                                    <button type="submit" class="btn btn-info waves-effect waves-light m-r-10 pull-right" id="btn-filter_dis" onclick="on_save_dis();" >Save <i class="mdi  mdi-content-save fa-fw"></i></button>
                                      <button type="button" class="btn btn-invert waves-effect waves-light m-r-10 pull-right" id="btn-reset_dis" onclick="on_clear_dis();">Clear <i class="mdi  mdi-delete fa-fw"></i></button>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">History Pengambilan Keping</div>
                                <div class="panel-wrapper collapse in">
                                    <table class="table table-hover" name="tbl_hist_dist" id="tbl_hist_dist">
                                        <thead>
                                            <tr>
                                                <th class="text-center">#</th>
                                                <th>Kecamatan</th>
                                                <th>Tanggal Pengambilan</th>
                                                <th>Penerima</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                           
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">Pengambilan Terakhir Keping Perkecamatan</div>
                                <div class="panel-wrapper collapse in">
                                    <table class="table table-hover" name="tbl_hist_dist_kec" id="tbl_hist_dist_kec">
                                        <thead>
                                            <tr>
                                                <th class="text-center"><b>#</b></th>
                                                <th><b>Kecamatan</b></th>
                                                <th><b>Tanggal Pengambilan</b></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                           
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 
                <div class="tab-pane" id="prr_vw">
                             <div class="col-md-12 col-xs-12">
                            <div class="white-box">
                            <h3 class="box-title m-b-0">Update Dashboard Monitoring</h3>
                            <p class="text-muted m-b-30">Dinas Kependudukan Dan Pencatatan Sipil Kota Bandung</p>
                                    <div class="row">
                                        <div class="col-sm-6 col-lg-6">
                                            <div class="white-box">
                                                <h3 class="box-title">Print Ready Record Sampai Dengan Jam <span id="jam_prr">00:00</span></h3>
                                                <hr>
                                                <ul class="expense-box">
                                                    <li><i class="fa fa-paper-plane-o text-info"></i>
                                                        <div>
                                                            <a href ="#">
                                                                <h2><span id="sisa_prr">0</span></h2>
                                                                <h4>Print Ready Record</h4>
                                                            </a>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                       <div class="col-sm-6 col-lg-6">
                                        <div class="white-box">
                                            <h3 class="box-title">Sent For Enrollment Sampai Dengan Jam <span id="jam_sfe">00:00</span></h3>
                                                <hr>
                                                <ul class="expense-box">
                                                    <li><i class="ti-shopping-cart text-info"></i>
                                                        <div>
                                                            <a href ="#">
                                                                <h2><span id="sisa_sfe">0</span></h2>
                                                                <h4>Sent For Enrollment</h4>
                                                            </a>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                     </div><div class="row">
                                        <div class="col-sm-6 col-lg-6">
                                        <div class="white-box">
                                            <h3 class="box-title">Update Prr</h3>
                                            <hr>
                                                <div class="text-center">
                                                    <a href="#" onclick="update_prr()">
                                                        <i class="fa fa-sign-in fa-4x"></i>
                                                        <div class="huge">Update PRR</div>
                                                    </a>
                                                   
                                                </div>
                                        </div>
                                        </div>
                                       <div class="col-sm-6 col-lg-6">
                                        <div class="white-box">
                                            <h3 class="box-title">Update Sfe</h3>
                                            <hr>
                                                <div class="text-center">
                                                    <a href="#" onclick="update_sfe()">
                                                        <i class="fa fa-sign-out fa-4x"></i>
                                                        <div class="huge">Update Sfe</div>
                                                    </a>
                                                </div>
                                        </div>
                                        </div>
                                     </div>
                                </div>
                                    
                            </div>
                </div>
                </div>
                </div>
                <!-- <div class="row">
                <div class="col-md-12">
                        <div class="white-box">
                            <h3 class="box-title"><?php echo $mtitle; ?></h3>
                            <div class="scrollable">
                                <div class="table-responsive">
                                    <table id="mytable" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                    <thead>
                                     
                                        <tr>
                                            <th width="10%" style="text-align: center;">No Kec</th>
                                            <th width="15%" style="text-align: center;">Nik</th>
                                            <th width="20%" style="text-align: center;">Nama Lengkap</th>
                                            <th width="15%" style="text-align: center;">Status E-KTP</th>
                                            <th width="10%" style="text-align: center;">Req Date</th>
                                            <th width="10%" style="text-align: center;">Req By</th>
                                            <th width="10%" style="text-align: center;">Print Date</th>
                                            <th width="10%" style="text-align: center;">Print By</th>
                                            <th width="10%" style="text-align: center;">Nama Kecamatan</th>
                                            <th width="10%" style="text-align: center;">Nama Kelurahan</th>
                                        </tr>
                                       
                                    </thead>
                                    <tbody id="show_data">
                                       <?php
                                        if (!empty($data)){
                                           foreach($data as $row){?>
                                             <tr>
                                                <td width="10%" style="text-align: center;"><?php echo $row->NO_KEC ;?></td>
                                                <td width="15%" style="text-align: center;"><span style="color: #fff">,</span><?php echo $row->NIK ;?></td>
                                                <td width="20%" style="text-align: center;"><?php echo $row->NAMA_LENGKAP ;?></td>
                                                <td width="15%" style="text-align: center;"><?php echo $row->CURRENT_STATUS_CODE ;?></td>
                                                <td width="10%" style="text-align: center;"><?php echo $row->REQ_DATE ;?></td>
                                                <td width="10%" style="text-align: center;"><?php echo $row->REQ_BY ;?></td>
                                                <td width="10%" style="text-align: center;"><?php echo $row->PRINTED_DATE ;?></td>
                                                <td width="10%" style="text-align: center;"><?php echo $row->PRINTED_BY ;?></td>
                                                <td width="10%" style="text-align: center;"><?php echo $row->NAMA_KEC ;?></td>
                                                <td width="10%" style="text-align: center;"><?php echo $row->NAMA_KEL ;?></td>
                                            </tr>
                                          <?php } ?>
                                          <?php } ?>
                                       
                                        
                                    </tbody>
                                    
                                        <tfoot id="my_foot">
                                            <tr>
                                            <?php if (!empty($jumlah)){ ?>
                                            <th width="80%" colspan="9" style="text-align: center;">Jumlah</th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah),0,',','.');?></th>
                                            <?php } ?>
                                        </tr>

                                            </tfoot>
                                </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> -->
                 </div>
       @include('shared.footer_detail')