 <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $stitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                        <ol class="breadcrumb">
                           <li class="text-info"><?php echo $stitle; ?></li>
                        </ol>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                        <div class="white-box">
                            <div class="row">
                                <div class="col-xs-6">
                                    <h2 class="font-medium m-t-0">Jumlah Antrian <small><span id="loket_name"></span></small></h2>
                                    <h5 class="text-muted m-t-0">Tanggal <?php echo date('d-m-Y');?></h5>
                                </div>
                            </div>
                            <div class="row minus-margin">
                                 <div class="col-md-6 col-lg-6 col-xs-12 col-sm-6 b-t b-r b-b b-l">
                                    <ul class="expense-box">
                                        <li><i class="ti-headphone-alt text-info"></i>
                                            <div>
                                                <h2><span id="antrian_today">0</span></h2>
                                                <h4>Antrian Hari Ini</h4>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-6 col-lg-6 col-xs-12 col-sm-6 b-t b-r b-b b-l">
                                    <ul class="expense-box">
                                        <li><i class="ti-home text-info"></i>
                                            <div>
                                                <h2><span id="antrian_tomorrow">0</span></h2>
                                                <h4>Antrian Besok</h4>
                                            </div>
                                        </li>
                                    </ul>
                                </div> 
                            </div>
                            <div class="row" style="margin-top:  5px">
                                <div class="col-lg-12">         
                                    <div class="form-group">
                                        <h3 class="box-title m-b-0"><b>Loket</b></h3>
                                        <select class="form-control select2" name="kdloket" id="kdloket">
                                <option  value="0">-- Pilih Loket --</option></select>
                            </div>
                        </div>
                        </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                <div class="col-md-12">
                        <div class="white-box">
                            <h3 class="box-title"><?php echo $mtitle; ?> Hari Ini</h3>
                            <div class="scrollable">
                                <div class="table-responsive">
                                    <table id="antrian-list" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                   <thead>
                                    
                                        <tr>
                                            <th width="10%" style="text-align: center;">No</th>
                                            <th width="15%" style="text-align: center;">Nik</th>
                                            <th width="15%" style="text-align: center;">Nama</th>
                                            <th width="15%" style="text-align: center;">Layanan</th>
                                            <th width="10%" style="text-align: center;">Urutan</th>
                                            <th width="15%" style="text-align: center;">Tanggal</th>
                                            <th width="10%" style="text-align: center;">Jam</th>
                                            <th width="10%" style="text-align: center;">Hari</th>
                                            <th width="10%" style="text-align: center;">Telepon</th>

                                        </tr>
                                       
                                    </thead>
                                    <tbody id="show_data">
                                       
                                        
                                    </tbody>
                                    
                                </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
       @include('shared.footer_detail')