<!DOCTYPE html>
<html lang="en">
@include('shared.head')
<body class="fix-header">
    <div id="wrapper">
            @include('shared.nav_top')
            @include('shared/nav_side')
            @include('user_helpdesk_lihat/content')
    </div>
     
    @include('shared/footer')
    
    @include('user_helpdesk_lihat/javascript')
</body>
</html>