     <script>
        function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode != 44 && charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }
    
    $(document).ready(function() {
        console.log(init_kec);
        console.log(init_level);
        get_all_data();
    });
    function get_all_data(){
        $.ajax({
                    type: "post",
                    url: BASE_URL+"Blangko/get_gudang_data",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        tanggal : $("#tanggal").val()
                    },
                    beforeSend:
                    function () {
                       
                    },
                    success: function (data) {
                        console.log(data);
                        $('#blk_dinas').text(data.blk_dinas);
                        $('#blk_gudang').text(data.blk_gudang);
                        $('#blk_dafduk').text(data.blk_dafduk);
                        $('#txt_jumlah').val(data.blk_gudang);
                    },
                    error:
                    function (data) {
               

                    },
                    complete:
                    function (response) {

                        // $('select[name="no_kec"]').attr("disabled",false);
                    }
                });
    }
    function on_clear() {
        $('#txt_jumlah').val("0");
        $('#tanggal').val("<?php echo date('d-m-Y');?>");
        get_all_data();
    }
     function on_save(){
        if (validationdaily()){
          do_save();
        }
  
        
    }
    function validationdaily() {
        var blangko_gudang = $("#txt_jumlah");
        console.log(blangko_gudang);
            if (blangko_gudang.val().length == 0) {                
                  swal("Warning!", "Input Blangko Tidak Boleh Kosong !", "warning");  
                 return false;
            }
            if (blangko_gudang.val() > parseInt($('#blk_dinas').text())) {                
                  swal("Warning!", "Saldo Blangko Tidak Cukup !", "warning");  
                 return false;
            }
           
            if (init_kec != 0){
             console.log(init_kec);
             swal("Error!", "Hanya Operator Dinas Saja Yang Boleh Menginput Kendali Blangko", "error"); 
              return false;    
            }
            return true;
       
        }
        function do_save(){
            $.ajax({
                    type: "post",
                    url: BASE_URL+"Blangko/doin_save_gudang",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        blangko_gudang : $("#txt_jumlah").val()
                    },
                    beforeSend:
                    function () {
                         block_screen();
                    },
                    success: function (data) {
                        console.log(data);
                        swal("Success!", data.message, "success");
                    },
                    error:
                    function (data) {
                        unblock_screen();
                        swal("Error!", "Ooops!, Please Try Again, Something Went Wrong", "error"); 

                    },
                    complete:
                    function (response) {
                        unblock_screen();
                        on_clear();
                    }
                });
        }
    function block_screen(){
        
        $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff'
            },
            baseZ: 2000
        }); 
    }
    function unblock_screen(){
       $.unblockUI();
    }
    jQuery(document).ready(function() {
            $(".select2").select2();
            $('.input-daterange-datepicker').daterangepicker({
                buttonClasses: ['btn', 'btn-sm'],
                applyClass: 'btn-info',
                cancelClass: 'btn-info',
                locale: {
                    format: 'DD-MM-YYYY'
                },
                startDate: '<?php echo date('d-m-Y');?>',
                endDate: '<?php echo date('d-m-Y');?>'
            });
             $('#mytable').DataTable({
                 "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
        $('#txt_jumlah').val("0");
        $('#tanggal').val("<?php echo date('d-m-Y');?>");
        });
    </script>
   