    <script src="{{ url('/') }}/assets/plugins/bower_components/owl.carousel/owl.carousel.min.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/owl.carousel/owl.custom.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/flot/jquery.flot.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/Minimal-Gauge-chart/js/cmGauge.js"></script>
    <script type="text/javascript">
    $(document).ready(function () {
        setTimeout(function () { get_data(); }, 5000);
        setTimeout(function () { get_data_detail(); }, 5000);
    });
    function get_detail_pindah(data){
        if(data == 0){
              $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff' 
            },
            baseZ: 2000
        }); 
            window.location.href = "{{ url('/') }}/Detail/gdmpt";
        }else{
              $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff' 
            },
            baseZ: 2000
        }); 
            window.location.href = "{{ url('/') }}/Detail/gdmpy";
        }
    }
    function get_detail_datang(data){
        if(data == 0){
              $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff' 
            },
            baseZ: 2000
        }); 
            window.location.href = "{{ url('/') }}/Detail/gdmdt";
        }else{
              $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff' 
            },
            baseZ: 2000
        }); 
            window.location.href = "{{ url('/') }}/Detail/gdmdy";
        }
    }
    function get_data() {
       $.ajax({
                type: "post",
                url: BASE_URL+"Dashboard_mobilitas/get_data",
                data: {"_token": "{{ csrf_token() }}",},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                            $('#pindah_hn').html(data.pindah_hn);
                            $('#pindah_dn').html(data.pindah_dn);
                            $('#datang_hn').html(data.datang_hn);
                            $('#datang_dn').html(data.datang_dn);
                            $('#pindah_hy').html(data.pindah_hy);
                            $('#pindah_dy').html(data.pindah_dy);
                            $('#datang_hy').html(data.datang_hy);
                            $('#datang_dy').html(data.datang_dy);
                            
                },
                error:
                function (data) {


                },
                complete:
                function (response) {
                   
                   setTimeout(function () { get_data(); }, 5000);
                }
            });
        }
        function get_data_detail() {
       $.ajax({
                type: "post",
                url: BASE_URL+"Dashboard_mobilitas/get_data_detail",
                data: {"_token": "{{ csrf_token() }}",},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                            $('#pindah_hn_akab').html(data.pindah_hn_akab);
                            $('#pindah_dn_akab').html(data.pindah_dn_akab);
                            $('#pindah_hn_akec').html(data.pindah_hn_akec);
                            $('#pindah_dn_akec').html(data.pindah_dn_akec);
                            $('#pindah_hn_dkec').html(data.pindah_hn_dkec);
                            $('#pindah_dn_dkec').html(data.pindah_dn_dkec);
                            $('#datang_hn_akab').html(data.datang_hn_akab);
                            $('#datang_dn_akab').html(data.datang_dn_akab);
                            $('#datang_hn_akec').html(data.datang_hn_akec);
                            $('#datang_dn_akec').html(data.datang_dn_akec);
                            $('#datang_hn_dkec').html(data.datang_hn_dkec);
                            $('#datang_dn_dkec').html(data.datang_dn_dkec);
                            $('#pindah_hy_akab').html(data.pindah_hy_akab);
                            $('#pindah_dy_akab').html(data.pindah_dy_akab);
                            $('#pindah_hy_akec').html(data.pindah_hy_akec);
                            $('#pindah_dy_akec').html(data.pindah_dy_akec);
                            $('#pindah_hy_dkec').html(data.pindah_hy_dkec);
                            $('#pindah_dy_dkec').html(data.pindah_dy_dkec);
                            $('#datang_hy_akab').html(data.datang_hy_akab);
                            $('#datang_dy_akab').html(data.datang_dy_akab);
                            $('#datang_hy_akec').html(data.datang_hy_akec);
                            $('#datang_dy_akec').html(data.datang_dy_akec);
                            $('#datang_hy_dkec').html(data.datang_hy_dkec);
                            $('#datang_dy_dkec').html(data.datang_dy_dkec);
                            
                },
                error:
                function (data) {


                },
                complete:
                function (response) {
                   
                   setTimeout(function () { get_data(); }, 5000);
                }
            });
        }
    </script>
    