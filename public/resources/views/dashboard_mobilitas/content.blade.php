<div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $stitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                        <ol class="breadcrumb">
                             <li><a href="{{ url('/') }}/<?php echo $backurl; ?>"><?php echo $back_title; ?></a></li>
                           <li class="text-info"><?php echo $stitle; ?></li>
                        </ol>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-sm-12 col-xs-12">
                        <div class="row">
                            <div class="col-lg-12 col-sm-12 col-xs-12">
                                <div class="panel panel-info">
                                    <div class="panel-heading">Surat Pindah Hari Ini<div class="pull-right"><a href="#" data-perform="panel-collapse"><i class="ti-minus"></i></a></div>
                                         </div>
                                         <div class="panel-wrapper collapse in" aria-expanded="true">
                                        <div class="panel-body">
                                    <ul class="list-inline two-part">
                                        <li><i class="mdi mdi-file-document-box text-info"></i></li>
                                        <li class="text-right"><span class="counter"><a href ="#" onclick="get_detail_pindah(0)"><span id="pindah_hn">0</span></a></span></li>
                                    </ul>
                                    <ul class="list-inline two-part">
                                        <li><i class="mdi mdi-account-multiple text-info"></i></li>
                                        <li class="text-right"><span class="counter"><a href ="#" onclick="get_detail_pindah(0)">(<span id="pindah_dn">0</span>)</a></span></li>
                                    </ul>
                                    <ul class="feeds">
                                        <li>
                                            <div class="bg-info"><i class="mdi mdi-sync text-white"></i></div><a href ="#" style="color: #292961 !important;"> Pindah Antar Kabupaten <span id="pindah_hn_akab" style="float: none; font-size: auto;">0</span> (<span id="pindah_dn_akab" style="float: none; font-size: auto;" style="float: none; font-size: auto;">0</span>)</a></li>
                                        <li>
                                            <div class="bg-info"><i class="mdi mdi-sync text-white"></i></div><a href ="#" style="color: #292961 !important;"> Pindah Antar Kecamatan <span id="pindah_hn_akec" style="float: none; font-size: auto;">0</span> (<span id="pindah_dn_akec" style="float: none; font-size: auto;" style="float: none; font-size: auto;">0</span>)</a></li>
                                        <li>
                                            <div class="bg-info"><i class="mdi mdi-sync text-white"></i></div><a href ="#" style="color: #292961  !important;"> Pindah Dalam Kecamatan <span id="pindah_hn_dkec" style="float: none; font-size: auto;">0</span> (<span id="pindah_dn_dkec" style="float: none; font-size: auto;" style="float: none; font-size: auto;">0</span>)</a></li>
                                        
                                    </ul>
                                    <!-- <button class="btn btn-info btn-rounded waves-effect waves-light m-t-20" onclick="do_pindah_n();">Lihat Detail</button> -->
                                </div>
                                </div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-sm-12 col-xs-12">
                                <div class="panel panel-info">
                                    <div class="panel-heading">Surat Kedatangan Hari Ini<div class="pull-right"><a href="#" data-perform="panel-collapse"><i class="ti-minus"></i></a></div>
                                         </div>
                                         <div class="panel-wrapper collapse in" aria-expanded="true">
                                        <div class="panel-body">
                                    <ul class="list-inline two-part">
                                        <li><i class="mdi mdi-file-document-box text-info"></i></li>
                                        <li class="text-right"><span class="counter"><a href ="#" onclick="get_detail_datang(0)"><span id="datang_hn">0</span></a></span></li>
                                    </ul>
                                    <ul class="list-inline two-part">
                                        <li><i class="mdi mdi-account-multiple text-info"></i></li>
                                        <li class="text-right"><span class="counter"><a href ="#" onclick="get_detail_datang(0)">(<span id="datang_dn">0</span>)</a></span></li>
                                    </ul>
                                    <ul class="feeds">
                                        <li>
                                            <div class="bg-info"><i class="mdi mdi-sync text-white"></i></div><a href ="#" style="color: #292961 !important;"> Datang Antar Kabupaten <span id="datang_hn_akab" style="float: none; font-size: auto;">0</span> (<span id="datang_dn_akab" style="float: none; font-size: auto;" style="float: none; font-size: auto;">0</span>)</a></li>
                                        <li>
                                            <div class="bg-info"><i class="mdi mdi-sync text-white"></i></div><a href ="#" style="color: #292961 !important;"> Datang Antar Kecamatan <span id="datang_hn_akec" style="float: none; font-size: auto;">0</span> (<span id="datang_dn_akec" style="float: none; font-size: auto;" style="float: none; font-size: auto;">0</span>)</a></li>
                                        <li>
                                            <div class="bg-info"><i class="mdi mdi-sync text-white"></i></div><a href ="#" style="color: #292961  !important;"> Datang Dalam Kecamatan <span id="datang_hn_dkec" style="float: none; font-size: auto;">0</span> (<span id="datang_dn_dkec" style="float: none; font-size: auto;" style="float: none; font-size: auto;">0</span>)</a></li>
                                        
                                    </ul>
                                    <!-- <button class="btn btn-info btn-rounded waves-effect waves-light m-t-20" onclick="do_datang_n();">Lihat Detail</button> -->
                                </div>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-12 col-xs-12">
                        <div class="row">
                            <div class="col-lg-12 col-sm-12 col-xs-12">
                               <div class="panel panel-info">
                                    <div class="panel-heading">Surat Pindah Kemarin<div class="pull-right"><a href="#" data-perform="panel-collapse"><i class="ti-minus"></i></a></div>
                                         </div>
                                         <div class="panel-wrapper collapse in" aria-expanded="true">
                                        <div class="panel-body">
                                    <ul class="list-inline two-part">
                                        <li><i class="mdi mdi-file-document-box text-info"></i></li>
                                        <li class="text-right"><span class="counter"><a href ="#" onclick="get_detail_pindah(1)"><span id="pindah_hy">0</span></a></span></li>
                                    </ul>
                                    <ul class="list-inline two-part">
                                        <li><i class="mdi mdi-account-multiple text-info"></i></li>
                                        <li class="text-right"><span class="counter"><a href ="#" onclick="get_detail_pindah(1)">(<span id="pindah_dy">0</span>)</a></span></li>
                                    </ul>
                                    <ul class="feeds">
                                        <li>
                                            <div class="bg-info"><i class="mdi mdi-sync text-white"></i></div><a href ="#" style="color: #292961 !important;"> Pindah Antar Kabupaten <span id="pindah_hy_akab" style="float: none; font-size: auto;">0</span> (<span id="pindah_dy_akab" style="float: none; font-size: auto;" style="float: none; font-size: auto;">0</span>)</a></li>
                                        <li>
                                            <div class="bg-info"><i class="mdi mdi-sync text-white"></i></div><a href ="#" style="color: #292961 !important;"> Pindah Antar Kecamatan <span id="pindah_hy_akec" style="float: none; font-size: auto;">0</span> (<span id="pindah_dy_akec" style="float: none; font-size: auto;" style="float: none; font-size: auto;">0</span>)</a></li>
                                        <li>
                                            <div class="bg-info"><i class="mdi mdi-sync text-white"></i></div><a href ="#" style="color: #292961  !important;"> Pindah Dalam Kecamatan <span id="pindah_hy_dkec" style="float: none; font-size: auto;">0</span> (<span id="pindah_dy_dkec" style="float: none; font-size: auto;" style="float: none; font-size: auto;">0</span>)</a></li>
                                        
                                    </ul>
                                    <!-- <button class="btn btn-info btn-rounded waves-effect waves-light m-t-20" onclick="do_pindah_y();">Lihat Detail</button> -->
                                </div>
                                </div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-sm-12 col-xs-12">
                               <div class="panel panel-info">
                                    <div class="panel-heading">Surat Kedatangan Kemarin<div class="pull-right"><a href="#" data-perform="panel-collapse"><i class="ti-minus"></i></a></div>
                                         </div>
                                         <div class="panel-wrapper collapse in" aria-expanded="true">
                                        <div class="panel-body">
                                    <ul class="list-inline two-part">
                                        <li><i class="mdi mdi-file-document-box text-info"></i></li>
                                        <li class="text-right"><span class="counter"><a href ="#" onclick="get_detail_datang(1)"><span id="datang_hy">0</span></a></span></li>
                                    </ul>
                                    <ul class="list-inline two-part" style="margin-top: 0 !important; ">
                                        <li style="margin-top: 0 !important; "><i class="mdi mdi-account-multiple text-info"></i></li>
                                        <li class="text-right" style="margin-top: 0 !important; "><span class="counter"><a href ="#" onclick="get_detail_datang(1)">(<span id="datang_dy">0</span>)</a></span></li>
                                    </ul>
                                    <ul class="feeds">
                                        <li>
                                            <div class="bg-info"><i class="mdi mdi-sync text-white"></i></div><a href ="#" style="color: #292961 !important;"> Datang Antar Kabupaten <span id="datang_hy_akab" style="float: none; font-size: auto;">0</span> (<span id="datang_dy_akab" style="float: none; font-size: auto;" style="float: none; font-size: auto;">0</span>)</a></li>
                                        <li>
                                            <div class="bg-info"><i class="mdi mdi-sync text-white"></i></div><a href ="#" style="color: #292961 !important;"> Datang Antar Kecamatan <span id="datang_hy_akec" style="float: none; font-size: auto;">0</span> (<span id="datang_dy_akec" style="float: none; font-size: auto;" style="float: none; font-size: auto;">0</span>)</a></li>
                                        <li>
                                            <div class="bg-info"><i class="mdi mdi-sync text-white"></i></div><a href ="#" style="color: #292961  !important;"> Datang Dalam Kecamatan <span id="datang_hy_dkec" style="float: none; font-size: auto;">0</span> (<span id="datang_dy_dkec" style="float: none; font-size: auto;" style="float: none; font-size: auto;">0</span>)</a></li>
                                        
                                    </ul>
                                    <!-- <button class="btn btn-info btn-rounded waves-effect waves-light m-t-20" onclick="do_datang_y();">Lihat Detail</button> -->
                                </div>
                                </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
            </div>
        </div>
       @include('shared.footer_detail')