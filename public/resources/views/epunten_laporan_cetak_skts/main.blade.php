<!DOCTYPE html>
<html lang="en">
@include('shared.head')
<body class="fix-header">
    <div id="wrapper">
            @include('shared.nav_top')
            @include('shared/nav_side')
            @include('epunten_laporan_cetak_skts/content')
    </div>
     
    @include('shared/footer')
    
    @include('epunten_laporan_cetak_skts/javascript')
</body>
</html>