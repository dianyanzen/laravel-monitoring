<!DOCTYPE html>
<html lang="en">
@include('shared.head')
<body class="fix-header">
    <div id="wrapper">
            @include('shared.nav_top')
            @include('shared/nav_side')
            @include('absensi_co/content')
    </div>
     
    @include('shared/footer')
    
    @include('absensi_co/javascript')
</body>
</html>