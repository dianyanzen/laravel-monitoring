<div id="page-wrapper">
            <div class="container-fluid">
               <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $mtitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                
                        <ol class="breadcrumb">
                            <li><a href="{{ url('/') }}/">Laporan</a></li>
                            <li class="active"><?php echo $mtitle; ?></li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- .row -->
                <div class="row">
                <form name ="get_form" action="<?php echo $my_url; ?>" method="post">
                    @csrf
                    <div class="col-sm-12">
                        <div class="white-box">
                            <div class="row">
                                <div class="col-lg-12">         
                                    <div class="form-group">
                                        <h3 class="box-title m-b-0"><b>Bulan Laporan</b></h3>
                                        <!-- <input type="text" placeholder="" data-mask="99/99/9999" class="form-control">  -->
                                        <input class="form-control" type="text" id="bulan" data-mask="99/9999" name="bulan" onkeypress="return isNumberKey(event)" onchange="onlyNum()" />
                            </select>
                            
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <button type="submit" class="btn btn-info waves-effect waves-light m-r-10 pull-right" id="btn-filter" onclick="on_serach();" >Search <i class="mdi  mdi-magnify fa-fw"></i></button>
                                  <button type="button" class="btn btn-invert waves-effect waves-light m-r-10 pull-right" id="btn-reset" onclick="on_clear();">Clear <i class="mdi  mdi-delete fa-fw"></i></button>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
                <div class="row">
                <div class="col-sm-12">
                        <div class="white-box">
                            <h3 class="box-title m-b-0"><?php echo $stitle; ?></h3>
                            <p class="text-muted m-b-30">Dinas Kependudukan Dan Pencatatan Sipil Kota Bandung</p>
                            <div class="table-responsive">
                                <table id="mytable" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                   <thead>
                                        <tr>
                                            
                                            <th width="10%" style="text-align: right;"><?php echo $no_wil ;?></th>
                                            <th width="20%" style="text-align: left;"><?php echo $kode_wil ;?></th>
                                            <th width="20%" style="text-align: right;">Jumlah Laki-Laki</th>
                                            <th width="20%" style="text-align: right;">Jumlah Perempuan</th>
                                            <th width="20%" style="text-align: right;">Jumlah Penduduk</th>
                                            <th width="20%" style="text-align: right;">Jumlah WKP Laki-Laki</th>
                                            <th width="20%" style="text-align: right;">Jumlah WKP Perempuan</th>
                                            <th width="20%" style="text-align: right;">Jumlah WKP</th>
                                            <th width="20%" style="text-align: right;">Sudah Rekam</th>
                                            <th width="20%" style="text-align: right;">Belum Rekam</th>
                                            <th width="20%" style="text-align: right;">Sudah Cetak</th>
                                            <th width="20%" style="text-align: right;">PRR</th>
                                            <th width="20%" style="text-align: right;">Duplicate</th>
                                            <th width="20%" style="text-align: right;">Bio Captured</th>
                                            <th width="20%" style="text-align: right;">SFE</th>
                                            <th width="20%" style="text-align: right;">Gagal Rekam</th>
                                        </tr>
                                    </thead>
                                    <tbody id="my_data">
                                          <?php
                                            $SUM_JUMDUK_LK = 0;
                                            $SUM_JUMDUK_PR = 0;
                                            $SUM_JUMDUK = 0;
                                            $SUM_WKP_LK = 0;
                                            $SUM_WKP_PR = 0;
                                            $SUM_WKP = 0;
                                            $SUM_SUDAHRKM = 0;
                                            $SUM_BLMRKM = 0;
                                            $SUM_SUDAHCTK = 0;
                                            $SUM_PRR = 0;
                                            $SUM_DUPREC = 0;
                                            $SUM_BIOCAPTURE = 0;
                                            $SUM_SFE = 0;
                                            $SUM_GAGALRKM = 0;
                                        if (!empty($data)){
                                           foreach($data as $row){?>
                                             <tr>
                                                <td width="10%" style="text-align: right;"><?php echo $row->no_wil ;?></td>
                                                <td width="20%" style="text-align: left;"><?php echo $row->nama_wil ;?></td>
                                                <th width="20%" style="text-align: right;"><?php echo $row->jumduk_lk ;?></th>
                                                <th width="20%" style="text-align: right;"><?php echo $row->jumduk_pr ;?></th>
                                                <th width="20%" style="text-align: right;"><?php echo $row->jumduk ;?></th>
                                                <th width="20%" style="text-align: right;"><?php echo $row->wkp_lk ;?></th>
                                                <th width="20%" style="text-align: right;"><?php echo $row->wkp_pr ;?></th>
                                                <th width="20%" style="text-align: right;"><?php echo $row->wkp ;?></th>
                                                <th width="20%" style="text-align: right;"><?php echo $row->sudahrkm ;?></th>
                                                <th width="20%" style="text-align: right;"><?php echo $row->blmrkm ;?></th>
                                                <th width="20%" style="text-align: right;"><?php echo $row->sudahctk ;?></th>
                                                <th width="20%" style="text-align: right;"><?php echo $row->prr ;?></th>
                                                <th width="20%" style="text-align: right;"><?php echo $row->duprec ;?></th>
                                                <th width="20%" style="text-align: right;"><?php echo $row->biocapture ;?></th>
                                                <th width="20%" style="text-align: right;"><?php echo $row->sfe ;?></th>
                                                <th width="20%" style="text-align: right;"><?php echo $row->gagalrkm ;?></th>
                                                <?php 
                                                $SUM_JUMDUK_LK += $row->jumduk_lk;
                                                $SUM_JUMDUK_PR += $row->jumduk_pr; 
                                                $SUM_JUMDUK += $row->jumduk;
                                                $SUM_WKP_LK += $row->wkp_lk;
                                                $SUM_WKP_PR += $row->wkp_pr;
                                                $SUM_WKP += $row->wkp;
                                                $SUM_SUDAHRKM += $row->sudahrkm;
                                                $SUM_BLMRKM += $row->blmrkm;
                                                $SUM_SUDAHCTK += $row->sudahctk;
                                                $SUM_PRR += $row->prr;
                                                $SUM_DUPREC += $row->duprec;
                                                $SUM_BIOCAPTURE += $row->biocapture;
                                                $SUM_SFE += $row->sfe;
                                                $SUM_GAGALRKM += $row->gagalrkm;
                                                ?>
                                            </tr>
                                         <?php }
                                         } ?>
                                    </tbody>
                                   <?php
                                    if (!empty($data)){
                                    ?>
                                     <tfoot id="my_foot">
                                         <tr>
                                            <th width="30%" colspan="2" style="text-align: center;">Jumlah</th>
                                            <th width="15%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_JUMDUK_LK),0,',','.');?></th>
                                            <th width="15%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_JUMDUK_PR),0,',','.');?></th>
                                            <th width="15%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_JUMDUK),0,',','.');?></th>
                                            <th width="15%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_WKP_LK),0,',','.');?></th>
                                            <th width="15%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_WKP_PR),0,',','.');?></th>
                                            <th width="15%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_WKP),0,',','.');?></th>
                                            <th width="15%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_SUDAHRKM),0,',','.');?></th>
                                            <th width="15%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_BLMRKM),0,',','.');?></th>
                                            <th width="15%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_SUDAHCTK),0,',','.');?></th>
                                            <th width="15%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_PRR),0,',','.');?></th>
                                            <th width="15%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_DUPREC),0,',','.');?></th>
                                            <th width="15%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_BIOCAPTURE),0,',','.');?></th>
                                            <th width="15%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_SFE),0,',','.');?></th>
                                            <th width="15%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_GAGALRKM),0,',','.');?></th>
                                        </tr>
                                    </tfoot>
                                    <?php } ?>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
           @include('shared.footer_detail')
        </div>