     <script>
        function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode != 44 && charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }
    
    $(document).ready(function() {
    var init_user_id = <?php Session::get('S_USER_ID') ;?>
    console.log(init_user_id);
     $.ajax({
                    type: "post",
                    url: BASE_URL+"Shared_api/get_user_daily",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        user_id : init_user_id
                    },
                    beforeSend:
                    function () {
                        $('select[name="user_id"]').attr("disabled",true);
                    },
                    success: function (data) {
                        console.log(data);
                       $('select[name="user_id"]').empty();
                       $('select[name="user_id"]').append('<option value="0">-- Pilih User --</option>');
                        $.each(data, function(key, value) {
                            $('select[name="user_id"]').append('<option value="'+ value.user_id +'">'+ value.user_id +'</option>');
                        });
                        $('select[name="user_id"]').val(init_user_id).trigger("change");
                    },
                    error:
                    function (data) {
               

                    },
                    complete:
                    function (response) {
                        $('select[name="user_id"]').attr("disabled",false);
                    }
                });


    });
    function on_clear() {
        
        $('select[name="user_id"]').val(init_user_id).trigger("change");
        $('#tanggal').val("<?php echo date('d-m-Y');?> - <?php echo date('d-m-Y');?>");
    }
    function on_serach(){
        
        $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff'
            },
            baseZ: 2000
        }); 
    }
    jQuery(document).ready(function() {
            $(".select2").select2();
            $('.input-daterange-datepicker').daterangepicker({
                buttonClasses: ['btn', 'btn-sm'],
                applyClass: 'btn-info',
                cancelClass: 'btn-info',
                locale: {
                    format: 'DD-MM-YYYY'
                },
                startDate: '<?php echo date('d-m-Y');?>',
                endDate: '<?php echo date('d-m-Y');?>'
            });
             $('#mytable').DataTable({
                "pageLength": 50 ,
                 "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
           $('#tanggal').val("<?php echo date('d-m-Y');?> - <?php echo date('d-m-Y');?>");
        });
    </script>
   