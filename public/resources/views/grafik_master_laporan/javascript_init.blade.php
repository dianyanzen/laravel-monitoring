<?php if (!empty($jenis_lap)){ ?>
<?php if ($jenis_lap == 1 || $jenis_lap == 5 || $jenis_lap == 27 || $jenis_lap == 28){ ?>
<script type="text/javascript">
    $( document ).ready(function() {
    var ctx2 = document.getElementById("Bar_chart").getContext("2d");
    var data2 = {
        labels: [
            "LAKI-LAKI",
            "PEREMPUAN",
            ],
        datasets: [
            {
                label: "Grafik Bar",
                fillColor: "#0584f2",
                strokeColor: "#0584f2",
                highlightFill: "#0444bf",
                highlightStroke: "#0444bf",
                data: [
                <?php echo $jumlah[0]->lk ;?>,
                <?php echo $jumlah[0]->pr ;?>,
                ]
            }
        ]
    };
    
    
    var Bar_chart = new Chart(ctx2).Bar(data2, {
        scaleBeginAtZero : true,
        scaleShowGridLines : true,
        scaleGridLineColor : "rgba(0,0,0,.005)",
        scaleGridLineWidth : 0,
        scaleShowHorizontalLines: true,
        scaleShowVerticalLines: true,
        barShowStroke : true,
        barStrokeWidth : 0,
        tooltipCornerRadius: 2,
        barDatasetSpacing : 3,
        legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
        responsive: true
    });
    
    var ctx3 = document.getElementById("Pie_chart").getContext("2d");
    var data3 = [
        {
            value: <?php echo $jumlah[0]->lk ;?>,
            color: "#0584f2",
            highlight: "#0444bf",
            label: "LAKI-LAKI"
        },
        {
            value: <?php echo $jumlah[0]->pr ;?>,
            color: "#0aaff1",
            highlight: "#0444bf",
            label: "PEREMPUAN"
        },
    ];
    
    var myPieChart = new Chart(ctx3).Pie(data3,{
        segmentShowStroke : true,
        segmentStrokeColor : "#fff",
        segmentStrokeWidth : 0,
        animationSteps : 100,
        tooltipCornerRadius: 0,
        animationEasing : "easeOutBounce",
        animateRotate : true,
        animateScale : false,
        legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>",
        responsive: true
    });
 
    
});
</script>
<?php } ?>
<?php if ($jenis_lap == 2 || $jenis_lap == 3 || $jenis_lap == 4){ ?>
<script type="text/javascript">
    $( document ).ready(function() {
    var ctx2 = document.getElementById("Bar_chart").getContext("2d");
    var data2 = {
        labels: [
            "0-4 Tahun",
            "5-9 Tahun",
            "10-14 Tahun",
            "15-19 Tahun",
            "20-24 Tahun",
            "25-29 Tahun",
            "30-34 Tahun",
            "35-39 Tahun",
            "40-44 Tahun",
            "45-49 Tahun",
            "50-54 Tahun",
            "55-59 Tahun",
            "60-64 Tahun",
            "65-69 Tahun",
            "70-74 Tahun",
            "75> Tahun",
            ],
        datasets: [
            {
                label: "Grafik Bar",
                fillColor: "#0584f2",
                strokeColor: "#0584f2",
                highlightFill: "#0444bf",
                highlightStroke: "#0444bf",
                data: [
                <?php echo $jumlah[0]->r1 ;?>,
                <?php echo $jumlah[0]->r2 ;?>,
                <?php echo $jumlah[0]->r3 ;?>,
                <?php echo $jumlah[0]->r4 ;?>,
                <?php echo $jumlah[0]->r5 ;?>,
                <?php echo $jumlah[0]->r6 ;?>,
                <?php echo $jumlah[0]->r7 ;?>,
                <?php echo $jumlah[0]->r8 ;?>,
                <?php echo $jumlah[0]->r9 ;?>,
                <?php echo $jumlah[0]->r10 ;?>,
                <?php echo $jumlah[0]->r11 ;?>,
                <?php echo $jumlah[0]->r12 ;?>,
                <?php echo $jumlah[0]->r13 ;?>,
                <?php echo $jumlah[0]->r14 ;?>,
                <?php echo $jumlah[0]->r15 ;?>,
                <?php echo $jumlah[0]->r16 ;?>,
                ]
            }
        ]
    };
    
    var Bar_chart = new Chart(ctx2).Bar(data2, {
        scaleBeginAtZero : true,
        scaleShowGridLines : true,
        scaleGridLineColor : "rgba(0,0,0,.005)",
        scaleGridLineWidth : 0,
        scaleShowHorizontalLines: true,
        scaleShowVerticalLines: true,
        barShowStroke : true,
        barStrokeWidth : 0,
        tooltipCornerRadius: 2,
        barDatasetSpacing : 3,
        legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
        responsive: true
    });
    
    var ctx3 = document.getElementById("Pie_chart").getContext("2d");
    var data3 = [
        {
            value: <?php echo $jumlah[0]->r1 ;?>,
            color: "#0584f2",
            highlight: "#0444bf",
            label: "0-4 Tahun"
        },
        {
            value: <?php echo $jumlah[0]->r2 ;?>,
            color: "#0aaff1",
            highlight: "#0444bf",
            label: "5-9 Tahun"
        },
        {
            value: <?php echo $jumlah[0]->r3 ;?>,
            color: "#0584f2",
            highlight: "#0444bf",
            label: "10-14 Tahun"
        },
        {
            value: <?php echo $jumlah[0]->r4 ;?>,
            color: "#0aaff1",
            highlight: "#0444bf",
            label: "15-19 Tahun"
        },
        {
            value: <?php echo $jumlah[0]->r5 ;?>,
            color: "#0584f2",
            highlight: "#0444bf",
            label: "20-24 Tahun"
        },
        {
            value: <?php echo $jumlah[0]->r6 ;?>,
            color: "#0aaff1",
            highlight: "#0444bf",
            label: "25-29 Tahun"
        },
        {
            value: <?php echo $jumlah[0]->r7 ;?>,
            color: "#0584f2",
            highlight: "#0444bf",
            label: "30-34 Tahun"
        },
        {
            value: <?php echo $jumlah[0]->r8 ;?>,
            color: "#0aaff1",
            highlight: "#0444bf",
            label: "35-39 Tahun"
        },
        {
            value: <?php echo $jumlah[0]->r9 ;?>,
            color: "#0584f2",
            highlight: "#0444bf",
            label: "40-44 Tahun"
        },
        {
            value: <?php echo $jumlah[0]->r10 ;?>,
            color: "#0aaff1",
            highlight: "#0444bf",
            label: "45-49 Tahun"
        },
        {
            value: <?php echo $jumlah[0]->r11 ;?>,
            color: "#0584f2",
            highlight: "#0444bf",
            label: "50-54 Tahun"
        },
        {
            value: <?php echo $jumlah[0]->r12 ;?>,
            color: "#0aaff1",
            highlight: "#0444bf",
            label: "55-59 Tahun"
        },
        {
            value: <?php echo $jumlah[0]->r13 ;?>,
            color: "#0584f2",
            highlight: "#0444bf",
            label: "60-64 Tahun"
        },
        {
            value: <?php echo $jumlah[0]->r14 ;?>,
            color: "#0aaff1",
            highlight: "#0444bf",
            label: "65-69 Tahun"
        },
        {
            value: <?php echo $jumlah[0]->r15 ;?>,
            color: "#0584f2",
            highlight: "#0444bf",
            label: "70-74 Tahun"
        },
        {
            value: <?php echo $jumlah[0]->r16 ;?>,
            color: "#0aaff1",
            highlight: "#0444bf",
            label: "75> Tahun"
        },
    ];
    
    var myPieChart = new Chart(ctx3).Pie(data3,{
        segmentShowStroke : true,
        segmentStrokeColor : "#fff",
        segmentStrokeWidth : 0,
        animationSteps : 100,
        tooltipCornerRadius: 0,
        animationEasing : "easeOutBounce",
        animateRotate : true,
        animateScale : false,
        legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>",
        responsive: true
    });
 
    
});
</script>
<?php } ?>
<?php if ($jenis_lap == 6 || $jenis_lap == 7 || $jenis_lap == 8){ ?>
<script type="text/javascript">
    $( document ).ready(function() {
    var ctx2 = document.getElementById("Bar_chart").getContext("2d");
    var data2 = {
        labels: [
            "Belum Sekolah",
            "Tidak Tamat SD",
            "Tamat SD",
            "SLTP",
            "SLTA",
            "Diploma II",
            "Diploma III",
            "SI",
            "SII",
            "SIII",
            ],
        datasets: [
            {
                label: "Grafik Bar",
                fillColor: "#0584f2",
                strokeColor: "#0584f2",
                highlightFill: "#0444bf",
                highlightStroke: "#0444bf",
                data: [
                <?php echo $jumlah[0]->r1 ;?>,
                <?php echo $jumlah[0]->r2 ;?>,
                <?php echo $jumlah[0]->r3 ;?>,
                <?php echo $jumlah[0]->r4 ;?>,
                <?php echo $jumlah[0]->r5 ;?>,
                <?php echo $jumlah[0]->r6 ;?>,
                <?php echo $jumlah[0]->r7 ;?>,
                <?php echo $jumlah[0]->r8 ;?>,
                <?php echo $jumlah[0]->r9 ;?>,
                <?php echo $jumlah[0]->r10 ;?>,
                ]
            }
        ]
    };
    
    var Bar_chart = new Chart(ctx2).Bar(data2, {
        scaleBeginAtZero : true,
        scaleShowGridLines : true,
        scaleGridLineColor : "rgba(0,0,0,.005)",
        scaleGridLineWidth : 0,
        scaleShowHorizontalLines: true,
        scaleShowVerticalLines: true,
        barShowStroke : true,
        barStrokeWidth : 0,
        tooltipCornerRadius: 2,
        barDatasetSpacing : 3,
        legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
        responsive: true
    });
    
    var ctx3 = document.getElementById("Pie_chart").getContext("2d");
    var data3 = [
        {
            value: <?php echo $jumlah[0]->r1 ;?>,
            color: "#0584f2",
            highlight: "#0444bf",
            label: "Belum Sekolah"
        },
        {
            value: <?php echo $jumlah[0]->r2 ;?>,
            color: "#0aaff1",
            highlight: "#0444bf",
            label: "Tidak Tamat SD"
        },
        {
            value: <?php echo $jumlah[0]->r3 ;?>,
            color: "#0584f2",
            highlight: "#0444bf",
            label: "Tamat SD"
        },
        {
            value: <?php echo $jumlah[0]->r4 ;?>,
            color: "#0aaff1",
            highlight: "#0444bf",
            label: "SLTP"
        },
        {
            value: <?php echo $jumlah[0]->r5 ;?>,
            color: "#0584f2",
            highlight: "#0444bf",
            label: "SLTA"
        },
        {
            value: <?php echo $jumlah[0]->r6 ;?>,
            color: "#0aaff1",
            highlight: "#0444bf",
            label: "Diploma II"
        },
        {
            value: <?php echo $jumlah[0]->r7 ;?>,
            color: "#0584f2",
            highlight: "#0444bf",
            label: "Diploma III"
        },
        {
            value: <?php echo $jumlah[0]->r8 ;?>,
            color: "#0aaff1",
            highlight: "#0444bf",
            label: "SI"
        },
        {
            value: <?php echo $jumlah[0]->r9 ;?>,
            color: "#0584f2",
            highlight: "#0444bf",
            label: "SII"
        },
        {
            value: <?php echo $jumlah[0]->r10 ;?>,
            color: "#0aaff1",
            highlight: "#0444bf",
            label: "SIII"
        },
    ];
    
    var myPieChart = new Chart(ctx3).Pie(data3,{
        segmentShowStroke : true,
        segmentStrokeColor : "#fff",
        segmentStrokeWidth : 0,
        animationSteps : 100,
        tooltipCornerRadius: 0,
        animationEasing : "easeOutBounce",
        animateRotate : true,
        animateScale : false,
        legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>",
        responsive: true
    });
 
    
});
</script>
<?php } ?>
<?php if ($jenis_lap == 9 || $jenis_lap == 10 || $jenis_lap == 11){ ?>
<script type="text/javascript">
    $( document ).ready(function() {
    var ctx2 = document.getElementById("Bar_chart").getContext("2d");
    var data2 = {
        labels: [
            "Belum Tidak Bekerja",
            "Mengurus Rumah Tangga",
            "Pelajar/Mahasiswa",
            "Aparatur Sipil Negara",
            "Tni/Polri",
            "Pensiunan",
            "Karyawan Swasta",
            "Karyawan BUMN/BUMD",
            "Tenaga Medis",
            "Wiraswasta",
            "Pengajar (Dosen/Guru)",
            "Pekerjaan Lainya",
            ],
        datasets: [
            {
                label: "Grafik Bar",
                fillColor: "#0584f2",
                strokeColor: "#0584f2",
                highlightFill: "#0444bf",
                highlightStroke: "#0444bf",
                data: [
                <?php echo $jumlah[0]->r1 ;?>,
                <?php echo $jumlah[0]->r2 ;?>,
                <?php echo $jumlah[0]->r3 ;?>,
                <?php echo $jumlah[0]->r4 ;?>,
                <?php echo $jumlah[0]->r5 ;?>,
                <?php echo $jumlah[0]->r6 ;?>,
                <?php echo $jumlah[0]->r7 ;?>,
                <?php echo $jumlah[0]->r8 ;?>,
                <?php echo $jumlah[0]->r9 ;?>,
                <?php echo $jumlah[0]->r10 ;?>,
                <?php echo $jumlah[0]->r11 ;?>,
                <?php echo $jumlah[0]->r12 ;?>,
                ]
            }
        ]
    };
    
    var Bar_chart = new Chart(ctx2).Bar(data2, {
        scaleBeginAtZero : true,
        scaleShowGridLines : true,
        scaleGridLineColor : "rgba(0,0,0,.005)",
        scaleGridLineWidth : 0,
        scaleShowHorizontalLines: true,
        scaleShowVerticalLines: true,
        barShowStroke : true,
        barStrokeWidth : 0,
        tooltipCornerRadius: 2,
        barDatasetSpacing : 3,
        legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
        responsive: true
    });
    
    var ctx3 = document.getElementById("Pie_chart").getContext("2d");
    var data3 = [
        {
            value: <?php echo $jumlah[0]->r1 ;?>,
            color: "#0584f2",
            highlight: "#0444bf",
            label: "Belum Tidak Bekerja"
        },
        {
            value: <?php echo $jumlah[0]->r2 ;?>,
            color: "#0aaff1",
            highlight: "#0444bf",
            label: "Mengurus Rumah Tangga"
        },
        {
            value: <?php echo $jumlah[0]->r3 ;?>,
            color: "#0584f2",
            highlight: "#0444bf",
            label: "Pelajar/Mahasiswa"
        },
        {
            value: <?php echo $jumlah[0]->r4 ;?>,
            color: "#0aaff1",
            highlight: "#0444bf",
            label: "Aparatur Sipil Negara"
        },
        {
            value: <?php echo $jumlah[0]->r5 ;?>,
            color: "#0584f2",
            highlight: "#0444bf",
            label: "Tni/Polri"
        },
        {
            value: <?php echo $jumlah[0]->r6 ;?>,
            color: "#0aaff1",
            highlight: "#0444bf",
            label: "Pensiunan"
        },
        {
            value: <?php echo $jumlah[0]->r7 ;?>,
            color: "#0584f2",
            highlight: "#0444bf",
            label: "Karyawan Swasta"
        },
        {
            value: <?php echo $jumlah[0]->r8 ;?>,
            color: "#0aaff1",
            highlight: "#0444bf",
            label: "Karyawan BUMN/BUMD"
        },
        {
            value: <?php echo $jumlah[0]->r9 ;?>,
            color: "#0584f2",
            highlight: "#0444bf",
            label: "Tenaga Medis"
        },
        {
            value: <?php echo $jumlah[0]->r10 ;?>,
            color: "#0aaff1",
            highlight: "#0444bf",
            label: "Wiraswasta"
        },
        {
            value: <?php echo $jumlah[0]->r11 ;?>,
            color: "#0584f2",
            highlight: "#0444bf",
            label: "Pengajar (Dosen/Guru)"
        },
        {
            value: <?php echo $jumlah[0]->r12 ;?>,
            color: "#0aaff1",
            highlight: "#0444bf",
            label: "Pekerjaan Lainya"
        },
    ];
    
    var myPieChart = new Chart(ctx3).Pie(data3,{
        segmentShowStroke : true,
        segmentStrokeColor : "#fff",
        segmentStrokeWidth : 0,
        animationSteps : 100,
        tooltipCornerRadius: 0,
        animationEasing : "easeOutBounce",
        animateRotate : true,
        animateScale : false,
        legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>",
        responsive: true
    });
 
    
});
</script>
<?php } ?>
<?php if ($jenis_lap == 12 || $jenis_lap == 13 || $jenis_lap == 14){ ?>
<script type="text/javascript">
    $( document ).ready(function() {
    var ctx2 = document.getElementById("Bar_chart").getContext("2d");
    var data2 = {
        labels: [
            "Belum Kawin",
            "Kawin",
            "Cerai Hidup",
            "Cerai Mati",
            ],
        datasets: [
            {
                label: "Grafik Bar",
                fillColor: "#0584f2",
                strokeColor: "#0584f2",
                highlightFill: "#0444bf",
                highlightStroke: "#0444bf",
                data: [
                <?php echo $jumlah[0]->r1 ;?>,
                <?php echo $jumlah[0]->r2 ;?>,
                <?php echo $jumlah[0]->r3 ;?>,
                <?php echo $jumlah[0]->r4 ;?>,
                ]
            }
        ]
    };
    
    var Bar_chart = new Chart(ctx2).Bar(data2, {
        scaleBeginAtZero : true,
        scaleShowGridLines : true,
        scaleGridLineColor : "rgba(0,0,0,.005)",
        scaleGridLineWidth : 0,
        scaleShowHorizontalLines: true,
        scaleShowVerticalLines: true,
        barShowStroke : true,
        barStrokeWidth : 0,
        tooltipCornerRadius: 2,
        barDatasetSpacing : 3,
        legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
        responsive: true
    });
    
    var ctx3 = document.getElementById("Pie_chart").getContext("2d");
    var data3 = [
        {
            value: <?php echo $jumlah[0]->r1 ;?>,
            color: "#0584f2",
            highlight: "#0444bf",
            label: "Belum Kawin"
        },
        {
            value: <?php echo $jumlah[0]->r2 ;?>,
            color: "#0aaff1",
            highlight: "#0444bf",
            label: "Kawin"
        },
        {
            value: <?php echo $jumlah[0]->r3 ;?>,
            color: "#0584f2",
            highlight: "#0444bf",
            label: "Cerai Hidup"
        },
        {
            value: <?php echo $jumlah[0]->r4 ;?>,
            color: "#0aaff1",
            highlight: "#0444bf",
            label: "Cerai Mati"
        },
    ];
    
    var myPieChart = new Chart(ctx3).Pie(data3,{
        segmentShowStroke : true,
        segmentStrokeColor : "#fff",
        segmentStrokeWidth : 0,
        animationSteps : 100,
        tooltipCornerRadius: 0,
        animationEasing : "easeOutBounce",
        animateRotate : true,
        animateScale : false,
        legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>",
        responsive: true
    });
 
    
});
</script>
<?php } ?>
<?php if ($jenis_lap == 15 || $jenis_lap == 16 || $jenis_lap == 17){ ?>
<script type="text/javascript">
    $( document ).ready(function() {
    var ctx2 = document.getElementById("Bar_chart").getContext("2d");
    var data2 = {
        labels: [
                "Islam",
                "Kristen",
                "Katholik",
                "Hindu",
                "Budha",
                "Kong Huchu",
                "Kepercayaan",
            ],
        datasets: [
            {
                label: "Grafik Bar",
                fillColor: "#0584f2",
                strokeColor: "#0584f2",
                highlightFill: "#0444bf",
                highlightStroke: "#0444bf",
                data: [
                <?php echo $jumlah[0]->r1 ;?>,
                <?php echo $jumlah[0]->r2 ;?>,
                <?php echo $jumlah[0]->r3 ;?>,
                <?php echo $jumlah[0]->r4 ;?>,
                <?php echo $jumlah[0]->r5 ;?>,
                <?php echo $jumlah[0]->r6 ;?>,
                <?php echo $jumlah[0]->r7 ;?>,
                ]
            }
        ]
    };
    
    var Bar_chart = new Chart(ctx2).Bar(data2, {
        scaleBeginAtZero : true,
        scaleShowGridLines : true,
        scaleGridLineColor : "rgba(0,0,0,.005)",
        scaleGridLineWidth : 0,
        scaleShowHorizontalLines: true,
        scaleShowVerticalLines: true,
        barShowStroke : true,
        barStrokeWidth : 0,
        tooltipCornerRadius: 2,
        barDatasetSpacing : 3,
        legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
        responsive: true
    });
    
    var ctx3 = document.getElementById("Pie_chart").getContext("2d");
    var data3 = [
        {
            value: <?php echo $jumlah[0]->r1 ;?>,
            color: "#0584f2",
            highlight: "#0444bf",
            label: "Islam"
        },
        {
            value: <?php echo $jumlah[0]->r2 ;?>,
            color: "#0aaff1",
            highlight: "#0444bf",
            label: "Kristen"
        },
        {
            value: <?php echo $jumlah[0]->r3 ;?>,
            color: "#0584f2",
            highlight: "#0444bf",
            label: "Katholik"
        },
        {
            value: <?php echo $jumlah[0]->r4 ;?>,
            color: "#0aaff1",
            highlight: "#0444bf",
            label: "Hindu"
        },
        {
            value: <?php echo $jumlah[0]->r5 ;?>,
            color: "#0584f2",
            highlight: "#0444bf",
            label: "Budha"
        },
        {
            value: <?php echo $jumlah[0]->r6 ;?>,
            color: "#0aaff1",
            highlight: "#0444bf",
            label: "Kong Huchu"
        },
        {
            value: <?php echo $jumlah[0]->r7 ;?>,
            color: "#0584f2",
            highlight: "#0444bf",
            label: "Kepercayaan"
        },
    ];
    
    var myPieChart = new Chart(ctx3).Pie(data3,{
        segmentShowStroke : true,
        segmentStrokeColor : "#fff",
        segmentStrokeWidth : 0,
        animationSteps : 100,
        tooltipCornerRadius: 0,
        animationEasing : "easeOutBounce",
        animateRotate : true,
        animateScale : false,
        legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>",
        responsive: true
    });
 
    
});
</script>
<?php } ?>
<?php if ($jenis_lap == 18 || $jenis_lap == 19 || $jenis_lap == 20){ ?>
<script type="text/javascript">
    $( document ).ready(function() {
    var ctx2 = document.getElementById("Bar_chart").getContext("2d");
    var data2 = {
        labels: [
            "A",
            "B",
            "AB",
            "O",
            "A(+)",
            "A(-)",
            "B(+)",
            "B(-)",
            "AB(+)",
            "AB(-)",
            "O(+)",
            "O(-)",
            "Tidak Tahu",
            ],
        datasets: [
            {
                label: "Grafik Bar",
                fillColor: "#0584f2",
                strokeColor: "#0584f2",
                highlightFill: "#0444bf",
                highlightStroke: "#0444bf",
                data: [
                <?php echo $jumlah[0]->r1 ;?>,
                <?php echo $jumlah[0]->r2 ;?>,
                <?php echo $jumlah[0]->r3 ;?>,
                <?php echo $jumlah[0]->r4 ;?>,
                <?php echo $jumlah[0]->r5 ;?>,
                <?php echo $jumlah[0]->r6 ;?>,
                <?php echo $jumlah[0]->r7 ;?>,
                <?php echo $jumlah[0]->r8 ;?>,
                <?php echo $jumlah[0]->r9 ;?>,
                <?php echo $jumlah[0]->r10 ;?>,
                <?php echo $jumlah[0]->r11 ;?>,
                <?php echo $jumlah[0]->r12 ;?>,
                <?php echo $jumlah[0]->r13 ;?>,
                ]
            }
        ]
    };
    
    var Bar_chart = new Chart(ctx2).Bar(data2, {
        scaleBeginAtZero : true,
        scaleShowGridLines : true,
        scaleGridLineColor : "rgba(0,0,0,.005)",
        scaleGridLineWidth : 0,
        scaleShowHorizontalLines: true,
        scaleShowVerticalLines: true,
        barShowStroke : true,
        barStrokeWidth : 0,
        tooltipCornerRadius: 2,
        barDatasetSpacing : 3,
        legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
        responsive: true
    });
    
    var ctx3 = document.getElementById("Pie_chart").getContext("2d");
    var data3 = [
        {
            value: <?php echo $jumlah[0]->r1 ;?>,
            color: "#0584f2",
            highlight: "#0444bf",
            label: "A"
        },
        {
            value: <?php echo $jumlah[0]->r2 ;?>,
            color: "#0aaff1",
            highlight: "#0444bf",
            label: "B"
        },
        {
            value: <?php echo $jumlah[0]->r3 ;?>,
            color: "#0584f2",
            highlight: "#0444bf",
            label: "AB"
        },
        {
            value: <?php echo $jumlah[0]->r4 ;?>,
            color: "#0aaff1",
            highlight: "#0444bf",
            label: "O"
        },
        {
            value: <?php echo $jumlah[0]->r5 ;?>,
            color: "#0584f2",
            highlight: "#0444bf",
            label: "A(+)"
        },
        {
            value: <?php echo $jumlah[0]->r6 ;?>,
            color: "#0aaff1",
            highlight: "#0444bf",
            label: "A(-)"
        },
        {
            value: <?php echo $jumlah[0]->r7 ;?>,
            color: "#0584f2",
            highlight: "#0444bf",
            label: "B(+)"
        },
        {
            value: <?php echo $jumlah[0]->r8 ;?>,
            color: "#0aaff1",
            highlight: "#0444bf",
            label: "B(-)"
        },
        {
            value: <?php echo $jumlah[0]->r9 ;?>,
            color: "#0584f2",
            highlight: "#0444bf",
            label: "AB(+)"
        },
        {
            value: <?php echo $jumlah[0]->r10 ;?>,
            color: "#0aaff1",
            highlight: "#0444bf",
            label: "AB(-)"
        },
        {
            value: <?php echo $jumlah[0]->r11 ;?>,
            color: "#0584f2",
            highlight: "#0444bf",
            label: "O(+)"
        },
        {
            value: <?php echo $jumlah[0]->r12 ;?>,
            color: "#0aaff1",
            highlight: "#0444bf",
            label: "O(-)"
        },
        {
            value: <?php echo $jumlah[0]->r13 ;?>,
            color: "#0584f2",
            highlight: "#0444bf",
            label: "Tidak Tahu"
        },
    ];
    
    var myPieChart = new Chart(ctx3).Pie(data3,{
        segmentShowStroke : true,
        segmentStrokeColor : "#fff",
        segmentStrokeWidth : 0,
        animationSteps : 100,
        tooltipCornerRadius: 0,
        animationEasing : "easeOutBounce",
        animateRotate : true,
        animateScale : false,
        legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>",
        responsive: true
    });
 
    
});
</script>
<?php } ?>
<?php if ($jenis_lap == 21 || $jenis_lap == 22 || $jenis_lap == 23){ ?>
<script type="text/javascript">
    $( document ).ready(function() {
    var ctx2 = document.getElementById("Bar_chart").getContext("2d");
    var data2 = {
        labels: [
            "Cacat Fisik",
            "Tuna Netra",
            "Tuna Rungu",
            "Cacat Mental",
            "Cacat Fisik Mental",
            "Lainnya",
            ],
        datasets: [
            {
                label: "Grafik Bar",
                fillColor: "#0584f2",
                strokeColor: "#0584f2",
                highlightFill: "#0444bf",
                highlightStroke: "#0444bf",
                data: [
                <?php echo $jumlah[0]->r1 ;?>,
                <?php echo $jumlah[0]->r2 ;?>,
                <?php echo $jumlah[0]->r3 ;?>,
                <?php echo $jumlah[0]->r4 ;?>,
                <?php echo $jumlah[0]->r5 ;?>,
                <?php echo $jumlah[0]->r6 ;?>,
                ]
            }
        ]
    };
    
    var Bar_chart = new Chart(ctx2).Bar(data2, {
        scaleBeginAtZero : true,
        scaleShowGridLines : true,
        scaleGridLineColor : "rgba(0,0,0,.005)",
        scaleGridLineWidth : 0,
        scaleShowHorizontalLines: true,
        scaleShowVerticalLines: true,
        barShowStroke : true,
        barStrokeWidth : 0,
        tooltipCornerRadius: 2,
        barDatasetSpacing : 3,
        legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
        responsive: true
    });
    
    var ctx3 = document.getElementById("Pie_chart").getContext("2d");
    var data3 = [
        {
            value: <?php echo $jumlah[0]->r1 ;?>,
            color: "#0584f2",
            highlight: "#0444bf",
            label: "Cacat Fisik"
        },
        {
            value: <?php echo $jumlah[0]->r2 ;?>,
            color: "#0aaff1",
            highlight: "#0444bf",
            label: "Tuna Netra"
        },
        {
            value: <?php echo $jumlah[0]->r3 ;?>,
            color: "#0584f2",
            highlight: "#0444bf",
            label: "Tuna Rungu"
        },
        {
            value: <?php echo $jumlah[0]->r4 ;?>,
            color: "#0aaff1",
            highlight: "#0444bf",
            label: "Cacat Mental"
        },
        {
            value: <?php echo $jumlah[0]->r5 ;?>,
            color: "#0584f2",
            highlight: "#0444bf",
            label: "Cacat Fisik Mental"
        },
        {
            value: <?php echo $jumlah[0]->r6 ;?>,
            color: "#0aaff1",
            highlight: "#0444bf",
            label: "Lainnya"
        },
    ];
    
    var myPieChart = new Chart(ctx3).Pie(data3,{
        segmentShowStroke : true,
        segmentStrokeColor : "#fff",
        segmentStrokeWidth : 0,
        animationSteps : 100,
        tooltipCornerRadius: 0,
        animationEasing : "easeOutBounce",
        animateRotate : true,
        animateScale : false,
        legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>",
        responsive: true
    });
 
    
});
</script>
<?php } ?>
<?php if ($jenis_lap == 24 || $jenis_lap == 25 || $jenis_lap == 26){ ?>
<script type="text/javascript">
    $( document ).ready(function() {
    var ctx2 = document.getElementById("Bar_chart").getContext("2d");
    var data2 = {
        labels: [
            "60-64 Tahun",
            "65-69 Tahun",
            "70-74 Tahun",
            "75-79 Tahun",
            "80-84 Tahun",
            "85-89 Tahun",
            "90-94 Tahun",
            "95-99 Tahun",
            "100> Tahun",
            ],
        datasets: [
            {
                label: "Grafik Bar",
                fillColor: "#0584f2",
                strokeColor: "#0584f2",
                highlightFill: "#0444bf",
                highlightStroke: "#0444bf",
                data: [
                <?php echo $jumlah[0]->r1 ;?>,
                <?php echo $jumlah[0]->r2 ;?>,
                <?php echo $jumlah[0]->r3 ;?>,
                <?php echo $jumlah[0]->r4 ;?>,
                <?php echo $jumlah[0]->r5 ;?>,
                <?php echo $jumlah[0]->r6 ;?>,
                <?php echo $jumlah[0]->r7 ;?>,
                <?php echo $jumlah[0]->r8 ;?>,
                <?php echo $jumlah[0]->r9 ;?>,
                ]
            }
        ]
    };
    
    var Bar_chart = new Chart(ctx2).Bar(data2, {
        scaleBeginAtZero : true,
        scaleShowGridLines : true,
        scaleGridLineColor : "rgba(0,0,0,.005)",
        scaleGridLineWidth : 0,
        scaleShowHorizontalLines: true,
        scaleShowVerticalLines: true,
        barShowStroke : true,
        barStrokeWidth : 0,
        tooltipCornerRadius: 2,
        barDatasetSpacing : 3,
        legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
        responsive: true
    });
    
    var ctx3 = document.getElementById("Pie_chart").getContext("2d");
    var data3 = [
        {
            value: <?php echo $jumlah[0]->r1 ;?>,
            color: "#0584f2",
            highlight: "#0444bf",
            label: "60-64 Tahun"
        },
        {
            value: <?php echo $jumlah[0]->r2 ;?>,
            color: "#0aaff1",
            highlight: "#0444bf",
            label: "65-69 Tahun"
        },
        {
            value: <?php echo $jumlah[0]->r3 ;?>,
            color: "#0584f2",
            highlight: "#0444bf",
            label: "70-74 Tahun"
        },
        {
            value: <?php echo $jumlah[0]->r4 ;?>,
            color: "#0aaff1",
            highlight: "#0444bf",
            label: "75-79 Tahun"
        },
        {
            value: <?php echo $jumlah[0]->r5 ;?>,
            color: "#0584f2",
            highlight: "#0444bf",
            label: "80-84 Tahun"
        },
        {
            value: <?php echo $jumlah[0]->r6 ;?>,
            color: "#0aaff1",
            highlight: "#0444bf",
            label: "85-89 Tahun"
        },
        {
            value: <?php echo $jumlah[0]->r7 ;?>,
            color: "#0584f2",
            highlight: "#0444bf",
            label: "90-94 Tahun"
        },
        {
            value: <?php echo $jumlah[0]->r8 ;?>,
            color: "#0aaff1",
            highlight: "#0444bf",
            label: "95-99 Tahun"
        },
        {
            value: <?php echo $jumlah[0]->r9 ;?>,
            color: "#0584f2",
            highlight: "#0444bf",
            label: "100> Tahun"
        },
    ];
    
    var myPieChart = new Chart(ctx3).Pie(data3,{
        segmentShowStroke : true,
        segmentStrokeColor : "#fff",
        segmentStrokeWidth : 0,
        animationSteps : 100,
        tooltipCornerRadius: 0,
        animationEasing : "easeOutBounce",
        animateRotate : true,
        animateScale : false,
        legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>",
        responsive: true
    });
 
    
});
</script>
<?php } ?>
<?php } ?>
