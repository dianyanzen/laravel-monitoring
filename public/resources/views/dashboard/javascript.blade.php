    <script src="{{ url('/') }}/assets/plugins/bower_components/owl.carousel/owl.carousel.min.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/owl.carousel/owl.custom.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/flot/jquery.flot.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/Minimal-Gauge-chart/js/cmGauge.js"></script>
    <script type="text/javascript">
    $(document).ready(function () {
    
        setTimeout(function () { get_data_time(); }, 500);
    });
   function dsb_rekam(){
        if (<?php echo Session::get('S_USER_LEVEL') ;?> == 5 || <?php echo Session::get('S_USER_LEVEL') ;?> == 1){
                $.blockUI({ css: { 
                    border: 'none', 
                    padding: '15px', 
                    backgroundColor: '#000', 
                    '-webkit-border-radius': '10px', 
                    '-moz-border-radius': '10px', 
                    opacity: .5, 
                    color: '#fff' 
                },
                baseZ: 2000
            }); 
            window.location.href = "{{ url('/') }}/Detail/gdert";
        }
    }
    function dsb_cetak(){
         if (<?php echo Session::get('S_USER_LEVEL') ;?> == 5 || <?php echo Session::get('S_USER_LEVEL') ;?> == 1){
             $.blockUI({ css: { 
                    border: 'none', 
                    padding: '15px', 
                    backgroundColor: '#000', 
                    '-webkit-border-radius': '10px', 
                    '-moz-border-radius': '10px', 
                    opacity: .5, 
                    color: '#fff' 
                },
                baseZ: 2000
            }); 
            window.location.href = "{{ url('/') }}/Dashboard/Userktp";
        }
    }
    function dsb_blangko_sisa(){
         if (<?php echo Session::get('S_USER_LEVEL') ;?> == 5 || <?php echo Session::get('S_USER_LEVEL') ;?> == 1){
             $.blockUI({ css: { 
                    border: 'none', 
                    padding: '15px', 
                    backgroundColor: '#000', 
                    '-webkit-border-radius': '10px', 
                    '-moz-border-radius': '10px', 
                    opacity: .5, 
                    color: '#fff' 
                },
                baseZ: 2000
            }); 
            window.location.href = "{{ url('/') }}/Blangko/blangko_out";
        }
    }
    function dsb_blanko_keluar(){
         if (<?php echo Session::get('S_USER_LEVEL') ;?> == 5 || <?php echo Session::get('S_USER_LEVEL') ;?> == 1){
             $.blockUI({ css: { 
                    border: 'none', 
                    padding: '15px', 
                    backgroundColor: '#000', 
                    '-webkit-border-radius': '10px', 
                    '-moz-border-radius': '10px', 
                    opacity: .5, 
                    color: '#fff' 
                },
                baseZ: 2000
            }); 
            window.location.href = "{{ url('/') }}/Blangko/blangko_gudang";
        }
    }
    function dsb_prr(){
         if (<?php echo Session::get('S_USER_LEVEL') ;?> == 5 || <?php echo Session::get('S_USER_LEVEL') ;?> == 1){
             $.blockUI({ css: { 
                    border: 'none', 
                    padding: '15px', 
                    backgroundColor: '#000', 
                    '-webkit-border-radius': '10px', 
                    '-moz-border-radius': '10px', 
                    opacity: .5, 
                    color: '#fff' 
                },
                baseZ: 2000
            }); 
            window.location.href = "{{ url('/') }}/Check/Prr";
        }
    }
    function dsb_sfe(){
         if (<?php echo Session::get('S_USER_LEVEL') ;?> == 5 || <?php echo Session::get('S_USER_LEVEL') ;?> == 1){
             $.blockUI({ css: { 
                    border: 'none', 
                    padding: '15px', 
                    backgroundColor: '#000', 
                    '-webkit-border-radius': '10px', 
                    '-moz-border-radius': '10px', 
                    opacity: .5, 
                    color: '#fff' 
                },
                baseZ: 2000
            }); 
            window.location.href = "{{ url('/') }}/Check/Sfe";
        }
    }
    function dsb_duplicate(){
         if (<?php echo Session::get('S_USER_LEVEL') ;?> == 5 || <?php echo Session::get('S_USER_LEVEL') ;?> == 1){
             $.blockUI({ css: { 
                    border: 'none', 
                    padding: '15px', 
                    backgroundColor: '#000', 
                    '-webkit-border-radius': '10px', 
                    '-moz-border-radius': '10px', 
                    opacity: .5, 
                    color: '#fff' 
                },
                baseZ: 2000
            }); 
            window.location.href = "{{ url('/') }}/Check/ListDuplicate";
        }
    }
    function dsb_sisa_suket(){
         if (<?php echo Session::get('S_USER_LEVEL') ;?> == 5 || <?php echo Session::get('S_USER_LEVEL') ;?> == 1){
             $.blockUI({ css: { 
                    border: 'none', 
                    padding: '15px', 
                    backgroundColor: '#000', 
                    '-webkit-border-radius': '10px', 
                    '-moz-border-radius': '10px', 
                    opacity: .5, 
                    color: '#fff' 
                },
                baseZ: 2000
            }); 
            window.location.href = "{{ url('/') }}/Check/Suket";
        }
    }
    function dsb_cetak_kk(){
         if (<?php echo Session::get('S_USER_LEVEL') ;?> == 5 || <?php echo Session::get('S_USER_LEVEL') ;?> == 1){
             $.blockUI({ css: { 
                    border: 'none', 
                    padding: '15px', 
                    backgroundColor: '#000', 
                    '-webkit-border-radius': '10px', 
                    '-moz-border-radius': '10px', 
                    opacity: .5, 
                    color: '#fff' 
                },
                baseZ: 2000
            }); 
            window.location.href = "{{ url('/') }}/Check/Cetak_kk";
        }
    }
    function dsb_cetak_kia(){
         if (<?php echo Session::get('S_USER_LEVEL') ;?> == 5 || <?php echo Session::get('S_USER_LEVEL') ;?> == 1){
             $.blockUI({ css: { 
                    border: 'none', 
                    padding: '15px', 
                    backgroundColor: '#000', 
                    '-webkit-border-radius': '10px', 
                    '-moz-border-radius': '10px', 
                    opacity: .5, 
                    color: '#fff' 
                },
                baseZ: 2000
            }); 
            window.location.href = "{{ url('/') }}/Check/Kia";
        }
    }
    function dsb_nik_baru(){
         if (<?php echo Session::get('S_USER_LEVEL') ;?> == 5 || <?php echo Session::get('S_USER_LEVEL') ;?> == 1){
             $.blockUI({ css: { 
                    border: 'none', 
                    padding: '15px', 
                    backgroundColor: '#000', 
                    '-webkit-border-radius': '10px', 
                    '-moz-border-radius': '10px', 
                    opacity: .5, 
                    color: '#fff' 
                },
                baseZ: 2000
            }); 
            window.location.href = "{{ url('/') }}/Check/Nik_baru";
        }
    }
    function dsb_mobilitas(){
         if (<?php echo Session::get('S_USER_LEVEL') ;?> == 5 || <?php echo Session::get('S_USER_LEVEL') ;?> == 1){
             $.blockUI({ css: { 
                    border: 'none', 
                    padding: '15px', 
                    backgroundColor: '#000', 
                    '-webkit-border-radius': '10px', 
                    '-moz-border-radius': '10px', 
                    opacity: .5, 
                    color: '#fff' 
                },
                baseZ: 2000
            }); 
            window.location.href = "{{ url('/') }}/Dashboard/Mobilitas";
        }
    }
    function dsb_lahir_lu(){
         if (<?php echo Session::get('S_USER_LEVEL') ;?> == 5 || <?php echo Session::get('S_USER_LEVEL') ;?> == 1){
             $.blockUI({ css: { 
                    border: 'none', 
                    padding: '15px', 
                    backgroundColor: '#000', 
                    '-webkit-border-radius': '10px', 
                    '-moz-border-radius': '10px', 
                    opacity: .5, 
                    color: '#fff' 
                },
                baseZ: 2000
            }); 
            window.location.href = "{{ url('/') }}/Detail/gdclut";
        }
    }
    function dsb_lahir_lt(){
         if (<?php echo Session::get('S_USER_LEVEL') ;?> == 5 || <?php echo Session::get('S_USER_LEVEL') ;?> == 1){
             $.blockUI({ css: { 
                    border: 'none', 
                    padding: '15px', 
                    backgroundColor: '#000', 
                    '-webkit-border-radius': '10px', 
                    '-moz-border-radius': '10px', 
                    opacity: .5, 
                    color: '#fff' 
                },
                baseZ: 2000
            }); 
            window.location.href = "{{ url('/') }}/Detail/gdcltt";
        }
    }
    function dsb_mati(){
         if (<?php echo Session::get('S_USER_LEVEL') ;?> == 5 || <?php echo Session::get('S_USER_LEVEL') ;?> == 1){
             $.blockUI({ css: { 
                    border: 'none', 
                    padding: '15px', 
                    backgroundColor: '#000', 
                    '-webkit-border-radius': '10px', 
                    '-moz-border-radius': '10px', 
                    opacity: .5, 
                    color: '#fff' 
                },
                baseZ: 2000
            }); 
            window.location.href = "{{ url('/') }}/Detail/gdcmt";
        }
    }
    function dsb_kawin(){
         if (<?php echo Session::get('S_USER_LEVEL') ;?> == 5 || <?php echo Session::get('S_USER_LEVEL') ;?> == 1){
             $.blockUI({ css: { 
                    border: 'none', 
                    padding: '15px', 
                    backgroundColor: '#000', 
                    '-webkit-border-radius': '10px', 
                    '-moz-border-radius': '10px', 
                    opacity: .5, 
                    color: '#fff' 
                },
                baseZ: 2000
            }); 
            window.location.href = "{{ url('/') }}/Detail/gdckt";
        }
    }
    function dsb_cerai(){
         if (<?php echo Session::get('S_USER_LEVEL') ;?> == 5 || <?php echo Session::get('S_USER_LEVEL') ;?> == 1){
             $.blockUI({ css: { 
                    border: 'none', 
                    padding: '15px', 
                    backgroundColor: '#000', 
                    '-webkit-border-radius': '10px', 
                    '-moz-border-radius': '10px', 
                    opacity: .5, 
                    color: '#fff' 
                },
                baseZ: 2000
            }); 
            window.location.href = "{{ url('/') }}/Detail/gdcct";
        }
    }
    // Api Dashboard
    function get_data_time() {
       $.ajax({
                type: "post",
                url: BASE_URL+"dashboard/get_dashboard_time",
                data: {"_token": "{{ csrf_token() }}",},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                            $('#date-part').html(data.dashboard_date);
                            $('#time-part').html(data.dashboard_time);
                            if(data.dashboard_between == 0){
                                $('#date-diff').html(data.dashboard_minutes+ " Minutes Ago");
                                $('#is_services').show();

                            }else{
                                $('#is_services').hide();
                            }
                },
                error:
                function (data) {


                },
                complete:
                function (response) {
                   
                   setTimeout(function () { get_data_m1(); }, 500);
                }
            });
        }
    function get_data_m1() {
       $.ajax({
                type: "post",
                url: BASE_URL+"dashboard/get_dashboard_m1",
                data: {"_token": "{{ csrf_token() }}",},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                            $('#perekaman_today').html(data.perekaman_today);
                },
                error:
                function (data) {


                },
                complete:
                function (response) {
                   
                   setTimeout(function () { get_data_m2(); }, 500);
                }
            });
        }
    function get_data_m2() {
       $.ajax({
                type: "post",
                url: BASE_URL+"dashboard/get_dashboard_m2",
                data: {"_token": "{{ csrf_token() }}",},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                            $('#pencetakan_today').html(data.pencetakan_today);
                },
                error:
                function (data) {


                },
                complete:
                function (response) {
                   
                   setTimeout(function () { get_data_m3(); }, 500);
                }
            });
        }
    function get_data_m3() {
       $.ajax({
                type: "post",
                url: BASE_URL+"dashboard/get_dashboard_m3",
                data: {"_token": "{{ csrf_token() }}",},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                            $('#sisa_prr').html(data.sisa_prr);
                },
                error:
                function (data) {


                },
                complete:
                function (response) {
                   
                   setTimeout(function () { get_data_m4(); }, 500);
                }
            });
        }
    function get_data_m4() {
       $.ajax({
                type: "post",
                url: BASE_URL+"dashboard/get_dashboard_m4",
                data: {"_token": "{{ csrf_token() }}",},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                            $('#sisa_sfe').html(data.sisa_sfe);
                },
                error:
                function (data) {


                },
                complete:
                function (response) {
                   
                   setTimeout(function () { get_data_m5(); }, 500);
                }
            });
        }
    function get_data_m5() {
       $.ajax({
                type: "post",
                url: BASE_URL+"dashboard/get_dashboard_m5",
                data: {"_token": "{{ csrf_token() }}",},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                           $('#sisa_suket').html(data.sisa_suket);
                },
                error:
                function (data) {


                },
                complete:
                function (response) {
                   
                   setTimeout(function () { get_data_m6(); }, 500);
                }
            });
        }
    function get_data_m6() {
       $.ajax({
                type: "post",
                url: BASE_URL+"dashboard/get_dashboard_m6",
                data: {"_token": "{{ csrf_token() }}",},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                            $('#blangko_out').html(data.blangko_out);
                },
                error:
                function (data) {


                },
                complete:
                function (response) {
                   
                   setTimeout(function () { get_data_m7(); }, 500);
                }
            });
        }
    function get_data_m7() {
       $.ajax({
                type: "post",
                url: BASE_URL+"dashboard/get_dashboard_m7",
                data: {"_token": "{{ csrf_token() }}",},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                            $('#sisa_duplicate').html(data.duplicate);
                },
                error:
                function (data) {


                },
                complete:
                function (response) {
                   
                   setTimeout(function () { get_data_m8(); }, 500);
                }
            });
        }
    function get_data_m8() {
       $.ajax({
                type: "post",
                url: BASE_URL+"dashboard/get_dashboard_m8",
                data: {"_token": "{{ csrf_token() }}",},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                            $('#sisa_blangko').html(data.sisa_blangko);
                },
                error:
                function (data) {


                },
                complete:
                function (response) {
                   
                   setTimeout(function () { get_data_kk(); }, 500);
                }
            });
        }

      function get_data_kk() {
       $.ajax({
                type: "post",
                url: BASE_URL+"dashboard/get_dashboard_kk",
                data: {"_token": "{{ csrf_token() }}",},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                            $('#pen_kk').html(data.pen_kk);
                },
                error:
                function (data) {


                },
                complete:
                function (response) {
                   
                   setTimeout(function () { get_data_kia(); }, 500);
                }
            });
        }
        function get_data_kia() {
       $.ajax({
                type: "post",
                url: BASE_URL+"dashboard/get_dashboard_kia",
                data: {"_token": "{{ csrf_token() }}",},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                            $('#pen_kia').html(data.pen_kia);
                },
                error:
                function (data) {


                },
                complete:
                function (response) {
                   
                   setTimeout(function () { get_data_nik(); }, 500);
                }
            });
        }
        function get_data_nik() {
       $.ajax({
                type: "post",
                url: BASE_URL+"dashboard/get_dashboard_nik",
                data: {"_token": "{{ csrf_token() }}",},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                            $('#pen_nik_baru').html(data.pen_nik_baru);
                },
                error:
                function (data) {


                },
                complete:
                function (response) {
                   
                   setTimeout(function () { get_data_akta_lu(); }, 500);
                }
            });
        }
        function get_data_akta_lu() {
       $.ajax({
                type: "post",
                url: BASE_URL+"dashboard/get_dashboard_akta_lu",
                data: {"_token": "{{ csrf_token() }}",},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                            $('#pen_lahir_lu').html(data.pen_lahir_lu);
                },
                error:
                function (data) {


                },
                complete:
                function (response) {
                   
                   setTimeout(function () { get_data_akta_lt(); }, 500);
                }
            });
        }
         function get_data_akta_lt() {
       $.ajax({
                type: "post",
                url: BASE_URL+"dashboard/get_dashboard_akta_lt",
                data: {"_token": "{{ csrf_token() }}",},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                            $('#pen_lahir_lt').html(data.pen_lahir_lt);
                },
                error:
                function (data) {


                },
                complete:
                function (response) {
                   
                   setTimeout(function () { get_data_akta_mt(); }, 500);
                }
            });
        }
        function get_data_akta_mt() {
       $.ajax({
                type: "post",
                url: BASE_URL+"dashboard/get_dashboard_akta_mt",
                data: {"_token": "{{ csrf_token() }}",},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                            $('#pen_mati').html(data.pen_mati);
                },
                error:
                function (data) {


                },
                complete:
                function (response) {
                   
                   setTimeout(function () { get_data_akta_kwn(); }, 500);
                }
            });
        }
        function get_data_akta_kwn() {
       $.ajax({
                type: "post",
                url: BASE_URL+"dashboard/get_dashboard_akta_kwn",
                data: {"_token": "{{ csrf_token() }}",},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                            $('#pen_kawin').html(data.pen_kawin);
                },
                error:
                function (data) {


                },
                complete:
                function (response) {
                   
                   setTimeout(function () { get_data_akta_cry(); }, 500);
                }
            });
        }
        function get_data_akta_cry() {
       $.ajax({
                type: "post",
                url: BASE_URL+"dashboard/get_dashboard_akta_cry",
                data: {"_token": "{{ csrf_token() }}",},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                            $('#pen_cerai').html(data.pen_cerai);
                },
                error:
                function (data) {


                },
                complete:
                function (response) {
                   
                   setTimeout(function () { get_data_pdh_akab(); }, 500);
                }
            });
        }
        function get_data_pdh_akab() {
       $.ajax({
                type: "post",
                url: BASE_URL+"dashboard/get_dashboard_pdh_akab",
                data: {"_token": "{{ csrf_token() }}",},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                            $('#pen_pindah_akab').html(data.pen_pindah_akab);
                },
                error:
                function (data) {


                },
                complete:
                function (response) {
                   
                   setTimeout(function () { get_data_pdh_akec(); }, 500);
                }
            });
        }
        function get_data_pdh_akec() {
       $.ajax({
                type: "post",
                url: BASE_URL+"dashboard/get_dashboard_pdh_akec",
                data: {"_token": "{{ csrf_token() }}",},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                            $('#pen_pindah_akec').html(data.pen_pindah_akec);
                },
                error:
                function (data) {


                },
                complete:
                function (response) {
                   
                   setTimeout(function () { get_data_pdh_dkec(); }, 500);
                }
            });
        }

        function get_data_pdh_dkec() {
       $.ajax({
                type: "post",
                url: BASE_URL+"dashboard/get_dashboard_pdh_dkec",
                data: {"_token": "{{ csrf_token() }}",},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                            $('#pen_pindah_dkec').html(data.pen_pindah_dkec);
                },
                error:
                function (data) {


                },
                complete:
                function (response) {
                   
                   setTimeout(function () { get_data_dtg_akab(); }, 500);
                }
            });
        }

        function get_data_dtg_akab() {
       $.ajax({
                type: "post",
                url: BASE_URL+"dashboard/get_dashboard_dtg_akab",
                data: {"_token": "{{ csrf_token() }}",},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                            $('#pen_datang_akab').html(data.pen_datang_akab);
                },
                error:
                function (data) {


                },
                complete:
                function (response) {
                   
                   setTimeout(function () { get_data_dtg_akec(); }, 500);
                }
            });
        }
        function get_data_dtg_akec() {
       $.ajax({
                type: "post",
                url: BASE_URL+"dashboard/get_dashboard_dtg_akec",
                data: {"_token": "{{ csrf_token() }}",},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                            $('#pen_datang_akec').html(data.pen_datang_akec);
                },
                error:
                function (data) {


                },
                complete:
                function (response) {
                   
                   setTimeout(function () { get_data_dtg_dkec(); }, 500);
                }
            });
        }
        function get_data_dtg_dkec() {
       $.ajax({
                type: "post",
                url: BASE_URL+"dashboard/get_dashboard_dtg_dkec",
                data: {"_token": "{{ csrf_token() }}",},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                            $('#pen_datang_dkec').html(data.pen_datang_dkec);
                },
                error:
                function (data) {


                },
                complete:
                function (response) {
                   
                   setTimeout(function () { get_data_time(); }, 50000);
                }
            });
        }

    </script>   
    