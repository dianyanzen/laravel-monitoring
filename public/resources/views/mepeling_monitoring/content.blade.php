<div id="page-wrapper">
            <div class="container-fluid">
               <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">{{ $mtitle }}</h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                
                        <ol class="breadcrumb">
                            <li><a href="{{ url('/') }}/">Report</a></li>
                            <li class="active">{{ $mtitle }}</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- .row -->
                <div class="row">
                <form >
                    <div class="col-sm-12">
                        <div class="white-box">
                            <div class="row">
                                <div class="col-lg-4">         
                                    <div class="form-group">
                                        <h3 class="box-title m-b-0"><b>Tanggal {{ $type_tgl}}</b></h3>
                                        <input class="form-control input-daterange-datepicker" type="text" id="tanggal" name="tanggal"/> </div>
                                    
                                </div>
                                <div class="col-lg-4">         
                                    <div class="form-group">
                                        <h3 class="box-title m-b-0"><b>Seksi</b></h3>
                                        <select class="form-control select2" name="jns_seksi" id="jns_seksi">
                                <option  value="1">KELAHIRAN</option>
                                <option  value="2">KEMATIAN</option>
                            </select>
                            
                                    </div>
                                </div>
                                <div class="col-lg-4">         
                                    <div class="form-group">
                                        <h3 class="box-title m-b-0"><b>User</b></h3>
                                        <select class="form-control select2" name="user_id" id="user_id">
                                <option  value="0">-- PILIH USER --</option>
                            </select>
                            
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <button type="button" class="btn btn-info waves-effect waves-light m-r-10 pull-right" id="btn-filter" onclick="on_serach();" >Search <i class="mdi  mdi-magnify fa-fw"></i></button>
                                  <button type="button" class="btn btn-invert waves-effect waves-light m-r-10 pull-right" id="btn-reset" onclick="on_clear();">Clear <i class="mdi  mdi-delete fa-fw"></i></button>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>

                <div class="row">
                <div class="col-md-12">
                        <div class="white-box">
                            <h3 class="box-title">{{ $mtitle }}</h3>
                            <div class="scrollable">
                                <div class="table-responsive">
                                    <table id="daily-list" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                   <thead>
                                    
                                        <tr>
                                            <th width="5%" style="text-align: center;">No</th>
                                            <th width="15%" style="text-align: center;">Petugas</th>
                                            <th width="15%" style="text-align: center;">Tanggal</th>
                                            <th width="15%" style="text-align: center;">Kecamatan</th>
                                            <th width="15%" style="text-align: center;">Jumlah</th>

                                        </tr>
                                       
                                    </thead>
                                    <tbody id="show_data">
                                       
                                        
                                    </tbody>
                                    
                                </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
       @include('shared.footer_detail')