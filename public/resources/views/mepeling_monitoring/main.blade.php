<!DOCTYPE html>
<html lang="en">
@include('shared.head')
<body class="fix-header">
    <div id="wrapper">
            @include('shared.nav_top')
            @include('shared/nav_side')
            @include('Mepeling_monitoring/content')
    </div>
    @include('shared/footer')
    @include('Mepeling_monitoring/javascript')
</body>
</html>