 <script>
    
  
    function on_clear() {
         $('#tanggal').val("{{ date('d-m-Y') }} - {{ date('d-m-Y') }}");
        $('select[name="jns_seksi"]').val("1").trigger("change");
        $('select[name="user_id"]').val("0").trigger("change");
    }
    jQuery(document).ready(function() {
            $(".select2").select2();
            $('.input-daterange-datepicker').daterangepicker({
                buttonClasses: ['btn', 'btn-sm'],
                applyClass: 'btn-info',
                cancelClass: 'btn-info',
                locale: {
                    format: 'DD-MM-YYYY'
                },
                startDate: '{{ date('d-m-Y')}}',
                endDate: '{{ date('d-m-Y')}}'
            });
           get_user_mepeling(1);
           
           $('.input-daterange-datepicker').on('change', function() {
                var tgl = $(this).val();
                get_table_tgl(tgl);
            });
           
           $('select[name="jns_seksi"]').on('change', function() {
                var jns_seksi = $(this).val();
                console.log(jns_seksi);
                get_user_mepeling(jns_seksi);
                $('select[name="user_id"]').val("0").trigger("change");
            });
           
           $('select[name="user_id"]').on('change', function() {
                on_serach();
            });
        });
        $('#daily-list').DataTable({
         "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });

  
    function get_table_tgl(tgl) {
        $('#daily-list').DataTable({
                destroy: true,
                 "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                 "columnDefs": [
                    {"className": "dt-left", "targets": "_all"}
                  ],
                "pageLength" : 50,
                "ajax": {
                 "url": BASE_URL+"Capil/get_rekap_mepeling_tgl",
                "type": "post",
                "data": {
                "_token": "{{ csrf_token() }}",
                "tanggal":  tgl,
                "user_id":   $('select[name="user_id"]').val(),
                "jns_seksi":   $('select[name="jns_seksi"]').val(),
                }
                }
            });
    }

    function get_user_mepeling(seksi){
        console.log(seksi);
        $('select[name="user_id"]').empty();
        if(seksi != 1){
            $.ajax({
                    type: "post",
                    url: BASE_URL+"Shared_api/get_user_kmt",
                    dataType: "json",
                    data: {
                      "_token": "{{ csrf_token() }}"
                    },
                    beforeSend:
                    function () {
                    },
                    success: function (data) {
                        console.log(data);
                       $('select[name="user_id"]').empty();
                       $('select[name="user_id"]').append('<option value="0">-- PILIH USER --</option>');
                        $.each(data, function(key, value) {
                            $('select[name="user_id"]').append('<option value="'+ value.user_id +'">'+ value.nama_lgkp +'</option>');
                        });
                        $('select[name="user_id"]').val("0").trigger("change");
                      
                        
                    },
                    error:
                    function (data) {
               

                    },
                    complete:
                    function (response) {
                    }
                });
        }else{
            $.ajax({
                    type: "post",
                    url: BASE_URL+"Shared_api/get_user_lhr",
                    dataType: "json",
                    data: {
                      "_token": "{{ csrf_token() }}"
                    },
                    beforeSend:
                    function () {
                    },
                    success: function (data) {
                        console.log(data);
                       $('select[name="user_id"]').empty();
                       $('select[name="user_id"]').append('<option value="0">-- PILIH USER --</option>');
                        $.each(data, function(key, value) {
                            $('select[name="user_id"]').append('<option value="'+ value.user_id +'">'+ value.nama_lgkp +'</option>');
                        });
                        $('select[name="user_id"]').val("0").trigger("change");
                        
                    },
                    error:
                    function (data) {
               

                    },
                    complete:
                    function (response) {
                    }
                });
        }
        
    }
    function on_serach() {
        $('#daily-list').DataTable({
                destroy: true,
                 "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                 "columnDefs": [
                    {"className": "dt-left", "targets": "_all"}
                  ],
                "pageLength" : 50,
                "ajax": {
                 "url": BASE_URL+"Capil/get_rekap_mepeling_tgl",
                "type": "post",
                "data": {
                "_token": "{{ csrf_token() }}",
                "tanggal":  $("#tanggal").val(),
                "user_id":   $('select[name="user_id"]').val(),
                "jns_seksi":   $('select[name="jns_seksi"]').val(),
                }
                }
            });
    }
    function block_screen(){
        
        $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff'
            },
            baseZ: 2000
        }); 
    }
    function unblock_screen(){
       $.unblockUI();
    }
    </script>