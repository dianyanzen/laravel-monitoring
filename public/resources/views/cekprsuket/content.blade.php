 <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $stitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                        <ol class="breadcrumb">
                            <li class="active"><?php echo $mtitle; ?></li>
                        </ol>
                    </div>
                </div>
                 <div class="row">
                <form name ="get_form" action="<?php echo $my_url; ?>" method="post">
                    @csrf
                    <div class="col-sm-12">
                        <div class="white-box">
                            <div class="row">
                                <div class="col-lg-3">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_kec" type="checkbox" checked disabled="true">
                                            <label for="cb_kec"> Kecamatan</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3">         
                                    <div class="form-group" <?php if (!empty($is_colnotwil)){echo 'style="display: none!important;"'; }?>>
                                        <select class="form-control select2" name="no_kec" id="no_kec">
                                         <option  value="0">-- Pilih Kecamatan --</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-3">         
                                    <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_kel" type="checkbox" checked disabled="true">
                                            <label for="cb_kel"> Kelurahan</label>
                                        </div>
                                </div>
                                <div class="col-lg-3">         
                                    <div class="form-group" <?php if (!empty($is_colnotwil)){echo 'style="display: none!important;"'; }?>>
                                        <select class="form-control select2" name="no_kel" id="no_kel">
                                        <option value="0">-- Pilih Kelurahan --</option>  
                                    </select>
                           
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <button type="submit" class="btn btn-info waves-effect waves-light m-r-10 pull-right" id="btn-filter" onclick="on_serach();" >Search <i class="mdi  mdi-magnify fa-fw"></i></button>
                                  <button type="button" class="btn btn-invert waves-effect waves-light m-r-10 pull-right" id="btn-reset" onclick="on_clear();">Clear <i class="mdi  mdi-delete fa-fw"></i></button>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
                <div class="row">
                <div class="col-md-12">
                        <div class="white-box">
                            <h3 class="box-title"><?php echo $mtitle; ?></h3>
                            <div class="scrollable">
                                <div class="table-responsive">
                                    <table id="mytable" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                   <thead>
                                    
                                        <tr>
                                            <th width="10%" style="text-align: center;">No Kec</th>
                                            <th width="20%" style="text-align: center;">Nik</th>
                                            <th width="20%" style="text-align: center;">Nama Lengkap</th>
                                            <th width="15%" style="text-align: center;">Status E-KTP</th>
                                            <th width="15%" style="text-align: center;">Agama</th>
                                            <th width="15%" style="text-align: center;">Tahun Suket</th>
                                            <th width="15%" style="text-align: center;">Tanggal Suket</th>
                                            <th width="15%" style="text-align: center;">Tanggal KTP</th>
                                            <th width="10%" style="text-align: center;">Nama Kecamatan</th>
                                            <th width="10%" style="text-align: center;">Nama Kelurahan</th>
                                            <th width="15%" style="text-align: center;">RW</th>
                                            <th width="15%" style="text-align: center;">RT</th>
                                            <th width="15%" style="text-align: center;">Selisih</th>
                                            <th width="15%" style="text-align: center;">Action</th>
                                        </tr>
                                       
                                    </thead>
                                    <tbody id="show_data">
                                       <?php
                                        if (!empty($data)){
                                            $i=0;
                                           foreach($data as $row){
                                            $i++;
                                            ?>
                                             <tr>
                                                <td width="10%" style="text-align: center;"><?php echo $row->no_kec ;?></td>
                                                <td width="20%" style="text-align: center;"><span style="color: #fff">,</span><?php echo $row->nik ;?></td>
                                                <td width="20%" style="text-align: center;"><?php echo $row->nama_lgkp ;?></td>
                                                <td width="15%" style="text-align: center;"><?php echo $row->current_status_code ;?></td>
                                                <td width="15%" style="text-align: center;"><?php echo $row->agama ;?></td>
                                                <td width="15%" style="text-align: center;">TAHUN <?php echo $row->thn_suket ;?></td>
                                                <td width="15%" style="text-align: center;"><?php echo $row->tgl_suket ;?></td>
                                                <td width="15%" style="text-align: center;"><?php echo $row->tgl_ktp ;?></td>
                                                <td width="10%" style="text-align: center;"><?php echo $row->nama_kec ;?></td>
                                                <td width="10%" style="text-align: center;"><?php echo $row->nama_kel ;?></td>
                                                <td width="10%" style="text-align: center;"><?php echo $row->rw ;?></td>
                                                <td width="10%" style="text-align: center;"><?php echo $row->rt ;?></td>
                                                <td width="10%" style="text-align: center;">
                                                    <?php 
                                                    if ($row->tgl_ktp != '-'){
                                                        $date1=date_create($row->tgl_suket);
                                                        $date2=date_create($row->tgl_ktp);
                                                        $diff=date_diff($date1,$date2);
                                                        echo $diff->format("%R%a days");
                                                    }else{
                                                        echo "-";
                                                    }
                                                    ?>
                                                </td>
                                                <td width="10%" style="text-align: center;">
                                                    <?php 
                                                    if ($row->tgl_ktp != '-'){
                                                        $date1=date_create($row->tgl_suket);
                                                        $date2=date_create($row->tgl_ktp);
                                                        $diff=date_diff($date1,$date2);
                                                        $selisih = $diff->format("%R%a");
                                                        if($selisih > -30){
                                                            ?>
                                                            <button type="button" class="btn btn-danger btn-xs" id="btn-hapus" onclick="hapus_suket('{{$row->no_dokumen}}','{{$row->nik}}','{{$row->nama_lgkp}}');" style="margin-top: 5px;">Hapus Suket <i class="mdi  mdi-sync fa-fw"></i></button>
                                                            <?php
                                                        }else{
                                                            echo "-";    
                                                        }
                                                    }else{
                                                        echo "-";
                                                    }
                                                    ?>
                                                </td>
                                            </tr>
                                          <?php } ?>
                                          <?php } ?>
                                       
                                        
                                    </tbody>
                                    
                                        <tfoot id="my_foot">
                                            <tr>
                                            <?php if (!empty($data)){ ?>
                                            <th width="80%" colspan="13" style="text-align: center;">Jumlah</th>
                                            <th width="20%"  style="text-align: left;"><?php echo number_format(htmlentities($i),0,',','.');?></th>
                                            <?php } ?>
                                        </tr>

                                            </tfoot>
                                </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                 </div>
       @include('shared.footer_detail')