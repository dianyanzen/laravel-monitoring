<!DOCTYPE html>
<html lang="en">
@include('shared.head')
<body class="fix-header">
    <div id="wrapper">
            @include('shared.nav_top')
            @include('shared/nav_side')
            @include('perbaikan_list_data/content')
            @include('perbaikan_list_data/modal')
    </div>
     
    @include('shared/footer')
    
    @include('perbaikan_list_data/javascript')
</body>
</html>