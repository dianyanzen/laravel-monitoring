    <script src="{{ url('/') }}/assets/plugins/bower_components/owl.carousel/owl.carousel.min.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/owl.carousel/owl.custom.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/flot/jquery.flot.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/Minimal-Gauge-chart/js/cmGauge.js"></script>
   <script type="text/javascript">
   	$(document).ready(function() {
        request_pending();
    });
   
     function request_pending(){
         $.ajax({
                    type: "post",
                    url: BASE_URL+"Siak/list_request_pending",
                    dataType: "json",
                    data: {
                      "_token": "{{ csrf_token() }}",
                        user_id : "<?php echo $user_id; ?>"
                    },
                    beforeSend:
                    function () {
                        
                    },
                    success: function (data) {
                        console.log(data);
                         if (!$.trim(data)){  
                        	$('#request_pending').empty();
                        	$('#request_pending').append('<div class="sl-item"><div class="sl-right">There Is No Data To Be Displayed On Your Page</div></div>');

                        }else{
                            $('#request_pending').empty();
                            $.each(data, function(key, value) {
                                $('#request_pending').append('<div class="sl-item"><div class="sl-left"><img src="{{ url('/') }}/assets/upload/pp/'+value.nik+'.jpg"  class="img-circle img-responsive" alt="user-img" > </div><div class="sl-right"><div class="m-l-40"> <a href="#" class="text-info">'+value.nama_lgkp+'</a> <span class="sl-date">Tanggal '+value.activity_date+'</span><div class="m-t-20 row"><div class="col-md-12 col-xs-12"><a href="#" class="btn btn-info" style="margin-bottom: 10px !important">'+value.p1+'</a><p>'+value.p2+'</p><p> '+value.p3+'</p><div class="row"><button type="button" class="btn btn-info waves-effect waves-light m-r-10 pull-right" id="btn-aprove" onclick="do_aprove(\''+value.seq_id+'\');">Aprove <i class="mdi mdi-check-circle fa-fw"></i></button><button type="button" class="btn btn-info waves-effect waves-light m-r-10 pull-right" id="btn-reject" onclick="do_reject(\''+value.seq_id+'\');">Reject <i class="mdi mdi-close-circle fa-fw"></i></button></div><br></div></div></div></div></div>');
                            });
                        }
                        
                    },
                    error:
                    function (data) {
               

                    },
                    complete:
                    function (response) {
                        request_success();
                    }
                });
    }
     function request_success(){
         $.ajax({
                    type: "post",
                    url: BASE_URL+"Siak/list_request_success",
                    dataType: "json",
                    data: {
                      "_token": "{{ csrf_token() }}",
                        user_id : "<?php echo $user_id; ?>"
                    },
                    beforeSend:
                    function () {
                        
                    },
                    success: function (data) {
                        console.log(data);
                        if (!$.trim(data)){  
                        	$('#request_success').empty();
                        	$('#request_success').append('<div class="sl-item"><div class="sl-right">There Is No Data To Be Displayed On Your Page</div></div>');

                        }else{
                            $('#request_success').empty();
                            $.each(data, function(key, value) {
                                $('#request_success').append('<div class="sl-item"><div class="sl-left"><img src="{{ url('/') }}/assets/upload/pp/'+value.nik+'.jpg"  class="img-circle img-responsive" alt="user-img" > </div><div class="sl-right"><div class="m-l-40"> <a href="#" class="text-info">'+value.nama_lgkp+'</a> <span class="sl-date">Tanggal '+value.activity_date+'</span><div class="m-t-20 row"><div class="col-md-12 col-xs-12"><a href="#" class="btn btn-info" style="margin-bottom: 10px !important">'+value.p1+'</a><p>'+value.p2+'</p><hr><a href="#" class="text-info">Reply From : <b>'+value.p4+'</b></a><p>'+value.p3+'</p></div></div></div></div></div>');
                            });
                        }
                        
                    },
                    error:
                    function (data) {
               

                    },
                    complete:
                    function (response) {
                        request_reject();
                    }
                });
    }
     function request_reject(){
         $.ajax({
                    type: "post",
                    url: BASE_URL+"Siak/list_request_reject",
                    dataType: "json",
                    data: {
                      "_token": "{{ csrf_token() }}",
                        user_id : "<?php echo $user_id; ?>"
                    },
                    beforeSend:
                    function () {
                        
                    },
                    success: function (data) {
                        console.log(data);
                        if (!$.trim(data)){  
                        	$('#request_reject').empty();
                        	$('#request_reject').append('<div class="sl-item"><div class="sl-right">There Is No Data To Be Displayed On Your Page</div></div>');

                        }else{
                            $('#request_reject').empty();
                            $.each(data, function(key, value) {
                                $('#request_reject').append('<div class="sl-item"><div class="sl-left"><img src="{{ url('/') }}/assets/upload/pp/'+value.nik+'.jpg"  class="img-circle img-responsive" alt="user-img" > </div><div class="sl-right"><div class="m-l-40"> <a href="#" class="text-info">'+value.nama_lgkp+'</a> <span class="sl-date">Tanggal '+value.activity_date+'</span><div class="m-t-20 row"><div class="col-md-12 col-xs-12"><a href="#" class="btn btn-info" style="margin-bottom: 10px !important">'+value.p1+'</a><p>'+value.p2+'</p><hr><a href="#" class="text-info">Reply From : <b>'+value.p4+'</b></a><p>'+value.p3+'</p></div></div></div></div></div>');
                            });
                        }
                        
                    },
                    error:
                    function (data) {
               

                    },
                    complete:
                    function (response) {

                    }
                });
    }
   function refresh_pending(){

         $.ajax({
                    type: "post",
                    url: BASE_URL+"Siak/list_request_pending",
                    dataType: "json",
                    data: {
                      "_token": "{{ csrf_token() }}",
                        user_id : "<?php echo $user_id; ?>"
                    },
                    beforeSend:
                    function () {
                        
                    },
                    success: function (data) {
                        console.log(data);
                         if (!$.trim(data)){  
                            $('#request_pending').empty();
                            $('#request_pending').append('<div class="sl-item"><div class="sl-right">There Is No Data To Be Displayed On Your Page</div></div>');

                        }else{
                            $('#request_pending').empty();
                            $.each(data, function(key, value) {
                                $('#request_pending').append('<div class="sl-item"><div class="sl-left"><img src="{{ url('/') }}/assets/upload/pp/'+value.nik+'.jpg"  class="img-circle img-responsive" alt="user-img" > </div><div class="sl-right"><div class="m-l-40"> <a href="#" class="text-info">'+value.nama_lgkp+'</a> <span class="sl-date">Tanggal '+value.activity_date+'</span><div class="m-t-20 row"><div class="col-md-12 col-xs-12"><a href="#" class="btn btn-info" style="margin-bottom: 10px !important">'+value.p1+'</a><p>'+value.p2+'</p><p> '+value.p3+'</p><div class="row"><button type="button" class="btn btn-info waves-effect waves-light m-r-10 pull-right" id="btn-aprove" onclick="do_aprove(\''+value.seq_id+'\');">Aprove <i class="mdi mdi-check-circle fa-fw"></i></button><button type="button" class="btn btn-info waves-effect waves-light m-r-10 pull-right" id="btn-reject" onclick="do_reject(\''+value.seq_id+'\');">Reject <i class="mdi mdi-close-circle fa-fw"></i></button></div><br></div></div></div></div></div>');
                            });
                        }
                        
                    },
                    error:
                    function (data) {
               

                    },
                    complete:
                    function (response) {
                        refresh_success();
                    }
                });
    }
     function refresh_success(){
         $.ajax({
                    type: "post",
                    url: BASE_URL+"Siak/list_request_success",
                    dataType: "json",
                    data: {
                      "_token": "{{ csrf_token() }}",
                        user_id : "<?php echo $user_id; ?>"
                    },
                    beforeSend:
                    function () {
                        
                    },
                    success: function (data) {
                        console.log(data);
                        if (!$.trim(data)){  
                            $('#request_success').empty();
                            $('#request_success').append('<div class="sl-item"><div class="sl-right">There Is No Data To Be Displayed On Your Page</div></div>');

                        }else{
                            $('#request_success').empty();
                            $.each(data, function(key, value) {
                                $('#request_success').append('<div class="sl-item"><div class="sl-left"><img src="{{ url('/') }}/assets/upload/pp/'+value.nik+'.jpg"  class="img-circle img-responsive" alt="user-img" > </div><div class="sl-right"><div class="m-l-40"> <a href="#" class="text-info">'+value.nama_lgkp+'</a> <span class="sl-date">Tanggal '+value.activity_date+'</span><div class="m-t-20 row"><div class="col-md-12 col-xs-12"><a href="#" class="btn btn-info" style="margin-bottom: 10px !important">'+value.p1+'</a><p>'+value.p2+'</p><hr><a href="#" class="text-info">Reply From : <b>'+value.p4+'</b></a><p>'+value.p3+'</p></div></div></div></div></div>');
                            });
                        }
                        
                    },
                    error:
                    function (data) {
               

                    },
                    complete:
                    function (response) {
                        refresh_reject();
                    }
                });
    }
     function refresh_reject(){
         $.ajax({
                    type: "post",
                    url: BASE_URL+"Siak/list_request_reject",
                    dataType: "json",
                    data: {
                      "_token": "{{ csrf_token() }}",
                        user_id : "<?php echo $user_id; ?>"
                    },
                    beforeSend:
                    function () {
                        
                    },
                    success: function (data) {
                        console.log(data);
                        if (!$.trim(data)){  
                            $('#request_reject').empty();
                            $('#request_reject').append('<div class="sl-item"><div class="sl-right">There Is No Data To Be Displayed On Your Page</div></div>');

                        }else{
                            $('#request_reject').empty();
                            $.each(data, function(key, value) {
                                $('#request_reject').append('<div class="sl-item"><div class="sl-left"><img src="{{ url('/') }}/assets/upload/pp/'+value.nik+'.jpg"  class="img-circle img-responsive" alt="user-img" > </div><div class="sl-right"><div class="m-l-40"> <a href="#" class="text-info">'+value.nama_lgkp+'</a> <span class="sl-date">Tanggal '+value.activity_date+'</span><div class="m-t-20 row"><div class="col-md-12 col-xs-12"><a href="#" class="btn btn-info" style="margin-bottom: 10px !important">'+value.p1+'</a><p>'+value.p2+'</p><hr><a href="#" class="text-info">Reply From : <b>'+value.p4+'</b></a><p>'+value.p3+'</p></div></div></div></div></div>');
                            });
                        }
                        
                    },
                    error:
                    function (data) {
               

                    },
                    complete:
                    function (response) {
                        unblock_screen();
                          swal("Success!", "Data Already Up To Date", "success");
                    }
                });
         
    }
    function do_refresh(){
        block_screen();
        refresh_pending();
    }
    function do_aprove(data){
        get_data_aprove(data);
    }
    function do_reject(data){
        get_data_reject(data);
    }
    function get_data_aprove(data){
       $.ajax({
                    type: "post",
                    url: BASE_URL+"Siak/get_request",
                    dataType: "json",
                    data: {
                      "_token": "{{ csrf_token() }}",
                        seq_id : data
                    },
                    beforeSend:
                    function () {
                        
                    },
                    success: function (data) {
                        console.log(data);
                       $.each(data, function(key, value) {
                        $("#aprove_seq_id").val(value.seq_id);       
                        $("#aprove_nama").val(value.nama_lgkp);       
                        $("#aprove_option_req").val(value.p1);       
                        $("#keterangan_aprove").val(value.p2);       
                        });
                     
                        
                        
                    },
                    error:
                    function (data) {
               

                    },
                    complete:
                    function (response) {
                        $('#do_aprove').click();
                    }
                });
    }
    function get_data_reject(data){
       $.ajax({
                    type: "post",
                    url: BASE_URL+"Siak/get_request",
                    dataType: "json",
                    data: {
                      "_token": "{{ csrf_token() }}",
                        seq_id : data
                    },
                    beforeSend:
                    function () {
                        
                    },
                    success: function (data) {
                        console.log(data);
                         $.each(data, function(key, value) {
                        $("#reject_seq_id").val(value.seq_id);       
                        $("#reject_nama").val(value.nama_lgkp);       
                        $("#reject_option_req").val(value.p1);       
                        $("#keterangan_reject").val(value.p2);       
                        });
                        
                        
                    },
                    error:
                    function (data) {
                        

                    },
                    complete:
                    function (response) {
                        $('#do_reject').click();
                    }
                });
    }
    function onClearModal_aprove(){

    }
    function onClearModal_reject(){
        
    }
    function do_close_aprove(){
      $('#closemodalaprove').click();
    }

    function do_close_reject(){
      $('#closemodalreject').click();
    }
    function validation_aprove() {
            var aprove_seq_id = $("#aprove_seq_id");
            if (aprove_seq_id.val().length == 0) {
              aprove_seq_id.select();
                toastr.warning('Seq Id tidak boleh kosong');
                return false;
            }
            var balasan_aprove = $("#balasan_aprove");
            if (balasan_aprove.val().length == 0) {
                toastr.warning('Balasan tidak boleh kosong');
                balasan_aprove.select();
                return false;
            } 
            if (balasan_aprove.val().length > 0 && balasan_aprove.val().length < 4) {
                toastr.warning('Balasan terlalu pendek');
                balasan_aprove.select();
                return false;
            } 
           

            return true;
    }
     function save_aprove(){
         if (!validation_aprove()) return;
           $.ajax({
                type: "post",
                url: BASE_URL+"Siak/send_request_aprove",
                data: {
                  "_token": "{{ csrf_token() }}",
                  aprove_seq_id :  $("#aprove_seq_id").val(),
                  balasan_aprove :  $("#balasan_aprove").val(),
                  user_id :  "<?php echo $user_id; ?>"
                },
                beforeSend:
                function () {
                  $('#go_aprove').html("<i class='fa fa-circle-o-notch fa-spin fa-fw'></i> Wait");
                },
                success: function (data) {

                    if (data.message_type == 1){
                    toastr.success(data.message);
                    $("#balasan_aprove").val('');
                    }else if (data.message_type == 2){
                      toastr.error(data.message);
                    }
                    
                },
                error:
                function (data) {
                    toastr.error("Please Try Again, Something Went Wrong");
                $("#balasan_aprove").val('');
                do_close_aprove();

                },
                complete:
                function (response) {
                  $('#go_aprove').html("Aprove");
                  do_close_aprove();
                  do_refresh();
                }
            });

        }
    function validation_reject() {
            var reject_seq_id = $("#reject_seq_id");
            if (reject_seq_id.val().length == 0) {
              reject_seq_id.select();
                toastr.warning('Seq Id tidak boleh kosong');
                return false;
            }
            var balasan_reject = $("#balasan_reject");
            if (balasan_reject.val().length == 0) {
                toastr.warning('Balasan tidak boleh kosong');
                balasan_reject.select();
                return false;
            } 
            if (balasan_reject.val().length > 0 && balasan_reject.val().length < 4) {
                toastr.warning('Balasan terlalu pendek');
                balasan_reject.select();
                return false;
            } 
           

            return true;
    }
     function save_reject(){
         if (!validation_reject()) return;
           $.ajax({
                type: "post",
                url: BASE_URL+"Siak/send_request_reject",
                data: {
                  "_token": "{{ csrf_token() }}",
                  reject_seq_id :  $("#reject_seq_id").val(),
                  balasan_reject :  $("#balasan_reject").val(),
                  user_id :  "<?php echo $user_id; ?>"
                },
                beforeSend:
                function () {
                  $('#go_reject').html("<i class='fa fa-circle-o-notch fa-spin fa-fw'></i> Wait");
                },
                success: function (data) {

                    if (data.message_type == 1){
                    toastr.success(data.message);
                    $("#balasan_reject").val('');
                    }else if (data.message_type == 2){
                      toastr.error(data.message);
                    }
                    
                },
                error:
                function (data) {
                    toastr.error("Please Try Again, Something Went Wrong");
                $("#balasan_reject").val('');
                do_close_reject();

                },
                complete:
                function (response) {
                  $('#go_reject').html("Reject");
                  do_close_reject();
                  do_refresh();
                }
            });

        }


        function block_screen(){
        
        $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff'
            },
            baseZ: 2000
        }); 

    }
       
    function unblock_screen(){
       $.unblockUI();
    }
   </script>