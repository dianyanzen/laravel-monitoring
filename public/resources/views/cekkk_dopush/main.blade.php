<!DOCTYPE html>
<html lang="en">
@include('shared.head')
<body class="fix-header">
    <div id="wrapper">
            @include('shared.nav_top')
            @include('shared/nav_side')
            @include('cekkk_dopush/content')
    </div>
     
    @include('shared/footer')
    
    @include('cekkk_dopush/javascript')
</body>
</html>