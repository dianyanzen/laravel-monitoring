 <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $stitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                        <ol class="breadcrumb">
                           <li class="text-info"><?php echo $stitle; ?></li>
                        </ol>
                    </div>
                </div>
                 <div class="row">
                    <?php if (!empty($_GET['no_kk'])){ ?>
                <form name ="get_form" action="<?php echo $my_url; ?>?no_kk=<?php echo $_GET['no_kk']; ?>" method="post">
                    @csrf
                    <?php }else{ ?>
                <form name ="get_form" action="<?php echo $my_url; ?>" method="post">
                    @csrf
                    <?php } ?>
                    <div class="col-sm-12">
                        <div class="white-box">
                            
                                <?php if (!empty($_GET['no_kk'])){ ?>
                                <div class="row">
                                <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_no_kk" type="checkbox" checked disabled="true">
                                            <label for="cb_no_kk"> No KK</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">         
                                    <div class="form-group">
                                        <input class="form-control" type="text" id="no_kk" name="no_kk" onkeypress="return isNumberKey(event)" onchange="onlyNum()" value="<?php echo $_GET['no_kk']; ?>" readonly/></div>
                                </div>
                                </div>
                                <?php }else{ ?>
                                <?php if ($my_url != 'Push'){ ?>
                                <div class="row">
                                <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_no_kk" type="checkbox" checked disabled="true">
                                            <label for="cb_no_kk"> No KK</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">         
                                    <div class="form-group">
                                        <input class="form-control" type="text" id="no_kk" name="no_kk" onkeypress="return isNumberKey(event)" onchange="onlyNum()" /></div>
                                </div>
                                </div>
                       
                                <?php } ?>
                                <?php } ?>
                            
                           <?php if ($my_url == 'Push'){ ?>
                            <?php if (!empty($data)){ ?>
                            <div class="row">
                                  <button type="button" class="btn btn-info waves-effect waves-light m-r-10 pull-right" id="btn-reset" onclick="openTab('{{ url('/') }}/Check/KK');">Back <i class="mdi  mdi-backspace fa-fw"></i></button>
                                  
                            </div>
                            <?php }else{ ?>
                            <div class="row">
                                <button type="submit" name="do_search" class="btn btn-info waves-effect waves-light m-r-10 pull-right" id="btn-filter" onclick="on_serach();" >Push <i class="mdi  mdi-near-me fa-fw"></i></button>
                                   <button type="button" class="btn btn-info waves-effect waves-light m-r-10 pull-right" id="btn-reset" onclick="openTab('{{ url('/') }}/Check/KK');">Back <i class="mdi  mdi-backspace fa-fw"></i></button>
                                  
                            </div>
                            <?php } ?> 
                           <?php }else{ ?>
                           <div class="row">
                                <button type="submit" name="do_search" class="btn btn-info waves-effect waves-light m-r-10 pull-right" id="btn-filter" onclick="on_serach();" >Search <i class="mdi  mdi-magnify fa-fw"></i></button>
                                  <button type="button" class="btn btn-invert waves-effect waves-light m-r-10 pull-right" id="btn-reset" onclick="on_clear();">Clear <i class="mdi  mdi-delete fa-fw"></i></button>
                                  
                            </div>
                           <?php } ?>
                            
                        </div>
                    </div>
                    </form>
                </div>
                
                <div class="row">
                <div class="col-md-12">
                        <div class="white-box">
                            <h3 class="box-title"><?php echo $stitle; ?></h3>
                            <?php if (!empty($data)){ 
                                $NO_KKs = array();
                                foreach($data as $row){
                                    $NO_KKs[] = $row->no_kk;
                                    }
                                ?>
                                <div class="row">
                                <div class="col-md-4">
                                <div class="button-box">
                                     
                                </div>
                                </div>
                                </div>
                            <?php } ?>
                            <div class="scrollable">
                                <div class="table-responsive">
                                    <table id="demo-foo-row-toggler" class="table m-t-30 table-hover contact-list color-table info-table" data-page-size="100" style="margin-left: 1px; margin-right: 1px;">
                                        <thead>
                                            <tr>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">NO</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">NO KK</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">NAMA KEP</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">STATUS KK</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">REQUEST TTE</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">PRINT KK</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">NAMA KEC</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">NAMA KEL</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">NOMOR RW</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">NOMOR RT</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">ALAMAT</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">KODE POS</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">DOWNLOAD</th>
                                            </tr>
                                        </thead>
                                        <tbody id='my_data' style="border: 1px solid #e4e7ea;">
                                             <?php
                                        if (!empty($data)){
                                            $i = 0;
                                           foreach($data as $row){
                                            $i++;
                                            ?>
                                            <tr style="border: 1px solid #e4e7ea;">
                                                <td valign="center" style="border: 1px solid #e4e7ea; text-align: center;"><?php echo $i ;?></td>
                                                <td valign="center" style="border: 1px solid #e4e7ea; text-align: center;">
                                                   
                                                          <?php echo $row->no_kk ;?>
                                                </td>
                                                <td valign="center" style="border: 1px solid #e4e7ea; text-align: center;"><?php echo $row->nama_kep ;?>
                                                     
                                                </td>
                                                 <?php if ($row->cert_status == '0' || $row->cert_status == '1'){ ?>
                                                <td valign="center" style="border: 1px solid #e4e7ea; text-align: center;"> <a type="button" class="btn btn-info btn-xs" style="margin-top: 5px;"> BELUM DISERTIVIKASI <i class="mdi  mdi-information fa-fw"></i></a><br />
                                                  <?php }else if ($row->cert_status == '2'){ ?>
                                                <td valign="center" style="border: 1px solid #e4e7ea; text-align: center;"> <a type="button" class="btn btn-info btn-xs" style="margin-top: 5px;"> PROSES PENERBITAN <i class="mdi  mdi-upload fa-fw"></i></a><br />
                                                <?php }else if  ($row->cert_status == '3'){ ?>
                                                    <td valign="center" style="border: 1px solid #e4e7ea; text-align: center;"> <a type="button" class="btn btn-info btn-xs" style="margin-top: 5px;"> BELUM DIPUBLISH <i class="mdi  mdi-rocket fa-fw"></i></a><br />
                                                <?php }else if  ($row->cert_status == '9'){ ?>
                                                    <td valign="center" style="border: 1px solid #e4e7ea; text-align: center;"> <a type="button" class="btn btn-info btn-xs" style="margin-top: 5px;"> BELUM DIVERIVIKASI <i class="mdi  mdi-information fa-fw"></i></a><br />  
                                                <?php }else if  ($row->cert_status == '-'){ ?>
                                                    <td valign="center" style="border: 1px solid #e4e7ea; text-align: center;"> -<br />
                                                <?php }else{ ?>
                                                    <td valign="center" style="border: 1px solid #e4e7ea; text-align: center;"> <a type="button" class="btn btn-info btn-xs" style="margin-top: 5px;"> SUDAH DIPUBLISH <i class="mdi  mdi-check-circle fa-fw"></i></a><br />
                                                <?php } ?>
                                                <?php if ($row->cert_status == '2' || $row->cert_status == '3'){ ?>
                                                <?php if ($my_url != 'Push'){ ?>
                                                <?php if (!empty($data)){ ?>
                                                <button type="button" class="btn btn-info btn-xs" id="btn-repush" onclick="openInNewTab('{{ url('/') }}/Check/KK/Push?no_kk=<?php echo $row->no_kk ;?>');" style="margin-top: 5px;">Ajukan Ulang <i class="mdi  mdi-near-me fa-fw"></i></button>
                                                <?php } ?>
                                                <?php } ?>
                                                <?php } ?>
                                                </td>
                                                <td valign="center" style="border: 1px solid #e4e7ea;">
                                                    <?php if ($row->jml_pengajuan != '0'){ ?>
                                                    <i class="mdi mdi-rocket text-white" style="float: none; color: #292961 !important;"></i><span style="float: none; color: #292961 !important;"> Di Ajukan Sebanyak <?php echo $row->jml_pengajuan ;?> Kali</span><br>
                                                    <?php } ?>
                                                    <i class="mdi mdi-account-circle text-white" style="float: none; color: #292961 !important;"></i><span style="float: none; color: #292961 !important;"> Request Date : <?php echo $row->req_date ;?></span><br>
                                                    <i class="mdi mdi-label text-white" style="float: none; color: #292961 !important;"></i><span style="float: none; color: #292961 !important;"> Request By : <?php echo $row->req_by ;?></span><br>
                                                    <?php if ($row->aprove_date != '-'){ ?>
                                                    <i class="mdi mdi-printer-alert text-white" style="float: none; color: #292961 !important;"></i><span style="float: none; color: #292961 !important;"> Di Aprove Tanggal <?php echo $row->aprove_date ;?></span>
                                                    <?php } ?>
                                                </td>
                                                <td valign="center" style="border: 1px solid #e4e7ea; ">
                                                    <i class="mdi mdi-account-circle text-white" style="float: none; color: #292961 !important;"></i><span style="float: none; color: #292961 !important;"> Print Date : <?php echo $row->printed_date ;?></span><br>
                                                    <i class="mdi mdi-label text-white" style="float: none; color: #292961 !important;"></i><span style="float: none; color: #292961 !important;"> Print By : <?php echo $row->printed_by ;?></span><br>
                                                    <?php if ($row->count_kk != 0 && $row->printed_by != '-'){ ?>
                                                    <i class="mdi mdi-printer-alert text-white" style="float: none; color: #292961 !important;"></i><span style="float: none; color: #292961 !important;"> Di Cetak Sebanyak <?php echo $row->count_kk ;?> Kali</span>
                                                    <?php } ?>
                                                </td>
                                                 <td valign="center" style="border: 1px solid #e4e7ea; text-align: center;"><?php echo $row->nama_kec ;?></td>
                                                <td valign="center" style="border: 1px solid #e4e7ea; text-align: center;"><?php echo $row->nama_kel ;?></td>
                                                <td valign="center" style="border: 1px solid #e4e7ea; text-align: center;"><?php echo $row->no_rw ;?></td>
                                                <td valign="center" style="border: 1px solid #e4e7ea; text-align: center;"><?php echo $row->no_rt ;?></td>
                                                <td valign="center" style="border: 1px solid #e4e7ea; text-align: center;"><?php echo $row->alamat ;?></td>
                                                <td valign="center" style="border: 1px solid #e4e7ea; text-align: center;"><?php echo $row->kode_pos ;?></td>
                                                 <?php if ($row->cert_status == '1' || $row->cert_status == '2' || $row->cert_status == '3'){ ?>
                                                <td valign="center" style="border: 1px solid #e4e7ea; text-align: center;">-</td>
                                                 <?php }else{ ?>
                                                     <?php if ($row->url_download != '-'){ ?>
                                                <td valign="center" style="border: 1px solid #e4e7ea; text-align: center;"><button type="button" class="btn btn-info btn-xs" id="btn-download" onclick="openInNewTab('<?php echo $row->url_download ;?>');" style="margin-top: 5px;">DOWNLOAD <i class="mdi mdi-download fa-fw"></i></button></td>
                                                    <?php } ?>
                                                <?php } ?>
                                            </tr>

                                            <?php } }else{ ?>
                                            
                                                <tr>
                                                <td colspan="13" style="text-align: center;">No data available</td>
                                            </tr>
                                            <?php }?>

                                        </tbody>
                                        <tfoot>
                                            <tr style="border: 0px solid black;">
                                                
                                                <td colspan="13" style="border: 0px solid black;">
                                                    <div class="text-left">
                                                        <ul class="pagination"> </ul>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tfoot>
                                        <tfoot id="my_foot">
                                            
                                                <?php if (!empty($data)){ ?>
                                                <tr>
                                                <th colspan="13" style="text-align: left;">Total : <?php echo $i ;?> Data</th>
                                                </tr>
                                                <?php }else{ ?>
                                                <tr>
                                                <th colspan="13" style="text-align: left;">Total : 0 Data</th>
                                                </tr>
                                                <?php } ?>
                                            </tfoot>

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                 </div>
       @include('shared.footer_detail')