    <script src="{{ url('/') }}/assets/plugins/bower_components/owl.carousel/owl.carousel.min.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/owl.carousel/owl.custom.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/flot/jquery.flot.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/Minimal-Gauge-chart/js/cmGauge.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/Minimal-Gauge-chart/js/cmGauge.js"></script>
    <!-- Footable -->
    <script src="{{ url('/') }}/assets/plugins/bower_components/footable/js/footable.all.min.js"></script>
    
    <!--FooTable init-->
    <script src="{{ url('/') }}/assets/js/footable-init.js"></script>
    <script type="text/javascript" src="{{ url('/') }}/assets/plugins/bower_components/viewerjs-master/dist/jquery.magnify.js"></script>
   <script src="{{ url('/') }}/assets/plugins/bower_components/dropify/dist/js/dropify.min.js"></script>
    <script>
        function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }
        function isphone(evt)
       {
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode != 43 && charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }
       function validateEmail(email) {
          const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
          return re.test(email);
        }

       jQuery('#tgl_lhr').datepicker({
            format: 'yyyy-mm-dd',               
            autoclose: true,
            todayHighlight: true,
            orientation: "bottom left"
        });

       jQuery('#tgl_datang').datepicker({
            format: 'yyyy-mm-dd',               
            autoclose: true,
            todayHighlight: true,
            orientation: "bottom left"
        });
        $(document).ready(function () {
             $(".yztoptip").tooltip({
                  placement:"top"
              });
            $('#nik').on('paste', function() {
              var $el = $(this);
              setTimeout(function() {
                $el.val(function(i, val) {
                  return val.replace(/[^0-9,]/g, '')
                })
              })
            });
            $('#Telepon').on('paste', function() {
              var $el = $(this);
              setTimeout(function() {
                $el.val(function(i, val) {
                  return val.replace(/[^0-9,]/g, '')
                })
              })
            });
            $('#src_no_rw').on('paste', function() {
              var $el = $(this);
              setTimeout(function() {
                $el.val(function(i, val) {
                  return val.replace(/[^0-9,]/g, '')
                })
              })
            });
            $('#src_no_rt').on('paste', function() {
              var $el = $(this);
              setTimeout(function() {
                $el.val(function(i, val) {
                  return val.replace(/[^0-9,]/g, '')
                })
              })
            });
            $('#no_rw').on('paste', function() {
              var $el = $(this);
              setTimeout(function() {
                $el.val(function(i, val) {
                  return val.replace(/[^0-9,]/g, '')
                })
              })
            });
            $('#no_rt').on('paste', function() {
              var $el = $(this);
              setTimeout(function() {
                $el.val(function(i, val) {
                  return val.replace(/[^0-9,]/g, '')
                })
              })
            });
            $('.dropify').dropify();
          
            get_api();
             $('select[name="src_prov"]').on('change', function() {

            var no_prop = $(this).val();

            if(no_prop != 0) {
                
                $.ajax({
                    type: "post",
                    url: BASE_URL+"api/ektp-kab",
                    dataType: "json",
                    data: {
                      "_token": "{{ csrf_token() }}",
                        no_prop : $('select[name="src_prov"]').val()
                    },
                    beforeSend:
                    function () {
                        $('select[name="src_kab"]').attr("disabled",true);
                        $('select[name="src_kec"]').attr("disabled",true);
                        $('select[name="src_kel"]').attr("disabled",true);
                    },
                    success: function (data) {
                        console.log(data);
                        $('select[name="src_kab"]').empty();
                        $('select[name="src_kab"]').append('<option value="0">-- Pilih Kabupaten --</option>');
                        $.each(data.kabupaten, function(key, value) {
                            $('select[name="src_kab"]').append('<option value="'+ value.no_kab +'">'+ value.nama_kab +'</option>');
                        });
                       $('select[name="src_kab"]').val("0").trigger("change");
                        
                    },
                    error:
                    function (data) {
               

                    },
                    complete:
                    function (response) {
                        $('select[name="src_kab"]').attr("disabled",false);
                        $('select[name="src_kec"]').attr("disabled",false);
                        $('select[name="src_kel"]').attr("disabled",false);
                    }
                });
            }else{
                 $('select[name="src_kab"]').empty();
                 $('select[name="src_kab"]').append('<option value="0">-- Pilih Kabupaten --</option>');
                 $('select[name="src_kab"]').val("0").trigger("change");
            }

        });
             $('select[name="src_kab"]').on('change', function() {

            var no_kab = $(this).val();

            if(no_kab != 0) {
                
                $.ajax({
                    type: "post",
                    url: BASE_URL+"api/ektp-kec",
                    dataType: "json",
                    data: {
                      "_token": "{{ csrf_token() }}",
                        no_prop : $('select[name="src_prov"]').val(),
                        no_kab : $('select[name="src_kab"]').val()
                    },
                    beforeSend:
                    function () {
                        $('select[name="src_kec"]').attr("disabled",true);
                        $('select[name="src_kel"]').attr("disabled",true);
                    },
                    success: function (data) {
                        console.log(data);
                        $('select[name="src_kec"]').empty();
                        $('select[name="src_kec"]').append('<option value="0">-- Pilih Kecamatan --</option>');
                        $.each(data.kecamatan, function(key, value) {
                            $('select[name="src_kec"]').append('<option value="'+ value.no_kec +'">'+ value.nama_kec +'</option>');
                        });
                       $('select[name="src_kec"]').val("0").trigger("change");
                        
                    },
                    error:
                    function (data) {
               

                    },
                    complete:
                    function (response) {
                        $('select[name="src_kec"]').attr("disabled",false);
                        $('select[name="src_kel"]').attr("disabled",false);
                    }
                });
            }else{
                 $('select[name="src_kec"]').empty();
                 $('select[name="src_kec"]').append('<option value="0">-- Pilih Kecamatan --</option>');
                 $('select[name="src_kec"]').val("0").trigger("change");
            }

        });
         $('select[name="src_kec"]').on('change', function() {

        var no_kec = $(this).val();

        if(no_kec != 0) {
            
            $.ajax({
                type: "post",
                url: BASE_URL+"api/ektp-kel",
                dataType: "json",
                data: {
                  "_token": "{{ csrf_token() }}",
                    no_prop : $('select[name="src_prov"]').val(),
                    no_kab : $('select[name="src_kab"]').val(),
                    no_kec : $('select[name="src_kec"]').val()
                },
                beforeSend:
                function () {
                    $('select[name="src_kel"]').attr("disabled",true);
                },
                success: function (data) {
                    console.log(data);
                      $('select[name="src_kel"]').empty();
                   $('select[name="src_kel"]').append('<option value="0">-- Pilih Kelurahan --</option>');
                    $.each(data.kelurahan, function(key, value) {
                        $('select[name="src_kel"]').append('<option value="'+ value.no_kel +'">'+ value.nama_kel +'</option>');
                    });
                   $('select[name="src_kel"]').val("0").trigger("change");
                    
                },
                error:
                function (data) {
           

                },
                complete:
                function (response) {
                    $('select[name="src_kel"]').attr("disabled",false);
                }
            });
        }else{
             $('select[name="src_kel"]').empty();
             $('select[name="src_kel"]').append('<option value="0">-- Pilih Kelurahan --</option>');
             $('select[name="src_kel"]').val("0").trigger("change");
        }

    });

        $('select[name="kec"]').on('change', function() {

            var no_kec = $(this).val();

            if(no_kec != 0) {
                
                $.ajax({
                    type: "post",
                    url: BASE_URL+"api/ektp-kel",
                    dataType: "json",
                    data: {
                      "_token": "{{ csrf_token() }}",
                        no_prop : 32,
                        no_kab : 73,
                        no_kec : $('select[name="kec"]').val()
                    },
                    beforeSend:
                    function () {
                        $('select[name="kel"]').attr("disabled",true);
                    },
                    success: function (data) {
                        console.log(data);
                          $('select[name="kel"]').empty();
                       $('select[name="kel"]').append('<option value="0">-- Pilih Kelurahan --</option>');
                        $.each(data.kelurahan, function(key, value) {
                            $('select[name="kel"]').append('<option value="'+ value.no_kel +'">'+ value.nama_kel +'</option>');
                        });
                       $('select[name="kel"]').val("0").trigger("change");
                        
                    },
                    error:
                    function (data) {
               

                    },
                    complete:
                    function (response) {
                        $('select[name="kel"]').attr("disabled",false);
                    }
                });
            }else{
                 $('select[name="kel"]').empty();
                 $('select[name="kel"]').append('<option value="0">-- Pilih Kelurahan --</option>');
                 $('select[name="kel"]').val("0").trigger("change");
            }

        });

        });

    </script>
 <script>


       
    function format ( d ) {
            // `d` is the original data object for the row
            return '<table class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%" >'+
                '<tr>'+
                    '<td>'+d.img+'</td>'+
                '</tr>'+
            '</table>';
        }
         
        $(document).ready(function() {

            var table = $('#pengajuan-list').DataTable( {
                "ajax": {
                 "url": BASE_URL+"ApiEpunten/get_skts_melengkapi",
                "type": "post",
                },
                responsive: true,
                destroy: true,
                 "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                 "columnDefs": [
                    {"className": "dt-center", "targets": "_all"}
                  ],
                "pageLength" : 50,
                "columns": [
                    {
                        "className":      'details-control',
                        "orderable":      false,
                        "data":           null,
                        "defaultContent": ''
                    },
                    { "data": "no" },
                    { "data": "nik" },
                    { "data": "nama_lgkp" },
                    { "data": "telepon" },
                    { "data": "email" },
                    { "data": "created_at" },
                    { "data": "aksi" }
                ],
                "order": [[1, 'asc']]
            });
         $('#pengajuan-list tbody').on('click', 'td.details-control', function () {
                var tr = $(this).closest('tr');
                var row = table.row( tr );
         
                if ( row.child.isShown() ) {
                    row.child.hide();
                    tr.removeClass('shown');
                }
                else {
                    row.child( format(row.data()) ).show();
                    tr.addClass('shown');
                }
            });
            // get_table();
             
            
        });
    function get_api(){
                $.ajax({
                    type: "post",
                    url: BASE_URL+"api/ektp",
                    dataType: "json",
                    data: {
                      "_token": "{{ csrf_token() }}",
                    },
                    beforeSend:
                    function () {

                    },
                    success: function (data) {
                        console.log(data);
                        //init data src prop
                        $('select[name="src_prov"]').empty();
                        $('select[name="src_prov"]').append('<option value="0">-- Pilih Provinsi --</option>');
                        $.each(data.prov, function(key, value) {
                            $('select[name="src_prov"]').append('<option value="'+ value.no_prop +'">'+ value.nama_prop +'</option>');
                        });
                        $('select[name="src_prov"]').val("0").trigger("change");

                        //init data kec
                        $('select[name="kec"]').empty();
                        $('select[name="kec"]').append('<option value="0">-- Pilih Kecamatan --</option>');
                        $.each(data.kecamatan, function(key, value) {
                            $('select[name="kec"]').append('<option value="'+ value.no_kec +'">'+ value.nama_kec +'</option>');
                        });
                        $('select[name="kec"]').val("0").trigger("change");

                        //init data jenis klmin
                        $('select[name="jenis_klmin"]').empty();
                        $('select[name="jenis_klmin"]').append('<option value="0">-- Pilih Jenis Kelamin --</option>');
                        $.each(data.jenis_klmin, function(key, value) {
                            $('select[name="jenis_klmin"]').append('<option value="'+ value.no +'">'+ value.no +' - '+ value.jenis_klmin +'</option>');
                        });
                        $('select[name="jenis_klmin"]').val("0").trigger("change");

                        //init data agama
                        $('select[name="agama"]').empty();
                        $('select[name="agama"]').append('<option value="0">-- Pilih Agama --</option>');
                        $.each(data.agama, function(key, value) {
                            $('select[name="agama"]').append('<option value="'+ value.no +'">'+ value.agama +'</option>');
                        });
                        $('select[name="agama"]').val("0").trigger("change");

                        //init data stat_kwn
                        $('select[name="stat_kwn"]').empty();
                        $('select[name="stat_kwn"]').append('<option value="0">-- Pilih Status Perkawinan --</option>');
                        $.each(data.stat_kwn, function(key, value) {
                            $('select[name="stat_kwn"]').append('<option value="'+ value.no +'">'+ value.stat_kwn +'</option>');
                        });
                        $('select[name="stat_kwn"]').val("0").trigger("change");

                        //init data gol_drh
                        $('select[name="gol_drh"]').empty();
                        $('select[name="gol_drh"]').append('<option value="0">-- Pilih Golongan Darah --</option>');
                        $.each(data.gol_drh, function(key, value) {
                            $('select[name="gol_drh"]').append('<option value="'+ value.no +'">'+ value.gol_drh +'</option>');
                        });
                        $('select[name="gol_drh"]').val("0").trigger("change");

                        //init data pendidikan
                        $('select[name="pendidikan"]').empty();
                        $('select[name="pendidikan"]').append('<option value="0">-- Pilih Pendidikan --</option>');
                        $.each(data.pendidikan, function(key, value) {
                            $('select[name="pendidikan"]').append('<option value="'+ value.no +'">'+ value.pendidikan +'</option>');
                        });
                        $('select[name="pendidikan"]').val("0").trigger("change");

                        //init data pekerjaan
                        $('select[name="pekerjaan"]').empty();
                        $('select[name="pekerjaan"]').append('<option value="0">-- Pilih Pekerjaan --</option>');
                        $.each(data.pekerjaan, function(key, value) {
                            $('select[name="pekerjaan"]').append('<option value="'+ value.no +'">'+ value.pekerjaan +'</option>');
                        });
                        $('select[name="pekerjaan"]').val("0").trigger("change");

                        //init data pekerjaan
                        $('select[name="stat_hbkel"]').empty();
                        $('select[name="stat_hbkel"]').append('<option value="0">-- Pilih Hubungan Keluarga --</option>');
                        $.each(data.stat_hbkel, function(key, value) {
                            $('select[name="stat_hbkel"]').append('<option value="'+ value.no +'">'+ value.stat_hbkel +'</option>');
                        });
                        $('select[name="stat_hbkel"]').val("0").trigger("change");

                        //init data pekerjaan
                        $('select[name="alasan_pindah"]').empty();
                        $('select[name="alasan_pindah"]').append('<option value="0">-- Pilih Alasan Pindah --</option>');
                        $.each(data.alasan, function(key, value) {
                            $('select[name="alasan_pindah"]').append('<option value="'+ value.no +'">'+ value.alasan +'</option>');
                        });
                        $('select[name="alasan_pindah"]').val("0").trigger("change");
                        
                    },
                    error:
                    function (data) {
               

                    },
                    complete:
                    function (response) {
                      
                    }
                });
            }
   
    function get_wil(){
                $.ajax({
                    type: "post",
                    url: BASE_URL+"api/ektp",
                    dataType: "json",
                    data: {
                      "_token": "{{ csrf_token() }}",
                    },
                    beforeSend:
                    function () {

                    },
                    success: function (data) {
                        console.log(data);
                        //init data src prop
                        $('select[name="src_prov"]').empty();
                        $('select[name="src_prov"]').append('<option value="0">-- Pilih Provinsi --</option>');
                        $.each(data.prov, function(key, value) {
                            $('select[name="src_prov"]').append('<option value="'+ value.no_prop +'">'+ value.nama_prop +'</option>');
                        });
                        $('select[name="src_prov"]').val("0").trigger("change");

                        
                        
                    },
                    error:
                    function (data) {
               

                    },
                    complete:
                    function (response) {
                      
                    }
                });
            }
             function get_wil2(){
                $.ajax({
                    type: "post",
                    url: BASE_URL+"api/ektp",
                    dataType: "json",
                    data: {
                      "_token": "{{ csrf_token() }}",
                    },
                    beforeSend:
                    function () {

                    },
                    success: function (data) {
                        console.log(data);
                        //init data src prop
                       $('select[name="kec"]').empty();
                        $('select[name="kec"]').append('<option value="0">-- Pilih Kecamatan --</option>');
                        $.each(data.kecamatan, function(key, value) {
                            $('select[name="kec"]').append('<option value="'+ value.no_kec +'">'+ value.nama_kec +'</option>');
                        });
                        $('select[name="kec"]').val("0").trigger("change");

                        
                        
                    },
                    error:
                    function (data) {
               

                    },
                    complete:
                    function (response) {
                      
                    }
                });
            }
    function get_table(){
        var table = $('#pengajuan-list').DataTable();
        table.ajax.reload();
        toastr.success('Data Berhasil Di Perbarui !');
    }
    function block_screen(){
        
        $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff'
            },
            baseZ: 2000
        }); 
    }
    function unblock_screen(){
       $.unblockUI();
    }
    function get_upl(){
        $("#blockimgupload1").show();
        $("#blockimgdisplay1").hide();
        $("#blockimgupload2").show();
        $("#blockimgdisplay2").hide();
        $("#blockimgupload3").show();
        $("#blockimgdisplay3").hide();
        $("#blockimgupload4").show();
        $("#blockimgdisplay4").hide();
        $("#blockimgupload5").show();
        $("#blockimgdisplay5").hide();
    }
    function get_info(){
      $('#info_modal').modal('show');
    }

    function on_clear (data){
        get_wil();
        get_wil2();

        $('#nik').val("");
        $('#no_kk').val("");
        $('#nama_lgkp').val("");
        $('#tmpt_lhr').val("");
        $('#tgl_lhr').val("");
        $('#telepon').val("+62");
        $('#email').val("");
        $('#src_alamat').val("");
        $('#src_rw').val("");
        $('#src_rt').val("");
        $('#alamat').val("");
        $('#no_rw').val("");
        $('#no_rt').val("");
        $('#jum_anggota').val("0");
        $('#jangka_waktu').val("0");
        $('#tgl_datang').val("");
        $('#modal-title').html("TAMBAH PENGAJUAN");
        $('select[name="src_prov"]').val("0").trigger("change");
        $('select[name="kec"]').val("0").trigger("change");
        $('select[name="jenis_klmin"]').val("0").trigger("change");
        $('select[name="agama"]').val("0").trigger("change");
        $('select[name="stat_kwn"]').val("0").trigger("change");
        $('select[name="gol_drh"]').val("0").trigger("change");
        $('select[name="pendidikan"]').val("0").trigger("change");
        $('select[name="pekerjaan"]').val("0").trigger("change");
        $('select[name="stat_hbkel"]').val("0").trigger("change");
        $('select[name="alasan_pindah"]').val("0").trigger("change");
        $('.dropify-clear').click();
        $('#is_asal').hide();
        $('#is_tujuan').hide();
        $('#is_upload').hide();
        $("#blockimgdisplay1").hide();
        $("#blockimgdisplay2").hide();
        $("#blockimgdisplay3").hide();
        $("#blockimgdisplay4").hide();
        $("#blockimgdisplay5").hide();
        $("#blockimgupload1").show();
        $("#blockimgupload2").show();
        $("#blockimgupload3").show();
        $("#blockimgupload4").show();
        $("#blockimgupload5").show();
        $("#submit_edit").hide();
        $('#submit_btn').show();
        $('#pengajuan_modal').modal('show');
    }

    $('#submit_btn').click(function (e) {
       e.preventDefault();
        if (!validationmodal()) return;
        var jenis_layanan =  1;
        var status =  1;
        var nik = $('#nik').val();
        var no_kk = $('#no_kk').val();
        var nama_lgkp = $('#nama_lgkp').val();
        var tmpt_lhr = $('#tmpt_lhr').val();
        var tgl_lhr = $('#tgl_lhr').val();
        var telepon = $('#telepon').val();
        var email = $('#email').val();
        var src_alamat = $('#src_alamat').val();
        var src_rw = $('#src_rw').val();
        var src_rt = $('#src_rt').val();
        var alamat = $('#alamat').val();
        var no_rw = $('#no_rw').val();
        var no_rt = $('#no_rt').val();
        var jum_anggota = $('#jum_anggota').val();
        var jangka_waktu = $('#jangka_waktu').val();
        var tgl_datang = $('#tgl_datang').val();
        var src_prov = $('select[name="src_prov"]').val();
        var src_kab = $('select[name="src_kab"]').val();
        var src_kec = $('select[name="src_kec"]').val();
        var src_kel = $('select[name="src_kel"]').val();
        var prop = $('select[name="prop"]').val();
        var kab = $('select[name="kab"]').val();
        var kec = $('select[name="kec"]').val();
        var kel = $('select[name="kel"]').val();
        var jenis_klmin = $('select[name="jenis_klmin"]').val();
        var agama = $('select[name="agama"]').val();
        var stat_kwn = $('select[name="stat_kwn"]').val();
        var gol_drh = $('select[name="gol_drh"]').val();
        var stat_hbkel = $('select[name="stat_hbkel"]').val();
        var alasan_pindah = $('select[name="alasan_pindah"]').val();
        var pendidikan = $('select[name="pendidikan"]').val();
        var pekerjaan = $('select[name="pekerjaan"]').val();
        var imgupload1 = $('#imgupload1').prop('files')[0];   
        var imgupload2 = $('#imgupload2').prop('files')[0];   
        var imgupload3 = $('#imgupload3').prop('files')[0];   
        var imgupload4 = $('#imgupload4').prop('files')[0];  
        var imgupload5 = $('#imgupload5').prop('files')[0];  
        var data = new FormData(); 
        data.append('imgupload1',  imgupload1);
        data.append('imgupload2',  imgupload2);
        data.append('imgupload3',  imgupload3);
        data.append('imgupload4',  imgupload4);
        data.append('imgupload5',  imgupload5);
        data.append('jenis_layanan',jenis_layanan);
        data.append('status',status);
        data.append('nik',nik);
        data.append('no_kk',no_kk);
        data.append('nama_lgkp',nama_lgkp);
        data.append('tmpt_lhr',tmpt_lhr);
        data.append('tgl_lhr',tgl_lhr);
        data.append('telepon',telepon);
        data.append('email',email);
        data.append('src_alamat',src_alamat);
        data.append('src_rw',src_rw);
        data.append('src_rt',src_rt);
        data.append('alamat',alamat);
        data.append('no_rw',no_rw);
        data.append('no_rt',no_rt);
        data.append('jum_anggota',jum_anggota);
        data.append('jangka_waktu',jangka_waktu);
        data.append('tgl_datang',tgl_datang);
        data.append('src_prov',src_prov);
        data.append('src_kab',src_kab);
        data.append('src_kec',src_kec);
        data.append('src_kel',src_kel);
        data.append('prop',prop);
        data.append('kab',kab);
        data.append('kec',kec);
        data.append('kel',kel);
        data.append('jenis_klmin',jenis_klmin);
        data.append('agama',agama);
        data.append('stat_kwn',stat_kwn);
        data.append('gol_drh',gol_drh);
        data.append('stat_hbkel',stat_hbkel);
        data.append('alasan_pindah',alasan_pindah);
        data.append('pendidikan',pendidikan);
        data.append('pekerjaan',pekerjaan);

         $.ajax({
            type: "post",
            url: BASE_URL+"api/andro/epunten",
            data: data,
              enctype: 'multipart/form-data',
              async: false,
              dataType: 'json',
              processData: false,
              contentType: false,
                beforeSend:
                function () {
                  block_screen();
                },
                success: function (data) {

                    if (data.status == 'error'){
                        swal("Danger !", data.data, "warning");
                    }else{
                      swal("Success !", data.data, "success");
                    }
                    
                },
                error:
                function (data) {
                     swal("Danger !", "Please Try Again, Something Went Wrong", "warning");

                },
                complete:
                function (response) {
                 unblock_screen();
                 on_clear();
                 $('#pengajuan_modal').modal('hide');
                 get_table();
                }
            });
      
    });

    $('#submit_edit').click(function (e) {
       e.preventDefault();
        if (!validationmodaledit()) return;
        var jenis_layanan =  1;
        var status =  1;
        var nik = $('#nik').val();
        var no_kk = $('#no_kk').val();
        var nama_lgkp = $('#nama_lgkp').val();
        var tmpt_lhr = $('#tmpt_lhr').val();
        var tgl_lhr = $('#tgl_lhr').val();
        var telepon = $('#telepon').val();
        var email = $('#email').val();
        var src_alamat = $('#src_alamat').val();
        var src_rw = $('#src_rw').val();
        var src_rt = $('#src_rt').val();
        var alamat = $('#alamat').val();
        var no_rw = $('#no_rw').val();
        var no_rt = $('#no_rt').val();
        var jum_anggota = $('#jum_anggota').val();
        var jangka_waktu = $('#jangka_waktu').val();
        var tgl_datang = $('#tgl_datang').val();
        var src_prov = $('select[name="src_prov"]').val();
        var src_kab = $('select[name="src_kab"]').val();
        var src_kec = $('select[name="src_kec"]').val();
        var src_kel = $('select[name="src_kel"]').val();
        var prop = $('select[name="prop"]').val();
        var kab = $('select[name="kab"]').val();
        var kec = $('select[name="kec"]').val();
        var kel = $('select[name="kel"]').val();
        var jenis_klmin = $('select[name="jenis_klmin"]').val();
        var agama = $('select[name="agama"]').val();
        var stat_kwn = $('select[name="stat_kwn"]').val();
        var gol_drh = $('select[name="gol_drh"]').val();
        var stat_hbkel = $('select[name="stat_hbkel"]').val();
        var alasan_pindah = $('select[name="alasan_pindah"]').val();
        var pendidikan = $('select[name="pendidikan"]').val();
        var pekerjaan = $('select[name="pekerjaan"]').val();
        var imgupload1 = $('#imgupload1').prop('files')[0];   
        var imgupload2 = $('#imgupload2').prop('files')[0];   
        var imgupload3 = $('#imgupload3').prop('files')[0];   
        var imgupload4 = $('#imgupload4').prop('files')[0];  
        var imgupload5 = $('#imgupload5').prop('files')[0];  
        var verified_by = '{{ $user_id }}';  
        var data = new FormData(); 
        data.append('imgupload1',  imgupload1);
        data.append('imgupload2',  imgupload2);
        data.append('imgupload3',  imgupload3);
        data.append('imgupload4',  imgupload4);
        data.append('imgupload5',  imgupload5);
        data.append('jenis_layanan',jenis_layanan);
        data.append('status',status);
        data.append('nik',nik);
        data.append('no_kk',no_kk);
        data.append('nama_lgkp',nama_lgkp);
        data.append('tmpt_lhr',tmpt_lhr);
        data.append('tgl_lhr',tgl_lhr);
        data.append('telepon',telepon);
        data.append('email',email);
        data.append('src_alamat',src_alamat);
        data.append('src_rw',src_rw);
        data.append('src_rt',src_rt);
        data.append('alamat',alamat);
        data.append('no_rw',no_rw);
        data.append('no_rt',no_rt);
        data.append('jum_anggota',jum_anggota);
        data.append('jangka_waktu',jangka_waktu);
        data.append('tgl_datang',tgl_datang);
        data.append('src_prov',src_prov);
        data.append('src_kab',src_kab);
        data.append('src_kec',src_kec);
        data.append('src_kel',src_kel);
        data.append('prop',prop);
        data.append('kab',kab);
        data.append('kec',kec);
        data.append('kel',kel);
        data.append('jenis_klmin',jenis_klmin);
        data.append('agama',agama);
        data.append('stat_kwn',stat_kwn);
        data.append('gol_drh',gol_drh);
        data.append('stat_hbkel',stat_hbkel);
        data.append('alasan_pindah',alasan_pindah);
        data.append('pendidikan',pendidikan);
        data.append('pekerjaan',pekerjaan);
        data.append('verified_by',verified_by);

         $.ajax({
            type: "post",
            url: BASE_URL+"api/andro/re_edit",
            data: data,
              enctype: 'multipart/form-data',
              async: false,
              dataType: 'json',
              processData: false,
              contentType: false,
                beforeSend:
                function () {
                  block_screen();
                },
                success: function (data) {

                    if (data.status == 'error'){
                        swal("Danger !", data.data, "warning");
                    }else{
                      swal("Success !", data.data, "success");
                    }
                    
                },
                error:
                function (data) {
                     swal("Danger !", "Please Try Again, Something Went Wrong", "warning");

                },
                complete:
                function (response) {
                 unblock_screen();
                 on_clear();
                 $('#pengajuan_modal').modal('hide');
                 get_table();
                }
            });
      
    });

    $('#submit_btnntf').click(function (e) {
       if (!validationmodaltolak()) return;
            $.ajax({
                    type: "post",
                    url: BASE_URL+"ApiEpunten/get_skts_tolak",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        id_peng : $("#id_pengajuan_tolak").val(),
                        isi_pesan : $("#isi_pesan").val(),
                        jenis_layanan_tolak : $("#jenis_layanan_tolak").val(),
                        nik_tolak : $("#nik_tolak").val(),
                    },
                    beforeSend:
                    function () {
                         block_screen();
                    },
                    success: function (data) {
                        console.log(data);
                        swal("Success!", data.message, "success");
                        $('#tolak_modal').modal('hide');
                    },
                    error:
                    function (data) {
                        unblock_screen();
                        swal("Error!", "Ooops!, Please Try Again, Something Went Wrong", "error"); 

                    },
                    complete:
                    function (response) {
                        unblock_screen();
                        get_table();
                    }
                });
      
    });

       function validationmodal() {
            var nik = $("#nik");
            if (nik.val().length != 16) {
                nik.select();
                swal("Warning!", "NIK Tidak Boleh Kosong Atau Kurang Dari 16 Digit !", "warning");  
                return false;
            }
            var no_kk = $("#no_kk");
            if (no_kk.val().length != 16) {
                no_kk.select();
                swal("Warning!", "NOKK Tidak Boleh Kosong Atau Kurang Dari 16 Digit !", "warning");  
                return false;
            }
            var nama_lgkp = $("#nama_lgkp");
            if (nama_lgkp.val().length == 0) {
                nama_lgkp.select();
                swal("Warning!", "Nama Lengkap Tidak Boleh Kosong !", "warning");  
                return false;
            }
            var tmpt_lhr = $("#tmpt_lhr");
            if (tmpt_lhr.val().length == 0) {
                tmpt_lhr.select();
                swal("Warning!", "Tempat Lahir Tidak Boleh Kosong !", "warning");  
                return false;
            }
            var jum_anggota = $("#jum_anggota");
            if (jum_anggota.val().length == 0) {
                jum_anggota.select();
                swal("Warning!", "Jumlah Anggota Tidak Boleh Kosong !", "warning");  
                return false;
            }
            var jangka_waktu = $("#jangka_waktu");
            if (jangka_waktu.val().length == 0) {
                jangka_waktu.select();
                swal("Warning!", "Jangka Waktu Tidak Boleh Kosong !", "warning");  
                return false;
            }
            var tgl_lhr = $("#tgl_lhr");
            if (tgl_lhr.val().length == 0) {
                tgl_lhr.select();
                swal("Warning!", "Tanggal Lahir Tidak Boleh Kosong !", "warning");  
                return false;
            }
            var tgl_datang = $("#tgl_datang");
            if (tgl_datang.val().length == 0) {
                tgl_datang.select();
                swal("Warning!", "Tanggal Datang Tidak Boleh Kosong !", "warning");  
                return false;
            }
            var jenis_klmin = $("#jenis_klmin");
            if (jenis_klmin.val() == 0) {
                jenis_klmin.focus();
                swal("Warning!", "Jenis Kelamin Harus Dipilih !", "warning");  
                return false;
            }
            var agama = $("#agama");
            if (agama.val() == 0) {
                agama.focus();
                swal("Warning!", "Agama Harus Dipilih !", "warning");  
                return false;
            }
            var stat_hbkel = $("#stat_hbkel");
            if (stat_hbkel.val() == 0) {
                stat_hbkel.focus();
                swal("Warning!", "Hubungan Keluarga Harus Dipilih !", "warning");  
                return false;
            }
            var alasan_pindah = $("#alasan_pindah");
            if (alasan_pindah.val() == 0) {
                alasan_pindah.focus();
                swal("Warning!", "Alasan Pindah Harus Dipilih !", "warning");  
                return false;
            }
            var gol_drh = $("#gol_drh");
            if (gol_drh.val() == 0) {
                gol_drh.focus();
                swal("Warning!", "Golongan Darah Harus Dipilih !", "warning");  
                return false;
            }
            var pendidikan = $("#pendidikan");
            if (pendidikan.val() == 0) {
                pendidikan.focus();
                swal("Warning!", "Pendidikan Harus Dipilih !", "warning");  
                return false;
            }
            var pekerjaan = $("#pekerjaan");
            if (pekerjaan.val() == 0) {
                pekerjaan.focus();
                swal("Warning!", "Pekerjaan Harus Dipilih !", "warning");  
                return false;
            }
            var telepon = $("#telepon");
            if (telepon.val().length < 4) {
                telepon.select();
                swal("Warning!", "Telepon Tidak Boleh Kosong !", "warning");  
                return false;
            }
            var email = $("#email");
            if (email.val().length == 0) {
                email.select();
                swal("Warning!", "Email Tidak Boleh Kosong !", "warning");  
                return false;
            }
            if (!validateEmail(email.val())){
                 email.select();
                swal("Warning!", "Format Email Salah !", "warning");  
                return false;
            }

            var src_prov = $("#src_prov");
            if (src_prov.val() == 0) {
                src_prov.focus();
                swal("Warning!", "Provinsi Asal Harus Dipilih !", "warning");  
                return false;
            }
            var src_kab = $("#src_kab");
            if (src_kab.val() == 0) {
                src_kab.focus();
                swal("Warning!", "Kabupaten Asal Harus Dipilih !", "warning");  
                return false;
            }
            var src_kec = $("#src_kec");
            if (src_kec.val() == 0) {
                src_kec.focus();
                swal("Warning!", "Kecamatan Asal Harus Dipilih !", "warning");  
                return false;
            }
            var src_kel = $("#src_kel");
            if (src_kel.val() == 0) {
                src_kel.focus();
                swal("Warning!", "Kelurahan Asal Harus Dipilih !", "warning");  
                return false;
            }
            var src_alamat = $("#src_alamat");
            if (src_alamat.val().length == 0) {
                src_alamat.select();
                swal("Warning!", "Alamat Asal Tidak Boleh Kosong !", "warning");  
                return false;
            }
            var src_rw = $("#src_rw");
            if (src_rw.val().length == 0) {
                src_rw.select();
                swal("Warning!", "Rw Asal Tidak Boleh Kosong !", "warning");  
                return false;
            }
            var src_rt = $("#src_rt");
            if (src_rt.val().length == 0) {
                src_rt.select();
                swal("Warning!", "Rt Asal Tidak Boleh Kosong !", "warning");  
                return false;
            }

            var prop = $("#prop");
            if (prop.val() == 0) {
                prop.focus();
                swal("Warning!", "Provinsi Harus Dipilih !", "warning");  
                return false;
            }
            var kab = $("#kab");
            if (kab.val() == 0) {
                kab.focus();
                swal("Warning!", "Kabupaten Harus Dipilih !", "warning");  
                return false;
            }
            var kec = $("#kec");
            if (kec.val() == 0) {
                kec.focus();
                swal("Warning!", "Kecamatan Harus Dipilih !", "warning");  
                return false;
            }
            var kel = $("#kel");
            if (kel.val() == 0) {
                kel.focus();
                swal("Warning!", "Kelurahan Harus Dipilih !", "warning");  
                return false;
            }
            var alamat = $("#alamat");
            if (alamat.val().length == 0) {
                alamat.select();
                swal("Warning!", "Alamat Tidak Boleh Kosong !", "warning");  
                return false;
            }
            var no_rw = $("#no_rw");
            if (no_rw.val().length == 0) {
                no_rw.select();
                swal("Warning!", "Rw Tidak Boleh Kosong !", "warning");  
                return false;
            }
            var no_rt = $("#no_rt");
            if (no_rt.val().length == 0) {
                no_rt.select();
                swal("Warning!", "Rt Tidak Boleh Kosong !", "warning");  
                return false;
            }
            var imgupload1 = $("#imgupload1");
            if (imgupload1.val().length == 0) {
                imgupload1.focus();
                swal("Warning!", "Pas Foto Tidak Boleh Kosong !", "warning");  
                return false;
            }
            var imgupload2 = $("#imgupload2");
            if (imgupload2.val().length == 0) {
                imgupload2.focus();
                swal("Warning!", "Foto Ktp Tidak Boleh Kosong !", "warning");  
                return false;
            }
            var imgupload3 = $("#imgupload3");
            if (imgupload3.val().length == 0) {
                imgupload3.focus();
                swal("Warning!", "Foto KK Tidak Boleh Kosong !", "warning");  
                return false;
            }
            var imgupload4 = $("#imgupload4");
            if (imgupload4.val().length == 0) {
                imgupload4.focus();
                swal("Warning!", "Foto Pengantar RT/RW Tidak Boleh Kosong !", "warning");  
                return false;
            }
           return true
        }

        function validationmodaledit() {
            var nik = $("#nik");
            if (nik.val().length != 16) {
                nik.select();
                swal("Warning!", "NIK Tidak Boleh Kosong Atau Kurang Dari 16 Digit !", "warning");  
                return false;
            }
            var no_kk = $("#no_kk");
            if (no_kk.val().length != 16) {
                no_kk.select();
                swal("Warning!", "NOKK Tidak Boleh Kosong Atau Kurang Dari 16 Digit !", "warning");  
                return false;
            }
            var nama_lgkp = $("#nama_lgkp");
            if (nama_lgkp.val().length == 0) {
                nama_lgkp.select();
                swal("Warning!", "Nama Lengkap Tidak Boleh Kosong !", "warning");  
                return false;
            }
            var tmpt_lhr = $("#tmpt_lhr");
            if (tmpt_lhr.val().length == 0) {
                tmpt_lhr.select();
                swal("Warning!", "Tempat Lahir Tidak Boleh Kosong !", "warning");  
                return false;
            }
            var jum_anggota = $("#jum_anggota");
            if (jum_anggota.val().length == 0) {
                jum_anggota.select();
                swal("Warning!", "Jumlah Anggota Tidak Boleh Kosong !", "warning");  
                return false;
            }
            var jangka_waktu = $("#jangka_waktu");
            if (jangka_waktu.val().length == 0) {
                jangka_waktu.select();
                swal("Warning!", "Jangka Waktu Tidak Boleh Kosong !", "warning");  
                return false;
            }
            var tgl_lhr = $("#tgl_lhr");
            if (tgl_lhr.val().length == 0) {
                tgl_lhr.select();
                swal("Warning!", "Tanggal Lahir Tidak Boleh Kosong !", "warning");  
                return false;
            }
            var tgl_datang = $("#tgl_datang");
            if (tgl_datang.val().length == 0) {
                tgl_datang.select();
                swal("Warning!", "Tanggal Datang Tidak Boleh Kosong !", "warning");  
                return false;
            }
            var jenis_klmin = $("#jenis_klmin");
            if (jenis_klmin.val() == 0) {
                jenis_klmin.focus();
                swal("Warning!", "Jenis Kelamin Harus Dipilih !", "warning");  
                return false;
            }
            var agama = $("#agama");
            if (agama.val() == 0) {
                agama.focus();
                swal("Warning!", "Agama Harus Dipilih !", "warning");  
                return false;
            }
            var stat_hbkel = $("#stat_hbkel");
            if (stat_hbkel.val() == 0) {
                stat_hbkel.focus();
                swal("Warning!", "Hubungan Keluarga Harus Dipilih !", "warning");  
                return false;
            }
            var alasan_pindah = $("#alasan_pindah");
            if (alasan_pindah.val() == 0) {
                alasan_pindah.focus();
                swal("Warning!", "Alasan Pindah Harus Dipilih !", "warning");  
                return false;
            }
            var gol_drh = $("#gol_drh");
            if (gol_drh.val() == 0) {
                gol_drh.focus();
                swal("Warning!", "Golongan Darah Harus Dipilih !", "warning");  
                return false;
            }
            var pendidikan = $("#pendidikan");
            if (pendidikan.val() == 0) {
                pendidikan.focus();
                swal("Warning!", "Pendidikan Harus Dipilih !", "warning");  
                return false;
            }
            var pekerjaan = $("#pekerjaan");
            if (pekerjaan.val() == 0) {
                pekerjaan.focus();
                swal("Warning!", "Pekerjaan Harus Dipilih !", "warning");  
                return false;
            }
            var telepon = $("#telepon");
            if (telepon.val().length < 4) {
                telepon.select();
                swal("Warning!", "Telepon Tidak Boleh Kosong !", "warning");  
                return false;
            }
            var email = $("#email");
            if (email.val().length == 0) {
                email.select();
                swal("Warning!", "Email Tidak Boleh Kosong !", "warning");  
                return false;
            }
            if (!validateEmail(email.val())){
                 email.select();
                swal("Warning!", "Format Email Salah !", "warning");  
                return false;
            }

            var src_prov = $("#src_prov");
            if (src_prov.val() == 0) {
                src_prov.focus();
                swal("Warning!", "Provinsi Asal Harus Dipilih !", "warning");  
                return false;
            }
            var src_kab = $("#src_kab");
            if (src_kab.val() == 0) {
                src_kab.focus();
                swal("Warning!", "Kabupaten Asal Harus Dipilih !", "warning");  
                return false;
            }
            var src_kec = $("#src_kec");
            if (src_kec.val() == 0) {
                src_kec.focus();
                swal("Warning!", "Kecamatan Asal Harus Dipilih !", "warning");  
                return false;
            }
            var src_kel = $("#src_kel");
            if (src_kel.val() == 0) {
                src_kel.focus();
                swal("Warning!", "Kelurahan Asal Harus Dipilih !", "warning");  
                return false;
            }
            var src_alamat = $("#src_alamat");
            if (src_alamat.val().length == 0) {
                src_alamat.select();
                swal("Warning!", "Alamat Asal Tidak Boleh Kosong !", "warning");  
                return false;
            }
            var src_rw = $("#src_rw");
            if (src_rw.val().length == 0) {
                src_rw.select();
                swal("Warning!", "Rw Asal Tidak Boleh Kosong !", "warning");  
                return false;
            }
            var src_rt = $("#src_rt");
            if (src_rt.val().length == 0) {
                src_rt.select();
                swal("Warning!", "Rt Asal Tidak Boleh Kosong !", "warning");  
                return false;
            }

            var prop = $("#prop");
            if (prop.val() == 0) {
                prop.focus();
                swal("Warning!", "Provinsi Harus Dipilih !", "warning");  
                return false;
            }
            var kab = $("#kab");
            if (kab.val() == 0) {
                kab.focus();
                swal("Warning!", "Kabupaten Harus Dipilih !", "warning");  
                return false;
            }
            var kec = $("#kec");
            if (kec.val() == 0) {
                kec.focus();
                swal("Warning!", "Kecamatan Harus Dipilih !", "warning");  
                return false;
            }
            var kel = $("#kel");
            if (kel.val() == 0) {
                kel.focus();
                swal("Warning!", "Kelurahan Harus Dipilih !", "warning");  
                return false;
            }
            var alamat = $("#alamat");
            if (alamat.val().length == 0) {
                alamat.select();
                swal("Warning!", "Alamat Tidak Boleh Kosong !", "warning");  
                return false;
            }
            var no_rw = $("#no_rw");
            if (no_rw.val().length == 0) {
                no_rw.select();
                swal("Warning!", "Rw Tidak Boleh Kosong !", "warning");  
                return false;
            }
            var no_rt = $("#no_rt");
            if (no_rt.val().length == 0) {
                no_rt.select();
                swal("Warning!", "Rt Tidak Boleh Kosong !", "warning");  
                return false;
            }
           return true
        }
        function validationmodaltolak() {
            var id_pengajuan_tolak = $("#id_pengajuan_tolak");
            if (id_pengajuan_tolak.val().length == 0) {
                id_pengajuan_tolak.select();
                swal("Warning!", "ID Pengajuan Tidak Boleh Kosong !", "warning");  
                return false;
            }
            var isi_pesan = $("#isi_pesan");
            if (isi_pesan.val().length < 10) {
                isi_pesan.select();
                swal("Warning!", "Isi Pesan Tidak Boleh Kurang Dari 10 Karakter !", "warning");  
                return false;
            }
           return true
        }

    function edit(id_peng){
         
        var pengajuan = id_peng
        $('#pengajuan_modal').modal('show');
         $.ajax({
                    type: "post",
                    url:  BASE_URL+"ApiEpunten/get_skts_edit",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        id_peng : pengajuan,
                    },
                    beforeSend:
                    function () {
                         // block_screen();
                    },
                    success: function (data) {
                        console.log(data);
                        $('#modal-title').html("EDIT PENGAJUAN");
                        $("#submit_edit").show();
                        $('#submit_btn').hide();
                        $('#nik').val(data[0].nik);
                        $('#no_kk').val(data[0].no_kk);
                        $('#nama_lgkp').val(data[0].nama_lgkp);
                        $('#tmpt_lhr').val(data[0].tmpt_lhr);
                        $('#tgl_lhr').val(data[0].tgl_lhr);
                        $('select[name="jenis_klmin"]').val(data[0].jenis_klmin).trigger("change");
                        $('select[name="agama"]').val(data[0].agama).trigger("change");
                        $('select[name="stat_kwn"]').val(data[0].stat_kwn).trigger("change");
                        $('select[name="gol_drh"]').val(data[0].gol_drh).trigger("change");
                        $('select[name="pendidikan"]').val(data[0].pendidikan).trigger("change");
                        $('select[name="pekerjaan"]').val(data[0].pekerjaan).trigger("change");
                        $('select[name="stat_hbkel"]').val(data[0].stat_hbkel).trigger("change");
                        $('select[name="alasan_pindah"]').val(data[0].alasan_pindah).trigger("change");
                        $('#telepon').val(data[0].telepon);
                        $('#email').val(data[0].email);
                        $('#jum_anggota').val(data[0].jum_anggota);
                        $('#jangka_waktu').val(data[0].jangka_waktu);
                        $('#tgl_datang').val(data[0].tgl_datang);
                        $('#is_asal').show();
                        $('select[name="src_prov"]').empty();
                        $('select[name="src_prov"]').append('<option value="'+data[0].src_prov+'">'+data[0].src_prov+' - '+data[0].nama_src_prov+'</option>');
                        $('select[name="src_kab"]').empty();
                        $('select[name="src_kab"]').append('<option value="'+data[0].src_kab+'">'+data[0].src_kab+' - '+data[0].nama_src_kab+'</option>');
                        $('select[name="src_kec"]').empty();
                        $('select[name="src_kec"]').append('<option value="'+data[0].src_kec+'">'+data[0].src_kec+' - '+data[0].nama_src_kec+'</option>');
                        $('select[name="src_kel"]').empty();
                        $('select[name="src_kel"]').append('<option value="'+data[0].src_kel+'">'+data[0].src_kel+' - '+data[0].nama_src_kel+'</option>');
                        $('#src_alamat').val(data[0].src_alamat);
                        $('#src_rw').val(data[0].src_rw);
                        $('#src_rt').val(data[0].src_rt);
                        $('#is_tujuan').show();
                        $('#is_upload').show();
                        $('select[name="prop"]').empty();
                        $('select[name="prop"]').append('<option value="'+data[0].prop+'">'+data[0].prop+' - '+data[0].nama_prop+'</option>');
                        $('select[name="kab"]').empty();
                        $('select[name="kab"]').append('<option value="'+data[0].kab+'">'+data[0].kab+' - '+data[0].nama_kab+'</option>');
                        $('select[name="kec"]').empty();
                        $('select[name="kec"]').append('<option value="'+data[0].kec+'">'+data[0].kec+' - '+data[0].nama_kec+'</option>');
                        $('select[name="kel"]').empty();
                        $('select[name="kel"]').append('<option value="'+data[0].kel+'">'+data[0].kel+' - '+data[0].nama_kel+'</option>');
                        
                        $('#alamat').val(data[0].alamat);
                        $('#no_rw').val(data[0].no_rw);
                        $('#no_rt').val(data[0].no_rt);
                        $("#blockimgupload1").hide();
                        $("#blockimgupload2").hide();
                        $("#blockimgupload3").hide();
                        $("#blockimgupload4").hide();
                        $("#blockimgupload5").hide();
                        
                        
                        
                        
                        
                       
                        if (data[0].img_thumb_1 == null){
                          $("#blockimgdisplay1").hide();
                        }else{
                          $("#blockimgdisplay1").show();
                          $("#imgdisplay1").attr("src",BASE_URL+data[0].img_thumb_1);
                        }
                        if (data[0].img_thumb_2 == null){
                          $("#blockimgdisplay2").hide();
                        }else{
                          $("#blockimgdisplay2").show();
                          $("#imgdisplay2").attr("src",BASE_URL+data[0].img_thumb_2);
                        }
                        if (data[0].img_thumb_3 == null){
                          $("#blockimgdisplay3").hide();
                        }else{
                          $("#blockimgdisplay3").show();
                          $("#imgdisplay3").attr("src",BASE_URL+data[0].img_thumb_3);
                        }
                        if (data[0].img_thumb_4 == null){
                          $("#blockimgdisplay4").hide();
                        }else{
                          $("#blockimgdisplay4").show();
                          $("#imgdisplay4").attr("src",BASE_URL+data[0].img_thumb_4);
                        }
                        if (data[0].img_thumb_5 == null){
                          $("#blockimgdisplay5").hide();
                        }else{
                          $("#blockimgdisplay5").show();
                          $("#imgdisplay5").attr("src",BASE_URL+data[0].img_thumb_5);
                        }
                        
                    },
                    error:
                    function (data) {
                        // unblock_screen();
                        // swal("Error!", "Ooops!, Please Try Again, Something Went Wrong", "error"); 

                    },
                    complete:
                    function (response) {
                        // unblock_screen();  
                        // $("#no_kk_baru").val("");
                        
                    }
                });
    }
    function hapus(id_peng,nama_lgkp,nik){
      var pengajuan = id_peng;
      var nama_lgkp = toTitleCase(nama_lgkp.toLowerCase());
      var nik = nik;
         swal({   
            title: "Apakah Anda Yakin ?",   
            text: "Menghapus Pengajuan "+nama_lgkp+" ?",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#f33155",   
            confirmButtonText: "Ya, Hapus Pengajuan",   
            cancelButtonText: "Tidak, Jangan Dihapus",   
            closeOnConfirm: false,   
            closeOnCancel: false 
            }, function(isConfirm){   
                if (isConfirm) {
                $.ajax({
                    type: "post",
                    url:  BASE_URL+"ApiEpunten/get_skts_delete",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        id_peng : pengajuan,
                        nik : nik,
                    },
                    beforeSend:
                    function () {
                         block_screen();
                    },
                    success: function (data) {
                        console.log(data);
                         if (data.message_type > 0){
                              swal("Berhasil !", "Pengajuan "+nama_lgkp+" Telah Dihapus !", "success");
                        }else{
                            swal("Warning!", data.message, "warning");  
                        }
                    },
                    error:
                    function (data) {
                        unblock_screen();
                        swal("Error!", "Ooops!, Please Try Again, Something Went Wrong", "error"); 

                    },
                    complete:
                    function (response) {
                        unblock_screen();
                        get_table();
                    }
                });
                     
                } else {     
                    swal("Cancelled", "Pengajuan "+nama_lgkp+" Tidak Jadi Dihapus :)", "error");   
                } 
            });
       
    }

   function print_data(id_peng){
         
        var pengajuan = id_peng
         $.ajax({
                    type: "post",
                    url:  BASE_URL+"ApiEpunten/get_skts_edit",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        id_peng : pengajuan,
                    },
                    beforeSend:
                    function () {
                         // block_screen();
                    },
                    success: function (data) {
                        console.log(data);
                        PrintImage(data[0].img_thumb_1,data[0].img_thumb_2,data[0].img_thumb_3,data[0].img_thumb_4,data[0].img_thumb_5);
                       
                    },
                    error:
                    function (data) {
                        // unblock_screen();
                        // swal("Error!", "Ooops!, Please Try Again, Something Went Wrong", "error"); 

                    },
                    complete:
                    function (response) {
                        // unblock_screen();  
                        // $("#no_kk_baru").val("");
                        
                    }
                });
       }


        function ImagetoPrint(img_thumb_1,img_thumb_2,img_thumb_3,img_thumb_4,img_thumb_5)
        {
          var data = "<html><head><scri"+"pt>function step1(){\n" +
                    "setTimeout('step2()', 10);}\n" +
                    "function step2(){window.print();window.close()}\n" +
                    "</scri" + "pt></head><body onload='step1()'>\n" ;
                    if(img_thumb_1 != null){
                     data +="<img src='" + BASE_URL+""+img_thumb_1 + "' />\n";
                    }
                    if(img_thumb_2 != null){
                      data += "<img src='" + BASE_URL+""+img_thumb_2 + "' />\n";  
                    }
                    if(img_thumb_3 != null){
                      data += "<img src='" + BASE_URL+""+img_thumb_3 + "' />\n";  
                    }
                    if(img_thumb_4 != null){
                      data += "<img src='" + BASE_URL+""+img_thumb_4 + "' />\n";  
                    }
                    if(img_thumb_5 != null){
                      data += "<img src='" + BASE_URL+""+img_thumb_5 + "' />\n";  
                    }
                     data += "</body></html>";
            return data;
        }

        function PrintImage(img_thumb_1,img_thumb_2,img_thumb_3,img_thumb_4,img_thumb_5)
        {
            var Pagelink = "about:blank";
            var pwa = window.open(Pagelink, "_new");
            pwa.document.open();
            pwa.document.write(ImagetoPrint(img_thumb_1,img_thumb_2,img_thumb_3,img_thumb_4,img_thumb_5));
            pwa.document.close();
        }

      function pesan(id_peng,nik){
        var pengajuan = id_peng;
        var nik_tolak = nik;
        $('#tolak_modal').modal('show');
        $('#id_pengajuan_tolak').val(pengajuan);
        $('#nik_tolak').val(nik_tolak);
        $('#isi_pesan').val("");
        $('#cntnum').html("Minimal Anda Harus Mengisi 10 Karakter Untuk Mengirim Pesan, Anda Baru Memasukan 0 Karakter !");
        $('#cntnum').css("color","red");
      }

      function proses(id_peng,nama_lgkp,nik){
        var pengajuan = id_peng;
        var nik = nik;
        var nama_lgkp = toTitleCase(nama_lgkp.toLowerCase());
        swal({   
            title: "Apakah Anda Yakin ?",   
            text: "Mempublish Pengajuan "+nama_lgkp+" ?",   
            type: "info",   
            showCancelButton: true,   
            confirmButtonColor: "#7ace4c",   
            confirmButtonText: "Ya, Publish Pengajuan",   
            cancelButtonText: "Tidak, Jangan Dipublish",   
            closeOnConfirm: false,   
            closeOnCancel: false 
            }, function(isConfirm){   
                if (isConfirm) {
                $.ajax({
                    type: "post",
                    url:  BASE_URL+"ApiEpunten/get_skts_acc",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        id_peng : pengajuan,
                        nik : nik,
                    },
                    beforeSend:
                    function () {
                         block_screen();
                    },
                    success: function (data) {
                        console.log(data);
                         if (data.message_type > 0){
                              swal("Berhasil !", "Pengajuan "+nama_lgkp+" Telah Dipublish !", "success");
                        }else{
                            swal("Warning!", data.message, "warning");  
                        }
                    },
                    error:
                    function (data) {
                        unblock_screen();
                        swal("Error!", "Ooops!, Please Try Again, Something Went Wrong", "error"); 

                    },
                    complete:
                    function (response) {
                        unblock_screen();
                        get_table();
                    }
                });
                     
                } else {     
                    swal("Cancelled", "Pengajuan "+nama_lgkp+" Tidak Jadi Dipublish :)", "error");   
                } 
            });
      }

      
      function beres(id_peng,nama_lgkp,nik){
        var pengajuan = id_peng;
        var nik = nik;
        var nama_lgkp = toTitleCase(nama_lgkp.toLowerCase());
        swal({   
            title: "Apakah Anda Yakin ?",   
            text: "Mempublish Pengajuan "+nama_lgkp+" ?",   
            type: "info",   
            showCancelButton: true,   
            confirmButtonColor: "#7ace4c",   
            confirmButtonText: "Ya, Publish Pengajuan",   
            cancelButtonText: "Tidak, Jangan Dipublish",   
            closeOnConfirm: false,   
            closeOnCancel: false 
            }, function(isConfirm){   
                if (isConfirm) {
                $.ajax({
                    type: "post",
                    url:  BASE_URL+"ApiEpunten/get_skts_beres",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        id_peng : pengajuan,
                        nik : nik,
                    },
                    beforeSend:
                    function () {
                         block_screen();
                    },
                    success: function (data) {
                        console.log(data);
                         if (data.message_type > 0){
                              swal("Berhasil !", "Pengajuan "+nama_lgkp+" Telah Dipublish !", "success");
                        }else{
                            swal("Warning!", data.message, "warning");  
                        }
                    },
                    error:
                    function (data) {
                        unblock_screen();
                        swal("Error!", "Ooops!, Please Try Again, Something Went Wrong", "error"); 

                    },
                    complete:
                    function (response) {
                        unblock_screen();
                        get_table();
                    }
                });
                     
                } else {     
                    swal("Cancelled", "Pengajuan "+nama_lgkp+" Tidak Jadi Dipublish :)", "error");   
                } 
            });
      }

      function cntText(){
        var text = $('#isi_pesan');
        var angka = text.val().length;
        var textangka = "Minimal Anda Harus Mengisi 10 Karakter Untuk Mengirim Pesan, Anda Baru Memasukan "+text.val().length+" Karakter !";
        var txtdata = "Anda Telah Memasukan "+text.val().length+" Karakter !";
        if(angka >= 10){
          $('#cntnum').html(txtdata);
          $('#cntnum').css("color","black");
        }else if(angka < 10){
          $('#cntnum').html(textangka);
          $('#cntnum').css("color","red");
        }
      }
      function toTitleCase(str) {
        return str.replace(/(?:^|\s)\w/g, function(match) {
            return match.toUpperCase();
        });
    }
    </script>