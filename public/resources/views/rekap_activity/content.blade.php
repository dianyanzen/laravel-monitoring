<div id="page-wrapper">
            <div class="container-fluid">
               <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $mtitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                
                        <ol class="breadcrumb">
                            <li><a href="{{ url('/') }}/">Report</a></li>
                            <li class="active"><?php echo $mtitle; ?></li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- .row -->
                <div class="row">
                <form >
                    <div class="col-sm-12">
                        <div class="white-box">
                            <div class="row">
                                <div class="col-lg-12">         
                                    <div class="form-group">
                                        <h3 class="box-title m-b-0"><b>Tanggal Daily</b></h3>
                                        <input class="form-control input-daterange-datepicker" type="text" id="tanggal" name="tanggal"/> </div>
                                    
                                </div>
                            </div>
                            <div class="row">
                                  <button type="button" class="btn btn-info waves-effect waves-light m-r-10 pull-right" id="btn-reset" onclick="on_back();">Back <i class="mdi mdi-arrow-left-box fa-fw"></i></button>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>

                <div class="row">
                <div class="col-md-12">
                        <div class="white-box">
                            <h3 class="box-title"><?php echo $mtitle; ?></h3>
                            <div class="scrollable">
                                <div class="table-responsive">
                                    <table id="daily-list" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                   <thead>
                                    
                                        <tr>
                                            <th width="5%" style="text-align: center;">No</th>
                                            <th width="15%" style="text-align: center;">User Id</th>
                                            <th width="15%" style="text-align: center;">Tanggal</th>
                                            <th width="15%" style="text-align: center;">Aktivitas</th>
                                            <th width="15%" style="text-align: center;">Description</th>
                                            <th width="15%" style="text-align: center;">Jam</th>
                                            <th width="10%" style="text-align: center;">Jumlah</th>
                                            <th width="10%" style="text-align: center;">Menit Aktivitas</th>
                                            <th width="10%" style="text-align: center;">Action</th>

                                        </tr>
                                       
                                    </thead>
                                    <tbody id="show_data">
                                       
                                        
                                    </tbody>
                                    
                                </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
       @include('shared.footer_detail')