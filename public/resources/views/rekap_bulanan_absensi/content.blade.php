<div id="page-wrapper">
            <div class="container-fluid">
               <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $mtitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                
                        <ol class="breadcrumb">
                            <li><a href="{{ url('/') }}/">Report</a></li>
                            <li class="active"><?php echo $mtitle; ?></li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- .row -->
                <div class="row">
                <form >
                    <div class="col-sm-12">
                        <div class="white-box">
                            <div class="row">
                                <div class="col-lg-12">         
                                    <div class="form-group">
                                        <h3 class="box-title m-b-0"><b>Tanggal Absensi</b></h3>
                                        <input type="text" class="form-control" id="tanggal" value="<?php echo date('m-Y');?>" placeholder="MM-YYYY" readonly>
                                    
                                </div>
                            </div>
                            <div class="row">
                                  <button type="button" class="btn btn-info waves-effect waves-light m-r-10 pull-right" id="btn-reset" onclick="on_back();">Back <i class="mdi mdi-arrow-left-box fa-fw"></i></button>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>

                <div class="row">
                <div class="col-md-12">
                        <div class="white-box">
                            <h3 class="box-title"><?php echo $mtitle; ?></h3>
                            <div class="scrollable">
                                <div class="table-responsive">
                                    <table id="absensi-list" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                   <thead>
                                    
                                        <tr>
                                            <th width="5%" style="text-align: center;">No</th>
                                            <th width="15%" style="text-align: center;">User Id</th>
                                            <th width="15%" style="text-align: center;">Nama</th>
                                            <th width="15%" style="text-align: center;">Bulan</th>
                                            <th width="10%" style="text-align: center;">Tanggal 1</th>
                                            <th width="10%" style="text-align: center;">Tanggal 2</th>
                                            <th width="10%" style="text-align: center;">Tanggal 3</th>
                                            <th width="10%" style="text-align: center;">Tanggal 4</th>
                                            <th width="10%" style="text-align: center;">Tanggal 5</th>
                                            <th width="10%" style="text-align: center;">Tanggal 6</th>
                                            <th width="10%" style="text-align: center;">Tanggal 7</th>
                                            <th width="10%" style="text-align: center;">Tanggal 8</th>
                                            <th width="10%" style="text-align: center;">Tanggal 9</th>
                                            <th width="10%" style="text-align: center;">Tanggal 10</th>
                                            <th width="10%" style="text-align: center;">Tanggal 11</th>
                                            <th width="10%" style="text-align: center;">Tanggal 12</th>
                                            <th width="10%" style="text-align: center;">Tanggal 13</th>
                                            <th width="10%" style="text-align: center;">Tanggal 14</th>
                                            <th width="10%" style="text-align: center;">Tanggal 15</th>
                                            <th width="10%" style="text-align: center;">Tanggal 16</th>
                                            <th width="10%" style="text-align: center;">Tanggal 17</th>
                                            <th width="10%" style="text-align: center;">Tanggal 18</th>
                                            <th width="10%" style="text-align: center;">Tanggal 19</th>
                                            <th width="10%" style="text-align: center;">Tanggal 20</th>
                                            <th width="10%" style="text-align: center;">Tanggal 21</th>
                                            <th width="10%" style="text-align: center;">Tanggal 22</th>
                                            <th width="10%" style="text-align: center;">Tanggal 23</th>
                                            <th width="10%" style="text-align: center;">Tanggal 24</th>
                                            <th width="10%" style="text-align: center;">Tanggal 25</th>
                                            <th width="10%" style="text-align: center;">Tanggal 26</th>
                                            <th width="10%" style="text-align: center;">Tanggal 27</th>
                                            <th width="10%" style="text-align: center;">Tanggal 28</th>
                                            <th width="10%" style="text-align: center;">Tanggal 29</th>
                                            <th width="10%" style="text-align: center;">Tanggal 30</th>
                                            <th width="10%" style="text-align: center;">Tanggal 31</th>
                                            <th width="10%" style="text-align: center;">Kehadiran</th>
                                            <th width="10%" style="text-align: center;">Tepat Waktu</th>
                                            <th width="10%" style="text-align: center;">Terlambat</th>

                                        </tr>
                                       
                                    </thead>
                                    <tbody id="show_data">
                                       
                                        
                                    </tbody>
                                    
                                </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
       @include('shared.footer_detail')