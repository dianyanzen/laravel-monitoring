<!DOCTYPE html>
<html lang="en">
@include('shared.head')
<body class="fix-header">
    <!-- ============================================================== -->
    <!-- Preloader -->
    <!-- ============================================================== -->
    <!-- <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
        </svg>
    </div> -->
    <!-- ============================================================== -->
    <!-- Wrapper -->
    <!-- ============================================================== -->
    <div id="wrapper">
            @include('shared.nav_top')
            @include('shared/nav_side')
            @include('rekap_bulanan_absensi/content')
    </div>
    @include('shared/footer')
    @include('rekap_bulanan_absensi/javascript')
</body>
</html>