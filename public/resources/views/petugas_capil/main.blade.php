<!DOCTYPE html>
<html lang="en">
@include('shared.head')
<body class="fix-header">
    <div id="wrapper">
            @include('shared.nav_top')
            @include('shared/nav_side')
            @include('petugas_capil/content')
    </div>
     
    @include('shared/footer')
    
    @include('petugas_capil/javascript')
</body>
</html>