<!DOCTYPE html>
<html lang="en">
@include('shared.head')
<body class="fix-header">
    <div id="wrapper">
            @include('shared.nav_top')
            @include('shared/nav_side')
            @include('master_salaman/content')
    </div>
     
    @include('shared/footer')
    
    @include('master_salaman/javascript')
</body>
</html>