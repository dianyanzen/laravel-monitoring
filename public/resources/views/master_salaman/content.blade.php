<div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $stitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                        <ol class="breadcrumb">
                           <li class="text-info"><?php echo $stitle; ?></li>
                        </ol>
                    </div>
                </div>
                <!-- .row -->
               <div class="row">
                <div class="row">
                    <?php   if (!empty($data)){
                            foreach($data as $row){?>
                    <div class="col-md-6 col-xs-12">
                        <div class="white-box">
                            <div class="user-bg"> <img width="100%" alt="user" src="{{ url('/') }}/assets/plugins/images/wallpaper2.jpg">
                                <div class="overlay-box">
                                    <div class="user-content">
                                        <a href="javascript:void(0)"> 
                                            @if ($row->layanan == 'AKTA KELAHIRAN')
                                            <img src="{{ url('/') }}/assets/plugins/images/Disduk/akta_kelahiran.png" class=" thumb-lg" alt="user-img">
                                            @elseif ($row->layanan == 'AKTA KEMATIAN')
                                            <img src="{{ url('/') }}/assets/plugins/images/Disduk/akta_kematian.png" class=" thumb-lg" alt="user-img">
                                            @elseif ($row->layanan == 'KARTU IDENTITAS ANAK')
                                            <img src="{{ url('/') }}/assets/plugins/images/Disduk/kia.png" class=" thumb-lg" alt="user-img">
                                            @elseif ($row->layanan == 'PINDAH KELUAR')
                                            <img src="{{ url('/') }}/assets/plugins/images/Disduk/pindah.png" class=" thumb-lg" alt="user-img">
                                            @elseif ($row->layanan == 'PINDAH DATANG')
                                            <img src="{{ url('/') }}/assets/plugins/images/Disduk/datang.png" class=" thumb-lg" alt="user-img">
                                            @elseif ($row->layanan == 'CETAK KTP-EL')
                                            <img src="{{ url('/') }}/assets/plugins/images/Disduk/ktp.png" class=" thumb-lg" alt="user-img">
                                            @elseif ($row->layanan == 'PERBAIKAN DATA')
                                            <img src="{{ url('/') }}/assets/plugins/images/Disduk/perbaikan.png" class=" thumb-lg" alt="user-img">
                                            @elseif ($row->layanan == 'INPUT BIODATA')
                                            <img src="{{ url('/') }}/assets/plugins/images/Disduk/input.png" class=" thumb-lg" alt="user-img">
                                            @elseif ($row->layanan == 'KARTU KELUARGA')
                                            <img src="{{ url('/') }}/assets/plugins/images/Disduk/kk.png" class=" thumb-lg" alt="user-img">
                                            @else
                                            <img src="{{ url('/') }}/assets/plugins/images/pemkot.png" class=" thumb-lg" alt="user-img">
                                            @endif
                                        </a>
                                        <h4 class="text-white"><?php echo $row->layanan; ?></h4>
                                    </div>
                                </div>
                            </div>
                            <div class="user-btm-box">
                                <div class="col-md-4 col-sm-4 text-center">
                                    <p class="text-info"><b>Pengajuan Baru<br>Hari Ini</b></p>
                                    <h1><?php echo $row->baru_tdy; ?></h1> 
                                </div>
                                <div class="col-md-4 col-sm-4 text-center">
                                    <p class="text-info"><b>Upload Ulang<br>Hari Ini</b></p>
                                    <h1><?php echo $row->upload_ulang_tdy; ?></h1> 
                                </div>
                                <div class="col-md-4 col-sm-4 text-center">
                                    <p class="text-info"><b>Berkas Tidak Lengkap<br>Hari Ini</b></p>
                                    <h1><?php echo $row->btl_tdy; ?></h1> 
                                </div>
                                <div class="col-md-4 col-sm-4 text-center">
                                    <p class="text-info"><b>Pengajuan Baru<br>Semua</b></p>
                                    <h1><?php echo $row->baru; ?></h1> 
                                </div>
                                <div class="col-md-4 col-sm-4 text-center">
                                    <p class="text-info"><b>Upload Ulang<br>Semua</b></p>
                                    <h1><?php echo $row->upload_ulang; ?></h1> 
                                </div>
                                <div class="col-md-4 col-sm-4 text-center">
                                    <p class="text-info"><b>Berkas Tidak Lengkap<br>Semua</b></p>
                                    <h1><?php echo $row->btl; ?></h1> 
                                </div>
                                <div class="col-md-6 col-sm-6 text-center">
                                    <p class="text-info"><b>Pengajuan<br>Terverifikasi</b></p>
                                    <h1><?php echo $row->terverifikasi; ?></h1> 
                                </div>
                                <div class="col-md-6 col-sm-6 text-center">
                                    <p class="text-info"><b>Pengajuan<br>Terpublish</b></p>
                                    <h1><?php echo $row->terpublish; ?></h1> 
                                </div>
                            </div>
                          
                        </div>
                    </div>
                    <?php } ?>
                    <?php } ?>
                    
                </div>
                   
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- /.container-fluid -->
         @include('shared.footer_detail')