 <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $stitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                        <ol class="breadcrumb">
                            <li class="active"><?php echo $mtitle; ?></li>
                        </ol>
                    </div>
                </div>
                 <div class="row">
                <form name ="get_form" action="<?php echo $my_url; ?>" method="post">
                    @csrf
                    <div class="col-sm-12">
                        <div class="white-box">
                            <div class="row">
                                <div class="col-lg-3">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_tgl" type="checkbox" checked disabled="true">
                                            <label for="cb_tgl"> Tanggal Request</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3">         
                                    <div class="form-group">
                                        <input class="form-control input-daterange-datepicker" type="text" id="tanggal" name="tanggal" /></div>
                                </div>
                                <div class="col-lg-3">         
                                    <div class="form-group">
                                        <!-- <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_nik" type="checkbox">
                                            <label for="cb_nik"> Nik</label>
                                        </div> -->
                                    </div>
                                </div>
                                <div class="col-lg-3">         
                                    <!-- <div class="form-group">
                                        <input class="form-control" type="text" id="nik" name="nik" disabled="true" onkeypress="return isNumberKey(event)" onchange="onlyNum()" /></div> -->
                                </div>
                            </div>
                            <div class="row">
                                <button type="submit" class="btn btn-info waves-effect waves-light m-r-10 pull-right" id="btn-filter" onclick="on_serach();" >Search <i class="mdi  mdi-magnify fa-fw"></i></button>
                                  <button type="button" class="btn btn-invert waves-effect waves-light m-r-10 pull-right" id="btn-reset" onclick="on_clear();">Clear <i class="mdi  mdi-delete fa-fw"></i></button>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
                <div class="row">
                <div class="col-md-12">
                        <div class="white-box">
                            <h3 class="box-title"><?php echo $mtitle; ?></h3>
                            <div class="scrollable">
                                <div class="table-responsive">
                                    <table id="mytable" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                    <thead>
                                     
                                        <tr>
                                            <th width="10%" style="text-align: center;">No Kec</th>
                                            <th width="15%" style="text-align: center;">Nik</th>
                                            <th width="20%" style="text-align: center;">Nama Lengkap</th>
                                            <th width="15%" style="text-align: center;">Status E-KTP</th>
                                            <th width="10%" style="text-align: center;">Tanggal Lahir</th>
                                            <th width="10%" style="text-align: center;">Kecamatan</th>
                                            <th width="10%" style="text-align: center;">Kelurahan</th>
                                            <th width="10%" style="text-align: center;">Alamat</th>
                                            <th width="10%" style="text-align: center;">Kode Pos</th>
                                            <th width="10%" style="text-align: center;">No Rw</th>
                                            <th width="10%" style="text-align: center;">No Rt</th>
                                            <th width="10%" style="text-align: center;">Tanggal Rekam</th>
                                            <th width="10%" style="text-align: center;">Petugas Rekam</th>
                                            <th width="10%" style="text-align: center;">Tanggal Cetak</th>
                                            <th width="10%" style="text-align: center;">Petugas Cetak</th>
                                        </tr>
                                       
                                    </thead>
                                    <tbody id="show_data">
                                       <?php
                                        if (!empty($data)){
                                            $i = 0;
                                           foreach($data as $row){
                                            $i ++;
                                            ?>
                                             <tr>
                                                <td width="10%" style="text-align: center;"><?php echo $row->no_kec ;?></td>
                                                <td width="15%" style="text-align: center;"><span style="color: #fff">,</span><?php echo $row->nik ;?></td>
                                                <td width="20%" style="text-align: center;"><?php echo $row->nama_lgkp ;?></td>
                                                <td width="15%" style="text-align: center;"><?php echo $row->current_status_code ;?></td>
                                                <td width="10%" style="text-align: center;"><?php echo $row->tgl_lhr ;?></td>
                                                <td width="10%" style="text-align: center;"><?php echo $row->nama_kec ;?></td>
                                                <td width="10%" style="text-align: center;"><?php echo $row->nama_kel ;?></td>
                                                <td width="10%" style="text-align: center;"><?php echo $row->alamat ;?></td>
                                                <td width="10%" style="text-align: center;"><?php echo $row->kode_pos ;?></td>
                                                <td width="10%" style="text-align: center;"><?php echo $row->no_rw ;?></td>
                                                <td width="10%" style="text-align: center;"><?php echo $row->no_rt ;?></td>
                                                <td width="10%" style="text-align: center;"><?php echo $row->tgl_rekam ;?></td>
                                                <td width="10%" style="text-align: center;"><?php echo $row->petugas_rekam ;?></td>
                                                <td width="10%" style="text-align: center;"><?php echo $row->ktp_dt ;?></td>
                                                <td width="10%" style="text-align: center;"><?php echo $row->ktp_by ;?></td>
                                            </tr>
                                          <?php } ?>
                                          <?php } ?>
                                       
                                        
                                    </tbody>
                                    
                                        <tfoot id="my_foot">
                                            <tr>
                                            <?php if (!empty($data)){ ?>
                                            <th width="80%" colspan="14" style="text-align: center;">Jumlah</th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($i),0,',','.');?></th>
                                            <?php } ?>
                                        </tr>

                                            </tfoot>
                                </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                 </div>
       @include('shared.footer_detail')