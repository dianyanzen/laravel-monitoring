 <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <h4 class="page-title"><?php echo $stitle; ?></h4> </div>
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                        <ol class="breadcrumb">
                            <li class="active"><?php echo $mtitle; ?></li>
                        </ol>
                    </div>
                </div>
                 <div class="row">
                <form name ="get_form" action="<?php echo $my_url; ?>" method="post">
                    @csrf
                    <div class="col-sm-12">
                        <div class="white-box">
                            <div class="row">
                                <div class="col-lg-3">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_tgl" type="checkbox" checked disabled="true">
                                            <label for="cb_tgl"> Tanggal Penerbitan</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3">         
                                    <div class="form-group">
                                        <input class="form-control input-daterange-datepicker" type="text" id="tanggal" name="tanggal" /></div>
                                </div>
                                <div class="col-lg-3">         
                                    <div class="form-group">
                                        <!-- <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_nik" type="checkbox">
                                            <label for="cb_nik"> Nik</label>
                                        </div> -->
                                    </div>
                                </div>
                                <div class="col-lg-3">         
                                    <!-- <div class="form-group">
                                        <input class="form-control" type="text" id="nik" name="nik" disabled="true" onkeypress="return isNumberKey(event)" onchange="onlyNum()" /></div> -->
                                </div>
                            </div>
                            <div class="row">
                                <button type="submit" class="btn btn-info waves-effect waves-light m-r-10 pull-right" id="btn-filter" onclick="on_serach();" >Search <i class="mdi  mdi-magnify fa-fw"></i></button>
                                  <button type="button" class="btn btn-invert waves-effect waves-light m-r-10 pull-right" id="btn-reset" onclick="on_clear();">Clear <i class="mdi  mdi-delete fa-fw"></i></button>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
                <div class="row">
                <div class="col-md-12">
                        <div class="white-box">
                            <h3 class="box-title"><?php echo $mtitle; ?></h3>
                            <div class="scrollable">
                                <div class="table-responsive">
                                    <table id="mytable" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                    <thead>
                                     
                                        <tr>
                                             <th width="10%" style="text-align: center;">No</th>
                                            <th width="30%" style="text-align: center;">Petugas</th>
                                            <th width="30%" style="text-align: center;">Tanggal</th>
                                            <th width="15%" style="text-align: center;">Pengajuan TTE</th>
                                            <th width="15%" style="text-align: center;">Sudah Tanda Tangan</th>
                                            <th width="15%" style="text-align: center;">Belum Tanda Tangan</th>
                                            <th width="15%" style="text-align: center;">Proses Penerbitan TTE</th>
                                            <th width="15%" style="text-align: center;">TTE Belum Masuk BSRE</th>
                                            <th width="15%" style="text-align: center;">TTE Belum Verivikasi</th>
                                            <th width="15%" style="text-align: center;">TTE Sudah Masuk BSRE</th>
                                        </tr>
                                       
                                    </thead>
                                    <tbody id="show_data">
                                        <?php
                                        if (!empty($data)){
                                            $i = 0;
                                            $jumlah_request_n = 0;
                                            $jumlah_aproved_n = 0;
                                            $jumlah_the_rest_n = 0;
                                            $jumlah_aproved_orange = 0;
                                            $jumlah_aproved_blue = 0;
                                            $jumlah_aproved_red = 0;
                                            $jumlah_aproved_green = 0;
                                           foreach($data as $row){
                                            $i++;
                                            $jumlah_request_n = $jumlah_request_n + $row->request_n;
                                            $jumlah_aproved_n = $jumlah_aproved_n + $row->aproved_n;
                                            $jumlah_the_rest_n = $jumlah_the_rest_n + $row->the_rest_n;
                                            $jumlah_aproved_orange = $jumlah_aproved_orange + $row->aproved_orange;
                                            $jumlah_aproved_blue = $jumlah_aproved_blue + $row->aproved_blue;
                                            $jumlah_aproved_red = $jumlah_aproved_red + $row->aproved_red;
                                            $jumlah_aproved_green = $jumlah_aproved_green + $row->aproved_green;
                                            ?>
                                             <tr>
                                                <td width="10%" style="text-align: center;"><?php echo $i ;?></td>
                                                <td width="30%" style="text-align: left;"><?php echo $row->nama_petugas ;?></td>
                                                <td width="30%" style="text-align: center;"><?php echo $row->tgl ;?></td>
                                                <td width="15%" style="text-align: right;"><?php echo $row->request_n ;?></td>
                                                <td width="15%" style="text-align: right;"><?php echo $row->aproved_n ;?></td>
                                                <td width="15%" style="text-align: right;"><?php echo $row->the_rest_n ;?></td>
                                                <td width="15%" style="text-align: right;"><?php echo $row->aproved_orange ;?></td>
                                                <td width="15%" style="text-align: right;"><?php echo $row->aproved_blue ;?></td>
                                                <td width="15%" style="text-align: right;"><?php echo $row->aproved_red ;?></td>
                                                <td width="15%" style="text-align: right;"><?php echo $row->aproved_green ;?></td>
                                            </tr>
                                          <?php } ?>
                                          <?php } ?>
                                    </tbody>
                                     <tfoot id="my_foot">
                                            <tr>
                                            <?php if (!empty($jumlah)){ ?>
                                            <th width="70%" colspan="3" style="text-align: center;">Jumlah</th>
                                            <th width="15%"  style="text-align: right;"><?php echo number_format(htmlentities($jumlah_request_n),0,',','.');?></th>
                                            <th width="15%"  style="text-align: right;"><?php echo number_format(htmlentities($jumlah_aproved_n),0,',','.');?></th>
                                            <th width="15%"  style="text-align: right;"><?php echo number_format(htmlentities($jumlah_the_rest_n),0,',','.');?></th>
                                            <th width="15%"  style="text-align: right;"><?php echo number_format(htmlentities($jumlah_aproved_orange),0,',','.');?></th>
                                            <th width="15%"  style="text-align: right;"><?php echo number_format(htmlentities($jumlah_aproved_blue),0,',','.');?></th>
                                            <th width="15%"  style="text-align: right;"><?php echo number_format(htmlentities($jumlah_aproved_red),0,',','.');?></th>
                                            <th width="15%"  style="text-align: right;"><?php echo number_format(htmlentities($jumlah_aproved_green),0,',','.');?></th>
                                            <?php } ?>
                                        </tr>

                                            </tfoot>
                                </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                 </div>
       @include('shared.footer_detail')