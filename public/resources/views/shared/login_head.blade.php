<!DOCTYPE html>  
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="icon" type="image/png" sizes="16x16" href="{{ url('/') }}/assets/plugins/images/pemkot.png">
<title>Sistem Monitoring Pelayanan Disduk Capil Kota Bandung - Dinas Kependudukan Dan Pencatatan Sipil Kota Bandung</title>
<link href="{{ url('/') }}/assets/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<link href="{{ url('/') }}/assets/css/animate.css" rel="stylesheet">
<link href="{{ url('/') }}/assets/css/style.css" rel="stylesheet">
<link href="{{ url('/') }}/assets/css/colors/blue.css" id="theme"  rel="stylesheet">
<link href="{{ url('/') }}/assets/css/toastr.css" rel="stylesheet">
</head>