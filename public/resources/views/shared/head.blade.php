
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ url('/') }}/assets/plugins/images/pemkot.png">
    <title>{{ (!empty($stabletitle)) ? $stabletitle : $stitle }}</title>
    <link href="{{ url('/') }}/assets/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/css/animate.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/plugins/bower_components/morrisjs/morris.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/plugins/bower_components/css-chart/css-chart.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/plugins/bower_components/chartist-js/dist/chartist.min.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/plugins/bower_components/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/plugins/bower_components/owl.carousel/owl.carousel.min.css" rel="stylesheet" type="text/css" />
    <link href="{{ url('/') }}/assets/plugins/bower_components/owl.carousel/owl.theme.default.css" rel="stylesheet" type="text/css" />
    <link href="{{ url('/') }}/assets/plugins/bower_components/Minimal-Gauge-chart/css/cmGauge.css" rel="stylesheet" type="text/css" />
    <link href="{{ url('/') }}/assets/plugins/bower_components/calendar/dist/fullcalendar.css" rel="stylesheet" />
    <link href="{{ url('/') }}/assets/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
    <link href="{{ url('/') }}/assets/plugins/bower_components/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/plugins/bower_components/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
    <link href="{{ url('/') }}/assets/datatable/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
    <link href="{{ url('/') }}/assets/css/toastr.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/plugins/bower_components/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
    <link href="{{ url('/') }}/assets/plugins/bower_components/custom-select/custom-select.css" rel="stylesheet" type="text/css" />
    <link href="{{ url('/') }}/assets/plugins/bower_components/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" />
    <link href="{{ url('/') }}/assets/plugins/bower_components/multiselect/css/multi-select.css" rel="stylesheet" type="text/css" />
    <link href="{{ url('/') }}/assets/plugins/bower_components/switchery/dist/switchery.min.css" rel="stylesheet" />
    <link href="{{ url('/') }}/assets/plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />
    <link href="{{ url('/') }}/assets/plugins/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />
    <link href="{{ url('/') }}/assets/plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/plugins/bower_components/jquery-asColorPicker-master/css/asColorPicker.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
    <link href="{{ url('/') }}/assets/plugins/bower_components/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/plugins/bower_components/jquery-asColorPicker-master/css/asColorPicker.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
    <link href="{{ url('/') }}/assets/plugins/bower_components/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/plugins/bower_components/Magnific-Popup-master/dist/magnific-popup.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/plugins/bower_components/fancybox/ekko-lightbox.min.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/plugins/bower_components/gallery/css/animated-masonry-gallery.css" rel="stylesheet">
    <!-- <link href="{{ url('/') }}/assets/plugins/bower_components/viewerjs-master/dist/viewer.css" rel="stylesheet"> -->
    <link href="{{ url('/') }}/assets/plugins/bower_components/dropify/dist/css/dropify.min.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/css/animate.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/css/style.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/css/colors/default.css" id="theme" rel="stylesheet">
    <link rel="stylesheet" href="{{ url('/') }}/assets/plugins/bower_components/viewerjs-master/dist/jquery.magnify.css">
    <style type="text/css">
      
      td.details-control {
          background: url('{{ url('/') }}/assets/plugins/images/add.png') no-repeat center center;
          background-size: 40% 50%;
          cursor: pointer;
      }
      tr.shown td.details-control {
          background: url('{{ url('/') }}/assets/plugins/images/delete-sign.png') no-repeat center center;
          background-size: 40% 50%;
      }

      @font-face {
          font-family: 'footable';
          src: url('{{ url('/') }}/assets/fonts/footable.eot');
          src: url('{{ url('/') }}/assets/fonts/footable.eot?#iefix') format('embedded-opentype'), url('{{ url('/') }}/assets/fonts/footable.woff') format('woff'), url('{{ url('/') }}/assets/fonts/footable.ttf') format('truetype'), url('{{ url('/') }}/assets/fonts/footable.svg#footable') format('svg');
          font-weight: normal;
          font-style: normal;
        }
        @media screen and (-webkit-min-device-pixel-ratio: 0) {
          @font-face {
            font-family: 'footable';
            src: url('{{ url('/') }}/assets/fonts/footable.svg#footable') format('svg');
            font-weight: normal;
            font-style: normal;
          }
        }
        .footable {
          width: 100%;
          /** SORTING **/

          /** PAGINATION **/

        }
        .footable.breakpoint > tbody > tr.footable-detail-show > td {
          border-bottom: none;
        }
        .footable.breakpoint > tbody > tr.footable-detail-show > td > span.footable-toggle:before {
          content: "\e001";
        }
        .footable.breakpoint > tbody > tr:hover:not(.footable-row-detail) {
          cursor: pointer;
        }
        .footable.breakpoint > tbody > tr > td.footable-cell-detail {
          background: #eee;
          border-top: none;
        }
        .footable.breakpoint > tbody > tr > td > span.footable-toggle {
          display: inline-block;
          font-family: 'footable';
          speak: none;
          font-style: normal;
          font-weight: normal;
          font-variant: normal;
          text-transform: none;
          -webkit-font-smoothing: antialiased;
          padding-right: 10px;
          color: #777;
        }
        .footable.breakpoint > tbody > tr > td > span.footable-toggle:before {
          content: "\e000";
        }
        .footable.breakpoint.toggle-circle > tbody > tr.footable-detail-show > td > span.footable-toggle:before {
          content: "\e005";
        }
        .footable.breakpoint.toggle-circle > tbody > tr > td > span.footable-toggle:before {
          content: "\e004";
        }
        .footable.breakpoint.toggle-circle-filled > tbody > tr.footable-detail-show > td > span.footable-toggle:before {
          content: "\e003";
        }
        .footable.breakpoint.toggle-circle-filled > tbody > tr > td > span.footable-toggle:before {
          content: "\e002";
        }
        .footable.breakpoint.toggle-square > tbody > tr.footable-detail-show > td > span.footable-toggle:before {
          content: "\e007";
        }
        .footable.breakpoint.toggle-square > tbody > tr > td > span.footable-toggle:before {
          content: "\e006";
        }
        .footable.breakpoint.toggle-square-filled > tbody > tr.footable-detail-show > td > span.footable-toggle:before {
          content: "\e009";
        }
        .footable.breakpoint.toggle-square-filled > tbody > tr > td > span.footable-toggle:before {
          content: "\e008";
        }
        .footable.breakpoint.toggle-arrow > tbody > tr.footable-detail-show > td > span.footable-toggle:before {
          content: "\e00f";
        }
        .footable.breakpoint.toggle-arrow > tbody > tr > td > span.footable-toggle:before {
          content: "\e011";
        }
        .footable.breakpoint.toggle-arrow-small > tbody > tr.footable-detail-show > td > span.footable-toggle:before {
          content: "\e013";
        }
        .footable.breakpoint.toggle-arrow-small > tbody > tr > td > span.footable-toggle:before {
          content: "\e015";
        }
        .footable.breakpoint.toggle-arrow-circle > tbody > tr.footable-detail-show > td > span.footable-toggle:before {
          content: "\e01b";
        }
        .footable.breakpoint.toggle-arrow-circle > tbody > tr > td > span.footable-toggle:before {
          content: "\e01d";
        }
        .footable.breakpoint.toggle-arrow-circle-filled > tbody > tr.footable-detail-show > td > span.footable-toggle:before {
          content: "\e00b";
        }
        .footable.breakpoint.toggle-arrow-circle-filled > tbody > tr > td > span.footable-toggle:before {
          content: "\e00d";
        }
        .footable.breakpoint.toggle-arrow-tiny > tbody > tr.footable-detail-show > td > span.footable-toggle:before {
          content: "\e01f";
        }
        .footable.breakpoint.toggle-arrow-tiny > tbody > tr > td > span.footable-toggle:before {
          content: "\e021";
        }
        .footable.breakpoint.toggle-arrow-alt > tbody > tr.footable-detail-show > td > span.footable-toggle:before {
          content: "\e017";
        }
        .footable.breakpoint.toggle-arrow-alt > tbody > tr > td > span.footable-toggle:before {
          content: "\e019";
        }
        .footable.breakpoint.toggle-medium > tbody > tr > td > span.footable-toggle {
          font-size: 18px;
        }
        .footable.breakpoint.toggle-large > tbody > tr > td > span.footable-toggle {
          font-size: 24px;
        }
        .footable > thead > tr > th {
          -webkit-touch-callout: none;
          -webkit-user-select: none;
          -khtml-user-select: none;
          -moz-user-select: -moz-none;
          -ms-user-select: none;
          user-select: none;
        }
        .footable > thead > tr > th.footable-sortable:hover {
          cursor: pointer;
        }
        .footable > thead > tr > th.footable-sorted > span.footable-sort-indicator:before {
          content: "\e013";
        }
        .footable > thead > tr > th.footable-sorted-desc > span.footable-sort-indicator:before {
          content: "\e012";
        }
        .footable > thead > tr > th > span.footable-sort-indicator {
          display: inline-block;
          font-family: 'footable';
          speak: none;
          font-style: normal;
          font-weight: normal;
          font-variant: normal;
          text-transform: none;
          -webkit-font-smoothing: antialiased;
          padding-left: 5px;
        }
        .footable > thead > tr > th > span.footable-sort-indicator:before {
          content: "\e022";
        }
        .footable > tfoot .pagination {
          margin: 0;
        }
        .footable.no-paging .hide-if-no-paging {
          display: none;
        }
        .footable-row-detail{
          background-color:#f5f5f5
        }
        .footable-row-detail-inner {
          display: table;  
        }
        .footable-row-detail-row {
          display: table-row;
          line-height: 1.5em;
        }
        .footable-row-detail-group {
          display: block;
          line-height: 2em;
          font-size: 1.2em;
          font-weight: bold;
        }
        .footable-row-detail-name {
          display: table-cell;
          font-weight: 500;
          padding-right: 1em;
          padding-bottom: 5px;
        }
        .footable-row-detail-value {
          display: table-cell;
        }
        .footable-odd {
          background-color: #f7f7f7;
        }
    </style>
</head>
