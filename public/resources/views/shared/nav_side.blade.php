 <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav slimscrollsidebar">
                <div class="sidebar-head">
                    <h3><span class="fa-fw open-close"><i class="ti-close ti-menu"></i></span> <span class="hide-menu">Navigation</span></h3> </div>
                <div class="user-profile">
     
                </div>
                <ul class="nav" id="side-menu">
                   @if (!empty($menu))
                    @foreach($menu as $mn)
                    <li><a @if ($mn->url != '#')  href="{{ url('/') }}/{{ $mn->url }}" @else href="{{ $mn->url }}" @endif @if ($mn->is_active != 0) onclick="on_menu();" @else class="waves-effect" @endif><i class="{{ $mn->icon }}"></i> <span class="hide-menu">{{ $mn->title }} @if ($mn->is_active == 0) <span class="fa arrow"></span></span>@endif</a>
                        
                        @if (!empty($mn->sub_menu))
                        <ul class="nav nav-second-level">
                            @foreach ($mn->sub_menu as $s_mn)
                            <li><a @if ($s_mn->url != '#') href="{{ url('/') }}/{{ $s_mn->url }}" @else href="{{ $s_mn->url }}" @endif @if ($s_mn->is_active != 0) onclick="on_menu();" @else class="waves-effect" @endif><i class="{{ $s_mn->icon }}"></i> <span class="hide-menu">{{ $s_mn->title }} @if ($s_mn->is_active == 0) <span class="fa arrow"></span></span>@endif</a>
                                @if (!empty($s_mn->sub_menu))
                                <ul class="nav nav-third-level">
                                @foreach ($s_mn->sub_menu as $ss_mn)
                                    <li><a @if ($ss_mn->url != '#') href="{{ url('/') }}/{{ $ss_mn->url }}" @else  href="{{ $ss_mn->url }}" @endif @if ($ss_mn->is_active != 0)  onclick="on_menu();" @else class="waves-effect" @endif><i class="{{ $ss_mn->icon }}"></i> <span class="hide-menu">{{ $ss_mn->title }} @if ($ss_mn->is_active == 0) <span class="fa arrow"></span></span> @endif </a>
                                    </li>
                                @endforeach
                                </ul>
                                @endif
                            </li>
                            @endforeach
                        </ul>
                        @endif

                    </li>
                    @endforeach
                    @endif

                </ul>
            </div>
        </div>