<script src="{{ url('/') }}/assets/plugins/bower_components/jquery/dist/jquery.min.js"></script>
<script src="{{ url('/') }}/assets/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="{{ url('/') }}/assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
<script src="{{ url('/') }}/assets/js/jquery.slimscroll.js"></script>
<script src="{{ url('/') }}/assets/js/waves.js"></script>
<script src="{{ url('/') }}/assets/js/custom.min.js"></script>
<script src="{{ url('/') }}/assets/js/toastr.js"></script>
<script src="{{ url('/') }}/assets/plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
<script type="text/javascript">
	    var BASE_URL = "{{ url('/') }}/";
</script>

<!-- The core Firebase JS SDK is always required and must be listed first -->
<!-- <script src="https://www.gstatic.com/firebasejs/7.23.0/firebase-app.js"></script> -->

<!-- TODO: Add SDKs for Firebase products that you want to use
     https://firebase.google.com/docs/web/setup#available-libraries -->
<!-- <script src="https://www.gstatic.com/firebasejs/7.23.0/firebase-analytics.js"></script> -->

<!-- <script> -->
<!--   // Your web app's Firebase configuration
  // For Firebase JS SDK v7.20.0 and later, measurementId is optional
  var firebaseConfig = {
    apiKey: "AIzaSyATIjISpETUoHwRk45w5ggxqNZGgTlAP_w",
    authDomain: "monitoringdisduk.firebaseapp.com",
    databaseURL: "https://monitoringdisduk.firebaseio.com",
    projectId: "monitoringdisduk",
    storageBucket: "monitoringdisduk.appspot.com",
    messagingSenderId: "166679085774",
    appId: "1:166679085774:web:755eddfd898dacbee3069f",
    measurementId: "G-F3RC0EB5E6"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
  firebase.analytics();
</script> -->