     <script>
        function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode != 44 && charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }
        function akses(data) {
            block_screen();
            var url = "{{ url('/') }}/";
            url += "Setting/User/Edit/";
            url += data;
            var win = window.location.replace(url);
            win.focus();
        }
        function lihat(data) {
            block_screen();
            var url = "{{ url('/') }}/";
            url += "Setting/User/Lihat/";
            url += data;
            var win = window.location.replace(url);
            win.focus();
        }
        function edit(data) {
            block_screen();
            var url = "{{ url('/') }}/";
            url += "Setting/User/Change/";
            url += data;
            var win = window.location.replace(url);
            win.focus();
        }
        function hapus(data) {
            block_screen();
            var url = "{{ url('/') }}/";
            url += "Setting/User/Delete/";
            url += data;
            var win = window.location.replace(url);
            win.focus();
        }
        function on_add_level() {
            block_screen();
            var url = "{{ url('/') }}/";
            url += "Setting/User/Add";
            var win = window.location.replace(url);
            win.focus();
        }
    
        $(document).ready(function() {
           
        $.ajax({
                    type: "post",
                    url: BASE_URL+"Setting/get_group",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                    },
                    beforeSend:
                    function () {
                        $('select[name="kdgroup"]').attr("disabled",true);
                    },
                    success: function (data) {
                        console.log(data);
                       $('select[name="kdgroup"]').empty();
                       $('select[name="kdgroup"]').append('<option value="0">-- Select Group --</option>');
                        $.each(data, function(key, value) {
                            $('select[name="kdgroup"]').append('<option value="'+ value.level_code +'">'+ value.level_name +'</option>');
                        });
                        $('select[name="kdgroup"]').val("0").trigger("change");
                        
                    },
                    error:
                    function (data) {
               

                    },
                    complete:
                    function (response) {
                        $('select[name="kdgroup"]').attr("disabled",false);
                    }
                });
        $('select[name="kdgroup"]').on('change', function() {

            var kdgroup = $(this).val();;
            get_table(kdgroup);
        });

    });
    function get_table(kdgroup) {
        console.log(kdgroup);
        $('#group-list').DataTable({
                destroy: true,
                 "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                 "columnDefs": [
                    {"className": "dt-center", "targets": "_all"}
                  ],
                "pageLength" : 50,
                "ajax": {
                 "url": BASE_URL+"Setting/get_user_level",
                "type": "post",
                "data": {
                "_token": "{{ csrf_token() }}",
                "group_id":  kdgroup
                }
                }
            });
    }
    
    jQuery(document).ready(function() {
        $(".select2").select2();
        $('#tanggal').val("<?php echo date('d-m-Y');?>");
        });
     function block_screen(){
        
        $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff'
            },
            baseZ: 2000
        }); 
    }
    function unblock_screen(){
       $.unblockUI();
    }
    </script>