 <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $stitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                        <ol class="breadcrumb">
                           <li class="text-info"><?php echo $stitle; ?></li>
                        </ol>
                    </div>
                </div>
                 <div class="row">
                 <!-- .row -->
               <div id="pengajuan_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog modal-dialog-scrollable">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title" id="modal-title">Tambah Pengajuan</h4> </div>
                                        <div class="modal-body">
                                            <div class="container-fluid">
                                            <form id="data_pengajuan">
                                                <h2 class="text-center">BIODATA</h2>
                                                <input type="hidden" id="jenis_layanan" name="jenis_layanan" value="1">
                                                <input type="hidden" id="status" name="status" value="1"> 
                                                <hr>
                                                <div class="form-group">
                                                    <label for="nik" class="control-label"><span style="color: red!important">* </span>NIK:</label>
                                                    <input type="text" class="form-control" id="nik" name="nik" maxlength="16" onkeypress="return isphone(event)"> </div>
                                                <div class="form-group">
                                                    <label for="no_kk" class="control-label"><span style="color: red!important">* </span>NO KK:</label>
                                                    <input type="text" class="form-control" id="no_kk" name="no_kk" maxlength="16" onkeypress="return isphone(event)"> </div>
                                                <div class="form-group">
                                                    <label for="nama_lgkp" class="control-label"><span style="color: red!important">* </span>Nama Lengkap:</label>
                                                    <input type="text" class="form-control" id="nama_lgkp" name="nama_lgkp" style="text-transform:uppercase"> </div>
                                                <div class="form-group">
                                                    <label for="tmpt_lhr" class="control-label"><span style="color: red!important">* </span>Tempat Lahir:</label>
                                                    <input type="text" class="form-control" id="tmpt_lhr" name="tmpt_lhr" style="text-transform:uppercase"> </div>
                                                <div class="form-group">
                                                    <label for="tgl_lhr" class="control-label"><span style="color: red!important">* </span>Tanggal Lahir:</label>
                                                    <input type="text" placeholder="yyyy-mm-dd" data-mask="9999-99-99" class="form-control" id="tgl_lhr" name="tgl_lhr"> </div>
                                                <div class="form-group">
                                                    <label for="jenis_klmin" class="control-label"><span style="color: red!important">* </span>Jenis Kelamin:</label>
                                                    <select class="form-control" name="jenis_klmin" id="jenis_klmin" value="">
                                                  </select> </div>

                                                <div class="form-group">
                                                    <label for="stat_hbkel" class="control-label"><span style="color: red!important">* </span>Hubungan Keluarga:</label>
                                                    <select class="form-control" name="stat_hbkel" id="stat_hbkel" value="">
                                                  </select> </div>
                                                <div class="form-group">
                                                    <label for="agama" class="control-label"><span style="color: red!important">* </span>Agama:</label>
                                                    <select class="form-control" name="agama" id="agama" value="">
                                                  </select> </div>
                                                <div class="form-group">
                                                    <label for="stat_kwn" class="control-label"><span style="color: red!important">* </span>Status Kawin:</label>
                                                    <select class="form-control" name="stat_kwn" id="stat_kwn" value="">
                                                  </select> </div>
                                                <div class="form-group">
                                                    <label for="gol_drh" class="control-label"><span style="color: red!important">* </span>Golongan Darah:</label>
                                                    <select class="form-control" name="gol_drh" id="gol_drh" value="">
                                                  </select> </div>
                                                <div class="form-group">
                                                    <label for="pendidikan" class="control-label"><span style="color: red!important">* </span>Pendidikan:</label>
                                                    <select class="form-control" name="pendidikan" id="pendidikan" value="">
                                                  </select> </div>
                                                <div class="form-group">
                                                    <label for="pekerjaan" class="control-label"><span style="color: red!important">* </span>Pekerjaan:</label>
                                                    <select class="form-control" name="pekerjaan" id="pekerjaan" value="">
                                                  </select> </div>
                                                <div class="form-group">
                                                    <label for="alasan_pindah" class="control-label"><span style="color: red!important">* </span>Alasan Pindah:</label>
                                                    <select class="form-control" name="alasan_pindah" id="alasan_pindah" value="">
                                                  </select> </div>

                                                <div class="form-group">
                                                    <label for="jum_anggota" class="control-label">Jumlah Anggota Yang Dibawa:</label>
                                                    <input type="text" class="form-control" placeholder="0" value="0" name="jum_anggota" id="jum_anggota" onkeypress="return isphone(event)"> </div>

                                                <div class="form-group">
                                                    <label for="jangka_waktu" class="control-label">Jangka Waktu:</label>
                                                    <input type="text" class="form-control" placeholder="0" value="0" name="jangka_waktu" id="jangka_waktu" onkeypress="return isphone(event)"> </div>
                                                     <div class="form-group">
                                                    <label for="tgl_datang" class="control-label"><span style="color: red!important">* </span>Tanggal Kedatangan:</label>
                                                    <input type="text" placeholder="yyyy-mm-dd" data-mask="9999-99-99" class="form-control" id="tgl_datang" name="tgl_datang"> </div>


                                                <div class="form-group">
                                                    <label for="telepon" class="control-label">Telepon:</label>
                                                    <input type="text" class="form-control" placeholder="+62" value="+62" name="telepon" id="telepon" onkeypress="return isphone(event)"> </div>
                                                <div class="form-group">
                                                    <label for="email" class="control-label">E-mail:</label>
                                                    <input type="text" class="form-control" id="email" name="email"> </div>

                                                <h2 class="text-center">DATA DAERAH ASAL</h2>
                                                <div class="col text-center">
                                                    <a class="fcbtn btn btn-info btn-outline btn-1f text-center" id="is_asal" onclick="get_wil();" style="display: none;">Rubah Wilayah Asal</a>
                                                </div>
                                                <hr>
                                                <div class="form-group">
                                                    <label for="src_prov" class="control-label"><span style="color: red!important">* </span>Provinsi:</label>
                                                    <select class="form-control" name="src_prov" id="src_prov" value="">
                                                  </select> </div>
                                                <div class="form-group">
                                                    <label for="src_kab" class="control-label"><span style="color: red!important">* </span>Kab/Kota:</label>
                                                    <select class="form-control" name="src_kab" id="src_kab" value="">
                                                  </select> </div>
                                                <div class="form-group">
                                                    <label for="src_kec" class="control-label"><span style="color: red!important">* </span>Kecamatan:</label>
                                                    <select class="form-control" name="src_kec" id="src_kec" value="">
                                                  </select> </div>
                                                <div class="form-group">
                                                    <label for="src_kel" class="control-label"><span style="color: red!important">* </span>Kelurahan:</label>
                                                    <select class="form-control" name="src_kel" id="src_kel" value="">
                                                  </select> </div>
                                                <div class="form-group">
                                                    <label for="src_alamat" class="control-label">Alamat:</label>
                                                    <input type="text" class="form-control" name="src_alamat" id="src_alamat" style="text-transform:uppercase"> </div>
                                                <div class="form-group">
                                                    <label for="src_no_rt" class="control-label">No RT:</label>
                                                    <input type="text" class="form-control" name="src_rt" id="src_rt" maxlength="3" onkeypress="return isphone(event)"> </div>
                                                <div class="form-group">
                                                    <label for="src_no_rw" class="control-label">No RW:</label>
                                                    <input type="text" class="form-control" name="src_rw" id="src_rw" maxlength="3" onkeypress="return isphone(event)"> </div>
                                                <h2 class="text-center">DATA KOTA BANDUNG</h2>
                                                <div class="col text-center">
                                                    <a class="fcbtn btn btn-info btn-outline btn-1f text-center" id="is_tujuan" onclick="get_wil2();" style="display: none;">Rubah Wilayah Tujuan</a>
                                                </div>
                                                <hr>
                                                <div class="form-group">
                                                    <label for="prop" class="control-label"><span style="color: red!important">* </span>Provinsi:</label>
                                                    <select class="form-control" name="prop" id="prop" value="">
                                                        <option value="32" selected>32 - JAWA BARAT</option>
                                                  </select> </div>
                                                <div class="form-group">
                                                    <label for="kab" class="control-label"><span style="color: red!important">* </span>Kab/Kota:</label>
                                                    <select class="form-control" name="kab" id="kab" value="">
                                                        <option value="73" selected>73 - JAWA BARAT</option>
                                                  </select> </div>
                                                <div class="form-group">
                                                    <label for="kec" class="control-label"><span style="color: red!important">* </span>Kecamatan:</label>
                                                    <select class="form-control" name="kec" id="kec" value="">
                                                  </select> </div>
                                                <div class="form-group">
                                                    <label for="kel" class="control-label"><span style="color: red!important">* </span>Kelurahan:</label>
                                                    <select class="form-control" name="kel" id="kel" value="">
                                                  </select> </div>
                                                <div class="form-group">
                                                    <label for="alamat" class="control-label">Alamat:</label>
                                                    <input type="text" class="form-control" name="alamat" id="alamat" style="text-transform:uppercase"> </div>
                                                <div class="form-group">
                                                    <label for="no_rt" class="control-label">No RT:</label>
                                                    <input type="text" class="form-control" name="no_rt" id="no_rt" maxlength="3" onkeypress="return isphone(event)"> </div>
                                                <div class="form-group">
                                                    <label for="no_rw" class="control-label">No RW:</label>
                                                    <input type="text" class="form-control" name="no_rw" id="no_rw" maxlength="3" onkeypress="return isphone(event)"> </div>
                                                
                                                <h2 class="text-center">BERKAS PERSYARATAN</h2>
                                                <div class="col text-center">
                                                    <a class="fcbtn btn btn-info btn-outline btn-1f text-center" id="is_upload" onclick="get_upl();" style="display: none;">Upload Ulang Dokumen</a>
                                                </div>
                                                <hr>
                                                
                                                    <div class="form-group col-sm-6 ol-md-6 col-xs-12" id="blockimgupload1">
                                                        <input type="file" name="imgupload1" id="imgupload1" class="dropify" data-max-file-size="2M" accept='image/*'/> 
                                                        <h3 class="box-title text-center">Pas Foto Berwarna</h3>
                                                    </div>
                                                
                                                    <div class="form-group col-sm-6 ol-md-6 col-xs-12" id="blockimgupload2">
                                                        <input type="file" name="imgupload2" id="imgupload2" class="dropify" data-max-file-size="2M" accept='image/*'/> 
                                                        <h3 class="box-title text-center">Ktp-El</h3>
                                                        </div>
                                                
                                                    <div class="form-group col-sm-6 ol-md-6 col-xs-12" id="blockimgupload3">
                                                        <input type="file" name="imgupload3" id="imgupload3" class="dropify" data-max-file-size="2M" accept='image/*'/> 
                                                        <h3 class="box-title text-center">Kartu Keluarga</h3>
                                                    </div>
                                                
                                                    <div class="form-group col-sm-6 ol-md-6 col-xs-12" id="blockimgupload4">
                                                        <input type="file" name="imgupload4" id="imgupload4" class="dropify" data-max-file-size="2M" accept='image/*'/> 
                                                        <h3 class="box-title text-center">Surat Pengantar RT/RW</h3>
                                                    </div>
                                                
                                                    <div class="form-group col-sm-12 ol-md-12 col-xs-12" id="blockimgupload5">
                                                        <input type="file" name="imgupload5" id="imgupload5" class="dropify" data-max-file-size="2M" accept='image/*'/> 
                                                        <h3 class="box-title text-center">Dokumen Pendukung</h3>
                                                    </div>
                                                    <div class="form-group col-sm-6 ol-md-6 col-xs-12" id="blockimgdisplay1" style="display: none;">
                                                       <div class="m-t-20 row"><img id="imgdisplay1" src="http://10.32.73.7:8080/monitoring/assets/plugins/images/female-placeholder.jpg" alt="user" class="col-md-12 col-xs-12" /></div>
                                                    </div>
                                                    <div class="form-group col-sm-6 ol-md-6 col-xs-12" id="blockimgdisplay2" style="display: none;">
                                                       <div class="m-t-20 row"><img id="imgdisplay2" src="http://10.32.73.7:8080/monitoring/assets/plugins/images/female-placeholder.jpg" alt="user" class="col-md-12 col-xs-12" /> </div>
                                                    </div>
                                                    <div class="form-group col-sm-6 ol-md-6 col-xs-12" id="blockimgdisplay3" style="display: none;">
                                                       <div class="m-t-20 row"><img id="imgdisplay3" src="http://10.32.73.7:8080/monitoring/assets/plugins/images/female-placeholder.jpg" alt="user" class="col-md-12 col-xs-12" /> </div>
                                                    </div>
                                                    <div class="form-group col-sm-6 ol-md-6 col-xs-12" id="blockimgdisplay4" style="display: none;">
                                                       <div class="m-t-20 row"><img id="imgdisplay4" src="http://10.32.73.7:8080/monitoring/assets/plugins/images/female-placeholder.jpg" alt="user" class="col-md-12 col-xs-12" /> </div>
                                                    </div>
                                                    <div class="form-group col-sm-12 ol-md-12 col-xs-12" id="blockimgdisplay5" style="display: none;">
                                                       <div class="m-t-20 row"><img id="imgdisplay5" src="http://10.32.73.7:8080/monitoring/assets/plugins/images/female-placeholder.jpg" alt="user" class="col-md-12 col-xs-12" /> </div>
                                                    </div>

                                            </form>
                                        </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                            <button type="button" id="submit_btn" class="btn btn-info waves-effect waves-light">Save</button>
                                            <button type="button" id="submit_edit" class="btn btn-info waves-effect waves-light" style="display: none;">Edit</button>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                            <div id="tolak_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog modal-dialog-scrollable">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title" id="modal-title">Kirim Pemberitahuan</h4> </div>
                                        <div class="modal-body">
                                            <div class="container-fluid">
                                            <form id="data_tolak">
                                                <input type="hidden" id="jenis_layanan_tolak" name="jenis_layanan_tolak" value="1">
                                                <input type="hidden" id="nik_tolak" name="nik_tolak" >
                                                <input type="hidden" id="status_tolak" name="status_tolak" value="1"> 
                                                <input type="hidden" id="id_pengajuan_tolak" name="id_pengajuan_tolak"> 
                                                <div class="form-group">
                                                    <label for="nik" class="control-label"><span style="color: red!important">* </span>Isi Pesan:</label>
                                                    <textarea  class="form-control" id="isi_pesan" name="isi_pesan" minlength="10" rows="5" style="resize: none;" onkeypress="cntText()"></textarea> 
                                                    <h6 class="control-label"><span id="cntnum" style="color: red">Minimal Anda Harus Mengisi 10 Karakter Untuk Mengirim Pesan, Anda Baru Memasukan 0 Karakter !</span></h6>
                                                </div>
                                                

                                            </form>
                                        </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                            <button type="button" id="submit_btnntf" class="btn btn-info waves-effect waves-light">Kirim</button>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                            <div id="info_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog modal-dialog-scrollable">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title" id="modal-title">Info Penggunaan Aplikasi</h4> </div>
                                        <div class="modal-body">
                                            <div class="container-fluid">
                                                <a class="btn btn-info btn-circle"  style="margin-top: 5px"><i class="fa fa-pencil"></i></a> 
                                                 <span style="margin-top: 10px!important vertical-align:middle!important">: Tombol Untuk Melakukan Pengeditan Pengajuan.</span>
                                            <br> <a class="btn btn-danger btn-circle" style="margin-top: 5px"><i class="fa fa-trash-o"></i></a>
                                                 <span style="margin-top: 10px!important">: Tombol Untuk Menghapus Pengajuan.</span>
                                            <br> <a class="btn btn-info btn-circle" style="margin-top: 5px"><i class="fa  fa-print"></i></a>
                                                 <span style="margin-top: 10px!important">: Tombol Untuk Mencetak Berkas Persyaratan.</span>
                                            <br> <a class="btn btn-warning btn-circle" style="margin-top: 5px"><i class="fa fa-envelope"></i></a>
                                                 <span style="margin-top: 10px!important">: Tombol Untuk Memberikan Pesan Kepada Masyarakat.</span>
                                            <br> <a class="btn btn-success btn-circle" style="margin-top: 5px"><i class="fa fa-check"></i></a>
                                                 <span style="margin-top: 10px!important">: Tombol Untuk Mempublish Pengajuan.</span>
                                        </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                <form name ="get_form" action="<?php echo $my_url; ?>" method="post">
                    @csrf
                    <div class="col-sm-12">
                        <div class="white-box">
                            
                              
                                <?php if ($my_url != 'Push'){ ?>
                                    <div class="row">
                                <div class="col-lg-3">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_kec" type="checkbox" checked disabled="true">
                                            <label for="cb_kec"> Kecamatan</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3">         
                                    <div class="form-group" <?php if (!empty($is_colnotwil)){echo 'style="display: none!important;"'; }?>>
                                        <select class="form-control select2" name="no_kec" id="no_kec">
                                         <option  value="0">-- Pilih Kecamatan --</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-3">         
                                    <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_kel" type="checkbox" checked disabled="true">
                                            <label for="cb_kel"> Kelurahan</label>
                                        </div>
                                </div>
                                <div class="col-lg-3">         
                                    <div class="form-group" <?php if (!empty($is_colnotwil)){echo 'style="display: none!important;"'; }?>>
                                        <select class="form-control select2" name="no_kel" id="no_kel">
                                        <option value="0">-- Pilih Kelurahan --</option>  
                                    </select>
                           
                                    </div>
                                </div>
                            </div>
                                <div class="row">
                                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_nik" type="checkbox" checked disabled="true">
                                            <label for="cb_nik"> Nik</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">         
                                    <div class="form-group">
                                        <input class="form-control" type="text" id="nik" name="nik" onkeypress="return isNumberKey(event)" onchange="onlyNum()" /></div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_nik" type="checkbox" checked disabled="true">
                                            <label for="cb_nik"> No KK</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">         
                                    <div class="form-group">
                                        <input class="form-control" type="text" id="no_kk" name="no_kk" onkeypress="return isNumberKey(event)" onchange="onlyNum()" /></div>
                                </div>
                                </div>
                                <div class="row">
                                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_nik" type="checkbox" checked disabled="true">
                                            <label for="cb_nik"> Nama Lengkap</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">         
                                    <div class="form-group">
                                        <input class="form-control" type="text" id="nama_lgkp" name="nama_lgkp" /></div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_tgl_lhr" type="checkbox" checked disabled="true">
                                            <label for="cb_tgl_lhr"> Tanggal Lahir</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">         
                                    <div class="form-group">
                                         <input type="text" name="tgl_lhr" class="form-control" id="tgl_lhr" value="" placeholder="DD-MM-YYYY" readonly></div>
                                </div>
                                </div>
                                <?php } ?>
                                
                            
                           <?php if ($my_url == 'Push'){ ?>
                            <?php if (!empty($data)){ ?>
                            <div class="row">
                                  <button type="button" class="btn btn-info waves-effect waves-light m-r-10 pull-right" id="btn-reset" onclick="openTab('{{ url('/') }}/Check/Ktpel');">Back <i class="mdi  mdi-backspace fa-fw"></i></button>
                                  
                            </div>
                            <?php }else{ ?>
                            <div class="row">
                                <button type="submit" name="do_search" class="btn btn-info waves-effect waves-light m-r-10 pull-right" id="btn-filter" onclick="on_serach();" >Push <i class="mdi  mdi-near-me fa-fw"></i></button>
                                   <button type="button" class="btn btn-info waves-effect waves-light m-r-10 pull-right" id="btn-reset" onclick="openTab('{{ url('/') }}/Check/Ktpel');">Back <i class="mdi  mdi-backspace fa-fw"></i></button>
                                  
                            </div>
                            <?php } ?> 
                           <?php }else{ ?>
                           <div class="row">
                                <button type="submit" name="do_search" class="btn btn-info waves-effect waves-light m-r-10 pull-right" id="btn-filter" onclick="on_serach();" >Search <i class="mdi  mdi-magnify fa-fw"></i></button>
                                  <button type="button" class="btn btn-invert waves-effect waves-light m-r-10 pull-right" id="btn-reset" onclick="on_clear();">Clear <i class="mdi  mdi-delete fa-fw"></i></button>
                                  
                            </div>
                           <?php } ?>
                            
                        </div>
                    </div>
                    </form>
                </div>
                <div class="row">
                <div class="col-md-12">
                        <div class="white-box">
                            <h3 class="box-title"><?php echo $stitle; ?></h3>
                            <?php if (!empty($data)){ 
                                $niks = array();
                                foreach($data as $row){
                                    $niks[] = $row->nik;
                                    }
                                ?>
                                <div class="row">
                                <div class="col-md-4">
                                <div class="button-box">
                                        <button type="submit" onclick="do_export();" class="btn btn-info waves-effect waves-light"> <i class="mdi mdi-file-excel-box m-r-5" ></i> <span>Export</span></button>
                                    
                                        <button type="submit" onclick="do_pdf();" class="btn btn-info waves-effect waves-light"> <i class="mdi mdi-file-excel-box m-r-5"></i> <span>Pdf</span></button>
                                    
                                    <form target="_blank" id="export_data" name ="get_form" action="<?php echo $my_url; ?>/Export" method="post">
                                        @csrf
                                        <input type="hidden" name="niks" id="export_niks" value="<?php echo implode(',',$niks); ?>">
                                    </form>
                                    <form target="_blank" id="pdf_data" name ="get_form" action="<?php echo $my_url; ?>/Pdf" method="get">
                                        <input type="hidden" id="pdf_niks" name="niks" value="<?php echo implode(',',$niks); ?>">
                                    </form>
                                </div>
                                </div>
                                </div>
                            <?php } ?>
                            <div class="scrollable">
                                <div class="table-responsive">
                                    <table id="demo-foo-row-toggler" class="table m-t-30 table-hover contact-list color-table info-table" data-page-size="100" style="margin-left: 1px; margin-right: 1px;">
                                        <thead>
                                            <tr>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">NO</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">NAMA </th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">NIK</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">LAHIR</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">NO SKTS</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">STATUS CETAK</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">PENGAJUAN</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">NOMOR KK</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">NAMA KEC</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">NAMA KEL</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">ALAMAT</th>
                                                <th data-sort-ignore="true" style="text-align: center; border: 1px solid #fff;" valign="center">AKSI</th>
                                            </tr>
                                        </thead>
                                        <tbody id='my_data' style="border: 1px solid #e4e7ea;">
                                             <?php
                                        if (!empty($data)){
                                            $i = 0;
                                           foreach($data as $row){
                                            $i++;
                                            ?>
                                            <tr style="border: 1px solid #e4e7ea;">
                                                <td valign="center" style="border: 1px solid #e4e7ea;"><?php echo $i ;?></td>
                                                <td valign="center" style="border: 1px solid #e4e7ea;">
                                                   
                                                            
                                                          <?php echo $row->nama_lgkp ;?>
                                                </td>
                                                <td valign="center" style="border: 1px solid #e4e7ea; text-align: center;"><?php echo $row->nik ;?>
                                                     
                                                </td>
                                                <td valign="center" style="border: 1px solid #e4e7ea; text-align: center;"><?php echo $row->tmpt_lhr ;?><br><?php echo $row->tgl_lhr ;?>
                                                     
                                                </td>
                                                
                                                <td valign="center" style="border: 1px solid #e4e7ea; text-align: center;"><?php echo $row->no_skts ;?><br />
                                               
                                                </td>
                                                <td valign="center" style="border: 1px solid #e4e7ea;">
                                                    <i class="mdi mdi-account-circle text-white" style="float: none; color: #292961 !important;"></i><span style="float: none; color: #292961 !important;"> {{ ($row->ktp_by == '32730000yanzen' || $row->ktp_by == '32730000dian') ? '32730000dns' : $row->ktp_by }}</span><br>
                                                    <i class="mdi mdi-label text-white" style="float: none; color: #292961 !important;"></i><span style="float: none; color: #292961 !important;"> <?php echo $row->ktp_dt ;?></span><br>
                                                   
                                                </td>
                                                <td valign="center" style="border: 1px solid #e4e7ea;">
                                                    <i class="mdi mdi-account-circle text-white" style="float: none; color: #292961 !important;"></i><span style="float: none; color: #292961 !important;"> Tanggal Berlaku : <?php echo $row->req_by ;?></span><br>
                                                    <i class="mdi mdi-label text-white" style="float: none; color: #292961 !important;"></i><span style="float: none; color: #292961 !important;"> Tanggal Entri : <?php echo $row->req_date ;?></span><br>
                                                    <i class="mdi mdi-spotify text-white" style="float: none; color: #292961 !important;"></i><span style="float: none; color: #292961 !important;"><?php echo $row->proc_stat ;?> || <?php echo $row->step_proc ;?></span>
                                                </td>
                                                <td valign="center" style="border: 1px solid #e4e7ea; text-align: center;"><?php echo $row->no_kk ;?></td>
                                                <td valign="center" style="border: 1px solid #e4e7ea;"><?php echo $row->nama_kec ;?></td>
                                                <td valign="center" style="border: 1px solid #e4e7ea;"><?php echo $row->nama_kel ;?></td>
                                                <td valign="center" style="border: 1px solid #e4e7ea;"><?php echo $row->alamat ;?><br>RT. <?php echo $row->rt ;?> RW. <?php echo $row->rw ;?></td>
                                                <td valign="center" style="border: 1px solid #e4e7ea;">
                                                    <button type="button" class="btn btn-info btn-circle"  id="btn-edit" onclick="edit('{{$row->id}}');" ><i class="fa fa-pencil"></i></button> 
                                                    &nbsp <button type="button" class="btn btn-danger btn-circle" id="btn-hapus" onclick="hapus('{{$row->id}}','{{$row->nama_lgkp}}','{{$row->nik}}');" ><i class="fa fa-trash-o"></i></button>
                                                    &nbsp <button type="button" class="btn btn-info btn-circle" id="btn-print" onclick="print_data('{{$row->id}}');" ><i class="fa  fa-print"></i></button>
                                                    &nbsp <button type="button" class="btn btn-warning btn-circle" id="btn-pesan" onclick="pesan('{{$row->id}}','{{$row->nik}}');" ><i class="fa fa-envelope"></i></button>
                                                    <?php if($row->proc_stat == 1){ ?>
                                                    &nbsp <button type="button" class="btn btn-success btn-circle" id="btn-proses" onclick="proses('{{$row->id}}',\''.$r->nama_lgkp.'\','.$r->nik.');" ><i class="fa fa-check"></i></button>
                                                    <?php } ?>
                                                </td>
                                            </tr>

                                            <?php } }else{ ?>
                                            
                                                <tr>
                                                <td colspan="12" style="text-align: center;">No data available</td>
                                            </tr>
                                            <?php }?>

                                        </tbody>
                                        <tfoot>
                                            <tr style="border: 0px solid black;">
                                                
                                                <td colspan="12" style="border: 0px solid black;">
                                                    <div class="text-left">
                                                        <ul class="pagination"> </ul>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tfoot>
                                        <tfoot id="my_foot">
                                            
                                                <?php if (!empty($data)){ ?>
                                                <tr>
                                                <th colspan="12" style="text-align: left;">Total : <?php echo $i ;?> Data</th>
                                                </tr>
                                                <?php }else{ ?>
                                                <tr>
                                                <th colspan="12" style="text-align: left;">Total : 0 Data</th>
                                                </tr>
                                                <?php } ?>
                                            </tfoot>

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                 </div>
       @include('shared.footer_detail')

       