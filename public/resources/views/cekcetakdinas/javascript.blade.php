    <script src="{{ url('/') }}/assets/plugins/bower_components/owl.carousel/owl.carousel.min.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/owl.carousel/owl.custom.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/flot/jquery.flot.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/Minimal-Gauge-chart/js/cmGauge.js"></script>
    <!-- Footable -->
    <script src="{{ url('/') }}/assets/plugins/bower_components/footable/js/footable.all.min.js"></script>
    
    <!--FooTable init-->
    <script src="{{ url('/') }}/assets/js/footable-init.js"></script>
   
     <script>
        function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode != 44 && charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }
    $(document).ready(function($) {
        $('input[type=text]').on('input', function(evt) {
            $(this).val(function(_, val) {
            return val.toUpperCase();
            });
        });
        $('#cb_nik').change(function() {
            if ($('#cb_nik').is(':checked')) {
                $('#nik').attr("disabled",false);
                $('#nik').val("");
            }else{
                $('#nik').attr("disabled",true);
                $('#nik').val("");
            }
            
        });
        $('#nik').on('paste', function() {
          var $el = $(this);
          setTimeout(function() {
            $el.val(function(i, val) {
              return val.replace(/[^0-9,]/g, '')
            })
          })
        });
    });
    function on_clear() {
        $('#my_data').empty();
        $('#my_data').append('<tr><td colspan="10" style="text-align: center;">No data available</td></tr>');
        $('#my_foot').empty();
        $('#my_foot').append('<tr><th colspan="10" style="text-align: left;">Total : 0 Data</th></tr><tr style="border: 0px solid black;"><td colspan="10" style="border: 0px solid black;"><div class="text-right"><ul class="pagination"> </ul></div></td></tr>');
        $('#cb_nik').attr('checked', false);
        $('#nik').attr("disabled",true);
        $('#nik').val("");
    }
    function on_serach(){
        $('#my_data').html('<tr><td colspan="10" style="text-align: center;" valign="center">Waiting For Generate Data</td></td>');
        $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff'
            },
            baseZ: 2000
        }); 
    }
    function openInNewTab(url) {
        var win = window.open(url, '_blank');
        win.focus();
    }
    function openTab(url) {
        var win = window.location.replace(url);
        win.focus();
    }
    </script>