    <script src="{{ url('/') }}/assets/plugins/bower_components/owl.carousel/owl.carousel.min.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/owl.carousel/owl.custom.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/flot/jquery.flot.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/Minimal-Gauge-chart/js/cmGauge.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/Minimal-Gauge-chart/js/cmGauge.js"></script>
    <!-- Footable -->
    <script src="{{ url('/') }}/assets/plugins/bower_components/footable/js/footable.all.min.js"></script>
    
    <!--FooTable init-->
    <script src="{{ url('/') }}/assets/js/footable-init.js"></script>
    <script type="text/javascript" src="{{ url('/') }}/assets/plugins/bower_components/viewerjs-master/dist/jquery.magnify.js"></script>
   <script src="{{ url('/') }}/assets/plugins/bower_components/dropify/dist/js/dropify.min.js"></script>
    <script>
        function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }
        function isphone(evt)
       {
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode != 43 && charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }
       function validateEmail(email) {
          const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
          return re.test(email);
        }

      
    </script>
 <script>


  
         
        $(document).ready(function() {

            var table = $('#pengajuan-list').DataTable( {
                "ajax": {
                 "url": BASE_URL+"Report/get_absensi_v",
                "type": "post",
                },
                responsive: true,
                destroy: true,
                 "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                 "columnDefs": [
                    
                    {
                      render: function (data, type, full, meta) {
                            return "<div class='text-wrap width-250'>" + data + "</div>";
                        },
                        targets: '_all'
                    },
                    {
                      width    : "5%", targets: [0]
                    },
                    {
                      width    : "8%", targets: [1]
                    },
                    {
                      width    : "10%", targets: [5]
                    },
                    {
                      className: "text-center" , targets: [0,1]
                    }
                    
                 ]  
                  ,
                "pageLength" : 100,
                "columns": [
                    { "data": "no" },
                    { "data": "aksi" },
                    { "data": "nama_lgkp" },
                    { "data": "activity" },
                    { "data": "description" },
                    { "data": "created_dt" },
                    
                ],
                "order": [[0, 'asc']]
            });
            // get_table();
             
            
        });
    
    function get_table(){
        var table = $('#pengajuan-list').DataTable();
        table.ajax.reload();
        toastr.success('Data Berhasil Di Perbarui !');
    }
    function block_screen(){
        
        $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff'
            },
            baseZ: 2000
        }); 
    }
    function unblock_screen(){
       $.unblockUI();
    }
    function tolak(daily_id){
        var daily = daily_id;
        $('#tolak_modal').modal('show');
        $('#id_daily_tolak').val(daily);
        $('#isi_pesan').val("");
        $('#cntnum').html("Minimal Anda Harus Mengisi 10 Karakter Untuk Mengirim Pesan, Anda Baru Memasukan 0 Karakter !");
        $('#cntnum').css("color","red");
      }

    function terima(daily_id,nama_lgkp,nik){
        var daily_id = daily_id;
        var nama_lgkp = toTitleCase(nama_lgkp.toLowerCase());
        swal({   
            title: "Apakah Anda Yakin ?",   
            text: "Memverigikasi Aktifitas "+nama_lgkp+" ?",   
            type: "info",   
            showCancelButton: true,   
            confirmButtonColor: "#7ace4c",   
            confirmButtonText: "Ya, Verifikasi Aktifitas",   
            cancelButtonText: "Tidak, Jangan Diverifikasi",   
            closeOnConfirm: false,   
            closeOnCancel: false 
            }, function(isConfirm){   
                if (isConfirm) {
                $.ajax({
                    type: "post",
                    url:  BASE_URL+"Report/get_daily_acc",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        daily_id : daily_id,
                    },
                    beforeSend:
                    function () {
                         block_screen();
                    },
                    success: function (data) {
                        console.log(data);
                         if (data.message_type > 0){
                              swal("Berhasil !", "Aktifitas "+nama_lgkp+" Telah Diverifikasi !", "success");
                        }else{
                            swal("Warning!", data.message, "warning");  
                        }
                    },
                    error:
                    function (data) {
                        unblock_screen();
                        swal("Error!", "Ooops!, Please Try Again, Something Went Wrong", "error"); 

                    },
                    complete:
                    function (response) {
                        unblock_screen();
                        get_table();
                    }
                });
                     
                } else {     
                    swal("Cancelled", "Aktifitas "+nama_lgkp+" Tidak Jadi Diverifikasi :)", "error");   
                } 
            });
      }

      $('#submit_btnntf').click(function (e) {
       if (!validationmodaltolak()) return;
            $.ajax({
                    type: "post",
                    url: BASE_URL+"Report/get_daily_tlk",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        daily_id : $("#id_daily_tolak").val(),
                        isi_pesan : $("#isi_pesan").val(),
                    },
                    beforeSend:
                    function () {
                         block_screen();
                    },
                    success: function (data) {
                        console.log(data);
                        swal("Success!", data.message, "success");
                        $('#tolak_modal').modal('hide');
                    },
                    error:
                    function (data) {
                        unblock_screen();
                        swal("Error!", "Ooops!, Please Try Again, Something Went Wrong", "error"); 

                    },
                    complete:
                    function (response) {
                        unblock_screen();
                        get_table();
                    }
                });
      
      });
      function validationmodaltolak() {
            var id_daily_tolak = $("#id_daily_tolak");
            if (id_daily_tolak.val().length == 0) {
                id_daily_tolak.select();
                swal("Warning!", "ID Pengajuan Tidak Boleh Kosong !", "warning");  
                return false;
            }
            var isi_pesan = $("#isi_pesan");
            if (isi_pesan.val().length < 10) {
                isi_pesan.select();
                swal("Warning!", "Isi Pesan Tidak Boleh Kurang Dari 10 Karakter !", "warning");  
                return false;
            }
           return true
        }
    

   

      function cntText(){
        var text = $('#isi_pesan');
        var angka = text.val().length;
        var textangka = "Minimal Anda Harus Mengisi 10 Karakter Untuk Mengirim Pesan, Anda Baru Memasukan "+text.val().length+" Karakter !";
        var txtdata = "Anda Telah Memasukan "+text.val().length+" Karakter !";
        if(angka >= 10){
          $('#cntnum').html(txtdata);
          $('#cntnum').css("color","black");
        }else if(angka < 10){
          $('#cntnum').html(textangka);
          $('#cntnum').css("color","red");
        }
      }
      function toTitleCase(str) {
        return str.replace(/(?:^|\s)\w/g, function(match) {
            return match.toUpperCase();
        });
    }
    </script>