<style type="text/css">
    .text-wrap{
    white-space:normal;
}
</style>

<div id="page-wrapper">
            <div class="container-fluid">
               <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $mtitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                
                        <ol class="breadcrumb">
                            <li class="active"><?php echo $mtitle; ?></li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>

                <!-- .row -->
                           
                            
                <!-- /.row -->
                <!-- /row -->
                <div class="row">
                <div class="col-md-12">
                        <div class="white-box">
                            <h3 class="box-title"><?php echo $mtitle; ?></h3>
                            <div class="form-group">
                                <button class="fcbtn btn btn-info btn-outline btn-1f" onclick="get_table();">Refresh</button>
                                <div id="tolak_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog modal-dialog-scrollable">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title" id="modal-title">Kirim Pemberitahuan Penolakan</h4> </div>
                                        <div class="modal-body">
                                            <div class="container-fluid">
                                            <form id="data_tolak">
                                                <input type="hidden" id="id_daily_tolak" name="id_daily_tolak"> 
                                                <div class="form-group">
                                                    <label for="nik" class="control-label"><span style="color: red!important">* </span>Isi Pesan:</label>
                                                    <textarea  class="form-control" id="isi_pesan" name="isi_pesan" minlength="10" rows="5" style="resize: none;" onkeypress="cntText()"></textarea> 
                                                    <h6 class="control-label"><span id="cntnum" style="color: red">Minimal Anda Harus Mengisi 10 Karakter Untuk Mengirim Pesan, Anda Baru Memasukan 0 Karakter !</span></h6>
                                                </div>
                                                

                                            </form>
                                        </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                            <button type="button" id="submit_btnntf" class="btn btn-info waves-effect waves-light">Kirim</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                            <div class="scrollable">
                                <div class="table-responsive">
                                    <table id="pengajuan-list" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                   <thead>
                                    
                                        <tr>
                                            <!-- <th width="5%" style="text-align: center;">#</th> -->
                                            <th width="5%" style="text-align: center;">#</th>
                                            <th width="30%" style="text-align: center;">Aksi</th>
                                            <th width="20%" style="text-align: center;">Nama Lengkap</th>
                                            <th width="30%" style="text-align: center;">Aktivitas</th>
                                            <th width="30%" style="text-align: center;">Keterangan</th>
                                            <th width="30%" style="text-align: center;">Tanggal</th>
                                            

                                        </tr>
                                       
                                    </thead>
                                    <tbody id="show_data">
                                       
                                        
                                    </tbody>
                                    
                                </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
                <!-- .row -->
               

            </div>
       @include('shared.footer_detail')