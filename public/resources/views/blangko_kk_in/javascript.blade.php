    <script>
        function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode != 44 && charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }
    
    $(document).ready(function() {
        console.log(init_kec);
        console.log(init_level);
        get_all_data();
    });
    function get_all_data(){
        $.ajax({
                    type: "post",
                    url: BASE_URL+"Blangko/get_allin_kk_data",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        tanggal : $("#tanggal").val()
                    },
                    beforeSend:
                    function () {
                       
                    },
                    success: function (data) {
                        console.log(data);
                        $('#lbl_masuk').text(data.masuk);
                        $('#lbl_keluar').text(data.keluar);
                        $('#lbl_rusak').text(data.rusak);
                        $('#lbl_sisa').text(data.sisa);
                    },
                    error:
                    function (data) {
               

                    },
                    complete:
                    function (response) {

                        // $('select[name="no_kec"]').attr("disabled",false);
                    }
                });
    }
    function on_clear() {
        
        // $('select[name="no_kec"]').val(init_kec).trigger("change");
        // $('#txt_pengambilan').val("0");
        // $('#txt_rusak').val("0");
        $('#txt_jumlah').val("0");
        $('#txt_ket').val("");
        $('#tanggal').val("<?php echo date('d-m-Y');?>");
        get_all_data();
    }
     function on_save(){
        if (validationdaily()){
          do_save();
        }
  
        
    }
    function validationdaily() {
        var blangko_masuk = $("#txt_jumlah");
        console.log(blangko_masuk);
            if (blangko_masuk.val().length == 0) {                
                  swal("Warning!", "Input Blangko Tidak Boleh Kosong !", "warning");  
                 return false;
            }
            if (parseInt(blangko_masuk.val()) == 0) {                
                  swal("Warning!", "Input Blangko Tidak Boleh 0 !", "warning");  
                 return false;
            }
           
            if (init_kec != 0){
             console.log(init_kec);
             swal("Error!", "Hanya Operator Dinas Saja Yang Boleh Menginput Kendali Blangko", "error"); 
              return false;    
            }
            return true;
       
        }
        function do_save(){
            $.ajax({
                    type: "post",
                    url: BASE_URL+"Blangko/doin_save",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        blangko_masuk : $("#txt_jumlah").val(),
                        tanggal : $("#tanggal").val(),
                        keterangan : $("#txt_ket").val(),
                        user_name : '<?php $user_id = (!empty($user_id)) ? $user_id : '-'; echo $user_id;?>'
                    },
                    beforeSend:
                    function () {
                         block_screen();
                    },
                    success: function (data) {
                        console.log(data);
                        swal("Success!", data.message, "success");
                    },
                    error:
                    function (data) {
                        unblock_screen();
                        swal("Error!", "Ooops!, Please Try Again, Something Went Wrong", "error"); 

                    },
                    complete:
                    function (response) {
                        unblock_screen();
                        on_clear();
                    }
                });
        }
    function block_screen(){
        
        $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff'
            },
            baseZ: 2000
        }); 
    }
    function unblock_screen(){
       $.unblockUI();
    }
    jQuery(document).ready(function() {
            $(".select2").select2();
            $('.input-daterange-datepicker').daterangepicker({
                buttonClasses: ['btn', 'btn-sm'],
                applyClass: 'btn-info',
                cancelClass: 'btn-info',
                locale: {
                    format: 'DD-MM-YYYY'
                },
                startDate: '<?php echo date('d-m-Y');?>',
                endDate: '<?php echo date('d-m-Y');?>'
            });
             $('#mytable').DataTable({
                 "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
        $('#txt_jumlah').val("0");
        $('#txt_ket').val("");
        $('#tanggal').val("<?php echo date('d-m-Y');?>");
        });
    </script>
   