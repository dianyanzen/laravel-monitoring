    <script src="{{ url('/') }}/assets/plugins/bower_components/owl.carousel/owl.carousel.min.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/owl.carousel/owl.custom.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/flot/jquery.flot.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/Minimal-Gauge-chart/js/cmGauge.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/Minimal-Gauge-chart/js/cmGauge.js"></script>
    <!-- Footable -->
    <script src="{{ url('/') }}/assets/plugins/bower_components/footable/js/footable.all.min.js"></script>
    <script src="{{ url('/') }}/assets/js/footable-init.js"></script>
    <script src="{{ url('/') }}/assets/js/chat.js"></script>

    <script>
      
         function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }
        function isphone(evt)
       {
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode != 43 && charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }
        $(document).ready(function () {
         $("#cb_nik_satu").change(function() {
                if(this.checked) {
                    $("#nik_satu").attr("disabled",false);
                    $("#btn_satu").attr("disabled",false);
                }else{
                    $("#nik_satu").attr("disabled",true);
                    $("#nik_satu").val("");
                    $("#btn_satu").attr("disabled",true);
                    clear_nik_satu();
                }
            });
         $("#cb_nik_dua").change(function() {
                if(this.checked) {
                    $("#nik_dua").attr("disabled",false);
                    $("#btn_dua").attr("disabled",false);
                }else{
                    $("#nik_dua").attr("disabled",true);
                    $("#nik_dua").val("");
                    $("#btn_dua").attr("disabled",true);
                    clear_nik_dua();
                }
            });
         $("#cb_nik_tiga").change(function() {
                if(this.checked) {
                    $("#nik_tiga").attr("disabled",false);
                    $("#btn_tiga").attr("disabled",false);
                }else{
                    $("#nik_tiga").attr("disabled",true);
                    $("#nik_tiga").val("");
                    $("#btn_tiga").attr("disabled",true);
                    clear_nik_tiga();
                }
            });
         $("#cb_nik_empat").change(function() {
                if(this.checked) {
                    $("#nik_empat").attr("disabled",false);
                    $("#btn_empat").attr("disabled",false);
                }else{
                    $("#nik_empat").attr("disabled",true);
                    $("#nik_empat").val("");
                    $("#btn_empat").attr("disabled",true);
                    clear_nik_empat();
                }
            });
         $("#cb_nik_lima").change(function() {
                if(this.checked) {
                    $("#nik_lima").attr("disabled",false);
                    $("#btn_lima").attr("disabled",false);
                }else{
                    $("#nik_lima").attr("disabled",true);
                    $("#nik_lima").val("");
                    $("#btn_lima").attr("disabled",true);
                    clear_nik_lima();
                }
            });
         $("#cb_nik_enam").change(function() {
                if(this.checked) {
                    $("#nik_enam").attr("disabled",false);
                    $("#btn_enam").attr("disabled",false);
                }else{
                    $("#nik_enam").attr("disabled",true);
                    $("#nik_enam").val("");
                    $("#btn_enam").attr("disabled",true);
                    clear_nik_enam();
                }
            });
         $("#cb_nik_tujuh").change(function() {
                if(this.checked) {
                    $("#nik_tujuh").attr("disabled",false);
                    $("#btn_tujuh").attr("disabled",false);
                }else{
                    $("#nik_tujuh").attr("disabled",true);
                    $("#nik_tujuh").val("");
                    $("#btn_tujuh").attr("disabled",true);
                    clear_nik_tujuh();
                }
            });
         $("#cb_nik_delapan").change(function() {
                if(this.checked) {
                    $("#nik_delapan").attr("disabled",false);
                    $("#btn_delapan").attr("disabled",false);
                }else{
                    $("#nik_delapan").attr("disabled",true);
                    $("#nik_delapan").val("");
                    $("#btn_delapan").attr("disabled",true);
                    clear_nik_delapan();
                }
            });
        });

        function get_nik_satu(){
            if($("#nik_satu").val().length != 16){
                swal("Warning!", "Nik Tidak Boleh Kosong !", "warning");
            }else{
                $.ajax({
                    type: "post",
                    url: BASE_URL+"api/get_nik_cetak",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        nik : $("#nik_satu").val(),
                    },
                    beforeSend:
                    function () {
                    },
                    success: function (data) {
                        console.log(data);
                        if(Object.keys(data).length > 0){
                            $('#box_satu').show();    
                            $('#txt_nik_satu').html(data[0].nik);    
                            $('#txt_nama_lgkp_satu').html(data[0].nama_lgkp);    
                            $('#txt_tmpt_lhr_satu').html(data[0].tmpt_lhr);    
                            $('#txt_tgl_lhr_satu').html(data[0].tgl_lhr);    
                            $('#txt_jml_satu').html(data[0].count_cetak);    
                            $('#val_no_skts_satu').val(data[0].no_skts);    
                            $('#val_nik_satu').val(data[0].nik); 
                            $('#val_nama_lgkp_satu').val(data[0].nama_lgkp); 
                            $('#val_tmpt_lhr_satu').val(data[0].tmpt_lhr); 
                            $('#val_tgl_lhr_satu').val(data[0].tgl_lhr); 
                            $('#val_jenis_klmin_satu').val(data[0].jenis_klmin); 
                            $('#val_pic_satu').val(data[0].pic); 
                            $('#val_agama_satu').val(data[0].agama); 
                            $('#val_stat_kwn_satu').val(data[0].stat_kwn); 
                            $('#val_gol_drh_satu').val(data[0].gol_drh); 
                            $('#val_pendidikan_satu').val(data[0].pendidikan); 
                            $('#val_pekerjaan_satu').val(data[0].pekerjaan); 
                            $('#val_nama_prop_satu').val(data[0].nama_prop); 
                            $('#val_nama_kab_satu').val(data[0].nama_kab); 
                            $('#val_nama_kec_satu').val(data[0].nama_kec); 
                            $('#val_nama_kel_satu').val(data[0].nama_kel); 
                            $('#val_src_alamat_satu').val(data[0].src_alamat); 
                            $('#val_src_rt_satu').val(data[0].src_rt); 
                            $('#val_src_rw_satu').val(data[0].src_rw); 
                            $('#val_kec_satu').val(data[0].kec); 
                            $('#val_kel_satu').val(data[0].kel); 
                            $('#val_alamat_satu').val(data[0].alamat); 
                            $('#val_no_rt_satu').val(data[0].no_rt); 
                            $('#val_no_rw_satu').val(data[0].no_rw); 
                            $('#val_telepon_satu').val(data[0].telepon); 
                            $('#val_email_satu').val(data[0].email); 
                            $('#val_status_satu').val(data[0].status); 
                            $('#val_tgl_berlaku_satu').val(data[0].tgl_berlaku); 
                            $('#val_jml_satu').val(data[0].count_cetak);   
                            $("#img_satu").attr("src",BASE_URL+data[0].pic);
                        }else{
                            clear_nik_satu();
                            swal("Warning!", "Nik Tidak Ditemukan Atau Tidak Siap Cetak !", "warning");
                        }
                    },
                    error:
                    function (data) {

                    },
                    complete:
                    function (response) {
                        
                    }
                });
            }
            
        }

        function clear_nik_satu(){
            $('#box_satu').hide();
            $('#txt_nik_satu').html('-');    
            $('#txt_nama_lgkp_satu').html('-');    
            $('#txt_tmpt_lhr_satu').html('-');    
            $('#txt_tgl_lhr_satu').html('-');    
            $('#txt_jml_satu').html('-');
            $('#val_no_skts_satu').val('');    
            $('#val_nik_satu').val(''); 
            $('#val_nama_lgkp_satu').val(''); 
            $('#val_tmpt_lhr_satu').val(''); 
            $('#val_tgl_lhr_satu').val(''); 
            $('#val_jenis_klmin_satu').val(''); 
            $('#val_pic_satu').val(''); 
            $('#val_agama_satu').val(''); 
            $('#val_stat_kwn_satu').val(''); 
            $('#val_gol_drh_satu').val(''); 
            $('#val_pendidikan_satu').val(''); 
            $('#val_pekerjaan_satu').val(''); 
            $('#val_nama_prop_satu').val(''); 
            $('#val_nama_kab_satu').val(''); 
            $('#val_nama_kec_satu').val(''); 
            $('#val_nama_kel_satu').val(''); 
            $('#val_src_alamat_satu').val(''); 
            $('#val_src_rt_satu').val(''); 
            $('#val_src_rw_satu').val(''); 
            $('#val_kec_satu').val(''); 
            $('#val_kel_satu').val(''); 
            $('#val_alamat_satu').val(''); 
            $('#val_no_rt_satu').val(''); 
            $('#val_no_rw_satu').val(''); 
            $('#val_telepon_satu').val(''); 
            $('#val_email_satu').val(''); 
            $('#val_status_satu').val(''); 
            $('#val_tgl_berlaku_satu').val(''); 
            $('#val_jml_satu').val('');   
            $("#img_satu").attr("src",BASE_URL+'/assets/plugins/images/pemkot.png');
        }

        function get_nik_dua(){
            if($("#nik_dua").val().length != 16){
                swal("Warning!", "Nik Tidak Boleh Kosong !", "warning");
            }else{
                $.ajax({
                    type: "post",
                    url: BASE_URL+"api/get_nik_cetak",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        nik : $("#nik_dua").val(),
                    },
                    beforeSend:
                    function () {
                    },
                    success: function (data) {
                        console.log(data);
                        if(Object.keys(data).length > 0){
                            $('#box_dua').show();    
                            $('#txt_nik_dua').html(data[0].nik);    
                            $('#txt_nama_lgkp_dua').html(data[0].nama_lgkp);    
                            $('#txt_tmpt_lhr_dua').html(data[0].tmpt_lhr);    
                            $('#txt_tgl_lhr_dua').html(data[0].tgl_lhr);    
                            $('#txt_jml_dua').html(data[0].count_cetak);    
                            $('#val_no_skts_dua').val(data[0].no_skts);    
                            $('#val_nik_dua').val(data[0].nik); 
                            $('#val_nama_lgkp_dua').val(data[0].nama_lgkp); 
                            $('#val_tmpt_lhr_dua').val(data[0].tmpt_lhr); 
                            $('#val_tgl_lhr_dua').val(data[0].tgl_lhr); 
                            $('#val_jenis_klmin_dua').val(data[0].jenis_klmin); 
                            $('#val_pic_dua').val(data[0].pic); 
                            $('#val_agama_dua').val(data[0].agama); 
                            $('#val_stat_kwn_dua').val(data[0].stat_kwn); 
                            $('#val_gol_drh_dua').val(data[0].gol_drh); 
                            $('#val_pendidikan_dua').val(data[0].pendidikan); 
                            $('#val_pekerjaan_dua').val(data[0].pekerjaan); 
                            $('#val_nama_prop_dua').val(data[0].nama_prop); 
                            $('#val_nama_kab_dua').val(data[0].nama_kab); 
                            $('#val_nama_kec_dua').val(data[0].nama_kec); 
                            $('#val_nama_kel_dua').val(data[0].nama_kel); 
                            $('#val_src_alamat_dua').val(data[0].src_alamat); 
                            $('#val_src_rt_dua').val(data[0].src_rt); 
                            $('#val_src_rw_dua').val(data[0].src_rw); 
                            $('#val_kec_dua').val(data[0].kec); 
                            $('#val_kel_dua').val(data[0].kel); 
                            $('#val_alamat_dua').val(data[0].alamat); 
                            $('#val_no_rt_dua').val(data[0].no_rt); 
                            $('#val_no_rw_dua').val(data[0].no_rw); 
                            $('#val_telepon_dua').val(data[0].telepon); 
                            $('#val_email_dua').val(data[0].email); 
                            $('#val_status_dua').val(data[0].status); 
                            $('#val_tgl_berlaku_dua').val(data[0].tgl_berlaku); 
                            $('#val_jml_dua').val(data[0].count_cetak);   
                            $("#img_dua").attr("src",BASE_URL+data[0].pic);
                        }else{
                            clear_nik_dua();
                            swal("Warning!", "Nik Tidak Ditemukan Atau Tidak Siap Cetak !", "warning");
                        }
                    },
                    error:
                    function (data) {

                    },
                    complete:
                    function (response) {
                        
                    }
                });
            }
            
        }

        function clear_nik_dua(){
            $('#box_dua').hide();
            $('#txt_nik_dua').html('-');    
            $('#txt_nama_lgkp_dua').html('-');    
            $('#txt_tmpt_lhr_dua').html('-');    
            $('#txt_tgl_lhr_dua').html('-');    
            $('#txt_jml_dua').html('-');
            $('#val_no_skts_dua').val('');    
            $('#val_nik_dua').val(''); 
            $('#val_nama_lgkp_dua').val(''); 
            $('#val_tmpt_lhr_dua').val(''); 
            $('#val_tgl_lhr_dua').val(''); 
            $('#val_jenis_klmin_dua').val(''); 
            $('#val_pic_dua').val(''); 
            $('#val_agama_dua').val(''); 
            $('#val_stat_kwn_dua').val(''); 
            $('#val_gol_drh_dua').val(''); 
            $('#val_pendidikan_dua').val(''); 
            $('#val_pekerjaan_dua').val(''); 
            $('#val_nama_prop_dua').val(''); 
            $('#val_nama_kab_dua').val(''); 
            $('#val_nama_kec_dua').val(''); 
            $('#val_nama_kel_dua').val(''); 
            $('#val_src_alamat_dua').val(''); 
            $('#val_src_rt_dua').val(''); 
            $('#val_src_rw_dua').val(''); 
            $('#val_kec_dua').val(''); 
            $('#val_kel_dua').val(''); 
            $('#val_alamat_dua').val(''); 
            $('#val_no_rt_dua').val(''); 
            $('#val_no_rw_dua').val(''); 
            $('#val_telepon_dua').val(''); 
            $('#val_email_dua').val(''); 
            $('#val_status_dua').val(''); 
            $('#val_tgl_berlaku_dua').val(''); 
            $('#val_jml_dua').val('');   
            $("#img_dua").attr("src",BASE_URL+'/assets/plugins/images/pemkot.png');
        }

        function get_nik_tiga(){
            if($("#nik_tiga").val().length != 16){
                swal("Warning!", "ID Pengajuan Tidak Boleh Kosong !", "warning");
            }else{
                $.ajax({
                    type: "post",
                    url: BASE_URL+"api/get_nik_cetak",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        nik : $("#nik_tiga").val(),
                    },
                    beforeSend:
                    function () {
                    },
                    success: function (data) {
                        console.log(data);
                        if(Object.keys(data).length > 0){
                            $('#box_tiga').show();    
                            $('#txt_nik_tiga').html(data[0].nik);    
                            $('#txt_nama_lgkp_tiga').html(data[0].nama_lgkp);    
                            $('#txt_tmpt_lhr_tiga').html(data[0].tmpt_lhr);    
                            $('#txt_tgl_lhr_tiga').html(data[0].tgl_lhr);    
                            $('#txt_jml_tiga').html(data[0].count_cetak);    
                            $('#val_no_skts_tiga').val(data[0].no_skts);    
                            $('#val_nik_tiga').val(data[0].nik); 
                            $('#val_nama_lgkp_tiga').val(data[0].nama_lgkp); 
                            $('#val_tmpt_lhr_tiga').val(data[0].tmpt_lhr); 
                            $('#val_tgl_lhr_tiga').val(data[0].tgl_lhr); 
                            $('#val_jenis_klmin_tiga').val(data[0].jenis_klmin); 
                            $('#val_pic_tiga').val(data[0].pic); 
                            $('#val_agama_tiga').val(data[0].agama); 
                            $('#val_stat_kwn_tiga').val(data[0].stat_kwn); 
                            $('#val_gol_drh_tiga').val(data[0].gol_drh); 
                            $('#val_pendidikan_tiga').val(data[0].pendidikan); 
                            $('#val_pekerjaan_tiga').val(data[0].pekerjaan); 
                            $('#val_nama_prop_tiga').val(data[0].nama_prop); 
                            $('#val_nama_kab_tiga').val(data[0].nama_kab); 
                            $('#val_nama_kec_tiga').val(data[0].nama_kec); 
                            $('#val_nama_kel_tiga').val(data[0].nama_kel); 
                            $('#val_src_alamat_tiga').val(data[0].src_alamat); 
                            $('#val_src_rt_tiga').val(data[0].src_rt); 
                            $('#val_src_rw_tiga').val(data[0].src_rw); 
                            $('#val_kec_tiga').val(data[0].kec); 
                            $('#val_kel_tiga').val(data[0].kel); 
                            $('#val_alamat_tiga').val(data[0].alamat); 
                            $('#val_no_rt_tiga').val(data[0].no_rt); 
                            $('#val_no_rw_tiga').val(data[0].no_rw); 
                            $('#val_telepon_tiga').val(data[0].telepon); 
                            $('#val_email_tiga').val(data[0].email); 
                            $('#val_status_tiga').val(data[0].status); 
                            $('#val_tgl_berlaku_tiga').val(data[0].tgl_berlaku); 
                            $('#val_jml_tiga').val(data[0].nik);   
                            $("#img_tiga").attr("src",BASE_URL+data[0].pic);
                        }else{
                            clear_nik_tiga();
                            swal("Warning!", "Nik Tidak Ditemukan Atau Tidak Siap Cetak !", "warning");
                        }
                    },
                    error:
                    function (data) {

                    },
                    complete:
                    function (response) {
                        
                    }
                });
            }
            
        }

        function clear_nik_tiga(){
            $('#box_tiga').hide();
            $('#txt_nik_tiga').html('-');    
            $('#txt_nama_lgkp_tiga').html('-');    
            $('#txt_tmpt_lhr_tiga').html('-');    
            $('#txt_tgl_lhr_tiga').html('-');    
            $('#txt_jml_tiga').html('-');
            $('#val_no_skts_tiga').val('');    
            $('#val_nik_tiga').val(''); 
            $('#val_nama_lgkp_tiga').val(''); 
            $('#val_tmpt_lhr_tiga').val(''); 
            $('#val_tgl_lhr_tiga').val(''); 
            $('#val_jenis_klmin_tiga').val(''); 
            $('#val_pic_tiga').val(''); 
            $('#val_agama_tiga').val(''); 
            $('#val_stat_kwn_tiga').val(''); 
            $('#val_gol_drh_tiga').val(''); 
            $('#val_pendidikan_tiga').val(''); 
            $('#val_pekerjaan_tiga').val(''); 
            $('#val_nama_prop_tiga').val(''); 
            $('#val_nama_kab_tiga').val(''); 
            $('#val_nama_kec_tiga').val(''); 
            $('#val_nama_kel_tiga').val(''); 
            $('#val_src_alamat_tiga').val(''); 
            $('#val_src_rt_tiga').val(''); 
            $('#val_src_rw_tiga').val(''); 
            $('#val_kec_tiga').val(''); 
            $('#val_kel_tiga').val(''); 
            $('#val_alamat_tiga').val(''); 
            $('#val_no_rt_tiga').val(''); 
            $('#val_no_rw_tiga').val(''); 
            $('#val_telepon_tiga').val(''); 
            $('#val_email_tiga').val(''); 
            $('#val_status_tiga').val(''); 
            $('#val_tgl_berlaku_tiga').val(''); 
            $('#val_jml_tiga').val('');   
            $("#img_tiga").attr("src",BASE_URL+'/assets/plugins/images/pemkot.png');
        }

        function get_nik_empat(){
            if($("#nik_empat").val().length != 16){
                swal("Warning!", "ID Pengajuan Tidak Boleh Kosong !", "warning");
            }else{
                $.ajax({
                    type: "post",
                    url: BASE_URL+"api/get_nik_cetak",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        nik : $("#nik_empat").val(),
                    },
                    beforeSend:
                    function () {
                    },
                    success: function (data) {
                        console.log(data);
                        if(Object.keys(data).length > 0){
                            $('#box_empat').show();    
                            $('#txt_nik_empat').html(data[0].nik);    
                            $('#txt_nama_lgkp_empat').html(data[0].nama_lgkp);    
                            $('#txt_tmpt_lhr_empat').html(data[0].tmpt_lhr);    
                            $('#txt_tgl_lhr_empat').html(data[0].tgl_lhr);    
                            $('#txt_jml_empat').html(data[0].count_cetak);    
                            $('#val_no_skts_empat').val(data[0].no_skts);    
                            $('#val_nik_empat').val(data[0].nik); 
                            $('#val_nama_lgkp_empat').val(data[0].nama_lgkp); 
                            $('#val_tmpt_lhr_empat').val(data[0].tmpt_lhr); 
                            $('#val_tgl_lhr_empat').val(data[0].tgl_lhr); 
                            $('#val_jenis_klmin_empat').val(data[0].jenis_klmin); 
                            $('#val_pic_empat').val(data[0].pic); 
                            $('#val_agama_empat').val(data[0].agama); 
                            $('#val_stat_kwn_empat').val(data[0].stat_kwn); 
                            $('#val_gol_drh_empat').val(data[0].gol_drh); 
                            $('#val_pendidikan_empat').val(data[0].pendidikan); 
                            $('#val_pekerjaan_empat').val(data[0].pekerjaan); 
                            $('#val_nama_prop_empat').val(data[0].nama_prop); 
                            $('#val_nama_kab_empat').val(data[0].nama_kab); 
                            $('#val_nama_kec_empat').val(data[0].nama_kec); 
                            $('#val_nama_kel_empat').val(data[0].nama_kel); 
                            $('#val_src_alamat_empat').val(data[0].src_alamat); 
                            $('#val_src_rt_empat').val(data[0].src_rt); 
                            $('#val_src_rw_empat').val(data[0].src_rw); 
                            $('#val_kec_empat').val(data[0].kec); 
                            $('#val_kel_empat').val(data[0].kel); 
                            $('#val_alamat_empat').val(data[0].alamat); 
                            $('#val_no_rt_empat').val(data[0].no_rt); 
                            $('#val_no_rw_empat').val(data[0].no_rw); 
                            $('#val_telepon_empat').val(data[0].telepon); 
                            $('#val_email_empat').val(data[0].email); 
                            $('#val_status_empat').val(data[0].status); 
                            $('#val_tgl_berlaku_empat').val(data[0].tgl_berlaku); 
                            $('#val_jml_empat').val(data[0].nik);   
                            $("#img_empat").attr("src",BASE_URL+data[0].pic);
                        }else{
                            clear_nik_empat();
                            swal("Warning!", "Nik Tidak Ditemukan Atau Tidak Siap Cetak !", "warning");
                        }
                    },
                    error:
                    function (data) {

                    },
                    complete:
                    function (response) {
                        
                    }
                });
            }
            
        }

        function clear_nik_empat(){
            $('#box_empat').hide();
            $('#txt_nik_empat').html('-');    
            $('#txt_nama_lgkp_empat').html('-');    
            $('#txt_tmpt_lhr_empat').html('-');    
            $('#txt_tgl_lhr_empat').html('-');    
            $('#txt_jml_empat').html('-');
            $('#val_no_skts_empat').val('');    
            $('#val_nik_empat').val(''); 
            $('#val_nama_lgkp_empat').val(''); 
            $('#val_tmpt_lhr_empat').val(''); 
            $('#val_tgl_lhr_empat').val(''); 
            $('#val_jenis_klmin_empat').val(''); 
            $('#val_pic_empat').val(''); 
            $('#val_agama_empat').val(''); 
            $('#val_stat_kwn_empat').val(''); 
            $('#val_gol_drh_empat').val(''); 
            $('#val_pendidikan_empat').val(''); 
            $('#val_pekerjaan_empat').val(''); 
            $('#val_nama_prop_empat').val(''); 
            $('#val_nama_kab_empat').val(''); 
            $('#val_nama_kec_empat').val(''); 
            $('#val_nama_kel_empat').val(''); 
            $('#val_src_alamat_empat').val(''); 
            $('#val_src_rt_empat').val(''); 
            $('#val_src_rw_empat').val(''); 
            $('#val_kec_empat').val(''); 
            $('#val_kel_empat').val(''); 
            $('#val_alamat_empat').val(''); 
            $('#val_no_rt_empat').val(''); 
            $('#val_no_rw_empat').val(''); 
            $('#val_telepon_empat').val(''); 
            $('#val_email_empat').val(''); 
            $('#val_status_empat').val(''); 
            $('#val_tgl_berlaku_empat').val(''); 
            $('#val_jml_empat').val('');   
            $("#img_empat").attr("src",BASE_URL+'/assets/plugins/images/pemkot.png');
        }

        function get_nik_lima(){
            if($("#nik_lima").val().length != 16){
                swal("Warning!", "ID Pengajuan Tidak Boleh Kosong !", "warning");
            }else{
                $.ajax({
                    type: "post",
                    url: BASE_URL+"api/get_nik_cetak",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        nik : $("#nik_lima").val(),
                    },
                    beforeSend:
                    function () {
                    },
                    success: function (data) {
                        console.log(data);
                        if(Object.keys(data).length > 0){
                            $('#box_lima').show();    
                            $('#txt_nik_lima').html(data[0].nik);    
                            $('#txt_nama_lgkp_lima').html(data[0].nama_lgkp);    
                            $('#txt_tmpt_lhr_lima').html(data[0].tmpt_lhr);    
                            $('#txt_tgl_lhr_lima').html(data[0].tgl_lhr);    
                            $('#txt_jml_lima').html(data[0].count_cetak);    
                            $('#val_no_skts_lima').val(data[0].no_skts);    
                            $('#val_nik_lima').val(data[0].nik); 
                            $('#val_nama_lgkp_lima').val(data[0].nama_lgkp); 
                            $('#val_tmpt_lhr_lima').val(data[0].tmpt_lhr); 
                            $('#val_tgl_lhr_lima').val(data[0].tgl_lhr); 
                            $('#val_jenis_klmin_lima').val(data[0].jenis_klmin); 
                            $('#val_pic_lima').val(data[0].pic); 
                            $('#val_agama_lima').val(data[0].agama); 
                            $('#val_stat_kwn_lima').val(data[0].stat_kwn); 
                            $('#val_gol_drh_lima').val(data[0].gol_drh); 
                            $('#val_pendidikan_lima').val(data[0].pendidikan); 
                            $('#val_pekerjaan_lima').val(data[0].pekerjaan); 
                            $('#val_nama_prop_lima').val(data[0].nama_prop); 
                            $('#val_nama_kab_lima').val(data[0].nama_kab); 
                            $('#val_nama_kec_lima').val(data[0].nama_kec); 
                            $('#val_nama_kel_lima').val(data[0].nama_kel); 
                            $('#val_src_alamat_lima').val(data[0].src_alamat); 
                            $('#val_src_rt_lima').val(data[0].src_rt); 
                            $('#val_src_rw_lima').val(data[0].src_rw); 
                            $('#val_kec_lima').val(data[0].kec); 
                            $('#val_kel_lima').val(data[0].kel); 
                            $('#val_alamat_lima').val(data[0].alamat); 
                            $('#val_no_rt_lima').val(data[0].no_rt); 
                            $('#val_no_rw_lima').val(data[0].no_rw); 
                            $('#val_telepon_lima').val(data[0].telepon); 
                            $('#val_email_lima').val(data[0].email); 
                            $('#val_status_lima').val(data[0].status); 
                            $('#val_tgl_berlaku_lima').val(data[0].tgl_berlaku); 
                            $('#val_jml_lima').val(data[0].nik);   
                            $("#img_lima").attr("src",BASE_URL+data[0].pic);
                        }else{
                            clear_nik_lima();
                            swal("Warning!", "Nik Tidak Ditemukan Atau Tidak Siap Cetak !", "warning");
                        }
                    },
                    error:
                    function (data) {

                    },
                    complete:
                    function (response) {
                        
                    }
                });
            }
            
        }

        function clear_nik_lima(){
            $('#box_lima').hide();
            $('#txt_nik_lima').html('-');    
            $('#txt_nama_lgkp_lima').html('-');    
            $('#txt_tmpt_lhr_lima').html('-');    
            $('#txt_tgl_lhr_lima').html('-');    
            $('#txt_jml_lima').html('-');
            $('#val_no_skts_lima').val('');    
            $('#val_nik_lima').val(''); 
            $('#val_nama_lgkp_lima').val(''); 
            $('#val_tmpt_lhr_lima').val(''); 
            $('#val_tgl_lhr_lima').val(''); 
            $('#val_jenis_klmin_lima').val(''); 
            $('#val_pic_lima').val(''); 
            $('#val_agama_lima').val(''); 
            $('#val_stat_kwn_lima').val(''); 
            $('#val_gol_drh_lima').val(''); 
            $('#val_pendidikan_lima').val(''); 
            $('#val_pekerjaan_lima').val(''); 
            $('#val_nama_prop_lima').val(''); 
            $('#val_nama_kab_lima').val(''); 
            $('#val_nama_kec_lima').val(''); 
            $('#val_nama_kel_lima').val(''); 
            $('#val_src_alamat_lima').val(''); 
            $('#val_src_rt_lima').val(''); 
            $('#val_src_rw_lima').val(''); 
            $('#val_kec_lima').val(''); 
            $('#val_kel_lima').val(''); 
            $('#val_alamat_lima').val(''); 
            $('#val_no_rt_lima').val(''); 
            $('#val_no_rw_lima').val(''); 
            $('#val_telepon_lima').val(''); 
            $('#val_email_lima').val(''); 
            $('#val_status_lima').val(''); 
            $('#val_tgl_berlaku_lima').val(''); 
            $('#val_jml_lima').val('');   
            $("#img_lima").attr("src",BASE_URL+'/assets/plugins/images/pemkot.png');
        }

        function get_nik_enam(){
            if($("#nik_enam").val().length != 16){
                swal("Warning!", "ID Pengajuan Tidak Boleh Kosong !", "warning");
            }else{
                $.ajax({
                    type: "post",
                    url: BASE_URL+"api/get_nik_cetak",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        nik : $("#nik_enam").val(),
                    },
                    beforeSend:
                    function () {
                    },
                    success: function (data) {
                        console.log(data);
                        if(Object.keys(data).length > 0){
                            $('#box_enam').show();    
                            $('#txt_nik_enam').html(data[0].nik);    
                            $('#txt_nama_lgkp_enam').html(data[0].nama_lgkp);    
                            $('#txt_tmpt_lhr_enam').html(data[0].tmpt_lhr);    
                            $('#txt_tgl_lhr_enam').html(data[0].tgl_lhr);    
                            $('#txt_jml_enam').html(data[0].count_cetak);    
                            $('#val_no_skts_enam').val(data[0].no_skts);    
                            $('#val_nik_enam').val(data[0].nik); 
                            $('#val_nama_lgkp_enam').val(data[0].nama_lgkp); 
                            $('#val_tmpt_lhr_enam').val(data[0].tmpt_lhr); 
                            $('#val_tgl_lhr_enam').val(data[0].tgl_lhr); 
                            $('#val_jenis_klmin_enam').val(data[0].jenis_klmin); 
                            $('#val_pic_enam').val(data[0].pic); 
                            $('#val_agama_enam').val(data[0].agama); 
                            $('#val_stat_kwn_enam').val(data[0].stat_kwn); 
                            $('#val_gol_drh_enam').val(data[0].gol_drh); 
                            $('#val_pendidikan_enam').val(data[0].pendidikan); 
                            $('#val_pekerjaan_enam').val(data[0].pekerjaan); 
                            $('#val_nama_prop_enam').val(data[0].nama_prop); 
                            $('#val_nama_kab_enam').val(data[0].nama_kab); 
                            $('#val_nama_kec_enam').val(data[0].nama_kec); 
                            $('#val_nama_kel_enam').val(data[0].nama_kel); 
                            $('#val_src_alamat_enam').val(data[0].src_alamat); 
                            $('#val_src_rt_enam').val(data[0].src_rt); 
                            $('#val_src_rw_enam').val(data[0].src_rw); 
                            $('#val_kec_enam').val(data[0].kec); 
                            $('#val_kel_enam').val(data[0].kel); 
                            $('#val_alamat_enam').val(data[0].alamat); 
                            $('#val_no_rt_enam').val(data[0].no_rt); 
                            $('#val_no_rw_enam').val(data[0].no_rw); 
                            $('#val_telepon_enam').val(data[0].telepon); 
                            $('#val_email_enam').val(data[0].email); 
                            $('#val_status_enam').val(data[0].status); 
                            $('#val_tgl_berlaku_enam').val(data[0].tgl_berlaku); 
                            $('#val_jml_enam').val(data[0].nik);   
                            $("#img_enam").attr("src",BASE_URL+data[0].pic);
                        }else{
                            clear_nik_enam();
                            swal("Warning!", "Nik Tidak Ditemukan Atau Tidak Siap Cetak !", "warning");
                        }
                    },
                    error:
                    function (data) {

                    },
                    complete:
                    function (response) {
                        
                    }
                });
            }
            
        }

        function clear_nik_enam(){
            $('#box_enam').hide();
            $('#txt_nik_enam').html('-');    
            $('#txt_nama_lgkp_enam').html('-');    
            $('#txt_tmpt_lhr_enam').html('-');    
            $('#txt_tgl_lhr_enam').html('-');    
            $('#txt_jml_enam').html('-');
            $('#val_no_skts_enam').val('');    
            $('#val_nik_enam').val(''); 
            $('#val_nama_lgkp_enam').val(''); 
            $('#val_tmpt_lhr_enam').val(''); 
            $('#val_tgl_lhr_enam').val(''); 
            $('#val_jenis_klmin_enam').val(''); 
            $('#val_pic_enam').val(''); 
            $('#val_agama_enam').val(''); 
            $('#val_stat_kwn_enam').val(''); 
            $('#val_gol_drh_enam').val(''); 
            $('#val_pendidikan_enam').val(''); 
            $('#val_pekerjaan_enam').val(''); 
            $('#val_nama_prop_enam').val(''); 
            $('#val_nama_kab_enam').val(''); 
            $('#val_nama_kec_enam').val(''); 
            $('#val_nama_kel_enam').val(''); 
            $('#val_src_alamat_enam').val(''); 
            $('#val_src_rt_enam').val(''); 
            $('#val_src_rw_enam').val(''); 
            $('#val_kec_enam').val(''); 
            $('#val_kel_enam').val(''); 
            $('#val_alamat_enam').val(''); 
            $('#val_no_rt_enam').val(''); 
            $('#val_no_rw_enam').val(''); 
            $('#val_telepon_enam').val(''); 
            $('#val_email_enam').val(''); 
            $('#val_status_enam').val(''); 
            $('#val_tgl_berlaku_enam').val(''); 
            $('#val_jml_enam').val('');   
            $("#img_enam").attr("src",BASE_URL+'/assets/plugins/images/pemkot.png');
        }

        function get_nik_tujuh(){
            if($("#nik_tujuh").val().length != 16){
                swal("Warning!", "ID Pengajuan Tidak Boleh Kosong !", "warning");
            }else{
                $.ajax({
                    type: "post",
                    url: BASE_URL+"api/get_nik_cetak",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        nik : $("#nik_tujuh").val(),
                    },
                    beforeSend:
                    function () {
                    },
                    success: function (data) {
                        console.log(data);
                        if(Object.keys(data).length > 0){
                            $('#box_tujuh').show();    
                            $('#txt_nik_tujuh').html(data[0].nik);    
                            $('#txt_nama_lgkp_tujuh').html(data[0].nama_lgkp);    
                            $('#txt_tmpt_lhr_tujuh').html(data[0].tmpt_lhr);    
                            $('#txt_tgl_lhr_tujuh').html(data[0].tgl_lhr);    
                            $('#txt_jml_tujuh').html(data[0].count_cetak);    
                            $('#val_no_skts_tujuh').val(data[0].no_skts);    
                            $('#val_nik_tujuh').val(data[0].nik); 
                            $('#val_nama_lgkp_tujuh').val(data[0].nama_lgkp); 
                            $('#val_tmpt_lhr_tujuh').val(data[0].tmpt_lhr); 
                            $('#val_tgl_lhr_tujuh').val(data[0].tgl_lhr); 
                            $('#val_jenis_klmin_tujuh').val(data[0].jenis_klmin); 
                            $('#val_pic_tujuh').val(data[0].pic); 
                            $('#val_agama_tujuh').val(data[0].agama); 
                            $('#val_stat_kwn_tujuh').val(data[0].stat_kwn); 
                            $('#val_gol_drh_tujuh').val(data[0].gol_drh); 
                            $('#val_pendidikan_tujuh').val(data[0].pendidikan); 
                            $('#val_pekerjaan_tujuh').val(data[0].pekerjaan); 
                            $('#val_nama_prop_tujuh').val(data[0].nama_prop); 
                            $('#val_nama_kab_tujuh').val(data[0].nama_kab); 
                            $('#val_nama_kec_tujuh').val(data[0].nama_kec); 
                            $('#val_nama_kel_tujuh').val(data[0].nama_kel); 
                            $('#val_src_alamat_tujuh').val(data[0].src_alamat); 
                            $('#val_src_rt_tujuh').val(data[0].src_rt); 
                            $('#val_src_rw_tujuh').val(data[0].src_rw); 
                            $('#val_kec_tujuh').val(data[0].kec); 
                            $('#val_kel_tujuh').val(data[0].kel); 
                            $('#val_alamat_tujuh').val(data[0].alamat); 
                            $('#val_no_rt_tujuh').val(data[0].no_rt); 
                            $('#val_no_rw_tujuh').val(data[0].no_rw); 
                            $('#val_telepon_tujuh').val(data[0].telepon); 
                            $('#val_email_tujuh').val(data[0].email); 
                            $('#val_status_tujuh').val(data[0].status); 
                            $('#val_tgl_berlaku_tujuh').val(data[0].tgl_berlaku); 
                            $('#val_jml_tujuh').val(data[0].nik);   
                            $("#img_tujuh").attr("src",BASE_URL+data[0].pic);
                        }else{
                            clear_nik_tujuh();
                            swal("Warning!", "Nik Tidak Ditemukan Atau Tidak Siap Cetak !", "warning");
                        }
                    },
                    error:
                    function (data) {

                    },
                    complete:
                    function (response) {
                        
                    }
                });
            }
            
        }

        function clear_nik_tujuh(){
            $('#box_tujuh').hide();
            $('#txt_nik_tujuh').html('-');    
            $('#txt_nama_lgkp_tujuh').html('-');    
            $('#txt_tmpt_lhr_tujuh').html('-');    
            $('#txt_tgl_lhr_tujuh').html('-');    
            $('#txt_jml_tujuh').html('-');
            $('#val_no_skts_tujuh').val('');    
            $('#val_nik_tujuh').val(''); 
            $('#val_nama_lgkp_tujuh').val(''); 
            $('#val_tmpt_lhr_tujuh').val(''); 
            $('#val_tgl_lhr_tujuh').val(''); 
            $('#val_jenis_klmin_tujuh').val(''); 
            $('#val_pic_tujuh').val(''); 
            $('#val_agama_tujuh').val(''); 
            $('#val_stat_kwn_tujuh').val(''); 
            $('#val_gol_drh_tujuh').val(''); 
            $('#val_pendidikan_tujuh').val(''); 
            $('#val_pekerjaan_tujuh').val(''); 
            $('#val_nama_prop_tujuh').val(''); 
            $('#val_nama_kab_tujuh').val(''); 
            $('#val_nama_kec_tujuh').val(''); 
            $('#val_nama_kel_tujuh').val(''); 
            $('#val_src_alamat_tujuh').val(''); 
            $('#val_src_rt_tujuh').val(''); 
            $('#val_src_rw_tujuh').val(''); 
            $('#val_kec_tujuh').val(''); 
            $('#val_kel_tujuh').val(''); 
            $('#val_alamat_tujuh').val(''); 
            $('#val_no_rt_tujuh').val(''); 
            $('#val_no_rw_tujuh').val(''); 
            $('#val_telepon_tujuh').val(''); 
            $('#val_email_tujuh').val(''); 
            $('#val_status_tujuh').val(''); 
            $('#val_tgl_berlaku_tujuh').val(''); 
            $('#val_jml_tujuh').val('');   
            $("#img_tujuh").attr("src",BASE_URL+'/assets/plugins/images/pemkot.png');
        }

        function get_nik_delapan(){
            if($("#nik_delapan").val().length != 16){
                swal("Warning!", "ID Pengajuan Tidak Boleh Kosong !", "warning");
            }else{
                $.ajax({
                    type: "post",
                    url: BASE_URL+"api/get_nik_cetak",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        nik : $("#nik_delapan").val(),
                    },
                    beforeSend:
                    function () {
                    },
                    success: function (data) {
                        console.log(data);
                        if(Object.keys(data).length > 0){
                            $('#box_delapan').show();    
                            $('#txt_nik_delapan').html(data[0].nik);    
                            $('#txt_nama_lgkp_delapan').html(data[0].nama_lgkp);    
                            $('#txt_tmpt_lhr_delapan').html(data[0].tmpt_lhr);    
                            $('#txt_tgl_lhr_delapan').html(data[0].tgl_lhr);    
                            $('#txt_jml_delapan').html(data[0].count_cetak);    
                            $('#val_no_skts_delapan').val(data[0].no_skts);    
                            $('#val_nik_delapan').val(data[0].nik); 
                            $('#val_nama_lgkp_delapan').val(data[0].nama_lgkp); 
                            $('#val_tmpt_lhr_delapan').val(data[0].tmpt_lhr); 
                            $('#val_tgl_lhr_delapan').val(data[0].tgl_lhr); 
                            $('#val_jenis_klmin_delapan').val(data[0].jenis_klmin); 
                            $('#val_pic_delapan').val(data[0].pic); 
                            $('#val_agama_delapan').val(data[0].agama); 
                            $('#val_stat_kwn_delapan').val(data[0].stat_kwn); 
                            $('#val_gol_drh_delapan').val(data[0].gol_drh); 
                            $('#val_pendidikan_delapan').val(data[0].pendidikan); 
                            $('#val_pekerjaan_delapan').val(data[0].pekerjaan); 
                            $('#val_nama_prop_delapan').val(data[0].nama_prop); 
                            $('#val_nama_kab_delapan').val(data[0].nama_kab); 
                            $('#val_nama_kec_delapan').val(data[0].nama_kec); 
                            $('#val_nama_kel_delapan').val(data[0].nama_kel); 
                            $('#val_src_alamat_delapan').val(data[0].src_alamat); 
                            $('#val_src_rt_delapan').val(data[0].src_rt); 
                            $('#val_src_rw_delapan').val(data[0].src_rw); 
                            $('#val_kec_delapan').val(data[0].kec); 
                            $('#val_kel_delapan').val(data[0].kel); 
                            $('#val_alamat_delapan').val(data[0].alamat); 
                            $('#val_no_rt_delapan').val(data[0].no_rt); 
                            $('#val_no_rw_delapan').val(data[0].no_rw); 
                            $('#val_telepon_delapan').val(data[0].telepon); 
                            $('#val_email_delapan').val(data[0].email); 
                            $('#val_status_delapan').val(data[0].status); 
                            $('#val_tgl_berlaku_delapan').val(data[0].tgl_berlaku); 
                            $('#val_jml_delapan').val(data[0].nik);   
                            $("#img_delapan").attr("src",BASE_URL+data[0].pic);
                        }else{
                            clear_nik_delapan();
                            swal("Warning!", "Nik Tidak Ditemukan Atau Tidak Siap Cetak !", "warning");
                        }
                    },
                    error:
                    function (data) {

                    },
                    complete:
                    function (response) {
                        
                    }
                });
            }
            
        }

        function clear_nik_delapan(){
            $('#box_delapan').hide();
            $('#txt_nik_delapan').html('-');    
            $('#txt_nama_lgkp_delapan').html('-');    
            $('#txt_tmpt_lhr_delapan').html('-');    
            $('#txt_tgl_lhr_delapan').html('-');    
            $('#txt_jml_delapan').html('-');
            $('#val_no_skts_delapan').val('');    
            $('#val_nik_delapan').val(''); 
            $('#val_nama_lgkp_delapan').val(''); 
            $('#val_tmpt_lhr_delapan').val(''); 
            $('#val_tgl_lhr_delapan').val(''); 
            $('#val_jenis_klmin_delapan').val(''); 
            $('#val_pic_delapan').val(''); 
            $('#val_agama_delapan').val(''); 
            $('#val_stat_kwn_delapan').val(''); 
            $('#val_gol_drh_delapan').val(''); 
            $('#val_pendidikan_delapan').val(''); 
            $('#val_pekerjaan_delapan').val(''); 
            $('#val_nama_prop_delapan').val(''); 
            $('#val_nama_kab_delapan').val(''); 
            $('#val_nama_kec_delapan').val(''); 
            $('#val_nama_kel_delapan').val(''); 
            $('#val_src_alamat_delapan').val(''); 
            $('#val_src_rt_delapan').val(''); 
            $('#val_src_rw_delapan').val(''); 
            $('#val_kec_delapan').val(''); 
            $('#val_kel_delapan').val(''); 
            $('#val_alamat_delapan').val(''); 
            $('#val_no_rt_delapan').val(''); 
            $('#val_no_rw_delapan').val(''); 
            $('#val_telepon_delapan').val(''); 
            $('#val_email_delapan').val(''); 
            $('#val_status_delapan').val(''); 
            $('#val_tgl_berlaku_delapan').val(''); 
            $('#val_jml_delapan').val('');   
            $("#img_delapan").attr("src",BASE_URL+'/assets/plugins/images/pemkot.png');
        }


        function on_cetak(){

            var listCetak = [];
            
            if($('#val_nik_satu').val().length == 16){
                var arr_nik_satu = {};
                var arr_count_satu = {};
                arr_nik_satu.rn = 1;
                arr_nik_satu.posisi = 1;
                arr_nik_satu.no_skts = $('#val_no_skts_satu').val();
                arr_nik_satu.nik = $('#val_nik_satu').val();
                arr_nik_satu.nama_lgkp = $('#val_nama_lgkp_satu').val();
                arr_nik_satu.tmpt_lhr = $('#val_tmpt_lhr_satu').val();
                arr_nik_satu.tgl_lhr = $('#val_tgl_lhr_satu').val();
                arr_nik_satu.jenis_klmin = $('#val_jenis_klmin_satu').val();
                arr_nik_satu.pic = $('#val_pic_satu').val();
                arr_nik_satu.agama = $('#val_agama_satu').val();
                arr_nik_satu.stat_kwn = $('#val_stat_kwn_satu').val();
                arr_nik_satu.gol_drh = $('#val_gol_drh_satu').val();
                arr_nik_satu.pendidikan = $('#val_pendidikan_satu').val();
                arr_nik_satu.pekerjaan = $('#val_pekerjaan_satu').val();
                arr_nik_satu.nama_prop = $('#val_nama_prop_satu').val();
                arr_nik_satu.nama_kab = $('#val_nama_kab_satu').val();
                arr_nik_satu.nama_kec = $('#val_nama_kec_satu').val();
                arr_nik_satu.nama_kel = $('#val_nama_kel_satu').val();
                arr_nik_satu.src_alamat = $('#val_src_alamat_satu').val();
                arr_nik_satu.src_rt = $('#val_src_rt_satu').val();
                arr_nik_satu.src_rw = $('#val_src_rw_satu').val();
                arr_nik_satu.kec = $('#val_kec_satu').val();
                arr_nik_satu.kel = $('#val_kel_satu').val();
                arr_nik_satu.alamat = $('#val_alamat_satu').val();
                arr_nik_satu.no_rt = $('#val_no_rt_satu').val();
                arr_nik_satu.no_rw = $('#val_no_rw_satu').val();
                arr_nik_satu.telepon = $('#val_telepon_satu').val();
                arr_nik_satu.email = $('#val_email_satu').val();
                arr_nik_satu.status = $('#val_status_satu').val();
                arr_nik_satu.tgl_berlaku = $('#val_tgl_berlaku_satu').val();
                arr_count_satu.nik = $('#nik_satu').val();
                arr_count_satu.count = $('#val_jml_satu').val();
                arr_nik_satu.count_cetak =arr_count_satu;
                listCetak.push(arr_nik_satu);
            }

            if($('#val_nik_dua').val().length == 16){
                var arr_nik_dua = {};
                var arr_count_dua = {};
                arr_nik_dua.rn = 2;
                arr_nik_dua.posisi = 2;
                arr_nik_dua.no_skts = $('#val_no_skts_dua').val();
                arr_nik_dua.nik = $('#val_nik_dua').val();
                arr_nik_dua.nama_lgkp = $('#val_nama_lgkp_dua').val();
                arr_nik_dua.tmpt_lhr = $('#val_tmpt_lhr_dua').val();
                arr_nik_dua.tgl_lhr = $('#val_tgl_lhr_dua').val();
                arr_nik_dua.jenis_klmin = $('#val_jenis_klmin_dua').val();
                arr_nik_dua.pic = $('#val_pic_dua').val();
                arr_nik_dua.agama = $('#val_agama_dua').val();
                arr_nik_dua.stat_kwn = $('#val_stat_kwn_dua').val();
                arr_nik_dua.gol_drh = $('#val_gol_drh_dua').val();
                arr_nik_dua.pendidikan = $('#val_pendidikan_dua').val();
                arr_nik_dua.pekerjaan = $('#val_pekerjaan_dua').val();
                arr_nik_dua.nama_prop = $('#val_nama_prop_dua').val();
                arr_nik_dua.nama_kab = $('#val_nama_kab_dua').val();
                arr_nik_dua.nama_kec = $('#val_nama_kec_dua').val();
                arr_nik_dua.nama_kel = $('#val_nama_kel_dua').val();
                arr_nik_dua.src_alamat = $('#val_src_alamat_dua').val();
                arr_nik_dua.src_rt = $('#val_src_rt_dua').val();
                arr_nik_dua.src_rw = $('#val_src_rw_dua').val();
                arr_nik_dua.kec = $('#val_kec_dua').val();
                arr_nik_dua.kel = $('#val_kel_dua').val();
                arr_nik_dua.alamat = $('#val_alamat_dua').val();
                arr_nik_dua.no_rt = $('#val_no_rt_dua').val();
                arr_nik_dua.no_rw = $('#val_no_rw_dua').val();
                arr_nik_dua.telepon = $('#val_telepon_dua').val();
                arr_nik_dua.email = $('#val_email_dua').val();
                arr_nik_dua.status = $('#val_status_dua').val();
                arr_nik_dua.tgl_berlaku = $('#val_tgl_berlaku_dua').val();
                arr_count_dua.nik = $('#nik_dua').val();
                arr_count_dua.count = $('#val_jml_dua').val();
                arr_nik_dua.count_cetak =arr_count_dua;
                listCetak.push(arr_nik_dua);
            }
            
            if($('#val_nik_tiga').val().length == 16){
                var arr_nik_tiga = {};
                var arr_count_tiga = {};
                arr_nik_tiga.rn = 3;
                arr_nik_tiga.posisi = 3;
                arr_nik_tiga.no_skts = $('#val_no_skts_tiga').val();
                arr_nik_tiga.nik = $('#val_nik_tiga').val();
                arr_nik_tiga.nama_lgkp = $('#val_nama_lgkp_tiga').val();
                arr_nik_tiga.tmpt_lhr = $('#val_tmpt_lhr_tiga').val();
                arr_nik_tiga.tgl_lhr = $('#val_tgl_lhr_tiga').val();
                arr_nik_tiga.jenis_klmin = $('#val_jenis_klmin_tiga').val();
                arr_nik_tiga.pic = $('#val_pic_tiga').val();
                arr_nik_tiga.agama = $('#val_agama_tiga').val();
                arr_nik_tiga.stat_kwn = $('#val_stat_kwn_tiga').val();
                arr_nik_tiga.gol_drh = $('#val_gol_drh_tiga').val();
                arr_nik_tiga.pendidikan = $('#val_pendidikan_tiga').val();
                arr_nik_tiga.pekerjaan = $('#val_pekerjaan_tiga').val();
                arr_nik_tiga.nama_prop = $('#val_nama_prop_tiga').val();
                arr_nik_tiga.nama_kab = $('#val_nama_kab_tiga').val();
                arr_nik_tiga.nama_kec = $('#val_nama_kec_tiga').val();
                arr_nik_tiga.nama_kel = $('#val_nama_kel_tiga').val();
                arr_nik_tiga.src_alamat = $('#val_src_alamat_tiga').val();
                arr_nik_tiga.src_rt = $('#val_src_rt_tiga').val();
                arr_nik_tiga.src_rw = $('#val_src_rw_tiga').val();
                arr_nik_tiga.kec = $('#val_kec_tiga').val();
                arr_nik_tiga.kel = $('#val_kel_tiga').val();
                arr_nik_tiga.alamat = $('#val_alamat_tiga').val();
                arr_nik_tiga.no_rt = $('#val_no_rt_tiga').val();
                arr_nik_tiga.no_rw = $('#val_no_rw_tiga').val();
                arr_nik_tiga.telepon = $('#val_telepon_tiga').val();
                arr_nik_tiga.email = $('#val_email_tiga').val();
                arr_nik_tiga.status = $('#val_status_tiga').val();
                arr_nik_tiga.tgl_berlaku = $('#val_tgl_berlaku_tiga').val();
                arr_count_tiga.nik = $('#nik_tiga').val();
                arr_count_tiga.count = $('#val_jml_tiga').val();
                arr_nik_tiga.count_cetak =arr_count_tiga;
                listCetak.push(arr_nik_tiga);
            }

            if($('#val_nik_empat').val().length == 16){
                var arr_nik_empat = {};
                var arr_count_empat = {};
                arr_nik_empat.rn = 4;
                arr_nik_empat.posisi = 4;
                arr_nik_empat.no_skts = $('#val_no_skts_empat').val();
                arr_nik_empat.nik = $('#val_nik_empat').val();
                arr_nik_empat.nama_lgkp = $('#val_nama_lgkp_empat').val();
                arr_nik_empat.tmpt_lhr = $('#val_tmpt_lhr_empat').val();
                arr_nik_empat.tgl_lhr = $('#val_tgl_lhr_empat').val();
                arr_nik_empat.jenis_klmin = $('#val_jenis_klmin_empat').val();
                arr_nik_empat.pic = $('#val_pic_empat').val();
                arr_nik_empat.agama = $('#val_agama_empat').val();
                arr_nik_empat.stat_kwn = $('#val_stat_kwn_empat').val();
                arr_nik_empat.gol_drh = $('#val_gol_drh_empat').val();
                arr_nik_empat.pendidikan = $('#val_pendidikan_empat').val();
                arr_nik_empat.pekerjaan = $('#val_pekerjaan_empat').val();
                arr_nik_empat.nama_prop = $('#val_nama_prop_empat').val();
                arr_nik_empat.nama_kab = $('#val_nama_kab_empat').val();
                arr_nik_empat.nama_kec = $('#val_nama_kec_empat').val();
                arr_nik_empat.nama_kel = $('#val_nama_kel_empat').val();
                arr_nik_empat.src_alamat = $('#val_src_alamat_empat').val();
                arr_nik_empat.src_rt = $('#val_src_rt_empat').val();
                arr_nik_empat.src_rw = $('#val_src_rw_empat').val();
                arr_nik_empat.kec = $('#val_kec_empat').val();
                arr_nik_empat.kel = $('#val_kel_empat').val();
                arr_nik_empat.alamat = $('#val_alamat_empat').val();
                arr_nik_empat.no_rt = $('#val_no_rt_empat').val();
                arr_nik_empat.no_rw = $('#val_no_rw_empat').val();
                arr_nik_empat.telepon = $('#val_telepon_empat').val();
                arr_nik_empat.email = $('#val_email_empat').val();
                arr_nik_empat.status = $('#val_status_empat').val();
                arr_nik_empat.tgl_berlaku = $('#val_tgl_berlaku_empat').val();
                arr_count_empat.nik = $('#nik_empat').val();
                arr_count_empat.count = $('#val_jml_empat').val();
                arr_nik_empat.count_cetak =arr_count_empat;
                listCetak.push(arr_nik_empat);
            }

            if($('#val_nik_lima').val().length == 16){
                var arr_nik_lima = {};
                var arr_count_lima = {};
                arr_nik_lima.rn = 5;
                arr_nik_lima.posisi = 5;
                arr_nik_lima.no_skts = $('#val_no_skts_lima').val();
                arr_nik_lima.nik = $('#val_nik_lima').val();
                arr_nik_lima.nama_lgkp = $('#val_nama_lgkp_lima').val();
                arr_nik_lima.tmpt_lhr = $('#val_tmpt_lhr_lima').val();
                arr_nik_lima.tgl_lhr = $('#val_tgl_lhr_lima').val();
                arr_nik_lima.jenis_klmin = $('#val_jenis_klmin_lima').val();
                arr_nik_lima.pic = $('#val_pic_lima').val();
                arr_nik_lima.agama = $('#val_agama_lima').val();
                arr_nik_lima.stat_kwn = $('#val_stat_kwn_lima').val();
                arr_nik_lima.gol_drh = $('#val_gol_drh_lima').val();
                arr_nik_lima.pendidikan = $('#val_pendidikan_lima').val();
                arr_nik_lima.pekerjaan = $('#val_pekerjaan_lima').val();
                arr_nik_lima.nama_prop = $('#val_nama_prop_lima').val();
                arr_nik_lima.nama_kab = $('#val_nama_kab_lima').val();
                arr_nik_lima.nama_kec = $('#val_nama_kec_lima').val();
                arr_nik_lima.nama_kel = $('#val_nama_kel_lima').val();
                arr_nik_lima.src_alamat = $('#val_src_alamat_lima').val();
                arr_nik_lima.src_rt = $('#val_src_rt_lima').val();
                arr_nik_lima.src_rw = $('#val_src_rw_lima').val();
                arr_nik_lima.kec = $('#val_kec_lima').val();
                arr_nik_lima.kel = $('#val_kel_lima').val();
                arr_nik_lima.alamat = $('#val_alamat_lima').val();
                arr_nik_lima.no_rt = $('#val_no_rt_lima').val();
                arr_nik_lima.no_rw = $('#val_no_rw_lima').val();
                arr_nik_lima.telepon = $('#val_telepon_lima').val();
                arr_nik_lima.email = $('#val_email_lima').val();
                arr_nik_lima.status = $('#val_status_lima').val();
                arr_nik_lima.tgl_berlaku = $('#val_tgl_berlaku_lima').val();
                arr_count_lima.nik = $('#nik_lima').val();
                arr_count_lima.count = $('#val_jml_lima').val();
                arr_nik_lima.count_cetak =arr_count_lima;
                listCetak.push(arr_nik_lima);
            }

            if($('#val_nik_enam').val().length == 16){
                var arr_nik_enam = {};
                var arr_count_enam = {};
                arr_nik_enam.rn = 6;
                arr_nik_enam.posisi = 6;
                arr_nik_enam.no_skts = $('#val_no_skts_enam').val();
                arr_nik_enam.nik = $('#val_nik_enam').val();
                arr_nik_enam.nama_lgkp = $('#val_nama_lgkp_enam').val();
                arr_nik_enam.tmpt_lhr = $('#val_tmpt_lhr_enam').val();
                arr_nik_enam.tgl_lhr = $('#val_tgl_lhr_enam').val();
                arr_nik_enam.jenis_klmin = $('#val_jenis_klmin_enam').val();
                arr_nik_enam.pic = $('#val_pic_enam').val();
                arr_nik_enam.agama = $('#val_agama_enam').val();
                arr_nik_enam.stat_kwn = $('#val_stat_kwn_enam').val();
                arr_nik_enam.gol_drh = $('#val_gol_drh_enam').val();
                arr_nik_enam.pendidikan = $('#val_pendidikan_enam').val();
                arr_nik_enam.pekerjaan = $('#val_pekerjaan_enam').val();
                arr_nik_enam.nama_prop = $('#val_nama_prop_enam').val();
                arr_nik_enam.nama_kab = $('#val_nama_kab_enam').val();
                arr_nik_enam.nama_kec = $('#val_nama_kec_enam').val();
                arr_nik_enam.nama_kel = $('#val_nama_kel_enam').val();
                arr_nik_enam.src_alamat = $('#val_src_alamat_enam').val();
                arr_nik_enam.src_rt = $('#val_src_rt_enam').val();
                arr_nik_enam.src_rw = $('#val_src_rw_enam').val();
                arr_nik_enam.kec = $('#val_kec_enam').val();
                arr_nik_enam.kel = $('#val_kel_enam').val();
                arr_nik_enam.alamat = $('#val_alamat_enam').val();
                arr_nik_enam.no_rt = $('#val_no_rt_enam').val();
                arr_nik_enam.no_rw = $('#val_no_rw_enam').val();
                arr_nik_enam.telepon = $('#val_telepon_enam').val();
                arr_nik_enam.email = $('#val_email_enam').val();
                arr_nik_enam.status = $('#val_status_enam').val();
                arr_nik_enam.tgl_berlaku = $('#val_tgl_berlaku_enam').val();
                arr_count_enam.nik = $('#nik_enam').val();
                arr_count_enam.count = $('#val_jml_enam').val();
                arr_nik_enam.count_cetak =arr_count_enam;
                listCetak.push(arr_nik_enam);
            }

            if($('#val_nik_tujuh').val().length == 16){
                var arr_nik_tujuh = {};
                var arr_count_tujuh = {};
                arr_nik_tujuh.rn = 7;
                arr_nik_tujuh.posisi = 7;
                arr_nik_tujuh.no_skts = $('#val_no_skts_tujuh').val();
                arr_nik_tujuh.nik = $('#val_nik_tujuh').val();
                arr_nik_tujuh.nama_lgkp = $('#val_nama_lgkp_tujuh').val();
                arr_nik_tujuh.tmpt_lhr = $('#val_tmpt_lhr_tujuh').val();
                arr_nik_tujuh.tgl_lhr = $('#val_tgl_lhr_tujuh').val();
                arr_nik_tujuh.jenis_klmin = $('#val_jenis_klmin_tujuh').val();
                arr_nik_tujuh.pic = $('#val_pic_tujuh').val();
                arr_nik_tujuh.agama = $('#val_agama_tujuh').val();
                arr_nik_tujuh.stat_kwn = $('#val_stat_kwn_tujuh').val();
                arr_nik_tujuh.gol_drh = $('#val_gol_drh_tujuh').val();
                arr_nik_tujuh.pendidikan = $('#val_pendidikan_tujuh').val();
                arr_nik_tujuh.pekerjaan = $('#val_pekerjaan_tujuh').val();
                arr_nik_tujuh.nama_prop = $('#val_nama_prop_tujuh').val();
                arr_nik_tujuh.nama_kab = $('#val_nama_kab_tujuh').val();
                arr_nik_tujuh.nama_kec = $('#val_nama_kec_tujuh').val();
                arr_nik_tujuh.nama_kel = $('#val_nama_kel_tujuh').val();
                arr_nik_tujuh.src_alamat = $('#val_src_alamat_tujuh').val();
                arr_nik_tujuh.src_rt = $('#val_src_rt_tujuh').val();
                arr_nik_tujuh.src_rw = $('#val_src_rw_tujuh').val();
                arr_nik_tujuh.kec = $('#val_kec_tujuh').val();
                arr_nik_tujuh.kel = $('#val_kel_tujuh').val();
                arr_nik_tujuh.alamat = $('#val_alamat_tujuh').val();
                arr_nik_tujuh.no_rt = $('#val_no_rt_tujuh').val();
                arr_nik_tujuh.no_rw = $('#val_no_rw_tujuh').val();
                arr_nik_tujuh.telepon = $('#val_telepon_tujuh').val();
                arr_nik_tujuh.email = $('#val_email_tujuh').val();
                arr_nik_tujuh.status = $('#val_status_tujuh').val();
                arr_nik_tujuh.tgl_berlaku = $('#val_tgl_berlaku_tujuh').val();
                arr_count_tujuh.nik = $('#nik_tujuh').val();
                arr_count_tujuh.count = $('#val_jml_tujuh').val();
                arr_nik_tujuh.count_cetak =arr_count_tujuh;
                listCetak.push(arr_nik_tujuh);
            }

            if($('#val_nik_delapan').val().length == 16){
                var arr_nik_delapan = {};
                var arr_count_delapan = {};
                arr_nik_delapan.rn = 8;
                arr_nik_delapan.posisi = 8;
                arr_nik_delapan.no_skts = $('#val_no_skts_delapan').val();
                arr_nik_delapan.nik = $('#val_nik_delapan').val();
                arr_nik_delapan.nama_lgkp = $('#val_nama_lgkp_delapan').val();
                arr_nik_delapan.tmpt_lhr = $('#val_tmpt_lhr_delapan').val();
                arr_nik_delapan.tgl_lhr = $('#val_tgl_lhr_delapan').val();
                arr_nik_delapan.jenis_klmin = $('#val_jenis_klmin_delapan').val();
                arr_nik_delapan.pic = $('#val_pic_delapan').val();
                arr_nik_delapan.agama = $('#val_agama_delapan').val();
                arr_nik_delapan.stat_kwn = $('#val_stat_kwn_delapan').val();
                arr_nik_delapan.gol_drh = $('#val_gol_drh_delapan').val();
                arr_nik_delapan.pendidikan = $('#val_pendidikan_delapan').val();
                arr_nik_delapan.pekerjaan = $('#val_pekerjaan_delapan').val();
                arr_nik_delapan.nama_prop = $('#val_nama_prop_delapan').val();
                arr_nik_delapan.nama_kab = $('#val_nama_kab_delapan').val();
                arr_nik_delapan.nama_kec = $('#val_nama_kec_delapan').val();
                arr_nik_delapan.nama_kel = $('#val_nama_kel_delapan').val();
                arr_nik_delapan.src_alamat = $('#val_src_alamat_delapan').val();
                arr_nik_delapan.src_rt = $('#val_src_rt_delapan').val();
                arr_nik_delapan.src_rw = $('#val_src_rw_delapan').val();
                arr_nik_delapan.kec = $('#val_kec_delapan').val();
                arr_nik_delapan.kel = $('#val_kel_delapan').val();
                arr_nik_delapan.alamat = $('#val_alamat_delapan').val();
                arr_nik_delapan.no_rt = $('#val_no_rt_delapan').val();
                arr_nik_delapan.no_rw = $('#val_no_rw_delapan').val();
                arr_nik_delapan.telepon = $('#val_telepon_delapan').val();
                arr_nik_delapan.email = $('#val_email_delapan').val();
                arr_nik_delapan.status = $('#val_status_delapan').val();
                arr_nik_delapan.tgl_berlaku = $('#val_tgl_berlaku_delapan').val();
                arr_count_delapan.nik = $('#nik_delapan').val();
                arr_count_delapan.count = $('#val_jml_delapan').val();
                arr_nik_delapan.count_cetak =arr_count_delapan;
                listCetak.push(arr_nik_delapan);
            }

            if (!jQuery.isEmptyObject(listCetak)) {
                $.ajax({
                    type: "post",
                    url: BASE_URL+"api/print",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        printer : $('#printer').val(),
                        nik : {{ $user_nik = (!empty($user_nik)) ? $user_nik : 0 }},
                        listCetak : listCetak,
                    },
                    beforeSend:
                    function () {
                    },
                    success: function (data) {
                        console.log(data);
                        window.open(BASE_URL+data,'_blank');
                    },
                    error:
                    function (data) {

                    },
                    complete:
                    function (response) {
                        
                    }
                });
            }else{
                swal("Warning!", "Masukan Nik Yang Akan Dicetak!", "warning");
            }
            
        }
        function on_clear(){
           $("#cb_nik_satu").prop( "checked", false );
            $("#nik_satu").attr("disabled",true);
            $("#nik_satu").val("");
            $("#btn_satu").attr("disabled",true);
            clear_nik_satu();

            $("#cb_nik_dua").prop( "checked", false );
            $("#nik_dua").attr("disabled",true);
            $("#nik_dua").val("");
            $("#btn_dua").attr("disabled",true);
            clear_nik_dua();

            $("#cb_nik_tiga").prop( "checked", false );
            $("#nik_tiga").attr("disabled",true);
            $("#nik_tiga").val("");
            $("#btn_tiga").attr("disabled",true);
            clear_nik_tiga();

            $("#cb_nik_empat").prop( "checked", false );
            $("#nik_empat").attr("disabled",true);
            $("#nik_empat").val("");
            $("#btn_empat").attr("disabled",true);
            clear_nik_empat();

            $("#cb_nik_lima").prop( "checked", false );
            $("#nik_lima").attr("disabled",true);
            $("#nik_lima").val("");
            $("#btn_lima").attr("disabled",true);
            clear_nik_lima();

            $("#cb_nik_enam").prop( "checked", false );
            $("#nik_enam").attr("disabled",true);
            $("#nik_enam").val("");
            $("#btn_enam").attr("disabled",true);
            clear_nik_enam();

            $("#cb_nik_tujuh").prop( "checked", false );
            $("#nik_tujuh").attr("disabled",true);
            $("#nik_tujuh").val("");
            $("#btn_tujuh").attr("disabled",true);
            clear_nik_tujuh();

            $("#cb_nik_delapan").prop( "checked", false );
            $("#nik_delapan").attr("disabled",true);
            $("#nik_delapan").val("");
            $("#btn_delapan").attr("disabled",true);
            clear_nik_delapan();
        }
    </script>