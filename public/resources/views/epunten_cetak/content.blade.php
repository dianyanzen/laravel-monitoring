 <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $stitle; ?></h5> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                        <ol class="breadcrumb">
                           <li class="text-info"><?php echo $stitle; ?></li>
                        </ol>
                    </div>
                </div>
                 
                
                <div class="row">
                    <div class="col-md-12 col-xs-12">
                        <div class="white-box">
                            <div class="btn-group m-r-10 pull-right">
                                    <button aria-expanded="false" data-toggle="dropdown" class="fcbtn btn btn-info btn-outline dropdown-toggle waves-effect waves-light" type="button">Navigasi <span class="caret"></span></button>
                                     <ul role="menu" class="dropdown-menu animated flipInX">
                                        <li><a href="{{ url('/') }}/Epunten/skts/baru">Baru</a></li>
                                        <li class="divider"></li>
                                        <li><a href="{{ url('/') }}/Epunten/skts/batal">Batal</a></li>
                                        <li><a href="{{ url('/') }}/Epunten/skts/melengkapi">Melengkapi</a></li>
                                        <li class="divider"></li>
                                        <li><a href="{{ url('/') }}/Epunten/skts/acc_dokumen">Acc Dokumen</a></li>
                                        <li><a href="{{ url('/') }}/Epunten/skts/arsip">Arsip</a></li>
                                        <li class="divider"></li>
                                        <li><a href="{{ url('/') }}/Epunten/cetak">Cetak Kartu</a></li>
                                        <li><a href="{{ url('/') }}/Epunten/skts/pembaruan">Pembaruan</a></li>
                                    </ul>
                                </div>
                            <div class="row">
                                <div class="col-lg-3">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_kec" type="checkbox" checked disabled="true">
                                            <label for="cb_kec"> Printer</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3">         
                                    <div class="form-group" <?php if (!empty($is_colnotwil)){echo 'style="display: none!important;"'; }?>>
                                        <select class="form-control select2" name="printer" id="printer">
                                         <option  value="1" selected>Brother DCP-T300</option>
                                         <option  value="2">Epson L300</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
                <div class="row">
                <div class="col-md-12">
                        <div class="white-box">
                            <div class="row">
                                <div class="col-md-6 col-xs-12" id="head_satu">
                                    <div class="col-lg-6">         
                                        <div class="form-group">
                                            <div class="checkbox checkbox-info align-bottom">
                                                <input id="cb_nik_satu" type="checkbox">
                                                <label for="cb_nik_satu"> Posisi 1</label>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <div class="col-lg-6">         
                                        <div class="form-group input-group">
                                            <input class="form-control" type="text" id="nik_satu" name="nik_satu" onkeypress="return isNumberKey(event)" disabled/> 
                                            <span class="input-group-btn"> 
                                                <button class="btn btn-info" type="button" id="btn_satu" name="btn_satu" onclick="get_nik_satu();" disabled><i class="ti-search"></i></button> 
                                            </span> 
                                        </div>        
                                    </div>
                                    <div class="col-md-12 col-xs-12" id="box_satu" style="display: none;">
                                    <img src="{{ url('/') }}/assets/plugins/images/pemkot.png" alt="user" id="img_satu" class="col-md-3 col-xs-6" height="160px" >
                                       <h5>Nik :  <span id="txt_nik_satu">-</span></h5>
                                       <h5>Nama Lengkap :  <span id="txt_nama_lgkp_satu">-</span></h5>
                                       <h5>Tempat Lahir :  <span id="txt_tmpt_lhr_satu">-</span></h5>
                                       <h5>Tanggal Lahir :  <span id="txt_tgl_lhr_satu">-</span></h5>
                                       <h5>Jumlah Cetak :  <span id="txt_jml_satu">-</span></h5>
                                        <input type="hidden" id="val_rn_satu" val="1"/> 
                                        <input type="hidden" id="val_posisi_satu" val="1"/> 
                                        <input type="hidden" id="val_no_skts_satu" /> 
                                        <input type="hidden" id="val_nik_satu" /> 
                                        <input type="hidden" id="val_nama_lgkp_satu" /> 
                                        <input type="hidden" id="val_tmpt_lhr_satu" /> 
                                        <input type="hidden" id="val_tgl_lhr_satu" /> 
                                        <input type="hidden" id="val_jenis_klmin_satu" /> 
                                        <input type="hidden" id="val_pic_satu" /> 
                                        <input type="hidden" id="val_agama_satu" /> 
                                        <input type="hidden" id="val_stat_kwn_satu" /> 
                                        <input type="hidden" id="val_gol_drh_satu" /> 
                                        <input type="hidden" id="val_pendidikan_satu" /> 
                                        <input type="hidden" id="val_pekerjaan_satu" /> 
                                        <input type="hidden" id="val_nama_prop_satu" /> 
                                        <input type="hidden" id="val_nama_kab_satu" /> 
                                        <input type="hidden" id="val_nama_kec_satu" /> 
                                        <input type="hidden" id="val_nama_kel_satu" /> 
                                        <input type="hidden" id="val_src_alamat_satu" /> 
                                        <input type="hidden" id="val_src_rt_satu" /> 
                                        <input type="hidden" id="val_src_rw_satu" /> 
                                        <input type="hidden" id="val_kec_satu" /> 
                                        <input type="hidden" id="val_kel_satu" /> 
                                        <input type="hidden" id="val_alamat_satu" /> 
                                        <input type="hidden" id="val_no_rt_satu" /> 
                                        <input type="hidden" id="val_no_rw_satu" /> 
                                        <input type="hidden" id="val_telepon_satu" /> 
                                        <input type="hidden" id="val_email_satu" /> 
                                        <input type="hidden" id="val_status_satu" /> 
                                        <input type="hidden" id="val_tgl_berlaku_satu" /> 
                                        <input type="hidden" id="val_jml_satu" /> 
                                    </div>
                                </div>
                                <div class="col-md-6 col-xs-12" id="head_dua">
                                    <div class="col-lg-6">         
                                        <div class="form-group">
                                            <div class="checkbox checkbox-info align-bottom">
                                                <input id="cb_nik_dua" type="checkbox">
                                                <label for="cb_nik_dua"> Posisi 2</label>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <div class="col-lg-6">         
                                        <div class="form-group input-group">
                                            <input class="form-control" type="text" id="nik_dua" name="nik_dua" onkeypress="return isNumberKey(event)" disabled/> 
                                            <span class="input-group-btn"> 
                                                <button class="btn btn-info" type="button" id="btn_dua" name="btn_dua" onclick="get_nik_dua();" disabled><i class="ti-search"></i></button> 
                                            </span> 
                                        </div>        
                                    </div>
                                    <div class="col-md-12 col-xs-12" id="box_dua" style="display: none;">
                                    <img src="{{ url('/') }}/assets/plugins/images/pemkot.png" alt="user" id="img_dua" class="col-md-3 col-xs-6" height="160px" >
                                       <h5>Nik :  <span id="txt_nik_dua">-</span></h5>
                                       <h5>Nama Lengkap :  <span id="txt_nama_lgkp_dua">-</span></h5>
                                       <h5>Tempat Lahir :  <span id="txt_tmpt_lhr_dua">-</span></h5>
                                       <h5>Tanggal Lahir :  <span id="txt_tgl_lhr_dua">-</span></h5>
                                       <h5>Jumlah Cetak :  <span id="txt_jml_dua">-</span></h5>
                                        <input type="hidden" id="val_rn_dua" val="1"/> 
                                        <input type="hidden" id="val_posisi_dua" val="1"/> 
                                        <input type="hidden" id="val_no_skts_dua" /> 
                                        <input type="hidden" id="val_nik_dua" /> 
                                        <input type="hidden" id="val_nama_lgkp_dua" /> 
                                        <input type="hidden" id="val_tmpt_lhr_dua" /> 
                                        <input type="hidden" id="val_tgl_lhr_dua" /> 
                                        <input type="hidden" id="val_jenis_klmin_dua" /> 
                                        <input type="hidden" id="val_pic_dua" /> 
                                        <input type="hidden" id="val_agama_dua" /> 
                                        <input type="hidden" id="val_stat_kwn_dua" /> 
                                        <input type="hidden" id="val_gol_drh_dua" /> 
                                        <input type="hidden" id="val_pendidikan_dua" /> 
                                        <input type="hidden" id="val_pekerjaan_dua" /> 
                                        <input type="hidden" id="val_nama_prop_dua" /> 
                                        <input type="hidden" id="val_nama_kab_dua" /> 
                                        <input type="hidden" id="val_nama_kec_dua" /> 
                                        <input type="hidden" id="val_nama_kel_dua" /> 
                                        <input type="hidden" id="val_src_alamat_dua" /> 
                                        <input type="hidden" id="val_src_rt_dua" /> 
                                        <input type="hidden" id="val_src_rw_dua" /> 
                                        <input type="hidden" id="val_kec_dua" /> 
                                        <input type="hidden" id="val_kel_dua" /> 
                                        <input type="hidden" id="val_alamat_dua" /> 
                                        <input type="hidden" id="val_no_rt_dua" /> 
                                        <input type="hidden" id="val_no_rw_dua" /> 
                                        <input type="hidden" id="val_telepon_dua" /> 
                                        <input type="hidden" id="val_email_dua" /> 
                                        <input type="hidden" id="val_status_dua" /> 
                                        <input type="hidden" id="val_tgl_berlaku_dua" /> 
                                        <input type="hidden" id="val_jml_dua" /> 
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-xs-12" id="head_tiga" style="margin-top: 15px !important">
                                    <div class="col-lg-6">         
                                        <div class="form-group">
                                            <div class="checkbox checkbox-info align-bottom">
                                                <input id="cb_nik_tiga" type="checkbox">
                                                <label for="cb_nik_tiga"> Posisi 3</label>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <div class="col-lg-6">         
                                        <div class="form-group input-group">
                                            <input class="form-control" type="text" id="nik_tiga" name="nik_tiga" onkeypress="return isNumberKey(event)" disabled/> 
                                            <span class="input-group-btn"> 
                                                <button class="btn btn-info" type="button" id="btn_tiga" name="btn_tiga" onclick="get_nik_tiga();" disabled><i class="ti-search"></i></button> 
                                            </span> 
                                        </div>        
                                    </div>
                                    <div class="col-md-12 col-xs-12" id="box_tiga" style="display: none;">
                                    <img src="{{ url('/') }}/assets/plugins/images/pemkot.png" alt="user" id="img_tiga" class="col-md-3 col-xs-6" height="160px" >
                                       <h5>Nik :  <span id="txt_nik_tiga">-</span></h5>
                                       <h5>Nama Lengkap :  <span id="txt_nama_lgkp_tiga">-</span></h5>
                                       <h5>Tempat Lahir :  <span id="txt_tmpt_lhr_tiga">-</span></h5>
                                       <h5>Tanggal Lahir :  <span id="txt_tgl_lhr_tiga">-</span></h5>
                                       <h5>Jumlah Cetak :  <span id="txt_jml_tiga">-</span></h5>
                                        <input type="hidden" id="val_rn_tiga" val="1"/> 
                                        <input type="hidden" id="val_posisi_tiga" val="1"/> 
                                        <input type="hidden" id="val_no_skts_tiga" /> 
                                        <input type="hidden" id="val_nik_tiga" /> 
                                        <input type="hidden" id="val_nama_lgkp_tiga" /> 
                                        <input type="hidden" id="val_tmpt_lhr_tiga" /> 
                                        <input type="hidden" id="val_tgl_lhr_tiga" /> 
                                        <input type="hidden" id="val_jenis_klmin_tiga" /> 
                                        <input type="hidden" id="val_pic_tiga" /> 
                                        <input type="hidden" id="val_agama_tiga" /> 
                                        <input type="hidden" id="val_stat_kwn_tiga" /> 
                                        <input type="hidden" id="val_gol_drh_tiga" /> 
                                        <input type="hidden" id="val_pendidikan_tiga" /> 
                                        <input type="hidden" id="val_pekerjaan_tiga" /> 
                                        <input type="hidden" id="val_nama_prop_tiga" /> 
                                        <input type="hidden" id="val_nama_kab_tiga" /> 
                                        <input type="hidden" id="val_nama_kec_tiga" /> 
                                        <input type="hidden" id="val_nama_kel_tiga" /> 
                                        <input type="hidden" id="val_src_alamat_tiga" /> 
                                        <input type="hidden" id="val_src_rt_tiga" /> 
                                        <input type="hidden" id="val_src_rw_tiga" /> 
                                        <input type="hidden" id="val_kec_tiga" /> 
                                        <input type="hidden" id="val_kel_tiga" /> 
                                        <input type="hidden" id="val_alamat_tiga" /> 
                                        <input type="hidden" id="val_no_rt_tiga" /> 
                                        <input type="hidden" id="val_no_rw_tiga" /> 
                                        <input type="hidden" id="val_telepon_tiga" /> 
                                        <input type="hidden" id="val_email_tiga" /> 
                                        <input type="hidden" id="val_status_tiga" /> 
                                        <input type="hidden" id="val_tgl_berlaku_tiga" /> 
                                        <input type="hidden" id="val_jml_tiga" /> 
                                    </div>
                                </div>
                                <div class="col-md-6 col-xs-12" id="head_empat" style="margin-top: 15px !important">
                                    <div class="col-lg-6">         
                                        <div class="form-group">
                                            <div class="checkbox checkbox-info align-bottom">
                                                <input id="cb_nik_empat" type="checkbox">
                                                <label for="cb_nik_empat"> Posisi 4</label>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <div class="col-lg-6">         
                                        <div class="form-group input-group">
                                            <input class="form-control" type="text" id="nik_empat" name="nik_empat" onkeypress="return isNumberKey(event)" disabled/> 
                                            <span class="input-group-btn"> 
                                                <button class="btn btn-info" type="button" id="btn_empat" name="btn_empat" onclick="get_nik_empat();" disabled><i class="ti-search"></i></button> 
                                            </span> 
                                        </div>        
                                    </div>
                                    <div class="col-md-12 col-xs-12" id="box_empat" style="display: none;">
                                    <img src="{{ url('/') }}/assets/plugins/images/pemkot.png" alt="user" id="img_empat" class="col-md-3 col-xs-6" height="160px" >
                                       <h5>Nik :  <span id="txt_nik_empat">-</span></h5>
                                       <h5>Nama Lengkap :  <span id="txt_nama_lgkp_empat">-</span></h5>
                                       <h5>Tempat Lahir :  <span id="txt_tmpt_lhr_empat">-</span></h5>
                                       <h5>Tanggal Lahir :  <span id="txt_tgl_lhr_empat">-</span></h5>
                                       <h5>Jumlah Cetak :  <span id="txt_jml_empat">-</span></h5>
                                        <input type="hidden" id="val_rn_empat" val="1"/> 
                                        <input type="hidden" id="val_posisi_empat" val="1"/> 
                                        <input type="hidden" id="val_no_skts_empat" /> 
                                        <input type="hidden" id="val_nik_empat" /> 
                                        <input type="hidden" id="val_nama_lgkp_empat" /> 
                                        <input type="hidden" id="val_tmpt_lhr_empat" /> 
                                        <input type="hidden" id="val_tgl_lhr_empat" /> 
                                        <input type="hidden" id="val_jenis_klmin_empat" /> 
                                        <input type="hidden" id="val_pic_empat" /> 
                                        <input type="hidden" id="val_agama_empat" /> 
                                        <input type="hidden" id="val_stat_kwn_empat" /> 
                                        <input type="hidden" id="val_gol_drh_empat" /> 
                                        <input type="hidden" id="val_pendidikan_empat" /> 
                                        <input type="hidden" id="val_pekerjaan_empat" /> 
                                        <input type="hidden" id="val_nama_prop_empat" /> 
                                        <input type="hidden" id="val_nama_kab_empat" /> 
                                        <input type="hidden" id="val_nama_kec_empat" /> 
                                        <input type="hidden" id="val_nama_kel_empat" /> 
                                        <input type="hidden" id="val_src_alamat_empat" /> 
                                        <input type="hidden" id="val_src_rt_empat" /> 
                                        <input type="hidden" id="val_src_rw_empat" /> 
                                        <input type="hidden" id="val_kec_empat" /> 
                                        <input type="hidden" id="val_kel_empat" /> 
                                        <input type="hidden" id="val_alamat_empat" /> 
                                        <input type="hidden" id="val_no_rt_empat" /> 
                                        <input type="hidden" id="val_no_rw_empat" /> 
                                        <input type="hidden" id="val_telepon_empat" /> 
                                        <input type="hidden" id="val_email_empat" /> 
                                        <input type="hidden" id="val_status_empat" /> 
                                        <input type="hidden" id="val_tgl_berlaku_empat" /> 
                                        <input type="hidden" id="val_jml_empat" /> 
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                               <div class="col-md-6 col-xs-12" id="head_lima" style="margin-top: 15px !important">
                                    <div class="col-lg-6">         
                                        <div class="form-group">
                                            <div class="checkbox checkbox-info align-bottom">
                                                <input id="cb_nik_lima" type="checkbox">
                                                <label for="cb_nik_lima"> Posisi 5</label>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <div class="col-lg-6">         
                                        <div class="form-group input-group">
                                            <input class="form-control" type="text" id="nik_lima" name="nik_lima" onkeypress="return isNumberKey(event)" disabled/> 
                                            <span class="input-group-btn"> 
                                                <button class="btn btn-info" type="button" id="btn_lima" name="btn_lima" onclick="get_nik_lima();" disabled><i class="ti-search"></i></button> 
                                            </span> 
                                        </div>        
                                    </div>
                                    <div class="col-md-12 col-xs-12" id="box_lima" style="display: none;">
                                    <img src="{{ url('/') }}/assets/plugins/images/pemkot.png" alt="user" id="img_lima" class="col-md-3 col-xs-6" height="160px" >
                                       <h5>Nik :  <span id="txt_nik_lima">-</span></h5>
                                       <h5>Nama Lengkap :  <span id="txt_nama_lgkp_lima">-</span></h5>
                                       <h5>Tempat Lahir :  <span id="txt_tmpt_lhr_lima">-</span></h5>
                                       <h5>Tanggal Lahir :  <span id="txt_tgl_lhr_lima">-</span></h5>
                                       <h5>Jumlah Cetak :  <span id="txt_jml_lima">-</span></h5>
                                        <input type="hidden" id="val_rn_lima" val="1"/> 
                                        <input type="hidden" id="val_posisi_lima" val="1"/> 
                                        <input type="hidden" id="val_no_skts_lima" /> 
                                        <input type="hidden" id="val_nik_lima" /> 
                                        <input type="hidden" id="val_nama_lgkp_lima" /> 
                                        <input type="hidden" id="val_tmpt_lhr_lima" /> 
                                        <input type="hidden" id="val_tgl_lhr_lima" /> 
                                        <input type="hidden" id="val_jenis_klmin_lima" /> 
                                        <input type="hidden" id="val_pic_lima" /> 
                                        <input type="hidden" id="val_agama_lima" /> 
                                        <input type="hidden" id="val_stat_kwn_lima" /> 
                                        <input type="hidden" id="val_gol_drh_lima" /> 
                                        <input type="hidden" id="val_pendidikan_lima" /> 
                                        <input type="hidden" id="val_pekerjaan_lima" /> 
                                        <input type="hidden" id="val_nama_prop_lima" /> 
                                        <input type="hidden" id="val_nama_kab_lima" /> 
                                        <input type="hidden" id="val_nama_kec_lima" /> 
                                        <input type="hidden" id="val_nama_kel_lima" /> 
                                        <input type="hidden" id="val_src_alamat_lima" /> 
                                        <input type="hidden" id="val_src_rt_lima" /> 
                                        <input type="hidden" id="val_src_rw_lima" /> 
                                        <input type="hidden" id="val_kec_lima" /> 
                                        <input type="hidden" id="val_kel_lima" /> 
                                        <input type="hidden" id="val_alamat_lima" /> 
                                        <input type="hidden" id="val_no_rt_lima" /> 
                                        <input type="hidden" id="val_no_rw_lima" /> 
                                        <input type="hidden" id="val_telepon_lima" /> 
                                        <input type="hidden" id="val_email_lima" /> 
                                        <input type="hidden" id="val_status_lima" /> 
                                        <input type="hidden" id="val_tgl_berlaku_lima" /> 
                                        <input type="hidden" id="val_jml_lima" /> 
                                    </div>
                                </div>
                                <div class="col-md-6 col-xs-12" id="head_enam" style="margin-top: 15px !important">
                                    <div class="col-lg-6">         
                                        <div class="form-group">
                                            <div class="checkbox checkbox-info align-bottom">
                                                <input id="cb_nik_enam" type="checkbox">
                                                <label for="cb_nik_enam"> Posisi 6</label>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <div class="col-lg-6">         
                                        <div class="form-group input-group">
                                            <input class="form-control" type="text" id="nik_enam" name="nik_enam" onkeypress="return isNumberKey(event)" disabled/> 
                                            <span class="input-group-btn"> 
                                                <button class="btn btn-info" type="button" id="btn_enam" name="btn_enam" onclick="get_nik_enam();" disabled><i class="ti-search"></i></button> 
                                            </span> 
                                        </div>        
                                    </div>
                                    <div class="col-md-12 col-xs-12" id="box_enam" style="display: none;">
                                    <img src="{{ url('/') }}/assets/plugins/images/pemkot.png" alt="user" id="img_enam" class="col-md-3 col-xs-6" height="160px" >
                                       <h5>Nik :  <span id="txt_nik_enam">-</span></h5>
                                       <h5>Nama Lengkap :  <span id="txt_nama_lgkp_enam">-</span></h5>
                                       <h5>Tempat Lahir :  <span id="txt_tmpt_lhr_enam">-</span></h5>
                                       <h5>Tanggal Lahir :  <span id="txt_tgl_lhr_enam">-</span></h5>
                                       <h5>Jumlah Cetak :  <span id="txt_jml_enam">-</span></h5>
                                        <input type="hidden" id="val_rn_enam" val="1"/> 
                                        <input type="hidden" id="val_posisi_enam" val="1"/> 
                                        <input type="hidden" id="val_no_skts_enam" /> 
                                        <input type="hidden" id="val_nik_enam" /> 
                                        <input type="hidden" id="val_nama_lgkp_enam" /> 
                                        <input type="hidden" id="val_tmpt_lhr_enam" /> 
                                        <input type="hidden" id="val_tgl_lhr_enam" /> 
                                        <input type="hidden" id="val_jenis_klmin_enam" /> 
                                        <input type="hidden" id="val_pic_enam" /> 
                                        <input type="hidden" id="val_agama_enam" /> 
                                        <input type="hidden" id="val_stat_kwn_enam" /> 
                                        <input type="hidden" id="val_gol_drh_enam" /> 
                                        <input type="hidden" id="val_pendidikan_enam" /> 
                                        <input type="hidden" id="val_pekerjaan_enam" /> 
                                        <input type="hidden" id="val_nama_prop_enam" /> 
                                        <input type="hidden" id="val_nama_kab_enam" /> 
                                        <input type="hidden" id="val_nama_kec_enam" /> 
                                        <input type="hidden" id="val_nama_kel_enam" /> 
                                        <input type="hidden" id="val_src_alamat_enam" /> 
                                        <input type="hidden" id="val_src_rt_enam" /> 
                                        <input type="hidden" id="val_src_rw_enam" /> 
                                        <input type="hidden" id="val_kec_enam" /> 
                                        <input type="hidden" id="val_kel_enam" /> 
                                        <input type="hidden" id="val_alamat_enam" /> 
                                        <input type="hidden" id="val_no_rt_enam" /> 
                                        <input type="hidden" id="val_no_rw_enam" /> 
                                        <input type="hidden" id="val_telepon_enam" /> 
                                        <input type="hidden" id="val_email_enam" /> 
                                        <input type="hidden" id="val_status_enam" /> 
                                        <input type="hidden" id="val_tgl_berlaku_enam" /> 
                                        <input type="hidden" id="val_jml_enam" /> 
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-xs-12" id="head_tujuh" style="margin-top: 15px !important">
                                    <div class="col-lg-6">         
                                        <div class="form-group">
                                            <div class="checkbox checkbox-info align-bottom">
                                                <input id="cb_nik_tujuh" type="checkbox">
                                                <label for="cb_nik_tujuh"> Posisi 7</label>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <div class="col-lg-6">         
                                        <div class="form-group input-group">
                                            <input class="form-control" type="text" id="nik_tujuh" name="nik_tujuh" onkeypress="return isNumberKey(event)" disabled/> 
                                            <span class="input-group-btn"> 
                                                <button class="btn btn-info" type="button" id="btn_tujuh" name="btn_tujuh" onclick="get_nik_tujuh();" disabled><i class="ti-search"></i></button> 
                                            </span> 
                                        </div>        
                                    </div>
                                    <div class="col-md-12 col-xs-12" id="box_tujuh" style="display: none;">
                                    <img src="{{ url('/') }}/assets/plugins/images/pemkot.png" alt="user" id="img_tujuh" class="col-md-3 col-xs-6" height="160px" >
                                       <h5>Nik :  <span id="txt_nik_tujuh">-</span></h5>
                                       <h5>Nama Lengkap :  <span id="txt_nama_lgkp_tujuh">-</span></h5>
                                       <h5>Tempat Lahir :  <span id="txt_tmpt_lhr_tujuh">-</span></h5>
                                       <h5>Tanggal Lahir :  <span id="txt_tgl_lhr_tujuh">-</span></h5>
                                       <h5>Jumlah Cetak :  <span id="txt_jml_tujuh">-</span></h5>
                                        <input type="hidden" id="val_rn_tujuh" val="1"/> 
                                        <input type="hidden" id="val_posisi_tujuh" val="1"/> 
                                        <input type="hidden" id="val_no_skts_tujuh" /> 
                                        <input type="hidden" id="val_nik_tujuh" /> 
                                        <input type="hidden" id="val_nama_lgkp_tujuh" /> 
                                        <input type="hidden" id="val_tmpt_lhr_tujuh" /> 
                                        <input type="hidden" id="val_tgl_lhr_tujuh" /> 
                                        <input type="hidden" id="val_jenis_klmin_tujuh" /> 
                                        <input type="hidden" id="val_pic_tujuh" /> 
                                        <input type="hidden" id="val_agama_tujuh" /> 
                                        <input type="hidden" id="val_stat_kwn_tujuh" /> 
                                        <input type="hidden" id="val_gol_drh_tujuh" /> 
                                        <input type="hidden" id="val_pendidikan_tujuh" /> 
                                        <input type="hidden" id="val_pekerjaan_tujuh" /> 
                                        <input type="hidden" id="val_nama_prop_tujuh" /> 
                                        <input type="hidden" id="val_nama_kab_tujuh" /> 
                                        <input type="hidden" id="val_nama_kec_tujuh" /> 
                                        <input type="hidden" id="val_nama_kel_tujuh" /> 
                                        <input type="hidden" id="val_src_alamat_tujuh" /> 
                                        <input type="hidden" id="val_src_rt_tujuh" /> 
                                        <input type="hidden" id="val_src_rw_tujuh" /> 
                                        <input type="hidden" id="val_kec_tujuh" /> 
                                        <input type="hidden" id="val_kel_tujuh" /> 
                                        <input type="hidden" id="val_alamat_tujuh" /> 
                                        <input type="hidden" id="val_no_rt_tujuh" /> 
                                        <input type="hidden" id="val_no_rw_tujuh" /> 
                                        <input type="hidden" id="val_telepon_tujuh" /> 
                                        <input type="hidden" id="val_email_tujuh" /> 
                                        <input type="hidden" id="val_status_tujuh" /> 
                                        <input type="hidden" id="val_tgl_berlaku_tujuh" /> 
                                        <input type="hidden" id="val_jml_tujuh" /> 
                                    </div>
                                </div>
                                <div class="col-md-6 col-xs-12" id="head_delapan" style="margin-top: 15px !important">
                                    <div class="col-lg-6">         
                                        <div class="form-group">
                                            <div class="checkbox checkbox-info align-bottom">
                                                <input id="cb_nik_delapan" type="checkbox">
                                                <label for="cb_nik_delapan"> Posisi 8</label>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <div class="col-lg-6">         
                                        <div class="form-group input-group">
                                            <input class="form-control" type="text" id="nik_delapan" name="nik_delapan" onkeypress="return isNumberKey(event)" disabled/> 
                                            <span class="input-group-btn"> 
                                                <button class="btn btn-info" type="button" id="btn_delapan" name="btn_delapan" onclick="get_nik_delapan();" disabled><i class="ti-search"></i></button> 
                                            </span> 
                                        </div>        
                                    </div>
                                    <div class="col-md-12 col-xs-12" id="box_delapan" style="display: none;">
                                    <img src="{{ url('/') }}/assets/plugins/images/pemkot.png" alt="user" id="img_delapan" class="col-md-3 col-xs-6" height="160px" >
                                       <h5>Nik :  <span id="txt_nik_delapan">-</span></h5>
                                       <h5>Nama Lengkap :  <span id="txt_nama_lgkp_delapan">-</span></h5>
                                       <h5>Tempat Lahir :  <span id="txt_tmpt_lhr_delapan">-</span></h5>
                                       <h5>Tanggal Lahir :  <span id="txt_tgl_lhr_delapan">-</span></h5>
                                       <h5>Jumlah Cetak :  <span id="txt_jml_delapan">-</span></h5>
                                        <input type="hidden" id="val_rn_delapan" val="1"/> 
                                        <input type="hidden" id="val_posisi_delapan" val="1"/> 
                                        <input type="hidden" id="val_no_skts_delapan" /> 
                                        <input type="hidden" id="val_nik_delapan" /> 
                                        <input type="hidden" id="val_nama_lgkp_delapan" /> 
                                        <input type="hidden" id="val_tmpt_lhr_delapan" /> 
                                        <input type="hidden" id="val_tgl_lhr_delapan" /> 
                                        <input type="hidden" id="val_jenis_klmin_delapan" /> 
                                        <input type="hidden" id="val_pic_delapan" /> 
                                        <input type="hidden" id="val_agama_delapan" /> 
                                        <input type="hidden" id="val_stat_kwn_delapan" /> 
                                        <input type="hidden" id="val_gol_drh_delapan" /> 
                                        <input type="hidden" id="val_pendidikan_delapan" /> 
                                        <input type="hidden" id="val_pekerjaan_delapan" /> 
                                        <input type="hidden" id="val_nama_prop_delapan" /> 
                                        <input type="hidden" id="val_nama_kab_delapan" /> 
                                        <input type="hidden" id="val_nama_kec_delapan" /> 
                                        <input type="hidden" id="val_nama_kel_delapan" /> 
                                        <input type="hidden" id="val_src_alamat_delapan" /> 
                                        <input type="hidden" id="val_src_rt_delapan" /> 
                                        <input type="hidden" id="val_src_rw_delapan" /> 
                                        <input type="hidden" id="val_kec_delapan" /> 
                                        <input type="hidden" id="val_kel_delapan" /> 
                                        <input type="hidden" id="val_alamat_delapan" /> 
                                        <input type="hidden" id="val_no_rt_delapan" /> 
                                        <input type="hidden" id="val_no_rw_delapan" /> 
                                        <input type="hidden" id="val_telepon_delapan" /> 
                                        <input type="hidden" id="val_email_delapan" /> 
                                        <input type="hidden" id="val_status_delapan" /> 
                                        <input type="hidden" id="val_tgl_berlaku_delapan" /> 
                                        <input type="hidden" id="val_jml_delapan" /> 
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="margin-top: 15px !important">
                                <button type="submit" name="do_search" class="btn btn-info waves-effect waves-light m-r-10 pull-right" id="btn-filter" onclick="on_cetak();" >Cetak <i class="mdi mdi-printer fa-fw"></i></button>
                                  <button type="button" class="btn btn-invert waves-effect waves-light m-r-10 pull-right" id="btn-reset" onclick="on_clear();">Clear <i class="mdi  mdi-delete fa-fw"></i></button>
                            </div>
                        </div>
                    </div>

            </div>
        </div>

       @include('shared.footer_detail')