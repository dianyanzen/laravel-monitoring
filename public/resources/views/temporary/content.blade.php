<div id="page-wrapper">
            <div class="container-fluid">
               <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $stitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                
                        <ol class="breadcrumb">
                            <li><a href="{{ url('/') }}/<?php echo $backurl; ?>"><?php echo $back_title; ?></a></li>
                           <li class="text-info"><?php echo $stitle; ?></li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- .row -->
            
                <div class="row">
            <div class="col-sm-12">
                        <div class="white-box">
                            <h3 class="box-title m-b-0"><?php echo $stabletitle; ?></h3>
                            <p class="text-muted m-b-30">Dinas Kependudukan Dan Pencatatan Sipil Kota Bandung</p>
                            <div class="table-responsive">
                                <table id="mytable" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                   <thead>
                                        <?php if($status_tmp == 0){ ?>
                                         <tr>
                                            <th width="10%" style="text-align: right;">No Kec</th>
                                            <th width="45%" style="text-align: left;">Nama Kecamatan</th>
                                            <th width="15%" style="text-align: center;">Tanggal</th>
                                            <th width="10%" style="text-align: right;">Jumlah</th>
                                        </tr>
                                        <?php }else if($status_tmp == 1){ ?>
                                        <tr>
                                            <th width="10%" style="text-align: right;">No Kec</th>
                                            <th width="60%" style="text-align: left;">Nama Kecamatan</th>
                                            <th width="20%" style="text-align: center;">Tanggal</th>
                                            <th width="10%" style="text-align: right;">Jumlah</th>
                                        </tr>
                                        <?php }else if($status_tmp == 2){ ?>
                                        <tr>
                                            <th width="10%" style="text-align: right;">No Kec</th>
                                            <th width="40%" style="text-align: left;">Nama Kecamatan</th>
                                            <th width="20%" style="text-align: center;">Tanggal</th>
                                            <th width="15%" style="text-align: right;">Jumlah</th>
                                            <th width="15%" style="text-align: right;">Jumlah Anggota</th>
                                        </tr>
                                        <?php }else if($status_tmp == 3){ ?>
                                        <tr>
                                            <th width="20%" style="text-align: center;"><?php $col1 = (!empty($col1)) ? $col1 : "-"; echo $col1;?></th>
                                            <th width="20%" style="text-align: center;"><?php $col2 = (!empty($col2)) ? $col2 : "-"; echo $col2;?></th>
                                            <th width="20%" style="text-align: center;"><?php $col3 = (!empty($col3)) ? $col3 : "-"; echo $col3;?></th>
                                            <th width="20%" style="text-align: center;"><?php $col4 = (!empty($col4)) ? $col4 : "-"; echo $col4;?></th>
                                            <th width="20%" style="text-align: center;"><?php $col5 = (!empty($col5)) ? $col5 : "-"; echo $col5;?></th>
                                        </tr>
                                        <?php } ?>
                                    </thead>
                                    <tbody id="show_data">
                                        <?php 
                                            $jumlah = 0;
                                            $jumlah_anggota = 0;
                                            if($status_tmp == 0){ ?>
                                          <?php foreach($data as $row){?>
                                             <tr>
                                                <td width="10%" style="text-align: right;"><?php echo $row->no_kec ;?></td>
                                                <td width="45%" style="text-align: left;"><?php echo $row->nama_kec ;?></td>
                                                <td width="15%" style="text-align: center;"><?php echo $row->tanggal ;?></td>
                                                <td width="10%" style="text-align: right;"><?php echo $row->jumlah;?></td>
                                                <?php $jumlah = $jumlah + $row->jumlah; ?>
                                            </tr>
                                         <?php } ?>
                                        <?php }else if($status_tmp == 1){ ?>
                                         <?php foreach($data as $row){?>
                                             <tr>
                                                <td width="10%" style="text-align: right;"><?php echo $row->no_kec ;?></td>
                                                <td width="60%" style="text-align: left;"><?php echo $row->nama_kec ;?></td>
                                                <td width="20%" style="text-align: center;"><?php echo $row->tanggal ;?></td>
                                                <td width="10%" style="text-align: right;"><?php echo $row->jumlah;?></td>
                                                <?php $jumlah = $jumlah + $row->jumlah; ?>
                                            </tr>
                                         <?php } ?>
                                        <?php }else if($status_tmp == 2){ ?>
                                         <?php foreach($data as $row){?>
                                             <tr>
                                                <td width="10%" style="text-align: right;"><?php echo $row->no_kec ;?></td>
                                                <td width="40%" style="text-align: left;"><?php echo $row->nama_kec ;?></td>
                                                <td width="20%" style="text-align: center;"><?php echo $row->tanggal ;?></td>
                                                <td width="15%" style="text-align: right;"><?php echo $row->jumlah;?></td>
                                                <td width="15%" style="text-align: right;"><?php $jumlah_anggota = (!empty($row->jumlah_anggota)) ? $row->jumlah_anggota : 0; echo $jumlah_anggota;?></td>
                                                <?php $jumlah = $jumlah + $row->jumlah; ?>
                                                <?php $jumlah_anggota = $jumlah_anggota + $row->jumlah_anggota; ?>
                                            </tr>
                                          <?php } ?>
                                        <?php }else if($status_tmp == 3){ ?>
                                            <?php foreach($data as $row){?>
                                             <tr>
                                                <td width="20%" style="text-align: center;"><?php echo $row->capil_1 ;?></td>
                                                <td width="20%" style="text-align: center;"><span style="color: #fff">,</span><?php echo $row->capil_2 ;?></td>
                                                <td width="20%" style="text-align: center;"><span style="color: #fff">,</span><?php echo $row->capil_3 ;?></td>
                                                <td width="20%" style="text-align: center;"><?php echo $row->capil_4 ;?></td>
                                                <td width="20%" style="text-align: center;"><?php echo $row->capil_5 ;?></td>
                                            </tr>
                                          <?php } ?>
                                        <?php } ?>
                                        
                                    </tbody>
                                    
                                        <?php if($status_tmp == 0){ ?>
                                        <tfoot>
                                             <tr>
                                                <th width="70%" colspan="3" style="text-align: center;">Jumlah</th>
                                                <th width="10%" style="text-align: right;"><?php $jumlah;?></th>
                                            </tr>
                                        </tfoot>
                                        <?php }else if($status_tmp == 1){ ?>
                                        <tfoot>
                                        <tr>
                                            <th width="90%" colspan="3" style="text-align: center;">Jumlah</th>
                                            <th width="10%" style="text-align: right;"><?php echo $jumlah;?></th>
                                        </tr>
                                        </tfoot>
                                        <?php }else if($status_tmp == 2){ ?>
                                        <tfoot>
                                        <tr>
                                            <th width="70%" colspan="3" style="text-align: center;">Jumlah</th>
                                            <th width="15%" style="text-align: right;"><?php echo $jumlah;?></th>
                                            <th width="15%" style="text-align: right;"><?php echo $jumlah_anggota;?></th>
                                        </tr>
                                        </tfoot>
                                        <?php } ?>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
           @include('shared.footer_detail')
        </div>