 <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $stitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                        <ol class="breadcrumb">
                           <li class="text-info"><?php echo $stitle; ?></li>
                        </ol>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                        <div class="white-box">
                            <div class="row">
                                <div class="col-xs-6">
                                    <h2 class="font-medium m-t-0">Lihat User Menu <small><span id="group_name"></span></small></h2>
                                </div>
                            </div>
                            <div class="row" style="margin-top:  5px">
                                <div class="col-lg-4">         
                                    <div class="form-group">
                                        <h3 class="box-title m-b-0"><b> Menu ID</b></h3>
                                        <input class="form-control" value="<?php if (!empty($chead)){echo $chead[0]->menu_id; }?>" type="text" id="menu_id" name="menu_id" onkeypress="return isNumberKey(event)" readonly/>
                                    </div>
                                 </div>
                                 <div class="col-lg-8">         
                                    <div class="form-group">
                                        <h3 class="box-title m-b-0"><b> Title</b></h3>
                                        <input class="form-control" value="<?php if (!empty($chead)){echo $chead[0]->title; }?>" type="text" id="title" name="title" readonly/>
                                    </div>
                                 </div>
                                <div class="col-lg-4">         
                                    <div class="form-group">
                                        <h3 class="box-title m-b-0"><b> Parent ID</b></h3>
                                        <input class="form-control" value="<?php if (!empty($chead)){echo $chead[0]->parent_id; }?>" type="text" id="parent_id" name="parent_id" onkeypress="return isNumberKey(event)" readonly/>
                                    </div>
                                 </div>
                                 <div class="col-lg-8">         
                                    <div class="form-group">
                                        <h3 class="box-title m-b-0"><b> Parent Title</b></h3>
                                        <input class="form-control" value="<?php if (!empty($chead)){echo $chead[0]->parent_title; }?>" type="text" id="parent_title" name="parent_title" readonly/>
                                    </div>
                                 </div>

                                <div class="col-lg-4">         
                                    <div class="form-group">
                                        <h3 class="box-title m-b-0"><b> Menu Level</b></h3>
                                        <input class="form-control" value="<?php if (!empty($chead)){echo $chead[0]->menu_level; }?>" type="text" id="menu_level" name="menu_level" onkeypress="return isNumberKey(event)" readonly/>
                                    </div>
                                 </div>

                                <div class="col-lg-4">         
                                    <div class="form-group">
                                        <h3 class="box-title m-b-0"><b> Level Head</b></h3>
                                        <input class="form-control" value="<?php if (!empty($chead)){echo $chead[0]->level_head; }?>" type="text" id="level_head" name="level_head" onkeypress="return isNumberKey(event)" readonly/>
                                    </div>
                                 </div>

                                <div class="col-lg-4">         
                                    <div class="form-group">
                                        <h3 class="box-title m-b-0"><b> Level Sub</b></h3>
                                        <input class="form-control" value="<?php if (!empty($chead)){echo $chead[0]->level_sub; }?>" type="text" id="level_sub" name="level_sub" onkeypress="return isNumberKey(event)" readonly/>
                                    </div>
                                 </div>
                        </div>
                        <div class="row">

                                  <button type="button" class="btn btn-invert waves-effect waves-light m-r-10 pull-right" id="btn-back" onclick="on_back();">Back <i class="mdi  mdi-backspace fa-fw"></i></button>
                                  
                            </div>
                        </div>
                    </div>
                </div>
                

            </div>
       @include('shared.footer_detail')