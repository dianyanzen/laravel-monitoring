     <script>
        function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }
    $("#txt_receive").keydown(function (e) {
            if (e.keyCode == 13) {
             e.preventDefault();
             on_save();
            }
            
          });
    $(document).ready(function() {
        console.log(init_kec);
        console.log(init_level);
        $('input[type=text]').on('input', function(evt) {
            $(this).val(function(_, val) {
            return val.toUpperCase();
            });
        });
        $('#txt_nik').on('keyup',function(){
              var my_txt = $(this).val();
              var len = my_txt.length;
              if(len == 16)
              {
                  $('#hist_cetak tbody').empty();
                  $('#hist_delivery tbody').empty();
                  get_nik_paste();
                  
              }
        });
        $('#txt_nik_cek').on('keyup',function(){
              var my_txt = $(this).val();
              var len = my_txt.length;
              if(len == 16)
              {
                  $('#hist_cetak tbody').empty();
                  $('#hist_delivery tbody').empty();
                  get_nik_paste_cek();
                  
              }
        });
    });
    function get_nik_paste(){
         var nik = $("#txt_nik");
         if (nik.val().length != 16) {                
                  swal("Warning!", "Input Nik Harus 16 Digit !", "warning");  
          }else{
                get_nik();
          }
    }
    function get_nik_paste_cek(){
         var nik = $("#txt_nik_cek");
         if (nik.val().length != 16) {                
                  swal("Warning!", "Input Nik Harus 16 Digit !", "warning");  
          }else{
                get_nik_cek();
          }
    }
    function get_idpengajuan_paste_cek(){
         var nik = $("#txt_idpengajuan_cek");
         if (nik.val().length == 0) {                
                  swal("Warning!", "Input Id Pengajuan Harus Di Isi !", "warning");  
          }else{
                get_idpengajuan_cek();
          }
    }
    function get_nik(){
        $.ajax({
                    type: "post",
                    url: BASE_URL+"Cetak_dinas/get_nik_kia",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        nik : $("#txt_nik").val()
                    },
                    beforeSend:
                    function () {
                       block_screen();
                    },
                    success: function (data) {
                        console.log(data);
                        

                        $('#txt_nama').val(data.nama);
                        $('#tanggal_cetak').val(data.tgl_cetak);
                        $('select[name="no_kec"]').empty();
                        $('select[name="no_kec"]').append('<option value="'+ data.no_kec +'">'+ data.nama_kec +'</option>');
                        $('select[name="no_kec"]').val(data.no_kec).trigger("change");
                        if (data.is_exists == 1){
                            $('#tbl_nik').text(data.nik);    
                            $('#tbl_nama_lgkp').text(data.nama);
                            $('#tbl_idpengajuan').text("-");
                            $('#tbl_jenis_klmin').text(data.jenis_klmin);    
                            $('#tbl_ttl').text(data.ttl);   
                            $('#tbl_umur').text(data.umur);   
                            $('#tbl_gol_drh').text(data.gol_drh);   
                            $('#tbl_agama').text(data.agama);   
                            $('#tbl_nik_ibu').text(data.nik_ibu);   
                            $('#tbl_nama_ibu').text(data.nama_ibu);   
                            $('#tbl_nik_ayah').text(data.nik_ayah);   
                            $('#tbl_nama_ayah').text(data.nama_ayah);   
                            $('#tbl_no_kk').text(data.no_kk);   
                            $('#tbl_provinsi').text(data.provinsi);   
                            $('#tbl_kabupaten').text(data.kabupaten);   
                            $('#tbl_kecamatan').text(data.kecamatan);   
                            $('#tbl_kelurahan').text(data.kelurahan);   
                            $('#tbl_alamat').text(data.alamat);   
                            $('#tbl_rtrw').text(data.rtrw);   
                            $('#tbl_kodepos').text(data.kodepos);   
                            $('#tbl_no_akta_lhr').text(data.no_akta_lhr);   
                            $('#tbl_anak_ke').text(data.anak_ke);
                            $('#tbl_tgl_pengajuan').text(data.tgl_pengajuan);
                            $('#tbl_petugas').text(data.petugas);
                            if(data.path != ''){
                              $("#disp_img").attr("src",data.path);
                            }else{
                              if(data.jenis_klmin == "LAKI-LAKI"){
                                $("#disp_img").attr("src","{{ url('/') }}/assets/plugins/images/male-placeholder.jpg");
                              }else{
                                $("#disp_img").attr("src","{{ url('/') }}/assets/plugins/images/female-placeholder.jpg");
                              }
                            }
                            

                            $("#txt_nama").attr("readonly", true); 
                            $('#txt_request').focus();
                        }else{
                             swal("Warning!", "Nik Belum Pernah Melakukan Pencetakan Kia, Mohon Cetak Terlebih Dahulu !", "warning");
                        }
                        
                    },
                    error:
                    function (data) {
                        unblock_screen();
                        swal("Warning!", "Nik Belum Pernah Melakukan Pencetakan Kia, Mohon Cetak Terlebih Dahulu !", "warning"); 

                    },
                    complete:
                    function (response) {
                        unblock_screen();
                        // $('select[name="no_kec"]').attr("disabled",false);
                    }
                });
        $.ajax({
                    type: "post",
                    url: BASE_URL+"Cetak_dinas/get_seqn_print_kia",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        nik : $("#txt_nik").val()
                    },
                    beforeSend:
                    function () {
                    },
                    success: function (data) {
                        console.log(data);
                        

                       
                        if (data.is_exists == 1){
                            $('#hist_cetak tbody').empty();
                            $("#hist_cetak tbody").append(data.table_cetak); 
                        }
                        
                    },
                    error:
                    function (data) {
                        swal("Warning!", "Nik Belum Pernah Melakukan Pencetakan Kia, Mohon Cetak Terlebih Dahulu !", "warning"); 

                    },
                    complete:
                    function (response) {
                        // $('select[name="no_kec"]').attr("disabled",false);
                    }
                });
        $.ajax({
                    type: "post",
                    url: BASE_URL+"Cetak_dinas/get_delivery_kia",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        nik : $("#txt_nik").val()
                    },
                    beforeSend:
                    function () {
                    },
                    success: function (data) {
                        console.log(data);
                        

                       
                        if (data.is_exists == 1){
                          $('#hist_delivery tbody').empty();
                            $("#hist_delivery tbody").append(data.table_delivery); 
                        }
                        
                    },
                    error:
                    function (data) {
                        swal("Warning!", "Nik Belum Pernah Melakukan Pencetakan Kia, Mohon Cetak Terlebih Dahulu !", "warning"); 

                    },
                    complete:
                    function (response) {
                        // $('select[name="no_kec"]').attr("disabled",false);
                    }
                });
    }
    function get_nik_cek(){
        var parent = $('embed#kia_file').parent();

       $.ajax({
                    type: "post",
                    url: BASE_URL+"Cetak_dinas/get_nik_kia",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        nik : $("#txt_nik_cek").val()
                    },
                    beforeSend:
                    function () {
                       block_screen();
                    },
                    success: function (data) {
                        console.log(data);
                        if (data.is_exists == 1){
                            $('#tbl_nik').text(data.nik);    
                            $('#tbl_nama_lgkp').text(data.nama);
                            $('#tbl_idpengajuan').text("-");
                            $('#tbl_jenis_klmin').text(data.jenis_klmin);    
                            $('#tbl_ttl').text(data.ttl);   
                            $('#tbl_umur').text(data.umur);   
                            $('#tbl_gol_drh').text(data.gol_drh);   
                            $('#tbl_agama').text(data.agama);   
                            $('#tbl_nik_ibu').text(data.nik_ibu);   
                            $('#tbl_nama_ibu').text(data.nama_ibu);   
                            $('#tbl_nik_ayah').text(data.nik_ayah);   
                            $('#tbl_nama_ayah').text(data.nama_ayah);   
                            $('#tbl_no_kk').text(data.no_kk);   
                            $('#tbl_provinsi').text(data.provinsi);   
                            $('#tbl_kabupaten').text(data.kabupaten);   
                            $('#tbl_kecamatan').text(data.kecamatan);   
                            $('#tbl_kelurahan').text(data.kelurahan);   
                            $('#tbl_alamat').text(data.alamat);   
                            $('#tbl_rtrw').text(data.rtrw);   
                            $('#tbl_kodepos').text(data.kodepos);   
                            $('#tbl_no_akta_lhr').text(data.no_akta_lhr);   
                            $('#tbl_anak_ke').text(data.anak_ke);
                            $('#tbl_tgl_pengajuan').text(data.tgl_pengajuan);
                            $('#tbl_petugas').text(data.petugas);
                            if(data.path != ''){
                              $("#disp_img").attr("src",data.path);
                            }else{
                              if(data.jenis_klmin == "LAKI-LAKI"){
                                $("#disp_img").attr("src","{{ url('/') }}/assets/plugins/images/male-placeholder.jpg");
                              }else{
                                $("#disp_img").attr("src","{{ url('/') }}/assets/plugins/images/female-placeholder.jpg");
                              }
                            }
                          
                        }else{
                             swal("Warning!", "Nik Belum Pernah Melakukan Pencetakan Kia, Mohon Cetak Terlebih Dahulu !", "warning");
                        }
                        
                    },
                    error:
                    function (data) {
                        unblock_screen();
                        swal("Warning!", "Nik Belum Pernah Melakukan Pencetakan Kia, Mohon Cetak Terlebih Dahulu !", "warning"); 

                    },
                    complete:
                    function (response) {
                        unblock_screen();
                        // $('select[name="no_kec"]').attr("disabled",false);
                    }
                });
        $.ajax({

                    type: "post",
                    url: BASE_URL+"Cetak_dinas/get_seqn_print_kia",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        nik : $("#txt_nik_cek").val()
                    },
                    beforeSend:
                    function () {
                    },
                    success: function (data) {
                        console.log(data);
                        

                       
                        if (data.is_exists == 1){
                           $('#hist_cetak tbody').empty();
                            $("#hist_cetak tbody").append(data.table_cetak); 
                            $("#pdf_kia").show(); 
                            $('embed#kia_file').remove();
                            parent.append(data.file_pdf);
                        }else{
                            $("#pdf_kia").hide(); 
                        }
                        
                    },
                    error:
                    function (data) {
                        swal("Warning!", "Nik Belum Pernah Melakukan Pencetakan Kia, Mohon Cetak Terlebih Dahulu !", "warning"); 

                    },
                    complete:
                    function (response) {
                        // $('select[name="no_kec"]').attr("disabled",false);
                    }
                });
        $.ajax({
                    type: "post",
                    url: BASE_URL+"Cetak_dinas/get_delivery_kia",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        nik : $("#txt_nik_cek").val()
                    },
                    beforeSend:
                    function () {
                    },
                    success: function (data) {
                        console.log(data);
                        

                       
                        if (data.is_exists == 1){
                          $('#hist_delivery tbody').empty();
                            $("#hist_delivery tbody").append(data.table_delivery); 
                        }
                        
                    },
                    error:
                    function (data) {
                        swal("Warning!", "Nik Belum Pernah Melakukan Pencetakan Kia, Mohon Cetak Terlebih Dahulu !", "warning"); 

                    },
                    complete:
                    function (response) {
                        // $('select[name="no_kec"]').attr("disabled",false);
                    }
                });
    }
    function get_idpengajuan_cek(){
        var parent = $('embed#kia_file').parent();

       $.ajax({
                    type: "post",
                    url: BASE_URL+"Cetak_dinas/get_idpengajuan_kia",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        idpengajuan : $("#txt_idpengajuan_cek").val()
                    },
                    beforeSend:
                    function () {
                       block_screen();
                    },
                    success: function (data) {
                        console.log(data);
                        if (data.is_exists == 1){
                            $('#tbl_nik').text(data.nik);    
                            $('#tbl_nama_lgkp').text(data.nama);
                            $('#tbl_idpengajuan').text(data.idpengajuan);
                            $('#tbl_jenis_klmin').text(data.jenis_klmin);    
                            $('#tbl_ttl').text(data.ttl);   
                            $('#tbl_umur').text(data.umur);   
                            $('#tbl_gol_drh').text(data.gol_drh);   
                            $('#tbl_agama').text(data.agama);   
                            $('#tbl_nik_ibu').text(data.nik_ibu);   
                            $('#tbl_nama_ibu').text(data.nama_ibu);   
                            $('#tbl_nik_ayah').text(data.nik_ayah);   
                            $('#tbl_nama_ayah').text(data.nama_ayah);   
                            $('#tbl_no_kk').text(data.no_kk);   
                            $('#tbl_provinsi').text(data.provinsi);   
                            $('#tbl_kabupaten').text(data.kabupaten);   
                            $('#tbl_kecamatan').text(data.kecamatan);   
                            $('#tbl_kelurahan').text(data.kelurahan);   
                            $('#tbl_alamat').text(data.alamat);   
                            $('#tbl_rtrw').text(data.rtrw);   
                            $('#tbl_kodepos').text(data.kodepos);   
                            $('#tbl_no_akta_lhr').text(data.no_akta_lhr);   
                            $('#tbl_anak_ke').text(data.anak_ke);
                            $('#tbl_tgl_pengajuan').text(data.tgl_pengajuan);
                            $('#tbl_petugas').text(data.petugas);
                            if(data.path != ''){
                              $("#disp_img").attr("src",data.path);
                            }else{
                              if(data.jenis_klmin == "LAKI-LAKI"){
                                $("#disp_img").attr("src","{{ url('/') }}/assets/plugins/images/male-placeholder.jpg");
                              }else{
                                $("#disp_img").attr("src","{{ url('/') }}/assets/plugins/images/female-placeholder.jpg");
                              }
                            }
                          
                        }else{
                             swal("Warning!", "Nik Belum Pernah Melakukan Pencetakan Kia, Mohon Cetak Terlebih Dahulu !", "warning");
                        }
                        
                    },
                    error:
                    function (data) {
                        unblock_screen();
                        swal("Warning!", "Nik Belum Pernah Melakukan Pencetakan Kia, Mohon Cetak Terlebih Dahulu !", "warning"); 

                    },
                    complete:
                    function (response) {
                        unblock_screen();
                        // $('select[name="no_kec"]').attr("disabled",false);
                    }
                });
        $.ajax({

                    type: "post",
                    url: BASE_URL+"Cetak_dinas/get_seqn_print_kia_id",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        idpengajuan : $("#txt_idpengajuan_cek").val()
                    },
                    beforeSend:
                    function () {
                    },
                    success: function (data) {
                        console.log(data);
                        

                       
                        if (data.is_exists == 1){
                           $('#hist_cetak tbody').empty();
                            $("#hist_cetak tbody").append(data.table_cetak); 
                            $("#pdf_kia").show(); 
                            $('embed#kia_file').remove();
                            parent.append(data.file_pdf);
                        }else{
                            $("#pdf_kia").hide(); 
                        }
                        
                    },
                    error:
                    function (data) {
                        swal("Warning!", "Nik Belum Pernah Melakukan Pencetakan Kia, Mohon Cetak Terlebih Dahulu !", "warning"); 

                    },
                    complete:
                    function (response) {
                        // $('select[name="no_kec"]').attr("disabled",false);
                    }
                });
        $.ajax({
                    type: "post",
                    url: BASE_URL+"Cetak_dinas/get_delivery_kia_id",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        idpengajuan : $("#txt_idpengajuan_cek").val()
                    },
                    beforeSend:
                    function () {
                    },
                    success: function (data) {
                        console.log(data);
                        

                       
                        if (data.is_exists == 1){
                          $('#hist_delivery tbody').empty();
                            $("#hist_delivery tbody").append(data.table_delivery); 
                        }
                        
                    },
                    error:
                    function (data) {
                        swal("Warning!", "Nik Belum Pernah Melakukan Pencetakan Kia, Mohon Cetak Terlebih Dahulu !", "warning"); 

                    },
                    complete:
                    function (response) {
                        // $('select[name="no_kec"]').attr("disabled",false);
                    }
                });
    }
    function on_clear() {
        
        // $('select[name="no_kec"]').val(init_kec).trigger("change");
        // $('#txt_pengambilan').val("0");
        // $('#txt_rusak').val("0");
        $('#txt_nik').val("");
        $('#txt_idpengajuan_cek').val("");
        $('#txt_nik_cek').val("");
        $('#txt_nama').val("");
        $("#txt_nama").attr("readonly", true);
        $('select[name="no_kec"]').empty();
        $('select[name="no_kec"]').append('<option value="0">-- Pilih Kecamatan --</option>');
        $('select[name="no_kec"]').val(0).trigger("change");
        $('select[name="loc_id"]').val(0).trigger("change");
        $('select[name="pengambilan_id"]').val(0).trigger("change");
        $('#txt_request').val("");
        $('#txt_receive').val("");
        $('#tanggal_cetak').val("");
        $('#txt_nik').focus();

        $('#tbl_nik').text("-");    
        $('#tbl_nama_lgkp').text("-");
        $('#tbl_idpengajuan').text("-");
        $('#tbl_jenis_klmin').text("-");    
        $('#tbl_ttl').text("-");   
        $('#tbl_umur').text("-");   
        $('#tbl_gol_drh').text("-");   
        $('#tbl_agama').text("-");   
        $('#tbl_nik_ibu').text("-");   
        $('#tbl_nama_ibu').text("-");   
        $('#tbl_nik_ayah').text("-");   
        $('#tbl_nama_ayah').text("-");   
        $('#tbl_no_kk').text("-");   
        $('#tbl_provinsi').text("-");   
        $('#tbl_kabupaten').text("-");   
        $('#tbl_kecamatan').text("-");   
        $('#tbl_kelurahan').text("-");   
        $('#tbl_alamat').text("-");   
        $('#tbl_rtrw').text("-");   
        $('#tbl_kodepos').text("-");   
        $('#tbl_no_akta_lhr').text("-");   
        $('#tbl_anak_ke').text("-");
        $('#tbl_tgl_pengajuan').text("-");
        $('#tbl_petugas').text("-");
        $("#disp_img").attr("src","{{ url('/') }}/assets/plugins/images/calming-cat.gif");

        $('#hist_cetak tbody').empty();
        $('#hist_delivery tbody').empty();
        var parent = $('embed#kia_file').parent();
        $("#pdf_kia").hide(); 
        $('embed#kia_file').remove();
        // get_all_data();
    }
     function on_save(){
        if (validationdaily()){
          do_save();
          // alert("Suceess");
        }
  
        
    }
    function validationdaily() {
        var nik = $("#txt_nik");
        var nama = $("#txt_nama");
        var requset = $("#txt_request");
        var receive = $("#txt_receive");
        var location_id = $("#loc_id");
        var pengambilan_id = $("#pengambilan_id");
        var tanggal_cetak = $("#tanggal_cetak");
        console.log(nik);
            if (nik.val().length != 16) {                
                  swal("Warning!", "Input Nik Harus 16 Digit !", "warning");  
                 return false;
            }
            if (nama.val().length == 0) {                
                  swal("Warning!", "Nama Tidak Boleh Kosong !", "warning");  
                 return false;
            }
            if (requset.val().length == 0) {                
                  swal("Warning!", "Request Tidak Boleh Kosong !", "warning");  
                 return false;
            }
            if (receive.val().length == 0) {                
                  swal("Warning!", "Receive Tidak Boleh Kosong !", "warning");  
                 return false;
            }
            if (location_id.val() == 0) {                
                  swal("Warning!", "Lokasi Tidak Boleh Kosong !", "warning");  
                 return false;
            }
            if (location_id.val().length == 0) {                
                  swal("Warning!", "Lokasi Tidak Boleh Kosong !", "warning");  
                 return false;
            }
            if (pengambilan_id.val() == 0) {                
                  swal("Warning!", "Jenis Pengambilan Tidak Boleh Kosong !", "warning");  
                 return false;
            }
            if (pengambilan_id.val().length == 0) {                
                  swal("Warning!", "Jenis Pengambilan Tidak Boleh Kosong !", "warning");  
                 return false;
            }
            if (tanggal_cetak.val().length == 0) {                
                  swal("Warning!", "Tanggal Cetak Tidak Boleh Kosong !", "warning");  
                 return false;
            }
           
          
            return true;
       
        }
        function do_save(){
            $.ajax({
                    type: "post",
                    url: BASE_URL+"Cetak_dinas/do_save_kia",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        nik : $("#txt_nik").val(),
                        nama : $("#txt_nama").val(),
                        no_kec : $('select[name="no_kec"]').val(),
                        nama_kec : $('select[name="no_kec"]').text(),
                        location_id : $('select[name="loc_id"]').val(),
                        pengambilan_id : $('select[name="pengambilan_id"]').val(),
                        request_by : $("#txt_request").val(),
                        receive : $("#txt_receive").val(),
                        tanggal : $("#tanggal").val(),
                        tanggal_cetak : $("#tanggal_cetak").val()
                    },
                    beforeSend:
                    function () {
                         block_screen();
                    },
                    success: function (data) {
                        console.log(data);
                        swal("Success!", data.message, "success");
                    },
                    error:
                    function (data) {
                        unblock_screen();
                        swal("Warning!", "Nik Belum Pernah Melakukan Pencetakan Kia, Mohon Cetak Terlebih Dahulu !", "warning"); 

                    },
                    complete:
                    function (response) {
                        unblock_screen();
                        on_clear();
                    }
                });
        }
    function block_screen(){
        
        $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff'
            },
            baseZ: 2000
        }); 
    }
    function unblock_screen(){
       $.unblockUI();
    }
   jQuery(document).ready(function() {
            $(".select2").select2();
             $('#mytable').DataTable({
                 "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
           $('#tanggal').val("<?php echo date('d-m-Y');?>");
        });
    </script>
    </script>
   