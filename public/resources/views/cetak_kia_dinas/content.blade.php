 <style type="text/css">
     textarea {
  resize: none;
  height: 155px !important;
}
 </style>
 <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $stitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                        <ol class="breadcrumb">
                            <li class="active"><?php echo $mtitle; ?></li>
                        </ol>
                    </div>
                </div>
                <div class="row">
                <!-- <form name ="get_form" action="<?php echo $my_url; ?>" method="post"> -->
                    <div class="col-md-12 col-xs-12">
                        <div class="white-box">
                        <ul class="nav nav-tabs tabs customtab">
                                    <li class="active tab">
                                        <a href="#master_vw" data-toggle="tab"> <span class="visible-xs"><i class="mdi mdi-auto-fix"></i></span> <span class="hidden-xs" style="color: blue !important">Cek Status KIA</span> </a> 
                                    </li>
                                    <li class="tab">
                                        <a href="#distribusi_vw" data-toggle="tab"> <span class="visible-xs"><i class="mdi mdi-arrow-up-box"></i></span> <span class="hidden-xs" style="color: blue !important">Form Distribusi KIA</span> </a> 
                                    </li>
                                
                                   
                                </ul>
                        </div>
                            </div>
                            <div class="tab-content">
                                <div class="tab-pane active" id="master_vw">
                                    <div class="col-sm-12">
                                        <div class="white-box">
                                            
                                                
                                                <div class="row">
                                                <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">         
                                                    <div class="form-group">
                                                        <div class="checkbox checkbox-info align-bottom">
                                                            <input id="cb_nik" type="checkbox" checked="" disabled="true">
                                                            <label for="cb_nik"> Nik</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">         
                                                      <div class="input-group">
                                        <input class="form-control" id="txt_nik_cek" type="text" maxlength="16" onkeypress="return isNumberKey(event)" ><span class="input-group-btn"><button type="button" onclick="get_nik_paste_cek();" class="btn waves-effect waves-light btn-info"><i class="fa fa-search"></i></button></span></div>
                                                </div>
                                                </div>
                                            
                                                
                                                <div class="row">
                                                <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">         
                                                    <div class="form-group">
                                                        <div class="checkbox checkbox-info align-bottom">
                                                            <input id="cb_nik" type="checkbox" checked="" disabled="true">
                                                            <label for="cb_nik"> No. Pengajuan</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">         
                                                      <div class="input-group">
                                        <input class="form-control" id="txt_idpengajuan_cek" type="text" maxlength="16" onkeypress="return isNumberKey(event)" ><span class="input-group-btn"><button type="button" onclick="get_idpengajuan_paste_cek();" class="btn waves-effect waves-light btn-info"><i class="fa fa-search"></i></button></span></div>
                                                </div>
                                                </div>
                                                <div class="row">
                                                      <button type="button" class="btn btn-invert waves-effect waves-light m-r-10 pull-right" id="btn-reset" onclick="on_clear();">Clear <i class="mdi  mdi-delete fa-fw"></i></button>
                                                </div>
                                           
                                            
                                        </div>
                                    </div>
                                </div>
                            <div class="tab-pane" id="distribusi_vw">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="white-box">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="row">
                                <div class="col-lg-6">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_nik" type="checkbox" checked disabled="true">
                                            <label for="cb_nik"> Nik</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">         
                                    <div class="input-group">
                                        <input class="form-control" id="txt_nik" type="text" maxlength="16" onkeypress="return isNumberKey(event)" ><span class="input-group-btn"><button type="button" onclick="get_nik_paste();" class="btn waves-effect waves-light btn-info"><i class="fa fa-search"></i></button></span></div>
                                </div>
                            </div>
                            <div class="row">
                               
                                <div class="col-lg-6">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_nama" type="checkbox" checked disabled="true" >
                                            <label for="cb_nama"> Nama</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">         
                                    <div class="form-group">
                                        <input class="form-control" id="txt_nama" type="text" readonly></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_kec" type="checkbox" checked disabled="true">
                                            <label for="cb_kec"> Kecamatan</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6">         
                                    <div class="form-group" <?php if (!empty($is_colnotwil)){echo 'style="display: none!important;"'; }?>>
                                        <select class="form-control select2" name="no_kec" id="no_kec" disabled="true">
                                         <option  value="0">-- Pilih Kecamatan --</option>
                                        </select>
                                    </div>
                                </div>
                                </div>
                            <div class="row">
                                <div class="col-lg-6">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_loc" type="checkbox" checked disabled="true">
                                            <label for="cb_loc"> Lokasi Pengambilan</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6">         
                                    <div class="form-group" <?php if (!empty($is_colnotwil)){echo 'style="display: none!important;"'; }?>>
                                        <select class="form-control select2" name="loc_id" id="loc_id">
                                         <option  value="0">-- Pilih Lokasi --</option>
                                         <option  value="DINAS">1 - DINAS</option>
                                         <option  value="FCL">2 - FCL</option>
                                         <option  value="BTC">3 - BTC</option>
                                         <option  value="MIM">4 - MIM</option>
                                         <option  value="DPRD">5 - DPRD</option>
                                         <option  value="MEPELING">6 - MEPELING</option>
                                         <option  value="LAINNYA">7 - LAINYA</option>
                                        </select>
                                    </div>
                                </div>
                                </div>
                            <div class="row">
                                <div class="col-lg-6">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_loc" type="checkbox" checked disabled="true">
                                            <label for="cb_loc"> Jenis Pengambilan</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6">         
                                    <div class="form-group" <?php if (!empty($is_colnotwil)){echo 'style="display: none!important;"'; }?>>
                                        <select class="form-control select2" name="pengambilan_id" id="pengambilan_id">
                                         <option  value="0">-- Pilih Jenis Pengambilan --</option>
                                         <option  value="INDIVIDU">1 - Individu</option>
                                         <option  value="KOLEKTIF">2 - Kolektif</option>
                                         <option  value="GOJEK">3 - GOJEK Indonesia</option>
                                         <option  value="PT POS">4 - PT Pos Indonesia</option>
                                         <option  value="LAINNYA">5 - Lainnya</option>
                                        </select>
                                    </div>
                                </div>
                                </div>
                                </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <div class="row">
                                <div class="col-lg-6">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_req" type="checkbox" checked disabled="true">
                                            <label for="cb_req"> Request By</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6">         
                                    <div class="form-group">
                                        <input class="form-control" id="txt_request" type="text" ></div>
                                </div>
                                </div>
                                <div class="row">
                                <div class="col-lg-6">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_receive" type="checkbox" checked disabled="true">
                                            <label for="cb_receive"> Received By</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6">         
                                    <div class="form-group">
                                        <input class="form-control" id="txt_receive" type="text" ></div>
                                </div>
                                </div>
                                <div class="row">
                                <div class="col-lg-6">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_tgl" type="checkbox" checked disabled="true">
                                            <label for="cb_tgl"> Tanggal Pengambilan</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6">         
                                    <div class="form-group">
                                        <input class="form-control" id="tanggal" placeholder="dd/mm/yyyy" type="text" value="<?php echo date("d-m-Y") ?>" readonly></div>
                                </div>
                                </div>
                                <div class="row">
                                <div class="col-lg-6">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_tgl" type="checkbox" checked disabled="true">
                                            <label for="cb_tgl"> Tanggal Cetak</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6">         
                                    <div class="form-group">
                                        <input class="form-control" id="tanggal_cetak" placeholder="--/--/----" type="text" value="" readonly></div>
                                </div>
                                </div>
                            </div>
                            <div class="row">
                                <button type="submit" id="btn_dosave" class="btn btn-info waves-effect waves-light m-r-10 pull-right" id="btn-filter" onclick="on_save();" >Save <i class="mdi  mdi-content-save fa-fw"></i></button>
                                  <button type="button" class="btn btn-invert waves-effect waves-light m-r-10 pull-right" id="btn-reset" onclick="on_clear();">Clear <i class="mdi  mdi-delete fa-fw"></i></button>
                            </div>
                        
                        </div>
                        </div>
                    </div>
                    </div>
                </div>
             
                <div class="row">
                    <div class="col-lg-12">
                        <div class="white-box">
                            <div class="">
                                <h2 class="m-b-0 m-t-0">Data Kia (Kartu Identitas Anak)</h2>
                                <hr>
                                <div class="row el-element-overlay m-b-40">
                                    <div class="col-lg-3 col-md-3 col-sm-6">
                                    <div class="white-box">
                                       <div class="el-card-item">
                                            <div class="el-card-avatar el-overlay-1"> <img id="disp_img" src="{{ url('/') }}/assets/plugins/images/calming-cat.gif" />
                                                <div class="el-overlay">
                                                    <ul class="el-info">
                                                        <li><a class="btn default btn-outline image-popup-vertical-fit" href="{{ url('/') }}/assets/plugins/images/male-placeholder.jpg"><i class="icon-magnifier"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            </div>
                                    </div>
                                    </div>
                                    <div class="col-lg-9 col-md-9 col-sm-6">
                                        <div class="row">
                                        <table class="table" >
                                                <tbody>
                                                    <tr>
                                                        <td ><h4>NO. PENGAJUAN</h4></td>
                                                        <td> <h4>:</h4> </td>
                                                        <td> <h4 id="tbl_idpengajuan">-</h4> </td>
                                                        <td></td>
                                                        <td ></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td ><h4>NIK</h4></td>
                                                        <td> <h4>:</h4> </td>
                                                        <td> <h4 id="tbl_nik">-</h4> </td>
                                                        <td></td>
                                                        <td ><h4>Nama Lengkap</h4></td>
                                                        <td> <h4>:</h4> </td>
                                                        <td> <h4 id="tbl_nama_lgkp">-</h4> </td>
                                                    </tr>
                                                    <tr>
                                                        <td ><h4>Jenis Kelamin</h4></td>
                                                        <td> <h4>:</h4> </td>
                                                        <td> <h4 id="tbl_jenis_klmin">-</h4> </td>
                                                        <td></td>
                                                        <td ><h4>Tempat/Tanggal lahir</h4></td>
                                                        <td> <h4>:</h4> </td>
                                                        <td> <h4 id="tbl_ttl">-</h4> </td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>Umur</h4></td>
                                                        <td> <h4>:</h4> </td>
                                                        <td colspan="5"> <h4 id="tbl_umur">-</h4> </td>
                                                    </tr>
                                                    <tr>
                                                        <td ><h4>Gol. Darah</h4></td>
                                                        <td> <h4>:</h4> </td>
                                                        <td> <h4 id="tbl_gol_drh">-</h4> </td>
                                                        <td></td>
                                                        <td ><h4>Agama</h4></td>
                                                        <td> <h4>:</h4> </td>
                                                        <td> <h4 id="tbl_agama">-</h4> </td>
                                                    </tr>
                                                    <tr>
                                                        <td ><h4>NIK Ibu</h4></td>
                                                        <td> <h4>:</h4> </td>
                                                        <td> <h4 id="tbl_nik_ibu">-</h4> </td>
                                                        <td></td>
                                                        <td ><h4>Nama Ibu</h4></td>
                                                        <td> <h4>:</h4> </td>
                                                        <td> <h4 id="tbl_nama_ibu">-</h4> </td>
                                                    </tr>
                                                    <tr>
                                                        <td ><h4>NIK Ayah</h4></td>
                                                        <td> <h4>:</h4> </td>
                                                        <td> <h4 id="tbl_nik_ayah">-</h4> </td>
                                                        <td></td>
                                                        <td ><h4>Nama Ayah</h4></td>
                                                        <td> <h4>:</h4> </td>
                                                        <td> <h4 id="tbl_nama_ayah">-</h4> </td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>No. KK</h4></td>
                                                        <td> <h4>:</h4> </td>
                                                        <td colspan="5"> <h4 id="tbl_no_kk">-</h4> </td>
                                                    </tr>

                                                    <tr>
                                                        <td ><h4>Provinsi</h4></td>
                                                        <td> <h4>:</h4> </td>
                                                        <td> <h4 id="tbl_provinsi">-</h4> </td>
                                                        <td></td>
                                                        <td ><h4>Kabupaten</h4></td>
                                                        <td> <h4>:</h4> </td>
                                                        <td> <h4 id="tbl_kabupaten">-</h4> </td>
                                                    </tr>
                                                    <tr>
                                                        <td ><h4>Kecamatan</h4></td>
                                                        <td> <h4>:</h4> </td>
                                                        <td> <h4 id="tbl_kecamatan">-</h4> </td>
                                                        <td></td>
                                                        <td ><h4>Kelurahan</h4></td>
                                                        <td> <h4>:</h4> </td>
                                                        <td> <h4 id="tbl_kelurahan">-</h4> </td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>Alamat</h4></td>
                                                        <td> <h4>:</h4> </td>
                                                        <td colspan="5"> <h4 id="tbl_alamat">-</h4> </td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>RT / RW</h4></td>
                                                        <td> <h4>:</h4> </td>
                                                        <td> <h4 id="tbl_rtrw">-</h4> </td>
                                                        <td></td>
                                                        <td><h4>Kode Pos</h4></td>
                                                        <td> <h4>:</h4> </td>
                                                        <td> <h4 id="tbl_kodepos">-</h4> </td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>No.Akta Kelahiran</h4></td>
                                                        <td> <h4>:</h4> </td>
                                                        <td> <h4 id="tbl_no_akta_lhr">-</h4> </td>
                                                        <td></td>
                                                        <td><h4>Anak Ke</h4></td>
                                                        <td> <h4>:</h4> </td>
                                                        <td> <h4 id="tbl_anak_ke">-</h4> </td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>Tanggal Pengajuan</h4></td>
                                                        <td> <h4>:</h4> </td>
                                                        <td> <h4 id="tbl_tgl_pengajuan">-</h4> </td>
                                                        <td></td>
                                                        <td><h4>Petugas</h4></td>
                                                        <td> <h4>:</h4> </td>
                                                        <td> <h4 id="tbl_petugas">-</h4> </td>
                                                    </tr>
                                                    
                                                </tbody>
                                            </table>
                                            </div>
                                        

                                    </div>
                                    </div>
                                   
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="white-box">
                            <div class="">
                                <h2 class="m-b-0 m-t-0">History Pencetakan KIA (Kartu Identitas Anak)</h2>
                                <hr>
                                <div class="row el-element-overlay m-b-40">
                                    
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <div class="row">
                                        <table class="table table-bordered table-striped" id="hist_cetak">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center"><h3>#</h3></th>
                                                        <th class="text-center"><h3>NIK</h3></th>
                                                        <th class="text-center"><h3>NAMA LENGKAP</h3></th>
                                                        <th class="text-center"><h3>TANGGAL CETAK TERAKHIR</h3></th>
                                                        <th class="text-center"><h3>PETUGAS CETAK</h3></th>
                                                        <th class="text-center"><h3>FILE PDF</h3></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    
                                                    
                                                </tbody>
                                            </table>
                                            </div>
                                        

                                    </div>
                                    </div>
                                   
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" id="pdf_kia" style="display: none;">
                        <div class="col-md-12">
                            <div class="white-box">
                                <h2 class="m-b-0 m-t-0">Pdf KIA (Kartu Identitas Anak)</h2>
                                <hr>
                                <div class="row el-element-overlay m-b-40">
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <div class="row">
                            <embed src="" type="application/pdf"   height="500px" width="100%" id="kia_file" >
                                </div>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>



                <div class="row">
                    <div class="col-lg-12">
                        <div class="white-box">
                            <div class="">
                                <h2 class="m-b-0 m-t-0">History Pengambilan KIA (Kartu Identitas Anak)</h2>
                                <hr>
                                <div class="row el-element-overlay m-b-40">
                                    
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <div class="row">
                                        <table class="table table-bordered table-striped" id="hist_delivery">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center"><h3>#</h3></th>
                                                        <th class="text-center"><h3>NIK</h3></th>
                                                        <th class="text-center"><h3>NAMA LENGKAP</h3></th>
                                                        <th class="text-center"><h3>TANGGAL PENGAMBILAN</h3></th>
                                                        <th class="text-center"><h3>LOKASI</h3></th>
                                                        <th class="text-center"><h3>JENIS PENGAMBILAN</h3></th>
                                                        <th class="text-center"><h3>PEMOHON</h3></th>
                                                        <th class="text-center"><h3>PENERIMA</h3></th>
                                                        <th class="text-center"><h3>PETUGAS</h3></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    
                                                    
                                                </tbody>
                                            </table>
                                            </div>
                                        

                                    </div>
                                    </div>
                                   
                            </div>
                        </div>
                    </div>
                </div>
                </div>
              
                 </div>
       @include('shared.footer_detail')