<div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $stitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                        <ol class="breadcrumb">
                           <li class="text-info"><?php echo $stitle; ?></li>
                        </ol>
                    </div>
                </div>
                <!-- .row -->
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                    <div class="white-box">
                            <table style="width: 100% !important">
                                <tr>
                                    <td colspan="7" style="text-align: center; border: 1px solid #fff;">
                                    <b><h3 align="center" style="font-weight: bold; margin-top: 0; margin-bottom: 0;">Kartu Keluarga</h3></b>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="7" style="text-align: center; border: 1px solid #fff;">
                                    <b><h3 align="center" style="font-weight: bold; margin-top: 0;">NO. <?php echo $sno_kk; ?></h3></b>
                                    </td>
                                </tr>
                            </table>
                            <?php if (!empty($data_kk)){ 
                                foreach($data_kk as $row){
                            ?>
                            <div class="row">
                                <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                                    <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="text-align: center !important; lign-content: center !important">
                                        Alamat : <span style="font-weight: bold;"><?php echo $row->alamat ;?></span>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="text-align: center !important; lign-content: center !important">
                                        RT/RW : <span style="font-weight: bold;"><?php echo $row->no_rt ;?>/<?php echo $row->no_rw ;?></span>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="text-align: center !important; lign-content: center !important">
                                        Desa/Kelurahan : <span style="font-weight: bold;"><?php echo $row->nama_kel ;?></span>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="text-align: center !important; lign-content: center !important">
                                        Kecamatan : <span style="font-weight: bold;"><?php echo $row->nama_kec ;?></span>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="text-align: center !important; lign-content: center !important">
                                        Kabupaten/Kota : <span style="font-weight: bold;"><?php echo $row->nama_kab ;?></span>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="text-align: center !important; lign-content: center !important">
                                        Provinsi : <span style="font-weight: bold;"><?php echo $row->nama_prop ;?></span>
                                    </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                                        <a href="{{ url('/') }}/gisa?no_kec=<?php echo $row->no_kec ;?>&no_kel=<?php echo $row->no_kel ;?>&no_rw=<?php echo $row->no_rw ;?>&no_rt=<?php echo $row->no_rt ;?>" onclick="on_menu();"><span class="btn btn-outline btn-info btn-lg btn-block" style="margin-top: 20px !important;">Kembali</span></a>
                                </div>
                            </div>
                            <?php 
                                }
                                } 
                            ?>
                            
                        </div>
                        </div>
                        </div>
                        <div class="row">
                         <?php if (!empty($data_nik)){ 
                                foreach($data_nik as $row){
                            ?>
                            <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                            <div class="white-box">
                            <h3><b><?php echo $row->stat_hbkel ;?></b> <span class="pull-right"></span></h3>
                            <hr>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="pull-left">
                                        <address>
                                            <h3><b class="text-info"><?php echo $row->nama_lgkp ;?></b></h3>
                                             <h4 class="font-bold"><?php echo $row->nik ;?></h4>

                                            <p class="m-l-5">
                                                <b>Tempat Lahir : </b>&nbsp;<span class="font-bold"><?php echo $row->tmpt_lhr ?></span><br/>
                                                <b>Tanggal Lahir : </b> <i class="fa fa-calendar"></i>&nbsp;<span class="font-bold"><?php echo $row->tgl_lhr ?> (<?php echo $row->umur ?> Tahun)</span><br/>
                                                <b>Jenis Kelamin : </b>&nbsp;<span class="font-bold"><?php echo $row->jenis_klmin ?></span><br/>
                                                <b>Golongan Darah :</b>&nbsp;<span class="font-bold"><?php echo $row->gol_drh ?></span><br/> 
                                                <b>Agama :</b>&nbsp;<span class="font-bold"><?php echo $row->agama ?></span><br/> 
                                                <b>Status Kawin :</b>&nbsp;<span class="font-bold"><?php echo $row->status_kawin ?></span><br/> 
                                                <b>Pekerjaan :</b>&nbsp;<span class="font-bold"><?php echo $row->pekerjaan ?></span>
                                            </p>
                                             <h4 class="font-bold">Monitoring #GISA</h4>
                                            <p class="m-l-5">
                                                <b>Status KK : </b>&nbsp;<span class="font-bold"><?php echo $row->status_kk ?></span><br/>
                                                <b>Status Perekaman : </b>&nbsp;<span class="font-bold"><?php echo $row->status_rekam ?></span><br/>
                                                <b>Status KTP : </b>&nbsp;<span class="font-bold"><?php echo $row->status_ktp ?></span><br/>
                                                <b>Status Akta Kelahiran :</b>&nbsp;<span class="font-bold"><?php echo $row->status_akta_lhr ?></span><br/> 
                                                <b>No Akta Kelahiran :</b>&nbsp;<span class="font-bold"><?php echo $row->no_akta_lhr ?></span><br/> 
                                                <?php if($row->status_akta_kwn != '-'){ ?>
                                                <b>Status Akta Perkawinan :</b>&nbsp;<span class="font-bold"><?php echo $row->status_akta_kwn ?></span><br/> 
                                                <b>No Akta Perkawinan :</b>&nbsp;<span class="font-bold"><?php echo $row->no_akta_kwn ?></span><br/> 
                                                <?php } ?> 
                                                <?php if($row->status_akta_crai != '-'){ ?>
                                                <b>Status Akta Perceraian :</b>&nbsp;<span class="font-bold"><?php echo $row->status_akta_crai ?></span><br/> 
                                                <b>No Akta Perceraian :</b>&nbsp;<span class="font-bold"><?php echo $row->no_akta_crai ?></span><br/> 
                                                <?php } ?> 
                                                <?php if($row->status_kia != '-'){ ?>
                                                <b>Status KIA : </b>&nbsp;<span class="font-bold"><?php echo $row->status_kia ?></span>
                                                <?php } ?> 
                                            </p>
                                        </address>
                                    </div>
                                    <div class="pull-right text-right">
                                        <address>
                                           <a href="{{ url('/') }}/gisa/do_edit?nik=<?php echo $row->nik ;?>" onclick="on_menu();"><span  class="btn btn-info btn-sm" id="btn-detail" style="margin-top: 5px;">Ubah <i class="mdi mdi-tooltip-edit fa-fw"></i></span></a>
                                        </address>
                                    </div>
                                </div>
                            </div>
                        </div>
                            </div>
                            <?php 
                            }
                            }
                            ?>
                        </div>
                    <!-- .col -->
                    
           
                
                
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- /.container-fluid -->
         @include('shared.footer_detail')