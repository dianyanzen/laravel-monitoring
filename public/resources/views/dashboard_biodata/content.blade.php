<div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $stitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                        <ol class="breadcrumb">
                                <li><a href="{{ url('/') }}/<?php echo $backurl; ?>"><?php echo $back_title; ?></a></li>
                           <li class="text-info"><?php echo $stitle; ?></li>
                        </ol>
                    </div>
                </div>
               <div class="row">
                    <div class="col-lg-6 col-sm-12 col-xs-12">
                        <div class="row">
                            <div class="col-lg-12 col-sm-12 col-xs-12">
                                <div class="panel panel-info">
                                    <div class="panel-heading">Kartu Keluarga Hari Ini<div class="pull-right"><a href="#" data-perform="panel-collapse"><i class="ti-minus"></i></a></div>
                                         </div>
                                         <div class="panel-wrapper collapse in" aria-expanded="true">
                                        <div class="panel-body">
                                    <ul class="list-inline two-part">
                                        <li><i class="fa fa-users text-info"></i></li>
                                        <li class="text-right"><span class="counter"><a href ="#" onclick="get_detail_kk(0)"><span id="kk_n">0</span></a></span></li>
                                    </ul>
                                </div>
                                </div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-sm-12 col-xs-12">
                                <div class="panel panel-info">
                                    <div class="panel-heading">Biodata Hari Ini<div class="pull-right"><a href="#" data-perform="panel-collapse"><i class="ti-minus"></i></a></div>
                                         </div>
                                         <div class="panel-wrapper collapse in" aria-expanded="true">
                                        <div class="panel-body">
                                    <ul class="list-inline two-part">
                                        <li><i class="fa fa-user text-info"></i></li>
                                        <li class="text-right"><span class="counter"><a href ="#" onclick="get_detail_bio(0)"><span id="bio_n">0</span></a></span></li>
                                    </ul>
                                </div>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-12 col-xs-12">
                        <div class="row">
                            <div class="col-lg-12 col-sm-12 col-xs-12">
                                <div class="panel panel-info">
                                    <div class="panel-heading">Kartu Keluarga Kemarin<div class="pull-right"><a href="#" data-perform="panel-collapse"><i class="ti-minus"></i></a></div>
                                         </div>
                                         <div class="panel-wrapper collapse in" aria-expanded="true">
                                        <div class="panel-body">
                                    <ul class="list-inline two-part">
                                        <li><i class="fa fa-users text-info"></i></li>
                                        <li class="text-right"><span class="counter"><a href ="#" onclick="get_detail_kk(1)"><span id="kk_y">0</span></a></span></li>
                                    </ul>
                                </div>
                                </div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-sm-12 col-xs-12">
                               <div class="panel panel-info">
                                    <div class="panel-heading">Biodata Kemarin<div class="pull-right"><a href="#" data-perform="panel-collapse"><i class="ti-minus"></i></a></div>
                                         </div>
                                         <div class="panel-wrapper collapse in" aria-expanded="true">
                                        <div class="panel-body">
                                    <ul class="list-inline two-part">
                                        <li><i class="fa fa-user text-info"></i></li>
                                        <li class="text-right"><span class="counter"><a href ="#" onclick="get_detail_bio(1)"><span id="bio_y">0</span></a></span></li>
                                    </ul>
                                </div>
                                </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
        </div>
       @include('shared.footer_detail')