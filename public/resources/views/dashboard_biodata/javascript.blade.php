    <script src="{{ url('/') }}/assets/plugins/bower_components/owl.carousel/owl.carousel.min.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/owl.carousel/owl.custom.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/flot/jquery.flot.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/Minimal-Gauge-chart/js/cmGauge.js"></script>
    <script type="text/javascript">
    $(document).ready(function () {
        setTimeout(function () { get_data(); }, 5000);
    });
     function get_detail_kk(data){
        if(data == 0){
            $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff' 
            },
            baseZ: 2000
        }); 
            window.location.href = "{{ url('/') }}/Detail/gdbkt";
        }else{
              $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff' 
            },
            baseZ: 2000
        }); 
            window.location.href = "{{ url('/') }}/Detail/gdbky";
        }
    }
    function get_detail_bio(data){
        if(data == 0){
              $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff' 
            },
            baseZ: 2000
        }); 
            window.location.href = "{{ url('/') }}/Detail/gdbbt";
        }else{
              $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff' 
            },
            baseZ: 2000
        }); 
            window.location.href = "{{ url('/') }}/Detail/gdbby";
        }
    }
    function get_data() {
       $.ajax({
                type: "post",
                url: BASE_URL+"Dashboard_biodata/get_data",
                data: {"_token": "{{ csrf_token() }}",},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                            $('#kk_n').html(data.kk_n);
                            $('#kk_y').html(data.kk_y);
                            $('#bio_n').html(data.bio_n);
                            $('#bio_y').html(data.bio_y);
                },
                error:
                function (data) {


                },
                complete:
                function (response) {
                   
                   setTimeout(function () { get_data(); }, 5000);
                }
            });
        }
    </script>
    