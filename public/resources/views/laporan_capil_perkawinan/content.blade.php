 <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $stitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                        <ol class="breadcrumb">
                            <li class="active"><?php echo $mtitle; ?></li>
                        </ol>
                    </div>
                </div>
                 <div class="row">
                <form name ="get_form" action="<?php echo $my_url; ?>" method="post">
                    @csrf
                    <div class="col-sm-12">
                        <div class="white-box">

                            <div class="row">
                                <div class="col-lg-3">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_kec" type="checkbox" checked disabled="true">
                                            <label for="cb_kec"> Kecamatan</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3">         
                                    <div class="form-group" >
                                        <select class="form-control select2" name="no_kec" id="no_kec">
                                         <option  value="0">-- Pilih Kecamatan --</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-3">         
                                    <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_kel" type="checkbox" checked disabled="true">
                                            <label for="cb_kel"> Kelurahan</label>
                                        </div>
                                </div>
                                <div class="col-lg-3">         
                                    <div class="form-group" >
                                        <select class="form-control select2" name="no_kel" id="no_kel">
                                        <option value="0">-- Pilih Kelurahan --</option>  
                                    </select>
                           
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-3">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_lap" type="checkbox" checked disabled="true">
                                            <label for="cb_lap"> Jenis Laporan</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3">         
                                    <div class="form-group" >
                                        <select class="form-control select2" name="jenis_lap" id="jenis_lap">
                                        <option  value="0">-- Pilih Laporan --</option>
                                        <option  value="1">Non Muslim</option>
                                        <option  value="2">Muslim</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-3">         
                                    <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_db" type="checkbox" checked disabled="true">
                                            <label for="cb_db"> Database</label>
                                        </div>
                                </div>
                                <div class="col-lg-3">         
                                    <div class="form-group" >
                                        <select class="form-control select2" name="is_db" id="is_db">
                                        <option value="0">-- Pilih Database --</option>  
                                        <option value="1">1. DKB</option>  
                                        <option value="2">2. Pelayanan</option>  
                                    </select>
                           
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <button type="submit" class="btn btn-info waves-effect waves-light m-r-10 pull-right" id="btn-filter" onclick="on_serach();" >Search <i class="mdi  mdi-magnify fa-fw"></i></button>
                                  <button type="button" class="btn btn-invert waves-effect waves-light m-r-10 pull-right" id="btn-reset" onclick="on_clear();">Clear <i class="mdi  mdi-delete fa-fw"></i></button>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
                 <?php if (!empty($data)){ ?>
                <div class="row">
                <div class="col-md-12">
                        <div class="white-box">
                            <h3 class="box-title"><?php echo $mtitle; ?> <?php echo $db = ($is_db == 0 || $is_db == 1) ? 'DKB' : 'Pelayanan'; ?></h3>
                            <div class="scrollable">
                                <div class="table-responsive">
                                    <table id="mytable" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                   <thead>
                                    
                                        <tr>
                                            <th width="20%" style="text-align: center;">No</th>
                                            <th width="20%" style="text-align: center;">Wilayah</th>
                                            <th width="20%" style="text-align: right;">Jumlah Penduduk Status Kawin</th>
                                            <th width="20%" style="text-align: right;">Sudah Memiliki Akta Perkawinan</th>
                                            <th width="20%" style="text-align: right;">Belum Memiliki Akta Perkawinan</th>
                                            <th width="20%" style="text-align: right;">Ratio Kepemilikan Akta</th>

                                        </tr>
                                       
                                    </thead>
                                    <tbody id="show_data">
                                       <?php
                                        if (!empty($data)){
                                            $i = 0;
                                            $jumlah_r1 = 0;
                                            $jumlah_r2 = 0;
                                            $jumlah_r3 = 0;
                                           foreach($data as $row){
                                            $i++;
                                            $jumlah_r1 = $jumlah_r1 + $row->r1;
                                            $jumlah_r2 = $jumlah_r2 + $row->r2;
                                            $jumlah_r3 = $jumlah_r3 + $row->r3;
                                            ?>
                                             <tr>
                                                <td width="20%" style="text-align: left;"><?php echo $i ;?></td>
                                                <td width="20%" style="text-align: left;"><?php echo $row->nama_wil ;?></td>
                                                <td width="20%" style="text-align: left;"><?php echo $row->r1 ;?></td>
                                                <td width="20%" style="text-align: left;"><?php echo $row->r2 ;?></td>
                                                <td width="20%" style="text-align: left;"><?php echo $row->r3 ;?></td>
                                                <td width="20%" style="text-align: left;"><?php echo $row->prs ;?></td>
                                            </tr>
                                          <?php } ?>
                                          <?php } ?>
                                       
                                        
                                    </tbody>
                                    
                                        <tfoot id="my_foot">
                                            <tr>
                                            <?php if (!empty($data)){ ?>
                                            <th width="40%" colspan="2" style="text-align: center;">Jumlah</th>
                                            <th width="20%"  style="text-align: left;"><?php echo number_format(htmlentities($jumlah_r1),0,',','.');?></th>
                                            <th width="20%"  style="text-align: left;"><?php echo number_format(htmlentities($jumlah_r2),0,',','.');?></th>
                                            <th width="20%"  style="text-align: left;"><?php echo number_format(htmlentities($jumlah_r3),0,',','.');?></th>
                                            <th width="20%"  style="text-align: left;"><?php echo round ($jumlah_r2 * 100 / $jumlah_r1,2) ;?> %</th>
                                            <?php } ?>
                                        </tr>

                                            </tfoot>
                                </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <?php if (!empty($data2)){ ?>
                <div class="row">
                <div class="col-md-12">
                        <div class="white-box">
                            <h3 class="box-title"><?php echo $mtitle; ?> Pelayanan</h3>
                            <div class="scrollable">
                                <div class="table-responsive">
                                    <table id="mytable2" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                   <thead>
                                    
                                        <tr>
                                             <th width="20%" style="text-align: center;">No</th>
                                            <th width="20%" style="text-align: center;">Wilayah</th>
                                            <th width="20%" style="text-align: right;">Jumlah Penduduk Status Kawin</th>
                                            <th width="20%" style="text-align: right;">Sudah Memiliki Akta Perkawinan</th>
                                            <th width="20%" style="text-align: right;">Belum Memiliki Akta Perkawinan</th>
                                            <th width="20%" style="text-align: right;">Ratio Kepemilikan Akta</th>

                                        </tr>
                                       
                                    </thead>
                                    <tbody id="show_data2">
                                       <?php
                                        if (!empty($data2)){
                                            $i = 0;
                                            $jumlah_r1 = 0;
                                            $jumlah_r2 = 0;
                                            $jumlah_r3 = 0;
                                           foreach($data2 as $row){
                                            $i++;
                                            $jumlah_r1 = $jumlah_r1 + $row->r1;
                                            $jumlah_r2 = $jumlah_r2 + $row->r2;
                                            $jumlah_r3 = $jumlah_r3 + $row->r3;
                                            ?>
                                             <tr>
                                                <td width="20%" style="text-align: left;"><?php echo $i ;?></td>
                                                <td width="20%" style="text-align: left;"><?php echo $row->nama_wil ;?></td>
                                                <td width="20%" style="text-align: left;"><?php echo $row->r1 ;?></td>
                                                <td width="20%" style="text-align: left;"><?php echo $row->r2 ;?></td>
                                                <td width="20%" style="text-align: left;"><?php echo $row->r3 ;?></td>
                                                <td width="20%" style="text-align: left;"><?php echo $row->prs ;?></td>
                                            </tr>
                                          <?php } ?>
                                          <?php } ?>
                                       
                                        
                                    </tbody>
                                    
                                        <tfoot id="my_foot">
                                            <tr>
                                            <?php if (!empty($data2)){ ?>
                                            <th width="40%" colspan="2" style="text-align: center;">Jumlah</th>
                                            <th width="20%"  style="text-align: left;"><?php echo number_format(htmlentities($jumlah_r1),0,',','.');?></th>
                                            <th width="20%"  style="text-align: left;"><?php echo number_format(htmlentities($jumlah_r2),0,',','.');?></th>
                                            <th width="20%"  style="text-align: left;"><?php echo number_format(htmlentities($jumlah_r3),0,',','.');?></th>
                                            <th width="20%"  style="text-align: left;"><?php echo round ($jumlah_r2 * 100 / $jumlah_r1,2) ;?> %</th>
                                            <?php } ?>
                                        </tr>

                                            </tfoot>
                                </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
                
                 </div>
       @include('shared.footer_detail')