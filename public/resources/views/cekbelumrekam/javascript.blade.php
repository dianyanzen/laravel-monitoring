     <script>
        function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode != 44 && charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }
    $(document).ready(function() {
    
    console.log(init_kec);
    $.ajax({
                    type: "post",
                    url: BASE_URL+"api/ektp-kec",
                    dataType: "json",
                    data: {
                      "_token": "{{ csrf_token() }}",
                        no_prop : 32,
                        no_kab : 73,
                        no_kec : init_kec
                    },
                    beforeSend:
                    function () {
                        $('select[name="no_kec"]').attr("disabled",true);
                        $('select[name="no_kel"]').attr("disabled",true);
                    },
                    success: function (data) {
                        console.log(data);
                       $('select[name="no_kec"]').empty();
                       <?php if ($user_no_kec == 0){ ?>
                       $('select[name="no_kec"]').append('<option value="0">-- Pilih Kecamatan --</option>');
                       <?php }else if($akses_kec > 0){ ?>
                        $('select[name="no_kec"]').append('<option value="0">-- Pilih Kecamatan --</option>');
                       <?php } ?>
                        $.each(data.kecamatan, function(key, value) {
                            $('select[name="no_kec"]').append('<option value="'+ value.no_kec +'">'+ value.nama_kec +'</option>');
                        });
                        <?php if ($user_no_kec == 0){ ?>
                       $('select[name="no_kec"]').val("0").trigger("change");
                       <?php }else{ ?>
                        $('select[name="no_kec"]').val("<?php echo $user_no_kec; ?>").trigger("change");
                        <?php } ?>
                        
                    },
                    error:
                    function (data) {
               

                    },
                    complete:
                    function (response) {
                        $('select[name="no_kec"]').attr("disabled",false);
                        $('select[name="no_kel"]').attr("disabled",false);
                    }
                });
    $('select[name="no_kec"]').on('change', function() {

            var no_kec = $(this).val();

            if(no_kec != 0) {
                
                $.ajax({
                    type: "post",
                    url: BASE_URL+"api/ektp-kel",
                    dataType: "json",
                    data: {
                      "_token": "{{ csrf_token() }}",
                        no_prop : 32,
                        no_kab : 73,
                        no_kec : no_kec
                    },
                    beforeSend:
                    function () {
                        $('select[name="no_kel"]').attr("disabled",true);
                    },
                    success: function (data) {
                        console.log(data);
                          $('select[name="no_kel"]').empty();
                       <?php if ($user_no_kel == 0){ ?>
                       $('select[name="no_kel"]').append('<option value="0">-- Pilih Kelurahan --</option>');
                       <?php }else if($akses_kel > 0){ ?>
                        $('select[name="no_kel"]').append('<option value="0">-- Pilih Kelurahan --</option>');
                       <?php } ?>
                        $.each(data.kelurahan, function(key, value) {
                            $('select[name="no_kel"]').append('<option value="'+ value.no_kel +'">'+ value.nama_kel +'</option>');
                        });
                        <?php if ($user_no_kel == 0){ ?>
                       $('select[name="no_kel"]').val("0").trigger("change");
                       <?php }else{ ?>
                        $('select[name="no_kel"]').val("<?php echo $user_no_kel; ?>").trigger("change");
                        <?php } ?>
                        
                    },
                    error:
                    function (data) {
               

                    },
                    complete:
                    function (response) {
                        $('select[name="no_kel"]').attr("disabled",false);
                    }
                });
            }else{
                 $('select[name="no_kel"]').empty();
                 $('select[name="no_kel"]').append('<option value="0">-- Pilih Kelurahan --</option>');
                 $('select[name="no_kel"]').val("0").trigger("change");
            }

        });

    });
    function on_clear() {
        
        $('select[name="no_kec"]').val(init_kec).trigger("change");
        $('select[name="no_kel"]').empty();
        $('select[name="no_kel"]').append('<option value="0">-- Pilih Kelurahan --</option>');
        $('select[name="no_kel"]').val("0").trigger("change");
    }
    function on_serach(){
        
        $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff'
            },
            baseZ: 2000
        }); 
    }
    jQuery(document).ready(function() {
            $(".select2").select2();
             $('#mytable').DataTable({
                 "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
           $('#tanggal').val("<?php echo date('d-m-Y');?> - <?php echo date('d-m-Y');?>");
        });
    </script>
   