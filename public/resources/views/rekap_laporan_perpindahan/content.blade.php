<div id="page-wrapper">
            <div class="container-fluid">
               <div class="row bg-title">
                   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <h1 class="page-title"><?php echo $mtitle; ?></h1> </div>
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-6">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                
                        <ol class="breadcrumb">
                            <li><a href="{{ url('/') }}/">Rekap</a></li>
                            <li class="active"><?php echo $mtitle; ?></li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- .row -->
                <div class="row">
                <form >
                    <div class="col-sm-12">
                        <div class="white-box">
                            <div class="row">
                                <div class="col-lg-3">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_tgl" type="checkbox" checked disabled="true">
                                            <label for="cb_tgl"> Tanggal Activity</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3">         
                                    <div class="form-group">
                                         <input class="form-control input-daterange-datepicker" type="text" id="tanggal" name="tanggal"/> 
                                    </div>
                              
                            </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-3">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_kec" type="checkbox" checked disabled="true">
                                            <label for="cb_kec"> Kecamatan</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3">         
                                    <div class="form-group">
                                        <select class="form-control select2" name="no_kec" id="no_kec">
                                         <option  value="0">-- Pilih Kecamatan --</option>
                                        </select>
                                    </div>
                                </div>
                                </div>
                            <div class="row">
                                <div class="col-lg-3">         
                                    <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_kel" type="checkbox" checked disabled="true">
                                            <label for="cb_kel"> Kelurahan</label>
                                        </div>
                                </div>
                                <div class="col-lg-3">         
                                    <div class="form-group">
                                        <select class="form-control select2" name="no_kel" id="no_kel">
                                        <option value="0">-- Pilih Kelurahan --</option>  
                                    </select>
                           
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <button type="submit" name="do_search" class="btn btn-info waves-effect waves-light m-r-10 pull-right" id="btn-filter" onclick="on_serach();" >Search <i class="mdi  mdi-magnify fa-fw"></i></button>
                                  <button type="button" class="btn btn-invert waves-effect waves-light m-r-10 pull-right" id="btn-reset" onclick="on_clear();">Clear <i class="mdi  mdi-delete fa-fw"></i></button>
                                  
                            </div>
                        </div>
                    </div>
                    </form>
                </div>

                <div class="row">
                <div class="col-md-12">
                        <div class="white-box">
                            <h3 class="box-title"><?php echo $stitle; ?></h3>
                            <form target="_blank" id="pdf_data" name ="get_form" action="Pdf_detail" method="post">
                                        <input type="hidden" id="pdf_tanggal" name="pdf_tanggal" value="">
                                        <input type="hidden" id="pdf_no_kec" name="pdf_no_kec" value="">
                                        <input type="hidden" id="pdf_no_kel" name="pdf_no_kel" value="">
                                    </form>
                            <div class="scrollable">
                                <div class="table-responsive">
                                    <table id="activity-list" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                   <thead>
                                    
                                        <tr>
                                            <th width="10%" style="text-align: center;">No</th>
                                            <th width="20%" style="text-align: center;">Nama Lengkap</th>
                                            <th width="20%" style="text-align: center;">Alamat</th>
                                            <th width="20%" style="text-align: center;">RT</th>
                                            <th width="20%" style="text-align: center;">RW</th>
                                            <th width="20%" style="text-align: center;">Kecamatan</th>
                                            <th width="20%" style="text-align: center;">Kelurahan</th>
                                            <th width="20%" style="text-align: center;">Daerah Tujuan</th>

                                        </tr>
                                       
                                    </thead>
                                    <tbody id="show_data">
                                       
                                        
                                    </tbody>
                                    
                                </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
       @include('shared.footer_detail')