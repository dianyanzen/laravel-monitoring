 <style type="text/css">
     textarea {
  resize: none;
  height: 155px !important;
}
<?php
function bulan($bulan)
{
Switch ($bulan){
    case 1 : $bulan="Januari";
        Break;
    case 2 : $bulan="Februari";
        Break;
    case 3 : $bulan="Maret";
        Break;
    case 4 : $bulan="April";
        Break;
    case 5 : $bulan="Mei";
        Break;
    case 6 : $bulan="Juni";
        Break;
    case 7 : $bulan="Juli";
        Break;
    case 8 : $bulan="Agustus";
        Break;
    case 9 : $bulan="September";
        Break;
    case 10 : $bulan="Oktober";
        Break;
    case 11 : $bulan="November";
        Break;
    case 12 : $bulan="Desember";
        Break;
    }
return $bulan;
}
$bln=date("m");
?>
 </style>
 <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $stitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                        <ol class="breadcrumb">
                            <li class="active"><?php echo $mtitle; ?></li>
                        </ol>
                    </div>
                </div>
                 <div class="row">
                <!-- <form name ="get_form" action="<?php echo $my_url; ?>" method="post"> -->
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div class="white-box">
                            <div class="row">
                               
                                <div class="col-lg-6">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_tgl" type="checkbox" checked disabled="true">
                                            <label for="cb_tgl"> Tanggal</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">         
                                    <div class="form-group">
                                        <input class="form-control" id="tanggal" placeholder="dd/mm/yyyy" type="text" value="<?php echo date("d-m-Y") ?>" readonly></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_jenis" type="checkbox" checked disabled="true">
                                            <label for="cb_jenis"> JENIS BLANGKO</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">         
                                    <div class="form-group" <?php if (!empty($is_colnotwil)){echo 'style="display: none!important;"'; }?>>
                                        <select class="form-control select2" name="no_jenis" id="no_jenis">
                                         <option  value="0">JENIS BLANGKO</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_tgl" type="checkbox" checked disabled="true">
                                            <label for="cb_tgl"> Blangko Masuk</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">         
                                    <div class="form-group">
                                        <input class="form-control" id="txt_jumlah" type="text" value="0" onkeypress="return isNumberKey(event)" ></div>
                                </div>
                                </div>
                                <div class="row">
                                 <div class="col-lg-6">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_tgl" type="checkbox" checked disabled="true">
                                            <label for="cb_tgl"> Keterangan</label>
                                        </div>
                                    </div>
                                </div>
                                   <div class="col-lg-6">         
                                    <div class="form-group">
                                        <textarea class="form-control" type="text" id="txt_ket" ></textarea> </div>
                                </div>
                         
                                <div class="col-lg-3">         
                                    <div class="form-group">
                                       
                                    </div>
                                </div>
                                <div class="col-lg-3">         
                                   
                                </div>
                            </div>
                            <div class="row">
                                <button type="submit" id="btn_dosave" class="btn btn-info waves-effect waves-light m-r-10 pull-right" id="btn-filter" onclick="on_save();" >Save <i class="mdi  mdi-content-save fa-fw"></i></button>
                                  <button type="button" class="btn btn-invert waves-effect waves-light m-r-10 pull-right" id="btn-reset" onclick="on_clear();">Clear <i class="mdi  mdi-delete fa-fw"></i></button>
                            </div>
                        </div>
                    </div>
                    <!-- </form> -->
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">Kendali Blangko Capil <?php echo date('Y');?></div>
                            <div class="panel-wrapper collapse in">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th class="text-center">#</th>
                                            <th>Keterangan</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td align="center">1</td>
                                            <td>Request</td>
                                            <td>Rekap Blangko Capil <?php echo date('Y');?></td>
                                        </tr>
                                        <tr>
                                            <td align="center">2</td>
                                            <td>Sampai Bulan</td>
                                            <td><?php echo bulan(date("m")) ?> <?php echo date("Y") ?></td>
                                        </tr>
                                        <tr>
                                            <td align="center">2</td>
                                            <td>Jenis Blangko</td>
                                            <td><span id="lbl_blangko">-</span></td>
                                        </tr>
                                        <tr>
                                            <td align="center">3</td>
                                            <td>Blangko Masuk</td>
                                            <td><span id="lbl_masuk">0</span></td>
                                        </tr>
                                         <tr>
                                            <td align="center">4</td>
                                            <td>Blangko Keluar</td>
                                            <td><span id="lbl_keluar">0</span></td>
                                        </tr>
                                         <tr>
                                            <td align="center">5</td>
                                            <td>Blangko Rusak</td>
                                            <td><span id="lbl_rusak">0</span></td>
                                        </tr>
                                         <tr>
                                            <td align="center">6</td>
                                            <td>Sisa Blangko</td>
                                            <td><span id="lbl_sisa">0</span></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                
                 </div>
       @include('shared.footer_detail')