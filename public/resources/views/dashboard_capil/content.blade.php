<div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $stitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                        <ol class="breadcrumb">
                              <li><a href="{{ url('/') }}/<?php echo $backurl; ?>"><?php echo $back_title; ?></a></li>
                           <li class="text-info"><?php echo $stitle; ?></li>
                        </ol>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-sm-12 col-xs-12">
                        <div class="row">
                            <div class="col-lg-6 col-sm-6 col-xs-12">
                               <div class="panel panel-info">
                                    <div class="panel-heading">Akta Kelahiran<br>Hari Ini (LU)<div class="pull-right"><a href="#" data-perform="panel-collapse"><i class="ti-minus"></i></a></div>
                                         </div>
                                    <div class="panel-wrapper collapse in" aria-expanded="true">
                                        <div class="panel-body">
                                    <ul class="list-inline two-part">
                                        <li><i class="mdi mdi-baby-buggy text-info"></i></li>
                                        <li class="text-right"><span class="counter"><a href ="#" onclick="get_detail_lahir_u(0)"><span id="capil_nulahir">0</span></a></span></li>
                                    </ul>
                                </div>
                                </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-sm-6 col-xs-12">
                                <div class="panel panel-info">
                                    <div class="panel-heading">Akta Kelahiran<br>Hari Ini (LT)<div class="pull-right"><a href="#" data-perform="panel-collapse"><i class="ti-minus"></i></a></div>
                                         </div>
                                    <div class="panel-wrapper collapse in" aria-expanded="true">
                                        <div class="panel-body">
                                    <ul class="list-inline two-part">
                                        <li><i class="mdi mdi-baby-buggy text-info"></i></li>
                                        <li class="text-right"><span class="counter"><a href ="#" onclick="get_detail_lahir_t(0)"><span id="capil_ntlahir">0</span></a></span></li>
                                    </ul>
                                </div>
                                </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-sm-6 col-xs-12">
                                <div class="panel panel-info">
                                    <div class="panel-heading">Akta Perkawinan<br>Hari Ini<div class="pull-right"><a href="#" data-perform="panel-collapse"><i class="ti-minus"></i></a></div>
                                         </div>
                                    <div class="panel-wrapper collapse in" aria-expanded="true">
                                        <div class="panel-body">
                                    <ul class="list-inline two-part">
                                        <li><i class="mdi mdi-heart text-info"></i></li>
                                        <li class="text-right"><span class="counter"><a href ="#" onclick="get_detail_kawin(0)"><span id="capil_nkawin">0</span></a></span></li>
                                    </ul>
                                </div>
                                </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-sm-6 col-xs-12">
                                <div class="panel panel-info">
                                    <div class="panel-heading">Akta Perceraian<br>Hari Ini<div class="pull-right"><a href="#" data-perform="panel-collapse"><i class="ti-minus"></i></a></div>
                                         </div>
                                         <div class="panel-wrapper collapse in" aria-expanded="true">
                                        <div class="panel-body">
                                    <ul class="list-inline two-part">
                                        <li><i class="mdi mdi-heart-broken text-info"></i></li>
                                        <li class="text-right"><span class="counter"><a href ="#" onclick="get_detail_cerai(0)"><span id="capil_ncerai">0</span></a></span></li>
                                    </ul>
                                </div>
                                </div>
                                </div>
                            </div>
                            </div>
                            <div class="row">
                            <div class="col-lg-12 col-sm-12 col-xs-12">
                                <div class="panel panel-info">
                                    <div class="panel-heading">Akta Kematian Hari Ini<div class="pull-right"><a href="#" data-perform="panel-collapse"><i class="ti-minus"></i></a></div>
                                         </div>
                                         <div class="panel-wrapper collapse in" aria-expanded="true">
                                        <div class="panel-body">
                                    <ul class="list-inline two-part">
                                        <li><i class="mdi mdi-hospital-building text-info"></i></li>
                                        <li class="text-right"><span class="counter"><a href ="#" onclick="get_detail_mati(0)"><span id="capil_nmati">0</span></a></span></li>
                                    </ul>
                                </div>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-12 col-xs-12">
                        <div class="row">
                            <div class="col-lg-6 col-sm-6 col-xs-12">
                                <div class="panel panel-info">
                                    <div class="panel-heading">Akta Kelahiran<br>Kemarin (LU)<div class="pull-right"><a href="#" data-perform="panel-collapse"><i class="ti-minus"></i></a></div>
                                         </div>
                                         <div class="panel-wrapper collapse in" aria-expanded="true">
                                        <div class="panel-body">
                                    <ul class="list-inline two-part">
                                        <li><i class="mdi mdi-baby-buggy text-info"></i></li>
                                        <li class="text-right"><span class="counter"><a href ="#" onclick="get_detail_lahir_u(1)"><span id="capil_yulahir">0</span></a></span></li>
                                    </ul>
                                </div>
                                </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-sm-6 col-xs-12">
                               <div class="panel panel-info">
                                    <div class="panel-heading">Akta Kelahiran<br>Kemarin (LT)<div class="pull-right"><a href="#" data-perform="panel-collapse"><i class="ti-minus"></i></a></div>
                                         </div>
                                         <div class="panel-wrapper collapse in" aria-expanded="true">
                                        <div class="panel-body">
                                    <ul class="list-inline two-part">
                                        <li><i class="mdi mdi-baby-buggy text-info"></i></li>
                                        <li class="text-right"><span class="counter"><a href ="#" onclick="get_detail_lahir_t(1)"><span id="capil_ytlahir">0</span></a></span></li>
                                    </ul>
                                </div>
                                </div>
                                </div>
                            </div>
                            </div>
                            <div class="row">
                            <div class="col-lg-6 col-sm-6 col-xs-12">
                                <div class="panel panel-info">
                                    <div class="panel-heading">Akta Perkawinan<br>Kemarin<div class="pull-right"><a href="#" data-perform="panel-collapse"><i class="ti-minus"></i></a></div>
                                         </div>
                                         <div class="panel-wrapper collapse in" aria-expanded="true">
                                        <div class="panel-body">
                                    <ul class="list-inline two-part">
                                        <li><i class="mdi mdi-heart text-info"></i></li>
                                        <li class="text-right"><span class="counter"><a href ="#" onclick="get_detail_kawin(1)"><span id="capil_ykawin">0</span></a></span></li>
                                    </ul>
                                </div>
                                </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-sm-6 col-xs-12">
                                <div class="panel panel-info">
                                    <div class="panel-heading">Akta Perceraian<br>Kemarin<div class="pull-right"><a href="#" data-perform="panel-collapse"><i class="ti-minus"></i></a></div>
                                         </div>
                                         <div class="panel-wrapper collapse in" aria-expanded="true">
                                        <div class="panel-body">
                                    <ul class="list-inline two-part">
                                        <li><i class="mdi mdi-heart-broken text-info"></i></li>
                                        <li class="text-right"><span class="counter"><a href ="#" onclick="get_detail_cerai(1)"><span id="capil_ycerai">0</span></a></span></li>
                                    </ul>
                                </div>
                                </div>
                                </div>
                            </div>
                            </div>
                            <div class="row">
                            <div class="col-lg-12 col-sm-12 col-xs-12">
                                <div class="panel panel-info">
                                    <div class="panel-heading">Akta Kematian Kemarin<div class="pull-right"><a href="#" data-perform="panel-collapse"><i class="ti-minus"></i></a></div>
                                         </div>
                                         <div class="panel-wrapper collapse in" aria-expanded="true">
                                        <div class="panel-body">
                                    <ul class="list-inline two-part">
                                        <li><i class="mdi mdi-hospital-building text-info"></i></li>
                                        <li class="text-right"><span class="counter"><a href ="#" onclick="get_detail_mati(1)"><span id="capil_ymati">0</span></a></span></li>
                                    </ul>
                                </div>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
       @include('shared.footer_detail')