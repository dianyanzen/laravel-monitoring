<style type="text/css">
    .text-wrap{
    white-space:normal;
}
</style>

<div id="page-wrapper">
            <div class="container-fluid">
               <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $mtitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                
                        <ol class="breadcrumb">
                            <li class="active"><?php echo $mtitle; ?></li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>

                <!-- .row -->
                           
                            
                <!-- /.row -->
                <!-- /row -->
                <div class="row">
                <div class="col-md-12">
                        <div class="white-box">
                            <h3 class="box-title"><?php echo $mtitle; ?></h3>
                            <div class="form-group">
                                <div class="row">
                                <div class="col-lg-4">         
                                    <div class="form-group">
                                        <h3 class="box-title m-b-0"><b>Tanggal <?php echo $type_tgl; ?></b></h3>
                                        <input class="form-control input-daterange-datepicker" type="text" id="tanggal" name="tanggal"/> </div>
                                    
                                </div>
                                <div class="col-lg-4">         
                                    <div class="form-group">
                                        <h3 class="box-title m-b-0"><b>Petugas</b></h3>
                                        <select class="form-control select2" name="petugas" id="petugas">
                                            <option  value="0">-- PILIH PETUGAS --</option>
                                        </select>
                            
                                    </div>
                                </div>
                                <div class="col-lg-4">         
                                    <div class="form-group">
                                        <h3 class="box-title m-b-0"><b>Status Verifikasi</b></h3>
                                        <select class="form-control select2" name="stat_ver" id="stat_ver">
                                            <option  value="3">-- STATUS VERIVIKASI --</option>
                                            <option  value="0">1. BELUM DIVERIFIKASI</option>
                                            <option  value="1">2. SUDAH DIVERIVIKASI</option>
                                            <option  value="2">3. DITOLAK</option>
                                        </select>
                            
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <button type="submit" class="btn btn-info waves-effect waves-light m-r-10 pull-right" id="btn-filter" onclick="get_table();" >Search <i class="mdi  mdi-magnify fa-fw"></i></button>
                                  <button type="button" class="btn btn-invert waves-effect waves-light m-r-10 pull-right" id="btn-reset" onclick="on_clear();">Clear <i class="mdi  mdi-delete fa-fw"></i></button>
                            </div>
                            <br>
                            </div>

                            <div class="scrollable">
                                <div class="table-responsive">
                                    <table id="pengajuan-list" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                   <thead>
                                    
                                        <tr>
                                            <!-- <th width="5%" style="text-align: center;">#</th> -->
                                            <th width="5%" style="text-align: center;">#</th>
                                            <th width="30%" style="text-align: center;">Tanggal</th>
                                            <th width="20%" style="text-align: center;">Nama Lengkap</th>
                                            <th width="30%" style="text-align: center;">Aktivitas</th>
                                            <th width="30%" style="text-align: center;">Keterangan</th>
                                            <th width="30%" style="text-align: center;">Validasi Atasan</th>
                                            

                                        </tr>
                                       
                                    </thead>
                                    <tbody id="show_data">
                                       
                                        
                                    </tbody>
                                    
                                </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
                <!-- .row -->
               

            </div>
       @include('shared.footer_detail')