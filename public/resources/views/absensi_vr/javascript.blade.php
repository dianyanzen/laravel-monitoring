    <script src="{{ url('/') }}/assets/plugins/bower_components/owl.carousel/owl.carousel.min.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/owl.carousel/owl.custom.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/flot/jquery.flot.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/Minimal-Gauge-chart/js/cmGauge.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/Minimal-Gauge-chart/js/cmGauge.js"></script>
    <!-- Footable -->
    <script src="{{ url('/') }}/assets/plugins/bower_components/footable/js/footable.all.min.js"></script>
    
    <!--FooTable init-->
    <script src="{{ url('/') }}/assets/js/footable-init.js"></script>
    <script type="text/javascript" src="{{ url('/') }}/assets/plugins/bower_components/viewerjs-master/dist/jquery.magnify.js"></script>
   <script src="{{ url('/') }}/assets/plugins/bower_components/dropify/dist/js/dropify.min.js"></script>
    <script>
        function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }
        function isphone(evt)
       {
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode != 43 && charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }
       function validateEmail(email) {
          const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
          return re.test(email);
        }

      
    </script>
 <script>


  
         
        $(document).ready(function() {
            $(".select2").select2();
            $('.input-daterange-datepicker').daterangepicker({
                buttonClasses: ['btn', 'btn-sm'],
                applyClass: 'btn-info',
                cancelClass: 'btn-info',
                locale: {
                    format: 'DD-MM-YYYY'
                },
                startDate: '<?php echo date('d-m-Y');?>',
                endDate: '<?php echo date('d-m-Y');?>'
            });
            
            
            $.ajax({
                    type: "post",
                    url: BASE_URL+"api/report_user",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                    },
                    beforeSend:
                    function () {
                        $('select[name="petugas"]').attr("disabled",true);
                    },
                    success: function (data) {
                        console.log(data);
                       $('select[name="petugas"]').empty();
                       $('select[name="petugas"]').append('<option value="0">-- PILIH PETUGAS --</option>');
                       var i = 0;
                        $.each(data, function(key, value) {
                            i++;
                            $('select[name="petugas"]').append('<option value="'+ value.user_id +'">'+ i +'. '+ value.nama_lgkp +' ('+ value.nrp +')</option>');
                        });
                        $('select[name="petugas"]').val("<?php echo $user_id; ?>").trigger("change");
                        
                    },
                    error:
                    function (data) {
               

                    },
                    complete:
                    function (response) {
                        $('select[name="petugas"]').attr("disabled",false);
                        load_table();
                    }
                });      

            
        });
    function load_table(){
        var table = $('#pengajuan-list').DataTable( {
                "ajax": {
                 "url": BASE_URL+"Report/get_absensi_vr",
                "type": "post",
                "data" : {
                    "tgl" : $('#tanggal').val(),
                    "user_id": $('select[name="petugas"]').val(),
                    "stat_ver"  : $('select[name="stat_ver"]').val()
                }
                },
                responsive: true,
                destroy: true,
                 "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                 "columnDefs": [
                    
                    {
                      render: function (data, type, full, meta) {
                            return "<div class='text-wrap width-250'>" + data + "</div>";
                        },
                        targets: '_all'
                    },
                    {
                      width    : "5%", targets: [0]
                    },
                    {
                      width    : "8%", targets: [1]
                    },
                    {
                      width    : "10%", targets: [5]
                    },
                    {
                      className: "text-center" , targets: [0,1,5]
                    }
                    
                 ]  
                  ,
                "pageLength" : 100,
                "columns": [
                    { "data": "no" },
                    { "data": "created_dt" },
                    { "data": "nama_lgkp" },
                    { "data": "activity" },
                    { "data": "description" },
                    { "data": "aksi" },
                    
                ],
                "order": [[0, 'asc']]
            });
    }
    function get_table(){
        load_table();
        toastr.success('Data Berhasil Di Perbarui !');
    }
    function gettolak(id){
        var id = id;
        console.log(id)
        $.ajax({
                    type: "post",
                    url:  BASE_URL+"Report/get_tolak",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        daily_id : id,
                    },
                    beforeSend:
                    function () {
                         block_screen();
                    },
                    success: function (data) {
                        console.log(data);
                         swal("Alasan Penolakan!", data.message, "warning");
                    },
                    error:
                    function (data) {
                        unblock_screen();
                        swal("Error!", "Ooops!, Please Try Again, Something Went Wrong", "error"); 

                    },
                    complete:
                    function (response) {
                        unblock_screen();
                    }
                });
    }

    function block_screen(){
        
        $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff'
            },
            baseZ: 2000
        }); 
    }
    function unblock_screen(){
       $.unblockUI();
    }
    
    function toTitleCase(str) {
        return str.replace(/(?:^|\s)\w/g, function(match) {
            return match.toUpperCase();
        });
    }
    function on_clear(){
        $('select[name="petugas"]').val("0").trigger("change");
        $('select[name="stat_ver"]').val("3").trigger("change");
        $('#tanggal').val("<?php echo date('d-m-Y');?> - <?php echo date('d-m-Y');?>");
    }
    </script>