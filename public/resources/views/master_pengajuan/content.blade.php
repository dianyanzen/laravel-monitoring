<div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $stitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                        <ol class="breadcrumb">
                           <li class="text-info"><?php echo $stitle; ?></li>
                        </ol>
                    </div>
                </div>
                <!-- .row -->
               <div class="row">
                <div class="row">
                    <?php   if (!empty($data)){
                            foreach($data as $row){?>
                    <div class="col-md-4 col-xs-12">
                        <div class="white-box">
                            <div class="user-bg"> <img width="100%" alt="user" src="{{ url('/') }}/assets/plugins/images/wallpaper2.jpg">
                                <div class="overlay-box">
                                    <div class="user-content">
                                        <a href="javascript:void(0)"> <?php $filename = 'assets/upload/pp/'.$row->nik.'.jpg';
                                            if (file_exists($filename)) { ?>
                                            <img src="{{ url('/') }}/assets/upload/pp/<?php echo $row->nik; ?>.jpg"  class="img-circle thumb-lg" alt="user-img" >
                                            <?php } else {?>
                                            <img src="{{ url('/') }}/assets/plugins/images/calming-cat.gif" class="img-circle thumb-lg" alt="user-img">
                                        <?php }?></a>
                                        <h4 class="text-white"><?php echo $row->nama_lgkp; ?></h4>
                                        <h5 class="text-white"><?php echo $row->nama_kantor; ?></h5>  
                                        
                                    </div>
                                </div>
                            </div>
                            
                            <div class="user-btm-box">
                                <div class="col-md-4 col-sm-4 text-center">
                                    <p class="text-info"><b>Pengajuan<br>Kemarin</b></p>
                                    <h1><?php echo $row->pengajuan_y; ?></h1> 
                                </div>
                                <div class="col-md-4 col-sm-4 text-center">
                                    <p class="text-info"><b>Pengajuan<br>Hari Ini</b></p>
                                    <h1><?php echo $row->pengajuan_n; ?></h1> 
                                </div>
                                <div class="col-md-4 col-sm-4 text-center">
                                    <p class="text-info"><b>Pengajuan<br>Bulan Ini</b></p>
                                    <h1><?php echo $row->pengajuan_b; ?></h1> 
                                </div>
                            </div>
                         
                        </div>
                    </div>
                    <?php } ?>
                    <?php } ?>
                    
                </div>
                   
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- /.container-fluid -->
         @include('shared.footer_detail')