<script type="text/javascript">
        $( document ).ready(function() {
        
        function getRandomColor() {
          var letters = '0123456789ABCDEF';
          var color = '#2cab';
          for (var i = 4; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
          }
          return color;
        }
    
    var ctx2 = document.getElementById("Bar_chart").getContext("2d");
    var data2 = {
        labels: [
        <?php if (!empty($data)){
            foreach($data as $row){?>
            "<?php echo $row->nama_wil ;?>",
            <?php } ?>
            <?php } ?>
            ],
        datasets: [
            {
                label: "Grafik Bar",
                fillColor: "#0584f2",
                strokeColor: "#0584f2",
                highlightFill: "#0444bf",
                highlightStroke: "#0444bf",
                data: [
                <?php if (!empty($data)){
                foreach($data as $row){?>
                "<?php echo $row->jumlah ;?>",
                <?php } ?>
                <?php } ?>
                ]
            }
        ]
    };
    
    var Bar_chart = new Chart(ctx2).Bar(data2, {
        scaleBeginAtZero : true,
        scaleShowGridLines : true,
        scaleGridLineColor : "rgba(0,0,0,.005)",
        scaleGridLineWidth : 0,
        scaleShowHorizontalLines: true,
        scaleShowVerticalLines: true,
        barShowStroke : true,
        barStrokeWidth : 0,
        tooltipCornerRadius: 2,
        barDatasetSpacing : 3,
        legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
        responsive: true
    });
    
    var ctx3 = document.getElementById("Pie_chart").getContext("2d");
    var data3 = [
        <?php if (!empty($data)){
            $i = 0;
        foreach($data as $row){?>
        {
            
            <?php 
            $i++;
            if ($i % 2 == 0){
                $col1 = "#0584f2";
                $col2 = "#0444bf";
            }else{
                $col1 = "#0aaff1";
                $col2 = "#0444bf";
            } ?>
            value: <?php echo $row->jumlah ;?>,
            color: "<?php echo $col1 ;?>",
            highlight: "<?php echo $col2 ;?>",
            label: "<?php echo $row->nama_wil ;?>"
        },
        <?php } ?>
        <?php } ?>
    ];
    
    var myPieChart = new Chart(ctx3).Pie(data3,{
        segmentShowStroke : true,
        segmentStrokeColor : "#fff",
        segmentStrokeWidth : 0,
        animationSteps : 100,
        tooltipCornerRadius: 0,
        animationEasing : "easeOutBounce",
        animateRotate : true,
        animateScale : false,
        legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>",
        responsive: true
    });
 
    
});
    </script>