 <script>
  
    function on_clear() {
        $('#my_data').empty();
        $('#my_data').append('<tr><td colspan="4" style="text-align: center;" valign="center">No data available in table</td></td>');
        $('#my_foot').html('<tr><th width="80%" colspan="3" style="text-align: center;">Jumlah</th><th width="10%" style="text-align: right;">0</th></td>');
        $('select[name="no_kec"]').val("0").trigger("change");
        $('select[name="no_kel"]').empty();
        $('select[name="no_kel"]').append('<option value="0">-- Pilih Kelurahan --</option>');
        $('select[name="no_kel"]').val("0").trigger("change");
    }
    function on_serach(){
        $('#my_data').html('<tr><td colspan="4" style="text-align: center;" valign="center">Waiting For Generate Data</td></td>');
        $('#my_foot').html('<tr><th width="80%" colspan="3" style="text-align: center;">Jumlah</th><th width="10%" style="text-align: right;">0</th></td>');
        $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff'
            },
            baseZ: 2000
        }); 
    }
    
    jQuery(document).ready(function() {
        $.ajax({
                    type: "post",
                    url: BASE_URL+"api/ektp-kec",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        no_prop : 32,
                        no_kab : 73,
                        no_kec : init_kec
                    },
                    beforeSend:
                    function () {
                        $('select[name="no_kec"]').attr("disabled",true);
                        $('select[name="no_kel"]').attr("disabled",true);
                    },
                    success: function (data) {
                        console.log(data);
                       $('select[name="no_kec"]').empty();
                       <?php if ($user_no_kec == 0){ ?>
                       $('select[name="no_kec"]').append('<option value="0">-- Pilih Kecamatan --</option>');
                       <?php }else if($akses_kec > 0){ ?>
                        $('select[name="no_kec"]').append('<option value="0">-- Pilih Kecamatan --</option>');
                       <?php } ?>
                        $.each(data.kecamatan, function(key, value) {
                            $('select[name="no_kec"]').append('<option value="'+ value.no_kec +'">'+ value.nama_kec +'</option>');
                        });
                        <?php if ($user_no_kec == 0){ ?>
                       $('select[name="no_kec"]').val("0").trigger("change");
                       <?php }else{ ?>
                        $('select[name="no_kec"]').val("<?php echo $user_no_kec; ?>").trigger("change");
                        <?php } ?>
                        
                    },
                    error:
                    function (data) {
               

                    },
                    complete:
                    function (response) {
                        $('select[name="no_kec"]').attr("disabled",false);
                        $('select[name="no_kel"]').attr("disabled",false);
                    }
                });
        $('select[name="no_kec"]').on('change', function() {
            get_table();
            var no_kec = $(this).val();
            $('#pdf_no_kec').val(no_kec);

        });
            $(".select2").select2();
            $('.input-daterange-datepicker').daterangepicker({
                buttonClasses: ['btn', 'btn-sm'],
                applyClass: 'btn-info',
                cancelClass: 'btn-info',
                locale: {
                    format: 'DD-MM-YYYY'
                },
                startDate: '<?php echo date('d-m-Y');?>',
                endDate: '<?php echo date('d-m-Y');?>'
            });
           get_table();
           $('.input-daterange-datepicker').on('change', function() {
            var tgl = $(this).val();
            $('#pdf_tanggal').val(tgl);
            get_table_tgl(tgl);
        });
        });
        $('#kk-list').DataTable({
         "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });

    function get_table() {
        $('#kk-list').DataTable({
                destroy: true,
                 "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                 "columnDefs": [
                    {"className": "dt-left", "targets": "_all"}
                  ],
                "pageLength" : 50,
                "ajax": {
                 "url": BASE_URL+"Activity/rekap_kk_data",
                "type": "post",
                "data": {
                "_token": "{{ csrf_token() }}",
                "tanggal":  $("#tanggal").val(),
                "user_id":  '<?php echo $user_id; ?>',
                "no_kec":  $("#no_kec").val(),
                }
                }
            });
    }
    function get_table_tgl(tgl) {
        $('#kk-list').DataTable({
                destroy: true,
                 "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                 "columnDefs": [
                    {"className": "dt-left", "targets": "_all"}
                  ],
                "pageLength" : 50,
                "ajax": {
                 "url": BASE_URL+"Activity/rekap_kk_data",
                "type": "post",
                "data": {
                "_token": "{{ csrf_token() }}",
                "tanggal":  tgl,
                "user_id":  '<?php echo $user_id; ?>',
                "no_kec":  $("#no_kec").val(),
                }
                }
            });
    }
    function on_serach(user_id) {
        $('#kk-list').DataTable({
                destroy: true,
                 "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                dom: 'Bfrtip',
                        buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                 "columnDefs": [
                    {"className": "dt-left", "targets": "_all"}
                  ],
                "pageLength" : 50,
                "ajax": {
                 "url": BASE_URL+"Activity/rekap_kk_data",
                "type": "post",
                "data": {
                "_token": "{{ csrf_token() }}",
                "tanggal":  $("#tanggal").val(),
                "user_id":  '<?php echo $user_id; ?>',
                "no_kec":  $("#no_kec").val(),
                }
                }
            });
    }
    function do_pdf(){
        $('#pdf_userid').val();
        $('#pdf_tanggal').val();
        $('#pdf_data').submit();
    }
    </script>