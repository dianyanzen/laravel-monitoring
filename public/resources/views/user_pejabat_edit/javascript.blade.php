     <script>
        function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }
        function on_add_pejabat() {
			if (validationdaily()){
                do_save();
            }
           
        }
        function on_back() {
            block_screen();
            var url = "{{ url('/') }}/";
            url += "Setting/Pejabat";
            var win = window.location.replace(url);
            win.focus();
        }
        function validationdaily() {
        var nip = $("#nip");
            if (nip.val().length == 0) {                
                  swal("Warning!", "Nip Tidak Boleh Kosong !", "warning");  
                 return false;
            }
        var nama = $("#nama");
            if (nama.val().length == 0) {                
                  swal("Warning!", "Name Tidak Boleh Kosong !", "warning");  
                 return false;
            }
        var jabatan = $("#jabatan");
            if (jabatan.val().length == 0) {                
                  swal("Warning!", "Jabatan Tidak Boleh Kosong !", "warning");  
                 return false;
            }
        var struktural = $("#struktural");
            if (struktural.val().length == 0) {                
                  swal("Warning!", "Struktural Tidak Boleh Kosong !", "warning");  
                 return false;
            }

            return true;
        }
        function do_save(){
            $.ajax({
                    type: "post",
                    url: BASE_URL+"Setting/edit_pejabat_id",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        pejabat_id : $("#pejabat_id").val(),
                        nip : $("#nip").val(),
                        nama : $("#nama").val(),
                        jabatan : $("#jabatan").val(),
                        struktural : $("#struktural").val(),

                    },
                    beforeSend:
                    function () {
                         block_screen();
                    },
                    success: function (data) {
                        console.log(data);
                        swal("Success!", data.message, "success");
                    },
                    error:
                    function (data) {
                        unblock_screen();
                        swal("Error!", "Ooops!, Please Try Again, Something Went Wrong", "error"); 

                    },
                    complete:
                    function (response) {
                        unblock_screen();
                        on_back();
                    }
                });
        }
       
    jQuery(document).ready(function() {
        $('select[name="struktural"]').empty();
            $('select[name="struktural"]').append('<option value="1">YA</option>');
            $('select[name="struktural"]').append('<option value="0">TIDAK</option>');
            $('select[name="struktural"]').val("<?php if (!empty($data)){echo ($data[0]->is_struktural == '1') ? '1' : '0' ;}?>").trigger("change");
        $(".select2").select2();
        $('#tanggal').val("<?php echo date('d-m-Y');?>");
        });
    function block_screen(){
        
        $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff'
            },
            baseZ: 2000
        }); 
    }
    function unblock_screen(){
       $.unblockUI();
    }
    function on_clear() {
        $('#nip').val("");
        $('#nama').val("");
        $('select[name="jabatan"]').val(0).trigger("change");
        $('#nip').focus();
    }
    </script>