<!DOCTYPE html>
<html lang="en">
@include('shared.head')
<body class="fix-header">
    <div id="wrapper">
            @include('shared.nav_top')
            @include('shared/nav_side')
            @include('siak_repair_bio_wni/content')
    </div>
     
    @include('shared/footer')
    
    @include('siak_repair_bio_wni/javascript')
</body>
</html>