<div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $stitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                        <ol class="breadcrumb">
                           <li class="text-info"><?php echo $stitle; ?></li>
                        </ol>
                    </div>
                </div>
                <!-- .row -->
                 <div class="row">
                    <form name ="get_form" id="get_form" action="<?php echo $my_url; ?>" method="post">
                        @csrf
                    <div class="col-sm-12">
                        <div class="white-box">
                            <div class="row">
                                <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_nik" type="checkbox" checked disabled="true">
                                            <label for="cb_nik"> Nik Duplicate</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">         
                                    <div class="form-group">
                                        <input class="form-control" type="text" id="nik" name="nik" maxlength="16" onkeypress="return isNumberKey(event)"/></div>
                                </div>
                                </div>
                                <div class="row">
                                <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_nik_single" type="checkbox" checked disabled="true">
                                            <label for="cb_nik_single"> Nik Single</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">         
                                    <div class="form-group">
                                        <input class="form-control" type="text" id="nik_single" name="nik_single" maxlength="16" onkeypress="return isNumberKey(event)"/></div>
                                </div>
                                </div>
                            <div class="row">
                                <a class="btn btn-info waves-effect waves-light m-r-10 pull-right" id="btn-filter" onclick="get_data();" >Search <i class="mdi  mdi-magnify fa-fw"></i></a>
                                  <a  class="btn btn-info waves-effect waves-light m-r-10 pull-right" id="btn-reset" onclick="on_clear();">Clear <i class="mdi  mdi-delete fa-fw"></i></a>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
                <?php if (!empty($data) && !empty($sdata)){ ?>
                    
                            <?php if ($data[0]->ket == 'AKTIF DI SIAK' && $sdata[0]->ket == 'TIDAK AKTIF DI SIAK'){ ?>
                                          <?php if ($data[0]->current_status_code != 'DUPLICATE_RECORD' &&  $data[0]->current_status_code != 'BELUM REKAM'){ ?> 
                                            <div class="row" id="all_content_button">
                                            <div class="col-lg-12" id="all_<?php echo $sdata[0]->nik; ?>">
                                            <div class="white-box">
                                            <tr>
                                                <td>
                                                    <h2 style="color : red!important;text-align: center!important;"><i class="mdi mdi-alert fa-fw"></i> Nik Duplicate Memiliki Ktp Dengan Status E-Ktp <?php echo $data[0]->current_status_code; ?> <i class="mdi mdi-alert fa-fw"></i></h2>
                                                </td>
                                            </tr>
                                            </div>
                                            </div>
                                            </div>
                                         <?php }else{ ?>
                                            <?php if ($sdata[0]->current_status_code != 'DUPLICATE_RECORD'){ ?>
                                            <div class="row" id="all_content_button">
                                            <div class="col-lg-12" id="all_<?php echo $sdata[0]->nik; ?>">
                                            <div class="white-box">
                                            <tr>
                                                <td>
                                                    <button class="btn btn-block btn-info btn-rounded" onclick="repair_biodata('<?php echo $data[0]->nik; ?>','<?php echo $sdata[0]->nik; ?>','<?php echo $data[0]->current_status_code; ?>','<?php echo $sdata[0]->current_status_code; ?>','<?php echo $data[0]->nama_lgkp; ?>','<?php echo $sdata[0]->nama_lgkp; ?>');" id="repair_biodata"><i class="mdi mdi-database-plus"></i> SESUAIKAN BIODATA </button>
                                                </td>
                                            </tr>
                                            </div>
                                            </div>
                                            </div>
                                            <?php }else{ ?>                                 
                                                <div class="row" id="all_content_button">
                                            <div class="col-lg-12" id="all_<?php echo $sdata[0]->nik; ?>">
                                            <div class="white-box">
                                            <tr>
                                                <td>
                                                    <h2 style="color : red!important;text-align: center!important;"><i class="mdi mdi-alert fa-fw"></i> Nik Single Tidak Dapat Di Restore Karena Status E-Ktp <?php echo $sdata[0]->current_status_code; ?> <i class="mdi mdi-alert fa-fw"></i></h2>
                                                </td>
                                            </tr>
                                            </div>
                                            </div>
                                            </div>
                                            <?php } ?>
                                         <?php } ?>

                                    <?php }else if ($data[0]->ket == 'TIDAK AKTIF DI SIAK' && $sdata[0]->ket == 'AKTIF DI SIAK'){ ?>
                                            <div class="row" id="all_content_button">
                                            <div class="col-lg-12" id="all_<?php echo $sdata[0]->nik; ?>">
                                            <div class="white-box">
                                            <tr>
                                                <td>
                                                   <h2 style="color : red!important;text-align: center!important;"><i class="mdi mdi-alert fa-fw"></i> Nik Single Sudah Aktif Di Siak <i class="mdi mdi-alert fa-fw"></i></h2>
                                                </td>
                                            </tr>
                                            </div>
                                            </div>
                                            </div>
                                    <?php }else if ($data[0]->ket == 'AKTIF DI SIAK' && $sdata[0]->ket == 'AKTIF DI SIAK'){ ?>
                                            <div class="row" id="all_content_button">
                                            <div class="col-lg-12" id="all_<?php echo $sdata[0]->nik; ?>">
                                            <div class="white-box">
                                            <tr>
                                                <td>
                                                   <h2 style="color : red!important;text-align: center!important;"><i class="mdi mdi-alert fa-fw"></i> Keduanya Aktif Di Siak <i class="mdi mdi-alert fa-fw"></i></h2>
                                                </td>
                                            </tr>
                                            </div>
                                            </div>
                                            </div>
                                    <?php }else if ($data[0]->ket == 'TIDAK AKTIF DI SIAK' && $sdata[0]->ket == 'TIDAK AKTIF DI SIAK'){ ?>
                                            <div class="row" id="all_content_button">
                                            <div class="col-lg-12" id="all_<?php echo $sdata[0]->nik; ?>">
                                            <div class="white-box">
                                            <tr>
                                                <td>
                                                   <h2 style="color : red!important;text-align: center!important;"><i class="mdi mdi-alert fa-fw"></i> Keduanya Tidak Aktif Di Siak <i class="mdi mdi-alert fa-fw"></i></h2>
                                                </td>
                                            </tr>
                                            </div>
                                            </div>
                                            </div>
                                    <?php }else{ ?>
                                    <?php } ?>
                                    <?php } ?>
                <?php if (!empty($data)){ ?>
                         <div  id="all_content_duplicate">
                    <div class="col-lg-6" id="all_<?php echo $data[0]->nik; ?>">
                        <div class="white-box">
                            <div class="">
                                <h1 class="m-b-0 m-t-0">Nik Duplicate</h1>
                                <div class="row el-element-overlay m-b-40">
                                    <div class="col-lg-3 col-md-3 col-sm-6">
                                    <table class="table">
                                        <tbody>
                                        <tr>
                                    <div class="white-box">
                                        <?php if ($data[0]->path != '-'){ ?>
                                    <?php $filename = 'http://10.32.73.222:8080/Siak/'.$data[0]->path.'/'.$data[0]->nik.'.jpg';
                                        if ($_SERVER['SERVER_NAME'] == '10.32.73.7') { ?>
                                        <div class="el-card-item">
                                            <div class="el-card-avatar el-overlay-1"> <img src="<?php echo $filename; ?>" />
                                                <div class="el-overlay">
                                                    <ul class="el-info">
                                                        <li><a class="btn default btn-outline image-popup-vertical-fit" href="<?php echo $filename; ?>"><i class="icon-magnifier"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                            <?php }else{ ?>
                                                <?php if ($data[0]->jenis_klmin == 'LAKI-LAKI'){ ?>
                                            <div class="el-card-item">
                                            <div class="el-card-avatar el-overlay-1"> <img src="{{ url('/') }}/assets/plugins/images/male-placeholder.jpg" />
                                                <div class="el-overlay">
                                                    <ul class="el-info">
                                                        <li><a class="btn default btn-outline image-popup-vertical-fit" href="{{ url('/') }}/assets/plugins/images/male-placeholder.jpg"><i class="icon-magnifier"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            </div>
                                            <?php }else{ ?>
                                            <div class="el-card-item">
                                            <div class="el-card-avatar el-overlay-1"> <img src="{{ url('/') }}/assets/plugins/images/female-placeholder.jpg" />
                                                <div class="el-overlay">
                                                    <ul class="el-info">
                                                        <li><a class="btn default btn-outline image-popup-vertical-fit" href="{{ url('/') }}/assets/plugins/images/female-placeholder.jpg"><i class="icon-magnifier"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            </div>
                                            <?php } ?> 

                                            <?php } ?> 
                                        <?php }else{ ?>
                                            <?php if ($data[0]->jenis_klmin == 'LAKI-LAKI'){ ?>
                                            <div class="el-card-item">
                                            <div class="el-card-avatar el-overlay-1"> <img src="{{ url('/') }}/assets/plugins/images/male-placeholder.jpg" />
                                                <div class="el-overlay">
                                                    <ul class="el-info">
                                                        <li><a class="btn default btn-outline image-popup-vertical-fit" href="{{ url('/') }}/assets/plugins/images/male-placeholder.jpg"><i class="icon-magnifier"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            </div>
                                            <?php }else{ ?>
                                            <div class="el-card-item">
                                            <div class="el-card-avatar el-overlay-1"> <img src="{{ url('/') }}/assets/plugins/images/female-placeholder.jpg" />
                                                <div class="el-overlay">
                                                    <ul class="el-info">
                                                        <li><a class="btn default btn-outline image-popup-vertical-fit" href="{{ url('/') }}/assets/plugins/images/female-placeholder.jpg"><i class="icon-magnifier"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            </div>
                                            <?php } ?> 
                                        <?php } ?> 
                                    
                                    </div>
                                    </tr>
                                     
                                    </tbody>
                                    </table>
                                    </div>
                                    <div class="col-lg-9 col-md-9 col-sm-6">
                                        <table class="table">
                                                <tbody>
                                                    <tr>
                                                        <td width="250"><h4>NAMA LENGKAP</h4></td>
                                                        <td> <h4><?php echo $data[0]->nama_lgkp; ?></h4> </td>
                                                        <td><h4></h4></td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>NIK</h4></td>
                                                        <td> <h4><?php echo $data[0]->nik; ?></h4> </td>
                                                        <td><h4></h4></td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>NO KK</h4></td>
                                                        <td> <h4><?php echo $data[0]->no_kk; ?></h4> </td>
                                                        <td><h4></h4></td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>STAT HUBKEL</h4></td>
                                                        <td> <h4><?php echo $data[0]->stat_hbkel; ?></h4> </td>
                                                        <td><h4></h4></td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>TEMPAT LAHIR</h4></td>
                                                        <td> <h4><?php echo $data[0]->tmpt_lhr; ?></h4> </td>
                                                        <td><h4></h4></td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>TANGGAL LAHIR</h4></td>
                                                        <td> <h4><?php echo $data[0]->tgl_lhr; ?></h4> </td>
                                                        <td><h4></h4></td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>JENIS KELAMIN</h4></td>
                                                        <td> <h4><?php echo $data[0]->jenis_klmin; ?></h4> </td>
                                                        <td><h4></h4></td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>KECAMATAN</h4></td>
                                                        <td> <h4><?php echo $data[0]->nama_kec; ?></h4> </td>
                                                        <td><h4></h4></td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>KELURAHAN</h4></td>
                                                        <td> <h4><?php echo $data[0]->nama_kel; ?></h4> </td>
                                                        <td><h4></h4></td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>STATUS PEREKAMAN</h4></td>
                                                        <?php if ($data[0]->current_status_code == 'ENROLL_FAILURE_AT_REGIONAL' || $data[0]->current_status_code == 'PACKET_RETRY' || $data[0]->current_status_code == 'RECEIVED_AT_CENTRAL' || $data[0]->current_status_code == 'PROCESSING' || $data[0]->current_status_code == 'SENT_FOR_ENROLLMENT' || $data[0]->current_status_code == 'ENROLL_FAILURE_AT_CENTRAL' || $data[0]->current_status_code == 'SEARCH_FAILURE_AT_CENTRAL' || $data[0]->current_status_code == 'ADJUDICATE_RECORD' || $data[0]->current_status_code == 'ADJUDICATE_IN_PROCESS' || $data[0]->current_status_code == 'SENT_FOR_DEDUP' || $data[0]->current_status_code == 'DUPLICATE_RECORD' || $data[0]->current_status_code == 'BELUM REKAM'){ ?>
                                                        <td> <h4 class="text-danger"><b><?php echo $data[0]->current_status_code; ?></b></h4> </td>
                                                    <?php }else{ ?>
                                                        <td> <h4 class="text-success"><b><?php echo $data[0]->current_status_code; ?></b></h4> </td>
                                                    <?php } ?>
                                                        <td><h4></h4></td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>KETERANGAN </h4></td>
                                                    <?php if ($data[0]->ket == 'TIDAK AKTIF DI SIAK'){ ?>
                                                        <td> <h4 class="text-danger"><b><?php echo $data[0]->ket; ?></b></h4> </td>
                                                    <?php }else{ ?>
                                                        <td> <h4 class="text-success"><b><?php echo $data[0]->ket; ?></b></h4> </td>
                                                    <?php } ?>
                                                        <td><h4></h4></td>
                                                    </tr>
                                                    <?php if ($data[0]->ket == 'TIDAK AKTIF DI SIAK'){ ?>
                                                        <?php if (!empty($pindah)){ ?>
                                                        <tr>
                                                            <td><h4>HISTORY PINDAH</h4></td>
                                                            <?php if ($pindah[0]->klasifikasi_pindah == 'ANTAR KECAMATAN' || $pindah[0]->klasifikasi_pindah == 'ANTAR DESA/KELURAHAN' || $pindah[0]->klasifikasi_pindah == 'DALAM SATU DESA/KELURAHAN'){ ?>
                                                                <td> <h4 class="text-danger"><?php echo $pindah[0]->no_pindah; ?></h4> </td>
                                                            <td> <h4 class="text-danger"><?php echo $pindah[0]->klasifikasi_pindah; ?></h4> </
                                                               <?php }else{ ?> 
                                                                <td> <h4 class="text-danger"><?php echo $pindah[0]->no_pindah; ?></h4> </td>
                                                            <td> <h4 class="text-danger"><?php echo $pindah[0]->klasifikasi_pindah; ?></h4> </
                                                               <?php } ?> 
                                                            td>

                                                        </tr>
                                                        <tr>
                                                            <td><h4>DARI :</h4></td>
                                                            <td> <h4><?php echo $pindah[0]->dari_nama_provinsi; ?></h4> </td>
                                                            <td> <h4><?php echo $pindah[0]->dari_nama_kabupaten; ?></h4> </td>
                                                        </tr>
                                                        <tr>
                                                            <td><h4></h4></td>
                                                            <td> <h4><?php echo $pindah[0]->dari_nama_kecamatan; ?></h4> </td>
                                                            <td> <h4><?php echo $pindah[0]->dari_nama_kelurahan; ?></h4> </td>
                                                        </tr>
                                                        <tr>
                                                            <td><h4>KE :</h4></td>
                                                            <td> <h4><?php echo $pindah[0]->tujuan_nama_provinsi; ?></h4> </td>
                                                            <td> <h4><?php echo $pindah[0]->tujuan_nama_kabupaten; ?></h4> </td>
                                                        </tr>
                                                        <tr>
                                                            <td><h4></h4></td>
                                                            <td> <h4><?php echo $pindah[0]->tujuan_nama_kecamatan; ?></h4> </td>
                                                            <td> <h4><?php echo $pindah[0]->tujuan_nama_kelurahan; ?></h4> </td>
                                                        </tr>
                                                        <?php } ?>
                                                    <?php } ?>
                                                    <?php if ($data[0]->ket == 'TIDAK AKTIF DI SIAK'){ ?>
                                                        <?php if (!empty($mati)){ ?>
                                                        <tr>
                                                            <td><h4>HISTORY KEMATIAN</h4></td>
                                                            <td colspan="2"> <h4 class="text-danger"><?php echo $mati[0]->ket; ?></h4> </td>
                                                        </tr>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        

                                    </div>
                                    </div>
                                   
                            </div>
                        </div>
                    </div>
                    </div>
                   
               
                <?php }else{ ?>
                <?php if (!empty($is_ada)){ ?>
                 <div>
                    <div class="col-lg-6">
                        <div class="white-box">
                            <div class="">
                                <h1 class="m-b-0 m-t-0">Nik Duplicate Tidak Ditemukan</h1>
                                <div class="row el-element-overlay m-b-40">
                                    <div class="col-lg-4 col-md-4 col-sm-6">
                                    <div class="white-box">
                                        <div class="el-card-item">
                                            <div class="el-card-avatar el-overlay-1"> <img src="{{ url('/') }}/assets/plugins/images/calming-cat.gif" />
                                                <div class="el-overlay">
                                                    <ul class="el-info">
                                                        <li><a class="btn default btn-outline image-popup-vertical-fit" href="{{ url('/') }}/assets/plugins/images/calming-cat.gif"><i class="icon-magnifier"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            </div>
                                    </div>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-6">
                                        <table class="table">
                                                <tbody>
                                                    <tr>
                                                        <td width="250"><h4>NAMA LENGKAP</h4></td>
                                                        <td> <h4>-</h4> </td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>NIK</h4></td>
                                                        <td> <h4>-</h4> </td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>NO KK</h4></td>
                                                        <td> <h4>-</h4> </td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>TEMPAT LAHIR</h4></td>
                                                        <td> <h4>-</h4> </td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>TANGGAL LAHIR</h4></td>
                                                        <td> <h4>-</h4> </td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>JENIS KELAMIN</h4></td>
                                                        <td> <h4>-</h4> </td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>STATUS PEREKAMAN</h4></td>
                                                        
                                                        <td> <h4>-</h4> </td>
                                                    
                                                    </tr>
                                                    <tr>
                                                        <td><h4>KETERANGAN</h4></td>
                                                        
                                                        <td> <h4>-</h4> </td>
                                                    
                                                    </tr>
                                                </tbody>
                                            </table>
                                        

                                    </div>
                                    </div>
                                   
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <?php } ?>
                 <?php if (!empty($sdata)){ ?>
                         <div id="all_content_single">
                    <div class="col-lg-6" id="all_<?php echo $sdata[0]->nik; ?>">
                        <div class="white-box">
                            <div class="">
                                <h1 class="m-b-0 m-t-0">Nik Single</h1>
                                <div class="row el-element-overlay m-b-40">
                                    <div class="col-lg-3 col-md-3 col-sm-6">
                                    <table class="table">
                                        <tbody>
                                        <tr>
                                    <div class="white-box">
                                        <?php if ($sdata[0]->path != '-'){ ?>
                                    <?php $filename = 'http://10.32.73.222:8080/Siak/'.$sdata[0]->path.'/'.$sdata[0]->nik.'.jpg';
                                        if ($_SERVER['SERVER_NAME'] == '10.32.73.7') { ?>
                                        <div class="el-card-item">
                                            <div class="el-card-avatar el-overlay-1"> <img src="<?php echo $filename; ?>" />
                                                <div class="el-overlay">
                                                    <ul class="el-info">
                                                        <li><a class="btn default btn-outline image-popup-vertical-fit" href="<?php echo $filename; ?>"><i class="icon-magnifier"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                            <?php }else{ ?>
                                                <?php if ($sdata[0]->jenis_klmin == 'LAKI-LAKI'){ ?>
                                            <div class="el-card-item">
                                            <div class="el-card-avatar el-overlay-1"> <img src="{{ url('/') }}/assets/plugins/images/male-placeholder.jpg" />
                                                <div class="el-overlay">
                                                    <ul class="el-info">
                                                        <li><a class="btn default btn-outline image-popup-vertical-fit" href="{{ url('/') }}/assets/plugins/images/male-placeholder.jpg"><i class="icon-magnifier"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            </div>
                                            <?php }else{ ?>
                                            <div class="el-card-item">
                                            <div class="el-card-avatar el-overlay-1"> <img src="{{ url('/') }}/assets/plugins/images/female-placeholder.jpg" />
                                                <div class="el-overlay">
                                                    <ul class="el-info">
                                                        <li><a class="btn default btn-outline image-popup-vertical-fit" href="{{ url('/') }}/assets/plugins/images/female-placeholder.jpg"><i class="icon-magnifier"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            </div>
                                            <?php } ?> 

                                            <?php } ?> 
                                        <?php }else{ ?>
                                            <?php if ($sdata[0]->jenis_klmin == 'LAKI-LAKI'){ ?>
                                            <div class="el-card-item">
                                            <div class="el-card-avatar el-overlay-1"> <img src="{{ url('/') }}/assets/plugins/images/male-placeholder.jpg" />
                                                <div class="el-overlay">
                                                    <ul class="el-info">
                                                        <li><a class="btn default btn-outline image-popup-vertical-fit" href="{{ url('/') }}/assets/plugins/images/male-placeholder.jpg"><i class="icon-magnifier"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            </div>
                                            <?php }else{ ?>
                                            <div class="el-card-item">
                                            <div class="el-card-avatar el-overlay-1"> <img src="{{ url('/') }}/assets/plugins/images/female-placeholder.jpg" />
                                                <div class="el-overlay">
                                                    <ul class="el-info">
                                                        <li><a class="btn default btn-outline image-popup-vertical-fit" href="{{ url('/') }}/assets/plugins/images/female-placeholder.jpg"><i class="icon-magnifier"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            </div>
                                            <?php } ?> 
                                        <?php } ?> 
                                    
                                    </div>
                                    </tr>
                                    
                                    </tbody>
                                    </table>
                                    </div>
                                    <div class="col-lg-9 col-md-9 col-sm-6">
                                        <table class="table">
                                                <tbody>
                                                    <tr>
                                                        <td width="250"><h4>NAMA LENGKAP</h4></td>
                                                        <td> <h4><?php echo $sdata[0]->nama_lgkp; ?></h4> </td>
                                                        <td><h4></h4></td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>NIK</h4></td>
                                                        <td> <h4><?php echo $sdata[0]->nik; ?></h4> </td>
                                                        <td><h4></h4></td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>NO KK</h4></td>
                                                        <td> <h4><?php echo $sdata[0]->no_kk; ?></h4> </td>
                                                        <td><h4></h4></td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>STAT HUBKEL</h4></td>
                                                        <td> <h4><?php echo $sdata[0]->stat_hbkel; ?></h4> </td>
                                                        <td><h4></h4></td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>TEMPAT LAHIR</h4></td>
                                                        <td> <h4><?php echo $sdata[0]->tmpt_lhr; ?></h4> </td>
                                                        <td><h4></h4></td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>TANGGAL LAHIR</h4></td>
                                                        <td> <h4><?php echo $sdata[0]->tgl_lhr; ?></h4> </td>
                                                        <td><h4></h4></td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>JENIS KELAMIN</h4></td>
                                                        <td> <h4><?php echo $sdata[0]->jenis_klmin; ?></h4> </td>
                                                        <td><h4></h4></td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>KECAMATAN</h4></td>
                                                        <td> <h4><?php echo $sdata[0]->nama_kec; ?></h4> </td>
                                                        <td><h4></h4></td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>KELURAHAN</h4></td>
                                                        <td> <h4><?php echo $sdata[0]->nama_kel; ?></h4> </td>
                                                        <td><h4></h4></td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>STATUS PEREKAMAN</h4></td>
                                                        <?php if ($sdata[0]->current_status_code == 'ENROLL_FAILURE_AT_REGIONAL' || $sdata[0]->current_status_code == 'PACKET_RETRY' || $sdata[0]->current_status_code == 'RECEIVED_AT_CENTRAL' || $sdata[0]->current_status_code == 'PROCESSING' || $sdata[0]->current_status_code == 'SENT_FOR_ENROLLMENT' || $sdata[0]->current_status_code == 'ENROLL_FAILURE_AT_CENTRAL' || $sdata[0]->current_status_code == 'SEARCH_FAILURE_AT_CENTRAL' || $sdata[0]->current_status_code == 'ADJUDICATE_RECORD' || $sdata[0]->current_status_code == 'ADJUDICATE_IN_PROCESS' || $sdata[0]->current_status_code == 'SENT_FOR_DEDUP' || $sdata[0]->current_status_code == 'DUPLICATE_RECORD' || $sdata[0]->current_status_code == 'BELUM REKAM'){ ?>
                                                        <td> <h4 class="text-danger"><b><?php echo $sdata[0]->current_status_code; ?></b></h4> </td>
                                                    <?php }else{ ?>
                                                        <td> <h4 class="text-success"><b><?php echo $sdata[0]->current_status_code; ?></b></h4> </td>
                                                    <?php } ?>
                                                        <td><h4></h4></td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>KETERANGAN </h4></td>
                                                    <?php if ($sdata[0]->ket == 'TIDAK AKTIF DI SIAK'){ ?>
                                                        <td> <h4 class="text-danger"><b><?php echo $sdata[0]->ket; ?></b></h4> </td>
                                                    <?php }else{ ?>
                                                        <td> <h4 class="text-success"><b><?php echo $sdata[0]->ket; ?></b></h4> </td>
                                                    <?php } ?>
                                                        <td><h4></h4></td>
                                                    </tr>
                                                    <?php if ($sdata[0]->ket == 'TIDAK AKTIF DI SIAK'){ ?>
                                                        <?php if (!empty($spindah)){ ?>
                                                        <tr>
                                                            <td><h4>HISTORY PINDAH</h4></td>
                                                            <?php if ($spindah[0]->klasifikasi_pindah == 'ANTAR KECAMATAN' || $spindah[0]->klasifikasi_pindah == 'ANTAR DESA/KELURAHAN' || $spindah[0]->klasifikasi_pindah == 'DALAM SATU DESA/KELURAHAN'){ ?>
                                                                <td> <h4 class="text-danger"><?php echo $spindah[0]->no_pindah; ?></h4> </td>
                                                            <td> <h4 class="text-danger"><?php echo $spindah[0]->klasifikasi_pindah; ?></h4> </
                                                               <?php }else{ ?> 
                                                                <td> <h4 class="text-danger"><?php echo $spindah[0]->no_pindah; ?></h4> </td>
                                                            <td> <h4 class="text-danger"><?php echo $spindah[0]->klasifikasi_pindah; ?></h4> </
                                                               <?php } ?> 
                                                            td>

                                                        </tr>
                                                        <tr>
                                                            <td><h4>DARI :</h4></td>
                                                            <td> <h4><?php echo $spindah[0]->dari_nama_provinsi; ?></h4> </td>
                                                            <td> <h4><?php echo $spindah[0]->dari_nama_kabupaten; ?></h4> </td>
                                                        </tr>
                                                        <tr>
                                                            <td><h4></h4></td>
                                                            <td> <h4><?php echo $spindah[0]->dari_nama_kecamatan; ?></h4> </td>
                                                            <td> <h4><?php echo $spindah[0]->dari_nama_kelurahan; ?></h4> </td>
                                                        </tr>
                                                        <tr>
                                                            <td><h4>KE :</h4></td>
                                                            <td> <h4><?php echo $spindah[0]->tujuan_nama_provinsi; ?></h4> </td>
                                                            <td> <h4><?php echo $spindah[0]->tujuan_nama_kabupaten; ?></h4> </td>
                                                        </tr>
                                                        <tr>
                                                            <td><h4></h4></td>
                                                            <td> <h4><?php echo $spindah[0]->tujuan_nama_kecamatan; ?></h4> </td>
                                                            <td> <h4><?php echo $spindah[0]->tujuan_nama_kelurahan; ?></h4> </td>
                                                        </tr>
                                                        <?php } ?>
                                                    <?php } ?>
                                                    <?php if ($sdata[0]->ket == 'TIDAK AKTIF DI SIAK'){ ?>
                                                        <?php if (!empty($smati)){ ?>
                                                        <tr>
                                                            <td><h4>HISTORY KEMATIAN</h4></td>
                                                            <td colspan="2"> <h4 class="text-danger"><?php echo $smati[0]->ket; ?></h4> </td>
                                                        </tr>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        

                                    </div>
                                    </div>
                                   
                            </div>
                        </div>
                    </div>
                    </div>
                   
               
                <?php }else{ ?>
                <?php if (!empty($is_ada)){ ?>
                 <div >
                    <div class="col-lg-6">
                        <div class="white-box">
                            <div class="">
                                <h1 class="m-b-0 m-t-0">Nik Single Tidak Ditemukan</h1>
                                <div class="row el-element-overlay m-b-40">
                                    <div class="col-lg-4 col-md-4 col-sm-6">
                                    <div class="white-box">
                                        <div class="el-card-item">
                                            <div class="el-card-avatar el-overlay-1"> <img src="{{ url('/') }}/assets/plugins/images/calming-cat.gif" />
                                                <div class="el-overlay">
                                                    <ul class="el-info">
                                                        <li><a class="btn default btn-outline image-popup-vertical-fit" href="{{ url('/') }}/assets/plugins/images/calming-cat.gif"><i class="icon-magnifier"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            </div>
                                    </div>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-6">
                                        <table class="table">
                                                <tbody>
                                                    <tr>
                                                        <td width="250"><h4>NAMA LENGKAP</h4></td>
                                                        <td> <h4>-</h4> </td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>NIK</h4></td>
                                                        <td> <h4>-</h4> </td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>NO KK</h4></td>
                                                        <td> <h4>-</h4> </td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>TEMPAT LAHIR</h4></td>
                                                        <td> <h4>-</h4> </td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>TANGGAL LAHIR</h4></td>
                                                        <td> <h4>-</h4> </td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>JENIS KELAMIN</h4></td>
                                                        <td> <h4>-</h4> </td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>STATUS PEREKAMAN</h4></td>
                                                        
                                                        <td> <h4>-</h4> </td>
                                                    
                                                    </tr>
                                                    <tr>
                                                        <td><h4>KETERANGAN</h4></td>
                                                        
                                                        <td> <h4>-</h4> </td>
                                                    
                                                    </tr>
                                                </tbody>
                                            </table>
                                        

                                    </div>
                                    </div>
                                   
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <?php } ?>
                
                        

            <!-- /.container-fluid -->
         @include('shared.footer_detail')