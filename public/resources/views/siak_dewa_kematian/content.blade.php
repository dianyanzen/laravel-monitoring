<div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $stitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                        <ol class="breadcrumb">
                           <li class="text-info"><?php echo $stitle; ?></li>
                        </ol>
                    </div>
                </div>
                <!-- .row -->
                 <div class="row">
                    <form name ="get_form" action="<?php echo $my_url; ?>" method="post">
                        @csrf
                    <div class="col-sm-12">
                        <div class="white-box">
                            <div class="row">
                                <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_akta_kmt" type="checkbox" checked disabled="true">
                                            <label for="cb_akta_kmt"> No Akta Kematian</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">         
                                    <div class="form-group">
                                        <input class="form-control" type="text" id="akta_kmt" onkeypress="return isakta_kmt(event)" name="akta_kmt" maxlength="21"/></div>
                                </div>
                                </div>
                            
                            <div class="row">
                                <button type="submit" class="btn btn-info waves-effect waves-light m-r-10 pull-right" id="btn-filter" onclick="on_serach();" >Search <i class="mdi  mdi-magnify fa-fw"></i></button>
                                  <button type="button" class="btn btn-invert waves-effect waves-light m-r-10 pull-right" id="btn-reset" onclick="on_clear();">Clear <i class="mdi  mdi-delete fa-fw"></i></button>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
                <?php if (!empty($data)){ ?>
                   
                         <div class="row">
                    <div class="col-lg-12">
                        <div class="white-box">
                            <div class="">
                                <h2 class="m-b-0 m-t-0">Data Biometric</h2>
                                <hr>
                                <div class="row el-element-overlay m-b-40">
                                    <div class="col-lg-4 col-md-4 col-sm-6">
                                    <div class="white-box">
                                        <?php if (!empty($face)){ ?>
                                        <div class="el-card-item">
                                            <div class="el-card-avatar el-overlay-1"> <img src="data:image/jpeg;base64,<?php echo base64_encode($face[0]->face); ?>" />
                                                <div class="el-overlay">
                                                    <ul class="el-info">
                                                        <li><a class="btn default btn-outline image-popup-vertical-fit" href="data:image/jpeg;base64,<?php echo base64_encode($face[0]->face); ?>"><i class="icon-magnifier"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <?php }else{ ?>
                                            <?php if ($data[0]->mati_jns_kelamin == 'LAKI-LAKI'){ ?>
                                            <div class="el-card-item">
                                            <div class="el-card-avatar el-overlay-1"> <img src="{{ url('/') }}/assets/plugins/images/male-placeholder.jpg" />
                                                <div class="el-overlay">
                                                    <ul class="el-info">
                                                        <li><a class="btn default btn-outline image-popup-vertical-fit" href="{{ url('/') }}/assets/plugins/images/male-placeholder.jpg"><i class="icon-magnifier"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            </div>
                                            <?php }else{ ?>
                                            <div class="el-card-item">
                                            <div class="el-card-avatar el-overlay-1"> <img src="{{ url('/') }}/assets/plugins/images/female-placeholder.jpg" />
                                                <div class="el-overlay">
                                                    <ul class="el-info">
                                                        <li><a class="btn default btn-outline image-popup-vertical-fit" href="{{ url('/') }}/assets/plugins/images/female-placeholder.jpg"><i class="icon-magnifier"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            </div>
                                            <?php } ?> 
                                        <?php } ?> 
                                    </div>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-6">
                                        <div class="row">
                                        <table class="table">
                                                <tbody>
                                                    <tr>
                                                        <td  width="250"><h4>NO AKTA KEMATIAN</h4></td>
                                                        <td> <h4>:</h4> </td>
                                                        <td> <input class="form-control" type="text" id="adm_akta_no" name="adm_akta_no" value="<?php echo $data[0]->adm_akta_no; ?>" style="text-transform:uppercase !important" disabled/></td>
                                                        
                                                    </tr>
                                                     <tr>
                                                        <td><h4>NIK</h4></td>
                                                        <td> <h4>:</h4> </td>
                                                        <td> <input class="form-control" type="text" id="mati_nik" name="mati_nik" value="<?php echo $data[0]->mati_nik; ?>" style="text-transform:uppercase !important" disabled/></td>
                                                        
                                                    </tr>
                                                    <tr>
                                                        <td><h4>NO KK</h4></td>
                                                        <td> <h4>:</h4> </td>
                                                        <td> <input class="form-control" type="text" id="mati_no_kk" name="mati_no_kk" value="<?php echo $data[0]->mati_no_kk; ?>" style="text-transform:uppercase !important" disabled/></td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>JENIS KELAMIN</h4></td>
                                                        <td> <h4>:</h4> </td>
                                                        <td> <input class="form-control" type="text" id="mati_jns_kelamin" name="mati_jns_kelamin" value="<?php echo $data[0]->mati_jns_kelamin; ?>" style="text-transform:uppercase !important" disabled/></td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>NAMA LENGKAP</h4></td>
                                                        <td> <h4>:</h4> </td>
                                                        <td> <input class="form-control" type="text" id="mati_nama_lgkp" name="mati_nama_lgkp" value="<?php echo $data[0]->mati_nama_lgkp; ?>" style="text-transform:uppercase !important"/> </td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>TEMPAT LAHIR</h4></td>
                                                        <td> <h4>:</h4> </td>
                                                        <td> <input class="form-control" type="text" id="mati_tmpt_lahir" name="mati_tmpt_lahir" value="<?php echo $data[0]->mati_tmpt_lahir; ?>" style="text-transform:uppercase !important"/></td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>TANGGAL LAHIR</h4></td>
                                                        <td> <h4>:</h4> </td>
                                                        <td> <input class="form-control" type="text" id="mati_tgl_lahir" name="mati_tgl_lahir" value="<?php echo $data[0]->mati_tgl_lahir; ?>" data-mask="99-99-9999" onkeypress="return is_date(event)" maxlength="10"/> </td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>TEMPAT MENINGGAL</h4></td>
                                                        <td> <h4>:</h4> </td>
                                                        <td> <input class="form-control" type="text" id="mati_tmpt_mati" name="mati_tmpt_mati" value="<?php echo $data[0]->mati_tmpt_mati; ?>" style="text-transform:uppercase !important"/></td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>TANGGAL MENINGGAL</h4></td>
                                                        <td> <h4>:</h4> </td>
                                                        <td> <input class="form-control" type="text" id="mati_tgl_mati" name="mati_tgl_mati" value="<?php echo $data[0]->mati_tgl_mati; ?>" data-mask="99-99-9999" onkeypress="return is_date(event)" maxlength="10"/> </td>
                                                    </tr>

                                                    <tr>
                                                        <td><h4>NAMA IBU</h4></td>
                                                        <td> <h4>:</h4> </td>
                                                        <td> <input class="form-control" type="text" id="ibu_nama_lgkp" name="ibu_nama_lgkp" value="<?php echo $data[0]->ibu_nama_lgkp; ?>" style="text-transform:uppercase !important"/> </td>
                                                    </tr>
                                                    <tr>
                                                        <td><h4>NAMA AYAH</h4></td>
                                                        <td> <h4>:</h4> </td>
                                                        <td> <input class="form-control" type="text" id="ayah_nama_lgkp" name="ayah_nama_lgkp" value="<?php echo $data[0]->ayah_nama_lgkp; ?>" style="text-transform:uppercase !important"/> </td>
                                                    </tr>
                                                    
                                                </tbody>
                                            </table>
                                            </div>
                                            <div class="row">
                                                <button  class="btn btn-info waves-effect waves-light m-r-10 pull-right" id="btn-filter" onclick="on_edit();" >Edit <i class="mdi  mdi-pencil fa-fw"></i></button>
                                            </div>
                                        

                                    </div>
                                    </div>
                                   
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
            <!-- /.container-fluid -->
         @include('shared.footer_detail')