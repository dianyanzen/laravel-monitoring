<!DOCTYPE html>
<html lang="en">
@include('shared.head')
<body class="fix-header">
    <div id="wrapper">
            @include('shared.nav_top')
            @include('shared/nav_side')
            @include('siak_dewa_kematian/content')
    </div>
     
    @include('shared/footer')
    
    @include('siak_dewa_kematian/javascript')
</body>
</html>