
    <script type="text/javascript">
        var init_kec = <?php $user_no_kec = (!empty($user_no_kec)) ? $user_no_kec : 0; echo $user_no_kec;?>;
    </script>
     <script>
        function isakta_kmt(evt)
       {
         var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode != 45 && charCode != 75 && charCode != 107 && charCode != 77 && charCode != 109 && charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }

        function is_date(evt)
       {
         var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode != 45 && charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }
       
    $(document).ready(function($) {
        $('#akta_kmt').focus();
        $('#akta_kmt').on('paste', function() {
          var $el = $(this);
          setTimeout(function() {
            $el.val(function(i, val) {
              return val.replace(/[^0-9,-KM]/g, '')
            })
          })
        });
    });
   
    function on_clear() {
        $('#akta_kmt').val("");
    }

    function on_edit() {
        if (validationdaily()){
            do_edit();
        }
            
    }
     
    function validationdaily() {
            
            if ($("#mati_tgl_lahir").val().length != 10) {                
                  swal("Warning!", "Format Tanggal Lahir Salah !", "warning");  
                  $("#mati_tgl_lahir").focus();
                 return false;
            }
            if ($("#mati_tgl_mati").val().length != 10) {                
                  swal("Warning!", "Format Tanggal Meninggal Salah !", "warning");
                  $("#mati_tgl_mati").focus();
                 return false;
            }
            if ($("#mati_tgl_lahir").val().length == 0) {                
                  swal("Warning!", "Format Tanggal Lahir Salah !", "warning");  
                  $("#mati_tgl_lahir").focus();
                 return false;
            }
            if ($("#mati_tgl_mati").val().length == 0) {                
                  swal("Warning!", "Format Tanggal Meninggal Salah !", "warning");  
                  $("#mati_tgl_mati").focus();
            }
            if ($("#adm_akta_no").val().length == 0) {                
                  swal("Warning!", "Adm Akta No Tidak Boleh Kosong !", "warning");  
                  $("#adm_akta_no").focus();
                 return false;
            }
            if ($("#mati_nama_lgkp").val().length == 0) {                
                  swal("Warning!", "Nama Lengkap Tidak Boleh Kosong !", "warning");  
                  $("#mati_nama_lgkp").focus();
            }
            if ($("#mati_tmpt_lahir").val().length == 0) {                
                  swal("Warning!", "Tempat Lahir Tidak Boleh Kosong !", "warning");  
                  $("#mati_tmpt_lahir").focus();
                 return false;
            }
            if ($("#mati_tmpt_mati").val().length == 0) {                
                  swal("Warning!", "Tempat Meninggal Tidak Boleh Kosong !", "warning");  
                  $("#mati_tmpt_mati").focus();
                 return false;
            }
            if ($("#ibu_nama_lgkp").val().length == 0) {                
                  swal("Warning!", "Nama Ibu Tidak Boleh Kosong !", "warning");  
                  $("#ibu_nama_lgkp").focus();
                 return false;
            }
          
            return true;
    }
     function do_edit() {
            swal({   
            title: "Apakah Anda Yakin ?",   
            text: "Mengubah Data Akta Kematian ?",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Ya, Ubah Data",   
            cancelButtonText: "Tidak, Jangan Diubah",   
            closeOnConfirm: false,   
            closeOnCancel: false 
            }, function(isConfirm){   
                if (isConfirm) {
                 $.ajax({
                    type: "post",
                    url: BASE_URL+"Siak/update_dewa_kematian",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        adm_akta_no : $("#adm_akta_no").val(),
                        mati_nik : $("#mati_nik").val(),
                        mati_no_kk : $("#mati_no_kk").val(),
                        mati_jns_kelamin : $("#mati_jns_kelamin").val(),
                        mati_nama_lgkp : $("#mati_nama_lgkp").val(),
                        mati_tmpt_lahir : $("#mati_tmpt_lahir").val(),
                        mati_tgl_lahir : $("#mati_tgl_lahir").val(),
                        mati_tmpt_mati : $("#mati_tmpt_mati").val(),
                        mati_tgl_mati : $("#mati_tgl_mati").val(),
                        ibu_nama_lgkp : $("#ibu_nama_lgkp").val(),
                        ayah_nama_lgkp : $("#ayah_nama_lgkp").val(),

                    },
                    beforeSend:
                    function () {
                         block_screen();
                    },
                    success: function (data) {
                        console.log(data);
                        if(data.success){
                            swal("Success!", data.message, "success");    
                        }else{
                            swal("Warning!", data.message, "warning"); 
                        }
                        
                    },
                    error:
                    function (data) {
                        unblock_screen();
                        swal("Error!", "Ooops!, Please Try Again, Something Went Wrong", "error"); 

                    },
                    complete:
                    function (response) {
                        unblock_screen();
                    }
                });
                     
                } else {     
                    swal("Cancelled", "Data Tidak Jadi Diubah :)", "error");   
                } 
            });
        }
    
    function block_screen(){
        
        $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff'
            },
            baseZ: 2000
        }); 
    }
    function unblock_screen(){
       $.unblockUI();
    }
    function on_serach(){
        
        $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff'
            },
            baseZ: 2000
        }); 
    }
    jQuery(document).ready(function() {
            $(".select2").select2();
             $('#mytable').DataTable({
                 "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
           $('#tanggal').val("<?php echo date('d-m-Y');?> - <?php echo date('d-m-Y');?>");
        });
    </script>
   