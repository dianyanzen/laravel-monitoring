      <script>
        function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode != 44 && charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }
        function edit(data) {
            block_screen();
            var url = "{{ url('/') }}/";
            url += "Setting/Pejabat/Edit/";
            url += data;
            var win = window.location.replace(url);
            win.focus();
        }
       
        function hapus(pejabat) {
           
            swal({   
            title: "Apakah Anda Yakin ?",   
            text: "Menghapus Data Ini ?",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Ya, Hapus Pejabat",   
            cancelButtonText: "Tidak, Jangan Dihapus",   
            closeOnConfirm: false,   
            closeOnCancel: false 
            }, function(isConfirm){   
                if (isConfirm) {
                $.ajax({
                    type: "post",
                    url: BASE_URL+"Setting/delete_pejabat",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        pejabat : pejabat
                    },
                    beforeSend:
                    function () {
                         block_screen();
                    },
                    success: function (data) {
                        console.log(data);
                         if (data.message_type > 0){
                              swal("Berhasil !", "Data Ini Telah Dihapus !", "success");
                        }else{
                            swal("Warning!", data.message, "warning");  
                        }
                    },
                    error:
                    function (data) {
                        unblock_screen();
                        swal("Error!", "Ooops!, Please Try Again, Something Went Wrong", "error"); 

                    },
                    complete:
                    function (response) {
                        unblock_screen();
                        get_select_table();
                    }
                });
                     
                } else {     
                    swal("Cancelled", "Data Ini Tidak Jadi Dihapus :)", "error");   
                } 
            });
        }
        function on_add() {
            block_screen();
            var url = "{{ url('/') }}/";
            url += "Setting/Pejabat/Add";
            var win = window.location.replace(url);
            win.focus();
        }
        function on_select(){
            get_select_table();
        }
        $("#nip").keydown(function (e) {
            if (e.keyCode == 13) {
             e.preventDefault();
             submitform();
            }
            
          });

        $("#nama").keydown(function (e) {
            if (e.keyCode == 13) {
             e.preventDefault();
             submitform();
            }
            
          });

          function submitform(){
            get_select_table();
          }
        $(document).ready(function() {
           get_table();

        });
    function get_table() {
        $('#pejabat_list').DataTable({
                destroy: true,
                 "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                 "columnDefs": [
                    {"className": "dt-left", "targets": "_all"}
                  ],
                "pageLength" : 50,
                "ajax": {
                 "url": BASE_URL+"Setting/get_pejabat_list",
                "type": "post",
                "data": {
                "_token": "{{ csrf_token() }}",
                "nip":  $("#nip").val(),
                "nama":  $("#nama").val()

                }
                }
            });
    }
    function get_select_table() {
        $('#pejabat_list').DataTable({
                destroy: true,
                 "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                 "columnDefs": [
                    {"className": "dt-left", "targets": "_all"}
                  ],
                "pageLength" : 50,
                "ajax": {
                 "url": BASE_URL+"Setting/get_pejabat_list",
                "type": "post",
                "data": {
                "_token": "{{ csrf_token() }}",
                "nip":  $("#nip").val(),
                "nama":  $("#nama").val()

                }
                }
            });
    }
    
    jQuery(document).ready(function() {
        $(".select2").select2();
        $('#tanggal').val("<?php echo date('d-m-Y');?>");
        });
     function block_screen(){
        
        $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff'
            },
            baseZ: 2000
        }); 
    }
    function unblock_screen(){
       $.unblockUI();
    }
    </script>