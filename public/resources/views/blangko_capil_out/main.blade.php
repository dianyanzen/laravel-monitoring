<!DOCTYPE html>
<html lang="en">
@include('shared.head')
<body class="fix-header">
    <div id="wrapper">
            @include('shared.nav_top')
            @include('shared/nav_side')
            @include('blangko_capil_out/content')
    </div>
     
    @include('shared/footer')
    
    @include('blangko_capil_out/javascript')
</body>
</html>