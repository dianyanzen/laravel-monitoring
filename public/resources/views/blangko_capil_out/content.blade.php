 <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $stitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                        <ol class="breadcrumb">
                            <li class="active"><?php echo $mtitle; ?></li>
                        </ol>
                    </div>
                </div>
                 <div class="row">
                <!-- <form name ="get_form" action="<?php echo $my_url; ?>" method="post"> -->
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div class="white-box">
                            <div class="row">
                                <div class="col-lg-6">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_kec" type="checkbox" checked disabled="true">
                                            <label for="cb_kec"> Jenis Blangko</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">         
                                    <div class="form-group" <?php if (!empty($is_colnotwil)){echo 'style="display: none!important;"'; }?>>
                                        <select class="form-control select2" name="no_jenis" id="no_jenis">
                                         <option  value="0" selected>JENIS BLANGKO</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_tgl" type="checkbox" checked disabled="true">
                                            <label for="cb_tgl"> Tanggal</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">         
                                    <div class="form-group">
                                        <input class="form-control" id="tanggal" placeholder="dd/mm/yyyy" type="text" value="<?php echo date("d-m-Y") ?>"></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_tgl" type="checkbox" checked disabled="true">
                                            <label for="cb_tgl"> Pengambilan</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">         
                                    <div class="form-group">
                                        <input class="form-control" type="text" id="txt_pengambilan" value="0" onkeypress="return isNumberKey(event)" ></div>
                                </div>
                                 <div class="col-lg-6">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_tgl" type="checkbox" checked disabled="true">
                                            <label for="cb_tgl"> Rusak</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">         
                                    <div class="form-group">
                                        <input class="form-control" id="txt_rusak" type="text" value="0" onkeypress="return isNumberKey(event)" ></div>
                                </div>
                                 <div class="col-lg-6">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_tgl" type="checkbox" checked disabled="true">
                                            <label for="cb_tgl"> Keterangan</label>
                                        </div>
                                    </div>
                                </div>
                                   <div class="col-lg-6">         
                                    <div class="form-group">
                                        <textarea class="form-control" type="text" id="txt_ket" ></textarea> </div>
                                </div>
                                <div class="col-lg-3">         
                                    <div class="form-group">
                                        
                                    </div>
                                </div>
                                <div class="col-lg-3">         
                                    <!-- <div class="form-group">
                                        <input class="form-control" type="text" id="nik" name="nik" disabled="true" onkeypress="return isNumberKey(event)" onchange="onlyNum()" /></div> -->
                                </div>
                            </div>
                            <div class="row">
                                <button type="submit" class="btn btn-info waves-effect waves-light m-r-10 pull-right" id="btn-filter" onclick="on_save();" >Save <i class="mdi  mdi-content-save fa-fw"></i></button>
                                  <button type="button" class="btn btn-invert waves-effect waves-light m-r-10 pull-right" id="btn-reset" onclick="on_clear();">Clear <i class="mdi  mdi-delete fa-fw"></i></button>
                            </div>
                        </div>
                    </div>
                    <!-- </form> -->
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">Kendali Blangko Capil</div>
                            <div class="panel-wrapper collapse in">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th class="text-center">#</th>
                                            <th>Keterangan</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td align="center">1</td>
                                            <td>Jenis Blangko</td>
                                            <td><span id="lbl_blangko">-</span></td>
                                        </tr>
                                        <tr>
                                            <td align="center">2</td>
                                            <td>Tanggal</td>
                                            <td><?php echo date("d-m-Y") ?></td>
                                        </tr>
                                        <tr>
                                            <td align="center">3</td>
                                            <td>Pengambilan</td>
                                            <td><span id="lbl_pengeluaran">0</span></td>
                                        </tr>
                                         <tr>
                                            <td align="center">4</td>
                                            <td>Rusak</td>
                                            <td><span id="lbl_rusak">0</span></td>
                                        </tr>
                                         <tr>
                                            <td align="center">6</td>
                                            <td>Saldo</td>
                                            <td><span id="lbl_saldo">0</span></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                
                 </div>
       @include('shared.footer_detail')