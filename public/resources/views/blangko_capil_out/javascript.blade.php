     <script>
        function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode != 44 && charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }
      $("#txt_pengambilan").keydown(function (e) {
            if (e.keyCode == 13) {
             e.preventDefault();
             on_save();
            }
            
          });

        $("#txt_rusak").keydown(function (e) {
            if (e.keyCode == 13) {
             e.preventDefault();
             on_save();
            }
            
          });

    $(document).ready(function() {
    
    console.log(init_kec);
    console.log(init_level);
     $.ajax({
                    type: "post",
                    url: BASE_URL+"Shared_api/get_capil_blk_opt",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                    },
                    beforeSend:
                    function () {
                        $('select[name="no_jenis"]').attr("disabled",true);
                    },
                    success: function (data) {
                        console.log(data);
                       $('select[name="no_jenis"]').empty();
                        $.each(data, function(key, value) {
                            $('select[name="no_jenis"]').append('<option value="'+ value.id +'">'+ value.jenis_blangko +'</option>');
                        });
                        $('select[name="no_jenis"]').val(0).trigger("change");
                        
                    },
                    error:
                    function (data) {
               

                    },
                    complete:
                    function (response) {
                        $('select[name="no_jenis"]').attr("disabled",false);
                        
                        
                    }
                });
            $('select[name="no_jenis"]').on('change', function() {
                get_all_data();
                get_data();
            });

    });
    function get_data(){
        $.ajax({
                    type: "post",
                    url: BASE_URL+"BlangkoCapil/get_capil_data",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        jenis_blangko : $("#no_jenis").val(),
                        tanggal : $("#tanggal").val()
                    },
                    beforeSend:
                    function () {
                        
                    },
                    success: function (data) {
                        console.log(data);
                        $('#txt_pengambilan').val(data.pengambilan);
                        $('#txt_rusak').val(data.rusak);
                        $('#txt_ket').empty();
                        $('#txt_ket').append(data.ket);
                        
                    },
                    error:
                    function (data) {
               

                    },
                    complete:
                    function (response) {
                        // $('select[name="no_jenis"]').attr("disabled",false);
                    }
                });
    }
    function get_all_data(){
        $.ajax({
                    type: "post",
                    url: BASE_URL+"BlangkoCapil/get_allin_capil_data_out",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        jenis_blangko : $("#no_jenis").val(),
                        tanggal : $("#tanggal").val()
                    },
                    beforeSend:
                    function () {
                        
                    },
                    success: function (data) {
                        console.log(data);
                        $('#lbl_blangko').text(data.jenis_blangko);
                        $('#lbl_rusak').text(data.rusak);
                        $('#lbl_pengeluaran').text(data.keluar);
                        $('#lbl_saldo').text(data.sisa);
                    },
                    error:
                    function (data) {
               

                    },
                    complete:
                    function (response) {
                        // $('select[name="no_jenis"]').attr("disabled",false);
                    }
                });
    }
    function on_clear() {
        
        $('select[name="no_jenis"]').val(init_kec).trigger("change");
        $('#txt_pengambilan').val("0");
        $('#txt_rusak').val("0");
        $('#tanggal').val("<?php echo date('d-m-Y');?>");
        $('#txt_ket').append("");
    }
     function on_save(){
        if (validationdaily()){
          do_save();
        }
  
        
    }
    function validationdaily() {
        var jenis_blangko = $("#no_jenis");
        var txt_jenis = $("#lbl_blangko");
        var pengambilan = $("#txt_pengambilan");
        var rusak = $("#txt_rusak");
             if (jenis_blangko.val() == 0) {                
                  swal("Warning!", "Jenis Blangko Tidak Boleh Kosong !", "warning");  
                 return false;
            }
            if (txt_jenis.text() == '-') {                
                  swal("Warning!", "Jenis Blangko Tidak Boleh Kosong !", "warning");  
                 return false;
            }
            if (pengambilan.val().length == 0) {                
                  swal("Warning!", "Pengembalian Tidak Boleh Kosong !", "warning");  
                 return false;
            }
            if (rusak.val().length == 0) {                
                  swal("Warning!", "Blangko Rusak Tidak Boleh Kosong !", "warning");  
                 return false;
            }
            if( parseInt(pengambilan.val()) <= 0) {
                swal("Error!", "Tidak Boleh Mengambil Kurang Dari 0", "error"); 
              return false; 
            }
            if( parseInt(rusak.val()) < 0) {
                swal("Error!", "Rusak Tidak Boleh Kurang Dari 0", "error"); 
              return false; 
            }
            if (init_kec != 0){
             console.log(init_kec);
             swal("Error!", "Hanya Operator Dinas Saja Yang Boleh Menginput Kendali Blangko", "error"); 
              return false;    
            }
            return true;
       
        }
        function do_save(){
            $.ajax({
                    type: "post",
                    url: BASE_URL+"BlangkoCapil/do_save",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        jenis_blangko : $("#no_jenis").val(),
                        tanggal : $("#tanggal").val(),
                        pengambilan : $("#txt_pengambilan").val(),
                        rusak : $("#txt_rusak").val(),
                        ket : $("#txt_ket").val(),
                    },
                    beforeSend:
                    function () {
                        block_screen();
                    },
                    success: function (data) {
                        console.log(data);
                        swal("Success!", data.message, "success");
                    },
                    error:
                    function (data) {
                        unblock_screen();
                        swal("Error!", "Ooops!, Please Try Again, Something Went Wrong", "error"); 

                    },
                    complete:
                    function (response) {
                        unblock_screen();
                        get_all_data();
                    }
                });
        }
     function block_screen(){
        
        $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff'
            },
            baseZ: 2000
        }); 
    }
    function unblock_screen(){
       $.unblockUI();
    }
    jQuery(document).ready(function() {
            $(".select2").select2();
            $('.input-daterange-datepicker').daterangepicker({
                buttonClasses: ['btn', 'btn-sm'],
                applyClass: 'btn-info',
                cancelClass: 'btn-info',
                locale: {
                    format: 'DD-MM-YYYY'
                },
                startDate: '<?php echo date('d-m-Y');?>',
                endDate: '<?php echo date('d-m-Y');?>'
            });
             $('#mytable').DataTable({
                 "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
        $('#tanggal').val("<?php echo date('d-m-Y');?>");
        $('#txt_pengambilan').val("0");
        $('#txt_rusak').val("0");
        $('#txt_ket').empty();
        $('#txt_ket').append("");
        });
    </script>
   