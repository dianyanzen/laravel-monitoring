    <script src="{{ url('/') }}/assets/plugins/bower_components/owl.carousel/owl.carousel.min.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/owl.carousel/owl.custom.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/flot/jquery.flot.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/Minimal-Gauge-chart/js/cmGauge.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/Minimal-Gauge-chart/js/cmGauge.js"></script>
    <!-- Footable -->
    <script src="{{ url('/') }}/assets/plugins/bower_components/footable/js/footable.all.min.js"></script>
    
    <!--FooTable init-->
    <script src="{{ url('/') }}/assets/js/footable-init.js"></script>
    <script type="text/javascript" src="{{ url('/') }}/assets/plugins/bower_components/viewerjs-master/dist/jquery.magnify.js"></script>
   <script src="{{ url('/') }}/assets/plugins/bower_components/dropify/dist/js/dropify.min.js"></script>
    <script>
        
        $(document).ready(function () {
          get_api();
        });

    </script>
 <script>
    function format ( d ) {
            // `d` is the original data object for the row
            return '<table class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%" >'+
                '<tr>'+
                    '<td>'+d.img+'</td>'+
                '</tr>'+
            '</table>';
        }
         
        $(document).ready(function() {

            var table = $('#pengajuan-list').DataTable( {
                "ajax": {
                 "url": BASE_URL+"ApiEpunten/get_datang_melengkapi",
                "type": "post",
                },
                responsive: true,
                destroy: true,
                 "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                 "columnDefs": [
                    {"className": "dt-center", "targets": "_all"}
                  ],
                "pageLength" : 50,
                "columns": [
                    {
                        "className":      'details-control',
                        "orderable":      false,
                        "data":           null,
                        "defaultContent": ''
                    },
                    { "data": "no" },
                    { "data": "nik" },
                    { "data": "no_skpwni" },
                    { "data": "nama_lgkp" },
                    { "data": "telepon" },
                    { "data": "email" },
                    { "data": "created_at" },
                    { "data": "aksi" }
                ],
                "order": [[1, 'asc']]
            });
         $('#pengajuan-list tbody').on('click', 'td.details-control', function () {
                var tr = $(this).closest('tr');
                var row = table.row( tr );
         
                if ( row.child.isShown() ) {
                    row.child.hide();
                    tr.removeClass('shown');
                }
                else {
                    row.child( format(row.data()) ).show();
                    tr.addClass('shown');
                }
            });
            // get_table();
             
            
        });
    function get_api(){
                $.ajax({
                    type: "post",
                    url: BASE_URL+"api/ektp",
                    dataType: "json",
                    data: {
                      "_token": "{{ csrf_token() }}",
                    },
                    beforeSend:
                    function () {

                    },
                    success: function (data) {
                        console.log(data);
                        //init data src prop
                        $('select[name="src_prov"]').empty();
                        $('select[name="src_prov"]').append('<option value="0">-- Pilih Provinsi --</option>');
                        $.each(data.prov, function(key, value) {
                            $('select[name="src_prov"]').append('<option value="'+ value.no_prop +'">'+ value.nama_prop +'</option>');
                        });
                        $('select[name="src_prov"]').val("0").trigger("change");

                        //init data kec
                        $('select[name="kec"]').empty();
                        $('select[name="kec"]').append('<option value="0">-- Pilih Kecamatan --</option>');
                        $.each(data.kecamatan, function(key, value) {
                            $('select[name="kec"]').append('<option value="'+ value.no_kec +'">'+ value.nama_kec +'</option>');
                        });
                        $('select[name="kec"]').val("0").trigger("change");

                        //init data jenis klmin
                        $('select[name="jenis_klmin"]').empty();
                        $('select[name="jenis_klmin"]').append('<option value="0">-- Pilih Jenis Kelamin --</option>');
                        $.each(data.jenis_klmin, function(key, value) {
                            $('select[name="jenis_klmin"]').append('<option value="'+ value.no +'">'+ value.no +' - '+ value.jenis_klmin +'</option>');
                        });
                        $('select[name="jenis_klmin"]').val("0").trigger("change");

                        //init data agama
                        $('select[name="agama"]').empty();
                        $('select[name="agama"]').append('<option value="0">-- Pilih Agama --</option>');
                        $.each(data.agama, function(key, value) {
                            $('select[name="agama"]').append('<option value="'+ value.no +'">'+ value.agama +'</option>');
                        });
                        $('select[name="agama"]').val("0").trigger("change");

                        //init data stat_kwn
                        $('select[name="stat_kwn"]').empty();
                        $('select[name="stat_kwn"]').append('<option value="0">-- Pilih Status Perkawinan --</option>');
                        $.each(data.stat_kwn, function(key, value) {
                            $('select[name="stat_kwn"]').append('<option value="'+ value.no +'">'+ value.stat_kwn +'</option>');
                        });
                        $('select[name="stat_kwn"]').val("0").trigger("change");

                        //init data gol_drh
                        $('select[name="gol_drh"]').empty();
                        $('select[name="gol_drh"]').append('<option value="0">-- Pilih Golongan Darah --</option>');
                        $.each(data.gol_drh, function(key, value) {
                            $('select[name="gol_drh"]').append('<option value="'+ value.no +'">'+ value.gol_drh +'</option>');
                        });
                        $('select[name="gol_drh"]').val("0").trigger("change");

                        //init data pendidikan
                        $('select[name="pendidikan"]').empty();
                        $('select[name="pendidikan"]').append('<option value="0">-- Pilih Pendidikan --</option>');
                        $.each(data.pendidikan, function(key, value) {
                            $('select[name="pendidikan"]').append('<option value="'+ value.no +'">'+ value.pendidikan +'</option>');
                        });
                        $('select[name="pendidikan"]').val("0").trigger("change");

                        //init data pekerjaan
                        $('select[name="pekerjaan"]').empty();
                        $('select[name="pekerjaan"]').append('<option value="0">-- Pilih Pekerjaan --</option>');
                        $.each(data.pekerjaan, function(key, value) {
                            $('select[name="pekerjaan"]').append('<option value="'+ value.no +'">'+ value.pekerjaan +'</option>');
                        });
                        $('select[name="pekerjaan"]').val("0").trigger("change");

                        //init data pekerjaan
                        $('select[name="stat_hbkel"]').empty();
                        $('select[name="stat_hbkel"]').append('<option value="0">-- Pilih Hubungan Keluarga --</option>');
                        $.each(data.stat_hbkel, function(key, value) {
                            $('select[name="stat_hbkel"]').append('<option value="'+ value.no +'">'+ value.stat_hbkel +'</option>');
                        });
                        $('select[name="stat_hbkel"]').val("0").trigger("change");

                        //init data pekerjaan
                        $('select[name="alasan_pindah"]').empty();
                        $('select[name="alasan_pindah"]').append('<option value="0">-- Pilih Alasan Pindah --</option>');
                        $.each(data.alasan, function(key, value) {
                            $('select[name="alasan_pindah"]').append('<option value="'+ value.no +'">'+ value.alasan +'</option>');
                        });
                        $('select[name="alasan_pindah"]').val("0").trigger("change");

                        //init data pekerjaan
                        $('select[name="akta_lhr"]').empty();
                        $('select[name="akta_lhr"]').append('<option value="0">-- Pilih Kepemilikan Akta --</option>');
                            $('select[name="akta_lhr"]').append('<option value="1">1 - TIDAK ADA</option>');
                            $('select[name="akta_lhr"]').append('<option value="2">2 - ADA</option>');
                        $('select[name="akta_lhr"]').val("0").trigger("change");
                        
                    },
                    error:
                    function (data) {
               

                    },
                    complete:
                    function (response) {
                      
                    }
                });
            }
   
    function get_table(){
        var table = $('#pengajuan-list').DataTable();
        table.ajax.reload();
        toastr.success('Data Berhasil Di Perbarui !');
    }
    function block_screen(){
        
        $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff'
            },
            baseZ: 2000
        }); 
    }
    function unblock_screen(){
       $.unblockUI();
    }
    
    function get_info(){
      $('#info_modal').modal('show');
    }


    $('#submit_btnntf').click(function (e) {
       if (!validationmodaltolak()) return;
            $.ajax({
                    type: "post",
                    url: BASE_URL+"ApiEpunten/get_datang_tolak",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        id_peng : $("#id_pengajuan_tolak").val(),
                        isi_pesan : $("#isi_pesan").val(),
                        jenis_layanan_tolak : $("#jenis_layanan_tolak").val(),
                        nik_tolak : $("#nik_tolak").val(),
                    },
                    beforeSend:
                    function () {
                         block_screen();
                    },
                    success: function (data) {
                        console.log(data);
                        swal("Success!", data.message, "success");
                        $('#tolak_modal').modal('hide');
                    },
                    error:
                    function (data) {
                        unblock_screen();
                        swal("Error!", "Ooops!, Please Try Again, Something Went Wrong", "error"); 

                    },
                    complete:
                    function (response) {
                        unblock_screen();
                        get_table();
                    }
                });
      
    });

       
        function validationmodaltolak() {
            var id_pengajuan_tolak = $("#id_pengajuan_tolak");
            if (id_pengajuan_tolak.val().length == 0) {
                id_pengajuan_tolak.select();
                swal("Warning!", "ID Pengajuan Tidak Boleh Kosong !", "warning");  
                return false;
            }
            var isi_pesan = $("#isi_pesan");
            if (isi_pesan.val().length < 10) {
                isi_pesan.select();
                swal("Warning!", "Isi Pesan Tidak Boleh Kurang Dari 10 Karakter !", "warning");  
                return false;
            }
           return true
        }

    function edit(id_peng){
         
        var pengajuan = id_peng
        $('#pengajuan_modal').modal('show');
         $.ajax({
                    type: "post",
                    url:  BASE_URL+"ApiEpunten/get_datang_edit",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        id_peng : pengajuan,
                    },
                    beforeSend:
                    function () {
                         // block_screen();
                    },
                    success: function (data) {
                        console.log(data);
                        $('#submit_btn').hide();
                        $('#nik').val(data[0].nik);
                        $('#no_kk').val(data[0].no_kk);
                        $('#nama_lgkp').val(data[0].nama_lgkp);
                        $('#tmpt_lhr').val(data[0].tmpt_lhr);
                        $('#tgl_lhr').val(data[0].tgl_lhr);
                        $('select[name="jenis_klmin"]').val(data[0].jenis_klmin).trigger("change");
                        $('select[name="agama"]').val(data[0].agama).trigger("change");
                        $('select[name="stat_kwn"]').val(data[0].stat_kwn).trigger("change");
                        $('select[name="gol_drh"]').val(data[0].gol_drh).trigger("change");
                        $('select[name="pendidikan"]').val(data[0].pendidikan).trigger("change");
                        $('select[name="pekerjaan"]').val(data[0].pekerjaan).trigger("change");
                        $('select[name="stat_hbkel"]').val(data[0].stat_hbkel).trigger("change");
                        $('select[name="alasan_pindah"]').val(data[0].alasan_pindah).trigger("change");
                        $('select[name="akta_lhr"]').val(data[0].akta_lhr).trigger("change");
                        $('#no_akta_lhr').val(data[0].no_akta_lhr);
                        $('#no_skpwni').val(data[0].no_skpwni);
                        $('#telepon').val(data[0].telepon);
                        $('#email').val(data[0].email);
                        $('#jum_anggota').val(data[0].jum_anggota);
                        $('#jangka_waktu').val(data[0].jangka_waktu);
                        $('#tgl_datang').val(data[0].tgl_datang);
                        $('#is_asal').show();
                        $('select[name="src_prov"]').empty();
                        $('select[name="src_prov"]').append('<option value="'+data[0].src_prov+'">'+data[0].src_prov+' - '+data[0].nama_src_prov+'</option>');
                        $('select[name="src_kab"]').empty();
                        $('select[name="src_kab"]').append('<option value="'+data[0].src_kab+'">'+data[0].src_kab+' - '+data[0].nama_src_kab+'</option>');
                        $('select[name="src_kec"]').empty();
                        $('select[name="src_kec"]').append('<option value="'+data[0].src_kec+'">'+data[0].src_kec+' - '+data[0].nama_src_kec+'</option>');
                        $('select[name="src_kel"]').empty();
                        $('select[name="src_kel"]').append('<option value="'+data[0].src_kel+'">'+data[0].src_kel+' - '+data[0].nama_src_kel+'</option>');
                        $('#src_alamat').val(data[0].src_alamat);
                        $('#src_rw').val(data[0].src_rw);
                        $('#src_rt').val(data[0].src_rt);
                        $('#is_tujuan').show();
                        $('#is_upload').show();
                        $('select[name="prop"]').empty();
                        $('select[name="prop"]').append('<option value="'+data[0].prop+'">'+data[0].prop+' - '+data[0].nama_prop+'</option>');
                        $('select[name="kab"]').empty();
                        $('select[name="kab"]').append('<option value="'+data[0].kab+'">'+data[0].kab+' - '+data[0].nama_kab+'</option>');
                        $('select[name="kec"]').empty();
                        $('select[name="kec"]').append('<option value="'+data[0].kec+'">'+data[0].kec+' - '+data[0].nama_kec+'</option>');
                        $('select[name="kel"]').empty();
                        $('select[name="kel"]').append('<option value="'+data[0].kel+'">'+data[0].kel+' - '+data[0].nama_kel+'</option>');
                        
                        $('#alamat').val(data[0].alamat);
                        $('#no_rw').val(data[0].no_rw);
                        $('#no_rt').val(data[0].no_rt);
                        
                       
                        if (data[0].img_thumb_1 == null){
                          $("#blockimgdisplay1").hide();
                        }else{
                          $("#blockimgdisplay1").show();
                          $("#imgdisplay1").attr("src",BASE_URL+data[0].img_thumb_1);
                        }
                        if (data[0].img_thumb_2 == null){
                          $("#blockimgdisplay2").hide();
                        }else{
                          $("#blockimgdisplay2").show();
                          $("#imgdisplay2").attr("src",BASE_URL+data[0].img_thumb_2);
                        }
                        if (data[0].img_thumb_3 == null){
                          $("#blockimgdisplay3").hide();
                        }else{
                          $("#blockimgdisplay3").show();
                          $("#imgdisplay3").attr("src",BASE_URL+data[0].img_thumb_3);
                        }
                        if (data[0].img_thumb_4 == null){
                          $("#blockimgdisplay4").hide();
                        }else{
                          $("#blockimgdisplay4").show();
                          $("#imgdisplay4").attr("src",BASE_URL+data[0].img_thumb_4);
                        }
                        if (data[0].img_thumb_5 == null){
                          $("#blockimgdisplay5").hide();
                        }else{
                          $("#blockimgdisplay5").show();
                          $("#imgdisplay5").attr("src",BASE_URL+data[0].img_thumb_5);
                        }
                        if (data[0].img_thumb_6 == null){
                          $("#blockimgdisplay6").hide();
                        }else{
                          $("#blockimgdisplay6").show();
                          $("#imgdisplay6").attr("src",BASE_URL+data[0].img_thumb_6);
                        }
                        if (data[0].img_thumb_7 == null){
                          $("#blockimgdisplay7").hide();
                        }else{
                          $("#blockimgdisplay7").show();
                          $("#imgdisplay7").attr("src",BASE_URL+data[0].img_thumb_7);
                        }
                        
                    },
                    error:
                    function (data) {
                        // unblock_screen();
                        // swal("Error!", "Ooops!, Please Try Again, Something Went Wrong", "error"); 

                    },
                    complete:
                    function (response) {
                        // unblock_screen();  
                        // $("#no_kk_baru").val("");
                        
                    }
                });
    }
    function hapus(id_peng,nama_lgkp,nik){
      var pengajuan = id_peng;
      var nama_lgkp = toTitleCase(nama_lgkp.toLowerCase());
      var nik = nik;
         swal({   
            title: "Apakah Anda Yakin ?",   
            text: "Menghapus Pengajuan "+nama_lgkp+" ?",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#f33155",   
            confirmButtonText: "Ya, Hapus Pengajuan",   
            cancelButtonText: "Tidak, Jangan Dihapus",   
            closeOnConfirm: false,   
            closeOnCancel: false 
            }, function(isConfirm){   
                if (isConfirm) {
                $.ajax({
                    type: "post",
                    url:  BASE_URL+"ApiEpunten/get_datang_delete",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        id_peng : pengajuan,
                        nik : nik,
                    },
                    beforeSend:
                    function () {
                         block_screen();
                    },
                    success: function (data) {
                        console.log(data);
                         if (data.message_type > 0){
                              swal("Berhasil !", "Pengajuan "+nama_lgkp+" Telah Dihapus !", "success");
                        }else{
                            swal("Warning!", data.message, "warning");  
                        }
                    },
                    error:
                    function (data) {
                        unblock_screen();
                        swal("Error!", "Ooops!, Please Try Again, Something Went Wrong", "error"); 

                    },
                    complete:
                    function (response) {
                        unblock_screen();
                        get_table();
                    }
                });
                     
                } else {     
                    swal("Cancelled", "Pengajuan "+nama_lgkp+" Tidak Jadi Dihapus :)", "error");   
                } 
            });
       
    }

   function print_data(id_peng){
         
        var pengajuan = id_peng
         $.ajax({
                    type: "post",
                    url:  BASE_URL+"ApiEpunten/get_datang_edit",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        id_peng : pengajuan,
                    },
                    beforeSend:
                    function () {
                         // block_screen();
                    },
                    success: function (data) {
                        console.log(data);
                        PrintImage(data[0].img_thumb_1,data[0].img_thumb_2,data[0].img_thumb_3,data[0].img_thumb_4,data[0].img_thumb_5,data[0].img_thumb_6,data[0].img_thumb_7);
                       
                    },
                    error:
                    function (data) {
                        // unblock_screen();
                        // swal("Error!", "Ooops!, Please Try Again, Something Went Wrong", "error"); 

                    },
                    complete:
                    function (response) {
                        // unblock_screen();  
                        // $("#no_kk_baru").val("");
                        
                    }
                });
       }


        function ImagetoPrint(img_thumb_1,img_thumb_2,img_thumb_3,img_thumb_4,img_thumb_5,img_thumb_6,img_thumb_7)
        {
          var data = "<html><head><scri"+"pt>function step1(){\n" +
                    "setTimeout('step2()', 10);}\n" +
                    "function step2(){window.print();window.close()}\n" +
                    "</scri" + "pt></head><body onload='step1()'>\n" ;
                    if(img_thumb_1 != null){
                     data +="<img src='" + BASE_URL+""+img_thumb_1 + "' />\n";
                    }
                    if(img_thumb_2 != null){
                      data += "<img src='" + BASE_URL+""+img_thumb_2 + "' />\n";  
                    }
                    if(img_thumb_3 != null){
                      data += "<img src='" + BASE_URL+""+img_thumb_3 + "' />\n";  
                    }
                    if(img_thumb_4 != null){
                      data += "<img src='" + BASE_URL+""+img_thumb_4 + "' />\n";  
                    }
                    if(img_thumb_5 != null){
                      data += "<img src='" + BASE_URL+""+img_thumb_5 + "' />\n";  
                    }
                    if(img_thumb_6 != null){
                      data += "<img src='" + BASE_URL+""+img_thumb_6 + "' />\n";  
                    }
                    if(img_thumb_7 != null){
                      data += "<img src='" + BASE_URL+""+img_thumb_7 + "' />\n";  
                    }
                     data += "</body></html>";
            return data;
        }

        function PrintImage(img_thumb_1,img_thumb_2,img_thumb_3,img_thumb_4,img_thumb_5,img_thumb_6,img_thumb_7)
        {
            var Pagelink = "about:blank";
            var pwa = window.open(Pagelink, "_new");
            pwa.document.open();
            pwa.document.write(ImagetoPrint(img_thumb_1,img_thumb_2,img_thumb_3,img_thumb_4,img_thumb_5,img_thumb_6,img_thumb_7));
            pwa.document.close();
        }

      function pesan(id_peng,nik){
        var pengajuan = id_peng;
        var nik_tolak = nik;
        $('#tolak_modal').modal('show');
        $('#id_pengajuan_tolak').val(pengajuan);
        $('#nik_tolak').val(nik_tolak);
        $('#isi_pesan').val("");
        $('#cntnum').html("Minimal Anda Harus Mengisi 10 Karakter Untuk Mengirim Pesan, Anda Baru Memasukan 0 Karakter !");
        $('#cntnum').css("color","red");
      }

      function proses(id_peng,nama_lgkp,nik){
        var pengajuan = id_peng;
        var nik = nik;
        var nama_lgkp = toTitleCase(nama_lgkp.toLowerCase());
        swal({   
            title: "Apakah Anda Yakin ?",   
            text: "Mempublish Pengajuan "+nama_lgkp+" ?",   
            type: "info",   
            showCancelButton: true,   
            confirmButtonColor: "#7ace4c",   
            confirmButtonText: "Ya, Publish Pengajuan",   
            cancelButtonText: "Tidak, Jangan Dipublish",   
            closeOnConfirm: false,   
            closeOnCancel: false 
            }, function(isConfirm){   
                if (isConfirm) {
                $.ajax({
                    type: "post",
                    url:  BASE_URL+"ApiEpunten/get_datang_acc",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        id_peng : pengajuan,
                        nik : nik,
                    },
                    beforeSend:
                    function () {
                         block_screen();
                    },
                    success: function (data) {
                        console.log(data);
                         if (data.message_type > 0){
                              swal("Berhasil !", "Pengajuan "+nama_lgkp+" Telah Dipublish !", "success");
                        }else{
                            swal("Warning!", data.message, "warning");  
                        }
                    },
                    error:
                    function (data) {
                        unblock_screen();
                        swal("Error!", "Ooops!, Please Try Again, Something Went Wrong", "error"); 

                    },
                    complete:
                    function (response) {
                        unblock_screen();
                        get_table();
                    }
                });
                     
                } else {     
                    swal("Cancelled", "Pengajuan "+nama_lgkp+" Tidak Jadi Dipublish :)", "error");   
                } 
            });
      }
      
      function beres(id_peng,nama_lgkp,nik){
        var pengajuan = id_peng;
        var nik = nik;
        var nama_lgkp = toTitleCase(nama_lgkp.toLowerCase());
        swal({   
            title: "Apakah Anda Yakin ?",   
            text: "Mempublish Pengajuan "+nama_lgkp+" ?",   
            type: "info",   
            showCancelButton: true,   
            confirmButtonColor: "#7ace4c",   
            confirmButtonText: "Ya, Publish Pengajuan",   
            cancelButtonText: "Tidak, Jangan Dipublish",   
            closeOnConfirm: false,   
            closeOnCancel: false 
            }, function(isConfirm){   
                if (isConfirm) {
                $.ajax({
                    type: "post",
                    url:  BASE_URL+"ApiEpunten/get_datang_beres",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        id_peng : pengajuan,
                        nik : nik,
                    },
                    beforeSend:
                    function () {
                         block_screen();
                    },
                    success: function (data) {
                        console.log(data);
                         if (data.message_type > 0){
                              swal("Berhasil !", "Pengajuan "+nama_lgkp+" Telah Dipublish !", "success");
                        }else{
                            swal("Warning!", data.message, "warning");  
                        }
                    },
                    error:
                    function (data) {
                        unblock_screen();
                        swal("Error!", "Ooops!, Please Try Again, Something Went Wrong", "error"); 

                    },
                    complete:
                    function (response) {
                        unblock_screen();
                        get_table();
                    }
                });
                     
                } else {     
                    swal("Cancelled", "Pengajuan "+nama_lgkp+" Tidak Jadi Dipublish :)", "error");   
                } 
            });
      }

      function cntText(){
        var text = $('#isi_pesan');
        var angka = text.val().length;
        var textangka = "Minimal Anda Harus Mengisi 10 Karakter Untuk Mengirim Pesan, Anda Baru Memasukan "+text.val().length+" Karakter !";
        var txtdata = "Anda Telah Memasukan "+text.val().length+" Karakter !";
        if(angka >= 10){
          $('#cntnum').html(txtdata);
          $('#cntnum').css("color","black");
        }else if(angka < 10){
          $('#cntnum').html(textangka);
          $('#cntnum').css("color","red");
        }
      }
      function toTitleCase(str) {
        return str.replace(/(?:^|\s)\w/g, function(match) {
            return match.toUpperCase();
        });
    }
    </script>