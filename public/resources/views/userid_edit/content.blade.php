<div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $stitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                        <ol class="breadcrumb">
                           <li class="text-info"><?php echo $stitle; ?></li>
                        </ol>
                    </div>
                </div>
                <!-- .row -->
               <div class="row">
                <div class="row">

                    <div class="col-md-12 col-xs-12">
                        <div class="white-box">
                            <ul class="nav nav-tabs tabs customtab">
                                <li class="active tab">
                                    <a href="#user_detail" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-user"></i></span> <span class="hidden-xs" style="color: blue !important">DETAIL USER DATA</span> </a> 
                                </li>
                                <li class="tab">
                                    <a href="#user_wilayah" data-toggle="tab"> <span class="visible-xs"><i class="mdi mdi-access-point"></i></span> <span class="hidden-xs" style="color: blue !important">WILAYAH USER</span> </a> 
                                </li>
                                <li class="tab">
                                    <a href="#user_akunsiak" data-toggle="tab"> <span class="visible-xs"><i class="mdi mdi-access-point"></i></span> <span class="hidden-xs" style="color: blue !important">AKUN USER</span> </a> 
                                </li>
                                <li class="tab">
                                    <a href="#user_akunset" data-toggle="tab"> <span class="visible-xs"><i class="mdi mdi-access-point"></i></span> <span class="hidden-xs" style="color: blue !important">AKUN SETTING</span> </a> 
                                </li>
                                <li class="tab">
                                    <a href="#user_akunatasan" data-toggle="tab"> <span class="visible-xs"><i class="mdi mdi-access-point"></i></span> <span class="hidden-xs" style="color: blue !important">ATASAN SETTING</span> </a> 
                                </li>
                               
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="user_detail">
                                        <form class="form-horizontal form-material">
                                        <div class="form-group">
                                            <label class="col-md-12" style="color: blue !important">USER ID</label>
                                            <div class="col-md-12">
                                                <input type="text" value="<?php if (!empty($theuser)){echo $theuser[0]->user_id; }?>" id="user_id" class="form-control form-control-line" readonly> </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12" style="color: blue !important">NAMA LENGKAP</label>
                                            <div class="col-md-12">
                                                <input type="text" value="<?php if (!empty($theuser)){echo $theuser[0]->nama_lgkp; }?>" id="nama_lgkp" class="form-control form-control-line" style="text-transform:uppercase !important"> </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12" style="color: blue !important">NAMA DEPAN</label>
                                            <div class="col-md-12">
                                                <input type="text" value="<?php if (!empty($theuser)){echo $theuser[0]->nama_dpn; }?>" id="nama_dpn" class="form-control form-control-line" style="text-transform:uppercase !important"> </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12" style="color: blue !important">NIK</label>
                                            <div class="col-md-12">
                                                <input type="text" value="<?php if (!empty($theuser)){echo $theuser[0]->nik; }?>" id="nik" class="form-control form-control-line" onkeypress="return isNumberKey(event)" maxlength="16"> </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12" style="color: blue !important">TEMPAT LAHIR</label>
                                            <div class="col-md-12">
                                                <input type="text" value="<?php if (!empty($theuser)){echo $theuser[0]->tmpt_lhr; }?>" id="tmpt_lhr" class="form-control form-control-line" style="text-transform:uppercase !important"> </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12" style="color: blue !important">TANGGAL LAHIR</label>
                                            <div class="col-md-12">
                                                <input type="text" class="form-control" id="tgl_lhr" value="<?php if (!empty($theuser)){echo $theuser[0]->tgl_lhr; }?>" placeholder="DD-MM-YYYY" readonly>
                                                </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12" style="color: blue !important">JENIS KELAMIN</label>
                                            <div class="col-md-12">
                                                <select class="form-control" name="jenis_klmin" id="jenis_klmin">
                                                </select>
                                                </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12" style="color: blue !important">GOLONGAN DARAH</label>
                                            <div class="col-md-12">
                                                <select class="form-control" name="gol_drh" id="gol_drh">
                                                </select> </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12" style="color: blue !important">NAMA KANTOR</label>
                                            <div class="col-md-12">
                                                <input type="text" value="<?php if (!empty($theuser)){echo $theuser[0]->nama_kantor; }?>" id="nama_kantor" class="form-control form-control-line" style="text-transform:uppercase !important"> </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12" style="color: blue !important">ALAMAT KANTOR</label>
                                            <div class="col-md-12">
                                                <input type="text" value="<?php if (!empty($theuser)){echo $theuser[0]->alamat_kantor; }?>" id="alamat_kantor" class="form-control form-control-line" style="text-transform:uppercase !important"> </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12" style="color: blue !important">NOMOR TELEPON</label>
                                            <div class="col-md-12">
                                                <input type="text" value="<?php if (!empty($theuser)){echo $theuser[0]->telp; }?>" id="telp" class="form-control form-control-line" onkeypress="return isNumberKey(event)" > </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12" style="color: blue !important">ALAMAT RUMAH</label>
                                            <div class="col-md-12">
                                                <input type="text" value="<?php if (!empty($theuser)){echo $theuser[0]->alamat_rumah; }?>" id="alamat_rumah" class="form-control form-control-line" style="text-transform:uppercase !important"> </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12 pull-right">
                                                <button type="button" class="btn btn-info waves-effect waves-light m-r-10 pull-right" id="btnEdit1" onclick="on_edit();">Edit <i class="mdi  mdi-tooltip-edit fa-fw"></i></button>
                                               <button type="button" class="btn btn-invert waves-effect waves-light m-r-10 pull-right" id="btn-back1" onclick="on_back();">Back <i class="mdi  mdi-backspace fa-fw"></i></button>
                                            </div>
                                        </div>
                                    </form>
                                    </div>
                                     <div class="tab-pane" id="user_wilayah">
                                        <form class="form-horizontal form-material">
                                       <div class="form-group">
                                            <label class="col-md-12" style="color: blue !important">LEVEL USER</label>
                                            <div class="col-md-12">
                                                <select class="form-control" name="kdgroup" id="kdgroup">
                                                </select> </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12" style="color: blue !important">GROUP USER</label>
                                            <div class="col-md-12">
                                                 <select class="form-control" name="kdlvl" id="kdlvl">
                                                </select> </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12" style="color: blue !important">PROVINSI</label>
                                            <div class="col-md-12">
                                                <select class="form-control" name="kdprop" id="kdprop">
                                                </select> </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12" style="color: blue !important">KABUPATEN</label>
                                            <div class="col-md-12">
                                                <select class="form-control" name="kdkab" id="kdkab">
                                                </select> </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12" style="color: blue !important">KECAMATAN</label>
                                            <div class="col-md-12">
                                                <select class="form-control" name="kdkec" id="kdkec">
                                                </select> </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12" style="color: blue !important">KELURAHAN</label>
                                            <div class="col-md-12">
                                               <select class="form-control" name="kdkel" id="kdkel">
                                                </select> </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12" style="color: blue !important">NO RW</label>
                                            <div class="col-md-12">
                                                <input type="text" value="<?php if (!empty($theuser)){echo $theuser[0]->no_rw; }?>" id="no_rw" class="form-control form-control-line" readonly> </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12" style="color: blue !important">NO RT</label>
                                            <div class="col-md-12">
                                                <input type="text" value="<?php if (!empty($theuser)){echo $theuser[0]->no_rt; }?>" id="no_rt" class="form-control form-control-line" readonly> </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12 pull-right">
                                                <button type="button" class="btn btn-info waves-effect waves-light m-r-10 pull-right" id="btnEdit2" onclick="on_edit();">Edit <i class="mdi  mdi-tooltip-edit fa-fw"></i></button>
                                               <button type="button" class="btn btn-invert waves-effect waves-light m-r-10 pull-right" id="btn-back2" onclick="on_back();">Back <i class="mdi  mdi-backspace fa-fw"></i></button>
                                            </div>
                                        </div>
                                    </form>
                                    </div>
                                     <div class="tab-pane" id="user_akunsiak">
                                        <form class="form-horizontal form-material">
                                        <div class="form-group">
                                            <label class="col-md-12" style="color: blue !important">AKUN SIAK</label>
                                            <div class="col-md-12">
                                                <input type="text" value="<?php if (!empty($theuser)){echo $theuser[0]->user_siak; }?>" id="user_siak" class="form-control form-control-line"> </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12" style="color: blue !important">AKUN BIOMORF CARDMANAGEMENT</label>
                                            <div class="col-md-12">
                                                <input type="text" value="<?php if (!empty($theuser)){echo $theuser[0]->user_bcard; }?>" id="user_bcard" class="form-control form-control-line"> </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12" style="color: blue !important">AKUN BIOMORF ENROLMENT</label>
                                            <div class="col-md-12">
                                                <input type="text" value="<?php if (!empty($theuser)){echo $theuser[0]->user_benrol; }?>" id="user_benrol" class="form-control form-control-line"> </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12 pull-right">
                                                <button type="button" class="btn btn-info waves-effect waves-light m-r-10 pull-right" id="btnEdit3" onclick="on_edit();">Edit <i class="mdi  mdi-tooltip-edit fa-fw"></i></button>
                                               <button type="button" class="btn btn-invert waves-effect waves-light m-r-10 pull-right" id="btn-back3" onclick="on_back();">Back <i class="mdi  mdi-backspace fa-fw"></i></button>
                                            </div>
                                        </div>
                                    </form>
                                    </div>
                                     <div class="tab-pane" id="user_akunset">
                                        <form class="form-horizontal form-material">
                                        <div class="form-group">
                                            <label class="col-md-12" style="color: blue !important">MONITORING</label>
                                            <div class="col-md-12">
                                                <select class="form-control" name="is_monitoring" id="is_monitoring">
                                                </select> </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12" style="color: blue !important">#GISA</label>
                                            <div class="col-md-12">
                                                <select class="form-control" name="is_gisa" id="is_gisa">
                                                </select> </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12" style="color: blue !important">ABSEN</label>
                                            <div class="col-md-12">
                                                <select class="form-control" name="is_absen" id="is_absen">
                                                </select> </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12" style="color: blue !important">ASN</label>
                                            <div class="col-md-12">
                                                <select class="form-control" name="is_asn" id="is_asn">
                                                </select> </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12" style="color: blue !important">CHECK ABSENSI</label>
                                            <div class="col-md-12">
                                                <select class="form-control" name="absensi_checking" id="absensi_checking">
                                                </select> </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12" style="color: blue !important">ACTIVE</label>
                                            <div class="col-md-12">
                                                <select class="form-control" name="is_active" id="is_active">
                                                </select> </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12" style="color: blue !important">CHECK IP</label>
                                            <div class="col-md-12">
                                                <select class="form-control" name="ipaddress_check" id="ipaddress_check">
                                                </select> </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12" style="color: blue !important">SHOW INFO PROFIL</label>
                                            <div class="col-md-12">
                                                <select class="form-control" name="is_show" id="is_show">
                                                </select> </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12 pull-right">
                                                <button type="button" class="btn btn-info waves-effect waves-light m-r-10 pull-right" id="btnEdit4" onclick="on_edit();">Edit <i class="mdi  mdi-tooltip-edit fa-fw"></i></button>
                                               <button type="button" class="btn btn-invert waves-effect waves-light m-r-10 pull-right" id="btn-back4" onclick="on_back();">Back <i class="mdi  mdi-backspace fa-fw"></i></button>
                                            </div>
                                        </div>
                                    </form>
                                    </div>
                                    <div class="tab-pane" id="user_akunatasan">
                                        <form class="form-horizontal form-material">
                                        <div class="form-group">
                                            <label class="col-md-12" style="color: blue !important">ATASAN SATU</label>
                                            <div class="col-md-12">
                                                <select class="form-control" name="is_atasan_satu" id="is_atasan_satu">
                                                </select> </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12" style="color: blue !important">ATASAN DUA</label>
                                            <div class="col-md-12">
                                                <select class="form-control" name="is_atasan_dua" id="is_atasan_dua">
                                                </select> </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12 pull-right">
                                                 <button type="button" class="btn btn-info waves-effect waves-light m-r-10 pull-right" id="btnEdit5" onclick="on_edit();">Edit <i class="mdi  mdi-tooltip-edit fa-fw"></i></button>
                                               <button type="button" class="btn btn-invert waves-effect waves-light m-r-10 pull-right" id="btn-back5" onclick="on_back();">Back <i class="mdi  mdi-backspace fa-fw"></i></button>
                                            </div>
                                        </div>
                                    </form>
                                    </div>
                                    
                            </div>
                        </div>
                    </div>
                </div>
                   
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- /.container-fluid -->
         @include('shared.footer_detail')