 <style type="text/css">
     textarea {
  resize: none;
  height: 155px !important;
}

 </style>
 <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $stitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                        <ol class="breadcrumb">
                            <li class="active"><?php echo $snik; ?></li>
                        </ol>
                    </div>
                </div>
                 <?php if (!empty($data_nik)){ 
                                foreach($data_nik as $row){
                            ?>
                 <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                   
                        <div class="panel panel-default">
                            <div class="panel-heading">Edit Kepemilikan Dokumen #GISA</div>
                            <div class="panel-wrapper collapse in">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th class="text-center">#</th>
                                            <th>Monitoring #GISA</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td align="center">1</td>
                                            <td>Nik</td>
                                            <td><?php echo $row->nik ;?></td>
                                        </tr>
                                        <tr>
                                            <td align="center">2</td>
                                            <td>Nomor KK</td>
                                            <td><?php echo $row->no_kk ;?></td>
                                        </tr>
                                        <tr>
                                            <td align="center">3</td>
                                            <td>Nama Lengkap</td>
                                            <td><?php echo $row->nama_lgkp ;?></td>
                                        </tr>
                                         <tr>
                                            <td align="center">4</td>
                                            <td>Tempat Lahir</td>
                                            <td><?php echo $row->tmpt_lhr ;?></td>
                                        </tr>
                                         <tr>
                                            <td align="center">5</td>
                                            <td>Tanggal Lahir</td>
                                            <td><?php echo $row->tgl_lhr ?> (<?php echo $row->umur ?> Tahun)</td>
                                        </tr>
                                         <tr>
                                            <td align="center">6</td>
                                            <td>Jenis Kelamin</td>
                                            <td><?php echo $row->jenis_klmin ?></td>
                                        </tr>
                                        <tr>
                                            <td align="center">7</td>
                                            <td>Status Perkawinan</td>
                                            <td><?php echo $row->status_kawin ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                       
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="white-box">
                            <div class="row">
                               
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_tgl" type="checkbox" checked disabled="true">
                                            <label for="cb_tgl"> Akta Lahir</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">   
                                    <div class="form-group">
                                        <select class="selectpicker m-b-20 m-r-10" name="akta_lhr" id="akta_lhr" data-style="btn-info btn-outline">
                                            <?php if($row->status_akta_lhr =='TIDAK ADA') { ?>
                                            <option value="TIDAK ADA" selected>TIDAK ADA</option>
                                            <option value="ADA">ADA</option>
                                            <?php }else{ ?>
                                            <option value="TIDAK ADA">TIDAK ADA</option>
                                            <option value="ADA" selected>ADA</option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_tgl" type="checkbox" checked disabled="true">
                                            <label for="cb_tgl"> No Akta Lahir</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                    <div class="form-group">
                                        <?php if($row->no_akta_lhr =='-') { ?>
                                        <input class="form-control" id="no_akta_lhr" type="text" >
                                        <?php }else{ ?>
                                        <input class="form-control" id="no_akta_lhr" type="text" value="<?php echo $row->no_akta_lhr ?>" >
                                        <?php } ?>
                                    </div>
                                </div>
                                </div>
                                
                                <?php if($row->status_akta_kwn !='-') { ?>
                                <div class="row">
                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_tgl" type="checkbox" checked disabled="true">
                                            <label for="cb_tgl"> Akta Perkawinan</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">   
                                    <div class="form-group">
                                        <select class="selectpicker m-b-20 m-r-10" name="akta_kwn" id="akta_kwn" data-style="btn-info btn-outline">
                                            <?php if($row->status_akta_kwn =='TIDAK TERCATAT') { ?>
                                            <option value="TIDAK TERCATAT" selected>TIDAK TERCATAT</option>
                                            <option value="TERCATAT">TERCATAT</option>
                                            <?php }else{ ?>
                                            <option value="TIDAK TERCATAT">TIDAK TERCATAT</option>
                                            <option value="TERCATAT" selected>TERCATAT</option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_tgl" type="checkbox" checked disabled="true">
                                            <label for="cb_tgl"> No Akta Kawin</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                    <div class="form-group">
                                        <?php if($row->no_akta_kwn =='-') { ?>
                                        <input class="form-control" id="no_akta_kwn" type="text" >
                                        <?php }else{ ?>
                                        <input class="form-control" id="no_akta_kwn" type="text" value="<?php echo $row->no_akta_kwn ?>" >
                                        <?php } ?>
                                    </div>
                                </div>
                                
                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_tgl" type="checkbox" checked disabled="true">
                                            <label for="cb_tgl"> Tanggal Perkawinan</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">   
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="tgl_kwn" placeholder="DD/MM/YYYY" data-inputmask="'alias': 'date'" />
                                    </div>
                                </div>
                                </div>
                                <?php }else{ ?>
                                <div class="row" style="display: none !important;">
                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">   
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_tgl" type="checkbox" checked disabled="true">
                                            <label for="cb_tgl"> Akta Perkawinan</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">   
                                    <div class="form-group">
                                        <select class="selectpicker m-b-20 m-r-10" name="akta_kwn" id="akta_kwn" data-style="btn-info btn-outline">
                                            <option value="-">-</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">   
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_tgl" type="checkbox" checked disabled="true">
                                            <label for="cb_tgl"> No Akta Kawin</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">   
                                    <div class="form-group">
                                        <?php if($row->no_akta_kwn =='-') { ?>
                                        <input class="form-control" id="no_akta_kwn" type="text" >
                                        <?php }else{ ?>
                                        <input class="form-control" id="no_akta_kwn" type="text" value="<?php echo $row->no_akta_kwn ?>" >
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">   
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_tgl" type="checkbox" checked disabled="true">
                                            <label for="cb_tgl"> Tanggal Perkawinan</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">      
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="tgl_kwn" placeholder="DD/MM/YYYY" data-inputmask="'alias': 'date'" />
                                    </div>
                                </div>
                               
                                </div>
                                <?php } ?>
                                <?php if($row->status_akta_crai !='-') { ?>
                                <div class="row">
                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">   
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_tgl" type="checkbox" checked disabled="true">
                                            <label for="cb_tgl"> Akta Perceraian</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">      
                                    <div class="form-group">
                                        <select class="selectpicker m-b-20 m-r-10" name="akta_crai" id="akta_crai" data-style="btn-info btn-outline">
                                            <?php if($row->status_akta_crai =='TIDAK TERCATAT') { ?>
                                            <option value="TIDAK TERCATAT" selected>TIDAK TERCATAT</option>
                                            <option value="TERCATAT">TERCATAT</option>
                                            <?php }else{ ?>
                                            <option value="TIDAK TERCATAT">TIDAK TERCATAT</option>
                                            <option value="TERCATAT" selected>TERCATAT</option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">   
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_tgl" type="checkbox" checked disabled="true">
                                            <label for="cb_tgl"> No Akta Cerai</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">   
                                    <div class="form-group">
                                        <?php if($row->no_akta_crai =='-') { ?>
                                        <input class="form-control" id="no_akta_crai" type="text" >
                                        <?php }else{ ?>
                                        <input class="form-control" id="no_akta_crai" type="text" value="<?php echo $row->no_akta_crai ?>" >
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">   
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_tgl" type="checkbox" checked disabled="true">
                                            <label for="cb_tgl"> Tanggal Perceraian</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">      
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="tgl_crai" placeholder="DD/MM/YYYY" data-inputmask="'alias': 'date'" />
                                    </div>
                                </div>
                                </div>
                                <?php }else{ ?>
                                <div class="row" style="display: none !important;">
                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">   
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_tgl" type="checkbox" checked disabled="true">
                                            <label for="cb_tgl"> Akta Perceraian</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">      
                                    <div class="form-group">
                                        <select class="selectpicker m-b-20 m-r-10" name="akta_crai" id="akta_crai" data-style="btn-info btn-outline">
                                            <option value="-">-</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">   
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_tgl" type="checkbox" checked disabled="true">
                                            <label for="cb_tgl"> No Akta Cerai</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">   
                                    <div class="form-group">
                                        <?php if($row->no_akta_crai =='-') { ?>
                                        <input class="form-control" id="no_akta_crai" type="text" >
                                        <?php }else{ ?>
                                        <input class="form-control" id="no_akta_crai" type="text" value="<?php echo $row->no_akta_crai ?>" >
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">   
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_tgl" type="checkbox" checked disabled="true">
                                            <label for="cb_tgl"> Tanggal Perceraian</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">      
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="tgl_crai" placeholder="DD/MM/YYYY" data-inputmask="'alias': 'date'" />
                                    </div>
                                </div>
                                </div>
                                <?php } ?>
                                
                            <div class="row">
                                
                                  <a href="{{ url('/') }}/gisa/cek_detail?no_kk=<?php echo $row->no_kk ;?>" onclick="on_menu();"><span type="button" class="btn btn-info waves-effect waves-light m-r-10 pull-right">Back <i class="mdi  mdi-backspace fa-fw"></i></span></a>
                            </div>
                        </div>
                    </div>
                </div>
                        <?php 
                            }
                            }
                        ?>
                 </div>
       @include('shared.footer_detail')