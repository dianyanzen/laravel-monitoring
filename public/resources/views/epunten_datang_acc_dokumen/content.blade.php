<div id="page-wrapper">
            <div class="container-fluid">
               <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $mtitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                
                        <ol class="breadcrumb">
                            <li class="active"><?php echo $mtitle; ?></li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>

                <!-- .row -->
               <div id="pengajuan_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog modal-dialog-scrollable">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title" id="modal-title">Detail Pengajuan</h4> </div>
                                        <div class="modal-body">
                                            <div class="container-fluid">
                                            <form id="data_pengajuan">
                                                <h2 class="text-center">BIODATA</h2>
                                                <input type="hidden" id="jenis_layanan" name="jenis_layanan" value="1">
                                                <input type="hidden" id="status" name="status" value="1"> 
                                                <hr>
                                                <div class="form-group">
                                                    <label for="nik" class="control-label"><span style="color: red!important">* </span>NIK:</label>
                                                    <input type="text" class="form-control" id="nik" name="nik" maxlength="16" disabled> </div>
                                                <div class="form-group">
                                                    <label for="no_kk" class="control-label"><span style="color: red!important">* </span>NO KK:</label>
                                                    <input type="text" class="form-control" id="no_kk" name="no_kk" maxlength="16" disabled> </div>
                                                <div class="form-group">
                                                    <label for="nama_lgkp" class="control-label"><span style="color: red!important">* </span>Nama Lengkap:</label>
                                                    <input type="text" class="form-control" id="nama_lgkp" name="nama_lgkp" style="text-transform:uppercase" disabled> </div>
                                                <div class="form-group">
                                                    <label for="tmpt_lhr" class="control-label"><span style="color: red!important">* </span>Tempat Lahir:</label>
                                                    <input type="text" class="form-control" id="tmpt_lhr" name="tmpt_lhr" style="text-transform:uppercase" disabled> </div>
                                                <div class="form-group">
                                                    <label for="tgl_lhr" class="control-label"><span style="color: red!important">* </span>Tanggal Lahir:</label>
                                                    <input type="text" placeholder="yyyy-mm-dd" data-mask="9999-99-99" class="form-control" id="tgl_lhr" name="tgl_lhr"disabled> </div>
                                                <div class="form-group">
                                                    <label for="jenis_klmin" class="control-label"><span style="color: red!important">* </span>Jenis Kelamin:</label>
                                                    <select class="form-control" name="jenis_klmin" id="jenis_klmin" value="" disabled>
                                                  </select> </div>
                                                <div class="form-group">
                                                    <label for="akta_lhr" class="control-label"><span style="color: red!important">* </span>Akta Lahir:</label>
                                                    <select class="form-control" name="akta_lhr" id="akta_lhr" value="" disabled>
                                                  </select> </div>
                                                <div class="form-group">
                                                    <label for="no_akta_lhr" class="control-label"><span style="color: red!important">* </span>No Akta Lahir:</label>
                                                    <input type="text" class="form-control" id="no_akta_lhr" name="no_akta_lhr" style="text-transform:uppercase" disabled> </div>
                                                <div class="form-group">
                                                    <label for="no_skpwni" class="control-label"><span style="color: red!important">* </span>No Skpwni:</label>
                                                    <input type="text" class="form-control" id="no_skpwni" name="no_skpwni" style="text-transform:uppercase" disabled> </div>

                                                <div class="form-group">
                                                    <label for="stat_hbkel" class="control-label"><span style="color: red!important">* </span>Hubungan Keluarga:</label>
                                                    <select class="form-control" name="stat_hbkel" id="stat_hbkel" value="" disabled>
                                                  </select> </div>
                                                <div class="form-group">
                                                    <label for="agama" class="control-label"><span style="color: red!important">* </span>Agama:</label>
                                                    <select class="form-control" name="agama" id="agama" value="" disabled>
                                                  </select> </div>
                                                <div class="form-group">
                                                    <label for="stat_kwn" class="control-label"><span style="color: red!important">* </span>Status Kawin:</label>
                                                    <select class="form-control" name="stat_kwn" id="stat_kwn" value="" disabled>
                                                  </select> </div>
                                                <div class="form-group">
                                                    <label for="gol_drh" class="control-label"><span style="color: red!important">* </span>Golongan Darah:</label>
                                                    <select class="form-control" name="gol_drh" id="gol_drh" value="" disabled>
                                                  </select> </div>
                                                <div class="form-group">
                                                    <label for="pendidikan" class="control-label"><span style="color: red!important">* </span>Pendidikan:</label>
                                                    <select class="form-control" name="pendidikan" id="pendidikan" value="" disabled>
                                                  </select> </div>
                                                <div class="form-group">
                                                    <label for="pekerjaan" class="control-label"><span style="color: red!important">* </span>Pekerjaan:</label>
                                                    <select class="form-control" name="pekerjaan" id="pekerjaan" value="" disabled>
                                                  </select> </div>
                                                <div class="form-group">
                                                    <label for="alasan_pindah" class="control-label"><span style="color: red!important">* </span>Alasan Pindah:</label>
                                                    <select class="form-control" name="alasan_pindah" id="alasan_pindah" value="" disabled>
                                                  </select> </div>

                                                <div class="form-group">
                                                    <label for="jum_anggota" class="control-label">Jumlah Anggota Yang Dibawa:</label>
                                                    <input type="text" class="form-control" placeholder="0" value="0" name="jum_anggota" id="jum_anggota" disabled> </div>

                                                <div class="form-group">
                                                    <label for="jangka_waktu" class="control-label">Jangka Waktu:</label>
                                                    <input type="text" class="form-control" placeholder="0" value="0" name="jangka_waktu" id="jangka_waktu" disabled> </div>
                                                     <div class="form-group">
                                                    <label for="tgl_datang" class="control-label"><span style="color: red!important">* </span>Tanggal Kedatangan:</label>
                                                    <input type="text" placeholder="yyyy-mm-dd" data-mask="9999-99-99" class="form-control" id="tgl_datang" name="tgl_datang" disabled> </div>


                                                <div class="form-group">
                                                    <label for="telepon" class="control-label">Telepon:</label>
                                                    <input type="text" class="form-control" placeholder="+62" value="+62" name="telepon" id="telepon" disabled> </div>
                                                <div class="form-group">
                                                    <label for="email" class="control-label">E-mail:</label>
                                                    <input type="text" class="form-control" id="email" name="email" disabled> </div>

                                                <h2 class="text-center">DATA DAERAH ASAL</h2>
                                                <hr>
                                                <div class="form-group">
                                                    <label for="src_prov" class="control-label"><span style="color: red!important">* </span>Provinsi:</label>
                                                    <select class="form-control" name="src_prov" id="src_prov" value="" disabled>
                                                  </select> </div>
                                                <div class="form-group">
                                                    <label for="src_kab" class="control-label"><span style="color: red!important">* </span>Kab/Kota:</label>
                                                    <select class="form-control" name="src_kab" id="src_kab" value="" disabled>
                                                  </select> </div>
                                                <div class="form-group">
                                                    <label for="src_kec" class="control-label"><span style="color: red!important">* </span>Kecamatan:</label>
                                                    <select class="form-control" name="src_kec" id="src_kec" value="" disabled>
                                                  </select> </div>
                                                <div class="form-group">
                                                    <label for="src_kel" class="control-label"><span style="color: red!important">* </span>Kelurahan:</label>
                                                    <select class="form-control" name="src_kel" id="src_kel" value="" disabled>
                                                  </select> </div>
                                                <div class="form-group">
                                                    <label for="src_alamat" class="control-label">Alamat:</label>
                                                    <input type="text" class="form-control" name="src_alamat" id="src_alamat" style="text-transform:uppercase" disabled> </div>
                                                <div class="form-group">
                                                    <label for="src_no_rt" class="control-label">No RT:</label>
                                                    <input type="text" class="form-control" name="src_rt" id="src_rt" maxlength="3" disabled> </div>
                                                <div class="form-group">
                                                    <label for="src_no_rw" class="control-label">No RW:</label>
                                                    <input type="text" class="form-control" name="src_rw" id="src_rw" maxlength="3" disabled> </div>
                                                <h2 class="text-center">DATA KOTA BANDUNG</h2>
                                                <hr>
                                                <div class="form-group">
                                                    <label for="prop" class="control-label"><span style="color: red!important">* </span>Provinsi:</label>
                                                    <select class="form-control" name="prop" id="prop" value="" disabled>
                                                        <option value="32" selected>32 - JAWA BARAT</option>
                                                  </select> </div>
                                                <div class="form-group">
                                                    <label for="kab" class="control-label"><span style="color: red!important">* </span>Kab/Kota:</label>
                                                    <select class="form-control" name="kab" id="kab" value="" disabled>
                                                        <option value="73" selected>73 - JAWA BARAT</option>
                                                  </select> </div>
                                                <div class="form-group">
                                                    <label for="kec" class="control-label"><span style="color: red!important">* </span>Kecamatan:</label>
                                                    <select class="form-control" name="kec" id="kec" value="" disabled>
                                                  </select> </div>
                                                <div class="form-group">
                                                    <label for="kel" class="control-label"><span style="color: red!important">* </span>Kelurahan:</label>
                                                    <select class="form-control" name="kel" id="kel" value="" disabled>
                                                  </select> </div>
                                                <div class="form-group">
                                                    <label for="alamat" class="control-label">Alamat:</label>
                                                    <input type="text" class="form-control" name="alamat" id="alamat" style="text-transform:uppercase" disabled> </div>
                                                <div class="form-group">
                                                    <label for="no_rt" class="control-label">No RT:</label>
                                                    <input type="text" class="form-control" name="no_rt" id="no_rt" maxlength="3" disabled> </div>
                                                <div class="form-group">
                                                    <label for="no_rw" class="control-label">No RW:</label>
                                                    <input type="text" class="form-control" name="no_rw" id="no_rw" maxlength="3" disabled> </div>
                                                
                                                <h2 class="text-center">BERKAS PERSYARATAN</h2>
                                                <hr>
                                                    <div class="form-group col-sm-6 ol-md-6 col-xs-12" id="blockimgdisplay1" style="display: none;">
                                                       <div class="m-t-20 row"><img id="imgdisplay1" src="http://10.32.73.7:8080/monitoring/assets/plugins/images/female-placeholder.jpg" alt="user" class="col-md-12 col-xs-12" /></div>
                                                    </div>
                                                    <div class="form-group col-sm-6 ol-md-6 col-xs-12" id="blockimgdisplay2" style="display: none;">
                                                       <div class="m-t-20 row"><img id="imgdisplay2" src="http://10.32.73.7:8080/monitoring/assets/plugins/images/female-placeholder.jpg" alt="user" class="col-md-12 col-xs-12" /> </div>
                                                    </div>
                                                    <div class="form-group col-sm-6 ol-md-6 col-xs-12" id="blockimgdisplay3" style="display: none;">
                                                       <div class="m-t-20 row"><img id="imgdisplay3" src="http://10.32.73.7:8080/monitoring/assets/plugins/images/female-placeholder.jpg" alt="user" class="col-md-12 col-xs-12" /> </div>
                                                    </div>
                                                    <div class="form-group col-sm-6 ol-md-6 col-xs-12" id="blockimgdisplay4" style="display: none;">
                                                       <div class="m-t-20 row"><img id="imgdisplay4" src="http://10.32.73.7:8080/monitoring/assets/plugins/images/female-placeholder.jpg" alt="user" class="col-md-12 col-xs-12" /> </div>
                                                    </div>
                                                    <div class="form-group col-sm-6 ol-md-6 col-xs-12" id="blockimgdisplay5" style="display: none;">
                                                       <div class="m-t-20 row"><img id="imgdisplay5" src="http://10.32.73.7:8080/monitoring/assets/plugins/images/female-placeholder.jpg" alt="user" class="col-md-12 col-xs-12" /> </div>
                                                    </div>
                                                    <div class="form-group col-sm-6 ol-md-6 col-xs-12" id="blockimgdisplay6" style="display: none;">
                                                       <div class="m-t-20 row"><img id="imgdisplay6" src="http://10.32.73.7:8080/monitoring/assets/plugins/images/female-placeholder.jpg" alt="user" class="col-md-12 col-xs-12" /> </div>
                                                    </div>
                                                    <div class="form-group col-sm-12 ol-md-12 col-xs-12" id="blockimgdisplay7" style="display: none;">
                                                       <div class="m-t-20 row"><img id="imgdisplay7" src="http://10.32.73.7:8080/monitoring/assets/plugins/images/female-placeholder.jpg" alt="user" class="col-md-12 col-xs-12" /> </div>
                                                    </div>

                                            </form>
                                        </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                            <button type="button" id="submit_btn" class="btn btn-info waves-effect waves-light">Save</button>
                                            <button type="button" id="submit_edit" class="btn btn-info waves-effect waves-light" style="display: none;">Edit</button>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                            <div id="tolak_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog modal-dialog-scrollable">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title" id="modal-title">Kirim Pemberitahuan</h4> </div>
                                        <div class="modal-body">
                                            <div class="container-fluid">
                                            <form id="data_tolak">
                                                <input type="hidden" id="jenis_layanan_tolak" name="jenis_layanan_tolak" value="1">
                                                <input type="hidden" id="nik_tolak" name="nik_tolak" >
                                                <input type="hidden" id="status_tolak" name="status_tolak" value="1"> 
                                                <input type="hidden" id="id_pengajuan_tolak" name="id_pengajuan_tolak"> 
                                                <div class="form-group">
                                                    <label for="nik" class="control-label"><span style="color: red!important">* </span>Isi Pesan:</label>
                                                    <textarea  class="form-control" id="isi_pesan" name="isi_pesan" minlength="10" rows="5" style="resize: none;" onkeypress="cntText()"></textarea> 
                                                    <h6 class="control-label"><span id="cntnum" style="color: red">Minimal Anda Harus Mengisi 10 Karakter Untuk Mengirim Pesan, Anda Baru Memasukan 0 Karakter !</span></h6>
                                                </div>
                                                

                                            </form>
                                        </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                            <button type="button" id="submit_btnntf" class="btn btn-info waves-effect waves-light">Kirim</button>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                            <div id="info_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog modal-dialog-scrollable">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title" id="modal-title">Info Penggunaan Aplikasi</h4> </div>
                                        <div class="modal-body">
                                            <div class="container-fluid">
                                                <a class="btn btn-info btn-circle"  style="margin-top: 5px"><i class="fa fa-search"></i></a> 
                                                 <span style="margin-top: 10px!important vertical-align:middle!important">: Tombol Untuk Melihat Detail Pengajuan.</span>
                                            <br> <a class="btn btn-danger btn-circle" style="margin-top: 5px"><i class="fa fa-trash-o"></i></a>
                                                 <span style="margin-top: 10px!important">: Tombol Untuk Menghapus Pengajuan.</span>
                                            <br> <a class="btn btn-info btn-circle" style="margin-top: 5px"><i class="fa  fa-print"></i></a>
                                                 <span style="margin-top: 10px!important">: Tombol Untuk Mencetak Berkas Persyaratan.</span>
                                            <br> <a class="btn btn-warning btn-circle" style="margin-top: 5px"><i class="fa fa-envelope"></i></a>
                                                 <span style="margin-top: 10px!important">: Tombol Untuk Memberikan Pesan Kepada Masyarakat.</span>
                                            <br> <a class="btn btn-info btn-circle" style="margin-top: 5px"><i class="fa fa-rocket"></i></a>
                                                 <span style="margin-top: 10px!important">: Tombol Untuk Mengajukan TTE KeBSRE.</span>
                                            <br> <a class="btn btn-success btn-circle" style="margin-top: 5px"><i class="fa fa-check"></i></a>
                                                 <span style="margin-top: 10px!important">: Tombol Untuk Mempublish Pengajuan.</span>
                                        </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                <!-- /.row -->
                <!-- /row -->
                <div class="row">
                <div class="col-md-12">
                        <div class="white-box">
                            <h3 class="box-title"><?php echo $mtitle; ?></h3>
                            <div class="form-group">
                            <button class="fcbtn btn btn-info btn-outline btn-1f" onclick="get_table();">Refresh</button>
                            <button class="fcbtn btn btn-info btn-outline btn-1f" onclick="get_info();">Info</button>
                            <div class="btn-group m-r-10 pull-right">
                            <button aria-expanded="false" data-toggle="dropdown" class="fcbtn btn btn-info btn-outline dropdown-toggle waves-effect waves-light" type="button">Navigasi <span class="caret"></span></button>
                                <ul role="menu" class="dropdown-menu animated flipInX">
                                    <li><a href="{{ url('/') }}/Epunten/datang/baru">Baru</a></li>
                                    <li class="divider"></li>
                                    <li><a href="{{ url('/') }}/Epunten/datang/batal">Batal</a></li>
                                    <li><a href="{{ url('/') }}/Epunten/datang/melengkapi">Melengkapi</a></li>
                                    <li class="divider"></li>
                                    <li><a href="{{ url('/') }}/Epunten/datang/acc_dokumen">Acc Dokumen</a></li>
                                    <li><a href="{{ url('/') }}/Epunten/datang/arsip">Arsip</a></li>
                                </ul>
                            </div>
                            </div>
                            <div class="scrollable">
                                <div class="table-responsive">
                                    <table id="pengajuan-list" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                   <thead>
                                    
                                        <tr>
                                           <th width="5%" style="text-align: center;"></th>
                                            <th width="5%" style="text-align: center;">#</th>
                                            <th width="40%" style="text-align: center;">Nik</th>
                                            <th width="40%" style="text-align: center;">No KK</th>
                                            <th width="40%" style="text-align: center;">Status KK</th>
                                            <th width="40%" style="text-align: center;">No Skpwni</th>
                                            <th width="20%" style="text-align: center;">Nama Lengkap</th>
                                            <th width="30%" style="text-align: center;">Telepon</th>
                                            <th width="30%" style="text-align: center;">Email</th>
                                            <th width="30%" style="text-align: center;">Tanggal Pengajuan</th>
                                            <th width="30%" style="text-align: center;">Aksi</th>

                                        </tr>
                                       
                                    </thead>
                                    <tbody id="show_data">
                                       
                                        
                                    </tbody>
                                    
                                </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
                <!-- .row -->
               

            </div>
       @include('shared.footer_detail')