<!DOCTYPE html>
<html lang="en">
@include('shared.head')
<body class="fix-header">
    <div id="wrapper">
            @include('shared.nav_top')
            @include('shared.nav_side')
            @include('absensi.content')
            @include('absensi.modal')
    </div>
     
    @include('shared.footer')
    
    @include('absensi.javascript')
</body>
</html>