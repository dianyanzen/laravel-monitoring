<button class="btn btn-info btn-lg" id="do_report" data-toggle="modal" data-target="#report-modal" style="display: none;"></button>
<button class="btn btn-info btn-lg" id="do_excuse" data-toggle="modal" data-target="#excuse-modal" style="display: none;"></button>
<div class="modal fade" id="excuse-modal" tabindex="-1" role="dialog" aria-labelledby="ExcuseClaim" aria-hidden="true" >
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" aria-hidden="true" onclick="do_close_excuse()">&times;</button>
                      <h4 class="modal-title">Excuse Claim</h4>
                    </div>
                    <div class="modal-body">
                     <div class="row">
                            <div class="col-md-12">
                              <form class="form-horizontal" role="form">
                                <fieldset>
                                  <!-- Text input-->
                                   <div class="form-group">
                                    <label class="col-sm-2 control-label" for="textinput" style="text-align: right;">Tanggal</label>
                                    <div class="col-sm-10">
                                             <div class="input-group date">
                                             <input type="text" class="form-control pull-right input-modal" id="tanggal_absen" placeholder="dd/mm/yyyy" onkeypress="return isClock(event)"><span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                            </div>
                                
                                    </div>
                                  </div>


                                  <div class="form-group">
                                    <label class="col-sm-2 control-label " for="textinput" style="text-align: right;">Keterangan</label>
                                    <div class="col-sm-10">
                                      <select class="form-control" name="excuse_keterangan" id="excuse_keterangan" value="">
                                        <option value='0'>-- Pilih Jenis Excuse --</option>
                                   
                                
                                      </select>
                                    </div>
                                  </div>

                                  <!-- Text input-->
                                 

                               

                                  <!-- Text input-->
                                  <div class="form-group">
                                    <label class="col-sm-2 control-label" for="textinput" style="text-align: right;">In</label>
                                    <div class="col-sm-4">
                                      <div class="input-group clockpicker" data-placement="left" data-align="top" data-autoclose="true">
                                            <input type="text" class="form-control input-modal" maxlength="5"  name="excuse_in" id="excuse_in"   onkeypress="return isClock(event)" value="08:00">
                                            <span class="input-group-addon">
                                                <span class="fa fa-clock-o"></span>
                                            </span>
                                        </div>
                                    </div>

                                    <label class="col-sm-2 control-label" for="textinput" style="text-align: right;">Out</label>
                                    <div class="col-sm-4">
                                         <div class="input-group clockpicker" data-placement="left" data-align="top" data-autoclose="true">
                                            <input type="text" class="form-control input-modal" maxlength="5" name="excuse_out" id="excuse_out" onkeypress="return isClock(event)" value="17:00">
                                            <span class="input-group-addon">
                                                <span class="fa fa-clock-o"></span>
                                            </span>
                                        </div>
                                    </div>
                                  </div>

                                     <div class="col-sm-12" id="bantuan_text">
                                    
                                    
                                  </div>
                                  
                                </fieldset>
                              </form>
                            </div><!-- /.col-lg-12 -->
                        </div><!-- /.row -->
                        </div>

                    <div class="modal-footer" style="margin-top: 0px !important;">
                         
                      <button type="button" id="closeexcuse" class="btn btn-default" data-dismiss="modal" onclick="onClearExcuse()">Tutup</button>
                      <button type="button" id="go_sendexcuse" class="btn btn-info" onclick="do_excuse()">Kirim</button>
                    </div>
                  </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
              </div><!-- /.modal -->


           <div class="modal fade" id="report-modal" tabindex="-1" role="dialog" aria-labelledby="DailyReport" aria-hidden="true" >
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" aria-hidden="true" onclick="do_close()">&times;</button>
                      <h4 class="modal-title">Daily Report</h4>
                    </div>
                    <div class="modal-body">
                     <div class="row">
                            <div class="col-md-12">
                              <form class="form-horizontal" role="form">
                                <fieldset>
                                  <!-- Text input-->
                                   <div class="form-group">
                                    <label class="col-sm-2 control-label" for="textinput" style="text-align: right;">Tanggal</label>
                                    <div class="col-sm-10">
                                             <div class="input-group date">
                                             <input type="text" class="form-control pull-right input-modal" id="tanggal_report" placeholder="dd/mm/yyyy" onkeypress="return isClock(event)"><span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                            </div>
                                
                                    </div>
                                  </div>


                                  <div class="form-group">
                                    <label class="col-sm-2 control-label " for="textinput" style="text-align: right;">Aktivitas</label>
                                    <div class="col-sm-10">
                                      <select class="form-control" name="Aktivitas" id="Aktivitas" value="">
                                       <!-- <option value='0'>0. LAINNYA</option> -->
                                      </select>
                                    </div>
                                  </div>

                                  <!-- Text input-->
                                 <div class="form-group">
                                    <label class="col-sm-2 control-label" for="textinput" style="text-align: right;">Jumlah</label>
                                    <div class="col-sm-10">
                                              <input type="text" placeholder="Jumlah Aktivitas" maxlength="5" name="daily_jumlah" id="daily_jumlah"  class="form-control input-modal" onkeypress="return isNumber(event)">
                                
                                    </div>
                                  </div>

                               

                                  <!-- Text input-->
                                  <div class="form-group">
                                    <label class="col-sm-2 control-label" for="textinput" style="text-align: right;">Dari</label>
                                    <div class="col-sm-4">
                                      <div class="input-group clockpicker" data-placement="left" data-align="top" data-autoclose="true">
                                            <input type="text" class="form-control input-modal" maxlength="5"  name="daily_dari" id="daily_dari"   onkeypress="return isClock(event)" value="08:00">
                                            <span class="input-group-addon">
                                                <span class="fa fa-clock-o"></span>
                                            </span>
                                        </div>
                                    </div>

                                    <label class="col-sm-2 control-label" for="textinput" style="text-align: right;">Out</label>
                                    <div class="col-sm-4">
                                         <div class="input-group clockpicker" data-placement="left" data-align="top" data-autoclose="true">
                                            <input type="text" class="form-control input-modal" maxlength="5" name="daily_sampai" id="daily_sampai" onkeypress="return isClock(event)" value="17:00">
                                            <span class="input-group-addon">
                                                <span class="fa fa-clock-o"></span>
                                            </span>
                                        </div>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-sm-2 control-label " for="textinput" style="text-align: right;">Keterangan</label>
                                    <div class="col-sm-10">
                                     <textarea type="text" name="daily_keterangan" id="daily_keterangan" placeholder="Isilah Dengan Singkat, Jelas Dan Padat (Max 300 Karakter)" maxlength="300" class="form-control" style="resize: none;"></textarea>
                                    </div>
                                  </div>



                                  <!-- Text input-->
                                 
                                  
                                     <div class="col-sm-12" id="bantuan_text">
            
                                  </div>
                                  
                                </fieldset>
                              </form>
                            </div><!-- /.col-lg-12 -->
                        </div><!-- /.row -->
                        </div>

                    <div class="modal-footer" style="margin-top: 0px !important;">
                      <button type="button" id="closemodal" class="btn btn-default" data-dismiss="modal" onclick="onClearModal()">Tutup</button>
                      <button type="button" id="go_send" class="btn btn-info" onclick="do_daily()">Kirim</button>
                    </div>
                  </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
              </div><!-- /.modal -->