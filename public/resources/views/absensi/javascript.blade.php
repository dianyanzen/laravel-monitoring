    <script src="{{ url('/') }}/assets/plugins/bower_components/owl.carousel/owl.carousel.min.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/owl.carousel/owl.custom.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/flot/jquery.flot.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/Minimal-Gauge-chart/js/cmGauge.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/absensi/bootstrap-clockpicker.min.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/absensi/bootstrap-datepicker.js"></script>
   

   <script type="text/javascript">
   	$(document).ready(function() {
           
        $.ajax({
                    type: "post",
                    url: BASE_URL+"Activity/get_data_first",
                    dataType: "json",
                    data: {
                      "_token": "{{ csrf_token() }}",
                    	user_id : "{{ $user_id }}"
                    },
                    beforeSend:
                    function () {
                        
                    },
                    success: function (data) {
                        console.log(data);
                      $('#clock_in').html(data.clock_in);
                       $('#clock_out').html(data.clock_out);
                       $('#user_monev').html(data.user_id);
                       $('#user_siak').html(data.user_siak);
                       if(data.siak_ip == "-"){
                        $('#status_online').html('<b class="text-danger">Is Offline</b>');
                       }else{
                        $('#status_online').html('<b class="text-info">Is Online</b>');
                       }
                       $('#siak_ip').html(data.siak_ip);
                       $('#siak_last').html(data.siak_last);
                       if(data.monev_ip == "-"){
                        $('#status_online_monev').html('<b class="text-danger">Is Offline</b>');
                       }else{
                        $('#status_online_monev').html('<b class="text-info">Is Online</b>');
                       }
                       $('#monev_ip').html(data.monev_ip);
                       $('#monev_last').html(data.monev_last);
                      
                       
                        
                    },
                    error:
                    function (data) {
               

                    },
                    complete:
                    function (response) {
                    }

                });
        $.ajax({
                    type: "post",
                    url: BASE_URL+"Report/option_excuse",
                    dataType: "json",
                    data: {
                      "_token": "{{ csrf_token() }}",
                      
                    },
                    beforeSend:
                    function () {
                        
                    },
                    success: function (data) {
                        console.log(data);
                        var i = 0;
                       $('select[name="excuse_keterangan"]').empty();
                        $('select[name="excuse_keterangan"]').append('<option value="0">-- Pilih Jenis Excuse --</option>');
                        $.each(data, function(key, value) {
                          i = i+1;
                            $('select[name="excuse_keterangan"]').append('<option value="'+ value.excuse_id +'">'+i+' '+ value.excuse_description +'</option>');
                        });
                       
                        
                    },
                    error:
                    function (data) {
               

                    },
                    complete:
                    function (response) {
                    }

                }); 
        $.ajax({
                    type: "post",
                    url: BASE_URL+"Report/option_daily",
                    dataType: "json",
                    data: {
                      "_token": "{{ csrf_token() }}",
                      user_id :  "{{ $user_id }}"
                    },
                    beforeSend:
                    function () {
                        
                    },
                    success: function (data) {
                        console.log(data);
                        var i = 1;
                       $('select[name="Aktivitas"]').empty();
                        $.each(data, function(key, value) {
                            $('select[name="Aktivitas"]').append('<option value="'+ value.activity_id +'">'+i+'. '+ value.activity +' ('+ value.minute +' MENIT)'+'</option>');
                             i = i+1;
                             if (key == 0){
                               $('select[name="Aktivitas"]').val(value.activity_id).trigger("change");
                             }
                        });
                         // $('select[name="Aktivitas"]').append('<option value="0">'+i+'. LAINNYA</option>');
                    },
                    error:
                    function (data) {
               

                    },
                    complete:
                    function (response) {
                    }

                });

    });
   
    function do_clockin(data) {
            block_screen();
            var url = "{{ url('/') }}/";
            url += "Report/Doci/";
            url += data;
            var win = window.location.replace(url);
            win.focus();
        }
    function do_clockout (data) {
            block_screen();
            var url = "{{ url('/') }}/";
            url += "Report/Doco/";
            url += data;
            var win = window.location.replace(url);
            win.focus();
        }
    function do_rekap() {
            block_screen();
            var url = "{{ url('/') }}/";
            url += "Report/Rekap";
            var win = window.location.replace(url);
            win.focus();
        }
    function do_rekap_daily() {
            block_screen();
            var url = "{{ url('/') }}/";
            url += "Report/RekapDaily";
            var win = window.location.replace(url);
            win.focus();
        }
      function block_screen(){
        
        $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff'
            },
            baseZ: 2000
        }); 
    }
    function unblock_screen(){
       $.unblockUI();
    }
   </script>
   <style type="text/css">
     .clockpicker-popover {
        z-index: 999999 !important;
    }
   </style>
   <script type="text/javascript">
  $('.clockpicker').clockpicker();
    $('#tanggal_absen').datepicker({
      autoclose: true,
      todayHighlight: true,
      format : 'dd/mm/yyyy',
      weekStart : 1,
      todayBtn: true,
      language : 'id'
    });
    $('#tanggal_report').datepicker({
      autoclose: true,
      todayHighlight: true,
      format : 'dd/mm/yyyy',
      weekStart : 1,
      todayBtn: true,
      language : 'id'
    });
    $('#excuse_keterangan').on('change', function (e) {
   var tanggal_absen = $('#tanggal_absen').val();
   var tanggal_report = $('#tanggal_report').val();
    var optionExcuse = $("option:selected", this);
    var valueExcuse = this.value;
    if (valueExcuse == 2 && tanggal_absen.length > 0){
      $("#excuse_in").attr("disabled",false);
      $("#excuse_out").attr("disabled",true);
      $.ajax({
                type: "post",
                url: BASE_URL+"Report/excuse",
                data: {
                  "_token": "{{ csrf_token() }}",
                  get_out :  1,
                  tanggal_absen :  $("#tanggal_absen").val(),
                  user_id :  "{{ $user_id }}"
                },
                beforeSend:
                function () {

                },
                success: function (data) {
                    if (data.message_type == 1){
            $("#excuse_in").val(data.clock_in);
            $("#excuse_out").val(data.clock_out);
            $("#go_sendexcuse").attr("disabled",false);
                    }else if (data.message_type == 2){
                      $("#excuse_in").val(data.clock_in);
            $("#excuse_out").val(data.clock_out);
            $("#go_sendexcuse").attr("disabled",true);
            $("#excuse_keterangan").val("0");
            toastr.warning(data.message);
                    }
                    
                },
                error:
                function (data) {
                    toastr.error("Please Try Again, Something Went Wrong");
      
                },
                complete:
                function (response) {
       
                }
            });
      
    }else if (valueExcuse == 3 && tanggal_absen.length > 0){
      $("#excuse_out").attr("disabled",false);
      $("#excuse_in").attr("disabled",true);
      $.ajax({
                type: "post",
                url: BASE_URL+"Report/excuse",
                data: {
                  "_token": "{{ csrf_token() }}",
                  get_in :  1,
                  tanggal_absen :  $("#tanggal_absen").val(),
                  user_id :  "{{ $user_id }}"
                },
                beforeSend:
                function () {

                },
                success: function (data) {
                    if (data.message_type == 1){
            $("#excuse_in").val(data.clock_in);
            $("#excuse_out").val(data.clock_out);
            $("#go_sendexcuse").attr("disabled",false);
                    }else if (data.message_type == 2){
                      $("#excuse_in").val(data.clock_in);
            $("#excuse_out").val(data.clock_out);
            $("#go_sendexcuse").attr("disabled",true);
            $("#excuse_keterangan").val("0");
            toastr.warning(data.message);
                    }
                    
                },
                error:
                function (data) {
           

                },
                complete:
                function (response) {

                }
            });
    }else if((valueExcuse == 1 || valueExcuse == 4) && tanggal_absen.length > 0 ){
      $("#excuse_in").attr("disabled",true);
      $("#excuse_out").attr("disabled",true);
      $.ajax({
                type: "post",
                url: BASE_URL+"Report/excuse",
                data: {
                  "_token": "{{ csrf_token() }}",
                  get_absen :  1,
                  tanggal_absen :  $("#tanggal_absen").val(),
                  user_id :  "{{ $user_id }}"
                },
                beforeSend:
                function () {

                },
                success: function (data) {
                    if (data.message_type == 2){
            $("#excuse_in").val(data.clock_in);
            $("#excuse_out").val(data.clock_out);
            $("#go_sendexcuse").attr("disabled",false);
                    }else if (data.message_type == 1){
                      $("#excuse_in").val(data.clock_in);
            $("#excuse_out").val(data.clock_out);
            $("#go_sendexcuse").attr("disabled",true);
            $("#excuse_keterangan").val("0");
            toastr.warning(data.message);
                    }
                    
                },
                error:
                function (data) {
           

                },
                complete:
                function (response) {

                }
            });
    }else if((valueExcuse == 5 || valueExcuse == 6) && tanggal_absen.length > 0 ){
      $("#excuse_in").attr("disabled",true);
      $("#excuse_out").attr("disabled",true);
      $.ajax({
                type: "post",
                url: BASE_URL+"Report/excuse",
                data: {
                  "_token": "{{ csrf_token() }}",
                  get_excuse :  1,
                  tanggal_absen :  $("#tanggal_absen").val(),
                  user_id :  "{{ $user_id }}"
                },
                beforeSend:
                function () {

                },
                success: function (data) {
                    if (data.message_type == 2){
            $("#excuse_in").val(data.clock_in);
            $("#excuse_out").val(data.clock_out);
            $("#go_sendexcuse").attr("disabled",false);
                    }else if (data.message_type == 1){
                      $("#excuse_in").val(data.clock_in);
            $("#excuse_out").val(data.clock_out);
            $("#go_sendexcuse").attr("disabled",true);
            $("#excuse_keterangan").val("0");
            toastr.warning(data.message);
                    }
                    
                },
                error:
                function (data) {
           

                },
                complete:
                function (response) {

                }
            });
    }else{
      $("#excuse_in").val('08:00');
        $("#excuse_in").attr("disabled",true);
        $("#excuse_out").val('17:00');
        $("#excuse_out").attr("disabled",true);
        $("#go_sendexcuse").attr("disabled",true);
    }
    
  });
     $("#tanggal_absen").change(function(){
      var tanggalpick =  $("#tanggal_absen").val();
      $.ajax({
                type: "post",
                url: BASE_URL+"Report/excuse",
                data: {
                  "_token": "{{ csrf_token() }}",
                  get_date :  1,
                  tanggal_absen :  $("#tanggal_absen").val()
                },
                beforeSend:
                function () {

                },
                success: function (data) {
                    if (data.message_type == 2){
            // $("#go_sendexcuse").attr("disabled",false);
            $("#go_sendexcuse").attr("disabled",true);
            $("#excuse_keterangan").attr("disabled",false);
            $("#excuse_keterangan").val("0");
            }else if (data.message_type == 1){
            toastr.warning(data.message);
            $("#go_sendexcuse").attr("disabled",true);
            $("#excuse_keterangan").attr("disabled",true);
            $("#excuse_keterangan").val("0");
                    }
                    
                },
                error:
                function (data) {
           

                },
                complete:
                function (response) {

                }
            });
       $("#excuse_keterangan").attr("disabled",false);
       $("#go_sendexcuse").attr("disabled",false);
       $("#excuse_keterangan").val("0");
       $("#excuse_in").val('08:00');
       $("#excuse_in").attr("disabled",true);
       $("#excuse_out").val('17:00');
       $("#excuse_out").attr("disabled",true);
  });
   $("#tanggal_report").change(function(){
      var tanggalpick =  $("#tanggal_report").val();
      $.ajax({
                type: "post",
                url: BASE_URL+"Report/daily",
                data: {
                  "_token": "{{ csrf_token() }}",
                  get_date :  1,
                  tanggal_report :  $("#tanggal_report").val()
                },
                beforeSend:
                function () {

                },
                success: function (data) {
            if (data.message_type == 2){
            // $("#go_sendexcuse").attr("disabled",false);
            // toastr.warning(data.message);
            $("#go_send").attr("disabled",false);
            $("#Aktivitas").attr("disabled",false);
            $("#Aktivitas").val("1");
            $("#daily_jumlah").attr("disabled",false);
            $("#daily_jumlah").val("");
            $("#daily_dari").attr("disabled",false);
            $("#daily_dari").val("08:00");
            $("#daily_sampai").attr("disabled",false);
            $("#daily_sampai").val("17:00");
            $("#daily_keterangan").attr("disabled",false);
            $("#daily_keterangan").val("");
            $("#bantuan_text").append().empty();
            doing_bantuan = false;
                    }else if (data.message_type == 1){
            toastr.warning(data.message);
            $("#go_send").attr("disabled",true);
            $("#Aktivitas").attr("disabled",true);
            $("#Aktivitas").val("1");
            $("#daily_jumlah").attr("disabled",true);
            $("#daily_jumlah").val("");
            $("#daily_dari").attr("disabled",true);
            $("#daily_dari").val("08:00");
            $("#daily_sampai").attr("disabled",true);
            $("#daily_sampai").val("17:00");
            $("#daily_keterangan").attr("disabled",true);
            $("#daily_keterangan").val("");
            $("#bantuan_text").append().empty();
            doing_bantuan = false;
                    }
                    
                },
                error:
                function (data) {
           

                },
                complete:
                function (response) {
        $("#daily_jumlah").val('');
                }
            });
      

  });
    var doing_bantuan = false;
    function onClearModal() {
      $("#tanggal_report").val('');
      $("#go_send").attr("disabled",true);
    $("#Aktivitas").attr("disabled",true);
    $("#Aktivitas").val("1");
    $("#daily_jumlah").attr("disabled",true);
    $("#daily_jumlah").val("");
    $("#daily_dari").attr("disabled",true);
    $("#daily_dari").val("08:00");
    $("#daily_sampai").attr("disabled",true);
    $("#daily_sampai").val("17:00");
    $("#daily_keterangan").attr("disabled",true);
    $("#daily_keterangan").val("");
    $("#bantuan_text").append().empty();
    doing_bantuan = false;
    }
    function do_close(){
      $('#closemodal').click();
    }
    function do_bantuan(){
      var html = "";
      if(!doing_bantuan){
        html += "<p><span style='color: red'>*</span> Dalam Satu Hari Anda Dapat Membuat Daily Report Lebih Dari Satu";
      html += "<br><span style='color: red'>*</span> Dengan Catatan Aktivitas Yang Di Input Tidak Boleh Sama Dengan Aktivitas Sebelumnya";
      html += "<br><span style='color: red'>*</span> Apabila Anda Mengisi Lainnya Maka Sebaiknya Sebutkan Nama Aktivitasnya";
      html += "<br><span style='color: red'>*</span> Apabila Anda Mengerjakan Lebih Dari Satu Aktivitas Yang Sama Dengan Selisih Waktu Yang Berbeda";
      html += "<br><span style='color: red'>*</span> Maka Anda Mengisinya <b>Dari</b> Di Aktivitas Awal Dan <b>Sampai</b> Di Aktivitas Akhir</p>";
      $("#bantuan_text").append().empty();
          $("#bantuan_text").append(html);
          doing_bantuan = true;
      }else{
        $("#bantuan_text").append().empty();
        doing_bantuan = false;
      }
    }
   
    function onClearExcuse(){
      $("#excuse_in").val('08:00');
        $("#excuse_in").attr("disabled",true);
        $("#excuse_out").val('17:00');
        $("#excuse_out").attr("disabled",true);
        $("#excuse_keterangan").val("0");
        $("#excuse_keterangan").attr("disabled",true);
        $("#go_sendexcuse").attr("disabled",true);
        $("#tanggal_absen").val('');
    }
    function do_close_excuse(){
      $('#closeexcuse').click();
    }
    function validationexcuse() {
    var excuse_keterangan = $("#excuse_keterangan option:selected").val();
    if (excuse_keterangan == 0) {
          toastr.warning('Keterangan tidak boleh kosong');
          return false;
        }
        return true;
    }
    function validationdaily() {
            var daily_jumlah = $("#daily_jumlah");
            if (daily_jumlah.val().length == 0) {
              daily_jumlah.select();
                toastr.warning('Jumlah aktivitas tidak boleh kosong');
                return false;
            }
            var daily_keterangan = $("#daily_keterangan");
            if (daily_keterangan.val().length == 0) {
                toastr.warning('Keterangan tidak boleh kosong');
                daily_keterangan.select();
                return false;
            } 
            if (daily_keterangan.val().length > 0 && daily_keterangan.val().length < 10) {
                toastr.warning('Keterangan terlalu pendek');
                daily_keterangan.select();
                return false;
            } 
           

            return true;
        }
        function do_report_js(){
          $("#tanggal_report").val('');
          $("#go_send").attr("disabled",true);
      $("#Aktivitas").attr("disabled",true);
      $("#Aktivitas").val("1");
      $("#daily_jumlah").attr("disabled",true);
      $("#daily_jumlah").val("");
      $("#daily_dari").attr("disabled",true);
      $("#daily_dari").val("08:00");
      $("#daily_sampai").attr("disabled",true);
      $("#daily_sampai").val("17:00");
      $("#daily_keterangan").attr("disabled",true);
      $("#daily_keterangan").val("");
      $("#bantuan_text").append().empty();
      doing_bantuan = false;
          $('#do_report').click();
        }
        function do_excusejs(){
        //   $("#excuse_in").val('08:00');
        // $("#excuse_in").attr("disabled",true);
        // $("#excuse_out").val('17:00');
        // $("#excuse_out").attr("disabled",true);
        // $("#excuse_keterangan").val("0");
        // $("#excuse_keterangan").attr("disabled",true);
        // $("#go_sendexcuse").attr("disabled",true);
        // $("#tanggal_absen").val('');
        //   $('#do_excuse').click();
        toastr.warning('Excuse Sudah Tidak Berlaku');
        }
         
      function do_daily(){
         if (!validationdaily()) return;
           $.ajax({
                type: "post",
                url: BASE_URL+"Report/daily",
                data: {
                  "_token": "{{ csrf_token() }}",
                  daily_save :  1,
                  aktivitas :  $("#Aktivitas").val(),
                  tanggal_report :  $("#tanggal_report").val(),
                  jumlah :  $("#daily_jumlah").val(),
                  daily_dari :  $("#daily_dari").val(),
                  daily_sampai :  $("#daily_sampai").val(),
                  daily_keterangan :  $("#daily_keterangan").val(),
                  user_id :  "{{ $user_id }}"
                },
                beforeSend:
                function () {
                  document.getElementById("go_send").disabled = true;
                  $('#go_send').html("<i class='fa fa-circle-o-notch fa-spin fa-fw'></i> Wait");
                },
                success: function (data) {

                    if (data.message_type == 1){
                  toastr.success(data.message);
                  $("#daily_jumlah").val('');
                  $("#daily_keterangan").val('');
                    }else if (data.message_type == 2){
                      toastr.error(data.message);
                    }
                    
                },
                error:
                function (data) {
                    toastr.error("Please Try Again, Something Went Wrong");
                    $("#daily_jumlah").val('');
          $("#daily_keterangan").val('');
          onClearModal();

                },
                complete:
                function (response) {
                  document.getElementById("go_send").disabled = false;
                  $('#go_send').html("Kirim");
                  onClearModal();
                }
            });

        }
            function isNumber(evt) {
                evt = (evt) ? evt : window.event;
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                    return false;
                }
                return true;
            }
            function isClock(evt) {
                evt = (evt) ? evt : window.event;
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode != 31) {
                    return false;
                }
                return true;
            }
        
            function isNotSpace(evt) {
                evt = (evt) ? evt : window.event;
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode == 32) {
                    return false;
                }
                return true;
            }
  
      /*  $( function() {
          $('.datepicker').datepicker();
        } );*/
    function do_excuse(){
      if (!validationexcuse()) return;
         $.ajax({
                type: "post",
                url: BASE_URL+"Report/excuse",
                data: {
                  "_token": "{{ csrf_token() }}",
                  excuse_save :  1,
                  excuse_tanggal :  $("#tanggal_absen").val(),
                  excuse_keterangan :  $("#excuse_keterangan").val(),
                  excuse_ci :  $("#excuse_in").val(),
                  excuse_co :  $("#excuse_out").val(),
                  user_id :  "{{ $user_id }}",
                  nama_lgkp :  "{{ $user_nama_lgkp }}"
                },
                beforeSend:
                function () {
                  document.getElementById("go_sendexcuse").disabled = true;
                  $('#go_sendexcuse').html("<i class='fa fa-circle-o-notch fa-spin fa-fw'></i> Wait");
                },
                success: function (data) {

                    if (data.message_type == 1){
            toastr.success(data.message);
                    }else if (data.message_type == 2){
                      toastr.error(data.message);
                    }
                    
                },
                error:
                function (data) {
                    toastr.error("Please Try Again, Something Went Wrong");
                    onClearExcuse();

                },
                complete:
                function (response) {
                  $('#go_sendexcuse').html("Kirim");
                  $('#closeexcuse').click();
                }
            });

        }

  </script>