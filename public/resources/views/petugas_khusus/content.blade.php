 <style type="text/css">
     textarea {
  resize: none;
  height: 155px !important;
}
 </style>
 <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $stitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                        <ol class="breadcrumb">
                            <li class="active"><?php echo $mtitle; ?></li>
                        </ol>
                    </div>
                </div>
                 <div class="row">
                <!-- <form name ="get_form" action="<?php echo $my_url; ?>" method="post"> -->
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="white-box">
                            <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_ph" type="checkbox" checked disabled="true">
                                            <label for="cb_ph"> No Petugas Khusus</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">         
                                    <div class="form-group">
                                        <input class="form-control" id="txt_ph" type="text"></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_nik" type="checkbox" checked disabled="true">
                                            <label for="cb_nik"> Nik</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">         
                                    <div class="input-group">
                                        <input class="form-control" id="txt_nik" type="text" maxlength="16" onkeypress="return isNumberKey(event)" ><span class="input-group-btn"><button type="button" onclick="get_nik_paste();" class="btn waves-effect waves-light btn-info"><i class="fa fa-search"></i></button></span></div>
                                </div>
                            </div>
                            <div class="row">
                               
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_nama" type="checkbox" checked disabled="true" >
                                            <label for="cb_nama"> Nama</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">         
                                    <div class="form-group">
                                        <input class="form-control" id="txt_nama" type="text" readonly></div>
                                </div>
                            </div>
                            <div class="row">
                               
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_tmpt_lhr" type="checkbox" checked disabled="true" >
                                            <label for="cb_tmpt_lhr"> Tempat Lahir</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">         
                                    <div class="form-group">
                                        <input class="form-control" id="txt_tmpt_lhr" type="text" readonly></div>
                                </div>
                            </div>
                            <div class="row">
                               
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_tgl_lhr" type="checkbox" checked disabled="true" >
                                            <label for="cb_tgl_lhr"> Tanggal Lahir</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">         
                                    <div class="form-group">
                                        <input class="form-control" id="txt_tgl_lhr" type="text" readonly>
                                        <input class="form-control" id="txt_umur" type="text" style="display: none"></div>
                                </div>
                            </div>
                            
                            <div class="row">
                               
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_jenis_klmin" type="checkbox" checked disabled="true" >
                                            <label for="cb_jenis_klmin"> Jenis Kelamin</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">         
                                    <div class="form-group">
                                        <input class="form-control" id="txt_jenis_klmin" type="text" readonly></div>
                                </div>
                            </div>


                            <div class="row">
                               
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_jenis_pkrjn" type="checkbox" checked disabled="true" >
                                            <label for="cb_jenis_pkrjn"> Jenis Pekerjaan</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">         
                                    <div class="form-group">
                                        <input class="form-control" id="txt_jenis_pkrjn" type="text" readonly></div>
                                </div>
                            </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_prop" type="checkbox" checked disabled="true">
                                            <label for="cb_prop"> Provinsi</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">         
                                    <div class="form-group">
                                        <select class="form-control select2" name="no_prop" id="no_prop" disabled="true">
                                         <option  value="0">-- Pilih Provinsi --</option>
                                        </select>
                                    </div>
                                </div>
                                </div>
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_kab" type="checkbox" checked disabled="true">
                                            <label for="cb_kab"> Kabupaten</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">         
                                    <div class="form-group">
                                        <select class="form-control select2" name="no_kab" id="no_kab" disabled="true">
                                         <option  value="0">-- Pilih Kabupaten --</option>
                                        </select>
                                    </div>
                                </div>
                                </div>
                                <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_kec" type="checkbox" checked disabled="true">
                                            <label for="cb_kec"> Kecamatan</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">         
                                    <div class="form-group">
                                        <select class="form-control select2" name="no_kec" id="no_kec" disabled="true">
                                         <option  value="0">-- Pilih Kecamatan --</option>
                                        </select>
                                    </div>
                                </div>
                                </div>
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_kel" type="checkbox" checked disabled="true">
                                            <label for="cb_kel"> Kelurahan</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">         
                                    <div class="form-group">
                                        <select class="form-control select2" name="no_kel" id="no_kel" disabled="true">
                                         <option  value="0">-- Pilih Kelurahan --</option>
                                        </select>
                                    </div>
                                </div>
                                </div>
                                <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_alamat" type="checkbox" checked disabled="true">
                                            <label for="cb_alamat"> Alamat</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">         
                                    <div class="form-group">
                                        <input class="form-control" id="txt_alamat" type="text" readonly></div>
                                </div>
                                </div>
                                <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_rt" type="checkbox" checked disabled="true">
                                            <label for="cb_rt"> RT</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">         
                                    <div class="form-group">
                                        <input class="form-control" id="txt_rt" type="text" readonly></div>
                                </div>
                                </div>
                                <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_rw" type="checkbox" checked disabled="true">
                                            <label for="cb_rw"> RW</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">         
                                    <div class="form-group">
                                        <input class="form-control" id="txt_rw" type="text" readonly></div>
                                </div>
                                </div>
                            </div>
                            </div>
                            <div class="row">
                                <button type="submit" id="btn_dosave" class="btn btn-info waves-effect waves-light m-r-10 pull-right" id="btn-filter" onclick="on_save();" >Save <i class="mdi  mdi-content-save fa-fw"></i></button>
                                  <button type="button" class="btn btn-invert waves-effect waves-light m-r-10 pull-right" id="btn-reset" onclick="on_clear();">Clear <i class="mdi  mdi-delete fa-fw"></i></button>
                            </div>
                        
                        </div>
                    </div>
                    <!-- </form> -->
                
                </div>
              
                 </div>
       @include('shared.footer_detail')