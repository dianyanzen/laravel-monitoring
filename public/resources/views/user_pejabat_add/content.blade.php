 <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $stitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                        <ol class="breadcrumb">
                           <li class="text-info"><?php echo $stitle; ?></li>
                        </ol>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                        <div class="white-box">
                            <div class="row">
                                <div class="col-xs-6">
                                    <h2 class="font-medium m-t-0">Tambah Pejabat <small><span id="group_name"></span></small></h2>
                                </div>
                            </div>
                            <div class="row" style="margin-top:  5px">
                                <div class="col-lg-4">         
                                    <div class="form-group">
                                        <h3 class="box-title m-b-0"><b> Nip</b></h3>
                                        <input class="form-control" type="text" id="nip" name="nip" onkeypress="return isNumberKey(event)" maxlength="18"/>
                                    </div>
                                 </div>
                                 <div class="col-lg-8">         
                                    <div class="form-group">
                                        <h3 class="box-title m-b-0"><b> Nama</b></h3>
                                        <input class="form-control" type="text" id="nama" name="nama" style="text-transform:uppercase !important"/>
                                    </div>
                                 </div>
                                
                                <div class="col-lg-12">         
                                    <div class="form-group">
                                        <h3 class="box-title m-b-0"><b> Jabatan</b></h3>
                                        <input class="form-control" type="text" id="jabatan" name="jabatan" style="text-transform:uppercase !important"/>
                                    </div>
                                </div>
                                <div class="col-lg-12">         
                                    <div class="form-group">
                                        <h3 class="box-title m-b-0"><b> Struktural</b></h3>
                                        <select class="form-control select2" name="struktural" id="struktural">
                                        <option  value="0">Tidak</option>
                                        <option  value="1">Ya</option></select>
                                    </div>
                                </div>
                        </div>
                        <div class="row">

                                  <button type="button" class="btn btn-invert waves-effect waves-light m-r-10 pull-right" id="btn-back" onclick="on_back();">Back <i class="mdi  mdi-backspace fa-fw"></i></button>
                                  <button type="button" class="btn btn-info waves-effect waves-light m-r-10 pull-right" id="btn-addlevel" onclick="on_add_pejabat();">Save <i class="mdi  mdi-plus fa-fw"></i></button>
                                  
                            </div>
                        </div>
                    </div>
                </div>
                

            </div>
       @include('shared.footer_detail')