<!DOCTYPE html>
<html lang="en">
@include('shared.head')
<body class="fix-header">
    <div id="wrapper">
            @include('shared.nav_top')
            @include('shared/nav_side')
            @include('pengajuan_hilang_detail/content')
            @include('pengajuan_hilang_detail/modal')
    </div>
     
    @include('shared/footer')
    
    @include('pengajuan_hilang_detail/javascript')
</body>
</html>