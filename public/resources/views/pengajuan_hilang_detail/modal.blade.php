<button class="btn btn-info btn-lg" id="do_no_kehilangan" data-toggle="modal" data-target="#no_kehilangan-modal" style="display: none;"></button>

           <div class="modal fade" id="no_kehilangan-modal" tabindex="-1" role="dialog" aria-labelledby="no_kehilangan" aria-hidden="true" >
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" aria-hidden="true" onclick="do_close()">&times;</button>
                      <h4 class="modal-title">Tambah No Kehilangan</h4>
                    </div>
                    <div class="modal-body">
                     <div class="row">
                            <div class="col-md-12">
                              <form class="form-horizontal" role="form">
                                <fieldset>

                                  <div class="form-group">
                                    <label class="col-sm-3 control-label " for="textinput" style="text-align: right;">Nik</label>
                                    <div class="col-sm-9">
                                               <input class="form-control" id="mdl_nik" placeholder="" type="text" value="" readonly>
                                
                                    </div>
                                  </div>

                                  <div class="form-group">
                                    <label class="col-sm-3 control-label " for="textinput" style="text-align: right;">No Kehilangan</label>
                                    <div class="col-sm-9">
                                      <input type="text" placeholder="No Kehilangan"  name="mdl_no_kehilangan" id="mdl_no_kehilangan"  class="form-control input-modal" style="text-transform:uppercase">
                                    </div>
                                  </div>



                                  <!-- Text input-->
                                 
                                  
                                     <div class="col-sm-12" id="bantuan_text">
            
                                  </div>
                                  
                                </fieldset>
                              </form>
                            </div><!-- /.col-lg-12 -->
                        </div><!-- /.row -->
                        </div>

                    <div class="modal-footer" style="margin-top: 0px !important;">
                      <button type="button" id="closemodal" class="btn btn-default" data-dismiss="modal" onclick="onClearModal()">Tutup</button>
                      <button type="button" id="go_send" class="btn btn-info" onclick="do_save()">Kirim</button>
                    </div>
                  </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
              </div><!-- /.modal -->