     <script>

        $(document).ready(function() {
           get_table(0);

        });
    function get_table(kdgroup) {
        console.log(kdgroup);
         $('#pengajuan-list').DataTable({
                destroy: true,
                 "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                 "columnDefs": [
                    {"className": "dt-left", "targets": "_all"}
                  ],
                "pageLength" : 500,
                "ajax": {
                 "url": BASE_URL+"ListHilang",
                "type": "post",
                "data": {
                "_token": "{{ csrf_token() }}",
                "tanggal":  $("#tanggal").val(),
                },
                complete: function(data) {
                    
                }

                }
            });
    }
    
    jQuery(document).ready(function() {
        $(".select2").select2();
         $('#tanggal').val("<?php echo date('d-m-Y');?> - <?php echo date('d-m-Y');?>");
        });
     function block_screen(){
        
        $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff'
            },
            baseZ: 2000
        }); 
    }
    function unblock_screen(){
       $.unblockUI();
    }
     function do_kehilangan(data){
        $("#mdl_nik").val(data);
        $("#mdl_no_kehilangan").val("");
        $('#do_no_kehilangan').click();
    }
    function do_close() {
        $('#btn-reset').click();
        $('#closemodal').click();
        
    }
    function onClearModal() {
        $("#mdl_no_kehilangan").val("");
        $('#btn-reset').click();
    }
    function validation() {
            var mdl_nik = $("#mdl_nik");
            if (mdl_nik.val().length == 0) {
              mdl_nik.select();
                toastr.warning('Nik tidak boleh kosong');
                return false;
            }
            
            var mdl_no_kehilangan = $("#mdl_no_kehilangan");
            if (mdl_no_kehilangan.val().length == 0) {
                toastr.warning('No Kehilangan tidak boleh kosong');
                mdl_no_kehilangan.select();
                return false;
            } 
            if (mdl_no_kehilangan.val().length > 0 && mdl_no_kehilangan.val().length < 6) {
                toastr.warning('No Kehilangan terlalu pendek');
                mdl_no_kehilangan.select();
                return false;
            } 
           

            return true;
    }
     function do_save(){
         if (!validation()) return;
           $.ajax({
                type: "post",
                url: BASE_URL+"Pengajuan/update_hilang",
                data: {
                  "_token": "{{ csrf_token() }}",
                  nik :  $("#mdl_nik").val(),
                  no_kehilangan :  $("#mdl_no_kehilangan").val(),
                },
                beforeSend:
                function () {
                  document.getElementById("go_send").disabled = true;
                  $('#go_send').html("<i class='fa fa-circle-o-notch fa-spin fa-fw'></i> Wait");
                },
                success: function (data) {

                    if (data.message_type == 1){
                    toastr.success(data.message);
                    }else if (data.message_type == 2){
                      toastr.error(data.message);

                    }
                    
                },
                error:
                function (data) {
                    toastr.error("Please Try Again, Something Went Wrong");
                    do_close();
                    get_table(0);

                },
                complete:
                function (response) {
                  document.getElementById("go_send").disabled = false;
                  $('#go_send').html("Kirim");
                  do_close();
                  get_table(0);

                }
            });

        }
        function on_clear() {
         $('#pengajuan-list').DataTable({
                destroy: true,
                 "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                 "columnDefs": [
                    {"className": "dt-left", "targets": "_all"}
                  ],
                "pageLength" : 500,
                "ajax": {
                 "url": BASE_URL+"ListHilang",
                "type": "post",
                "data": {
                "_token": "{{ csrf_token() }}",
                "tanggal":  $("#tanggal").val(),
                },
                complete: function(data) {
                   
                }

                }
            });
    }
    </script>