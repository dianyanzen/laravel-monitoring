    <script src="{{ url('/') }}/assets/plugins/bower_components/owl.carousel/owl.carousel.min.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/owl.carousel/owl.custom.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/flot/jquery.flot.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/Minimal-Gauge-chart/js/cmGauge.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/Minimal-Gauge-chart/js/cmGauge.js"></script>
    <!-- Footable -->
    <script src="{{ url('/') }}/assets/plugins/bower_components/footable/js/footable.all.min.js"></script>
    <script src="{{ url('/') }}/assets/js/footable-init.js"></script>
    <script src="{{ url('/') }}/assets/js/chat.js"></script>

   <script>
        $(document).ready(function () {
          get_kontak();
          $("#isi_pesan").select();
          $('#search_kontak').change(function() {
             get_kontak_byname();
          });
          $("#isi_pesan").on('keypress',function(e) {
                if(e.which == 13) {
                     kirim_pesan();
                }
            });
        });
    </script>
      
  <script type="text/javascript">
      function get_kontak(){
          
                 $.ajax({
                    type: "post",
                    url: BASE_URL+"ApiEpunten/get_kontak",
                    data: {
                        "_token": "{{ csrf_token() }}",
                    },
                    beforeSend:
                    function () {
                    },
                    success: function (data) {
                        console.log(data);
                        $('#kontak').empty();
                        $('#kontak').append(data);
                        $('#kontak').append('<li class="p-20"></li>');
                        $("#search_kontak").val("");
                    },
                    error:
                    function (data) {

                    },
                    complete:
                    function (response) {
                        
                    }
                });
                
           
        }
      function get_kontak_byname(){
          
                 $.ajax({
                    type: "post",
                    url: BASE_URL+"ApiEpunten/get_kontak_byname",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        nama : $("#search_kontak").val(),
                    },
                    beforeSend:
                    function () {
                    },
                    success: function (data) {
                        console.log(data);
                        $('#kontak').empty();
                        $('#kontak').append(data);
                        $('#kontak').append('<li class="p-20"></li>');
                    },
                    error:
                    function (data) {

                    },
                    complete:
                    function (response) {
                        
                    }
                });
                
           
        }

      function kirim_pesan(){
       if (!validationpesan()) return;
            $.ajax({
                    type: "post",
                    url: BASE_URL+"ApiEpunten/chat_event",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        text : $("#isi_pesan").val(),
                        to_id : $("#to_id").val(),
                    },
                    beforeSend:
                    function () {

                    },
                    success: function (data) {
                        console.log(data);
                        get_kontak();
                        refresh_chat();
                        $("#isi_pesan").val("");
                    },
                    error:
                    function (data) {
                        swal("Error!", "Ooops!, Please Try Again, Something Went Wrong", "error"); 

                    },
                    complete:
                    function (response) {

                    }
                });
      
    }

      function chat_user(nik,nama_lgkp,color_id,is_active){
        $.ajax({
                    type: "post",
                    url: BASE_URL+"ApiEpunten/get_pesan",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        nik : nik,
                    },
                    beforeSend:
                    function () {
                    },
                    success: function (data) {
                        console.log(data);
                         $('#chat_title').html(nama_lgkp+'<small class="pull-right '+color_id+'">'+is_active+'</small></span>');
                         $("#chat_image").show();
                         $("#chat_image").attr("src",BASE_URL+'assets/upload/pp/'+nik+'.jpg');
                         $("#to_id").val(nik);
                         $('#pesan').empty();
                         $('#pesan').append(data);
                         get_kontak();

                    },
                    error:
                    function (data) {
                      console.log(data);
                         $('#pesan').empty();
                         $("#to_id").val();
                         $('#chat_title').html("Chat Message");
                         $("#chat_image").hide();
                         toastr.error('Gagal Memperbarui Pesan !');
                    },
                    complete:
                    function (response) {
                        var objDiv = document.getElementById("pesan");
                      objDiv.scrollTop = objDiv.scrollHeight;
                    }
                });
              
        }

      function refresh_chat(){
        $.ajax({
                    type: "post",
                    url: BASE_URL+"ApiEpunten/get_pesan",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        nik : $("#to_id").val(),
                    },
                    beforeSend:
                    function () {
                    },
                    success: function (data) {
                        console.log(data);
                         $('#pesan').empty();
                         $('#pesan').append(data);
                    },
                    error:
                    function (data) {
                      console.log(data);
                    },
                    complete:
                    function (response) {
                        var objDiv = document.getElementById("pesan");
                      objDiv.scrollTop = objDiv.scrollHeight;
                    }
                });
        }
         function validationpesan() {
            var isi_pesan = $("#isi_pesan");
            if (isi_pesan.val().length == 0) {
                toastr.warning("Isi Pesan Tidak Boleh Kosong");  
                return false;
            }
            var to_id = $("#to_id");
            if (to_id.val().length == 0) {
                toastr.warning("Penerima Tidak Boleh Kosong");  
                return false;
            }
           return true
        }
    </script>>