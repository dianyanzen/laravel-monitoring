<div id="page-wrapper">
            <div class="container-fluid">
               <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $mtitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                
                        <ol class="breadcrumb">
                            <li><a href="{{ url('/') }}/">Laporan</a></li>
                            <li class="active"><?php echo $mtitle; ?></li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- .row -->
                <div class="row">
                <form name ="get_form" action="<?php echo $my_url; ?>" method="post">
                    @csrf
                    <div class="col-sm-12">
                        <div class="white-box">
                            <div class="row">
                                <div class="col-lg-12">         
                                    <div class="form-group">
                                        <h3 class="box-title m-b-0"><b>Jenis Laporan</b></h3>
                                        <select class="form-control select2" name="mlap" id="mlap">
                                <option  value="0">-- Pilih Menu --</option>
                            </select>
                            
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">         
                                    <div class="form-group" <?php if (!empty($is_colnotwil)){echo 'style="display: none!important;"'; }?>>
                                        <h3 class="box-title m-b-0"><b>Kecamatan</b></h3>
                                        <select class="form-control select2" name="no_kec" id="no_kec">
                                <option  value="0">-- Pilih Kecamatan --</option>
                            </select>
                            
                                    </div>
                                </div>
                                <div class="col-lg-6">         
                                    <div class="form-group" <?php if (!empty($is_colnotwil)){echo 'style="display: none!important;"'; }?>>
                                        <h3 class="box-title m-b-0"><b>Kelurahan</b></h3>
                                        <select class="form-control select2" name="no_kel" id="no_kel">
                                        <option value="0">-- Pilih Kelurahan --</option>  
                                    </select>
                           
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <button type="submit" class="btn btn-info waves-effect waves-light m-r-10 pull-right" id="btn-filter" onclick="on_serach();" >Search <i class="mdi  mdi-magnify fa-fw"></i></button>
                                  <button type="button" class="btn btn-invert waves-effect waves-light m-r-10 pull-right" id="btn-reset" onclick="on_clear();">Clear <i class="mdi  mdi-delete fa-fw"></i></button>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
                <?php if (!empty($jenis_lap)){ ?>
                <?php if ($jenis_lap == 1 || $jenis_lap == 2 || $jenis_lap == 3){ ?>
                <div class="row">
                <div class="col-sm-12">
                        <div class="white-box">
                            <h3 class="box-title m-b-0"><?php echo $stitle; ?></h3>
                            <p class="text-muted m-b-30">Dinas Kependudukan Dan Pencatatan Sipil Kota Bandung</p>
                            <div class="table-responsive">
                                <table id="mytable" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                   <thead>
                                        <tr>
                                            <th width="10%" style="text-align: right;"><?php echo $no_wil ;?></th>
                                            <th width="30%" style="text-align: left;"><?php echo $kode_wil ;?></th>
                                            <th width="20%" style="text-align: right;">0-4 Tahun</th>
                                            <th width="20%" style="text-align: right;">5-9 Tahun</th>
                                            <th width="20%" style="text-align: right;">10-14 Tahun</th>
                                            <th width="20%" style="text-align: right;">15-19 Tahun</th>
                                            <th width="20%" style="text-align: right;">20-24 Tahun</th>
                                            <th width="20%" style="text-align: right;">25-29 Tahun</th>
                                            <th width="20%" style="text-align: right;">30-34 Tahun</th>
                                            <th width="20%" style="text-align: right;">35-39 Tahun</th>
                                            <th width="20%" style="text-align: right;">40-44 Tahun</th>
                                            <th width="20%" style="text-align: right;">45-49 Tahun</th>
                                            <th width="20%" style="text-align: right;">50-54 Tahun</th>
                                            <th width="20%" style="text-align: right;">55-59 Tahun</th>
                                            <th width="20%" style="text-align: right;">60-64 Tahun</th>
                                            <th width="20%" style="text-align: right;">65-69 Tahun</th>
                                            <th width="20%" style="text-align: right;">70-74 Tahun</th>
                                            <th width="20%" style="text-align: right;">75> Tahun</th>
                                            <th width="20%" style="text-align: right;">Jumlah</th>
                                        </tr>
                                    </thead>
                                    <tbody id="my_data">
                                          <?php
                                            $SUM_R1 = 0;
                                            $SUM_R2 = 0;
                                            $SUM_R3 = 0;
                                            $SUM_R4 = 0;
                                            $SUM_R5 = 0;
                                            $SUM_R6 = 0;
                                            $SUM_R7 = 0;
                                            $SUM_R8 = 0;
                                            $SUM_R9 = 0;
                                            $SUM_R10 = 0;
                                            $SUM_R11 = 0;
                                            $SUM_R12 = 0;
                                            $SUM_R13 = 0;
                                            $SUM_R14 = 0;
                                            $SUM_R15 = 0;
                                            $SUM_R16 = 0;
                                            $SUM_JUMLAH = 0;
                                        if (!empty($data)){
                                           foreach($data as $row){?>
                                             <tr>
                                                <td width="10%" style="text-align: right;"><?php echo $row->no_wil ;?></td>
                                                <td width="30%" style="text-align: left;"><?php echo $row->nama_wil ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r1 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r2 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r3 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r4 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r5 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r6 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r7 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r8 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r9 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r10 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r11 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r12 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r13 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r14 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r15 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r16 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->jumlah ;?></td>
                                            </tr>
                                         <?php 
                                            $SUM_R1 += $row->r1;
                                            $SUM_R2 += $row->r2;
                                            $SUM_R3 += $row->r3;
                                            $SUM_R4 += $row->r4;
                                            $SUM_R5 += $row->r5;
                                            $SUM_R6 += $row->r6;
                                            $SUM_R7 += $row->r7;
                                            $SUM_R8 += $row->r8;
                                            $SUM_R9 += $row->r9;
                                            $SUM_R10 += $row->r10;
                                            $SUM_R11 += $row->r11;
                                            $SUM_R12 += $row->r12;
                                            $SUM_R13 += $row->r13;
                                            $SUM_R14 += $row->r14;
                                            $SUM_R15 += $row->r15;
                                            $SUM_R16 += $row->r16;
                                            $SUM_JUMLAH += $row->jumlah;
                                            }
                                         } ?>
                                    </tbody>
                                   <?php if (!empty($data)){ ?>
                                    <tfoot id="my_foot">
                                         <tr>
                                            <th width="40%" colspan="2" style="text-align: center;">Jumlah</th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R1),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R2),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R3),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R4),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R5),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R6),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R7),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R8),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R9),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R10),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R11),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R12),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R13),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R14),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R15),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R16),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_JUMLAH),0,',','.');?></th>
                                        </tr>
                                    </tfoot>
                                    <?php } ?>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <?php if ($jenis_lap == 4 || $jenis_lap == 5 || $jenis_lap == 6){ ?>
                <div class="row">
                <div class="col-sm-12">
                        <div class="white-box">
                            <h3 class="box-title m-b-0"><?php echo $stitle; ?></h3>
                            <p class="text-muted m-b-30">Dinas Kependudukan Dan Pencatatan Sipil Kota Bandung</p>
                            <div class="table-responsive">
                                <table id="mytable" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                   <thead>
                                        <tr>
                                            <th width="10%" style="text-align: right;"><?php echo $no_wil ;?></th>
                                            <th width="30%" style="text-align: left;"><?php echo $kode_wil ;?></th>
                                            <th width="20%" style="text-align: right;">0-9 Tahun</th>
                                            <th width="20%" style="text-align: right;">10-19 Tahun</th>
                                            <th width="20%" style="text-align: right;">20-29 Tahun</th>
                                            <th width="20%" style="text-align: right;">30-39 Tahun</th>
                                            <th width="20%" style="text-align: right;">40-49 Tahun</th>
                                            <th width="20%" style="text-align: right;">50-59 Tahun</th>
                                            <th width="20%" style="text-align: right;">60-69 Tahun</th>
                                            <th width="20%" style="text-align: right;">70-79 Tahun</th>
                                            <th width="20%" style="text-align: right;">80 > Tahun</th>
                                            <th width="20%" style="text-align: right;">Jumlah</th>
                                        </tr>
                                    </thead>
                                    <tbody id="my_data">
                                          <?php
                                                $SUM_R1 = 0;
                                                $SUM_R2 = 0;
                                                $SUM_R3 = 0;
                                                $SUM_R4 = 0;
                                                $SUM_R5 = 0;
                                                $SUM_R6 = 0;
                                                $SUM_R7 = 0;
                                                $SUM_R8 = 0;
                                                $SUM_R9 = 0;
                                                $SUM_JUMLAH = 0;
                                        if (!empty($data)){
                                           foreach($data as $row){?>
                                             <tr>
                                                <td width="10%" style="text-align: right;"><?php echo $row->no_wil ;?></td>
                                                <td width="30%" style="text-align: left;"><?php echo $row->nama_wil ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r1 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r2 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r3 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r4 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r5 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r6 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r7 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r8 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r9 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->jumlah ;?></td>
                                            </tr>
                                         <?php 
                                                $SUM_R1 += $row->r1;
                                                $SUM_R2 += $row->r2;
                                                $SUM_R3 += $row->r3;
                                                $SUM_R4 += $row->r4;
                                                $SUM_R5 += $row->r5;
                                                $SUM_R6 += $row->r6;
                                                $SUM_R7 += $row->r7;
                                                $SUM_R8 += $row->r8;
                                                $SUM_R9 += $row->r9;
                                                $SUM_JUMLAH += $row->jumlah;
                                            }
                                         } ?>
                                    </tbody>
                                    <?php if (!empty($data)){ ?>
                                    <tfoot id="my_foot">
                                         <tr>
                                            <th width="40%" colspan="2" style="text-align: center;">Jumlah</th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R1),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R2),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R3),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R4),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R5),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R6),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R7),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R8),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R9),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_JUMLAH),0,',','.');?></th>
                                        </tr>
                                    </tfoot>
                                    <?php } ?>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <?php if ($jenis_lap == 7 || $jenis_lap == 8 || $jenis_lap == 9){ ?>
                <div class="row">
                <div class="col-sm-12">
                        <div class="white-box">
                            <h3 class="box-title m-b-0"><?php echo $stitle; ?></h3>
                            <p class="text-muted m-b-30">Dinas Kependudukan Dan Pencatatan Sipil Kota Bandung</p>
                            <div class="table-responsive">
                                <table id="mytable" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                   <thead>
                                        <tr>
                                            <th width="10%" style="text-align: right;"><?php echo $no_wil ;?></th>
                                            <th width="30%" style="text-align: left;"><?php echo $kode_wil ;?></th>
                                            <th width="20%" style="text-align: right;">0 ahun</th>
                                            <th width="20%" style="text-align: right;">1 ahun</th>
                                            <th width="20%" style="text-align: right;">2 ahun</th>
                                            <th width="20%" style="text-align: right;">3 ahun</th>
                                            <th width="20%" style="text-align: right;">4 ahun</th>
                                            <th width="20%" style="text-align: right;">5 ahun</th>
                                            <th width="20%" style="text-align: right;">6 ahun</th>
                                            <th width="20%" style="text-align: right;">7 ahun</th>
                                            <th width="20%" style="text-align: right;">8 ahun</th>
                                            <th width="20%" style="text-align: right;">9 Tahun</th>
                                            <th width="20%" style="text-align: right;">10 Tahun</th>
                                            <th width="20%" style="text-align: right;">11 Tahun</th>
                                            <th width="20%" style="text-align: right;">12 Tahun</th>
                                            <th width="20%" style="text-align: right;">13 Tahun</th>
                                            <th width="20%" style="text-align: right;">14 Tahun</th>
                                            <th width="20%" style="text-align: right;">15 Tahun</th>
                                            <th width="20%" style="text-align: right;">16 Tahun</th>
                                            <th width="20%" style="text-align: right;">17 Tahun</th>
                                            <th width="20%" style="text-align: right;">18 Tahun</th>
                                            <th width="20%" style="text-align: right;">19 Tahun</th>
                                            <th width="20%" style="text-align: right;">20 Tahun</th>
                                            <th width="20%" style="text-align: right;">21 Tahun</th>
                                            <th width="20%" style="text-align: right;">22 Tahun</th>
                                            <th width="20%" style="text-align: right;">23 Tahun</th>
                                            <th width="20%" style="text-align: right;">24 Tahun</th>
                                            <th width="20%" style="text-align: right;">25 Tahun</th>
                                            <th width="20%" style="text-align: right;">26 Tahun</th>
                                            <th width="20%" style="text-align: right;">27 Tahun</th>
                                            <th width="20%" style="text-align: right;">28 Tahun</th>
                                            <th width="20%" style="text-align: right;">29 Tahun</th>
                                            <th width="20%" style="text-align: right;">30 Tahun</th>
                                            <th width="20%" style="text-align: right;">31 Tahun</th>
                                            <th width="20%" style="text-align: right;">32 Tahun</th>
                                            <th width="20%" style="text-align: right;">33 Tahun</th>
                                            <th width="20%" style="text-align: right;">34 Tahun</th>
                                            <th width="20%" style="text-align: right;">35 Tahun</th>
                                            <th width="20%" style="text-align: right;">36 Tahun</th>
                                            <th width="20%" style="text-align: right;">37 Tahun</th>
                                            <th width="20%" style="text-align: right;">38 Tahun</th>
                                            <th width="20%" style="text-align: right;">39 Tahun</th>
                                            <th width="20%" style="text-align: right;">40 Tahun</th>
                                            <th width="20%" style="text-align: right;">41 Tahun</th>
                                            <th width="20%" style="text-align: right;">42 Tahun</th>
                                            <th width="20%" style="text-align: right;">43 Tahun</th>
                                            <th width="20%" style="text-align: right;">44 Tahun</th>
                                            <th width="20%" style="text-align: right;">45 Tahun</th>
                                            <th width="20%" style="text-align: right;">46 Tahun</th>
                                            <th width="20%" style="text-align: right;">47 Tahun</th>
                                            <th width="20%" style="text-align: right;">48 Tahun</th>
                                            <th width="20%" style="text-align: right;">49 Tahun</th>
                                            <th width="20%" style="text-align: right;">50 Tahun</th>
                                            <th width="20%" style="text-align: right;">51 Tahun</th>
                                            <th width="20%" style="text-align: right;">52 Tahun</th>
                                            <th width="20%" style="text-align: right;">53 Tahun</th>
                                            <th width="20%" style="text-align: right;">54 Tahun</th>
                                            <th width="20%" style="text-align: right;">55 Tahun</th>
                                            <th width="20%" style="text-align: right;">56 Tahun</th>
                                            <th width="20%" style="text-align: right;">57 Tahun</th>
                                            <th width="20%" style="text-align: right;">58 Tahun</th>
                                            <th width="20%" style="text-align: right;">59 Tahun</th>
                                            <th width="20%" style="text-align: right;">60 Tahun</th>
                                            <th width="20%" style="text-align: right;">61 Tahun</th>
                                            <th width="20%" style="text-align: right;">62 Tahun</th>
                                            <th width="20%" style="text-align: right;">63 Tahun</th>
                                            <th width="20%" style="text-align: right;">64 Tahun</th>
                                            <th width="20%" style="text-align: right;">65 Tahun</th>
                                            <th width="20%" style="text-align: right;">66 Tahun</th>
                                            <th width="20%" style="text-align: right;">67 Tahun</th>
                                            <th width="20%" style="text-align: right;">68 Tahun</th>
                                            <th width="20%" style="text-align: right;">69 Tahun</th>
                                            <th width="20%" style="text-align: right;">70 Tahun</th>
                                            <th width="20%" style="text-align: right;">71 Tahun</th>
                                            <th width="20%" style="text-align: right;">72 Tahun</th>
                                            <th width="20%" style="text-align: right;">73 Tahun</th>
                                            <th width="20%" style="text-align: right;">74 Tahun</th>
                                            <th width="20%" style="text-align: right;">75 Tahun</th>
                                            <th width="20%" style="text-align: right;">76 Tahun</th>
                                            <th width="20%" style="text-align: right;">77 Tahun</th>
                                            <th width="20%" style="text-align: right;">78 Tahun</th>
                                            <th width="20%" style="text-align: right;">79 Tahun</th>
                                            <th width="20%" style="text-align: right;">80 > Tahun</th>
                                            <th width="20%" style="text-align: right;">Jumlah</th>
                                        </tr>
                                    </thead>
                                    <tbody id="my_data">
                                          <?php
                                                $SUM_R1 = 0;
                                                $SUM_R2 = 0;
                                                $SUM_R3 = 0;
                                                $SUM_R4 = 0;
                                                $SUM_R5 = 0;
                                                $SUM_R6 = 0;
                                                $SUM_R7 = 0;
                                                $SUM_R8 = 0;
                                                $SUM_R9 = 0;
                                                $SUM_R10 = 0;
                                                $SUM_R11 = 0;
                                                $SUM_R12 = 0;
                                                $SUM_R13 = 0;
                                                $SUM_R14 = 0;
                                                $SUM_R15 = 0;
                                                $SUM_R16 = 0;
                                                $SUM_R17 = 0;
                                                $SUM_R18 = 0;
                                                $SUM_R19 = 0;
                                                $SUM_R20 = 0;
                                                $SUM_R21 = 0;
                                                $SUM_R22 = 0;
                                                $SUM_R23 = 0;
                                                $SUM_R24 = 0;
                                                $SUM_R25 = 0;
                                                $SUM_R26 = 0;
                                                $SUM_R27 = 0;
                                                $SUM_R28 = 0;
                                                $SUM_R29 = 0;
                                                $SUM_R30 = 0;
                                                $SUM_R31 = 0;
                                                $SUM_R32 = 0;
                                                $SUM_R33 = 0;
                                                $SUM_R34 = 0;
                                                $SUM_R35 = 0;
                                                $SUM_R36 = 0;
                                                $SUM_R37 = 0;
                                                $SUM_R38 = 0;
                                                $SUM_R39 = 0;
                                                $SUM_R40 = 0;
                                                $SUM_R41 = 0;
                                                $SUM_R42 = 0;
                                                $SUM_R43 = 0;
                                                $SUM_R44 = 0;
                                                $SUM_R45 = 0;
                                                $SUM_R46 = 0;
                                                $SUM_R47 = 0;
                                                $SUM_R48 = 0;
                                                $SUM_R49 = 0;
                                                $SUM_R50 = 0;
                                                $SUM_R51 = 0;
                                                $SUM_R52 = 0;
                                                $SUM_R53 = 0;
                                                $SUM_R54 = 0;
                                                $SUM_R55 = 0;
                                                $SUM_R56 = 0;
                                                $SUM_R57 = 0;
                                                $SUM_R58 = 0;
                                                $SUM_R59 = 0;
                                                $SUM_R60 = 0;
                                                $SUM_R61 = 0;
                                                $SUM_R62 = 0;
                                                $SUM_R63 = 0;
                                                $SUM_R64 = 0;
                                                $SUM_R65 = 0;
                                                $SUM_R66 = 0;
                                                $SUM_R67 = 0;
                                                $SUM_R68 = 0;
                                                $SUM_R69 = 0;
                                                $SUM_R70 = 0;
                                                $SUM_R71 = 0;
                                                $SUM_R72 = 0;
                                                $SUM_R73 = 0;
                                                $SUM_R74 = 0;
                                                $SUM_R75 = 0;
                                                $SUM_R76 = 0;
                                                $SUM_R77 = 0;
                                                $SUM_R78 = 0;
                                                $SUM_R79 = 0;
                                                $SUM_R80 = 0;
                                                $SUM_REND = 0;
                                                $SUM_JUMLAH = 0;
                                        if (!empty($data)){
                                           foreach($data as $row){?>
                                             <tr>
                                                <td width="10%" style="text-align: right;"><?php echo $row->no_wil ;?></td>
                                                <td width="30%" style="text-align: left;"><?php echo $row->nama_wil ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r1 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r2 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r3 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r4 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r5 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r6 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r7 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r8 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r9 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r10 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r11 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r12 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r13 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r14 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r15 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r16 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r17 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r18 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r19 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r20 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r21 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r22 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r23 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r24 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r25 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r26 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r27 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r28 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r29 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r30 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r31 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r32 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r33 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r34 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r35 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r36 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r37 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r38 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r39 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r40 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r41 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r42 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r43 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r44 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r45 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r46 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r47 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r48 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r49 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r50 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r51 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r52 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r53 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r54 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r55 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r56 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r57 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r58 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r59 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r60 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r61 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r62 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r63 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r64 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r65 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r66 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r67 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r68 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r69 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r70 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r71 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r72 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r73 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r74 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r75 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r76 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r77 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r78 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r79 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r80 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->rend ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->jumlah ;?></td>
                                            </tr>
                                         <?php 
                                                $SUM_R1 += $row->r1;
                                                $SUM_R2 += $row->r2;
                                                $SUM_R3 += $row->r3;
                                                $SUM_R4 += $row->r4;
                                                $SUM_R5 += $row->r5;
                                                $SUM_R6 += $row->r6;
                                                $SUM_R7 += $row->r7;
                                                $SUM_R8 += $row->r8;
                                                $SUM_R9 += $row->r9;
                                                $SUM_R10 += $row->r10;
                                                $SUM_R11 += $row->r11;
                                                $SUM_R12 += $row->r12;
                                                $SUM_R13 += $row->r13;
                                                $SUM_R14 += $row->r14;
                                                $SUM_R15 += $row->r15;
                                                $SUM_R16 += $row->r16;
                                                $SUM_R17 += $row->r17;
                                                $SUM_R18 += $row->r18;
                                                $SUM_R19 += $row->r19;
                                                $SUM_R20 += $row->r20;
                                                $SUM_R21 += $row->r21;
                                                $SUM_R22 += $row->r22;
                                                $SUM_R23 += $row->r23;
                                                $SUM_R24 += $row->r24;
                                                $SUM_R25 += $row->r25;
                                                $SUM_R26 += $row->r26;
                                                $SUM_R27 += $row->r27;
                                                $SUM_R28 += $row->r28;
                                                $SUM_R29 += $row->r29;
                                                $SUM_R30 += $row->r30;
                                                $SUM_R31 += $row->r31;
                                                $SUM_R32 += $row->r32;
                                                $SUM_R33 += $row->r33;
                                                $SUM_R34 += $row->r34;
                                                $SUM_R35 += $row->r35;
                                                $SUM_R36 += $row->r36;
                                                $SUM_R37 += $row->r37;
                                                $SUM_R38 += $row->r38;
                                                $SUM_R39 += $row->r39;
                                                $SUM_R40 += $row->r40;
                                                $SUM_R41 += $row->r41;
                                                $SUM_R42 += $row->r42;
                                                $SUM_R43 += $row->r43;
                                                $SUM_R44 += $row->r44;
                                                $SUM_R45 += $row->r45;
                                                $SUM_R46 += $row->r46;
                                                $SUM_R47 += $row->r47;
                                                $SUM_R48 += $row->r48;
                                                $SUM_R49 += $row->r49;
                                                $SUM_R50 += $row->r50;
                                                $SUM_R51 += $row->r51;
                                                $SUM_R52 += $row->r52;
                                                $SUM_R53 += $row->r53;
                                                $SUM_R54 += $row->r54;
                                                $SUM_R55 += $row->r55;
                                                $SUM_R56 += $row->r56;
                                                $SUM_R57 += $row->r57;
                                                $SUM_R58 += $row->r58;
                                                $SUM_R59 += $row->r59;
                                                $SUM_R60 += $row->r60;
                                                $SUM_R61 += $row->r61;
                                                $SUM_R62 += $row->r62;
                                                $SUM_R63 += $row->r63;
                                                $SUM_R64 += $row->r64;
                                                $SUM_R65 += $row->r65;
                                                $SUM_R66 += $row->r66;
                                                $SUM_R67 += $row->r67;
                                                $SUM_R68 += $row->r68;
                                                $SUM_R69 += $row->r69;
                                                $SUM_R70 += $row->r70;
                                                $SUM_R71 += $row->r71;
                                                $SUM_R72 += $row->r72;
                                                $SUM_R73 += $row->r73;
                                                $SUM_R74 += $row->r74;
                                                $SUM_R75 += $row->r75;
                                                $SUM_R76 += $row->r76;
                                                $SUM_R77 += $row->r77;
                                                $SUM_R78 += $row->r78;
                                                $SUM_R79 += $row->r79;
                                                $SUM_R80 += $row->r80;
                                                $SUM_REND += $row->rend;
                                                $SUM_JUMLAH += $row->jumlah;
                                            }
                                         } ?>
                                    </tbody>
                                    <?php if (!empty($data)){ ?>
                                    <tfoot id="my_foot">
                                         <tr>
                                            <th width="40%" colspan="2" style="text-align: center;">Jumlah</th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R1),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R2),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R3),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R4),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R5),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R6),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R7),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R8),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R9),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R10),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R11),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R12),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R13),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R14),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R15),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R16),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R17),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R18),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R19),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R20),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R21),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R22),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R23),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R24),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R25),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R26),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R27),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R28),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R29),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R30),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R31),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R32),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R33),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R34),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R35),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R36),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R37),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R38),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R39),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R40),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R41),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R42),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R43),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R44),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R45),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R46),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R47),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R48),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R49),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R50),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R51),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R52),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R53),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R54),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R55),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R56),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R57),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R58),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R59),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R60),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R61),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R62),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R63),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R64),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R65),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R66),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R67),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R68),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R69),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R70),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R71),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R72),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R73),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R74),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R75),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R76),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R77),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R78),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R79),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R80),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_REND),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_JUMLAH),0,',','.');?></th>
                                        </tr>
                                    </tfoot>
                                    <?php } ?>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <?php if ($jenis_lap == 10 || $jenis_lap == 11 || $jenis_lap == 12){ ?>
                <div class="row">
                <div class="col-sm-12">
                        <div class="white-box">
                            <h3 class="box-title m-b-0"><?php echo $stitle; ?></h3>
                            <p class="text-muted m-b-30">Dinas Kependudukan Dan Pencatatan Sipil Kota Bandung</p>
                            <div class="table-responsive">
                                <table id="mytable" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                   <thead>
                                        <tr>
                                            <th width="10%" style="text-align: right;"><?php echo $no_wil ;?></th>
                                            <th width="30%" style="text-align: left;"><?php echo $kode_wil ;?></th>
                                            <th width="20%" style="text-align: right;">Paud</th>
                                            <th width="20%" style="text-align: right;">TK</th>
                                            <th width="20%" style="text-align: right;">SD</th>
                                            <th width="20%" style="text-align: right;">SMP/SLTP</th>
                                            <th width="20%" style="text-align: right;">SMA/SLTA</th>
                                            <th width="20%" style="text-align: right;">Jumlah</th>
                                        </tr>
                                    </thead>
                                    <tbody id="my_data">
                                          <?php
                                                $SUM_R1 = 0;
                                                $SUM_R2 = 0;
                                                $SUM_R3 = 0;
                                                $SUM_R4 = 0;
                                                $SUM_R5 = 0;
                                                $SUM_JUMLAH = 0;
                                        if (!empty($data)){
                                           foreach($data as $row){?>
                                             <tr>
                                                <td width="10%" style="text-align: right;"><?php echo $row->no_wil ;?></td>
                                                <td width="30%" style="text-align: left;"><?php echo $row->nama_wil ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r1 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r2 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r3 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r4 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r5 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->jumlah ;?></td>
                                            </tr>
                                         <?php 
                                            $SUM_R1 += $row->r1;
                                            $SUM_R2 += $row->r2;
                                            $SUM_R3 += $row->r3;
                                            $SUM_R4 += $row->r4;
                                            $SUM_R5 += $row->r5;
                                            $SUM_JUMLAH += $row->jumlah;
                                            }
                                         } ?>
                                    </tbody>
                                    <?php if (!empty($data)){ ?>
                                    <tfoot id="my_foot">
                                         <tr>
                                            <th width="40%" colspan="2" style="text-align: center;">Jumlah</th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R1),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R2),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R3),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R4),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R5),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_JUMLAH),0,',','.');?></th>
                                        </tr>
                                    </tfoot>
                                    <?php } ?>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <?php if ($jenis_lap == 13){ ?>
                <div class="row">
                <div class="col-sm-12">
                        <div class="white-box">
                            <h3 class="box-title m-b-0"><?php echo $stitle; ?></h3>
                            <p class="text-muted m-b-30">Dinas Kependudukan Dan Pencatatan Sipil Kota Bandung</p>
                            <div class="table-responsive">
                                <table id="mytable" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                   <thead>
                                        <tr>
                                            <th width="10%" style="text-align: right;"><?php echo $no_wil ;?></th>
                                            <th width="30%" style="text-align: left;"><?php echo $kode_wil ;?></th>
                                            <th width="20%" style="text-align: right;">Laki-Laki</th>
                                            <th width="20%" style="text-align: right;">Perempuan</th>
                                            <th width="20%" style="text-align: right;">Jumlah</th>
                                        </tr>
                                    </thead>
                                    <tbody id="my_data">
                                          <?php
                                            $SUM_R1 = 0;
                                            $SUM_R2 = 0;
                                            $SUM_JUMLAH = 0;
                                        if (!empty($data)){
                                           foreach($data as $row){?>
                                             <tr>
                                                <td width="10%" style="text-align: right;"><?php echo $row->no_wil ;?></td>
                                                <td width="30%" style="text-align: left;"><?php echo $row->nama_wil ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r1 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r2 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->jumlah ;?></td>
                                            </tr>
                                         <?php 
                                            $SUM_R1 += $row->r1;
                                            $SUM_R2 += $row->r2;
                                            $SUM_JUMLAH += $row->jumlah;
                                            }
                                         } ?>
                                    </tbody>
                                    <?php if (!empty($data)){ ?>
                                    <tfoot id="my_foot">
                                         <tr>
                                            <th width="40%" colspan="2" style="text-align: center;">Jumlah</th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R1),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R2),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_JUMLAH),0,',','.');?></th>
                                        </tr>
                                    </tfoot>
                                    <?php } ?>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <?php if ($jenis_lap == 14){ ?>
                <div class="row">
                <div class="col-sm-12">
                        <div class="white-box">
                            <h3 class="box-title m-b-0"><?php echo $stitle; ?></h3>
                            <p class="text-muted m-b-30">Dinas Kependudukan Dan Pencatatan Sipil Kota Bandung</p>
                            <div class="table-responsive">
                                <table id="mytable" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                   <thead>
                                        <tr>
                                            <th width="10%" style="text-align: right;"><?php echo $no_wil ;?></th>
                                            <th width="30%" style="text-align: left;"><?php echo $kode_wil ;?></th>
                                            <th width="20%" style="text-align: right;">Belum Sekolah</th>
                                            <th width="20%" style="text-align: right;">Tidak Tamat SD</th>
                                            <th width="20%" style="text-align: right;">Tamat SD</th>
                                            <th width="20%" style="text-align: right;">SLTP</th>
                                            <th width="20%" style="text-align: right;">SLTA</th>
                                            <th width="20%" style="text-align: right;">Diploma II</th>
                                            <th width="20%" style="text-align: right;">Diploma II</th>
                                            <th width="20%" style="text-align: right;">SI</th>
                                            <th width="20%" style="text-align: right;">SII</th>
                                            <th width="20%" style="text-align: right;">SIII</th>
                                            <th width="20%" style="text-align: right;">Jumlah</th>
                                        </tr>
                                    </thead>
                                    <tbody id="my_data">
                                          <?php
                                                $SUM_R1 = 0;
                                                $SUM_R2 = 0;
                                                $SUM_R3 = 0;
                                                $SUM_R4 = 0;
                                                $SUM_R5 = 0;
                                                $SUM_R6 = 0;
                                                $SUM_R7 = 0;
                                                $SUM_R8 = 0;
                                                $SUM_R9 = 0;
                                                $SUM_R10 = 0;
                                                $SUM_JUMLAH = 0;
                                        if (!empty($data)){
                                           foreach($data as $row){?>
                                             <tr>
                                                <td width="10%" style="text-align: right;"><?php echo $row->no_wil ;?></td>
                                                <td width="30%" style="text-align: left;"><?php echo $row->nama_wil ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r1 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r2 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r3 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r4 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r5 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r6 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r7 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r8 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r9 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->r10 ;?></td>
                                                <td width="20%" style="text-align: right;"><?php echo $row->jumlah ;?></td>
                                            </tr>
                                         <?php $SUM_R1 += $row->r1;
                                                $SUM_R2 += $row->r2;
                                                $SUM_R3 += $row->r3;
                                                $SUM_R4 += $row->r4;
                                                $SUM_R5 += $row->r5;
                                                $SUM_R6 += $row->r6;
                                                $SUM_R7 += $row->r7;
                                                $SUM_R8 += $row->r8;
                                                $SUM_R9 += $row->r9;
                                                $SUM_R10 += $row->r10;
                                                $SUM_JUMLAH += $row->jumlah;
                                            }
                                         } ?>
                                    </tbody>
                                    <?php if (!empty($data)){ ?>
                                    <tfoot id="my_foot">
                                         <tr>
                                            <th width="40%" colspan="2" style="text-align: center;">Jumlah</th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R1),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R2),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R3),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R4),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R5),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R6),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R7),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R8),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R9),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_R10),0,',','.');?></th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($SUM_JUMLAH),0,',','.');?></th>
                                        </tr>
                                    </tfoot>
                                    <?php } ?>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <?php } ?>
            </div>
            <!-- /.container-fluid -->
           @include('shared.footer_detail')
        </div>