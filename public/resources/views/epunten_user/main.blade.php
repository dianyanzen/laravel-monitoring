<!DOCTYPE html>
<html lang="en">
@include('shared.head')
<body class="fix-header">
    <div id="wrapper">
            @include('shared.nav_top')
            @include('shared/nav_side')
            @include('epunten_user/content')
    </div>
     
    @include('shared/footer')
    
    @include('epunten_user/javascript')
</body>
</html>