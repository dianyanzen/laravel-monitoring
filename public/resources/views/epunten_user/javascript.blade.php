     <script>
        function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode != 44 && charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }
       function isphone(evt)
       {
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode != 43 && charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }
       
           $(document).ready(function() {
                get_table();
            });
    function get_table() {
        console.log();
        $('#level-list').DataTable({
                destroy: true,
                 "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                 "columnDefs": [
                    {"className": "dt-center", "targets": "_all"}
                  ],
                "pageLength" : 50,
                "ajax": {
                 "url": BASE_URL+"api/get_user_list",
                "type": "post",
                "data": {
                "_token": "{{ csrf_token() }}",

                }
                }
            });
    }
    function on_reset(nama_lgkp,id) {
           
            swal({   
            title: "Apakah Anda Yakin ?",   
            text: "Me-reset Password User "+nama_lgkp+" Menjadi 123456 ?",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Ya, Reset Password",   
            cancelButtonText: "Tidak, Jangan Dirubah",   
            closeOnConfirm: false,   
            closeOnCancel: false 
            }, function(isConfirm){   
                if (isConfirm) {
                $.ajax({
                    type: "post",
                    url: BASE_URL+"Epunten/do_reset",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        id_peng : id
                    },
                    beforeSend:
                    function () {
                         block_screen();
                    },
                    success: function (data) {
                        console.log(data);
                         if (data.message_type > 0){
                              swal("Berhasil !", "Password User "+nama_lgkp+" Telah Diubah Menjadi 123456 !", "success");
                        }else{
                            swal("Warning!", data.message, "warning");  
                        }
                    },
                    error:
                    function (data) {
                        unblock_screen();
                        swal("Error!", "Ooops!, Please Try Again, Something Went Wrong", "error"); 

                    },
                    complete:
                    function (response) {
                        unblock_screen();
                    }
                });
                     
                } else {     
                    swal("Cancelled", "Password User "+nama_lgkp+" Tidak Jadi Diubah :)", "error");   
                } 
            });
        }
        function edit(id_peng){
            var pengajuan = id_peng;
            $('#edit_modal').modal('show');
            $('#id_pengajuan_edit').val(pengajuan);
            $.ajax({
                    type: "post",
                    url:  BASE_URL+"ApiEpunten/get_user_edit",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        id_peng : pengajuan,
                    },
                    beforeSend:
                    function () {
                         // block_screen();
                    },
                    success: function (data) {
                        console.log(data);
                        $('#nama_lgkp').val(data[0].nama_lgkp);
                        $('#email').val(data[0].email);
                        $('#telepon').val(data[0].telepon);
                       
                        
                    },
                    error:
                    function (data) {
                        // unblock_screen();
                        // swal("Error!", "Ooops!, Please Try Again, Something Went Wrong", "error"); 

                    },
                    complete:
                    function (response) {
                        // unblock_screen();  
                        // $("#no_kk_baru").val("");
                        
                    }
                });
          }

        function change_nik(id_peng,nik_lama){
            var pengajuan = id_peng;
            var nik_lama = nik_lama;
            $('#change_nik_modal').modal('show');
            $('#id_pengajuan_change_nik').val(pengajuan);
            $('#nik_lama').val(nik_lama);
            $.ajax({
                    type: "post",
                    url:  BASE_URL+"ApiEpunten/get_user_edit",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        id_peng : pengajuan,
                    },
                    beforeSend:
                    function () {
                         // block_screen();
                    },
                    success: function (data) {
                        console.log(data);
                        $('#nik_baru').val(data[0].nik);
                       
                        
                    },
                    error:
                    function (data) {
                        // unblock_screen();
                        // swal("Error!", "Ooops!, Please Try Again, Something Went Wrong", "error"); 

                    },
                    complete:
                    function (response) {
                        // unblock_screen();  
                        // $("#no_kk_baru").val("");
                        
                    }
                });
          }
          function hapus(id_peng,nama_lgkp){
              var pengajuan = id_peng;
              var nama_lgkp = toTitleCase(nama_lgkp.toLowerCase());
                 swal({   
                    title: "Apakah Anda Yakin ?",   
                    text: "Menghapus User "+nama_lgkp+" ?",   
                    type: "warning",   
                    showCancelButton: true,   
                    confirmButtonColor: "#f33155",   
                    confirmButtonText: "Ya, Hapus User",   
                    cancelButtonText: "Tidak, Jangan Dihapus",   
                    closeOnConfirm: false,   
                    closeOnCancel: false 
                    }, function(isConfirm){   
                        if (isConfirm) {
                        $.ajax({
                            type: "post",
                            url:  BASE_URL+"Epunten/do_delete_user",
                            dataType: "json",
                            data: {
                                "_token": "{{ csrf_token() }}",
                                id_peng : pengajuan,
                            },
                            beforeSend:
                            function () {
                                 block_screen();
                            },
                            success: function (data) {
                                console.log(data);
                                 if (data.message_type > 0){
                                      swal("Berhasil !", "User "+nama_lgkp+" Telah Dihapus !", "success");
                                }else{
                                    swal("Warning!", data.message, "warning");  
                                }
                            },
                            error:
                            function (data) {
                                unblock_screen();
                                swal("Error!", "Ooops!, Please Try Again, Something Went Wrong", "error"); 

                            },
                            complete:
                            function (response) {
                                unblock_screen();
                                get_table();
                            }
                        });
                             
                        } else {     
                            swal("Cancelled", "User "+nama_lgkp+" Tidak Jadi Dihapus :)", "error");   
                        } 
                    });
               
            }
        $('#submit_edit').click(function (e) {
            e.preventDefault();
            $.ajax({
                    type: "post",
                    url:  BASE_URL+"Epunten/edit_data_user",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        id_peng : $('#id_pengajuan_edit').val(),
                        nama_lgkp : $('#nama_lgkp').val(),
                        telepon : $('#telepon').val(),
                        email : $('#email').val(),
                        password : $('#password').val(),
                    },
                    beforeSend:
                    function () {
                         block_screen();
                    },
                    success: function (data) {
                        console.log(data);
                        swal("Success!", data.message, "success");
                        $('#edit_modal').modal('hide');
                       get_table();
                        
                    },
                    error:
                    function (data) {
                        unblock_screen();
                        swal("Error!", "Ooops!, Please Try Again, Something Went Wrong", "error"); 

                    },
                    complete:
                    function (response) {
                        unblock_screen();  
                        
                    }
                });
          });
        $('#submit_change_nik').click(function (e) {
            if (!validationmodaltolak()) return;
            e.preventDefault();
            $.ajax({
                    type: "post",
                    url:  BASE_URL+"Epunten/change_nik",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        nik_lama : $('#nik_lama').val(),
                        nik_baru : $('#nik_baru').val(),
                    },
                    beforeSend:
                    function () {
                         block_screen();
                    },
                    success: function (data) {
                        console.log(data);
                        if (data.message_type > 0){
                            swal("Success!", data.message, "success");
                            $('#change_nik_modal').modal('hide');
                            get_table();
                        }else{
                            swal("Warning!", data.message, "warning");  
                        }
                        
                        
                    },
                    error:
                    function (data) {
                        unblock_screen();
                        swal("Error!", "Ooops!, Please Try Again, Something Went Wrong", "error"); 

                    },
                    complete:
                    function (response) {
                        unblock_screen();  
                        
                    }
                });
          });

        function validationmodaltolak() {
            var nik_baru = $("#nik_baru");
            if (nik_baru.val().length != 16) {
                nik_baru.select();
                swal("Warning!", "Nik Tidak 16 Digit !", "warning");  
                return false;
            }
           return true
        }

     function block_screen(){
        
        $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff'
            },
            baseZ: 2000
        }); 
    }
    function unblock_screen(){
       $.unblockUI();
    }
    function toTitleCase(str) {
        return str.replace(/(?:^|\s)\w/g, function(match) {
            return match.toUpperCase();
        });
    }
    </script>