 <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $stitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                        <ol class="breadcrumb">
                           <li class="text-info"><?php echo $stitle; ?></li>
                        </ol>
                    </div>
                </div>
                <div id="edit_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog modal-dialog-scrollable">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title" id="modal-title">Edit Pengguna</h4> </div>
                                        <div class="modal-body">
                                            <div class="container-fluid">
                                            <form id="data_edit">
                                                <input type="hidden" id="id_pengajuan_edit" name="id_pengajuan_edit"> 
                                                <div class="form-group">
                                                    <label for="nama_lgkp" class="control-label"><span style="color: red!important">* </span>Nama Lengkap:</label>
                                                    <input type="text" class="form-control" id="nama_lgkp" name="nama_lgkp" style="text-transform:uppercase"> </div>
                                                <div class="form-group">
                                                    <label for="telepon" class="control-label">Telepon:</label>
                                                    <input type="text" class="form-control" placeholder="+62" value="+62" name="telepon" id="telepon" onkeypress="return isphone(event)"> </div>
                                                <div class="form-group">
                                                    <label for="email" class="control-label">E-mail:</label>
                                                    <input type="text" class="form-control" id="email" name="email"> </div>
                                                <div class="form-group">
                                                    <label for="password" class="control-label">Password:</label>
                                                    <input type="password" class="form-control" id="password" name="password"> </div>
                                                

                                            </form>
                                        </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                            <button type="button" id="submit_edit" class="btn btn-info waves-effect waves-light">Ubah</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                <div id="change_nik_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog modal-dialog-scrollable">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title" id="modal-title">Rubah Nik Pengguna</h4> </div>
                                        <div class="modal-body">
                                            <div class="container-fluid">
                                            <form id="data_change_nik">
                                                <input type="hidden" id="id_pengajuan_change_nik" name="id_pengajuan_change_nik"> 
                                                <div class="form-group">
                                                    <label for="nik_lama" class="control-label"><span style="color: red!important">* </span>Nik Lama:</label>
                                                    <input type="text" class="form-control" id="nik_lama" name="nik_lama" style="text-transform:uppercase" disabled> </div>
                                                <div class="form-group">
                                                    <label for="nik" class="control-label"><span style="color: red!important">* </span>Nik Baru:</label>
                                                    <input type="text" class="form-control" id="nik_baru" name="nik_baru" maxlength="16" style="text-transform:uppercase" onkeypress="return isphone(event)"> </div>
                                        </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                            <button type="button" id="submit_change_nik" class="btn btn-info waves-effect waves-light">Ubah</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                <div class="row">
                <div class="col-md-12">
                        <div class="white-box">
                            <h3 class="box-title"><?php echo $mtitle; ?></h3>
                            <div class="scrollable">
                                <div class="table-responsive">
                                    <table id="level-list" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                   <thead>
                                    
                                        <tr>
                                            <th width="5%" style="text-align: center;">No</th>
                                            <th width="5%" style="text-align: center;">Nik</th>
                                            <th width="40%" style="text-align: center;">No KK</th>
                                            <th width="20%" style="text-align: center;">Nama Lengkap</th>
                                            <th width="20%" style="text-align: center;">Email</th>
                                            <th width="20%" style="text-align: center;">Telepon</th>
                                            <th width="30%" style="text-align: center;">Operation</th>

                                        </tr>
                                       
                                    </thead>
                                    <tbody id="show_data">
                                       
                                        
                                    </tbody>
                                    
                                </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
       @include('shared.footer_detail')