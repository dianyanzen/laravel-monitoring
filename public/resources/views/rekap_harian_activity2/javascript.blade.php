    <script src="{{ url('/') }}/assets/plugins/bower_components/owl.carousel/owl.carousel.min.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/owl.carousel/owl.custom.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/flot/jquery.flot.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/Minimal-Gauge-chart/js/cmGauge.js"></script>
    <!-- Footable -->
    <script src="{{ url('/') }}/assets/plugins/bower_components/footable/js/footable.all.min.js"></script>
    
    <!--FooTable init-->
    <script src="{{ url('/') }}/assets/js/footable-init.js"></script>
 <script>
   function on_back() {
            block_screen();
            var url = "{{ url('/') }}/";
            url += "Report/Absensi";
            var win = window.location.replace(url);
            win.focus();
        } 
    jQuery(document).ready(function() {
        $('#pdf_tanggal').val('<?php echo date('m-Y');?>');
        $('#pdf_userid').val('<?php echo $user_id; ?>');
            $(".select2").select2();
            $('#tanggal').datepicker({
                 format: 'mm-yyyy',
                    viewMode : "months",
                    minViewMode : "months",               
                    autoclose: true,
                    todayHighlight: true,

            todayHighlight: true,
            onSelect: function(d,i){
                      if(d !== i.lastVal){
                          $(this).change();
                      }
                 }
            });
           get_table();
          
           $('#tanggal').change(function(){
                 var tgl = $(this).val();
                $('#pdf_tanggal').val(tgl);
                get_table();
            });
        });
       
    
    function get_table() {
                $.ajax({
                    type: "post",
                    url: BASE_URL+"Report/get_daily_harian_table2",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "tanggal":  $("#tanggal").val(),
                        "user_id":  '<?php echo $user_id; ?>'
                    },
                    beforeSend:
                    function () {

                    },
                    success: function (data) {
                        console.log(data);
                        if (!$.trim(data)){
                             $('#my_data').empty();
                            $('#my_data').append("<tr><td colspan='5' style='text-align: center;'>No data available</td></tr>");  
                            $('#jumdata').val(0); 
                        }else{
                            var i=0;
                            $('#my_data').empty();
                            $.each(data, function(key, value) {
                                i++;
                                $('#my_data').append("<tr style='border: 1px solid #e4e7ea;'>"+
                                                    "<td valign='center' style='border: 1px solid #e4e7ea; text-align: center;'>"+i+". </td>"+
                                                    "<td valign='center' style='border: 1px solid #e4e7ea; text-align: center;'>"+value.user_id+"</td>"+
                                                    "<td valign='center' style='border: 1px solid #e4e7ea; text-align: center;'>"+value.tanggal+"</td>"+
                                                    "<td valign='center' style='border: 1px solid #e4e7ea; text-align: left;'>"+value.description+"<br /></td>"+
                                                    "<td valign='center' style='border: 1px solid #e4e7ea; text-align: left;'>"+value.jumlah+" Aktivitas<br /></td>"+
                                                    "</tr>");
                                $('#jumdata').html(i);
                            }); 
                            
                        }
                       
                    },
                    error:
                    function (data) {
               

                    },
                    complete:
                    function (response) {

                    }
                });
    }
    function do_pdf(){
        $('#pdf_userid').val();
        $('#pdf_tanggal').val();
        $('#pdf_data').submit();
    }
    function block_screen(){
        
        $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff'
            },
            baseZ: 2000
        }); 
    }
    function unblock_screen(){
       $.unblockUI();
    }
    </script>