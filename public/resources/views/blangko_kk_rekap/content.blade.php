 <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $stitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                        <ol class="breadcrumb">
                            <li class="active"><?php echo $mtitle; ?></li>
                        </ol>
                    </div>
                </div>
                 <div class="row">
                <form name ="get_form" action="<?php echo $my_url; ?>" method="post">
				@csrf
                    <div class="col-sm-12">
                        <div class="white-box">
                            <div class="row">
                                <div class="col-lg-3">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_tgl" type="checkbox" checked disabled="true">
                                            <label for="cb_tgl"> Tanggal Pengambilan</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3">         
                                    <div class="form-group">
                                        <input class="form-control input-daterange-datepicker" type="text" id="tanggal" name="tanggal" /></div>
                                </div>
                                <div class="col-lg-3">         
                                    <div class="form-group">
                                        <!-- <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_nik" type="checkbox">
                                            <label for="cb_nik"> Nik</label>
                                        </div> -->
                                    </div>
                                </div>
                                <div class="col-lg-3">         
                                    <!-- <div class="form-group">
                                        <input class="form-control" type="text" id="nik" name="nik" disabled="true" onkeypress="return isNumberKey(event)" onchange="onlyNum()" /></div> -->
                                </div>
                            </div>
                            <div class="row">
                                <button type="submit" class="btn btn-info waves-effect waves-light m-r-10 pull-right" id="btn-filter" onclick="on_serach();" >Search <i class="mdi  mdi-magnify fa-fw"></i></button>
                                  <button type="button" class="btn btn-invert waves-effect waves-light m-r-10 pull-right" id="btn-reset" onclick="on_clear();">Clear <i class="mdi  mdi-delete fa-fw"></i></button>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
                <div class="row">
                <div class="col-md-12">
                        <div class="white-box">
                            <h3 class="box-title"><?php echo $mtitle; ?></h3>
                            <div class="scrollable">
                                <div class="table-responsive">
                                    <table id="mytable" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                    <thead>
                                     
                                        <tr>
                                            <th width="10%" style="text-align: center;">No Wil</th>
                                            <th width="30%" style="text-align: center;">Nama Wilayah</th>
                                            <th width="15%" style="text-align: center;">Pengambilan</th>
                                            <th width="15%" style="text-align: center;">Rusak</th>
                                            <th width="15%" style="text-align: center;">Pengembalian</th>
                                            <th width="15%" style="text-align: center;">Total</th>
                                        </tr>
                                       
                                    </thead>
                                    <tbody id="show_data">
                                       <?php
                                        if (!empty($data)){
                                           foreach($data as $row){?>
                                             <tr>
                                                <td width="10%" style="text-align: center;"><?php echo $row->no_wil ;?></td>
                                                <td width="30%" style="text-align: center;"><?php echo $row->nama_wil ;?></td>
                                                <td width="15%" style="text-align: right;"><?php echo $row->ambil ;?></td>
                                                <td width="15%" style="text-align: right;"><?php echo $row->rusak ;?></td>
                                                <td width="15%" style="text-align: right;"><?php echo $row->pengembalian ;?></td>
                                                <td width="15%" style="text-align: right;"><?php echo $row->total ;?></td>
                                            </tr>
                                          <?php } ?>
                                          <?php } ?>
                                       
                                        
                                    </tbody>
                                    
                                        <tfoot id="my_foot">
                                            <tr>
                                            <?php if (!empty($jumlah)){ ?>
                                            <th width="40%" colspan="2" style="text-align: center;">Jumlah</th>
                                            <th width="15%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->ambil),0,',','.');?></th>
                                            <th width="15%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->rusak),0,',','.');?></th>
                                            <th width="15%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->pengembalian),0,',','.');?></th>
                                            <th width="15%" style="text-align: right;"><?php echo number_format(htmlentities($jumlah[0]->total),0,',','.');?></th>
                                            <?php } ?>
                                        </tr>

                                            </tfoot>
                                </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                 </div>
       @include('shared.footer_detail')