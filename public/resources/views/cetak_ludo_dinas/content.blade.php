 <style type="text/css">
     textarea {
  resize: none;
  height: 155px !important;
}
 </style>
 <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $stitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                        <ol class="breadcrumb">
                            <li class="active"><?php echo $mtitle; ?></li>
                        </ol>
                    </div>
                </div>
                 <div class="row">
                <!-- <form name ="get_form" action="<?php echo $my_url; ?>" method="post"> -->
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="white-box">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="row">
                                <div class="col-lg-6">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_nik" type="checkbox" checked disabled="true">
                                            <label for="cb_nik"> Jumlah Cetak</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">         
                                    <div class="input-group">
                                        <input class="form-control" id="txt_jml" type="text" maxlength="16" onkeypress="return isNumberKey(event)" ></div>
                                </div>
                            </div>
                          
                                </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                               
                                <div class="row">
                                <div class="col-lg-6">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_tgl" type="checkbox" checked disabled="true">
                                            <label for="cb_tgl"> Tanggal Cetak</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6">         
                                    <div class="form-group">
                                        <input class="form-control" id="tanggal" placeholder="dd/mm/yyyy" type="text" value="<?php echo date("d-m-Y") ?>" readonly></div>
                                </div>
                                </div>
                            </div>
                            <div class="row">
                                <button type="submit" id="btn_dosave" class="btn btn-info waves-effect waves-light m-r-10 pull-right" id="btn-filter" onclick="on_save();" >Save <i class="mdi  mdi-content-save fa-fw"></i></button>
                                  <button type="button" class="btn btn-invert waves-effect waves-light m-r-10 pull-right" id="btn-reset" onclick="on_clear();">Clear <i class="mdi  mdi-delete fa-fw"></i></button>
                            </div>
                        
                        </div>
                    </div>
                    <!-- </form> -->
                
                </div>
              
                 </div>
       @include('shared.footer_detail')