     <script>
        function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode != 44 && charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }
     $(document).bind('keypress', function(e) {
            if(e.keyCode==13){
                 $('#btn-search').trigger('click');
             }
        });
        $(document).ready(function() {
            $('#antrian-list').DataTable({
                 "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                "pageLength" : 10,
                "ajax": {
                 "url": BASE_URL+"Antrian/get_cek_antrian_biodata",
                "type": "post",
                "data": {
                  "_token": "{{ csrf_token() }}",
                    "nik":  $('#antrian_nik').val()
                }
                }
            });

    });
    function get_table() {
        if (validationdaily()){
        block_screen();
        $('#antrian-list').DataTable({
                destroy: true,
                 "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                 'columnDefs': [
                  {
                      "targets": 0, // your case first column
                      "className": "text-center",
                 },
                 {
                      "targets": 1,
                      "className": "text-center",
                 },
                 {
                      "targets": 2,
                      "className": "text-center",
                 },
                 {
                      "targets": 3,
                      "className": "text-center",
                 },
                 {
                      "targets": 4,
                      "className": "text-center",
                 },
                 {
                      "targets": 5,
                      "className": "text-center",
                 },
                 {
                      "targets": 6,
                      "className": "text-center",
                 }],
                "pageLength" : 50,
                "ajax": {
                 "url": BASE_URL+"Antrian/get_cek_antrian_biodata",
                "type": "post",
                "data": {
                  "_token": "{{ csrf_token() }}",
                    "nik":  $('#antrian_nik').val()
                },
                complete: function() {
                    cek_exists();
                }
                }
            });
        
        unblock_screen();
        }
    }
    function cek_exists(){
        var totalRecords =  $("#antrian-list").DataTable().page.info().recordsTotal;
        if (totalRecords ==0){
            if ($('#antrian_nik').val().length > 0){
        swal("Warning!", "Nik : "+$('#antrian_nik').val()+" \n Tidak Ditemukan !", "warning");  
            }
        }
        $('#antrian_nik').val("");
    }
    function validationdaily() {
        var nik = $("#antrian_nik");
            if (nik.val().length == 0 ) {                
                  swal("Warning!", "Nik Tidak Boleh Kosong !", "warning");  
                  on_clear();
                 return false;
            }
            return true;
       
        }
        function on_insert(nik) {
            $.ajax({
                    type: "post",
                    url: BASE_URL+"Antrian/do_insert_biodata",
                    dataType: "json",
                    data: {
                      "_token": "{{ csrf_token() }}",
                        nik : nik
                    },
                    beforeSend:
                    function () {
                         block_screen();
                    },
                    success: function (data) {
                        console.log(data);
                        swal("Success!", data.message, "success");
                    },
                    error:
                    function (data) {
                        unblock_screen();
                        swal("Error!", "Ooops!, Please Try Again, Something Went Wrong", "error"); 

                    },
                    complete:
                    function (response) {
                        unblock_screen();
                        on_clear();
                    }
                });
        }
    function on_clear() {
        $('#antrian_nik').val("");
        $('#antrian-list').DataTable({
                destroy: true,
                 "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                "pageLength" : 50,
                "ajax": {
                 "url": BASE_URL+"Antrian/get_cek_antrian_biodata",
                "type": "post",
                "data": {
                  "_token": "{{ csrf_token() }}",
                    "nik":  $('#antrian_nik').val()
                }
                }
            });
    }
    function block_screen(){
        $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff'
            },
            baseZ: 2000
        }); 
    }
    function unblock_screen(){
       $.unblockUI();
    }
    jQuery(document).ready(function() {
        $(".select2").select2();
        $('#tanggal').val("<?php echo date('d-m-Y');?>");
        });
    </script>