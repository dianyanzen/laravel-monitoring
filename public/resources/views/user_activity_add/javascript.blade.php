     <script>
        function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }
        function on_add_activityid() {
            if (validationdaily()){
                do_save();
            }
           
        }
        function on_back() {
            block_screen();
            var url = "{{ url('/') }}/";
            url += "Setting/ActivityId";
            var win = window.location.replace(url);
            win.focus();
        }
        function validationdaily() {
        var activity_nm = $("#activity_nm");
            if (activity_nm.val().length == 0) {                
                  swal("Warning!", "activity Name Cannot Be Empty !", "warning");  
                 return false;
            }
        var kdgroup = $("#kdgroup");
            if (kdgroup.val() == 0) {                
                  swal("Warning!", "Please Select The Group activity !", "warning");  
                 return false;
            }

            return true;
        }
        function do_save(){
            $.ajax({
                    type: "post",
                    url: BASE_URL+"Setting/save_activity",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        activity_nm : $("#activity_nm").val(),
                        kdgroup : $("#kdgroup").val()

                    },
                    beforeSend:
                    function () {
                         block_screen();
                    },
                    success: function (data) {
                        console.log(data);
                        swal("Success!", data.message, "success");
                    },
                    error:
                    function (data) {
                        unblock_screen();
                        swal("Error!", "Ooops!, Please Try Again, Something Went Wrong", "error"); 

                    },
                    complete:
                    function (response) {
                        unblock_screen();
                        on_clear();
                    }
                });
        }
        $(document).ready(function() {
           
       $.ajax({
                    type: "post",
                    url: BASE_URL+"Setting/get_group_user",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                    },
                    beforeSend:
                    function () {
                        $('select[name="kdgroup"]').attr("disabled",true);
                    },
                    success: function (data) {
                        console.log(data);
                       $('select[name="kdgroup"]').empty();
                       $('select[name="kdgroup"]').append('<option value="0">-- Select Group --</option>');
                        $.each(data, function(key, value) {
                            $('select[name="kdgroup"]').append('<option value="'+ value.user_level +'">'+ value.level_name +'</option>');
                        });
                        $('select[name="kdgroup"]').val("0").trigger("change");
                        
                    },
                    error:
                    function (data) {
               

                    },
                    complete:
                    function (response) {
                        $('select[name="kdgroup"]').attr("disabled",false);
                    }
                });
    });
    
    jQuery(document).ready(function() {
        $(".select2").select2();
        $('#tanggal').val("<?php echo date('d-m-Y');?>");
        });
    function block_screen(){
        
        $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff'
            },
            baseZ: 2000
        }); 
    }
    function unblock_screen(){
       $.unblockUI();
    }
    function on_clear() {
        $('#activity_nm').val("");
        $('select[name="kdgroup"]').val(0).trigger("change");
        $('#activity_nm').focus();
    }
    </script>