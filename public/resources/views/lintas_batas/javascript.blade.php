     <script>
        function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }
    $("#txt_receive").keydown(function (e) {
            if (e.keyCode == 13) {
             e.preventDefault();
             on_save();
            }
            
          });
    $(document).ready(function() {
        console.log(init_kec);
        console.log(init_level);
        $('input[type=text]').on('input', function(evt) {
            $(this).val(function(_, val) {
            return val.toUpperCase();
            });
        });
        $('#txt_nik').on('keyup',function(){
              var my_txt = $(this).val();
              var len = my_txt.length;
              if(len == 16)
              {
                  get_nik_paste();
                  
              }
        });
    });
    function get_nik_paste(){
         var nik = $("#txt_nik");
         if (nik.val().length != 16) {                
                  swal("Warning!", "Input Nik Harus 16 Digit !", "warning");  
          }else{
                get_nik();
          }
    }
    function get_nik(){
        $.ajax({
                    type: "post",
                    url: BASE_URL+"Plb/get_nik",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        nik : $("#txt_nik").val()
                    },
                    beforeSend:
                    function () {
                       block_screen();
                    },
                    success: function (data) {
                        console.log(data);
                        if (data.is_exists == 1){ 
                        $('#txt_nama').val(data.nama);
                        $('#txt_tmpt_lhr').val(data.tmpt_lhr);
                        $('#txt_tgl_lhr').val(data.tgl_lhr);
                        $('#txt_umur').val(data.umur);
                        $('#txt_jenis_klmin').val(data.jenis_klmin);
                        $('#txt_jenis_pkrjn').val(data.jenis_pkrjn);
                        $('#txt_alamat').val(data.alamat);
                        $('#txt_rt').val(data.rt);
                        $('#txt_rw').val(data.rw);
                        $('select[name="no_prop"]').empty();
                        $('select[name="no_prop"]').append('<option value="'+ data.no_prop +'">'+ data.nama_prop +'</option>');
                        $('select[name="no_prop"]').val(data.no_prop).trigger("change");

                        $('select[name="no_kab"]').empty();
                        $('select[name="no_kab"]').append('<option value="'+ data.no_kab +'">'+ data.nama_kab +'</option>');
                        $('select[name="no_kab"]').val(data.no_kab).trigger("change");

                        $('select[name="no_kec"]').empty();
                        $('select[name="no_kec"]').append('<option value="'+ data.no_kec +'">'+ data.nama_kec +'</option>');
                        $('select[name="no_kec"]').val(data.no_kec).trigger("change");

                        $('select[name="no_kel"]').empty();
                        $('select[name="no_kel"]').append('<option value="'+ data.no_kel +'">'+ data.nama_kel +'</option>');
                        $('select[name="no_kel"]').val(data.no_kel).trigger("change");
                        $('#txt_plb').focus();
                        }else{
                            on_clear();
                            swal("Warning!", "Data Tidak Ditemukan !", "warning");
                        }
                        
                    },
                    error:
                    function (data) {
                        unblock_screen();
                        swal("Error!", "Ooops!, Please Try Again, Something Went Wrong", "error"); 

                    },
                    complete:
                    function (response) {
                        unblock_screen();
                    }
                });
    }
    function on_clear() {
        
        
        $('#txt_plb').val("");
        $('#txt_nik').val("");
        $('#txt_nama').val("");
        $("#txt_nama").attr("readonly", true);
        $('#txt_tmpt_lhr').val("");
        $("#txt_tmpt_lhr").attr("readonly", true);
        $('#txt_tgl_lhr').val("");
        $("#txt_tgl_lhr").attr("readonly", true);
        $('#txt_umur').val("");
        $('#txt_jenis_klmin').val("");
        $("#txt_jenis_klmin").attr("readonly", true);
        $('#txt_jenis_pkrjn').val("");
        $("#txt_jenis_pkrjn").attr("readonly", true);

        $('select[name="no_prop"]').empty();
        $('select[name="no_prop"]').append('<option value="0">-- Pilih Provinsi --</option>');
        $('select[name="no_prop"]').val(0).trigger("change");

        $('select[name="no_kab"]').empty();
        $('select[name="no_kab"]').append('<option value="0">-- Pilih Kabupaten --</option>');
        $('select[name="no_kab"]').val(0).trigger("change");

        $('select[name="no_kec"]').empty();
        $('select[name="no_kec"]').append('<option value="0">-- Pilih Kecamatan --</option>');
        $('select[name="no_kec"]').val(0).trigger("change");

        $('select[name="no_kel"]').empty();
        $('select[name="no_kel"]').append('<option value="0">-- Pilih Kelurahan --</option>');
        $('select[name="no_kel"]').val(0).trigger("change");

        $('#txt_alamat').val("");
        $('#txt_rt').val("");
        $('#txt_rw').val("");
        $('#txt_plb').focus();
    }
     function on_save(){
        if (validationdaily()){
          do_save();
          // alert("Suceess");
        }
  
        
    }
    function validationdaily() {
        var plb = $("#txt_plb");
        var nik = $("#txt_nik");
        var nama = $("#txt_nama");
        console.log(nik);
            if (nik.val().length != 16) {                
                  swal("Warning!", "Input Nik Harus 16 Digit !", "warning");  
                 return false;
            }
            if (nama.val().length == 0) {                
                  swal("Warning!", "Nama Tidak Boleh Kosong !", "warning");  
                 return false;
            }
            if (plb.val().length == 0) {                
                  swal("Warning!", "Nomor Plb Tidak Boleh Kosong !", "warning");  
                 return false;
            }
           
          
            return true;
       
        }
        function do_save(){
            $.ajax({
                    type: "post",
                    url: BASE_URL+"Plb/do_save",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        plb : $("#txt_plb").val(),
                        nik : $("#txt_nik").val(),
                        nama : $("#txt_nama").val(),
                        umur : $("#txt_umur").val(),
                        tgl_lhr : $("#txt_tgl_lhr").val(),
                        tmpt_lhr : $("#txt_tmpt_lhr").val(),
                        jenis_klmin : $("#txt_jenis_klmin").val(),
                        jenis_pkrjn : $("#txt_jenis_pkrjn").val(),
                        no_prop : $('select[name="no_prop"]').val(),
                        no_kab : $('select[name="no_kab"]').val(),
                        no_kec : $('select[name="no_kec"]').val(),
                        no_kel : $('select[name="no_kel"]').val(),
                        alamat : $("#txt_alamat").val(),
                        rt : $("#txt_rt").val(),
                        rw : $("#txt_rw").val(),
                    },
                    beforeSend:
                    function () {
                         block_screen();
                    },
                    success: function (data) {
                        console.log(data);
                        if (data.success){ 
                        swal("Success!", data.message, "success");
                        }else{
                        swal("Warning!", data.message, "warning");
                        }
                    },
                    error:
                    function (data) {
                        unblock_screen();
                        swal("Error!", "Ooops!, Please Try Again, Something Went Wrong", "error"); 

                    },
                    complete:
                    function (response) {
                        unblock_screen();
                        on_clear();
                    }
                });
        }
    function block_screen(){
        
        $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff'
            },
            baseZ: 2000
        }); 
    }
    function unblock_screen(){
       $.unblockUI();
    }
   jQuery(document).ready(function() {
            $(".select2").select2();
             $('#mytable').DataTable({
                 "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
           $('#tanggal').val("<?php echo date('d-m-Y');?>");
        });
    </script>
    </script>
   