     <script>
        function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode != 44 && charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }
    
    $(document).ready(function() {
        console.log(init_kec);
        console.log(init_level);
    });
  
    function block_screen(){
        
        $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff'
            },
            baseZ: 2000
        }); 
    }
    function on_clear(){
        $('select[name="kd_brg"]').val("0").trigger("change");
            $('#tanggal').val("<?php echo date('d-m-Y');?> - <?php echo date('d-m-Y');?>");
            get_table();
    }
    function on_serach(){
            get_table();
    }
    function unblock_screen(){
       $.unblockUI();
    }
    jQuery(document).ready(function() {
         $.ajax({
                    type: "post",
                    url: BASE_URL+"Barang/get_barang",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                    },
                    beforeSend:
                    function () {
                        $('select[name="kd_brg"]').attr("disabled",true);
                    },
                    success: function (data) {
                        console.log(data);
                       $('select[name="kd_brg"]').empty();
                       $('select[name="kd_brg"]').append('<option value="0">-- Pilih Barang --</option>');
                        $.each(data, function(key, value) {
                            $('select[name="kd_brg"]').append('<option value="'+ value.barang_id +'">'+ value.jenis_barang +'</option>');
                        });
                        $('select[name="kd_brg"]').val("0").trigger("change");
                        
                    },
                    error:
                    function (data) {
               

                    },
                    complete:
                    function (response) {
                        $('select[name="kd_brg"]').attr("disabled",false);
                    }
                });
            $(".select2").select2();
            $('.input-daterange-datepicker').daterangepicker({
                buttonClasses: ['btn', 'btn-sm'],
                applyClass: 'btn-info',
                cancelClass: 'btn-info',
                locale: {
                    format: 'DD-MM-YYYY'
                },
                startDate: '<?php echo date('d-m-Y');?>',
                endDate: '<?php echo date('d-m-Y');?>'
            });
             $('#list_barang').DataTable({
                 "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
            $('#harga_satuan').val("0");
            $('#txt_ket').val("");
            $('#tanggal').val("<?php echo date('d-m-Y');?> - <?php echo date('d-m-Y');?>");
            get_table();
        });
    function get_table() {
        $('#list_barang').DataTable({
                destroy: true,
                 "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                 "columnDefs": [
                    {"className": "dt-center", "targets": "_all"}
                  ],
                "pageLength" : 50,
                "ajax": {
                 "url": BASE_URL+"Barang/get_master_barang_laporan",
                "type": "post",
                "data": {
                    "_token": "{{ csrf_token() }}",
                    tanggal : $('#tanggal').val(),
                    barang : $('#kd_brg').val()
                }
                }
            });
    }
    </script>
   