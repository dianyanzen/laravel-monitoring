<!DOCTYPE html>
<html lang="en">
@include('shared.head')
<body class="fix-header">
    <div id="wrapper">
          @include('shared.nav_top')
          @include('shared.nav_side')
          @include('dashboard_cetak_ktpel.content')
    </div>
    @include('shared.footer')
    @include('dashboard_cetak_ktpel.javascript')
</body>
</html>