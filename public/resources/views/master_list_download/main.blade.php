<!DOCTYPE html>
<html lang="en">
@include('shared.head')
<body class="fix-header">
    <div id="wrapper">
            @include('shared.nav_top')
            @include('shared/nav_side')
            @include('master_list_download/content')
    </div>
    @include('shared/footer')
   @include('master_list_download/javascript')
</body>
</html>