<div id="page-wrapper">
            <div class="container-fluid">
               <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $stitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                
                        <ol class="breadcrumb">
                            <li><a href="{{ url('/') }}/<?php echo $backurl; ?>"><?php echo $back_title; ?></a></li>
                           <li class="text-info"><?php echo $stitle; ?></li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- .row -->
            
                <div class="row">
            <div class="col-sm-12">
                        <div class="white-box">
                            <h3 class="box-title m-b-0"><?php echo $stabletitle; ?></h3>
                            <p class="text-muted m-b-30">Dinas Kependudukan Dan Pencatatan Sipil Kota Bandung</p>
                            <div class="table-responsive">
                                 <table id="mytable" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                   <thead>
                                         <tr>
                                            <th width="15%" style="text-align: center;">No Kec</th>
                                            <th width="30%" style="text-align: center;">Nama Kecamatan</th>
                                            <th width="15%" style="text-align: center;">Pengajuan</th>
                                            <th width="15%" style="text-align: center;">Perpindahan</th>
                                            <th width="15%" style="text-align: center;">Prr</th>
                                        </tr>
                                    </thead>
                                    <tbody id="show_data">
                                        <?php 
                                            $i = 0;?>
                                          <?php foreach($data as $row){
                                            $i++;
                                            ?>
                                             <tr>
                                                <td width="15%" style="text-align: center;"><?php echo $i ;?></td>
                                                <td width="30%" style="text-align: center;"><?php echo $row->nama_kantor ;?></td>
                                                <td width="15%" style="text-align: center;"><?php echo $row->pengajuan_s ;?></td>
                                                <td width="15%" style="text-align: center;"><?php echo $row->pindahan_s ;?></td>
                                                <td width="15%" style="text-align: center;"><?php echo $row->prr_s ;?></td>
                                            </tr>
                                         <?php } ?>
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
           @include('shared.footer_detail')
        </div>