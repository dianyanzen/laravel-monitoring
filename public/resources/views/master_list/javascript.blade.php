    <script src="{{ url('/') }}/assets/js/gijgo.min.js"></script>
    <link href="{{ url('/') }}/assets/css/gijgo.min.css" rel="stylesheet">
    <script src="{{ url('/') }}/assets/plugins/bower_components/owl.carousel/owl.carousel.min.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/owl.carousel/owl.custom.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/flot/jquery.flot.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/Minimal-Gauge-chart/js/cmGauge.js"></script>
    <script type="text/javascript">
    	function download_excel(){
         if (<?php echo Session::get('S_USER_LEVEL') ;?> == 5 || <?php echo Session::get('S_USER_LEVEL') ;?> == 1){
             $.blockUI({ css: { 
                    border: 'none', 
                    padding: '15px', 
                    backgroundColor: '#000', 
                    '-webkit-border-radius': '10px', 
                    '-moz-border-radius': '10px', 
                    opacity: .5, 
                    color: '#fff' 
                },
                baseZ: 2000
            }); 
            window.location.href = "{{ url('/') }}/Master/ListDownload";
        }
    }
    </script>
     <script>
   
    $('#mytable').DataTable({
         "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        dom: 'Bfrtip',
        "pageLength" : 50,
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
    </script>

   