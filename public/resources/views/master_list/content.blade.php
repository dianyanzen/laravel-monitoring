<div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $stitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                        <ol class="breadcrumb">
                           <li class="text-info"><?php echo $stitle; ?></li>
                        </ol>
                    </div>
                </div>
                <!-- .row -->
               <div class="row">

                <div class="row">
                    <div class="col-md-12 col-xs-12">
                        <div class="white-box">
                        <ul class="nav nav-tabs tabs customtab">
                                    <li class="active tab">
                                        <a href="#master_vw" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-user"></i></span> <span class="hidden-xs" style="color: blue !important">Master List View</span> </a> 
                                    </li>
                                    <li class="tab">
                                        <a href="#excel_vw" data-toggle="tab"> <span class="visible-xs"><i class="mdi mdi-file-excel"></i></span> <span class="hidden-xs" style="color: blue !important">Master List Excel</span> </a> 
                                    </li>
                                
                                   
                                </ul>
                        </div>
                            </div>
                            <div class="tab-content">
                                <div class="tab-pane active" id="master_vw">

                                <?php   if (!empty($data)){
                                        foreach($data as $row){?>
                                <div class="col-md-4 col-xs-12">
                                    <div class="white-box">
                                        <div class="user-bg"> <img width="100%" alt="user" src="{{ url('/') }}/assets/plugins/images/wallpaper2.jpg">
                                            <div class="overlay-box">
                                                 <div class="user-content">
                                                    <a href="javascript:void(0)"> <?php $filename = 'assets/upload/pp/'.$row->nik.'.jpg';
                                                        if (file_exists($filename)) { ?>
                                                        <img src="{{ url('/') }}/assets/upload/pp/<?php echo $row->nik; ?>.jpg"  class="img-circle thumb-lg" alt="user-img" >
                                                        <?php } else {?>
                                                        <img src="{{ url('/') }}/assets/plugins/images/calming-cat.gif" class="img-circle thumb-lg" alt="user-img">
                                                    <?php }?></a>
                                                    <h4 class="text-white"><?php echo $row->nama_lgkp; ?></h4>
                                                    <h5 class="text-white"><?php echo $row->nama_kantor; ?></h5>  
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        <div class="user-btm-box">
                                            <div class="col-md-4 col-sm-4 text-center">
                                                <p class="text-info"><b>Sisa<br>Pengajuan</b></p>
                                                <h1><?php echo $row->pengajuan_s; ?></h1> 
                                            </div>
                                            <div class="col-md-4 col-sm-4 text-center">
                                                <p class="text-info"><b>Sisa<br>Perpindahan</b></p>
                                                <h1><?php echo $row->pindahan_s; ?></h1> 
                                            </div>
                                            <div class="col-md-4 col-sm-4 text-center">
                                                <p class="text-info"><b>Sisa<br>Prr</b></p>
                                                <h1><?php echo $row->prr_s; ?></h1> 
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                                <?php } ?>
                                <?php } ?>
                            </div>
                                <div class="tab-pane" id="excel_vw">
                             <div class="col-md-12 col-xs-12">
                            <div class="white-box">
                            <h3 class="box-title m-b-0">Master List Excel</h3>
                            <p class="text-muted m-b-30">Dinas Kependudukan Dan Pencatatan Sipil Kota Bandung</p>
                                    <div class="table-responsive">
                               <table id="mytable" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                   <thead>
                                         <tr>
                                            <th width="15%" style="text-align: center;">No Kec</th>
                                            <th width="30%" style="text-align: center;">Nama Kecamatan</th>
                                            <th width="15%" style="text-align: center;">Pengajuan</th>
                                            <th width="15%" style="text-align: center;">Perpindahan</th>
                                            <th width="15%" style="text-align: center;">Prr</th>
                                        </tr>
                                    </thead>
                                    <tbody id="show_data">
                                        <?php 
                                            $i = 0;?>
                                          <?php foreach($data as $row){
                                            $i++;
                                            ?>
                                             <tr>
                                                <td width="15%" style="text-align: center;"><?php echo $i ;?></td>
                                                <td width="30%" style="text-align: center;"><?php echo $row->nama_kantor ;?></td>
                                                <td width="15%" style="text-align: center;"><?php echo $row->pengajuan_s ;?></td>
                                                <td width="15%" style="text-align: center;"><?php echo $row->pindahan_s ;?></td>
                                                <td width="15%" style="text-align: center;"><?php echo $row->prr_s ;?></td>
                                            </tr>
                                         <?php } ?>
                                        
                                    </tbody>
                                </table>
                            </div>
                                </div>
                                    
                            </div>
                </div>
                   
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- /.container-fluid -->
         @include('shared.footer_detail')