
    <script src="{{ url('/') }}/assets/plugins/bower_components/owl.carousel/owl.carousel.min.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/owl.carousel/owl.custom.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/flot/jquery.flot.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/Minimal-Gauge-chart/js/cmGauge.js"></script>
    <!-- Footable -->
    <script src="{{ url('/') }}/assets/plugins/bower_components/footable/js/footable.all.min.js"></script>
    
    <!--FooTable init-->
    <script src="{{ url('/') }}/assets/js/footable-init.js"></script>
     <script>
  
    $(document).ready(function() {
    $('#done_button').attr("disabled",true);
    console.log(init_kec);
     $.ajax({
                    type: "post",
                    url: BASE_URL+"Shared_api/gisa_get_one_kecamatan",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        no_kec : init_kec
                    },
                    beforeSend:
                    function () {
                        $('select[name="no_kec"]').attr("disabled",true);
                        $('select[name="no_kel"]').attr("disabled",true);
                    },
                    success: function (data) {
                        console.log(data);
                       $('select[name="no_kec"]').empty();
                       $('select[name="no_kec"]').append('<option value="0">-- Pilih Kecamatan --</option>');
                        $.each(data, function(key, value) {
                            $('select[name="no_kec"]').append('<option value="'+ value.no_kec +'">'+ value.nama_kec +'</option>');
                        });
                        $('select[name="no_kec"]').val(init_kec).trigger("change");

                        
                    },
                    error:
                    function (data) {
               

                    },
                    complete:
                    function (response) {
                        $('select[name="no_kec"]').attr("disabled",false);
                        $('select[name="no_kel"]').attr("disabled",false);
                        $('#done_button').attr("disabled",true);
                    }
                });
    $('select[name="no_kec"]').on('change', function() {

            var no_kec = $(this).val();

            if(no_kec != 0) {
                $('select[name="no_kel"]').empty();
                 $('select[name="no_kel"]').append('<option value="0">-- Pilih Kelurahan --</option>');
                 $('select[name="no_kel"]').val("0").trigger("change");
                $.ajax({
                    type: "post",
                    url: BASE_URL+"Shared_api/gisa_get_kelurahan",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        no_kec : no_kec
                    },
                    beforeSend:
                    function () {
                        $('select[name="no_kel"]').attr("disabled",true);
                    },
                    success: function (data) {
                        console.log(data);
                        $.each(data, function(key, value) {
                            $('select[name="no_kel"]').append('<option value="'+ value.no_kel +'">'+ value.nama_kel +'</option>');
                        });
                        
                    },
                    error:
                    function (data) {
               

                    },
                    complete:
                    function (response) {
                        $('select[name="no_kel"]').attr("disabled",false);
                        $('#done_button').attr("disabled",true);
                    }
                });
            }else{
                 $('select[name="no_kel"]').empty();
                 $('select[name="no_kel"]').append('<option value="0">-- Pilih Kelurahan --</option>');
                 $('select[name="no_kel"]').val("0").trigger("change");
            }

        });
    $('select[name="no_kel"]').on('change', function() {

            var no_kec = $('select[name="no_kec"]').val();
            var no_kel = $(this).val();

            if(no_kec != 0 && no_kel != 0) {
                $('select[name="no_rw"]').empty();
                 $('select[name="no_rw"]').append('<option value="0">-- Pilih No Rw --</option>');
                 $('select[name="no_rw"]').val("0").trigger("change");
                $.ajax({
                    type: "post",
                    url: BASE_URL+"Shared_api/gisa_get_rw",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        no_kec : no_kec,
                        no_kel : no_kel
                    },
                    beforeSend:
                    function () {
                        $('select[name="no_rw"]').attr("disabled",true);
                    },
                    success: function (data) {
                        console.log(data);
                        $.each(data, function(key, value) {
                            $('select[name="no_rw"]').append('<option value="'+ value.no_rw +'">'+ value.no_rw +'</option>');
                        });
                        
                    },
                    error:
                    function (data) {
               

                    },
                    complete:
                    function (response) {
                        $('select[name="no_rw"]').attr("disabled",false);
                        $('#done_button').attr("disabled",true);
                    }
                });
            }else{
                 $('select[name="no_rw"]').empty();
                 $('select[name="no_rw"]').append('<option value="0">-- Pilih No Rw --</option>');
                 $('select[name="no_rw"]').val("0").trigger("change");
            }

        });
     $('select[name="no_rw"]').on('change', function() {

            var no_kec = $('select[name="no_kec"]').val();
            var no_kel = $('select[name="no_kel"]').val();
            var no_rw = $(this).val();

            if(no_kec != 0 && no_kel != 0 && no_rw != 0) {
                $('select[name="no_rt"]').empty();
                 $('select[name="no_rt"]').append('<option value="0">-- Pilih No Rt --</option>');
                 $('select[name="no_rt"]').val("0").trigger("change");
                $.ajax({
                    type: "post",
                    url: BASE_URL+"Shared_api/gisa_get_rt",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        no_kec : no_kec,
                        no_kel : no_kel,
                        no_rw : no_rw
                    },
                    beforeSend:
                    function () {
                        $('select[name="no_rt"]').attr("disabled",true);
                    },
                    success: function (data) {
                        console.log(data);
                        $.each(data, function(key, value) {
                            $('select[name="no_rt"]').append('<option value="'+ value.no_rt +'">'+ value.no_rt +'</option>');
                        });
                        
                    },
                    error:
                    function (data) {
               

                    },
                    complete:
                    function (response) {
                        $('select[name="no_rt"]').attr("disabled",false);
                        $('#done_button').attr("disabled",true);
                    }
                });
            }else{
                 $('select[name="no_rt"]').empty();
                 $('select[name="no_rt"]').append('<option value="0">-- Pilih No Rt --</option>');
                 $('select[name="no_rt"]').val("0").trigger("change");
            }

        });
     $('select[name="no_rt"]').on('change', function() {

            var no_kec = $('select[name="no_kec"]').val();
            var no_kel = $('select[name="no_kel"]').val();
            var no_rw = $('select[name="no_rw"]').val();
            var no_rt = $(this).val();

            if(no_kec != 0 && no_kel != 0 && no_rw != 0 && no_rt != 0) {
                $('#done_button').attr("disabled",false);
            }else{
                $('#done_button').attr("disabled",true);
            }

        });

    });
    function on_clear() {
        
        $('select[name="no_kec"]').val(init_kec).trigger("change");
        $('select[name="no_kel"]').empty();
        $('select[name="no_kel"]').append('<option value="0">-- Pilih Kelurahan --</option>');
        $('select[name="no_kel"]').val("0").trigger("change");
        $('#cb_nik').attr('checked', false);
        $('#nik').attr("disabled",true);
        $('#nik').val("");
        $('#tanggal').val("<?php echo date('d-m-Y');?> - <?php echo date('d-m-Y');?>");
    }
    function on_search(){
            $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff'
            },
            baseZ: 2000
            }); 
      
    }
    function unblock_screen(){
       $.unblockUI();
    }

    jQuery(document).ready(function() {
            $(".select2").select2();
        });
    function openInNewTab(url) {
        var win = window.open(url, '_blank');
        win.focus();
    }
    function openTab(url) {
        var win = window.location.replace(url);
        win.focus();

    }
    function openUrl(url) {
        window.open(url);
        
    }
    </script>
   