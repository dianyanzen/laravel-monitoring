     <script>
        function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode != 44 && charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }
        function edit(data) {
            block_screen();
            var url = "{{ url('/') }}/";
            url += "Setting/ActivityId/Edit/";
            url += data;
            var win = window.location.replace(url);
            win.focus();
        }
        function hapus(activity_id) {
           
            swal({   
            title: "Apakah Anda Yakin ?",   
            text: "Menghapus Activity Ini ?",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Ya, Hapus activity",   
            cancelButtonText: "Tidak, Jangan Dihapus",   
            closeOnConfirm: false,   
            closeOnCancel: false 
            }, function(isConfirm){   
                if (isConfirm) {
                $.ajax({
                    type: "post",
                    url: BASE_URL+"Setting/delete_activity_id",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        activity_id : activity_id
                    },
                    beforeSend:
                    function () {
                         block_screen();
                    },
                    success: function (data) {
                        console.log(data);
                         if (data.message_type > 0){
                              swal("Berhasil !", "Activity Ini Telah Dihapus !", "success");
                        }else{
                            swal("Warning!", data.message, "warning");  
                        }
                    },
                    error:
                    function (data) {
                        unblock_screen();
                        swal("Error!", "Ooops!, Please Try Again, Something Went Wrong", "error"); 

                    },
                    complete:
                    function (response) {
                        unblock_screen();
                        get_select_table();
                    }
                });
                     
                } else {     
                    swal("Cancelled", "Activity Ini Tidak Jadi Dihapus :)", "error");   
                } 
            });
        }
        function on_add_activity() {
            block_screen();
            var url = "{{ url('/') }}/";
            url += "Setting/ActivityId/Add";
            var win = window.location.replace(url);
            win.focus();
        }
    
        $(document).ready(function() {
           
        $.ajax({
                    type: "post",
                    url: BASE_URL+"Setting/get_group_user",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                    },
                    beforeSend:
                    function () {
                        $('select[name="kdgroup"]').attr("disabled",true);
                    },
                    success: function (data) {
                        console.log(data);
                       $('select[name="kdgroup"]').empty();
                       $('select[name="kdgroup"]').append('<option value="0">-- Select Group --</option>');
                        $.each(data, function(key, value) {
                            $('select[name="kdgroup"]').append('<option value="'+ value.user_level +'">'+ value.level_name +'</option>');
                        });
                        $('select[name="kdgroup"]').val("0").trigger("change");
                        
                    },
                    error:
                    function (data) {
               

                    },
                    complete:
                    function (response) {
                        $('select[name="kdgroup"]').attr("disabled",false);
                    }
                });
        $('select[name="kdgroup"]').on('change', function() {

            var kdgroup = $(this).val();;
            get_table(kdgroup);
        });

    });
    function get_table(kdgroup) {
        console.log(kdgroup);
        $('#group-list').DataTable({
                destroy: true,
                 "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                 "columnDefs": [
                    {"className": "dt-center", "targets": "_all"}
                  ],
                "pageLength" : 50,
                "ajax": {
                 "url": BASE_URL+"Setting/get_user_activity",
                "type": "post",
                "data": {
                "_token": "{{ csrf_token() }}",
                "group_id":  kdgroup
                }
                }
            });
    }
    
    jQuery(document).ready(function() {
        $(".select2").select2();
        $('#tanggal').val("<?php echo date('d-m-Y');?>");
        });
     function block_screen(){
        
        $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff'
            },
            baseZ: 2000
        }); 
    }
    function unblock_screen(){
       $.unblockUI();
    }
    </script>