    <script src="{{ url('/') }}/assets/js/gijgo.min.js"></script>
    <link href="{{ url('/') }}/assets/css/gijgo.min.css" rel="stylesheet">
    
     <script>
      
         function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }
        function on_back() {
            block_screen();
            var url = "{{ url('/') }}/";
            url += "Setting/User";
            var win = window.location.replace(url);
            win.focus();
        }
   
      function block_screen(){
        
        $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff'
            },
            baseZ: 2000
        }); 
    }
    function unblock_screen(){
       $.unblockUI();
    }

    </script>