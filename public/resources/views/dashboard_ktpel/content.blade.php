<div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $stitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                        <ol class="breadcrumb">
                             <li><a href="{{ url('/') }}/<?php echo $backurl; ?>"><?php echo $back_title; ?></a></li>
                           <li class="text-info"><?php echo $stitle; ?></li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- ============================================================== -->
                <!-- Sales, finance & Expance widgets -->
                <!-- ============================================================== -->
                <!-- .row -->
                <div class="row">
                    <div class="col-lg-6 col-sm-12 col-xs-12">
                        <div class="row">
                            <div class="col-lg-12 col-sm-12 col-xs-12">
                                <div class="panel panel-info">
                                    <div class="panel-heading">
                                       Pencetakan Hari Ini
                                        <div class="pull-right"><a href="#" data-perform="panel-collapse"><i class="ti-minus"></i></a></div>
                                    </div>
                                    <div class="panel-wrapper collapse in" aria-expanded="true">
                                        <div class="panel-body">
                                            <ul class="list-inline two-part">
                                                <li><i class="icon-credit-card text-info"></i></li>
                                                <li class="text-right"><span class="counter"><a href ="#" onclick="get_detail_cetak(0);"><span id="cetak_today">0</span></a></span></li>
                                                <!-- <li class="text-right"><span class="counter"><a href ="#"><span id="cetak_today">0</span></a></span></li> -->
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-sm-12 col-xs-12">
                                <div class="panel panel-info">
                                    <div class="panel-heading">
                                        Perekaman Hari Ini
                                        <div class="pull-right"><a href="#" data-perform="panel-collapse"><i class="ti-minus"></i></a></div>
                                         </div>
                                    <div class="panel-wrapper collapse in" aria-expanded="true">
                                        <div class="panel-body">
                                    <ul class="list-inline two-part">
                                        <li><i class="icon-camera  text-info"></i></li>
                                        <li class="text-right"><span class="counter"><a href ="#" onclick="get_detail_rekam(0);"><span id="rekam_today">0</span></a></span></li>
                                    </ul>
                                    <ul class="feeds">
                                        <li>
                                            <div class="bg-info"><i class="mdi mdi-account-card-details text-white"></i></div><a href ="#" onclick="get_stat_cp(0);" style="color: #292961 !important;"> Card Printed <span id="cp_today" style="float: none; font-size: auto;">0</span></a></li>
                                        <li>
                                            <div class="bg-info"><i class="mdi mdi-fingerprint text-white"></i></div><a href ="#" onclick="get_stat_bio(0);" style="color: #292961 !important;"> Bio Captured <span id="bio_today" style="float: none; font-size: auto;">0</span></a></li>
                                        <li>
                                            <div class="bg-info"><i class="mdi mdi-sync text-white"></i></div><a href ="#" onclick="get_stat_proc(0);" style="color: #292961  !important;"> Processing <span id="process_today" style="float: none; font-size: auto;">0</span></a></li>
                                        <li>
                                            <div class="bg-info"><i class="mdi mdi-sync-alert text-white"></i></div><a href ="#" onclick="get_stat_adj(0);" style="color: #292961 !important;"> Adjudicate Record <span id="adjudicate_today" style="float: none; font-size: auto;">0</span></a></li>
                                        <li>
                                            <div class="bg-info"><i class="mdi mdi-sync-off text-white"></i></div><a href ="#" onclick="get_stat_efac(0);" style="color: #292961 !important;"> Enroll Failure At Central <span id="enroll_failure_today" style="float: none; font-size: auto;">0</span></a></li>
                                        <li>
                                            <div class="bg-info"><i class="mdi mdi-cloud-sync text-white"></i></div><a href ="#" onclick="get_stat_sfe(0);" style="color: #292961 !important;"> Sent For Enrollment <span id="sfe_today" style="float: none; font-size: auto;">0</span></a></li>
                                        <li>
                                            <div class="bg-info"><i class="mdi mdi-account-card-details text-white"></i></div><a href ="#" onclick="get_stat_prr(0);" style="color: #292961 !important;"> Print Ready Record <span id="prr_today" style="float: none; font-size: auto;">0</span></a></li>
                                        <li>
                                            <div class="bg-info"><i class="mdi mdi-account-switch text-white"></i></div><a href ="#" onclick="get_stat_durec(0);" style="color: #292961 !important;"> Duplicate Record <span id="durec_today" style="float: none; font-size: auto;">0</span></a></li>
                                    </ul>
                                    </div>
                                </div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-sm-12 col-xs-12">
                                <div class="panel panel-info">
                                    <div class="panel-heading">
                                       Kia Hari Ini
                                        <div class="pull-right"><a href="#" data-perform="panel-collapse"><i class="ti-minus"></i></a></div>
                                    </div>
                                    <div class="panel-wrapper collapse in" aria-expanded="true">
                                        <div class="panel-body">
                                            <ul class="list-inline two-part">
                                                <li><i class="icon-credit-card text-info"></i></li>
                                                <li class="text-right"><span class="counter"><a href ="#" onclick="get_detail_kia(0);"><span id="kia_today">0</span></a></span></li>
                                                <!-- <li class="text-right"><span class="counter"><a href ="#"><span id="cetak_today">0</span></a></span></li> -->
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-sm-12 col-xs-12">
                                <div class="panel panel-info">
                                    <div class="panel-heading">
                                        Suket Hari Ini
                                        <div class="pull-right"><a href="#" data-perform="panel-collapse"><i class="ti-minus"></i></a></div>
                                         </div>
                                    <div class="panel-wrapper collapse in" aria-expanded="true">
                                        <div class="panel-body">
                                    <ul class="list-inline two-part">
                                        <li><i class="icon-printer text-info"></i></li>
                                        <li class="text-right"><span class="counter"><a href ="#" onclick="get_detail_suket(0);"><span id="suket_today">0</span></a></span></li>
                                    </ul>
                                    <ul class="feeds">
                                        <li>
                                            <div class="bg-info"><i class="mdi mdi-crown text-white"></i></div><a href ="#" onclick="get_nrequest_jumlah(0);" style="color: #292961 !important;"> Cetak Dinas <span id="nreq_jumlah_today" style="float: none; font-size: auto;">0</span></a></li>
                                        <li>
                                            <div class="bg-info"><i class="mdi mdi-clock-alert text-white"></i></div><a href ="#" onclick="get_drequest_jumlah(0);" style="color: #292961 !important;"> Cetak Request Kecamatan <span id="dreq_jumlah_today" style="float: none; font-size: auto;">0</span></a></li>
                                        <!-- <li>
                                            <div class="bg-info"><i class="mdi mdi-cloud-print text-white"></i></div><a href ="#" onclick="get_request_jumlah(0);" style="color: #292961 !important;"> Total Request Kecamatan <span id="req_jumlah_today" style="float: none; font-size: auto;">0</span></a></li>
                                        <li>
                                            <div class="bg-info"><i class="mdi mdi-printer-settings text-white"></i></div><a href ="#" onclick="get_request_cetak(0);" style="color: #292961 !important;"> Request Tercetak <span id="req_cetak_today" style="float: none; font-size: auto;">0</span></a></li>
                                        <li>
                                            <div class="bg-info"><i class="mdi mdi-printer-alert text-white"></i></div><a href ="#" onclick="get_request_belum(0);" style="color: #292961 !important;"> Request Belum Tercetak <span id="req_belum_today" style="float: none; font-size: auto;">0</span></a></li> -->
                                    </ul>
                                </div>
                            </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-12 col-xs-12">
                        <div class="row">
                            <div class="col-lg-12 col-sm-12 col-xs-12">
                                <div class="panel panel-info">
                                    <div class="panel-heading">
                                        Pencetakan Kemarin
                                        <div class="pull-right"><a href="#" data-perform="panel-collapse"><i class="ti-minus"></i></a></div>
                                         </div>
                                    <div class="panel-wrapper collapse in" aria-expanded="true">
                                        <div class="panel-body">
                                    <ul class="list-inline two-part">
                                        <li><i class="icon-credit-card  text-info"></i></li>
                                        <li class="text-right"><span class="counter"><a href ="#" onclick="get_detail_cetak(1);"><span id="cetak_yesterday" >0</span></a></span></li>
                                        <!-- <li class="text-right"><span class="counter"><a href ="#"><span id="cetak_yesterday" >0</span></a></span></li> -->
                                    </ul>
                                     </div>
                                </div>
                                </div>
                            </div>
                           <div class="col-lg-12 col-sm-12 col-xs-12">
                                <div class="panel panel-info">
                                    <div class="panel-heading">
                                        Perekaman Kemarin
                                         <div class="pull-right"><a href="#" data-perform="panel-collapse"><i class="ti-minus"></i></a></div>
                                         </div>
                                    <div class="panel-wrapper collapse in" aria-expanded="true">
                                        <div class="panel-body">
                                    <ul class="list-inline two-part">
                                        <li><i class="icon-camera  text-info"></i></li>
                                        <li class="text-right"><span class="counter"><a href ="#" onclick="get_detail_rekam(1);"><span id="rekam_yesterday">0</span></a></span></li>
                                    </ul>
                                    <ul class="feeds">
                                         <li>
                                            <div class="bg-info"><i class="mdi mdi-account-card-details text-white"></i></div><a href ="#" onclick="get_stat_cp(1);" style="color: #292961 !important;"> Card Printed <span id="cp_yesterday" style="float: none; font-size: auto;">0</span></a></li>
                                        <li>
                                            <div class="bg-info"><i class="mdi mdi-fingerprint text-white"></i></div><a href ="#" onclick="get_stat_bio(1);" style="color: #292961 !important;"> Bio Captured <span id="bio_yesterday" style="float: none; font-size: auto;">0</span></a></li>
                                        <li>
                                            <div class="bg-info"><i class="mdi mdi-sync text-white"></i></div><a href ="#" onclick="get_stat_proc(1);" style="color: #292961  !important;"> Processing <span id="process_yesterday" style="float: none; font-size: auto;">0</span></a></li>
                                        <li>
                                            <div class="bg-info"><i class="mdi mdi-sync-alert text-white"></i></div><a href ="#" onclick="get_stat_adj(1);" style="color: #292961 !important;"> Adjudicate Record <span id="adjudicate_yesterday" style="float: none; font-size: auto;">0</span></a></li>
                                        <li>
                                            <div class="bg-info"><i class="mdi mdi-sync-off text-white"></i></div><a href ="#" onclick="get_stat_efac(1);" style="color: #292961 !important;"> Enroll Failure At Central <span id="enroll_failure_yesterday" style="float: none; font-size: auto;">0</span></a></li>
                                        <li>
                                            <div class="bg-info"><i class="mdi mdi-cloud-sync text-white"></i></div><a href ="#" onclick="get_stat_sfe(1);" style="color: #292961 !important;"> Sent For Enrollment <span id="sfe_yesterday" style="float: none; font-size: auto;">0</span></a></li>
                                        <li>
                                            <div class="bg-info"><i class="mdi mdi-account-card-details text-white"></i></div><a href ="#" onclick="get_stat_prr(1);" style="color: #292961 !important;"> Print Ready Record <span id="prr_yesterday" style="float: none; font-size: auto;">0</span></a></li>
                                        <li>
                                            <div class="bg-info"><i class="mdi mdi-account-switch text-white"></i></div><a href ="#" onclick="get_stat_durec(1);" style="color: #292961 !important;"> Duplicate Record <span id="durec_yesterday" style="float: none; font-size: auto;">0</span></a></li>
                                    </ul>
                                     </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-sm-12 col-xs-12">
                                <div class="panel panel-info">
                                    <div class="panel-heading">
                                        Kia Kemarin
                                        <div class="pull-right"><a href="#" data-perform="panel-collapse"><i class="ti-minus"></i></a></div>
                                         </div>
                                    <div class="panel-wrapper collapse in" aria-expanded="true">
                                        <div class="panel-body">
                                    <ul class="list-inline two-part">
                                        <li><i class="icon-credit-card  text-info"></i></li>
                                        <li class="text-right"><span class="counter"><a href ="#" onclick="get_detail_kia(1);"><span id="kia_yesterday" >0</span></a></span></li>
                                        <!-- <li class="text-right"><span class="counter"><a href ="#"><span id="cetak_yesterday" >0</span></a></span></li> -->
                                    </ul>
                                     </div>
                                </div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-sm-12 col-xs-12">
                                <div class="panel panel-info">
                                    <div class="panel-heading">Suket Kemarin
                                        <div class="pull-right"><a href="#" data-perform="panel-collapse"><i class="ti-minus"></i></a></div>
                                         </div>
                                    <div class="panel-wrapper collapse in" aria-expanded="true">
                                        <div class="panel-body">
                                    <ul class="list-inline two-part">
                                        <li><i class="icon-printer  text-info"></i></li>
                                        <li class="text-right"><span class="counter"><a href ="#" onclick="get_detail_suket(1);"><span id="suket_yesterday">0</span></a></span></li>
                                    </ul>
                                    <ul class="feeds">
                                        <li>
                                            <div class="bg-info"><i class="mdi mdi-crown text-white"></i></div><a href ="#" onclick="get_nrequest_jumlah(1);" style="color: #292961 !important;"> Cetak Dinas <span id="nreq_jumlah_yesterday" style="float: none; font-size: auto;">0</span></a></li>
                                        <li>
                                            <div class="bg-info"><i class="mdi mdi-clock-alert text-white"></i></div><a href ="#" onclick="get_drequest_jumlah(1);" style="color: #292961 !important;"> Cetak Request Kecamatan <span id="dreq_jumlah_yesterday" style="float: none; font-size: auto;">0</span></a></li>
                                        <!-- <li>
                                            <div class="bg-info"><i class="mdi mdi-cloud-print text-white"></i></div><a href ="#" onclick="get_request_jumlah(1);" style="color: #292961 !important;"> Total Request Kecamatan <span id="req_jumlah_yesterday" style="float: none; font-size: auto;">0</span></a></li>
                                        <li>
                                            <div class="bg-info"><i class="mdi mdi-printer-settings text-white"></i></div><a href ="#" onclick="get_request_cetak(1);" style="color: #292961 !important;"> Request Tercetak <span id="req_cetak_yesterday" style="float: none; font-size: auto;">0</span></a></li>
                                        <li>
                                            <div class="bg-info"><i class="mdi mdi-printer-alert text-white"></i></div><a href ="#" onclick="get_request_belum(1);" style="color: #292961 !important;"> Request Belum Tercetak <span id="req_belum_yesterday" style="float: none; font-size: auto;">0</span></a></li> -->
                                    </ul>
                                     </div>
                                </div>
                                </div>
                            </div>
                            
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- Rightsidebar -->
            <!-- ============================================================== -->
            <!-- .right-sidebar -->
          
        </div>
       @include('shared.footer_detail')