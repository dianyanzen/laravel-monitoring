    <script src="{{ url('/') }}/assets/plugins/bower_components/owl.carousel/owl.carousel.min.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/owl.carousel/owl.custom.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/flot/jquery.flot.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/Minimal-Gauge-chart/js/cmGauge.js"></script>
    <script type="text/javascript">
    $(document).ready(function () {
        get_data_ctk();
        get_data_rkm();
        get_data_suket();
        get_data_kia();
    });
    function get_detail_rekam(data){
        if(data == 0){
              $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff' 
            },
            baseZ: 2000
        }); 
            window.location.href = "{{ url('/') }}/Detail/gdert";
        }else{
              $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff' 
            },
            baseZ: 2000
        }); 
            window.location.href = "{{ url('/') }}/Detail/gdery";
        }
    }
    function get_detail_cetak(data){
        if(data == 0){
              $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff' 
            },
            baseZ: 2000
        }); 
            window.location.href = "{{ url('/') }}/Detail/gdect";
        }else{
              $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff' 
            },
            baseZ: 2000
        }); 
            window.location.href = "{{ url('/') }}/Detail/gdecy";
        }
    }
    function get_detail_suket(data){
        if(data == 0){
              $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff' 
            },
            baseZ: 2000
        }); 
            window.location.href = "{{ url('/') }}/Detail/gdest";
        }else{
              $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff' 
            },
            baseZ: 2000
        }); 
            window.location.href = "{{ url('/') }}/Detail/gdesy";
        }
    }
    function get_request_jumlah(data){
        if(data == 0){
              $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff' 
            },
            baseZ: 2000
        }); 
            window.location.href = "{{ url('/') }}/Detail/gdsjrt";
        }else{
              $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff' 
            },
            baseZ: 2000
        }); 
            window.location.href = "{{ url('/') }}/Detail/gdsjry";
        }
    }
    function get_nrequest_jumlah(data){
        if(data == 0){
              $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff' 
            },
            baseZ: 2000
        }); 
            window.location.href = "{{ url('/') }}/Detail/gdsjnrt";
        }else{
              $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff' 
            },
            baseZ: 2000
        }); 
            window.location.href = "{{ url('/') }}/Detail/gdsjnry";
        }
    }
    function get_drequest_jumlah(data){
        if(data == 0){
              $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff' 
            },
            baseZ: 2000
        }); 
            window.location.href = "{{ url('/') }}/Detail/gdsjdrt";
        }else{
              $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff' 
            },
            baseZ: 2000
        }); 
            window.location.href = "{{ url('/') }}/Detail/gdsjdry";
        }
    }
    function get_request_cetak(data){
        if(data == 0){
              $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff' 
            },
            baseZ: 2000
        }); 
            window.location.href = "{{ url('/') }}/Detail/gdsjct";
        }else{
              $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff' 
            },
            baseZ: 2000
        }); 
            window.location.href = "{{ url('/') }}/Detail/gdsjcy";
        }
    }
    function get_request_belum(data){
        if(data == 0){
              $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff' 
            },
            baseZ: 2000
        }); 
            window.location.href = "{{ url('/') }}/Detail/gdsjbt";
        }else{
              $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff' 
            },
            baseZ: 2000
        }); 
            window.location.href = "{{ url('/') }}/Detail/gdsjby";
        }
    }
    function get_stat_bio(data){
        if(data == 0){
              $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff' 
            },
            baseZ: 2000
        }); 
            window.location.href = "{{ url('/') }}/Detail/gdrsbiot";
        }else{
              $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff' 
            },
            baseZ: 2000
        }); 
            window.location.href = "{{ url('/') }}/Detail/gdrsbioy";
        }
    }
    function get_stat_proc(data){
        if(data == 0){
              $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff' 
            },
            baseZ: 2000
        }); 
            window.location.href = "{{ url('/') }}/Detail/gdrsproct";
        }else{
              $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff' 
            },
            baseZ: 2000
        }); 
            window.location.href = "{{ url('/') }}/Detail/gdrsprocy";
        }
    }
    function get_stat_adj(data){
        if(data == 0){
              $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff' 
            },
            baseZ: 2000
        }); 
            window.location.href = "{{ url('/') }}/Detail/gdrsadjt";
        }else{
              $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff' 
            },
            baseZ: 2000
        }); 
            window.location.href = "{{ url('/') }}/Detail/gdrsadjy";
        }
    }
    function get_stat_efac(data){
        if(data == 0){
              $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff' 
            },
            baseZ: 2000
        }); 
            window.location.href = "{{ url('/') }}/Detail/gdrsefact";
        }else{
              $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff' 
            },
            baseZ: 2000
        }); 
            window.location.href = "{{ url('/') }}/Detail/gdrsefacy";
        }
    }
    function get_stat_sfe(data){
        if(data == 0){
              $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff' 
            },
            baseZ: 2000
        }); 
            window.location.href = "{{ url('/') }}/Detail/gdrssfet";
        }else{
              $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff' 
            },
            baseZ: 2000
        }); 
            window.location.href = "{{ url('/') }}/Detail/gdrssfey";
        }
    }
    function get_stat_prr(data){
        if(data == 0){
              $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff' 
            },
            baseZ: 2000
        }); 
            window.location.href = "{{ url('/') }}/Detail/gdrsprrt";
        }else{
              $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff' 
            },
            baseZ: 2000
        }); 
            window.location.href = "{{ url('/') }}/Detail/gdrsprry";
        }
    }
    function get_stat_cp(data){
        if(data == 0){
              $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff' 
            },
            baseZ: 2000
        }); 
            window.location.href = "{{ url('/') }}/Detail/gdrscapt";
        }else{
              $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff' 
            },
            baseZ: 2000
        }); 
            window.location.href = "{{ url('/') }}/Detail/gdrscapy";
        }
    }
    function get_stat_durec(data){
        if(data == 0){
              $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff' 
            },
            baseZ: 2000
        }); 
            window.location.href = "{{ url('/') }}/Detail/gdrsdurect";
        }else{
              $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff' 
            },
            baseZ: 2000
        }); 
            window.location.href = "{{ url('/') }}/Detail/gdrsdurecy";
        }
    }


    function get_data_rkm() {
       $.ajax({
                type: "post",
                url: BASE_URL+"Dashboard_ktpel/get_data_rkm",
                data: {"_token": "{{ csrf_token() }}",},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                            $('#rekam_today').html(data.rekam_today);
                            $('#rekam_yesterday').html(data.rekam_yesterday);
                            $('#bio_today').html(data.bio_today);
                            $('#bio_yesterday').html(data.bio_yesterday);
                            $('#sfe_today').html(data.sfe_today);
                            $('#sfe_yesterday').html(data.sfe_yesterday);
                            $('#prr_today').html(data.prr_today);
                            $('#prr_yesterday').html(data.prr_yesterday);
                            $('#cp_today').html(data.other_today);
                            $('#cp_yesterday').html(data.other_yesterday);
                            $('#durec_today').html(data.durec_today);
                            $('#durec_yesterday').html(data.durec_yesterday);
                            $('#process_today').html(data.process_today);
                            $('#process_yesterday').html(data.process_yesterday);
                            $('#adjudicate_today').html(data.adjudicate_today);
                            $('#adjudicate_yesterday').html(data.adjudicate_yesterday);
                            $('#enroll_failure_today').html(data.enroll_failure_today);
                            $('#enroll_failure_yesterday').html(data.enroll_failure_yesterday);
                    
                },
                error:
                function (data) {


                },
                complete:
                function (response) {
                   
                   setTimeout(function () { get_data_rkm(); }, 5000);
                }
            });
        }
        function get_data_ctk() {
       $.ajax({
                type: "post",
                url: BASE_URL+"Dashboard_ktpel/get_data_ctk",
                data: {"_token": "{{ csrf_token() }}",},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                        $('#cetak_today').html(data.cetak_today);
                        $('#cetak_yesterday').html(data.cetak_yesterday);
                    
                },
                error:
                function (data) {


                },
                complete:
                function (response) {
                   
                   setTimeout(function () { get_data_ctk(); }, 5000);
                }
            });
        }
        function get_data_suket() {
       $.ajax({
                type: "post",
                url: BASE_URL+"Dashboard_ktpel/get_data_suket",
                data: {"_token": "{{ csrf_token() }}",},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                            $('#suket_today').html(data.suket_today);
                            $('#suket_yesterday').html(data.suket_yesterday);
                            $('#req_jumlah_today').html(data.req_jumlah_today);
                            $('#req_jumlah_yesterday').html(data.req_jumlah_yesterday);
                            $('#nreq_jumlah_today').html(data.nreq_jumlah_today);
                            $('#nreq_jumlah_yesterday').html(data.nreq_jumlah_yesterday);
                            $('#dreq_jumlah_today').html(data.dreq_jumlah_today);
                            $('#dreq_jumlah_yesterday').html(data.dreq_jumlah_yesterday);
                            $('#req_cetak_today').html(data.req_cetak_today);
                            $('#req_cetak_yesterday').html(data.req_cetak_yesterday);
                            $('#req_belum_today').html(data.req_belum_today);
                            $('#req_belum_yesterday').html(data.req_belum_yesterday);
                },
                error:
                function (data) {


                },
                complete:
                function (response) {
                   
                   setTimeout(function () { get_data_suket(); }, 5000);
                }
            });
        }
        function get_data_kia() {
       $.ajax({
                type: "post",
                url: BASE_URL+"Dashboard_ktpel/get_data_kia",
                data: {"_token": "{{ csrf_token() }}",},
                dataType: "json",
                beforeSend:
                function () {
                    
                },
                success: function (data) {
                            $('#kia_today').html(data.kia_today);
                            $('#kia_yesterday').html(data.kia_yesterday);
                },
                error:
                function (data) {


                },
                complete:
                function (response) {
                   
                   setTimeout(function () { get_data_suket(); }, 5000);
                }
            });
        }
    </script>