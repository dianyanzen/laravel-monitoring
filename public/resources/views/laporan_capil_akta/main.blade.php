<!DOCTYPE html>
<html lang="en">
@include('shared.head')
<body class="fix-header">
    <div id="wrapper">
            @include('shared.nav_top')
            @include('shared/nav_side')
            @include('laporan_capil_akta/content')
    </div>
     
    @include('shared/footer')
    
    @include('laporan_capil_akta/javascript')
</body>
</html>