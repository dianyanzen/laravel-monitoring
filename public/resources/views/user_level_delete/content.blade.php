 <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $stitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                        <ol class="breadcrumb">
                           <li class="text-info"><?php echo $stitle; ?></li>
                        </ol>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                        <div class="white-box">
                            <div class="row">
                                <div class="col-xs-6">
                                    <h2 class="font-medium m-t-0">INFORMASI USER LEVEL</h2>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                                    <h3 class="font-medium m-t-0">CODE : <span id="code_wil"><?php if (!empty($chead)){echo $chead[0]->user_level; }?></span></h3>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                                    <h3 class="font-medium m-t-0">GROUP NAME : <span id="group_name"><?php if (!empty($chead)){echo $chead[0]->level_name; }?></span></h3>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                                    <h3 class="font-medium m-t-0">GROUP LEVEL : <span id="group_level"><?php if (!empty($chead)){echo $chead[0]->group_name; }?></span></h3>
                                </div>
                            </div>
                            <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="panel panel-default">
                            <div class="panel-wrapper collapse in">
                                <table class="table table-hover">
                                   <tbody>
                                         <tr>
                                            <td><b style="font-weight:bold !important; color:#292961;">Delete This User Level, Will Be Impact To Delete All User Id In This Level !</b></td>
                                        </tr>
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                            </div>
                            <div class="row">

                                  <button type="button" class="btn btn-invert waves-effect waves-light m-r-10 pull-right" id="btn-back" onclick="on_back();">Back <i class="mdi  mdi-backspace fa-fw"></i></button>
                                  <button type="button" class="btn btn-info waves-effect waves-light m-r-10 pull-right" id="btn-deletelevel" onclick="do_delete(<?php if (!empty($chead)){echo $chead[0]->user_level; }?>);">Delete <i class="mdi  mdi-delete-forever fa-fw"></i></button>
                                  
                            </div>
                        </div>

                    </div>
                </div>
                <div class="row">
                <div class="col-md-12">
                        <div class="white-box">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="tabs.html#hak_akses" aria-controls="hak_akses" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="ti-home"></i></span><span class="hidden-xs"> Hak Akses</span></a></li>
                                <li role="presentation" class=""><a href="tabs.html#pengguna" aria-controls="pengguna" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="ti-user"></i></span> <span class="hidden-xs">Pengguna</span></a></li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="hak_akses">
                                    <div class="col-md-12">
                                        <div id="user_tree" style="background: #ffffff !important;"></div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="pengguna">
                                    <div class="col-md-12">
                                        <?php
                                            if (!empty($theuser)){
                                                $x = 0;
                                            foreach($theuser as $row){
                                                  $x++;?>
                                       <div class="col-md-6 col-sm-6">
                                            <div class="white-box">
                                                <div class="row">
                                                    <div class="col-md-3 col-sm-3 ext-center">
                                                        
                                                                 <?php $filename = 'assets/upload/pp/'.$row->nik.'.jpg';
                                                                    if (file_exists($filename)) { ?>
                                                                     <img src="{{ url('/') }}/assets/upload/pp/<?php echo $row->nik; ?>.jpg"  class="img-circle thumb-lg" alt="user-img" >
                                                                    <?php } else {?>
                                                                    <img src="{{ url('/') }}/assets/plugins/images/calming-cat.gif" class="img-circle thumb-lg" alt="user-img">
                                                                <?php }?>

                                                    </div>
                                                    <div class="col-md-9 col-sm-9">
                                                        <h4 class="box-title m-b-0"><?php echo $row->nama_lgkp ;?></h4> <a href="#"><small><?php echo $row->nama_kantor ;?></small></a><br><a href="#"><small>User : <?php echo $row->user_id ;?></small></a>
                                                        <p>
                                                            <address>
                                                                <?php echo $row->tmpt_lhr ;?>,
                                                                <br/> <?php echo $row->tgl_lhr ;?>
                                                                <br/>
                                                                <br/>
                                                                <abbr title="Phone">Phone: <?php echo $row->telp ;?></abbr>
                                                                <br/>
                                                                <br/> <?php echo $row->alamat_rumah ;?>
                                                            </address>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php } ?>
                                        <?php } ?>
                                   </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                

            </div>
       @include('shared.footer_detail')