 <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $stitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                        <ol class="breadcrumb">
                            <li class="active"><?php echo $mtitle; ?></li>
                        </ol>
                    </div>
                </div>
                 <div class="row">
                <form name ="get_form" action="<?php echo $my_url; ?>" method="post">
					@csrf
                    <div class="col-sm-12">
                        <div class="white-box">
                            <div class="row">
                                <div class="col-lg-3">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_tgl" type="checkbox" checked disabled="true">
                                            <label for="cb_tgl"> Tanggal Pengambilan</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3">         
                                    <div class="form-group">
                                        <input class="form-control input-daterange-datepicker" type="text" id="tanggal" name="tanggal" /></div>
                                </div>
                                <div class="col-lg-3">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_jenis" type="checkbox" checked disabled="true">
                                            <label for="cb_jenis"> Jenis Blangko</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3">         
                                   <select class="form-control select2" name="no_jenis" id="no_jenis">
                                         <option  value="0">JENIS BLANGKO</option>
                                        </select>
                                </div>
                                </div>
                            <div class="row">
                                <div class="col-lg-3">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_lap" type="checkbox" checked disabled="true">
                                            <label for="cb_lap"> Jenis Laporan</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3">         
                                   <select class="form-control select2" name="no_lap" id="no_lap">
                                         <option  value="0">JENIS LAPORAN</option>
                                         <option  value="1">LAPORAN MASUK</option>
                                         <option  value="2">LAPORAN KELUAR</option>
                                        </select>
                                </div>
                            </div>
                            <div class="row">
                                <button type="submit" class="btn btn-info waves-effect waves-light m-r-10 pull-right" id="btn-filter" onclick="on_serach();" >Search <i class="mdi  mdi-magnify fa-fw"></i></button>
                                  <button type="button" class="btn btn-invert waves-effect waves-light m-r-10 pull-right" id="btn-reset" onclick="on_clear();">Clear <i class="mdi  mdi-delete fa-fw"></i></button>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
                <div class="row">
                <div class="col-md-12">
                        <div class="white-box">
                            <h3 class="box-title"><?php echo $mtitle; ?></h3>
                            <div class="scrollable">
                                <div class="table-responsive">
                                    <table id="mytable" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                    <thead>
                                     
                                        <tr>
                                            <th width="10%" style="text-align: center;">No</th>
                                            <th width="15%" style="text-align: center;">Jenis Blangko</th>
                                            <th width="15%" style="text-align: center;">Jenis Laporan</th>
                                            <th width="15%" style="text-align: center;">Tanggal</th>
                                            <th width="15%" style="text-align: center;">Pengambilan</th>
                                            <th width="15%" style="text-align: center;">Rusak</th>
                                            <th width="15%" style="text-align: center;">Keterangan</th>
                                        </tr>
                                       
                                    </thead>
                                    <tbody id="show_data">
                                       <?php
                                        if (!empty($data)){
                                            $i = 0;
                                           foreach($data as $row){
                                            $i++;
                                            ?>

                                             <tr>
                                                <td width="10%" style="text-align: center;"><?php echo $i ;?></td>
                                                <td width="15%" style="text-align: center;"><?php echo $row->jenis_blangko ;?></td>
                                                <td width="15%" style="text-align: center;"><?php echo $row->jenis ;?></td>
                                                <td width="15%" style="text-align: center;"><?php echo $row->tanggal ;?></td>
                                                <td width="15%" style="text-align: center;"><?php echo $row->jumlah ;?></td>
                                                <td width="15%" style="text-align: center;"><?php echo $row->rusak ;?></td>
                                                <td width="15%" style="text-align: center;"><?php echo $row->ket ;?></td>
                                            </tr>
                                          <?php } ?>
                                          <?php } ?>
                                       
                                        
                                    </tbody>
                                </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                 </div>
       @include('shared.footer_detail')