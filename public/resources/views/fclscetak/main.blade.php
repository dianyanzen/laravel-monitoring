<!DOCTYPE html>
<html lang="en">
@include('shared.head')
<body class="fix-header">
    <div id="wrapper">
            @include('shared.nav_top')
            @include('shared/nav_side')
            @include('fclscetak/content')
    </div>
     
    @include('shared/footer')
    
    @include('fclscetak/javascript')
</body>
</html>