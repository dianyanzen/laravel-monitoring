     <script>
        function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode != 44 && charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }
    
    $(document).ready(function() {
        console.log(init_kec);
        console.log(init_level);
        $('select[name="kd_kategori"]').on('change', function() {

            var kdgroup = $(this).val();
            if (kdgroup == 0){
                $('#value_data').attr("disabled",true);
                $('#value_data').val("");
            }else{
                $('#value_data').attr("disabled",false);
                $('#value_data').val("");
            }
            get_table();
        });
    });
    function on_save(){
        if (validationdaily()){
          do_save();
        }
  
        
    }
    function edit(aset_no,aset_sect) {
            $.ajax({
                    type: "post",
                    url: BASE_URL+"Barang/get_modal_data",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        aset_no : aset_no,
                        aset_sect : aset_sect,
                       
                    },
                    beforeSend:
                    function () {

                    },
                    success: function (data) {
                        $('#mdl_value_data').val(data.value_data);

                    },
                    error:
                    function (data) {
                        unblock_screen();
                        swal("Error!", "Ooops!, Please Try Again, Something Went Wrong", "error"); 

                    },
                    complete:
                    function (response) {
                        unblock_screen();
                        
                    }
                });
            $('#mdl_no_data').val(aset_no);
            $('#mdl_sect_data').val(aset_sect);
            $('#do_aset').click();
        }
        function onClearModal() {
            $("#mdl_no_data").val("");
            $("#mdl_sect_data").val("");
            $("#mdl_value_data").val("");
        }
        function do_close(){
            $('#closemodal').click();
        }
        function do_refresh(){
            get_table();
        }
        function hapus(aset_no,aset_sect) {
           
            swal({   
            title: "Apakah Anda Yakin ?",   
            text: "Menghapus Data Ini ?",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Ya, Hapus Data",   
            cancelButtonText: "Tidak, Jangan Dihapus",   
            closeOnConfirm: false,   
            closeOnCancel: false 
            }, function(isConfirm){   
                if (isConfirm) {
                $.ajax({
                    type: "post",
                    url: BASE_URL+"Barang/delete_master_aset",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        aset_no : aset_no,
                        aset_sect : aset_sect,

                    },
                    beforeSend:
                    function () {
                         block_screen();
                    },
                    success: function (data) {
                        console.log(data);
                         if (data.message_type > 0){
                              swal("Berhasil !", "Data Ini Telah Dihapus !", "success");
                        }else{
                            swal("Warning!", data.message, "warning");  
                        }
                    },
                    error:
                    function (data) {
                        unblock_screen();
                        swal("Error!", "Ooops!, Please Try Again, Something Went Wrong", "error"); 

                    },
                    complete:
                    function (response) {
                        unblock_screen();
                        get_table();
                    }
                });
                     
                } else {     
                    swal("Cancelled", "Data Ini Tidak Jadi Dihapus :)", "error");   
                } 
            });
        }
    function validationdaily() {
         var kd_kategori = $("#kd_kategori");
            if (kd_kategori.val().length == 0) {                
                  swal("Warning!", "Kategori Tidak Boleh Kosong !", "warning");  
                  $("#kd_kategori").focus();
                 return false;
            }
            if (kd_kategori.val() == 0) {                
                  swal("Warning!", "Kategori Tidak Boleh Kosong !", "warning");  
                  $("#kd_kategori").focus();
                 return false;
            }
         var value_data = $("#value_data");
            if (value_data.val().length == 0) {                
                  swal("Warning!", "Data Tidak Boleh Kosong !", "warning");  
                  $("#value_data").focus();
                 return false;
            }
            return true;
       
        }
    function validationmodal() {
         var mdl_no_data = $("#mdl_no_data");
            if (mdl_no_data.val().length == 0) {                
                  swal("Warning!", "Kategori Tidak Boleh Kosong !", "warning");  
                  $("#mdl_no_data").focus();
                 return false;
            }
         var mdl_value_data = $("#mdl_value_data");
            if (mdl_value_data.val().length == 0) {                
                  swal("Warning!", "Data Tidak Boleh Kosong !", "warning");  
                  $("#mdl_value_data").focus();
                 return false;
            }
         var mdl_sect_data = $("#mdl_sect_data");
            if (mdl_sect_data.val().length == 0) {                
                  swal("Warning!", "Sector Tidak Boleh Kosong !", "warning");  
                  $("#mdl_sect_data").focus();
                 return false;
            }
            return true;
       
        }
       function do_save_mdl(){
         if (!validationmodal()) return;
           $.ajax({
                type: "post",
                url: BASE_URL+"Barang/do_edit_master_aset",
                data: {
                    "_token": "{{ csrf_token() }}",
                  mdl_no_data :  $("#mdl_no_data").val(),
                  mdl_value_data :  $("#mdl_value_data").val(),
                  mdl_sect_data :  $("#mdl_sect_data").val(),
                },
                beforeSend:
                function () {
                  document.getElementById("go_send").disabled = true;
                  $('#go_send').html("<i class='fa fa-circle-o-notch fa-spin fa-fw'></i> Wait");
                },
                success: function (data) {

                    if (data.message_type == 1){
                    toastr.success(data.message);
                    $("#mdl_value_data").val('');
                    }else if (data.message_type == 2){
                      toastr.error(data.message);
                    }
                    
                },
                error:
                function (data) {
                    toastr.error("Please Try Again, Something Went Wrong");
                $("#mdl_value_data").val('');
                do_close();

                },
                complete:
                function (response) {
                  document.getElementById("go_send").disabled = false;
                  $('#go_send').html("Kirim");
                  do_close();
                  do_refresh();
                }
            });

        }
        function do_save(){
            $.ajax({
                    type: "post",
                    url: BASE_URL+"Barang/insert_master_aset",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        kd_kategori : $("#kd_kategori").val(),
                        value_data : $("#value_data").val(),
                       
                    },
                    beforeSend:
                    function () {
                         block_screen();
                    },
                    success: function (data) {
                        console.log(data);
                        swal("Success!", data.message, "success");
                        on_clear();
                    },
                    error:
                    function (data) {
                        unblock_screen();
                        swal("Error!", "Ooops!, Please Try Again, Something Went Wrong", "error"); 

                    },
                    complete:
                    function (response) {
                        unblock_screen();
                        
                    }
                });
        }
    function block_screen(){
        
        $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff'
            },
            baseZ: 2000
        }); 
    }
    function on_clear(){
            $('#value_data').val("");
            $('select[name="kd_kategori"]').val("0").trigger("change");
            get_table();
    }
    function unblock_screen(){
       $.unblockUI();
    }
    jQuery(document).ready(function() {
            $(".select2").select2();
            $('.input-daterange-datepicker').daterangepicker({
                buttonClasses: ['btn', 'btn-sm'],
                applyClass: 'btn-info',
                cancelClass: 'btn-info',
                locale: {
                    format: 'DD-MM-YYYY'
                },
                startDate: '<?php echo date('d-m-Y');?>',
                endDate: '<?php echo date('d-m-Y');?>'
            });
             $('#list_barang').DataTable({
                 "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
             $('#value_data').val("");
             $('#value_data').attr("disabled",true);
            get_table();
        });
    function get_table() {
        $('#list_barang').DataTable({
                destroy: true,
                 "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                 "columnDefs": [
                    {"className": "dt-center", "targets": "_all"}
                  ],
                "pageLength" : 50,
                "ajax": {
                 "url": BASE_URL+"Barang/get_master_aset",
                "type": "post",
                "data": {
                    "_token": "{{ csrf_token() }}",
                    kategori : $('#kd_kategori').val()
                }
                }
            });
    }
    </script>
   