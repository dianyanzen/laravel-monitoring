<!DOCTYPE html>
<html lang="en">
@include('shared.head')
<body class="fix-header">
    <div id="wrapper">
            @include('shared.nav_top')
            @include('shared/nav_side')
            @include('Mepeling_wilayah/content')
            @include('Mepeling_wilayah/modal')
    </div>
    @include('shared/footer')
    @include('Mepeling_wilayah/javascript')
</body>
</html>