<div id="page-wrapper">
            <div class="container-fluid">
               <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">{{ $mtitle }}</h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                
                        <ol class="breadcrumb">
                            <li><a href="{{ url('/') }}/">Report</a></li>
                            <li class="active">{{ $mtitle }}</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- .row -->
    

                <div class="row">
                <div class="col-md-12">
                        <div class="white-box">
                            <h3 class="box-title">{{ $mtitle }}</h3>
                            <div class="scrollable">
                                <div class="table-responsive">
                                    <table id="daily-list" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                   <thead>
                                    
                                        <tr>
                                           <th width="5%" style="text-align: center;">No</th>
                                            <th width="15%" style="text-align: center;">User Siak</th>
                                            <th width="15%" style="text-align: center;">Nama Petugas</th>
                                            <th width="15%" style="text-align: center;">Provinsi</th>
                                            <th width="15%" style="text-align: center;">Kabupaten</th>
                                            <th width="15%" style="text-align: center;">Kecamatan</th>
                                            <th width="15%" style="text-align: center;">Keterangan</th>
                                            <th width="15%" style="text-align: center;">Status</th>
                                            <th width="15%" style="text-align: center;">Online</th>
                                            <th width="15%" style="text-align: center;">Action</th>

                                        </tr>
                                       
                                    </thead>
                                    <tbody id="show_data">
                                       
                                        
                                    </tbody>
                                    
                                </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
       @include('shared.footer_detail')