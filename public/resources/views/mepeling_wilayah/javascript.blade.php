 <script>
    toastr.options = {
          "closeButton": true,
          "debug": false,
          "newestOnTop": true,
          "progressBar": true,
          "positionClass": "toast-top-left",
          "preventDuplicates": true,
          "onclick": null,
          "showDuration": "300",
          "hideDuration": "500",
          "timeOut": "2000",
          "extendedTimeOut": "500",
          "showEasing": "swing",
          "hideEasing": "linear",
          "showMethod": "fadeIn",
          "hideMethod": "fadeOut"
        }
    function akses(user_id){
        var user_id_model = user_id;
         $("#user_id-model").text(user_id_model);
         $('select[name="no_kec"]').val("0").trigger("change");
         $('#do_changewil').click();
         
        }
    jQuery(document).ready(function() {
        $.ajax({
                    type: "post",
                    url: BASE_URL+"api/ektp-kec",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        no_prop : 32,
                        no_kab : 73,
                    },
                    beforeSend:
                    function () {
                    },
                    success: function (data) {
                        console.log(data);
                       $('select[name="no_kec"]').empty();
                       $('select[name="no_kec"]').append('<option value="0">-- Pilih Kecamatan --</option>');
                        $.each(data.kecamatan, function(key, value) {
                            $('select[name="no_kec"]').append('<option value="'+ value.no_kec +'">'+ value.nama_kec +'</option>');
                        });
                        $('select[name="no_kec"]').val("0").trigger("change");
                        
                    },
                    error:
                    function (data) {
               

                    },
                    complete:
                    function (response) {
                    }
                });
           get_table_mpl();

    });

    function do_model(){
        var user_id_model =  $("#user_id-model").text();
        var no_wilayah = $('select[name="no_kec"]').val();
             $.ajax({
                    type: "post",
                    url: BASE_URL+"Capil/edit_wil_mpl",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        no_wil : no_wilayah,
                        user_id :user_id_model,

                    },
                    beforeSend:
                    function () {

                    },
                    success: function (data) {
                         toastr.success('Wilayah : '+user_id_model+'<br> Berhasil Di Rubah :))','Success !');
                         $('#closemodel').click();
                         get_table_mpl();
                    },
                    error:
                    function (data) {
                        toastr.error('Ooops!, Please Try Again, Something Went Wrong','Error !');

                    },
                    complete:
                    function (response) {
                         
                    }
                });
           
        
    }
    function activate(user_id){
         $.ajax({
                    type: "post",
                    url: BASE_URL+"Capil/activate_mpl",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        user_id :user_id,

                    },
                    beforeSend:
                    function () {
                        block_screen();
                    },
                    success: function (data) {
                         toastr.success('User : '+user_id+'<br> Berhasil Di Aktifkan :))','Success !');
                         get_table_mpl();
                    },
                    error:
                    function (data) {
                        toastr.error('Ooops!, Please Try Again, Something Went Wrong','Error !');

                    },
                    complete:
                    function (response) {
                         unblock_screen();
                    }
                });
    }
    function deactivate(user_id){
         $.ajax({
                    type: "post",
                    url: BASE_URL+"Capil/deactivate_mpl",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        user_id :user_id,

                    },
                    beforeSend:
                    function () {
                        block_screen();
                    },
                    success: function (data) {
                         toastr.success('User : '+user_id+'<br> Berhasil Di Non Aktifkan :))','Success !');
                         get_table_mpl();
                    },
                    error:
                    function (data) {
                        toastr.error('Ooops!, Please Try Again, Something Went Wrong','Error !');

                    },
                    complete:
                    function (response) {
                         unblock_screen();
                    }
                });
    }
    function get_table_mpl() {
        $('#daily-list').DataTable({
                destroy: true,
                 "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                 "columnDefs": [
                    {"className": "dt-center", "targets": "_all"}
                  ],
                "pageLength" : 50,
                "ajax": {
                 "url": BASE_URL+"Capil/get_seting_user_mpl",
                "type": "post",
                "data": {
                "_token": "{{ csrf_token() }}",
                }
                }
            });
    }

    
    function on_serach() {
        get_table_mpl();
    }
    function do_close_model(){
      $('#closemodel').click();
    }
    function onClearmodel() {
        $("#user_id-model").text("");
    }
    function block_screen(){
        
        $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff'
            },
            baseZ: 2000
        }); 
    }
    function unblock_screen(){
       $.unblockUI();
    }
    </script>