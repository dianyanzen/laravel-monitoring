<button class="btn btn-info btn-lg" id="do_changewil" data-toggle="modal" data-target="#do_changewil-modal" style="display: none;"></button>
<div class="modal fade" id="do_changewil-modal" tabindex="-1" role="dialog" aria-labelledby="changewil" aria-hidden="true" >
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" aria-hidden="true" onclick="do_close_model()">&times;</button>
                      <h4 class="modal-title">Ubah Wilayah</h4>
                    </div>
                    <div class="modal-body">
                     <div class="row">
                            <div class="col-md-12">
                              <form class="form-horizontal" role="form">
                                <fieldset>
                                  <!-- Text input-->
                                   <div class="form-group">
                                    <label class="col-sm-2 control-label" for="textinput" style="text-align: right;">User</label>
                                    <div class="col-sm-10">
                                              <h5><span id="user_id-model"></span></h5>
                                
                                    </div>
                                  </div>


                                  <div class="form-group">
                                    <label class="col-sm-2 control-label " for="textinput" style="text-align: right;">Wilayah</label>
                                    <div class="col-sm-10">
                                      <select class="form-control select2" name="no_kec" id="no_kec">
                                        <option  value="0">-- Pilih Kecamatan --</option>
                                      </select>
                                    </div>
                                  </div>

                                     <div class="col-sm-12" id="bantuan_text">
                                    
                                    
                                  </div>
                                  
                                </fieldset>
                              </form>
                            </div><!-- /.col-lg-12 -->
                        </div><!-- /.row -->
                        </div>

                    <div class="modal-footer" style="margin-top: 0px !important;">
                         
                      <button type="button" id="closemodel" class="btn btn-default" data-dismiss="modal" onclick="onClearmodel()">Tutup</button>
                      <button type="button" id="go_sendmodel" class="btn btn-info" onclick="do_model()">Kirim</button>
                    </div>
                  </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
              </div><!-- /.modal -->

