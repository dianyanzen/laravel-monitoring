<!DOCTYPE html>
<html lang="en">
@include('shared.head')
<body class="fix-header">
    <div id="wrapper">
            @include('shared.nav_top')
            @include('shared/nav_side')
            @include('cetak_dinas/content')
    </div>
     
    @include('shared/footer')
    
    @include('cetak_dinas/javascript')
</body>
</html>