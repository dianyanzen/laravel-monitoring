<div id="page-wrapper">
            <div class="container-fluid">
               <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $mtitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                
                        <ol class="breadcrumb">
                            <li><a href="{{ url('/') }}/">Absensi</a></li>
                            <li class="active"><?php echo $mtitle; ?></li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                
                <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <i class="fa fa-desktop fa-fw"></i> Rekap Absensi
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                            <div class="scrollable">
                                <div class="table-responsive">
                                    <table id="absensi-list" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                   <thead>
                                    
                                        <tr>
                                            <th width="5%" style="text-align: center;">No</th>
                                            <th width="20%" style="text-align: center;">User Id</th>
                                            <th width="25%" style="text-align: center;">Nama Lengkap</th>
                                            <th width="20%" style="text-align: center;">Tanggal</th>
                                            <th width="10%" style="text-align: center;">Jam Masuk</th>
                                            <th width="10%" style="text-align: center;">Jam Keluar</th>
                                            <th width="10%" style="text-align: center;">Keterangan</th>

                                        </tr>
                                       
                                    </thead>
                                    <tbody id="show_data">
                                       
                                        
                                    </tbody>
                                    
                                </table>
                                </div>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                    <div class="panel-footer">
                    
                        <button type="button" name="is_ci" class="btn btn-info btn-lg btn-block" onclick="on_back()"><i class="fa fa-arrow-circle-left fa-1x"></i> Kembali</button>
                    </div>
            
                
                </div>
                </div>
                   
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>

            </div>
       @include('shared.footer_detail')