<!DOCTYPE html>
<html lang="en">
@include('shared.head')
<body class="fix-header">
    <div id="wrapper">
            @include('shared.nav_top')
            @include('shared/nav_side')
            @include('Laporan_dafduk_ktp_oa/content')
    </div>
     
    @include('shared/footer')
    
    @include('Laporan_dafduk_ktp_oa/javascript')
</body>
</html>