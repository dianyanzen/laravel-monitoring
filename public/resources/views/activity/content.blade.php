<div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $stitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                        <ol class="breadcrumb">
                           <li class="text-info"><?php echo $stitle; ?></li>
                        </ol>
                    </div>
                </div>
                <!-- .row -->
               <div class="row">
                <div class="row">
                    <div class="col-md-4 col-xs-12">
                        <div class="white-box">
                            <div class="user-bg"> <img width="100%" alt="user" src="{{ url('/') }}/assets/plugins/images/wallpaper2.jpg">
                                <div class="overlay-box">
                                    <div class="user-content">
                                        <a href="javascript:void(0)"> <?php $filename = 'assets/upload/pp/'.$user_nik.'.jpg';
                                            if (file_exists($filename)) { ?>
                                            <img src="{{ url('/') }}/assets/upload/pp/<?php echo $user_nik; ?>.jpg"  class="img-circle thumb-lg" alt="user-img" >
                                            <?php } else {?>
                                            <img src="{{ url('/') }}/assets/plugins/images/calming-cat.gif" class="img-circle thumb-lg" alt="user-img">
                                        <?php }?></a>
                                        <h4 class="text-white"><?php echo $user_nama_lgkp; ?></h4>
                                        <h5 class="text-white"><?php if($user_level == 1){echo 'KECAMATAN';}?> <?php echo $user_nama_kantor; ?></h5>  </div>
                                </div>
                            </div>
                            <div class="user-btm-box">
                                <div class="col-md-6 col-sm-6 text-center">
                                    <p class="text-info"><b> Clock In :</b></p>
                                   <h1><span id="clock_in">-</span></h1> 
                                </div>
                                <div class="col-md-6 col-sm-6 text-center">
                                    <p class="text-info"><b> Clock Out :</b></p>
                                    <h1><span id="clock_out">-</span></h1> 
                                </div>
                            </div>
                            <div class="user-btm-box">
                                <div class="col-md-4 col-sm-4 text-center">
                                    <p class="text-info"><b> Siak Activity</b></p>
                                    <h1><span id="jml_siak">0</span></h1> 
                                </div>
                                <div class="col-md-4 col-sm-4 text-center">
                                    <p class="text-info"><b> Bcard Activity</b></p>
                                    <h1><span id="jml_bcard">0</span></h1> 
                                </div>
                                <div class="col-md-4 col-sm-4 text-center">
                                    <p class="text-info"><b> Benroll Activity</b></p>
                                    <h1><span id="jml_benroll">0</span></h1> 
                                </div>
                            </div>
                            
                                <span id="rekap_activity">
                                </span>
                            
                        </div>
                        <div class="white-box">
                            <h3><b>My User Control</b> <span class="pull-right"></span></h3>
                            <hr>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="pull-left">
                                        <address>
                                            <h3><b class="text-info"><?php echo $user_nama_lgkp; ?></b></h3>
                                             <h4 class="font-bold">My Siak</h4>

                                            <p class="m-l-5">
                                                <b class="font-bold">User Id : &nbsp;<span id="user_siak">-</span></b><br/>
                                                <b>Status :</b> &nbsp; <span id="status_online">-</span>
                                                <br/> <b>Ip Address :</b> &nbsp;<span id="siak_ip">-</span>
                                                <br/> <b>Last Login :</b> <i class="fa fa-calendar"></i> &nbsp;<span id="siak_last">-</span>
                                            </p>
                                             <h4 class="font-bold">My Monitoring</h4>
                                            <p class="m-l-5">
                                                 <b class="font-bold">User Id : &nbsp;<span id="user_monev">-</span></b><br/>
                                                <b>Status :</b> &nbsp; <span id="status_online_monev">-</span>
                                                <br/> <b>Ip Address :</b> &nbsp;<span id="monev_ip">-</span>
                                                <br/> <b>Last Login :</b> <i class="fa fa-calendar"></i> &nbsp;<span id="monev_last">-</span>
                                            </p>
                                        </address>
                                    </div>
                                    <div class="pull-left text-left">
                                        <address>
                                            <h4 class="font-bold">Rekap Activity Siak</h4>
                                            <p class="m-l-5">
                                                <span id="master_siak" style="font-weight"><b>Tidak Ada Aktivitas Siak</b> </span>
                                            </p>
                                        </address>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8 col-xs-12">
                        <div class="white-box">
                            <ul class="nav nav-tabs tabs customtab">
                                <li class="active tab">
                                    <a href="#siak" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-desktop"></i></span> <span class="hidden-xs">Siak Activity</span> </a>
                                </li>
                                <li class="tab">
                                    <a href="#bcard" data-toggle="tab"> <span class="visible-xs"><i class="fa fa fa-credit-card"></i></span> <span class="hidden-xs">Bcard Activity</span> </a>
                                </li>
                                <li class="tab">
                                    <a href="#benroll" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-camera"></i></span> <span class="hidden-xs">Benroll Activity</span> </a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="siak">
                                    <div class="steamline">
                                        <div id="siak_data">
                                            <div class="sl-item">
                                                    <div class="sl-right" >Please Wait Data Is Still Loading On Your SIAK Activity If You Have This Activity !</div>
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="tab-pane" id="bcard">
                                    <div class="steamline">
                                        <div id="bcard_data">
                                            <div class="sl-item">
                                                    <div class="sl-right">Please Wait Data Is Still Loading On Your Biomorf Card Activity If You Have This Activity !</div>
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="tab-pane" id="benroll">
                                    <div class="steamline">
                                    <div id="benroll_data">
                                        <div class="sl-item">
                                                <div class="sl-right">Please Wait Data Is Still Loading On Your Biomorf Enrollment Activity If You Have This Activity !</div>
                                        </div>
                                    </div>
                                    </div>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
                   
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- /.container-fluid -->
         @include('shared.footer_detail')