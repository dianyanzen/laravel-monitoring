    <script src="{{ url('/') }}/assets/plugins/bower_components/owl.carousel/owl.carousel.min.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/owl.carousel/owl.custom.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/flot/jquery.flot.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/Minimal-Gauge-chart/js/cmGauge.js"></script>
   <script type="text/javascript">
   	$(document).ready(function() {
           
        $.ajax({
                    type: "post",
                    url: BASE_URL+"Activity/get_data_first",
                    dataType: "json",
                    data: {
                    	user_id : "<?php echo $user_id; ?>",
                        "_token": "{{ csrf_token() }}"
                    },
                    beforeSend:
                    function () {
                        
                    },
                    success: function (data) {
                        console.log(data);
                       $('#clock_in').html(data.clock_in);
                       $('#clock_out').html(data.clock_out);
                       $('#user_monev').html(data.user_id);
                       $('#user_siak').html(data.user_siak);
                       if(data.siak_ip == "-"){
                       	$('#status_online').html('<b class="text-info">Is Offline</b>');
                       }else{
                       	$('#status_online').html('<b class="text-info">Is Online</b>');
                       }
                       $('#siak_ip').html(data.siak_ip);
                       $('#siak_last').html(data.siak_last);
                       if(data.monev_ip == "-"){
                       	$('#status_online_monev').html('<b class="text-info">Is Offline</b>');
                       }else{
                       	$('#status_online_monev').html('<b class="text-info">Is Online</b>');
                       }
                       $('#monev_ip').html(data.monev_ip);
                       $('#monev_last').html(data.monev_last);
                       
                        
                    },
                    error:
                    function (data) {
               

                    },
                    complete:
                    function (response) {
                    	get_data_four();
                    }
                });

    });
    function get_data_second(){
         $.ajax({
                    type: "post",
                    url: BASE_URL+"Activity/get_data_second",
                    dataType: "json",
                    data: {
                        user_id : "<?php echo $user_id; ?>",
                        "_token": "{{ csrf_token() }}"
                    },
                    beforeSend:
                    function () {
                        
                    },
                    success: function (data) {
                        console.log(data);
                       $('#jml_siak').html(data.jml_siak);
                       $('#jml_bcard').html(data.jml_bcard);
                       $('#jml_benroll').html(data.jml_benroll);
                        
                    },
                    error:
                    function (data) {
               

                    },
                    complete:
                    function (response) {
                        get_data_third();
                    }
                });
    }
     function get_data_third(){
         $.ajax({
                    type: "post",
                    url: BASE_URL+"Activity/get_data_third",
                    dataType: "json",
                    data: {
                        user_id : "<?php echo $user_id; ?>",
                        "_token": "{{ csrf_token() }}"
                    },
                    beforeSend:
                    function () {
                        
                    },
                    success: function (data) {
                        console.log(data);
                        $('#rekap_activity').empty();
                            $.each(data, function(key, value) {
                                $('#rekap_activity').append(' <div class="user-btm-box"><div class="col-md-4 col-sm-4 text-center"><p class="text-info"><b> Siak Bulan Ini</b></p><h1>'+value.all_siak+'</h1></div><div class="col-md-4 col-sm-4 text-center"><p class="text-info"><b> Bcard Bulan Ini</b></p><h1>'+value.all_cetak+'</h1></div><div class="col-md-4 col-sm-4 text-center"><p class="text-info"><b> Benroll Bulan Ini</b></p><h1>'+value.all_rekam+'</h1></div></div>');
                            });
                        
                    },
                    error:
                    function (data) {
               

                    },
                    complete:
                    function (response) {
                        
                    }
                });
    }
     function get_data_four(){
         $.ajax({
                    type: "post",
                    url: BASE_URL+"Activity/get_data_four",
                    dataType: "json",
                    data: {
                        user_id : "<?php echo $user_id; ?>",
                        "_token": "{{ csrf_token() }}"
                    },
                    beforeSend:
                    function () {
                        
                    },
                    success: function (data) {
                        console.log(data);
                            $('#master_siak').empty();
                            $.each(data, function(key, value) {
                                $('#master_siak').append('<b>'+value.p1+' :</b> &nbsp;'+value.jml+' <br/>');
                            });
                        
                    },
                    error:
                    function (data) {
               

                    },
                    complete:
                    function (response) {
                        siak_data();
                    }
                });
    }
     function siak_data(){
         $.ajax({
                    type: "post",
                    url: BASE_URL+"Activity/siak_data",
                    dataType: "json",
                    data: {
                        user_id : "<?php echo $user_id; ?>",
                        "_token": "{{ csrf_token() }}"
                    },
                    beforeSend:
                    function () {
                        
                    },
                    success: function (data) {
                        console.log(data);
                         if (!$.trim(data)){  
                        	$('#siak_data').empty();
                        	$('#siak_data').append('<div class="sl-item"><div class="sl-right">There Is No Data To Be Displayed On Your Siak Activity</div></div>');

                        }else{
                            $('#siak_data').empty();
                            $.each(data, function(key, value) {
                                $('#siak_data').append('<div class="sl-item"><div class="sl-left"><?php $filename = "assets/upload/pp/".$user_nik.".jpg"; if (file_exists($filename)) { ?> <img src="{{ url('/') }}/assets/upload/pp/<?php echo $user_nik; ?>.jpg"  class="img-circle img-responsive" alt="user-img" ><?php } else {?><img src="{{ url('/') }}/assets/plugins/images/calming-cat.gif" class="img-circle" alt="user-img"><?php }?> </div><div class="sl-right"><div class="m-l-40"> <a href="#" class="text-info">'+value.nama_lgkp+'</a> <span class="sl-date">Jam '+value.activity_date+'</span><div class="m-t-20 row"><div class="col-md-12 col-xs-12"><a href="#" class="btn btn-info" style="margin-bottom: 10px !important">'+value.p1+' '+value.p2+'</a><p> '+value.p3+'</p></div></div></div></div></div>');
                            });
                        }
                        
                    },
                    error:
                    function (data) {
               

                    },
                    complete:
                    function (response) {
                        bcard_data();
                    }
                });
    }
     function bcard_data(){
         $.ajax({
                    type: "post",
                    url: BASE_URL+"Activity/bcard_data",
                    dataType: "json",
                    data: {
                        user_id : "<?php echo $user_id; ?>",
                        "_token": "{{ csrf_token() }}"
                    },
                    beforeSend:
                    function () {
                        
                    },
                    success: function (data) {
                        console.log(data);
                        if (!$.trim(data)){  
                        	$('#bcard_data').empty();
                        	$('#bcard_data').append('<div class="sl-item"><div class="sl-right">There Is No Data To Be Displayed On Your Biomorf Card Activity</div></div>');

                        }else{
                            $('#bcard_data').empty();
                            $.each(data, function(key, value) {
                                $('#bcard_data').append('<div class="sl-item"><div class="sl-left"><?php $filename = "assets/upload/pp/".$user_nik.".jpg"; if (file_exists($filename)) { ?> <img src="{{ url('/') }}/assets/upload/pp/<?php echo $user_nik; ?>.jpg"  class="img-circle img-responsive" alt="user-img" ><?php } else {?><img src="{{ url('/') }}/assets/plugins/images/calming-cat.gif" class="img-circle" alt="user-img"><?php }?> </div><div class="sl-right"><div class="m-l-40"> <a href="#" class="text-info">'+value.nama_lgkp+'</a> <span class="sl-date">Jam '+value.activity_date+'</span><div class="m-t-20 row"><div class="col-md-12 col-xs-12"><a href="#" class="btn btn-info" style="margin-bottom: 10px !important">'+value.p1+' '+value.p2+'</a><p> '+value.p3+'</p></div></div></div></div></div>');
                            });
                        }
                        
                    },
                    error:
                    function (data) {
               

                    },
                    complete:
                    function (response) {
                        benroll_data();
                    }
                });
    }
     function benroll_data(){
         $.ajax({
                    type: "post",
                    url: BASE_URL+"Activity/benroll_data",
                    dataType: "json",
                    data: {
                        user_id : "<?php echo $user_id; ?>",
                        "_token": "{{ csrf_token() }}"
                    },
                    beforeSend:
                    function () {
                        
                    },
                    success: function (data) {
                        console.log(data);
                        if (!$.trim(data)){  
                        	$('#benroll_data').empty();
                        	$('#benroll_data').append('<div class="sl-item"><div class="sl-right">There Is No Data To Be Displayed On Your Biomorf Enrollment Activity</div></div>');

                        }else{
                            $('#benroll_data').empty();
                            $.each(data, function(key, value) {
                                $('#benroll_data').append('<div class="sl-item"><div class="sl-left"><?php $filename = "assets/upload/pp/".$user_nik.".jpg"; if (file_exists($filename)) { ?> <img src="{{ url('/') }}/assets/upload/pp/<?php echo $user_nik; ?>.jpg"  class="img-circle img-responsive" alt="user-img" ><?php } else {?><img src="{{ url('/') }}/assets/plugins/images/calming-cat.gif" class="img-circle" alt="user-img"><?php }?> </div><div class="sl-right"><div class="m-l-40"> <a href="#" class="text-info">'+value.nama_lgkp+'</a> <span class="sl-date">Jam '+value.activity_date+'</span><div class="m-t-20 row"><div class="col-md-12 col-xs-12"><a href="#" class="btn btn-info" style="margin-bottom: 10px !important">'+value.p1+' '+value.p2+'</a><p> '+value.p3+'</p></div></div></div></div></div>');
                            });
                        }
                        
                    },
                    error:
                    function (data) {
               

                    },
                    complete:
                    function (response) {
                    	get_data_second();
                    }
                });
    }
   </script>