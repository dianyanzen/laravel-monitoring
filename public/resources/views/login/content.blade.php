<div class="preloader">
  <div class="cssload-speeding-wheel"></div>
</div>
<section id="wrapper" class="login-register">
  <div class="login-box login-sidebar">
    <div class="white-box">
      <form class="form-horizontal form-material" id="loginform" action="login/do_login">
        <a href="javascript:void(0)" class="text-center db"><img src="{{ url('/') }}/assets/plugins/images/pemkot.png" alt="Home" style="width: 50px;"/><br/><img src="{{ url('/') }}/assets/plugins/images/LogoDIsduk1.png" alt="Home" style="width: 200px; margin-top: 5px"/></a>  
        
        <div class="form-group m-t-40">
          <div class="col-xs-12">
            <input class="form-control" id="user_id" type="text" required="" placeholder="Username">
          </div>
        </div>
        <div class="form-group">
          <div class="col-xs-12">
            <input class="form-control" id="pass" type="password" required="" placeholder="Password">
          </div>
        </div>
        <div class="form-group text-center m-t-20">
          <div class="col-xs-12">
            <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" id="do_loginbtn" type="button" onclick="do_login()">Log In</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</section>