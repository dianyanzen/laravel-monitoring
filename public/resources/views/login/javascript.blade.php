<script>
var input_user_id = document.getElementById("user_id");
var input_pswd = document.getElementById("pass");
input_user_id.addEventListener("keyup", function(event) {
    event.preventDefault();
    if (event.keyCode === 13) {
        input_pswd.select();
    }
});
input_pswd.addEventListener("keyup", function(event) {
    event.preventDefault();
    if (event.keyCode === 13) {
        do_login();
    }
});
</script>
<script type="text/javascript">
		toastr.options = {
		  "closeButton": true,
		  "debug": false,
		  "newestOnTop": true,
		  "progressBar": true,
		  "positionClass": "toast-top-left",
		  "preventDuplicates": true,
		  "onclick": null,
		  "showDuration": "300",
		  "hideDuration": "500",
		  "timeOut": "2000",
		  "extendedTimeOut": "500",
		  "showEasing": "swing",
		  "hideEasing": "linear",
		  "showMethod": "fadeIn",
		  "hideMethod": "fadeOut"
		}
		function validationdaily() {
            var user_id = $("#user_id");
            if (user_id.val().length == 0) {            	
                toastr.warning('Username Cannot Empty');
                user_id.select();
                return false;
            }
            var pass = $("#pass");
            if (pass.val().length == 0) {
                toastr.warning('Password Cannot Empty');
                pass.select();
                return false;
            } 

            return true;
        }
	function do_login(){
	if (!validationdaily()) return;
		$.ajax({
	                type: "post",
	                url: BASE_URL+"login/dologin",
	                data: {
	                	"_token": "{{ csrf_token() }}",
	                	user_id :  $("#user_id").val(),
	                	pass :  $("#pass").val()
	                },

	                beforeSend:
	                function () {
	                document.getElementById("do_loginbtn").disabled = true;
                	$('#do_loginbtn').html("<i class='fa fa-circle-o-notch fa-spin fa-fw'></i> Waiting For Authentication");
	                },
	                success: function (data) {
						console.log(data);
	                   if (data.message_type > 0){
	                   		document.location.href = "{{ url('/') }}";
                    	}else{
							toastr.warning(data.message,'Sorry, Cannot Login');
							$("#user_id").val('');
							$("#pass").val('');
                    	}
	                },
	                error:
	                function (data) {
	                    toastr.info('Oops Something Went Wrong');
	                    $("#user_id").val('');
						$("#pass").val('');
	                },
	                complete:
	                function (response) {
	       			document.getElementById("do_loginbtn").disabled = false;
                	$('#do_loginbtn').html("Log In");
	                }
	            });
	}


</script>