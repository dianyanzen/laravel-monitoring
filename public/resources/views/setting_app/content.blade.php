<div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $stitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                        <ol class="breadcrumb">
                           <li class="text-info"><?php echo $stitle; ?></li>
                        </ol>
                    </div>
                </div>
                <!-- .row -->
               <div class="row">
                <div class="row">

                    <div class="col-md-12 col-xs-12">
                        <div class="white-box">
                            <ul class="nav nav-tabs tabs customtab">
                                <li class="active tab">
                                    <a href="#setting_app" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-user"></i></span> <span class="hidden-xs" style="color: blue !important">SETTING APLIKASI</span> </a> 
                                </li>
                               
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="setting_app">
                                        <form class="form-horizontal form-material">
                                        <div class="form-group">
                                            <label class="col-md-12" style="color: blue !important">NO PROVINSI</label>
                                            <div class="col-md-12">
                                                <input type="text" value="<?php if (!empty($data)){echo $data['no_prop']; }?>" id="no_prop" class="form-control form-control-line" onkeypress="return isNumberKey(event)" maxlength="2"> </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12" style="color: blue !important">NO KABUPATEN</label>
                                            <div class="col-md-12">
                                                <input type="text" value="<?php if (!empty($data)){echo $data['no_kab']; }?>" id="no_kab" class="form-control form-control-line" onkeypress="return isNumberKey(event)" maxlength="2"> </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12" style="color: blue !important">NAMA PROVINSI</label>
                                            <div class="col-md-12">
                                                <input type="text" value="<?php if (!empty($data)){echo $data['nm_prop']; }?>" id="nm_prop" class="form-control form-control-line"> </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12" style="color: blue !important">NAMA KABUPATEN</label>
                                            <div class="col-md-12">
                                                <input type="text" value="<?php if (!empty($data)){echo $data['nm_kab']; }?>" id="nm_kab" class="form-control form-control-line"> </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12" style="color: blue !important">SIAK DBLINK</label>
                                            <div class="col-md-12">
                                                <input type="text" value="<?php if (!empty($data)){echo $data['siak_dblink']; }?>" id="siak_dblink" class="form-control form-control-line"> </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12" style="color: blue !important">CETAK DBLINK</label>
                                            <div class="col-md-12">
                                                <input type="text" value="<?php if (!empty($data)){echo $data['cetak_dblink']; }?>" id="cetak_dblink" class="form-control form-control-line"> </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12" style="color: blue !important">REKAM DBLINK</label>
                                            <div class="col-md-12">
                                                <input type="text" value="<?php if (!empty($data)){echo $data['rekam_dblink']; }?>" id="rekam_dblink" class="form-control form-control-line"> </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12" style="color: blue !important">MONITORING DBLINK</label>
                                            <div class="col-md-12">
                                                <input type="text" value="<?php if (!empty($data)){echo $data['master_dblink']; }?>" id="master_dblink" class="form-control form-control-line"> </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12" style="color: blue !important">DKB BIODATA</label>
                                            <div class="col-md-12">
                                                <input type="text" value="<?php if (!empty($data)){echo $data['dkb_bio']; }?>" id="dkb_bio" class="form-control form-control-line"> </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12" style="color: blue !important">DKB KELUARGA</label>
                                            <div class="col-md-12">
                                                <input type="text" value="<?php if (!empty($data)){echo $data['dkb_kk']; }?>" id="dkb_kk" class="form-control form-control-line"> </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12" style="color: blue !important">TAHUN DKB</label>
                                            <div class="col-md-12">
                                                <input type="text" value="<?php if (!empty($data)){echo $data['dkb_tahun']; }?>" id="dkb_tahun" class="form-control form-control-line"> </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12" style="color: blue !important">DKB TANGGAL CUTOFF</label>
                                            <div class="col-md-12">
                                                 <input type="text" value="<?php if (!empty($data)){echo $data['dkb_cutoff']; }?>" class="form-control" id="dkb_cutoff" value="" placeholder="DD-MM-YYYY" readonly> </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12" style="color: blue !important">DEFAULT PASSWORD</label>
                                            <div class="col-md-12">
                                                <input type="text" value="<?php if (!empty($data)){echo $data['default_pass']; }?>" id="default_pass" class="form-control form-control-line"> </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12" style="color: blue !important">AKSES SEMUA KECAMATAN</label>
                                            <div class="col-md-12">
                                                <select class="form-control select2" name="give_kec" id="give_kec"></select> </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12" style="color: blue !important">AKSES SEMUA KELURAHAN</label>
                                            <div class="col-md-12">
                                                <select class="form-control select2" name="give_kel" id="give_kel"></select> </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12 pull-right">
                                                <button type="button" class="btn btn-info waves-effect waves-light m-r-10 pull-right" id="btnedit" onclick="on_change_master();">Edit <i class="mdi mdi-tooltip-edit fa-fw"></i></button>
                                            </div>
                                        </div>
                                    </form>
                                    </div>
                                    
                            </div>
                        </div>
                    </div>
                </div>
                   
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- /.container-fluid -->
         @include('shared.footer_detail')