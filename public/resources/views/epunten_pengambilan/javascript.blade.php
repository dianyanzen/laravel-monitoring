 <script>
    jQuery(document).ready(function() {
           get_table();
        });
        $('#pengambilan-list').DataTable({
             "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        });

    function get_table() {
        $('#pengambilan-list').DataTable({
                destroy: true,
                 "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                 "columnDefs": [
                    {"className": "dt-left", "targets": "_all"}
                  ],
                "pageLength" : 50,
                "ajax": {
                 "url": BASE_URL+"pengambilan/get_data_pengambilan",
                "type": "post",
                "data": { "_token": "{{ csrf_token() }}",
                
                }
                }
            });
    }
    </script>