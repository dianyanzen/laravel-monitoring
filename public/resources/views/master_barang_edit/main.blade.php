<!DOCTYPE html>
<html lang="en">
@include('shared.head')
<body class="fix-header">
    <div id="wrapper">
            @include('shared.nav_top')
            @include('shared/nav_side')
            @include('master_barang_edit/content')
    </div>
     
    @include('shared/footer')
    
    @include('master_barang_edit/javascript')
</body>
</html>