<!DOCTYPE html>
<html lang="en">
@include('shared.head')
<body class="fix-header">
    <div id="wrapper">
            @include('shared.nav_top')
            @include('shared/nav_side')
            @include('kpu_data/content')
    </div>
     
    @include('shared/footer')
    
    @include('kpu_data/javascript')
</body>
</html>