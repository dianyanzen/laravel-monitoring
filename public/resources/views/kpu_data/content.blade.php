 <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $stitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                        <ol class="breadcrumb">
                            <li class="active"><?php echo $mtitle; ?></li>
                        </ol>
                    </div>
                </div>
                <?php
                    if (!empty($dtl_agama)){
                ?>
                <div class="row">
                <div class="col-md-12">
                        <div class="white-box">
                            <h3 class="box-title"><?php echo $mtitle; ?> PER AGAMA</h3>
                            <div class="scrollable">
                                <div class="table-responsive">
                                    <table id="mytable5" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                   <thead>
                                     
                                        <tr>
                                            <th width="20%" style="text-align: center;">No</th>
                                            <th width="60%" style="text-align: center;">Agama</th>
                                            <th width="20%" style="text-align: center;">Jumlah</th>
                                            
                                        </tr>
                                       
                                    </thead>
                                    <tbody >
                                       <?php
                                            $i=0;
                                            $kec_jml = 0;
                                           foreach($dtl_agama as $row){
                                            $i++;
                                            $kec_jml = $kec_jml + $row->jml; 
                                            ?>
                                            
                                             <tr>
                                                <td width="20%" style="text-align: center;"><?php echo $i ;?></td>
                                                <td width="60%" style="text-align: center;"><?php echo $row->agama ;?></td>
                                                <td width="20%" style="text-align: center;"><?php echo $row->jml ;?></td>
                                                
                                            </tr>
                                          <?php } ?>
                                       
                                        
                                    </tbody>
                                    
                                        <tfoot id="my_foot">
                                            <tr>
                                            <th width="80%" colspan="2" style="text-align: center;">Jumlah</th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($kec_jml),0,',','.');?></th>
                                        </tr>

                                            </tfoot>
                                </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <?php
                    if (!empty($dtl_kec)){
                ?>
                <div class="row">
                <div class="col-md-12">
                        <div class="white-box">
                            <h3 class="box-title"><?php echo $mtitle; ?> PER KECAMATAN</h3>
                            <div class="scrollable">
                                <div class="table-responsive">
                                    <table id="mytable1" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                   <thead>
                                     
                                        <tr>
                                            <th width="20%" style="text-align: center;">No</th>
                                            <th width="60%" style="text-align: center;">Kecamatan</th>
                                            <th width="20%" style="text-align: center;">Jumlah</th>
                                            
                                        </tr>
                                       
                                    </thead>
                                    <tbody >
                                       <?php
                                            $i=0;
                                            $kec_jml = 0;
                                           foreach($dtl_kec as $row){
                                            $i++;
                                            $kec_jml = $kec_jml + $row->jml; 
                                            ?>
                                            
                                             <tr>
                                                <td width="20%" style="text-align: center;"><?php echo $i ;?></td>
                                                <td width="60%" style="text-align: center;"><?php echo $row->nama_kec ;?></td>
                                                <td width="20%" style="text-align: center;"><?php echo $row->jml ;?></td>
                                                
                                            </tr>
                                          <?php } ?>
                                       
                                        
                                    </tbody>
                                    
                                        <tfoot id="my_foot">
                                            <tr>
                                            <th width="80%" colspan="2" style="text-align: center;">Jumlah</th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($kec_jml),0,',','.');?></th>
                                        </tr>

                                            </tfoot>
                                </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <?php
                    if (!empty($dtl_kel)){
                ?>
                <div class="row">
                <div class="col-md-12">
                        <div class="white-box">
                            <h3 class="box-title"><?php echo $mtitle; ?> PER KELURAHAN</h3>
                            <div class="scrollable">
                                <div class="table-responsive">
                                    <table id="mytable2" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                   <thead>
                                     
                                        <tr>
                                            <th width="20%" style="text-align: center;">No</th>
                                            <th width="30%" style="text-align: center;">Kecamatan</th>
                                            <th width="30%" style="text-align: center;">Kelurahan</th>
                                            <th width="20%" style="text-align: center;">Jumlah</th>
                                            
                                        </tr>
                                       
                                    </thead>
                                    <tbody >
                                       <?php
                                            $i=0;
                                            $kel_jml = 0;
                                           foreach($dtl_kel as $row){
                                            $i++;
                                            $kel_jml = $kel_jml + $row->jml; 
                                            ?>
                                            
                                             <tr>
                                                <td width="20%" style="text-align: center;"><?php echo $i ;?></td>
                                                <td width="30%" style="text-align: center;"><?php echo $row->nama_kec ;?></td>
                                                <td width="30%" style="text-align: center;"><?php echo $row->nama_kel ;?></td>
                                                <td width="20%" style="text-align: center;"><?php echo $row->jml ;?></td>
                                                
                                            </tr>
                                          <?php } ?>
                                       
                                        
                                    </tbody>
                                    
                                        <tfoot id="my_foot">
                                            <tr>
                                            <th width="80%" colspan="3" style="text-align: center;">Jumlah</th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($kel_jml),0,',','.');?></th>
                                        </tr>

                                            </tfoot>
                                </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <?php
                    if (!empty($dtl_rw)){
                ?>
                <div class="row">
                <div class="col-md-12">
                        <div class="white-box">
                            <h3 class="box-title"><?php echo $mtitle; ?> PER RW</h3>
                            <div class="scrollable">
                                <div class="table-responsive">
                                    <table id="mytable3" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                   <thead>
                                     
                                        <tr>
                                            <th width="10%" style="text-align: center;">No</th>
                                            <th width="30%" style="text-align: center;">Kecamatan</th>
                                            <th width="30%" style="text-align: center;">Kelurahan</th>
                                            <th width="15%" style="text-align: center;">RW</th>
                                            <th width="15%" style="text-align: center;">Jumlah</th>
                                            
                                        </tr>
                                       
                                    </thead>
                                    <tbody >
                                       <?php
                                            $i=0;
                                            $kel_jml = 0;
                                           foreach($dtl_rw as $row){
                                            $i++;
                                            $kel_jml = $kel_jml + $row->jml; 
                                            ?>
                                            
                                             <tr>
                                                <td width="10%" style="text-align: center;"><?php echo $i ;?></td>
                                                <td width="30%" style="text-align: center;"><?php echo $row->nama_kec ;?></td>
                                                <td width="30%" style="text-align: center;"><?php echo $row->nama_kel ;?></td>
                                                <td width="15%" style="text-align: center;"><?php echo $row->rw ;?></td>
                                                <td width="15%" style="text-align: center;"><?php echo $row->jml ;?></td>
                                                
                                            </tr>
                                          <?php } ?>
                                       
                                        
                                    </tbody>
                                    
                                        <tfoot id="my_foot">
                                            <tr>
                                            <th width="80%" colspan="4" style="text-align: center;">Jumlah</th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($kel_jml),0,',','.');?></th>
                                        </tr>

                                            </tfoot>
                                </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <?php
                    if (!empty($dtl_rt)){
                ?>
                <div class="row">
                <div class="col-md-12">
                        <div class="white-box">
                            <h3 class="box-title"><?php echo $mtitle; ?> PER RT</h3>
                            <div class="scrollable">
                                <div class="table-responsive">
                                    <table id="mytable4" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                   <thead>
                                     
                                        <tr>
                                            <th width="10%" style="text-align: center;">No</th>
                                            <th width="20%" style="text-align: center;">Kecamatan</th>
                                            <th width="20%" style="text-align: center;">Kelurahan</th>
                                            <th width="15%" style="text-align: center;">RW</th>
                                            <th width="15%" style="text-align: center;">RT</th>
                                            <th width="20%" style="text-align: center;">Jumlah</th>
                                            
                                        </tr>
                                       
                                    </thead>
                                    <tbody >
                                       <?php
                                            $i=0;
                                            $kel_jml = 0;
                                           foreach($dtl_rt as $row){
                                            $i++;
                                            $kel_jml = $kel_jml + $row->jml; 
                                            ?>
                                            
                                             <tr>
                                                <td width="10%" style="text-align: center;"><?php echo $i ;?></td>
                                                <td width="20%" style="text-align: center;"><?php echo $row->nama_kec ;?></td>
                                                <td width="20%" style="text-align: center;"><?php echo $row->nama_kel ;?></td>
                                                <td width="15%" style="text-align: center;"><?php echo $row->rw ;?></td>
                                                <td width="15%" style="text-align: center;"><?php echo $row->rt ;?></td>
                                                <td width="20%" style="text-align: center;"><?php echo $row->jml ;?></td>
                                                
                                            </tr>
                                          <?php } ?>
                                       
                                        
                                    </tbody>
                                    
                                        <tfoot id="my_foot">
                                            <tr>
                                            <th width="80%" colspan="5" style="text-align: center;">Jumlah</th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($kel_jml),0,',','.');?></th>
                                        </tr>

                                            </tfoot>
                                </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <div class="row">
                <div class="col-md-12">
                        <div class="white-box">
                            <h3 class="box-title"><?php echo $mtitle; ?></h3>
                            <div class="scrollable">
                                <div class="table-responsive">
                                    <table id="mytable" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                   <thead>
                                     
                                        <tr>
                                            <th width="10%" style="text-align: center;">No</th>
                                            <th width="10%" style="text-align: center;">Chip ID</th>
                                            <th width="15%" style="text-align: center;">Nik</th>
                                            <th width="20%" style="text-align: center;">Nama Lengkap</th>
                                            <th width="20%" style="text-align: center;">Tanggal Lahir</th>
                                            <th width="20%" style="text-align: center;">Umur</th>
                                            <th width="15%" style="text-align: center;">Status E-KTP</th>
                                            <th width="10%" style="text-align: center;">Nama Kecamatan</th>
                                            <th width="10%" style="text-align: center;">Nama Kelurahan</th>
                                            <th width="10%" style="text-align: center;">Print Date</th>
                                            <th width="10%" style="text-align: center;">Print Hour</th>
                                            <th width="10%" style="text-align: center;">Print By</th>
                                            <th width="10%" style="text-align: center;">Req Date</th>
                                            <th width="10%" style="text-align: center;">Req By</th>
                                             <th width="10%" style="text-align: center;">Alamat</th>
                                             <th width="10%" style="text-align: center;">Rw</th>
                                             <th width="10%" style="text-align: center;">Rt</th>
                                             <th width="10%" style="text-align: center;">Kode Pos</th>
                                             <th width="15%" style="text-align: center;">Agama</th>
                                             <th width="10%" style="text-align: center;">Status Kawin</th>
                                            <th width="10%" style="text-align: center;">Jenis Pekerjaan</th>
                                            
                                        </tr>
                                       
                                    </thead>
                                    <tbody >
                                       <?php
                                        if (!empty($data)){
                                            $i=0;
                                           foreach($data as $row){
                                            $i++;
                                            ?>
                                            
                                             <tr>
                                                 <td width="10%" style="text-align: center;"><?php echo $i ;?></td>
                                                <td width="15%" style="text-align: center;"><span style="color: #fff">,</span><?php echo $row->chip_id ;?></td>
                                                <td width="15%" style="text-align: center;"><span style="color: #fff">,</span><?php echo $row->nik ;?></td>
                                                <td width="20%" style="text-align: center;"><?php echo $row->nama_lengkap ;?></td>
                                                <td width="20%" style="text-align: center;"><?php echo $row->tgl_lhr ;?></td>
                                                <td width="20%" style="text-align: center;"><?php echo $row->umur ;?></td>
                                                <td width="15%" style="text-align: center;"><?php echo $row->current_status_code ;?></td>
                                                <td width="10%" style="text-align: center;"><?php echo $row->nama_kec ;?></td>
                                                <td width="10%" style="text-align: center;"><?php echo $row->nama_kel ;?></td>
                                                <td width="10%" style="text-align: center;"><?php echo $row->printed_date ;?></td>
                                                <td width="10%" style="text-align: center;"><?php echo $row->printed_hr ;?></td>
                                                <td width="10%" style="text-align: center;"><?php echo $row->printed_by ;?></td>
                                                <td width="10%" style="text-align: center;"><?php echo $row->req_date ;?></td>
                                                <td width="10%" style="text-align: center;"><?php echo $row->req_by ;?></td>
                                                <td width="10%" style="text-align: center;"><?php echo $row->alamat ;?></td>
                                                <td width="10%" style="text-align: center;"><?php echo $row->rw ;?></td>
                                                <td width="10%" style="text-align: center;"><?php echo $row->rt ;?></td>
                                                <td width="10%" style="text-align: center;"><?php echo $row->kode_pos ;?></td>
                                                <td width="15%" style="text-align: center;"><?php echo $row->agama ;?></td>
                                                <td width="10%" style="text-align: center;"><?php echo $row->stat_kwn ;?></td>
                                                <td width="10%" style="text-align: center;"><?php echo $row->jenis_pkrjn ;?></td>

                                                
                                            </tr>
                                          <?php } ?>
                                          <?php } ?>
                                       
                                        
                                    </tbody>
                                    
                                        <tfoot id="my_foot">
                                            <tr>
                                            <?php if (!empty($data)){ ?>
                                            <th width="80%" colspan="20" style="text-align: center;">Jumlah</th>
                                            <th width="20%" style="text-align: right;"><?php echo number_format(htmlentities($i),0,',','.');?></th>
                                            <?php } ?>
                                        </tr>

                                            </tfoot>
                                </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                 </div>
       @include('shared.footer_detail')