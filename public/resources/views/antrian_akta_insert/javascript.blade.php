     <script>
        function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode != 44 && charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }
     $(document).bind('keypress', function(e) {
            if(e.keyCode==13){
                 $('#btn-search').trigger('click');
             }
        });
        $(document).ready(function() {
            $('#antrian-list').DataTable({
                 "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                "pageLength" : 10,
                "ajax": {
                 "url": BASE_URL+"Antrian/get_edit_antrianakta",
                "type": "post",
                "data": {
                  "_token": "{{ csrf_token() }}",
                    "nik":  $('#antrian_nik').val()
                }
                }
            });

    });
    function get_table() {
        if (validationdaily()){
        block_screen();
        $('#antrian-list').DataTable({
                destroy: true,
                 "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                 'columnDefs': [
                  {
                      "targets": 0, // your case first column
                      "className": "text-center",
                 },
                 {
                      "targets": 1,
                      "className": "text-center",
                 },
                 {
                      "targets": 2,
                      "className": "text-center",
                 },
                 {
                      "targets": 3,
                      "className": "text-center",
                 },
                 {
                      "targets": 4,
                      "className": "text-center",
                 },
                 {
                      "targets": 5,
                      "className": "text-center",
                 },
                 {
                      "targets": 6,
                      "className": "text-center",
                 }],
                "pageLength" : 50,
                "ajax": {
                 "url": BASE_URL+"Antrian/get_edit_antrianakta",
                "type": "post",
                "data": {
                  "_token": "{{ csrf_token() }}",
                    "nik":  $('#antrian_nik').val()
                },
                complete: function() {
                    cek_exists();
                }
                }
            });
        
        unblock_screen();
        }
    }
    function cek_exists(){
        var totalRecords =  $("#antrian-list").DataTable().page.info().recordsTotal;
        if (totalRecords ==0){
            if ($('#antrian_nik').val().length > 0){
        swal("Warning!", "Nik : "+$('#antrian_nik').val()+" \n Tidak Ditemukan !", "warning");  
            }
        }
        $('#antrian_nik').val("");
    }
    function validationdaily() {
        var nik = $("#antrian_nik");
            if (nik.val().length == 0 ) {                
                  swal("Warning!", "Nik Tidak Boleh Kosong !", "warning");  
                  on_clear();
                 return false;
            }
            return true;
       
        }
        
    function on_clear() {
        $('#antrian_nik').val("");
        $('#antrian-list').DataTable({
                destroy: true,
                 "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                "pageLength" : 50,
                "ajax": {
                 "url": BASE_URL+"Antrian/get_edit_antrianakta",
                "type": "post",
                "data": {
                  "_token": "{{ csrf_token() }}",
                    "nik":  $('#antrian_nik').val()
                }
                }
            });
    }
    function block_screen(){
        $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff'
            },
            baseZ: 2000
        }); 
    }
    function unblock_screen(){
       $.unblockUI();
    }
    jQuery(document).ready(function() {
        $(".select2").select2();
        $('#tanggal').val("<?php echo date('d-m-Y');?>");

        $("#mdl_ststus_akta").change(function(){
          var mdl_ststus_akta =  $("#mdl_ststus_akta").val();
            if (mdl_ststus_akta == 1){
              $("#mdl_no_akta_lhr").attr("disabled",true);
              $("#go_send").attr("disabled",true);
            }else{
              $("#mdl_no_akta_lhr").attr("disabled",false);
              $("#go_send").attr("disabled",false);
            }
        });
        });

       
    function on_update(data){
        $("#mdl_nik").val(data);
        $("#mdl_ststus_akta").val("1");
        $("#mdl_no_akta_lhr").val("");
        $("#mdl_no_akta_lhr").attr("disabled",true);
        $("#go_send").attr("disabled",true);
        $('#do_akta_lhr').click();
    }
    function do_close() {
        $('#btn-reset').click();
        $('#closemodal').click();
        
    }
    function onClearModal() {
        $("#mdl_ststus_akta").val("1");
        $("#mdl_no_akta_lhr").val("");
        $("#mdl_no_akta_lhr").attr("disabled",true);
        $("#go_send").attr("disabled",true);
        $('#btn-reset').click();
    }
    function validation() {
            var mdl_nik = $("#mdl_nik");
            if (mdl_nik.val().length == 0) {
              mdl_nik.select();
                toastr.warning('Nik tidak boleh kosong');
                return false;
            }
            var mdl_ststus_akta = $("#mdl_ststus_akta");
            if (mdl_ststus_akta.val().length == 0) {
                toastr.warning('Status Akta tidak boleh kosong');
                mdl_ststus_akta.select();
            }
            var mdl_no_akta_lhr = $("#mdl_no_akta_lhr");
            if (mdl_no_akta_lhr.val().length == 0) {
                toastr.warning('No Akta tidak boleh kosong');
                mdl_no_akta_lhr.select();
                return false;
            } 
            if (mdl_no_akta_lhr.val().length > 0 && mdl_no_akta_lhr.val().length < 3) {
                toastr.warning('No Akta Lahir terlalu pendek');
                mdl_no_akta_lhr.select();
                return false;
            } 
           

            return true;
    }
     function do_save(){
         if (!validation()) return;
           $.ajax({
                type: "post",
                url: BASE_URL+"Antrian/edit_akta_sms",
                data: {
                  "_token": "{{ csrf_token() }}",
                  nik :  $("#mdl_nik").val(),
                  akta_lhr :  $("#mdl_ststus_akta").val(),
                  no_akta_lhr :  $("#mdl_no_akta_lhr").val(),
                  user_id :  "<?php echo $user_id; ?>"
                },
                beforeSend:
                function () {
                  document.getElementById("go_send").disabled = true;
                  $('#go_send').html("<i class='fa fa-circle-o-notch fa-spin fa-fw'></i> Wait");
                },
                success: function (data) {

                    if (data.message_type == 1){
                    toastr.success(data.message);
                    }else if (data.message_type == 2){
                      toastr.error(data.message);

                    }
                    
                },
                error:
                function (data) {
                    toastr.error("Please Try Again, Something Went Wrong");
                    do_close();

                },
                complete:
                function (response) {
                  document.getElementById("go_send").disabled = false;
                  $('#go_send').html("Kirim");
                  do_close();

                }
            });

        }
    </script>