 <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $stitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                        <ol class="breadcrumb">
                           <li class="text-info"><?php echo $stitle; ?></li>
                        </ol>
                    </div>
                </div>

                <div class="row">
                 <form name ="get_form" action="<?php echo $my_url; ?>" method="post">
                @csrf
                <div class="col-sm-12">
                    <div class="white-box">
                        <div class="row">
                            <div class="col-lg-12">         
                                <div class="form-group">
                                    <h3 class="box-title m-b-0"><b>Bulan Laporan</b></h3>
                                    <input class="form-control" type="text" id="bulan" data-mask="99/9999" name="bulan" onkeypress="return isNumberKey(event)" onchange="onlyNum()" />
                                    </select>
                        
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">         
                                <div class="form-group">
                                    <h3 class="box-title m-b-0"><b>Tipe Data</b></h3>
                                    <select class="form-control select2" name="tipe_data" id="tipe_data">
                                         <option  value="0">-- Pilih Tipe --</option>
                                         <option  value="1">Data</option>
                                         <option  value="2">Statistik</option>
                                        </select>
                        
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">         
                                <div class="form-group">
                                    <h3 class="box-title m-b-0"><b>Kategori</b></h3>
                                    <select class="form-control select2" name="kategori_data" id="kategori_data">
                                         <option  value="0">-- Pilih Kategori --</option>
                                         <option  value="1">1. Pemula</option>
                                         <option  value="2">2. Datang</option>
                                         <option  value="3">3. Tni Polri</option>
                                         <option  value="4">4. Pindah Polri</option>
                                         <option  value="5">5. Akta Kematian Polri</option>
                                        </select>
                        
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <button type="submit" class="btn btn-info waves-effect waves-light m-r-10 pull-right" id="btn-filter" onclick="on_serach();" >Search <i class="mdi  mdi-magnify fa-fw"></i></button>
                              <button type="button" class="btn btn-invert waves-effect waves-light m-r-10 pull-right" id="btn-reset" onclick="on_clear();">Clear <i class="mdi  mdi-delete fa-fw"></i></button>
                        </div>
                    </div>
                    </form>
                </div>
                @if (!empty($data))
                    @if($tipe_data == 1)
                        @if($kategori_data == 1)
                            <div class="col-md-12">
                                    <div class="white-box">
                                        <h3 class="box-title" id="title_text">1. Data Pemula Pleno Bulan {{ $bulan }}</h3>
                                        <div class="scrollable">
                                            <div class="table-responsive">
                                                <table id="group-list" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                               <thead>
                                                
                                                    <tr>
                                                        <th width="20%" style="text-align: center;">Nik</th>
                                                        <th width="20%" style="text-align: center;">Nama Lgkp</th>
                                                        <th width="20%" style="text-align: center;">Jenis Klmin</th>
                                                        <th width="20%" style="text-align: center;">No Kec</th>
                                                        <th width="20%" style="text-align: center;">Nama Kec</th>
                                                        <th width="20%" style="text-align: center;">No Kel</th>
                                                        <th width="20%" style="text-align: center;">Nama Kel</th>
                                                        <th width="20%" style="text-align: center;">Stat Kwn</th>
                                                        <th width="20%" style="text-align: center;">Stat Rekam</th>
                                                        <th width="20%" style="text-align: center;">Pnydng Cct</th>

                                                    </tr>
                                                   
                                                </thead>
                                                <tbody id="show_data">
                                           @foreach($data as $row)
                                             <tr>
                                                <td width="20%" style="text-align: center;">{{ $row->nik }}</td>
                                                <td width="20%" style="text-align: center;">{{ $row->nama_lgkp }}</td>
                                                <td width="20%" style="text-align: center;">{{ $row->jenis_klmin }}</td>
                                                <td width="20%" style="text-align: center;">{{ $row->no_kec }}</td>
                                                <td width="20%" style="text-align: center;">{{ $row->nama_kec }}</td>
                                                <td width="20%" style="text-align: center;">{{ $row->no_kel }}</td>
                                                <td width="20%" style="text-align: center;">{{ $row->nama_kel }}</td>
                                                <td width="20%" style="text-align: center;">{{ $row->stat_kwn }}</td>
                                                <td width="20%" style="text-align: center;">{{ $row->stat_rekam }}</td>
                                                <td width="20%" style="text-align: center;">{{ $row->pnydng_cct }}</td>
                                            </tr>
                                         @endforeach
                                    </tbody>
                                    
                                            </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @elseif($kategori_data == 2)
                            <div class="col-md-12">
                                    <div class="white-box">
                                        <h3 class="box-title" id="title_text">2. Data Kedatangan Pleno Bulan {{ $bulan }}</h3>
                                        <div class="scrollable">
                                            <div class="table-responsive">
                                                <table id="group-list" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                               <thead>
                                                
                                                    <tr>
                                                        <th width="20%" style="text-align: center;">Nik</th>
                                                        <th width="20%" style="text-align: center;">Nama Lgkp</th>
                                                        <th width="20%" style="text-align: center;">No Kec</th>
                                                        <th width="20%" style="text-align: center;">Nama Kec</th>
                                                        <th width="20%" style="text-align: center;">No Kel</th>
                                                        <th width="20%" style="text-align: center;">Nama Kel</th>
                                                        <th width="20%" style="text-align: center;">Jenis Klmin</th>
                                                        <th width="20%" style="text-align: center;">Bulan</th>
                                                        <th width="20%" style="text-align: center;">Tahun</th>
                                                        <th width="20%" style="text-align: center;">Stat Kwn</th>
                                                        <th width="20%" style="text-align: center;">Stat Rekam</th>
                                                        <th width="20%" style="text-align: center;">Pnydng Cct</th>

                                                    </tr>
                                                   
                                                </thead>
                                                <tbody id="show_data">
                                           @foreach($data as $row)
                                             <tr>
                                                <td width="20%" style="text-align: center;">{{ $row->nik }}</td>
                                                <td width="20%" style="text-align: center;">{{ $row->nama_lengkap }}</td>
                                                <td width="20%" style="text-align: center;">{{ $row->no_kec }}</td>
                                                <td width="20%" style="text-align: center;">{{ $row->nama_kec }}</td>
                                                <td width="20%" style="text-align: center;">{{ $row->no_kel }}</td>
                                                <td width="20%" style="text-align: center;">{{ $row->nama_kel }}</td>
                                                <td width="20%" style="text-align: center;">{{ $row->jenis_klmin }}</td>
                                                <td width="20%" style="text-align: center;">{{ $row->bln }}</td>
                                                <td width="20%" style="text-align: center;">{{ $row->thn }}</td>
                                                <td width="20%" style="text-align: center;">{{ $row->stat_kwn }}</td>
                                                <td width="20%" style="text-align: center;">{{ $row->stat_rekam }}</td>
                                                <td width="20%" style="text-align: center;">{{ $row->pnydng_cct }}</td>
                                            </tr>
                                         @endforeach
                                    </tbody>
                                    
                                            </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @elseif($kategori_data == 3)
                            <div class="col-md-12">
                                    <div class="white-box">
                                        <h3 class="box-title" id="title_text">3. Data Tni/Polri Pleno Bulan {{ $bulan }}</h3>
                                        <div class="scrollable">
                                            <div class="table-responsive">
                                                <table id="group-list" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                               <thead>
                                                
                                                    <tr>
                                                        <th width="20%" style="text-align: center;">Nik</th>
                                                        <th width="20%" style="text-align: center;">Nama Lgkp</th>
                                                        <th width="20%" style="text-align: center;">Jenis Klmin</th>
                                                        <th width="20%" style="text-align: center;">No Kec</th>
                                                        <th width="20%" style="text-align: center;">Nama Kec</th>
                                                        <th width="20%" style="text-align: center;">No Kel</th>
                                                        <th width="20%" style="text-align: center;">Nama Kel</th>
                                                        <th width="20%" style="text-align: center;">Stat Kwn</th>
                                                        <th width="20%" style="text-align: center;">Stat Rekam</th>
                                                        <th width="20%" style="text-align: center;">Pnydng Cct</th>

                                                    </tr>
                                                   
                                                </thead>
                                                <tbody id="show_data">
                                           @foreach($data as $row)
                                             <tr>
                                                <td width="20%" style="text-align: center;">{{ $row->nik }}</td>
                                                <td width="20%" style="text-align: center;">{{ $row->nama_lgkp }}</td>
                                                <td width="20%" style="text-align: center;">{{ $row->jenis_klmin }}</td>
                                                <td width="20%" style="text-align: center;">{{ $row->no_kec }}</td>
                                                <td width="20%" style="text-align: center;">{{ $row->nama_kec }}</td>
                                                <td width="20%" style="text-align: center;">{{ $row->no_kel }}</td>
                                                <td width="20%" style="text-align: center;">{{ $row->nama_kel }}</td>
                                                <td width="20%" style="text-align: center;">{{ $row->stat_kwn }}</td>
                                                <td width="20%" style="text-align: center;">{{ $row->stat_rekam }}</td>
                                                <td width="20%" style="text-align: center;">{{ $row->pnydng_cct }}</td>
                                            </tr>
                                         @endforeach
                                    </tbody>
                                    
                                            </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                @elseif($kategori_data == 4)
                            <div class="col-md-12">
                                    <div class="white-box">
                                        <h3 class="box-title" id="title_text">4. Data Perpindahan Pleno Bulan {{ $bulan }}</h3>
                                        <div class="scrollable">
                                            <div class="table-responsive">
                                                <table id="group-list" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                               <thead>
                                                
                                                    <tr>
                                                        <th width="20%" style="text-align: center;">Nik</th>
                                                        <th width="20%" style="text-align: center;">Nama Lgkp</th>
                                                        <th width="20%" style="text-align: center;">No Kec</th>
                                                        <th width="20%" style="text-align: center;">Nama Kec</th>
                                                        <th width="20%" style="text-align: center;">No Kel</th>
                                                        <th width="20%" style="text-align: center;">Nama Kel</th>
                                                        <th width="20%" style="text-align: center;">Jenis Klmin</th>
                                                        <th width="20%" style="text-align: center;">Bulan</th>
                                                        <th width="20%" style="text-align: center;">Tahun</th>
                                                        <th width="20%" style="text-align: center;">Stat Kwn</th>
                                                        <th width="20%" style="text-align: center;">Stat Rekam</th>
                                                        <th width="20%" style="text-align: center;">Pnydng Cct</th>

                                                    </tr>
                                                   
                                                </thead>
                                                <tbody id="show_data">
                                           @foreach($data as $row)
                                             <tr>
                                                <td width="20%" style="text-align: center;">{{ $row->nik }}</td>
                                                <td width="20%" style="text-align: center;">{{ $row->nama_lengkap }}</td>
                                                <td width="20%" style="text-align: center;">{{ $row->no_kec }}</td>
                                                <td width="20%" style="text-align: center;">{{ $row->nama_kec }}</td>
                                                <td width="20%" style="text-align: center;">{{ $row->no_kel }}</td>
                                                <td width="20%" style="text-align: center;">{{ $row->nama_kel }}</td>
                                                <td width="20%" style="text-align: center;">{{ $row->jenis_klmin }}</td>
                                                <td width="20%" style="text-align: center;">{{ $row->bln }}</td>
                                                <td width="20%" style="text-align: center;">{{ $row->thn }}</td>
                                                <td width="20%" style="text-align: center;">{{ $row->stat_kwn }}</td>
                                                <td width="20%" style="text-align: center;">{{ $row->stat_rekam }}</td>
                                                <td width="20%" style="text-align: center;">{{ $row->pnydng_cct }}</td>
                                            </tr>
                                         @endforeach
                                    </tbody>
                                    
                                            </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                @elseif($kategori_data == 5)
                            <div class="col-md-12">
                                    <div class="white-box">
                                        <h3 class="box-title" id="title_text">5. Data Akta Kematian Pleno Bulan {{ $bulan }}</h3>
                                        <div class="scrollable">
                                            <div class="table-responsive">
                                                <table id="group-list" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                               <thead>
                                                
                                                    <tr>
                                                        <th width="20%" style="text-align: center;">Mati Nik</th>
                                                        <th width="20%" style="text-align: center;">Mati Nama Lgkp</th>
                                                        <th width="20%" style="text-align: center;">Mati Jenis Klmin</th>
                                                        <th width="20%" style="text-align: center;">Adm No Kec</th>
                                                        <th width="20%" style="text-align: center;">Adm Nama Kec</th>
                                                        <th width="20%" style="text-align: center;">Adm No Kel</th>
                                                        <th width="20%" style="text-align: center;">Adm Nama Kel</th>
                                                        <th width="20%" style="text-align: center;">Stat Kwn</th>
                                                        <th width="20%" style="text-align: center;">Stat Rekam</th>
                                                        <th width="20%" style="text-align: center;">Pnydng Cct</th>

                                                    </tr>
                                                   
                                                </thead>
                                                <tbody id="show_data">
                                           @foreach($data as $row)
                                             <tr>
                                                <td width="20%" style="text-align: center;">{{ $row->mati_nik }}</td>
                                                <td width="20%" style="text-align: center;">{{ $row->mati_nama_lgkp }}</td>
                                                <td width="20%" style="text-align: center;">{{ $row->mati_jns_kelamin }}</td>
                                                <td width="20%" style="text-align: center;">{{ $row->adm_no_kec }}</td>
                                                <td width="20%" style="text-align: center;">{{ $row->adm_nama_kec }}</td>
                                                <td width="20%" style="text-align: center;">{{ $row->adm_no_kel }}</td>
                                                <td width="20%" style="text-align: center;">{{ $row->adm_nama_kel }}</td>
                                                <td width="20%" style="text-align: center;">{{ $row->stat_kwn }}</td>
                                                <td width="20%" style="text-align: center;">{{ $row->stat_rekam }}</td>
                                                <td width="20%" style="text-align: center;">{{ $row->pnydng_cct }}</td>
                                            </tr>
                                         @endforeach
                                    </tbody>
                                    
                                            </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif

                        @elseif($tipe_data == 2)
                            <div class="col-md-12">
                                    <div class="white-box">
                                        <h3 class="box-title" id="title_text">5. Data Akta Kematian Pleno Bulan {{ $bulan }}</h3>
                                        <div class="scrollable">
                                            <div class="table-responsive">
                                                <table id="group-list" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                               <thead>
                                                
                                                    <tr>
                                                        <th width="20%" style="text-align: center;">No Kec</th>
                                                        <th width="20%" style="text-align: center;">Nama Kec</th>
                                                        <th width="20%" style="text-align: center;">No Kel</th>
                                                        <th width="20%" style="text-align: center;">Nama Kel</th>
                                                        <th width="20%" style="text-align: center;">Belum Kawin</th>
                                                        <th width="20%" style="text-align: center;">Sudah Kawin</th>
                                                        <th width="20%" style="text-align: center;">Pernah Kawin</th>
                                                        <th width="20%" style="text-align: center;">Belum Rekam</th>
                                                        <th width="20%" style="text-align: center;">Sudah Rekam</th>
                                                        <th width="20%" style="text-align: center;">Disabilitas</th>

                                                    </tr>
                                                   
                                                </thead>
                                                <tbody id="show_data">
                                            @php $jum_blm_kawin = 0 @endphp
                                            @php $jum_sdh_kawin = 0 @endphp
                                            @php $jum_prnh_kawin = 0 @endphp
                                            @php $jum_blm_rekam = 0 @endphp
                                            @php $jum_sdh_rekam = 0 @endphp
                                            @php $jum_disab = 0 @endphp
                                           @foreach($data as $row)
                                                @php $jum_blm_kawin = $jum_blm_kawin + $row->belum_kawin @endphp
                                                @php $jum_sdh_kawin = $jum_sdh_kawin + $row->sudah_kawin @endphp
                                                @php $jum_prnh_kawin = $jum_prnh_kawin + $row->pernah_kawin @endphp
                                                @php $jum_blm_rekam = $jum_blm_rekam + $row->belum_rekam @endphp
                                                @php $jum_sdh_rekam = $jum_sdh_rekam + $row->sudah_rekam @endphp
                                                @php $jum_disab = $jum_disab + $row->disabilitas @endphp
                                             <tr>
                                                <td width="20%" style="text-align: center;">{{ $row->no_kec }}</td>
                                                <td width="20%" style="text-align: center;">{{ $row->nama_kec }}</td>
                                                <td width="20%" style="text-align: center;">{{ $row->no_kel }}</td>
                                                <td width="20%" style="text-align: center;">{{ $row->nama_kel }}</td>
                                                <td width="20%" style="text-align: center;">{{ $row->belum_kawin }}</td>
                                                <td width="20%" style="text-align: center;">{{ $row->sudah_kawin }}</td>
                                                <td width="20%" style="text-align: center;">{{ $row->pernah_kawin }}</td>
                                                <td width="20%" style="text-align: center;">{{ $row->belum_rekam }}</td>
                                                <td width="20%" style="text-align: center;">{{ $row->sudah_rekam }}</td>
                                                <td width="20%" style="text-align: center;">{{ $row->disabilitas }}</td>
                                            </tr>
                                         @endforeach
                                    </tbody>
                                    <tfoot id="my_foot">
                                         <tr>
                                            <th width="55%" colspan="4" style="text-align: center;">Jumlah</th>
                                            <th width="15%" style="text-align: center;">{{ number_format(htmlentities($jum_blm_kawin),0,',','.') }}</th>
                                            <th width="15%" style="text-align: center;">{{ number_format(htmlentities($jum_sdh_kawin),0,',','.') }}</th>
                                            <th width="15%" style="text-align: center;">{{ number_format(htmlentities($jum_prnh_kawin),0,',','.') }}</th>
                                            <th width="15%" style="text-align: center;">{{ number_format(htmlentities($jum_blm_rekam),0,',','.') }}</th>
                                            <th width="15%" style="text-align: center;">{{ number_format(htmlentities($jum_sdh_rekam),0,',','.') }}</th>
                                            <th width="15%" style="text-align: center;">{{ number_format(htmlentities($jum_disab),0,',','.') }}</th>
                                        </tr>
                                    </tfoot>
                                            </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        @endif
                    @endif
                </div>

            </div>
       @include('shared.footer_detail')