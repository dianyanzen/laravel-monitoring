     <script>
        function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode != 44 && charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }
       
        $(document).ready(function() {
            get_table();
        });
    function get_table() {
        $('#group-list').DataTable({
             "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            dom: 'Bfrtip',
            "pageLength" : 50,
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        });
       
    }
    function validation(){
      var bulan = $("#bulan");
        if (bulan.val().length != 7) {
            return false;
        }
        return true
    }
    jQuery(document).ready(function() {
        $(".select2").select2();
        $('#tanggal').val("<?php echo date('d-m-Y');?>");
        });
     function block_screen(){
        
        $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff'
            },
            baseZ: 2000
        }); 
    }
    function unblock_screen(){
       $.unblockUI();
    }
    </script>