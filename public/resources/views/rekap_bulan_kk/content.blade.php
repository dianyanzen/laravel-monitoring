 <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $stitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                        <ol class="breadcrumb">
                            <li class="active"><?php echo $mtitle; ?></li>
                        </ol>
                    </div>
                </div>
                 <div class="row">
                <form name ="get_form" action="<?php echo $my_url; ?>" method="post">
                    @csrf
                    <div class="col-sm-12">
                        <div class="white-box">
                            <div class="row">
                                <div class="col-lg-3">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_kec" type="checkbox" checked disabled="true">
                                            <label for="cb_kec"> Bulan</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3">         
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="tanggal" name = "tanggal" value="<?php echo date('m-Y');?>" placeholder="MM-YYYY" readonly>
                                    </div>
                                </div>
                                
                            </div>
                            <div class="row">
                                <div class="col-lg-3">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_kec" type="checkbox" checked disabled="true">
                                            <label for="cb_kec"> Kecamatan</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3">         
                                    <div class="form-group">
                                        <select class="form-control select2" name="no_kec" id="no_kec">
                                         <option  value="0">-- Pilih Kecamatan --</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <button type="submit" class="btn btn-info waves-effect waves-light m-r-10 pull-right" id="btn-filter" onclick="on_serach();" >Search <i class="mdi  mdi-magnify fa-fw"></i></button>
                                  <button type="button" class="btn btn-invert waves-effect waves-light m-r-10 pull-right" id="btn-reset" onclick="on_clear();">Clear <i class="mdi  mdi-delete fa-fw"></i></button>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
                <div class="row">
                <div class="col-md-12">
                        <div class="white-box">
                            <h3 class="box-title"><?php echo $mtitle; ?></h3>
                            <div class="scrollable">
                                <div class="table-responsive">
                                    <table id="mytable" class="display nowrap table-bordered color-table info-table" cellspacing="0" width="100%">
                                   <thead>
                                    
                                        <tr>
                                            <th style="text-align: center;">No</th>
                                             <?php if (isset($data[0]->printed_date)){ ?>
                                            <th style="text-align: center;">TANGGAL CETAK</th>
                                            <?php } ?>

                                             <?php if (isset($data[0]->c1)){ ?>
                                                <?php if (!empty($nama_wil[0])){echo '<th style="text-align: center;">'.$nama_wil[0]->nama_wil.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->c2)){ ?>
                                                <?php if (!empty($nama_wil[1])){echo '<th style="text-align: center;">'.$nama_wil[1]->nama_wil.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->c3)){ ?>
                                                <?php if (!empty($nama_wil[2])){echo '<th style="text-align: center;">'.$nama_wil[2]->nama_wil.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->c4)){ ?>
                                                <?php if (!empty($nama_wil[3])){echo '<th style="text-align: center;">'.$nama_wil[3]->nama_wil.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->c5)){ ?>
                                                <?php if (!empty($nama_wil[4])){echo '<th style="text-align: center;">'.$nama_wil[4]->nama_wil.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->c6)){ ?>
                                                <?php if (!empty($nama_wil[5])){echo '<th style="text-align: center;">'.$nama_wil[5]->nama_wil.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->c7)){ ?>
                                                <?php if (!empty($nama_wil[6])){echo '<th style="text-align: center;">'.$nama_wil[6]->nama_wil.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->c8)){ ?>
                                                <?php if (!empty($nama_wil[7])){echo '<th style="text-align: center;">'.$nama_wil[7]->nama_wil.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->c9)){ ?>
                                                <?php if (!empty($nama_wil[8])){echo '<th style="text-align: center;">'.$nama_wil[8]->nama_wil.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->c10)){ ?>
                                                <?php if (!empty($nama_wil[9])){echo '<th style="text-align: center;">'.$nama_wil[9]->nama_wil.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->c11)){ ?>
                                                <?php if (!empty($nama_wil[10])){echo '<th style="text-align: center;">'.$nama_wil[10]->nama_wil.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->c12)){ ?>
                                                <?php if (!empty($nama_wil[11])){echo '<th style="text-align: center;">'.$nama_wil[11]->nama_wil.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->c13)){ ?>
                                                <?php if (!empty($nama_wil[12])){echo '<th style="text-align: center;">'.$nama_wil[12]->nama_wil.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->c14)){ ?>
                                                <?php if (!empty($nama_wil[13])){echo '<th style="text-align: center;">'.$nama_wil[13]->nama_wil.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->c15)){ ?>
                                                <?php if (!empty($nama_wil[14])){echo '<th style="text-align: center;">'.$nama_wil[14]->nama_wil.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->c16)){ ?>
                                                <?php if (!empty($nama_wil[15])){echo '<th style="text-align: center;">'.$nama_wil[15]->nama_wil.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->c17)){ ?>
                                                <?php if (!empty($nama_wil[16])){echo '<th style="text-align: center;">'.$nama_wil[16]->nama_wil.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->c18)){ ?>
                                                <?php if (!empty($nama_wil[17])){echo '<th style="text-align: center;">'.$nama_wil[17]->nama_wil.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->c19)){ ?>
                                                <?php if (!empty($nama_wil[18])){echo '<th style="text-align: center;">'.$nama_wil[18]->nama_wil.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->c20)){ ?>
                                                <?php if (!empty($nama_wil[19])){echo '<th style="text-align: center;">'.$nama_wil[19]->nama_wil.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->c21)){ ?>
                                                <?php if (!empty($nama_wil[20])){echo '<th style="text-align: center;">'.$nama_wil[20]->nama_wil.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->c22)){ ?>
                                                <?php if (!empty($nama_wil[21])){echo '<th style="text-align: center;">'.$nama_wil[21]->nama_wil.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->c23)){ ?>
                                                <?php if (!empty($nama_wil[22])){echo '<th style="text-align: center;">'.$nama_wil[22]->nama_wil.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->c24)){ ?>
                                                <?php if (!empty($nama_wil[23])){echo '<th style="text-align: center;">'.$nama_wil[23]->nama_wil.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->c25)){ ?>
                                                <?php if (!empty($nama_wil[24])){echo '<th style="text-align: center;">'.$nama_wil[24]->nama_wil.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->c26)){ ?>
                                                <?php if (!empty($nama_wil[25])){echo '<th style="text-align: center;">'.$nama_wil[25]->nama_wil.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->c27)){ ?>
                                                <?php if (!empty($nama_wil[26])){echo '<th style="text-align: center;">'.$nama_wil[26]->nama_wil.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->c28)){ ?>
                                                <?php if (!empty($nama_wil[27])){echo '<th style="text-align: center;">'.$nama_wil[27]->nama_wil.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->c29)){ ?>
                                                <?php if (!empty($nama_wil[28])){echo '<th style="text-align: center;">'.$nama_wil[28]->nama_wil.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->c30)){ ?>
                                                <?php if (!empty($nama_wil[29])){echo '<th style="text-align: center;">'.$nama_wil[29]->nama_wil.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->c31)){ ?>
                                                <?php if (!empty($nama_wil[30])){echo '<th style="text-align: center;">'.$nama_wil[30]->nama_wil.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->c32)){ ?>
                                                <?php if (!empty($nama_wil[31])){echo '<th style="text-align: center;">'.$nama_wil[31]->nama_wil.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->c33)){ ?>
                                                <?php if (!empty($nama_wil[32])){echo '<th style="text-align: center;">'.$nama_wil[32]->nama_wil.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->c34)){ ?>
                                                <?php if (!empty($nama_wil[33])){echo '<th style="text-align: center;">'.$nama_wil[33]->nama_wil.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->c35)){ ?>
                                                <?php if (!empty($nama_wil[34])){echo '<th style="text-align: center;">'.$nama_wil[34]->nama_wil.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->c36)){ ?>
                                                <?php if (!empty($nama_wil[35])){echo '<th style="text-align: center;">'.$nama_wil[35]->nama_wil.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->c37)){ ?>
                                                <?php if (!empty($nama_wil[36])){echo '<th style="text-align: center;">'.$nama_wil[36]->nama_wil.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->c38)){ ?>
                                                <?php if (!empty($nama_wil[37])){echo '<th style="text-align: center;">'.$nama_wil[37]->nama_wil.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->c39)){ ?>
                                                <?php if (!empty($nama_wil[38])){echo '<th style="text-align: center;">'.$nama_wil[38]->nama_wil.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->c40)){ ?>
                                                <?php if (!empty($nama_wil[39])){echo '<th style="text-align: center;">'.$nama_wil[39]->nama_wil.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->c41)){ ?>
                                                <?php if (!empty($nama_wil[40])){echo '<th style="text-align: center;">'.$nama_wil[40]->nama_wil.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->c42)){ ?>
                                                <?php if (!empty($nama_wil[41])){echo '<th style="text-align: center;">'.$nama_wil[41]->nama_wil.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->c43)){ ?>
                                                <?php if (!empty($nama_wil[42])){echo '<th style="text-align: center;">'.$nama_wil[42]->nama_wil.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->c44)){ ?>
                                                <?php if (!empty($nama_wil[43])){echo '<th style="text-align: center;">'.$nama_wil[43]->nama_wil.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->c45)){ ?>
                                                <?php if (!empty($nama_wil[44])){echo '<th style="text-align: center;">'.$nama_wil[44]->nama_wil.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->c46)){ ?>
                                                <?php if (!empty($nama_wil[45])){echo '<th style="text-align: center;">'.$nama_wil[45]->nama_wil.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->c47)){ ?>
                                                <?php if (!empty($nama_wil[46])){echo '<th style="text-align: center;">'.$nama_wil[46]->nama_wil.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->c48)){ ?>
                                                <?php if (!empty($nama_wil[47])){echo '<th style="text-align: center;">'.$nama_wil[47]->nama_wil.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->c49)){ ?>
                                                <?php if (!empty($nama_wil[48])){echo '<th style="text-align: center;">'.$nama_wil[48]->nama_wil.'</th>'; }?>
                                            <?php } ?>

                                             <?php if (isset($data[0]->c50)){ ?>
                                                <?php if (!empty($nama_wil[49])){echo '<th style="text-align: center;">'.$nama_wil[49]->nama_wil.'</th>'; }?>
                                            <?php } ?>
                                            <th style="text-align: center;">JUMLAH</th>
                                        </tr>
                                       
                                    </thead>
                                    <tbody id="show_data">
                                       <?php
                                        if (!empty($data)){
                                            $i=0;
                                            $j1=0;
                                            $j2=0;
                                            $j3=0;
                                            $j4=0;
                                            $j5=0;
                                            $j6=0;
                                            $j7=0;
                                            $j8=0;
                                            $j9=0;
                                            $j10=0;
                                            $j11=0;
                                            $j12=0;
                                            $j13=0;
                                            $j14=0;
                                            $j15=0;
                                            $j16=0;
                                            $j17=0;
                                            $j18=0;
                                            $j19=0;
                                            $j20=0;
                                            $j21=0;
                                            $j22=0;
                                            $j23=0;
                                            $j24=0;
                                            $j25=0;
                                            $j26=0;
                                            $j27=0;
                                            $j28=0;
                                            $j29=0;
                                            $j30=0;
                                            $j31=0;
                                            $j32=0;
                                            $j33=0;
                                            $j34=0;
                                            $j35=0;
                                            $j36=0;
                                            $j37=0;
                                            $j38=0;
                                            $j39=0;
                                            $j40=0;
                                            $j41=0;
                                            $j42=0;
                                            $j43=0;
                                            $j44=0;
                                            $j45=0;
                                            $j46=0;
                                            $j47=0;
                                            $j48=0;
                                            $j49=0;
                                            $j50=0;
                                            $jum_all=0;
                                           foreach($data as $row){
                                            $i++;
                                            $jum = 0;
                                            ?>
                                             <tr>
                                                <td style="text-align: center;">
                                                <?php echo $i ;?>
                                                </td>

                                                <?php if (isset($row->printed_date)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->printed_date ;?>
                                                </td>
                                                <?php } ?>

                                                <?php if (isset($row->c1)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->c1 ;?>
                                                <?php $jum = $jum + $row->c1;?>
                                                <?php $j1 = $j1 + $row->c1;?>
                                                </td>
                                                <?php } ?>

                                                <?php if (isset($row->c2)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->c2 ;?>
                                                <?php $jum = $jum + $row->c2;?>
                                                <?php $j2 = $j2 + $row->c2;?>
                                                </td>
                                                <?php } ?>
                                                
                                                <?php if (isset($row->c3)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->c3 ;?>
                                                <?php $jum = $jum + $row->c3;?>
                                                <?php $j3 = $j3 + $row->c3;?>
                                                </td>
                                                <?php } ?>
                                                
                                                <?php if (isset($row->c4)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->c4 ;?>
                                                <?php $jum = $jum + $row->c4;?>
                                                <?php $j4 = $j4 + $row->c4;?>
                                                </td>
                                                <?php } ?>
                                                
                                                <?php if (isset($row->c5)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->c5 ;?>
                                                <?php $jum = $jum + $row->c5;?>
                                                <?php $j5 = $j5 + $row->c5;?>
                                                </td>
                                                <?php } ?>
                                                
                                                <?php if (isset($row->c6)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->c6 ;?>
                                                <?php $jum = $jum + $row->c6;?>
                                                <?php $j6 = $j6 + $row->c6;?>
                                                </td>
                                                <?php } ?>
                                                
                                                <?php if (isset($row->c7)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->c7 ;?>
                                                <?php $jum = $jum + $row->c7;?>
                                                <?php $j7 = $j7 + $row->c7;?>
                                                </td>
                                                <?php } ?>
                                                
                                                <?php if (isset($row->c8)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->c8 ;?>
                                                <?php $jum = $jum + $row->c8;?>
                                                <?php $j8 = $j8 + $row->c8;?>
                                                </td>
                                                <?php } ?>
                                                
                                                <?php if (isset($row->c9)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->c9 ;?>
                                                <?php $jum = $jum + $row->c9;?>
                                                <?php $j9 = $j9 + $row->c9;?>
                                                </td>
                                                <?php } ?>
                                                
                                                <?php if (isset($row->c10)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->c10 ;?>
                                                <?php $jum = $jum + $row->c10;?>
                                                <?php $j10 = $j10 + $row->c10;?>
                                                </td>
                                                <?php } ?>

                                                <?php if (isset($row->c11)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->c11 ;?>
                                                <?php $jum = $jum + $row->c11;?>
                                                <?php $j11 = $j11 + $row->c11;?>
                                                </td>
                                                <?php } ?>

                                                <?php if (isset($row->c12)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->c12 ;?>
                                                <?php $jum = $jum + $row->c12;?>
                                                <?php $j12 = $j12 + $row->c12;?>
                                                </td>
                                                <?php } ?>
                                                
                                                <?php if (isset($row->c13)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->c13 ;?>
                                                <?php $jum = $jum + $row->c13;?>
                                                <?php $j13 = $j13 + $row->c13;?>
                                                </td>
                                                <?php } ?>
                                                
                                                <?php if (isset($row->c14)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->c14 ;?>
                                                <?php $jum = $jum + $row->c14;?>
                                                <?php $j14 = $j14 + $row->c14;?>
                                                </td>
                                                <?php } ?>
                                                
                                                <?php if (isset($row->c15)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->c15 ;?>
                                                <?php $jum = $jum + $row->c15;?>
                                                <?php $j15 = $j15 + $row->c15;?>
                                                </td>
                                                <?php } ?>
                                                
                                                <?php if (isset($row->c16)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->c16 ;?>
                                                <?php $jum = $jum + $row->c16;?>
                                                <?php $j16 = $j16 + $row->c16;?>
                                                </td>
                                                <?php } ?>
                                                
                                                <?php if (isset($row->c17)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->c17 ;?>
                                                <?php $jum = $jum + $row->c17;?>
                                                <?php $j17 = $j17 + $row->c17;?>
                                                </td>
                                                <?php } ?>
                                                
                                                <?php if (isset($row->c18)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->c18 ;?>
                                                <?php $jum = $jum + $row->c18;?>
                                                <?php $j18 = $j18 + $row->c18;?>
                                                </td>
                                                <?php } ?>
                                                
                                                <?php if (isset($row->c19)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->c19 ;?>
                                                <?php $jum = $jum + $row->c19;?>
                                                <?php $j19 = $j19 + $row->c19;?>
                                                </td>
                                                <?php } ?>
                                                
                                                <?php if (isset($row->c20)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->c20 ;?>
                                                <?php $jum = $jum + $row->c20;?>
                                                <?php $j20 = $j20 + $row->c20;?>
                                                </td>
                                                <?php } ?>

                                                <?php if (isset($row->c21)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->c21 ;?>
                                                <?php $jum = $jum + $row->c21;?>
                                                <?php $j21 = $j21 + $row->c21;?>
                                                </td>
                                                <?php } ?>

                                                <?php if (isset($row->c22)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->c22 ;?>
                                                <?php $jum = $jum + $row->c22;?>
                                                <?php $j22 = $j22 + $row->c22;?>
                                                </td>
                                                <?php } ?>
                                                
                                                <?php if (isset($row->c23)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->c23 ;?>
                                                <?php $jum = $jum + $row->c23;?>
                                                <?php $j23 = $j23 + $row->c23;?>
                                                </td>
                                                <?php } ?>
                                                
                                                <?php if (isset($row->c24)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->c24 ;?>
                                                <?php $jum = $jum + $row->c24;?>
                                                <?php $j24 = $j24 + $row->c24;?>
                                                </td>
                                                <?php } ?>
                                                
                                                <?php if (isset($row->c25)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->c25 ;?>
                                                <?php $jum = $jum + $row->c25;?>
                                                <?php $j25 = $j25 + $row->c25;?>
                                                </td>
                                                <?php } ?>
                                                
                                                <?php if (isset($row->c26)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->c26 ;?>
                                                <?php $jum = $jum + $row->c26;?>
                                                <?php $j26 = $j26 + $row->c26;?>
                                                </td>
                                                <?php } ?>
                                                
                                                <?php if (isset($row->c27)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->c27 ;?>
                                                <?php $jum = $jum + $row->c27;?>
                                                <?php $j27 = $j27 + $row->c27;?>
                                                </td>
                                                <?php } ?>
                                                
                                                <?php if (isset($row->c28)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->c28 ;?>
                                                <?php $jum = $jum + $row->c28;?>
                                                <?php $j28 = $j28 + $row->c28;?>
                                                </td>
                                                <?php } ?>
                                                
                                                <?php if (isset($row->c29)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->c29 ;?>
                                                <?php $jum = $jum + $row->c29;?>
                                                <?php $j29 = $j29 + $row->c29;?>
                                                </td>
                                                <?php } ?>
                                                
                                                <?php if (isset($row->c30)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->c30 ;?>
                                                <?php $jum = $jum + $row->c30;?>
                                                <?php $j30 = $j30 + $row->c30;?>
                                                </td>
                                                <?php } ?>

                                                <?php if (isset($row->c31)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->c31 ;?>
                                                <?php $jum = $jum + $row->c31;?>
                                                <?php $j31 = $j31 + $row->c31;?>
                                                </td>
                                                <?php } ?>

                                                <?php if (isset($row->c32)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->c32 ;?>
                                                <?php $jum = $jum + $row->c32;?>
                                                <?php $j32 = $j32 + $row->c32;?>
                                                </td>
                                                <?php } ?>
                                                
                                                <?php if (isset($row->c33)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->c33 ;?>
                                                <?php $jum = $jum + $row->c33;?>
                                                <?php $j33 = $j33 + $row->c33;?>
                                                </td>
                                                <?php } ?>
                                                
                                                <?php if (isset($row->c34)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->c34 ;?>
                                                <?php $jum = $jum + $row->c34;?>
                                                <?php $j34 = $j34 + $row->c34;?>
                                                </td>
                                                <?php } ?>
                                                
                                                <?php if (isset($row->c35)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->c35 ;?>
                                                <?php $jum = $jum + $row->c35;?>
                                                <?php $j35 = $j35 + $row->c35;?>
                                                </td>
                                                <?php } ?>
                                                
                                                <?php if (isset($row->c36)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->c36 ;?>
                                                <?php $jum = $jum + $row->c36;?>
                                                <?php $j36 = $j36 + $row->c36;?>
                                                </td>
                                                <?php } ?>
                                                
                                                <?php if (isset($row->c37)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->c37 ;?>
                                                <?php $jum = $jum + $row->c37;?>
                                                <?php $j37 = $j37 + $row->c37;?>
                                                </td>
                                                <?php } ?>
                                                
                                                <?php if (isset($row->c38)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->c38 ;?>
                                                <?php $jum = $jum + $row->c38;?>
                                                <?php $j38 = $j38 + $row->c38;?>
                                                </td>
                                                <?php } ?>
                                                
                                                <?php if (isset($row->c39)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->c39 ;?>
                                                <?php $jum = $jum + $row->c39;?>
                                                <?php $j39 = $j39 + $row->c39;?>
                                                </td>
                                                <?php } ?>
                                                
                                                <?php if (isset($row->c40)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->c40 ;?>
                                                <?php $jum = $jum + $row->c40;?>
                                                <?php $j40 = $j40 + $row->c40;?>
                                                </td>
                                                <?php } ?>

                                                <?php if (isset($row->c41)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->c41 ;?>
                                                <?php $jum = $jum + $row->c41;?>
                                                <?php $j41 = $j41 + $row->c41;?>
                                                </td>
                                                <?php } ?>

                                                <?php if (isset($row->c42)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->c42 ;?>
                                                <?php $jum = $jum + $row->c42;?>
                                                <?php $j42 = $j42 + $row->c42;?>
                                                </td>
                                                <?php } ?>
                                                
                                                <?php if (isset($row->c43)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->c43 ;?>
                                                <?php $jum = $jum + $row->c43;?>
                                                <?php $j43 = $j43 + $row->c43;?>
                                                </td>
                                                <?php } ?>
                                                
                                                <?php if (isset($row->c44)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->c44 ;?>
                                                <?php $jum = $jum + $row->c44;?>
                                                <?php $j44 = $j44 + $row->c44;?>
                                                </td>
                                                <?php } ?>
                                                
                                                <?php if (isset($row->c45)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->c45 ;?>
                                                <?php $jum = $jum + $row->c45;?>
                                                <?php $j45 = $j45 + $row->c45;?>
                                                </td>
                                                <?php } ?>
                                                
                                                <?php if (isset($row->c46)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->c46 ;?>
                                                <?php $jum = $jum + $row->c46;?>
                                                <?php $j46 = $j46 + $row->c46;?>
                                                </td>
                                                <?php } ?>
                                                
                                                <?php if (isset($row->c47)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->c47 ;?>
                                                <?php $jum = $jum + $row->c47;?>
                                                <?php $j47 = $j47 + $row->c47;?>
                                                </td>
                                                <?php } ?>
                                                
                                                <?php if (isset($row->c48)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->c48 ;?>
                                                <?php $jum = $jum + $row->c48;?>
                                                <?php $j48 = $j48 + $row->c48;?>
                                                </td>
                                                <?php } ?>
                                                
                                                <?php if (isset($row->c49)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->c49 ;?>
                                                <?php $jum = $jum + $row->c49;?>
                                                <?php $j49 = $j49 + $row->c49;?>
                                                </td>
                                                <?php } ?>
                                                
                                                <?php if (isset($row->c50)){  ?>
                                                <td style="text-align: center;">
                                                <?php echo $row->c50 ;?>
                                                <?php $jum = $jum + $row->c50;?>
                                                <?php $j50 = $j50 + $row->c50;?>
                                                </td>
                                                <?php } ?>

                                                <td style="text-align: center;">
                                                <?php echo $jum ;?>
                                                <?php $jum_all = $jum_all + $jum;?>
                                                </td>
                                            </tr>
                                          <?php } ?>
                                          <?php } ?>
                                       
                                        
                                    </tbody>
                                    <tfoot id="my_foot">
                                            <tr>
                                            <?php if (!empty($data)){ ?>
                                            <th width="80%" colspan="2" style="text-align: center;">Jumlah</th>

                                            <?php if (isset($data[0]->c1)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j1),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->c2)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j2),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->c3)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j3),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->c4)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j4),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->c5)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j5),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->c6)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j6),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->c7)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j7),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->c8)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j8),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->c9)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j9),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->c10)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j10),0,',','.').'</th>'; }?>
                                            <?php } ?>
                                            <?php if (isset($data[0]->c11)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j11),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->c12)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j12),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->c13)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j13),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->c14)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j14),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->c15)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j15),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->c16)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j16),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->c17)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j17),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->c18)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j18),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->c19)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j19),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->c20)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j20),0,',','.').'</th>'; }?>
                                            <?php } ?>
                                            <?php if (isset($data[0]->c21)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j21),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->c22)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j22),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->c23)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j23),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->c24)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j24),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->c25)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j25),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->c26)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j26),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->c27)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j27),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->c28)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j28),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->c29)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j29),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->c30)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j30),0,',','.').'</th>'; }?>
                                            <?php } ?>
                                            <?php if (isset($data[0]->c31)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j31),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->c32)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j32),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->c33)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j33),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->c34)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j34),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->c35)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j35),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->c36)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j36),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->c37)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j37),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->c38)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j38),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->c39)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j39),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->c40)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j40),0,',','.').'</th>'; }?>
                                            <?php } ?>
                                            <?php if (isset($data[0]->c41)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j41),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->c42)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j42),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->c43)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j43),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->c44)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j44),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->c45)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j45),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->c46)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j46),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->c47)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j47),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->c48)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j48),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->c49)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j49),0,',','.').'</th>'; }?>
                                            <?php } ?>

                                            <?php if (isset($data[0]->c50)){ ?>
                                                <?php if (!empty($data[0])){echo '<th style="text-align: center;">'.number_format(htmlentities($j50),0,',','.').'</th>'; }?>
                                            <?php } ?>
                                                <th width="80%" colspan="1" style="text-align: center;"><?php echo $jum_all ;?></th>

                                            <?php } ?>
                                        </tr>

                                            </tfoot>
                                </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                 </div>
       @include('shared.footer_detail')