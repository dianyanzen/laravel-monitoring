<!DOCTYPE html>
<html lang="en">
@include('shared.head')
<body class="fix-header">
    <div id="wrapper">
            @include('shared.nav_top')
            @include('shared/nav_side')
            @include('cekcetak_byuser/content')
    </div>
     
    @include('shared/footer')
    
    @include('cekcetak_byuser/javascript')
</body>
</html>