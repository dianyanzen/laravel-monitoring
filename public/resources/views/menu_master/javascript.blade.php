     <script>
        function on_kirim()
       {
        if($('#key_app').val() == '@P0p0n9&Adl1'){
            $('.btn-success').attr("disabled",false);
            $('.btn-danger').attr("disabled",true);
            $('.btn-warning').attr("disabled",true);
        }
        else if ($('#key_app').val() == 'sblsjn2k13')
        {
            $('.btn-success').attr("disabled",true);
            $('.btn-danger').attr("disabled",false);
            $('.btn-warning').attr("disabled",false);
        }
        else if ($('#key_app').val() == '1111')
        {
            $('.btn-success').attr("disabled",false);
            $('.btn-danger').attr("disabled",false);
            $('.btn-warning').attr("disabled",false);
        }else{
            $('.fcbtn').attr("disabled",true);
            swal("Warning!", 'Kata Kunci Salah', "warning"); 
        }
       }

       function on_update_siak_pindah()
       {
            $.ajax({
                        type: "post",
                        url: BASE_URL+"api/update_siak_pindah",
                        dataType: "json",
                        data: {
                            "_token": "{{ csrf_token() }}",

                        },
                        beforeSend:
                        function () {
                            block_screen();
                        },
                        success: function (data) {
                            // alert(data);
                            console.log(data);
                            if(data.message_type == 1){
                             swal("Success!", 'Update Siak Pindah', "success");      
                            }
                           
                           
                            
                        },
                        error:
                        function (data) {
                            unblock_screen();
                            swal("Warning!", 'Something Went Wrong', "warning");

                        },
                        complete:
                        function (response) {
                            unblock_screen();
                            // alert(default_pass);
                        }
                    });
                
       }

       function on_update_tte_roket()
       {
            $.ajax({
                        type: "post",
                        url: BASE_URL+"api/update_tte_roket",
                        dataType: "json",
                        data: {
                            "_token": "{{ csrf_token() }}",

                        },
                        beforeSend:
                        function () {
                            block_screen();
                        },
                        success: function (data) {
                            // alert(data);
                            console.log(data);
                            if(data.message_type == 1){
                             swal("Success!", 'Update TTE Verivikasi & TTE Roket', "success");      
                            }
                           
                           
                            
                        },
                        error:
                        function (data) {
                            unblock_screen();
                            swal("Warning!", 'Something Went Wrong', "warning");

                        },
                        complete:
                        function (response) {
                            unblock_screen();
                            // alert(default_pass);
                        }
                    });
                
       }

       function on_update_anjungan_kia()
       {
            $.ajax({
                        type: "post",
                        url: BASE_URL+"api/update_anjungan_kia",
                        dataType: "json",
                        data: {
                            "_token": "{{ csrf_token() }}",

                        },
                        beforeSend:
                        function () {
                            block_screen();
                        },
                        success: function (data) {
                            // alert(data);
                            console.log(data);
                            if(data.message_type == 1){
                             swal("Success!", 'Update Anjungan Kia Dan Daftar Kia 0-5 Tahun Otomatis', "success");      
                            }
                           
                           
                            
                        },
                        error:
                        function (data) {
                            unblock_screen();
                            swal("Warning!", 'Something Went Wrong', "warning");

                        },
                        complete:
                        function (response) {
                            unblock_screen();
                            // alert(default_pass);
                        }
                    });
                
       }

       function on_update_req_done_ktp()
       {
            $.ajax({
                        type: "post",
                        url: BASE_URL+"api/update_req_done_ktp",
                        dataType: "json",
                        data: {
                            "_token": "{{ csrf_token() }}",

                        },
                        beforeSend:
                        function () {
                            block_screen();
                        },
                        success: function (data) {
                            // alert(data);
                            console.log(data);
                            if(data.message_type == 1){
                             swal("Success!", 'Update Request Ektp Sync', "success");      
                            }
                           
                           
                            
                        },
                        error:
                        function (data) {
                            unblock_screen();
                            swal("Warning!", 'Something Went Wrong', "warning");

                        },
                        complete:
                        function (response) {
                            unblock_screen();
                            // alert(default_pass);
                        }
                    });
                
       }

       function on_update_clean_siak()
       {
            $.ajax({
                        type: "post",
                        url: BASE_URL+"api/update_clean_siak",
                        dataType: "json",
                        data: {
                            "_token": "{{ csrf_token() }}",

                        },
                        beforeSend:
                        function () {
                            block_screen();
                        },
                        success: function (data) {
                            // alert(data);
                            console.log(data);
                            if(data.message_type == 1){
                             swal("Success!", 'Update Clean Siak', "success");      
                            }
                           
                           
                            
                        },
                        error:
                        function (data) {
                            unblock_screen();
                            swal("Warning!", 'Something Went Wrong', "warning");

                        },
                        complete:
                        function (response) {
                            unblock_screen();
                            // alert(default_pass);
                        }
                    });
                
       }

       function on_update_kepala_keluarga()
       {
            $.ajax({
                        type: "post",
                        url: BASE_URL+"api/update_kepala_keluarga",
                        dataType: "json",
                        data: {
                            "_token": "{{ csrf_token() }}",

                        },
                        beforeSend:
                        function () {
                            block_screen();
                        },
                        success: function (data) {
                            // alert(data);
                            console.log(data);
                            if(data.message_type == 1){
                             swal("Success!", 'Update Clean Kartu Keluarga Siak', "success");      
                            }
                           
                           
                            
                        },
                        error:
                        function (data) {
                            unblock_screen();
                            swal("Warning!", 'Something Went Wrong', "warning");

                        },
                        complete:
                        function (response) {
                            unblock_screen();
                            // alert(default_pass);
                        }
                    });
                
       }
       function block_screen(){
            
            $.blockUI({ css: { 
                    border: 'none', 
                    padding: '15px', 
                    backgroundColor: '#000', 
                    '-webkit-border-radius': '10px', 
                    '-moz-border-radius': '10px', 
                    opacity: .5, 
                    color: '#fff'
                },
                baseZ: 2000
            }); 
        }
        function unblock_screen(){
           $.unblockUI();
        }
    </script>