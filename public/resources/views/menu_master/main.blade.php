<!DOCTYPE html>
<html lang="en">
@include('shared.head')
<body class="fix-header">
    <div id="wrapper">
            @include('shared.nav_top')
            @include('shared/nav_side')
            @include('menu_master/content')
    </div>
     
    @include('shared/footer')
    
    @include('menu_master/javascript')
</body>
</html>