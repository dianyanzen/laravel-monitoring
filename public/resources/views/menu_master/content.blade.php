 <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $stitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                        <ol class="breadcrumb">
                           <li class="text-info"><?php echo $stitle; ?></li>
                        </ol>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                        <div class="white-box">
                            <h3 class="box-title">Eksekusi Procedure Secara Hardcore</h3>
                            <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_key_app" type="checkbox" checked disabled="true">
                                            <label for="cb_key_app"> Key Master</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-9 col-md-9 col-sm-6 col-xs-6">         
                                    <div class="form-group">
                                        <input class="form-control" type="password" id="key_app" name="key_app" /></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                                <button type="submit" name="do_get" class="btn btn-info btn-block waves-effect waves-light m-r-10 pull-right" id="btn-filter" onclick="on_kirim();" >Kirim <i class="mdi  mdi-send fa-fw"></i></button>
                            </div>
                            </div>
                        </div>


                        <div class="white-box">
                            <h3 class="box-title">Eksekusi Procedure Secara Hardcore</h3>
                            <div class="button-box">

                                <p class="text-muted m-b-30">Exsekusi User Siak Hardcore <code>Pergunakan Tombol Eksekusi Dengan Bijak Karena Procedure Di Sini Sangat Membebani Server</code></p>
                                <div class="row">
                                    <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6">
                                        <button class="fcbtn btn btn-success btn-block btn-outline btn-1d" onclick="on_update_tte_roket();" disabled>Update TTE Verivikasi Dan Roket</button>
                                    </div>
                                    <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6">
                                        <button class="fcbtn btn btn-danger btn-block btn-outline btn-1d" onclick="on_update_siak_pindah();" disabled>Update Siak Pindah YZ Sync</button>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6">
                                        <button class="fcbtn btn btn-success btn-block btn-outline btn-1d" onclick="on_update_anjungan_kia();" disabled>Update Kia YZ Sync</button>
                                    </div>
                                    <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6">
                                        <button class="fcbtn btn btn-warning btn-block btn-outline btn-1d" onclick="on_update_req_done_ktp();" disabled>Update Request Ektp Sync</button>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6">
                                        <button class="fcbtn btn btn-danger btn-block btn-outline btn-1d" onclick="on_update_clean_siak();" disabled>Update Clean Siak Sync</button>
                                    </div> 
                                    <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6">
                                        <button class="fcbtn btn btn-danger btn-block btn-outline btn-1d" onclick="on_update_kepala_keluarga();" disabled>Update Kepala Keluarga Siak Sync</button>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6">
                                        <button class="fcbtn btn btn-danger btn-block btn-outline btn-1d" disabled>Insert Delete From Master</button>
                                    </div> 
                                    <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6">
                                        <button class="fcbtn btn btn-warning btn-block btn-outline btn-1d" disabled>Update Sms & Email Salaman</button>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6">
                                        <button class="fcbtn btn btn-success btn-block btn-outline btn-1d" disabled>Update Salaman Mohon Lahir</button>
                                    </div> 
                                    <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6">
                                        <button class="fcbtn btn btn-warning btn-block btn-outline btn-1d" disabled>Re-History User Siak</button>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6">
                                        <button class="fcbtn btn btn-danger btn-block btn-outline btn-1d" disabled>Clean Dashboard Log</button>
                                    </div> 
                                    <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6">
                                        <button class="fcbtn btn btn-danger btn-block btn-outline btn-1d" disabled>Refresh Dashboard</button>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6">
                                        <button class="fcbtn btn btn-danger btn-block btn-outline btn-1d" disabled>Update Absensi Monitoring</button>
                                    </div> 
                                    <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6">
                                        <button class="fcbtn btn btn-danger btn-block btn-outline btn-1d" disabled>Update Laporan Daily Akta 0-18</button>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6">
                                        <button class="fcbtn btn btn-danger btn-block btn-outline btn-1d" disabled>Update Target E-Ktp</button>
                                    </div> 
                                    <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6">
                                        <button class="fcbtn btn btn-danger btn-block btn-outline btn-1d" disabled>Update Target Akta Kelahiran</button>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6">
                                        <button class="fcbtn btn btn-danger btn-block btn-outline btn-1d" disabled>Update Laporan Bulanan</button>
                                    </div> 
                                    <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6">
                                        <button class="fcbtn btn btn-danger btn-block btn-outline btn-1d" disabled>Update Kampung Gisa DKB</button>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6">
                                        <button class="fcbtn btn btn-danger btn-block btn-outline btn-1d" disabled>Update Card Management</button>
                                    </div> 
                                    <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6">
                                        <button class="fcbtn btn btn-danger btn-block btn-outline btn-1d" disabled>Update Card Print</button>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6">
                                        <button class="fcbtn btn btn-danger btn-block btn-outline btn-1d" disabled>Update Demographics</button>
                                    </div> 
                                    <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6">
                                        <button class="fcbtn btn btn-danger btn-block btn-outline btn-1d" disabled>Update Demographics All</button>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6">
                                        <button class="fcbtn btn btn-danger btn-block btn-outline btn-1d" disabled>Insert Prr Rekam</button>
                                    </div> 
                                    <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6">
                                        <button class="fcbtn btn btn-danger btn-block btn-outline btn-1d" disabled>Update Biometric Bermasalah</button>
                                    </div>
                                </div>
                                <!-- <p class="text-muted m-b-30">Copyright © DianYanzen, DisdukCapil Kota Bandung</code></p> -->
                            </div>
                        </div>
                    </div>
                </div>
             

            </div>
       @include('shared.footer_detail')