    <script src="{{ url('/') }}/assets/plugins/bower_components/owl.carousel/owl.carousel.min.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/owl.carousel/owl.custom.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/flot/jquery.flot.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/Minimal-Gauge-chart/js/cmGauge.js"></script>
   <script type="text/javascript">
   	$(document).ready(function() {
           $.ajax({
                    type: "post",
                    url: BASE_URL+"Setting/get_helpdesk_option",
                    dataType: "json",
                    data: {
                      "_token": "{{ csrf_token() }}",
                      user_id :  "<?php echo $user_id; ?>"
                    },
                    beforeSend:
                    function () {
                        
                    },
                    success: function (data) {
                        console.log(data);
                        var i = 1;
                       $('select[name="request_id"]').empty();
                        $.each(data, function(key, value) {
                            $('select[name="request_id"]').append('<option value="'+ value.helpdesk_id +'">'+i+'. '+ value.helpdesk_description +'</option>');
                             i = i+1;
                             if (key == 0){
                               $('select[name="request_id"]').val(value.helpdesk_id).trigger("change");
                             }
                        });
                         $('select[name="request_id"]').append('<option value="0">'+i+'. LAINNYA</option>');
                    },
                    error:
                    function (data) {
               

                    },
                    complete:
                    function (response) {
                    }

                });
           $("#request_id").change(function(){
               $("#keterangan").val("");
          });
        request_pending();

    });
   
     function request_pending(){
         $.ajax({
                    type: "post",
                    url: BASE_URL+"Siak/request_pending",
                    dataType: "json",
                    data: {
                      "_token": "{{ csrf_token() }}",
                        user_id : "<?php echo $user_id; ?>"
                    },
                    beforeSend:
                    function () {
                        
                    },
                    success: function (data) {
                        console.log(data);
                         if (!$.trim(data)){  
                        	$('#request_pending').empty();
                        	$('#request_pending').append('<div class="sl-item"><div class="sl-right">There Is No Data To Be Displayed On Your Page</div></div>');

                        }else{
                            $('#request_pending').empty();
                            $.each(data, function(key, value) {
                                $('#request_pending').append('<div class="sl-item"><div class="sl-left"><img src="{{ url('/') }}/assets/upload/pp/'+value.nik+'.jpg"  class="img-circle img-responsive" alt="user-img" > </div><div class="sl-right"><div class="m-l-40"> <a href="#" class="text-info">'+value.nama_lgkp+'</a> <span class="sl-date">Tanggal '+value.activity_date+'</span><div class="m-t-20 row"><div class="col-md-12 col-xs-12"><a href="#" class="btn btn-info" style="margin-bottom: 10px !important">'+value.p1+'</a><p>'+value.p2+'</p><p> '+value.p3+'</p></div></div></div></div></div>');
                            });
                        }
                        
                    },
                    error:
                    function (data) {
               

                    },
                    complete:
                    function (response) {
                        request_success();
                    }
                });
    }
     function request_success(){
         $.ajax({
                    type: "post",
                    url: BASE_URL+"Siak/request_success",
                    dataType: "json",
                    data: {
                      "_token": "{{ csrf_token() }}",
                        user_id : "<?php echo $user_id; ?>"
                    },
                    beforeSend:
                    function () {
                        
                    },
                    success: function (data) {
                        console.log(data);
                        if (!$.trim(data)){  
                        	$('#request_success').empty();
                        	$('#request_success').append('<div class="sl-item"><div class="sl-right">There Is No Data To Be Displayed On Your Page</div></div>');

                        }else{
                            $('#request_success').empty();
                            $.each(data, function(key, value) {
                                $('#request_success').append('<div class="sl-item"><div class="sl-left"><img src="{{ url('/') }}/assets/upload/pp/'+value.nik+'.jpg"  class="img-circle img-responsive" alt="user-img" > </div><div class="sl-right"><div class="m-l-40"> <a href="#" class="text-info">'+value.nama_lgkp+'</a> <span class="sl-date">Tanggal '+value.activity_date+'</span><div class="m-t-20 row"><div class="col-md-12 col-xs-12"><a href="#" class="btn btn-info" style="margin-bottom: 10px !important">'+value.p1+'</a><p>'+value.p2+'</p><hr><a href="#" class="text-info">Reply From : <b>'+value.p4+'</b></a><p>'+value.p3+'</p></div></div></div></div></div>');
                            });
                        }
                        
                    },
                    error:
                    function (data) {
               

                    },
                    complete:
                    function (response) {
                        request_reject();
                    }
                });
    }
     function request_reject(){
        
         $.ajax({
                    type: "post",
                    url: BASE_URL+"Siak/request_reject",
                    dataType: "json",
                    data: {
                      "_token": "{{ csrf_token() }}",
                        user_id : "<?php echo $user_id; ?>"
                    },
                    beforeSend:
                    function () {
                        
                    },
                    success: function (data) {
                        console.log(data);
                        if (!$.trim(data)){  
                        	$('#request_reject').empty();
                        	$('#request_reject').append('<div class="sl-item"><div class="sl-right">There Is No Data To Be Displayed On Your Page</div></div>');

                        }else{
                            $('#request_reject').empty();
                            $.each(data, function(key, value) {
                                $('#request_reject').append('<div class="sl-item"><div class="sl-left"><img src="{{ url('/') }}/assets/upload/pp/'+value.nik+'.jpg"  class="img-circle img-responsive" alt="user-img" > </div><div class="sl-right"><div class="m-l-40"> <a href="#" class="text-info">'+value.nama_lgkp+'</a> <span class="sl-date">Tanggal '+value.activity_date+'</span><div class="m-t-20 row"><div class="col-md-12 col-xs-12"><a href="#" class="btn btn-info" style="margin-bottom: 10px !important">'+value.p1+'</a><p>'+value.p2+'</p><hr><a href="#" class="text-info">Reply From : <b>'+value.p4+'</b></a><p>'+value.p3+'</p></div></div></div></div></div>');
                            });
                        }
                        
                    },
                    error:
                    function (data) {
               

                    },
                    complete:
                    function (response) {

                    }
                });
    }
     function refresh_pending(){

         $.ajax({
                    type: "post",
                    url: BASE_URL+"Siak/request_pending",
                    dataType: "json",
                    data: {
                      "_token": "{{ csrf_token() }}",
                        user_id : "<?php echo $user_id; ?>"
                    },
                    beforeSend:
                    function () {
                        
                    },
                    success: function (data) {
                        console.log(data);
                         if (!$.trim(data)){  
                            $('#request_pending').empty();
                            $('#request_pending').append('<div class="sl-item"><div class="sl-right">There Is No Data To Be Displayed On Your Page</div></div>');

                        }else{
                            $('#request_pending').empty();
                            $.each(data, function(key, value) {
                                $('#request_pending').append('<div class="sl-item"><div class="sl-left"><img src="{{ url('/') }}/assets/upload/pp/'+value.nik+'.jpg"  class="img-circle img-responsive" alt="user-img" > </div><div class="sl-right"><div class="m-l-40"> <a href="#" class="text-info">'+value.nama_lgkp+'</a> <span class="sl-date">Tanggal '+value.activity_date+'</span><div class="m-t-20 row"><div class="col-md-12 col-xs-12"><a href="#" class="btn btn-info" style="margin-bottom: 10px !important">'+value.p1+'</a><p>'+value.p2+'</p><p> '+value.p3+'</p></div></div></div></div></div>');
                            });
                        }
                        
                    },
                    error:
                    function (data) {
               

                    },
                    complete:
                    function (response) {
                        refresh_success();
                    }
                });
    }
     function refresh_success(){
         $.ajax({
                    type: "post",
                    url: BASE_URL+"Siak/request_success",
                    dataType: "json",
                    data: {
                      "_token": "{{ csrf_token() }}",
                        user_id : "<?php echo $user_id; ?>"
                    },
                    beforeSend:
                    function () {
                        
                    },
                    success: function (data) {
                        console.log(data);
                        if (!$.trim(data)){  
                            $('#request_success').empty();
                            $('#request_success').append('<div class="sl-item"><div class="sl-right">There Is No Data To Be Displayed On Your Page</div></div>');

                        }else{
                            $('#request_success').empty();
                            $.each(data, function(key, value) {
                                $('#request_success').append('<div class="sl-item"><div class="sl-left"><img src="{{ url('/') }}/assets/upload/pp/'+value.nik+'.jpg"  class="img-circle img-responsive" alt="user-img" > </div><div class="sl-right"><div class="m-l-40"> <a href="#" class="text-info">'+value.nama_lgkp+'</a> <span class="sl-date">Tanggal '+value.activity_date+'</span><div class="m-t-20 row"><div class="col-md-12 col-xs-12"><a href="#" class="btn btn-info" style="margin-bottom: 10px !important">'+value.p1+'</a><p>'+value.p2+'</p><hr><a href="#" class="text-info">Reply From : <b>'+value.p4+'</b></a><p>'+value.p3+'</p></div></div></div></div></div>');
                            });
                        }
                        
                    },
                    error:
                    function (data) {
               

                    },
                    complete:
                    function (response) {
                        refresh_reject();
                    }
                });
    }
     function refresh_reject(){
         $.ajax({
                    type: "post",
                    url: BASE_URL+"Siak/request_reject",
                    dataType: "json",
                    data: {
                      "_token": "{{ csrf_token() }}",
                        user_id : "<?php echo $user_id; ?>"
                    },
                    beforeSend:
                    function () {
                        
                    },
                    success: function (data) {
                        console.log(data);
                        if (!$.trim(data)){  
                            $('#request_reject').empty();
                            $('#request_reject').append('<div class="sl-item"><div class="sl-right">There Is No Data To Be Displayed On Your Page</div></div>');

                        }else{
                            $('#request_reject').empty();
                            $.each(data, function(key, value) {
                                $('#request_reject').append('<div class="sl-item"><div class="sl-left"><img src="{{ url('/') }}/assets/upload/pp/'+value.nik+'.jpg"  class="img-circle img-responsive" alt="user-img" > </div><div class="sl-right"><div class="m-l-40"> <a href="#" class="text-info">'+value.nama_lgkp+'</a> <span class="sl-date">Tanggal '+value.activity_date+'</span><div class="m-t-20 row"><div class="col-md-12 col-xs-12"><a href="#" class="btn btn-info" style="margin-bottom: 10px !important">'+value.p1+'</a><p>'+value.p2+'</p><hr><a href="#" class="text-info">Reply From : <b>'+value.p4+'</b></a><p>'+value.p3+'</p></div></div></div></div></div>');
                            });
                        }
                        
                    },
                    error:
                    function (data) {
               

                    },
                    complete:
                    function (response) {
                        unblock_screen();
                          swal("Success!", "Data Already Up To Date", "success");
                    }
                });
         
    }
    function do_refresh(){
        block_screen();
        refresh_pending();
    }
    function do_request(){
        $("#request_id").val("1");
        $("#keterangan").val("");
        $("#bantuan_text").append().empty();
        $('#do_request').click();
    }

    function onClearModal() {
        $("#request_id").val("1");
        $("#keterangan").val("");
    }
    function do_close(){
      $('#closemodal').click();
    }
    function validation() {
            var request_id = $("#request_id");
            if (request_id.val().length == 0) {
              request_id.select();
                toastr.warning('Request Id tidak boleh kosong');
                return false;
            }
            var keterangan = $("#keterangan");
            if (keterangan.val().length == 0) {
                toastr.warning('Keterangan tidak boleh kosong');
                keterangan.select();
                return false;
            } 
            if (keterangan.val().length > 0 && keterangan.val().length < 10) {
                toastr.warning('Keterangan terlalu pendek');
                keterangan.select();
                return false;
            } 
           

            return true;
    }
     function do_save(){
         if (!validation()) return;
           $.ajax({
                type: "post",
                url: BASE_URL+"Siak/send_request",
                data: {
                  "_token": "{{ csrf_token() }}",
                  request_id :  $("#request_id").val(),
                  keterangan :  $("#keterangan").val(),
                  user_id :  "<?php echo $user_id; ?>"
                },
                beforeSend:
                function () {
                  document.getElementById("go_send").disabled = true;
                  $('#go_send').html("<i class='fa fa-circle-o-notch fa-spin fa-fw'></i> Wait");
                },
                success: function (data) {

                    if (data.message_type == 1){
                    toastr.success(data.message);
                    $("#keterangan").val('');
                    }else if (data.message_type == 2){
                      toastr.error(data.message);
                    }
                    
                },
                error:
                function (data) {
                    toastr.error("Please Try Again, Something Went Wrong");
                $("#keterangan").val('');
                do_close();

                },
                complete:
                function (response) {
                  document.getElementById("go_send").disabled = false;
                  $('#go_send').html("Kirim");
                  do_close();
                  do_refresh();
                }
            });

        }
        function block_screen(){
        
        $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff'
            },
            baseZ: 2000
        }); 

    }
       
    function unblock_screen(){
       $.unblockUI();
    }
   </script>