<!DOCTYPE html>
<html lang="en">
@include('shared.head')
<body class="fix-header">
    <div id="wrapper">
            @include('shared.nav_top')
            @include('shared/nav_side')
            @include('perbaikan_data/content')
            @include('perbaikan_data/modal')
    </div>
     
    @include('shared/footer')
    
    @include('perbaikan_data/javascript')
</body>
</html>