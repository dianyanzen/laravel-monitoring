<button class="btn btn-info btn-lg" id="do_request" data-toggle="modal" data-target="#request-modal" style="display: none;"></button>

           <div class="modal fade" id="request-modal" tabindex="-1" role="dialog" aria-labelledby="request" aria-hidden="true" >
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" aria-hidden="true" onclick="do_close()">&times;</button>
                      <h4 class="modal-title">Request Helpdesk</h4>
                    </div>
                    <div class="modal-body">
                     <div class="row">
                            <div class="col-md-12">
                              <form class="form-horizontal" role="form">
                                <fieldset>


                                  <div class="form-group">
                                    <label class="col-sm-2 control-label " for="textinput" style="text-align: right;">Pengajuan</label>
                                    <div class="col-sm-10">
                                      <select class="form-control" name="request_id" id="request_id" value="">
                                       <option value='0'>0. LAINNYA</option>
                                      </select>
                                    </div>
                                  </div>

                                  <div class="form-group">
                                    <label class="col-sm-2 control-label " for="textinput" style="text-align: right;">Keterangan</label>
                                    <div class="col-sm-10">
                                     <textarea type="text" name="keterangan" id="keterangan" placeholder="Isilah Dengan Singkat, Jelas Dan Mudah Dimengerti (Max 300 Karakter)" maxlength="300" class="form-control" style="resize: none;text-transform:uppercase !important"></textarea>
                                    </div>
                                  </div>



                                  <!-- Text input-->
                                 
                                  
                                     <div class="col-sm-12" id="bantuan_text">
            
                                  </div>
                                  
                                </fieldset>
                              </form>
                            </div><!-- /.col-lg-12 -->
                        </div><!-- /.row -->
                        </div>

                    <div class="modal-footer" style="margin-top: 0px !important;">
                      <button type="button" id="closemodal" class="btn btn-default" data-dismiss="modal" onclick="onClearModal()">Tutup</button>
                      <button type="button" id="go_send" class="btn btn-info" onclick="do_save()">Kirim</button>
                    </div>
                  </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
              </div><!-- /.modal -->