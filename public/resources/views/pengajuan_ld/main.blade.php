<!DOCTYPE html>
<html lang="en">
@include('shared.head')
<body class="fix-header">
    <div id="wrapper">
            @include('shared.nav_top')
            @include('shared/nav_side')
            @include('pengajuan_ld/content')
    </div>
     
    @include('shared/footer')
    
    @include('pengajuan_ld/javascript')
</body>
</html>