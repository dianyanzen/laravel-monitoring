     <script>
        function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }
    $(document).ready(function() {
        console.log(init_kec);
        console.log(init_level);
        $('input[type=text]').on('input', function(evt) {
            $(this).val(function(_, val) {
            return val.toUpperCase();
            });
        });
        $('#txt_nik').on('keyup',function(){
              var my_txt = $(this).val();
              var len = my_txt.length;
              if(len == 16)
              {
                  get_nik_paste();
                  
              }
        });
    });
    function get_nik_paste(){
         var nik = $("#txt_nik");
         if (nik.val().length != 16) {                
                  swal("Warning!", "Input Nik Harus 16 Digit !", "warning");  
          }else{
                get_nik();
          }
    }
    function get_nik(){
        $.ajax({
                    type: "post",
                    url: BASE_URL+"Pengajuan_ld/get_nik_ld",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        nik : $("#txt_nik").val()
                    },
                    beforeSend:
                    function () {
                       block_screen();
                    },
                    success: function (data) {
                        console.log(data);
                        $('#txt_nama').val(data.nama);
                        
                        
                        if (data.is_exists == 1){
                            $('select[name="no_kec"]').attr("readonly",true);
                            $('select[name="no_kec"]').val(data.no_kec).trigger("change");    
                            $("#txt_nama").attr("readonly", true); 
                            if (data.is_ulang == 1){
                              $('#tanggal').val(data.req_date);
                              $("#btn_dosave").hide(); 
                              $("#btn_doupdate").show(); 
                              $("#btn_dodelete").show(); 
                              $("#box_tanggal").show(); 
                            }else{
                              $("#btn_dosave").hide(); 
                              $("#btn_doupdate").hide(); 
                              $("#btn_dodelete").show(); 
                              $("#box_tanggal").hide(); 
                              $('#tanggal').val("");  
                            }

                        }else{
                          $('select[name="no_kec"]').attr("readonly",false);
                            $('select[name="no_kec"]').val(init_kec).trigger("change"); 
                            $("#btn_dosave").show();
                            $("#btn_doupdate").hide(); 
                            $("#btn_dodelete").show(); 
                            $("#box_tanggal").hide(); 
                            $('#tanggal').val("");  
                            $("#txt_nama").attr("readonly", false); 
                            $('#txt_nama').focus();
                        }
                        
                    },
                    error:
                    function (data) {
                        unblock_screen();
                        swal("Error!", "Ooops!, Please Try Again, Something Went Wrong", "error"); 

                    },
                    complete:
                    function (response) {
                        unblock_screen();
                        
                    }
                });
    }
    function on_clear() {
        
        $("#btn_dosave").hide();
        $("#btn_doupdate").hide(); 
        $("#btn_dodelete").hide(); 
        $("#box_tanggal").hide();
        $('#tanggal').val("");  
        $('#txt_nik').val("");
        $('#txt_nama').val("");
        $("#txt_nama").attr("readonly", true);
        $('select[name="no_kec"]').val(init_kec).trigger("change");
        $('#txt_nik').focus();
    }
     function on_save(){
        if (validationdaily()){
          do_save();
        }
    }
     function on_update(){
        if (validationdaily()){
          do_update();
        }
    }
     function on_delete(){
        if (validationdaily()){
          do_delete();
        }
    }

    function validationdaily() {
        var nik = $("#txt_nik");
        var nama = $("#txt_nama");
        var no_kec = $('select[name="no_kec"]');
        console.log(nik);
            if (nik.val().length != 16) {                
                  swal("Warning!", "Input Nik Harus 16 Digit !", "warning");  
                 return false;
            }
            if (nama.val().length == 0) {                
                  swal("Warning!", "Nama Tidak Boleh Kosong !", "warning");  
                 return false;
            }
            if (no_kec.val() == 0) {                
                  swal("Warning!", "Nomer Kecamatan Tidak Boleh Kosong !", "warning");  
                 return false;
            }
           
          
            return true;
       
        }
        function do_save(){
            $.ajax({
                    type: "post",
                    url: BASE_URL+"Req/do_ajukan_ld",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        nik : $("#txt_nik").val(),
                        nama : $("#txt_nama").val(),
                        no_kec : $('select[name="no_kec"]').val(),
                        state : 1,
                    },
                    beforeSend:
                    function () {
                         block_screen();
                    },
                    success: function (data) {
                        console.log(data);
                        swal("Success!", data.message, "success");
                    },
                    error:
                    function (data) {
                        unblock_screen();
                        swal("Error!", "Ooops!, Please Try Again, Something Went Wrong", "error"); 

                    },
                    complete:
                    function (response) {
                        unblock_screen();
                        on_clear();
                    }
                });
        }
        function do_update(){
            $.ajax({
                    type: "post",
                    url: BASE_URL+"Req/do_ajukan_ld",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        nik : $("#txt_nik").val(),
                        nama : $("#txt_nama").val(),
                        no_kec : $('select[name="no_kec"]').val(),
                        state : 2,
                    },
                    beforeSend:
                    function () {
                         block_screen();
                    },
                    success: function (data) {
                        console.log(data);
                        swal("Success!", data.message, "success");
                    },
                    error:
                    function (data) {
                        unblock_screen();
                        swal("Error!", "Ooops!, Please Try Again, Something Went Wrong", "error"); 

                    },
                    complete:
                    function (response) {
                        unblock_screen();
                        on_clear();
                    }
                });
        }
        function do_delete(){
            $.ajax({
                    type: "post",
                    url: BASE_URL+"Req/do_ajukan_ld",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        nik : $("#txt_nik").val(),
                        nama : $("#txt_nama").val(),
                        no_kec : $('select[name="no_kec"]').val(),
                        state : 3,
                    },
                    beforeSend:
                    function () {
                         block_screen();
                    },
                    success: function (data) {
                        console.log(data);
                        swal("Success!", data.message, "success");
                    },
                    error:
                    function (data) {
                        unblock_screen();
                        swal("Error!", "Ooops!, Please Try Again, Something Went Wrong", "error"); 

                    },
                    complete:
                    function (response) {
                        unblock_screen();
                        on_clear();
                    }
                });
        }
    function block_screen(){
        
        $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff'
            },
            baseZ: 2000
        }); 
    }
    function unblock_screen(){
       $.unblockUI();
    }
    function get_wilayah(){
      $.ajax({
                    type: "post",
                    url: BASE_URL+"api/ektp-kec",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        no_prop : 32,
                        no_kab : 73,
                        no_kec : init_kec
                    },
                    beforeSend:
                    function () {
                        $('select[name="no_kec"]').attr("disabled",true);
                    },
                    success: function (data) {
                        console.log(data);
                       $('select[name="no_kec"]').empty();
                       <?php if ($user_no_kec == 0){ ?>
                       $('select[name="no_kec"]').append('<option value="0">-- Pilih Kecamatan --</option>');
                       <?php }else if($akses_kec > 0){ ?>
                        $('select[name="no_kec"]').append('<option value="0">-- Pilih Kecamatan --</option>');
                       <?php } ?>
                        $.each(data.kecamatan, function(key, value) {
                            $('select[name="no_kec"]').append('<option value="'+ value.no_kec +'">'+ value.nama_kec +'</option>');
                        });
                        <?php if ($user_no_kec == 0){ ?>
                       $('select[name="no_kec"]').val("0").trigger("change");
                       <?php }else{ ?>
                        $('select[name="no_kec"]').val("<?php echo $user_no_kec; ?>").trigger("change");
                        <?php } ?>
                        
                    },
                    error:
                    function (data) {
               

                    },
                    complete:
                    function (response) {
                        $('select[name="no_kec"]').attr("disabled",false);
                    }
                });
    }
   jQuery(document).ready(function() {
            $(".select2").select2();
             $('#mytable').DataTable({
                 "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
             get_wilayah();
        });
    </script>
   