 <style type="text/css">
     textarea {
  resize: none;
  height: 155px !important;
}
 </style>
 <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $stitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                        <ol class="breadcrumb">
                            <li class="active"><?php echo $mtitle; ?></li>
                        </ol>
                    </div>
                </div>
                 <div class="row">
                <!-- <form name ="get_form" action="<?php echo $my_url; ?>" method="post"> -->
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="white-box">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="row">
                                <div class="col-lg-3">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_nik" type="checkbox" checked disabled="true">
                                            <label for="cb_nik"> Nik</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3">         
                                    <div class="input-group">
                                        <input class="form-control" id="txt_nik" type="text" maxlength="16" onkeypress="return isNumberKey(event)" ><span class="input-group-btn"><button type="button" onclick="get_nik_paste();" class="btn waves-effect waves-light btn-info"><i class="fa fa-search"></i></button></span></div>
                                </div>
                            </div>
                            <div class="row">
                               
                                <div class="col-lg-3">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_nama" type="checkbox" checked disabled="true" >
                                            <label for="cb_nama"> Nama</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3">         
                                    <div class="form-group">
                                        <input class="form-control" id="txt_nama" type="text" readonly></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-3">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_kec" type="checkbox" checked disabled="true">
                                            <label for="cb_kec"> Kecamatan</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-3">         
                                    <div class="form-group" <?php if (!empty($is_colnotwil)){echo 'style="display: none!important;"'; }?>>
                                        <select class="form-control select2" name="no_kec" id="no_kec" disabled="true">
                                         <option  value="0">-- Pilih Kecamatan --</option>
                                        </select>
                                    </div>
                                </div>
                                </div>
                                <div class="row" id="box_tanggal"  style="display: none;">
                                <div class="col-lg-3">         
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info align-bottom">
                                            <input id="cb_tgl" type="checkbox" checked disabled="true">
                                            <label for="cb_tgl"> Tanggal Pengajuan</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-3" >         
                                    <div class="form-group">
                                        <input class="form-control" id="tanggal" type="text" readonly></div>
                                </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <button type="submit" id="btn_dosave" class="btn btn-info waves-effect waves-light m-r-10 pull-right" id="btn-filter" onclick="on_save();" style="display: none;">Save <i class="mdi  mdi-content-save fa-fw"></i></button>
                                <button  id="btn_doupdate" class="btn btn-success waves-effect waves-light m-r-10 pull-right" id="btn-filter" onclick="on_update();" style="display: none;">Update <i class="mdi  mdi-tooltip-edit fa-fw"></i></button>
                                <button  id="btn_dodelete" class="btn btn-danger waves-effect waves-light m-r-10 pull-right" id="btn-filter" onclick="on_delete();" style="display: none;">Delete <i class="mdi  mdi-delete-forever fa-fw"></i></button>
                                  <button type="button" class="btn btn-invert waves-effect waves-light m-r-10 pull-right" id="btn-reset" onclick="on_clear();">Clear <i class="mdi  mdi-delete fa-fw"></i></button>
                            </div>
                        
                        </div>
                    </div>
                    <!-- </form> -->
                
                </div>
              
                 </div>
       @include('shared.footer_detail')