    <script src="{{ url('/') }}/assets/plugins/bower_components/owl.carousel/owl.carousel.min.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/owl.carousel/owl.custom.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/flot/jquery.flot.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
    <script src="{{ url('/') }}/assets/plugins/bower_components/Minimal-Gauge-chart/js/cmGauge.js"></script>
   <script type="text/javascript">
       function on_back() {
            block_screen();
            var url = "{{ url('/') }}/";
            url += "Report/Absensi";
            var win = window.location.replace(url);
            win.focus();
        }  
         function do_ci(){
            $.ajax({
                    type: "post",
                    url: BASE_URL+"Report/do_ci",
                    dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        absen_user_id : $("#absen_user_id").val(),
                        absen_nama : $("#absen_nama").val(),
                        absen_masuk : $("#absen_masuk").val(),
                        absen_keluar : $("#absen_keluar").val(),
                        absen_tanggal : $("#absen_tanggal").val(),
                        absen_ket : $("#absen_ket").val(),
                        absen_ip : $("#absen_ip").val()
                    },
                    beforeSend:
                    function () {
                        block_screen();
                    },
                    success: function (data) {
                        console.log(data);
                        if(data.success == true){
                            swal({   
                                title: "Success",   
                                text:  data.message,   
                                type: "success",   
                                showCancelButton: false,   
                                closeOnConfirm: false,   
                                closeOnCancel: false 
                            }, function(isConfirm){   
                                if (isConfirm) {     
                                    on_back();
                                } 
                            });
                        }else{
                            swal({   
                                title: "Warning",   
                                text:  data.message,   
                                type: "warning",   
                                showCancelButton: false,   
                                closeOnConfirm: false,   
                                closeOnCancel: false 
                            }, function(isConfirm){   
                                if (isConfirm) {     
                                    on_back();
                                } 
                            });
                        }
                    },
                    error:
                    function (data) {
                        unblock_screen();
                        swal("Error!", "Ooops!, Please Try Again, Something Went Wrong", "error"); 

                    },
                    complete:
                    function (response) {
                        unblock_screen();
                        
                    }
                });
        }
        function block_screen(){
        
        $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff'
            },
            baseZ: 2000
        }); 
    }
    function unblock_screen(){
       $.unblockUI();
    }
   </script>