<?php

use Illuminate\Support\Str;

return [

    /*
    |--------------------------------------------------------------------------
    | Default Database Connection Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of the database connections below you wish
    | to use as your default connection for all database work. Of course
    | you may use many connections at once using the Database library.
    |
    */

    'default' => env('DB_CONNECTION', 'oracle'),

    /*
    |--------------------------------------------------------------------------
    | Database Connections
    |--------------------------------------------------------------------------
    |
    | Here are each of the database connections setup for your application.
    | Of course, examples of configuring each database platform that is
    | supported by Laravel is shown below to make development simple.
    |
    |
    | All database work in Laravel is done through the PHP PDO facilities
    | so make sure you have the driver for your particular database of
    | choice installed on your machine before you begin development.
    |
    */

    'connections' => [

        'oracle' => [
            'driver' => 'oracle',
            'host' => '10.32.73.24',
            'port' => '1521',
            'database' => 'siakdb',
            'service_name' => 'siakdb',
            'username' => 'yzdb',
            'password' => 'technicalbsl7',
            'charset' => '',
            'prefix' => '',
            ],
        'db2' => [
            'driver' => 'oracle',
            'host' => '10.32.73.221',
            'port' => '1522',
            'database' => 'orcl',
            'service_name' => 'orcl',
            'username' => 'BMUSER',
            'password' => 'technicalbsl7',
            'charset' => '',
            'prefix' => '',
            ],
        'db2old' => [
            'driver' => 'oracle',
            'host' => '10.32.73.2',
            'port' => '1522',
            'database' => 'orcl',
            'service_name' => 'orcl',
            'username' => 'BMUSER',
            'password' => 'technicalbsl7',
            'charset' => '',
            'prefix' => '',
            ],
        'db221old' => [
            'driver' => 'oracle',
            'host' => '10.32.73.5',
            'port' => '1522',
            'database' => 'local',
            'service_name' => 'local',
            'username' => 'BMUSER',
            'password' => 'BM123',
            'charset' => '',
            'prefix' => '',
            ],
        'db221' => [
            'driver' => 'oracle',
            'host' => '10.32.73.221',
            'port' => '1522',
            'database' => 'local',
            'service_name' => 'local',
            'username' => 'BMUSER',
            'password' => 'BM123',
            'charset' => '',
            'prefix' => '',
            ],
        'db5cetak' => [
            'driver' => 'oracle',
            'host' => '10.32.73.221',
            'port' => '1522',
            'database' => 'orcl',
            'service_name' => 'orcl',
            'username' => 'BMUSER',
            'password' => 'technicalbsl7',
            'charset' => '',
            'prefix' => '',
            ],
        'db5' => [
            'driver' => 'oracle',
            'host' => '10.32.73.221',
            'port' => '1522',
            'database' => 'local',
            'service_name' => 'local',
            'username' => 'BMUSER',
            'password' => 'BM123',
            'charset' => '',
            'prefix' => '',
            ],
        'db222' => [
            'driver' => 'oracle',
            'host' => '10.32.73.222',
            'port' => '1521',
            'database' => 'siakdb',
            'service_name' => 'siakdb',
            'username' => 'SIAKOFF',
            'password' => 'siakoff_2019',
            'charset' => '',
            'prefix' => '',
            ],
        'smsdb' => [
            'driver' => 'oracle',
            'host' => '10.32.73.10',
            'port' => '1521',
            'database' => 'siakdb',
            'service_name' => 'siakdb',
            'username' => 'SMSDISDUK',
            'password' => 'SMS_DB',
            'charset' => '',
            'prefix' => '',
            ],
        'webdb' => [
            'driver' => 'oracle',
            'host' => '10.32.73.10',
            'port' => '1521',
            'database' => 'siakdb',
            'service_name' => 'siakdb',
            'username' => 'WEBDISDUK',
            'password' => 'ONLINEDISDUK2020',
            'charset' => '',
            'prefix' => '',
            ],
        'epunten' => [
            'driver' => 'oracle',
            'host' => '10.32.73.10',
            'port' => '1521',
            'database' => 'siakdb',
            'service_name' => 'siakdb',
            'username' => 'EPUNTEN',
            'password' => 'ADMIN',
            'charset' => '',
            'prefix' => '',
            ],
        'webpunten' => [
            'driver' => 'oracle',
            'host' => '10.32.73.10',
            'port' => '1521',
            'database' => 'siakdb',
            'service_name' => 'siakdb',
            'username' => 'WEBPUNTEN',
            'password' => 'ADMIN',
            'charset' => '',
            'prefix' => '',
            ],

    ],

    /*
    |--------------------------------------------------------------------------
    | Migration Repository Table
    |--------------------------------------------------------------------------
    |
    | This table keeps track of all the migrations that have already run for
    | your application. Using this information, we can determine which of
    | the migrations on disk haven't actually been run in the database.
    |
    */

    'migrations' => 'migrations',

    /*
    |--------------------------------------------------------------------------
    | Redis Databases
    |--------------------------------------------------------------------------
    |
    | Redis is an open source, fast, and advanced key-value store that also
    | provides a richer body of commands than a typical key-value system
    | such as APC or Memcached. Laravel makes it easy to dig right in.
    |
    */

    'redis' => [

        'client' => env('REDIS_CLIENT', 'predis'),

        'options' => [
            'cluster' => env('REDIS_CLUSTER', 'redis'),
            'prefix' => env('REDIS_PREFIX', Str::slug(env('APP_NAME', 'laravel'), '_').'_database_'),
        ],

        'default' => [
            'url' => env('REDIS_URL'),
            'host' => env('REDIS_HOST', '127.0.0.1'),
            'password' => env('REDIS_PASSWORD', null),
            'port' => env('REDIS_PORT', 6379),
            'database' => env('REDIS_DB', 0),
        ],

        'cache' => [
            'url' => env('REDIS_URL'),
            'host' => env('REDIS_HOST', '127.0.0.1'),
            'password' => env('REDIS_PASSWORD', null),
            'port' => env('REDIS_PORT', 6379),
            'database' => env('REDIS_CACHE_DB', 1),
        ],

    ],

];
