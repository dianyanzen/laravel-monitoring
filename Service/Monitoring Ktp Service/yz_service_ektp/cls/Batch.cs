﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Diagnostics;
using Oracle.ManagedDataAccess.Client;

namespace yz_service_ektp.cls
{
    class Batch
    {
        #region Main Proccess
        OracleConnection con;
    
        public static string dbcon222 = yz_service_ektp.Properties.Settings.Default.db222;
        public static string dbcon221 = yz_service_ektp.Properties.Settings.Default.db221;
        private string app_path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location.ToString());
        public void process()
        {
            Batch bct = new Batch();
            Console.WriteLine("Checking Connections");
            this.ReadSystemFile("Checking First Connections");
            bct.Connectfirst();
            bct.Connectsecond();
            bct.Connectthird();
            bct.Connectfour();
            bct.Connectfive();
            bct.Connectsix();
            bct.Connectseven();
            bct.Connecteight();
            bct.Connectdb5ctk();
            bct.Connectdb5rkm();
            bct.Connectdb5rkmupdt();
            bct.Connectdb5ctkupdt();
            bct.Connectcardmngmnt();
            bct.Connectdbrekam();

            bct.Connectdbsync();

            Console.WriteLine("Progres Done For One Cycle");
            this.ReadSystemFile("Progres Done For One Cycle");
            bct.Closeall();
            Console.WriteLine("Close All Connections");
            this.ReadSystemFile("Close All Connections");
        }
        void Connectfirst()
        {
            try
            {
                con = new OracleConnection();
                con.ConnectionString = dbcon222;
                con.Open();
                Console.WriteLine("Connected to Oracle db 221 " + con.ServerVersion);
                this.ReadSystemFile("1. Connected to Oracle db 221 " + con.ServerVersion);
                create_temp_table_first();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                this.WriteToFile("1. " + ex.Message + "Connected to Oracle db 221 : {0}");
            }

        }
        void Connectsecond()
        {
            try
            {
                con = new OracleConnection();
                con.ConnectionString = dbcon222;
                con.Open();
                Console.WriteLine("Connected to Oracle db 2 " + con.ServerVersion);
                this.ReadSystemFile("2. Connected to Oracle db 2 " + con.ServerVersion);
                create_temp_table_second();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                this.WriteToFile("2. " + ex.Message + "Connected to Oracle db 2 : {0}");
            }

        }
        void Connectthird()
        {
            try
            {
                con = new OracleConnection();
                con.ConnectionString = dbcon222;
                con.Open();
                Console.WriteLine("Connected to Oracle db 2 " + con.ServerVersion);
                this.ReadSystemFile("3. Connected to Oracle db 2 " + con.ServerVersion);
                create_temp_table_third();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                this.WriteToFile("3. " + ex.Message + "Connected to Oracle db 2 : {0}");
            }

        }
        void Connectfour()
        {
            try
            {
                con = new OracleConnection();
                con.ConnectionString = dbcon222;
                con.Open();
                Console.WriteLine("Connected to Oracle db 2 " + con.ServerVersion);
                this.ReadSystemFile("4. Connected to Oracle db 2 " + con.ServerVersion);
                create_temp_table_four();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                this.WriteToFile("4. " + ex.Message + "Connected to Oracle db 2 : {0}");
            }

        }
        void Connectfive()
        {
            try
            {
                con = new OracleConnection();
                con.ConnectionString = dbcon222;
                con.Open();
                Console.WriteLine("Connected to Oracle db 2 " + con.ServerVersion);
                this.ReadSystemFile("5. Connected to Oracle db 2 " + con.ServerVersion);
                create_temp_table_five();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                this.WriteToFile("5. " + ex.Message + "Connected to Oracle db 2 : {0}");
            }

        }
        void Connectsix()
        {
            try
            {
                con = new OracleConnection();
                con.ConnectionString = dbcon222;
                con.Open();
                Console.WriteLine("Connected to Oracle db 221 " + con.ServerVersion);
                this.ReadSystemFile("6. Connected to Oracle db 221 " + con.ServerVersion);
                create_temp_table_six();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                this.WriteToFile("6. " + ex.Message + "Connected to Oracle db 221 : {0}");
            }

        }
        void Connectseven()
        {
            try
            {
                con = new OracleConnection();
                con.ConnectionString = dbcon222;
                con.Open();
                Console.WriteLine("Connected to Oracle db 2 " + con.ServerVersion);
                this.ReadSystemFile("7. Connected to Oracle db 2 " + con.ServerVersion);
                create_temp_table_seven();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                this.WriteToFile("7. " + ex.Message + "Connected to Oracle db 2 : {0}");
            }

        }
        void Connecteight()
        {
            try
            {
                con = new OracleConnection();
                con.ConnectionString = dbcon222;
                con.Open();
                Console.WriteLine("Connected to Oracle db 2 " + con.ServerVersion);
                this.ReadSystemFile("8. Connected to Oracle db 2 " + con.ServerVersion);
                create_temp_table_eight();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                this.WriteToFile("8. " + ex.Message + "Connected to Oracle db 2 : {0}");
            }

        }
        void Connectdb5ctk()
        {
            try
            {
                con = new OracleConnection();
                con.ConnectionString = dbcon222;
                con.Open();
                Console.WriteLine("Connected to Oracle db 2 " + con.ServerVersion);
                this.ReadSystemFile("9. Connected to Oracle db 2 " + con.ServerVersion);
                create_temp_table_db5cetak();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                this.WriteToFile("9. " + ex.Message + "Connected to Oracle db 2 : {0}");
            }

        }

        void Connectdb5rkm()
        {
            try
            {
                con = new OracleConnection();
                con.ConnectionString = dbcon222;
                con.Open();
                Console.WriteLine("Connected to Oracle db 2 " + con.ServerVersion);
                this.ReadSystemFile("10. Connected to Oracle db 2 " + con.ServerVersion);
                create_temp_table_db5rekam();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                this.WriteToFile("10. " + ex.Message + "Connected to Oracle db 2 : {0}");
            }
            
           
        }

        void Connectdb5ctkupdt()
        {
            try
            {
                con = new OracleConnection();
                con.ConnectionString = dbcon222;
                con.Open();
                Console.WriteLine("Connected to Oracle db 2 " + con.ServerVersion);
                this.ReadSystemFile("11. Connected to Oracle db 2 " + con.ServerVersion);
                create_temp_table_5cetak();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                this.WriteToFile("11. " + ex.Message + "Connected to Oracle db 2 : {0}");
            }

        }

        void Connectdb5rkmupdt()
        {
            try
            {
                con = new OracleConnection();
                con.ConnectionString = dbcon222;
                con.Open();
                Console.WriteLine("Connected to Oracle db 2 " + con.ServerVersion);
                this.ReadSystemFile("12. Connected to Oracle db 2 " + con.ServerVersion);
                create_temp_table_5rekam();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                this.WriteToFile("12. " + ex.Message + "Connected to Oracle db 2 : {0}");
            }

        }

        void Connectcardmngmnt()
        {
            try
            {
                con = new OracleConnection();
                con.ConnectionString = dbcon222;
                con.Open();
                Console.WriteLine("Connected to Oracle db 2 " + con.ServerVersion);
                this.ReadSystemFile("13. Connected to Oracle db 2 " + con.ServerVersion);
                create_temp_table_card_management();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                this.WriteToFile("13. " + ex.Message + "Connected to Oracle db 2 : {0}");
            }

        }

        void Connectdbrekam()
        {
            try
            {
                con = new OracleConnection();
                con.ConnectionString = dbcon222;
                con.Open();
                Console.WriteLine("Connected to Oracle db 2 " + con.ServerVersion);
                this.ReadSystemFile("14. Connected to Oracle db 2 " + con.ServerVersion);
                create_temp_table_db221rekam();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                this.WriteToFile("14. " + ex.Message + "Connected to Oracle db 2 : {0}");
            }

            try
            {
                con = new OracleConnection();
                con.ConnectionString = dbcon222;
                con.Open();
                Console.WriteLine("Connected to Oracle db 5 " + con.ServerVersion);
                this.ReadSystemFile("14. Connected to Oracle db 5 " + con.ServerVersion);
                create_temp_table_db5rekam_all();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                this.WriteToFile("14. " + ex.Message + "Connected to Oracle db 5 : {0}");
            }

        }



        void Connectdbsync()
        {
            try
            {
                con = new OracleConnection();
                con.ConnectionString = dbcon221;
                con.Open();
                Console.WriteLine("Connected to Oracle db 2 " + con.ServerVersion);
                this.ReadSystemFile("15. Connected to Oracle db 2 " + con.ServerVersion);
                create_temp_table_sync();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                this.WriteToFile("15. " + ex.Message + "Connected to Oracle db 2 : {0}");
            }

        }
        
        void Closeall()
        {
            con.Close();
            con.Dispose();
        }
        #endregion

        #region Demogh 221 di update dari demogh 2
        public void create_temp_table_first()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    Console.WriteLine("1. State Demog 221 From Demog 2: {0}", connection.State);
                    this.WriteToFile("1. State Demog 221 From Demog 2: " + connection.State);
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "SELECT COUNT(1) AS CNT FROM (SELECT B.ID_STATUS, A.NIK, A.NAMA_LGKP,A.CURRENT_STATUS_CODE,A.CREATED, A.CREATED_USERNAME,A.LAST_UPDATED,A.LAST_UPDATED_USERNAME FROM DEMOGRAPHICS@DB2 A LEFT JOIN STATUS_ID B ON A.CURRENT_STATUS_CODE = B.CODE) A INNER JOIN (SELECT B.ID_STATUS, A.NIK, A.NAMA_LGKP,A.CURRENT_STATUS_CODE,A.CREATED, A.CREATED_USERNAME,A.LAST_UPDATED,A.LAST_UPDATED_USERNAME FROM DEMOGRAPHICS@DB221 A LEFT JOIN STATUS_ID B ON A.CURRENT_STATUS_CODE = B.CODE) B ON A.NIK = B.NIK WHERE A.ID_STATUS > B.ID_STATUS";
                    OracleDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        decimal cnt = (decimal)reader["CNT"];
                        Console.WriteLine("count data: {0}", cnt);
                        if (cnt > 0)
                        {
                            this.WriteToFile("1. Proccess Starting demogh 2 Update 221 with " + cnt.ToString() + " Data {0}");
                            do_select_table_first();
                        }
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("1. " + ex.Message + "Proccess demogh 2 Update 221 {0}");
                }
                finally
                {
                    connection.Close();
                }

            }
        }
        public void do_select_table_first()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "SELECT count(1) as CNT  FROM all_tables WHERE TABLE_NAME = 'TEMP_DEMOGH'";
                    OracleDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        decimal cnt = (decimal)reader["CNT"];
                        Console.WriteLine("count table: {0}", cnt);
                        if (cnt > 0)
                        {
                            do_delete_temp_one_first();

                        }
                        else
                        {
                            do_create_table_first();
                        }
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("1. " + ex.Message + "Proccess demogh 2 Update 221 {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public void do_delete_temp_one_first()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "DROP TABLE TEMP_DEMOGH";
                    command.CommandType = CommandType.Text;
                    Console.WriteLine("DROP TABLE TEMP_DEMOGH");
                    command.ExecuteNonQuery();

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("1. " + ex.Message + "Proccess demogh 2 Update 221 {0}");
                }
                finally
                {
                    connection.Close();
                }
                do_create_table_first();
            }
        }
        public void do_delete_temp_last_first()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "DROP TABLE TEMP_DEMOGH";
                    command.CommandType = CommandType.Text;
                    Console.WriteLine("DROP TABLE TEMP_DEMOGH");
                    command.ExecuteNonQuery();
                    this.WriteToFile("1. Proccess Finish " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("1. " + ex.Message + "Proccess demogh 2 Update 221 {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public void do_create_table_first()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO CREATE TABLE TEMP_DEMOGH");
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    OracleTransaction commited;
                    commited = connection.BeginTransaction(IsolationLevel.ReadCommitted);
                    command.Transaction = commited;
                    command.CommandText = "CREATE TABLE TEMP_DEMOGH AS SELECT A.* FROM (SELECT B.ID_STATUS, A.NIK, A.NAMA_LGKP,A.CURRENT_STATUS_CODE,A.CREATED, A.CREATED_USERNAME,A.LAST_UPDATED,A.LAST_UPDATED_USERNAME FROM DEMOGRAPHICS@DB2 A LEFT JOIN STATUS_ID B ON A.CURRENT_STATUS_CODE = B.CODE) A INNER JOIN (SELECT B.ID_STATUS, A.NIK, A.NAMA_LGKP,A.CURRENT_STATUS_CODE,A.CREATED, A.CREATED_USERNAME,A.LAST_UPDATED,A.LAST_UPDATED_USERNAME FROM DEMOGRAPHICS@DB221 A LEFT JOIN STATUS_ID B ON A.CURRENT_STATUS_CODE = B.CODE) B ON A.NIK = B.NIK WHERE A.ID_STATUS > B.ID_STATUS";
                    Console.WriteLine("CREATE TABLE TEMP_DEMOGH");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    commited.Commit();
                    this.WriteToFile("1. Proccess Create Table " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine("do_create_table_first" + ex.Message);
                    this.WriteToFile("1. " + ex.Message + "Proccess demogh 2 Update 221 {0}");
                }
                finally
                {
                    connection.Close();
                }
                do_update_table_first();
            }
        }

        public void do_update_table_first()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO UPDATE TABLE TEMP_DEMOGH");
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "UPDATE DEMOGRAPHICS@DB221 A SET (A.CURRENT_STATUS_CODE, A.TGL_CETAK_KTP,A.CREATED_USERNAME,A.LAST_UPDATED,A.LAST_UPDATED_USERNAME) = (SELECT B.CURRENT_STATUS_CODE, B.CREATED,B.CREATED_USERNAME,B.LAST_UPDATED,B.LAST_UPDATED_USERNAME FROM TEMP_DEMOGH B WHERE A.NIK = B.NIK) WHERE EXISTS (SELECT 1 FROM TEMP_DEMOGH B WHERE A.NIK = B.NIK )";
                    Console.WriteLine("UPDATE TABLE TEMP_DEMOGH");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("1. Proccess Update Table " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("1. " + ex.Message + "Proccess demogh 2 Update 221 {0}");
                }
                finally
                {
                    connection.Close();
                }
                do_delete_temp_last_first();
            }
        }
        #endregion

        #region Demogh 2 di update dari demogh 221
        public void create_temp_table_second()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    Console.WriteLine("2. State Demog 2 From Demog 221: {0}", connection.State);
                    this.WriteToFile("2. State Demog 2 From Demog 221: " + connection.State);

                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "SELECT COUNT(1) AS CNT FROM (SELECT B.ID_STATUS, A.NIK, A.NAMA_LGKP,A.CURRENT_STATUS_CODE,A.CREATED, A.CREATED_USERNAME,A.LAST_UPDATED,A.LAST_UPDATED_USERNAME FROM DEMOGRAPHICS@DB2 A LEFT JOIN STATUS_ID B ON A.CURRENT_STATUS_CODE = B.CODE) A INNER JOIN (SELECT B.ID_STATUS, A.NIK, A.NAMA_LGKP,A.CURRENT_STATUS_CODE,A.CREATED, A.CREATED_USERNAME,A.LAST_UPDATED,A.LAST_UPDATED_USERNAME FROM DEMOGRAPHICS@DB221 A LEFT JOIN STATUS_ID B ON A.CURRENT_STATUS_CODE = B.CODE) B ON A.NIK = B.NIK WHERE A.ID_STATUS < B.ID_STATUS";
                    OracleDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        decimal cnt = (decimal)reader["CNT"];
                        Console.WriteLine("count data: {0}", cnt);
                        if (cnt > 0)
                        {
                            this.WriteToFile("2. Proccess Starting demogh 221 update 2 with " + cnt.ToString() + " Data {0}");
                            do_select_table_second();
                        }
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("2. " + ex.Message + " demogh 221 update 2 {0}");
                }
                finally
                {
                    connection.Close();
                }

            }
        }
        public void do_select_table_second()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "SELECT count(1) as CNT  FROM all_tables WHERE TABLE_NAME = 'TEMP_DEMOGH'";
                    OracleDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        decimal cnt = (decimal)reader["CNT"];
                        Console.WriteLine("count table: {0}", cnt);
                        if (cnt > 0)
                        {
                            do_delete_temp_one_second();

                        }
                        else
                        {
                            do_create_table_second();
                        }
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("2. " + ex.Message + " demogh 221 update 2 {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public void do_delete_temp_one_second()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "DROP TABLE TEMP_DEMOGH";
                    command.CommandType = CommandType.Text;
                    Console.WriteLine("DROP TABLE TEMP_DEMOGH");
                    command.ExecuteNonQuery();

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("2. " + ex.Message + " demogh 221 update 2 {0}");
                }
                finally
                {
                    connection.Close();
                }
                do_create_table_second();
            }
        }
        public void do_delete_temp_last_second()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "DROP TABLE TEMP_DEMOGH";
                    command.CommandType = CommandType.Text;
                    Console.WriteLine("DROP TABLE TEMP_DEMOGH");
                    command.ExecuteNonQuery();
                    this.WriteToFile("2. Proccess Finish " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("2. " + ex.Message + " demogh 221 update 2 {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public void do_create_table_second()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO CREATE TABLE TEMP_DEMOGH");
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    OracleTransaction commited;
                    commited = connection.BeginTransaction(IsolationLevel.ReadCommitted);
                    command.Transaction = commited;
                    command.CommandText = "CREATE TABLE TEMP_DEMOGH AS SELECT B.* FROM (SELECT B.ID_STATUS, A.NIK, A.NAMA_LGKP,A.CURRENT_STATUS_CODE,A.CREATED, A.CREATED_USERNAME,A.LAST_UPDATED,A.LAST_UPDATED_USERNAME FROM DEMOGRAPHICS@DB2 A LEFT JOIN STATUS_ID B ON A.CURRENT_STATUS_CODE = B.CODE) A INNER JOIN (SELECT B.ID_STATUS, A.NIK, A.NAMA_LGKP,A.CURRENT_STATUS_CODE,A.CREATED, A.CREATED_USERNAME,A.LAST_UPDATED,A.LAST_UPDATED_USERNAME FROM DEMOGRAPHICS@DB221 A LEFT JOIN STATUS_ID B ON A.CURRENT_STATUS_CODE = B.CODE) B ON A.NIK = B.NIK WHERE A.ID_STATUS < B.ID_STATUS";
                                           
                    Console.WriteLine("CREATE TABLE TEMP_DEMOGH");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    commited.Commit();
                    this.WriteToFile("2. Proccess Create Table " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine("do_create_table_second" + ex.Message);
                    this.WriteToFile("2. " + ex.Message + " demogh 221 update 2 {0}");
                }
                finally
                {
                    connection.Close();
                }
                do_update_table_second();
            }
        }

        public void do_update_table_second()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO UPDATE TABLE TEMP_DEMOGH");
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "UPDATE DEMOGRAPHICS@DB2 A SET (A.CURRENT_STATUS_CODE, A.CREATED,A.CREATED_USERNAME,A.LAST_UPDATED,A.LAST_UPDATED_USERNAME) = (SELECT B.CURRENT_STATUS_CODE, B.CREATED,B.CREATED_USERNAME,B.LAST_UPDATED,B.LAST_UPDATED_USERNAME FROM TEMP_DEMOGH B WHERE A.NIK = B.NIK) WHERE EXISTS (SELECT 1 FROM TEMP_DEMOGH B WHERE A.NIK = B.NIK )";
                    Console.WriteLine("UPDATE TABLE TEMP_DEMOGH");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("2. Proccess Update Table " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("2. " + ex.Message + " demogh 221 update 2 {0}");
                }
                finally
                {
                    connection.Close();
                }
                do_delete_temp_last_second();
            }
        }
        #endregion

        #region Demogh all 2 di update dari demogh 2
        public void create_temp_table_third()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    Console.WriteLine("3. State Demog All 2 From Demog 2: {0}", connection.State);

                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "SELECT COUNT(1) AS CNT FROM (SELECT B.ID_STATUS, A.NIK, A.NAMA_LGKP,A.CURRENT_STATUS_CODE,A.CREATED, A.CREATED_USERNAME,A.LAST_UPDATED,A.LAST_UPDATED_USERNAME FROM DEMOGRAPHICS@DB2 A LEFT JOIN STATUS_ID B ON A.CURRENT_STATUS_CODE = B.CODE) A INNER JOIN (SELECT B.ID_STATUS, A.NIK, A.NAMA_LGKP,A.CURRENT_STATUS_CODE,A.CREATED, A.CREATED_USERNAME,A.LAST_UPDATED,A.LAST_UPDATED_USERNAME FROM DEMOGRAPHICS_ALL@DB2 A LEFT JOIN STATUS_ID B ON A.CURRENT_STATUS_CODE = B.CODE) B ON A.NIK = B.NIK WHERE A.ID_STATUS > B.ID_STATUS";
                    OracleDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        decimal cnt = (decimal)reader["CNT"];
                        Console.WriteLine("count data: {0}", cnt);
                        if (cnt > 0)
                        {
                            this.WriteToFile("3. Proccess Starting demogh 2 update all 2 with " + cnt.ToString() + " Data {0}");
                            do_select_table_third();
                        }
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("3. " + ex.Message + " demogh 2 update all 2 {0}");
                }
                finally
                {
                    connection.Close();
                }

            }
        }
        public void do_select_table_third()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "SELECT count(1) as CNT  FROM all_tables WHERE TABLE_NAME = 'TEMP_DEMOGH'";
                    OracleDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        decimal cnt = (decimal)reader["CNT"];
                        Console.WriteLine("count table: {0}", cnt);
                        if (cnt > 0)
                        {
                            do_delete_temp_one_third();

                        }
                        else
                        {
                            do_create_table_third();
                        }
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("3. " + ex.Message + " demogh 2 update all 2 {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public void do_delete_temp_one_third()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "DROP TABLE TEMP_DEMOGH";
                    command.CommandType = CommandType.Text;
                    Console.WriteLine("DROP TABLE TEMP_DEMOGH");
                    command.ExecuteNonQuery();

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("3. " + ex.Message + " demogh 2 update all 2 {0}");
                }
                finally
                {
                    connection.Close();
                }
                do_create_table_third();
            }
        }
        public void do_delete_temp_last_third()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "DROP TABLE TEMP_DEMOGH";
                    command.CommandType = CommandType.Text;
                    Console.WriteLine("DROP TABLE TEMP_DEMOGH");
                    command.ExecuteNonQuery();
                    this.WriteToFile("3. Proccess Finish " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("3. " + ex.Message + " demogh 2 update all 2 {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public void do_create_table_third()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO CREATE TABLE TEMP_DEMOGH");
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    OracleTransaction commited;
                    commited = connection.BeginTransaction(IsolationLevel.ReadCommitted);
                    command.Transaction = commited;
                    command.CommandText = "CREATE TABLE TEMP_DEMOGH AS SELECT A.* FROM (SELECT B.ID_STATUS, A.NIK, A.NAMA_LGKP,A.CURRENT_STATUS_CODE,A.CREATED, A.CREATED_USERNAME,A.LAST_UPDATED,A.LAST_UPDATED_USERNAME FROM DEMOGRAPHICS@DB2 A LEFT JOIN STATUS_ID B ON A.CURRENT_STATUS_CODE = B.CODE) A INNER JOIN (SELECT B.ID_STATUS, A.NIK, A.NAMA_LGKP,A.CURRENT_STATUS_CODE,A.CREATED, A.CREATED_USERNAME,A.LAST_UPDATED,A.LAST_UPDATED_USERNAME FROM DEMOGRAPHICS_ALL@DB2 A LEFT JOIN STATUS_ID B ON A.CURRENT_STATUS_CODE = B.CODE) B ON A.NIK = B.NIK WHERE A.ID_STATUS > B.ID_STATUS";

                    Console.WriteLine("CREATE TABLE TEMP_DEMOGH");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    commited.Commit();
                    this.WriteToFile("3. Proccess Create Table " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine("do_create_table_third" + ex.Message);
                    this.WriteToFile("3. " + ex.Message + " demogh 2 update all 2 {0}");
                }
                finally
                {
                    connection.Close();
                }
                do_update_table_third();
            }
        }

        public void do_update_table_third()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO UPDATE TABLE TEMP_DEMOGH");
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "UPDATE DEMOGRAPHICS_ALL@DB2 A SET (A.CURRENT_STATUS_CODE, A.CREATED,A.CREATED_USERNAME,A.LAST_UPDATED,A.LAST_UPDATED_USERNAME) = (SELECT B.CURRENT_STATUS_CODE, B.CREATED,B.CREATED_USERNAME,B.LAST_UPDATED,B.LAST_UPDATED_USERNAME FROM TEMP_DEMOGH B WHERE A.NIK = B.NIK) WHERE EXISTS (SELECT 1 FROM TEMP_DEMOGH B WHERE A.NIK = B.NIK)";
                    Console.WriteLine("UPDATE TABLE TEMP_DEMOGH");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("3. " + "Proccess Update Table " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("3. " + ex.Message + " demogh 2 update all 2 {0}");
                }
                finally
                {
                    connection.Close();
                }
                do_delete_temp_last_third();
            }
        }
        #endregion
        
        #region Demogh 2 di update dari demogh all 2
        public void create_temp_table_four()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    Console.WriteLine("4. State Demog 2 From Demog All 2: {0}", connection.State);

                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "SELECT COUNT(1) AS CNT FROM (SELECT B.ID_STATUS, A.NIK, A.NAMA_LGKP,A.CURRENT_STATUS_CODE,A.CREATED, A.CREATED_USERNAME,A.LAST_UPDATED,A.LAST_UPDATED_USERNAME FROM DEMOGRAPHICS@DB2 A LEFT JOIN STATUS_ID B ON A.CURRENT_STATUS_CODE = B.CODE) A INNER JOIN (SELECT B.ID_STATUS, A.NIK, A.NAMA_LGKP,A.CURRENT_STATUS_CODE,A.CREATED, A.CREATED_USERNAME,A.LAST_UPDATED,A.LAST_UPDATED_USERNAME FROM DEMOGRAPHICS_ALL@DB2 A LEFT JOIN STATUS_ID B ON A.CURRENT_STATUS_CODE = B.CODE) B ON A.NIK = B.NIK WHERE A.ID_STATUS > B.ID_STATUS";
                    OracleDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        decimal cnt = (decimal)reader["CNT"];
                        Console.WriteLine("count data: {0}", cnt);
                        if (cnt > 0)
                        {
                            this.WriteToFile("4. " + "Proccess Starting demogh all 2 update 2 with " + cnt.ToString() + " Data {0}");
                            do_select_table_four();
                        }
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("4. " + ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }

            }
        }
        public void do_select_table_four()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "SELECT count(1) as CNT  FROM all_tables WHERE TABLE_NAME = 'TEMP_DEMOGH'";
                    OracleDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        decimal cnt = (decimal)reader["CNT"];
                        Console.WriteLine("count table: {0}", cnt);
                        if (cnt > 0)
                        {
                            do_delete_temp_one_four();

                        }
                        else
                        {
                            do_create_table_four();
                        }
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("4. " + ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public void do_delete_temp_one_four()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "DROP TABLE TEMP_DEMOGH";
                    command.CommandType = CommandType.Text;
                    Console.WriteLine("DROP TABLE TEMP_DEMOGH");
                    command.ExecuteNonQuery();

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("4. " + ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
                do_create_table_four();
            }
        }
        public void do_delete_temp_last_four()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "DROP TABLE TEMP_DEMOGH";
                    command.CommandType = CommandType.Text;
                    Console.WriteLine("DROP TABLE TEMP_DEMOGH");
                    command.ExecuteNonQuery();
                    this.WriteToFile("4. " + "Proccess Finish " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("4. " + ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public void do_create_table_four()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO CREATE TABLE TEMP_DEMOGH");
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    OracleTransaction commited;
                    commited = connection.BeginTransaction(IsolationLevel.ReadCommitted);
                    command.Transaction = commited;
                    command.CommandText = "CREATE TABLE TEMP_DEMOGH AS SELECT B.* FROM (SELECT B.ID_STATUS, A.NIK, A.NAMA_LGKP,A.CURRENT_STATUS_CODE,A.CREATED, A.CREATED_USERNAME,A.LAST_UPDATED,A.LAST_UPDATED_USERNAME FROM DEMOGRAPHICS@DB2 A LEFT JOIN STATUS_ID B ON A.CURRENT_STATUS_CODE = B.CODE) A INNER JOIN (SELECT B.ID_STATUS, A.NIK, A.NAMA_LGKP,A.CURRENT_STATUS_CODE,A.CREATED, A.CREATED_USERNAME,A.LAST_UPDATED,A.LAST_UPDATED_USERNAME FROM DEMOGRAPHICS@DB221 A LEFT JOIN STATUS_ID B ON A.CURRENT_STATUS_CODE = B.CODE) B ON A.NIK = B.NIK WHERE A.ID_STATUS < B.ID_STATUS";

                    Console.WriteLine("CREATE TABLE TEMP_DEMOGH");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    commited.Commit();
                    this.WriteToFile("4. " + "Proccess Create Table " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine("do_create_table_four" + ex.Message);
                    this.WriteToFile("4. " + ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
                do_update_table_four();
            }
        }

        public void do_update_table_four()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO UPDATE TABLE TEMP_DEMOGH");
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "UPDATE DEMOGRAPHICS@DB2 A SET (A.CURRENT_STATUS_CODE, A.CREATED,A.CREATED_USERNAME,A.LAST_UPDATED,A.LAST_UPDATED_USERNAME) = (SELECT B.CURRENT_STATUS_CODE, B.CREATED,B.CREATED_USERNAME,B.LAST_UPDATED,B.LAST_UPDATED_USERNAME FROM TEMP_DEMOGH B WHERE A.NIK = B.NIK) WHERE EXISTS (SELECT 1 FROM TEMP_DEMOGH B WHERE A.NIK = B.NIK )";
                    Console.WriteLine("UPDATE TABLE TEMP_DEMOGH");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("4. " + "Proccess Update Table " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("4. " + ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
                do_delete_temp_last_four();
            }
        }
        #endregion

        #region Demogh all 2 di update dari demogh 221
        public void create_temp_table_five()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    Console.WriteLine("5. State Demog All 2 From Demog 221: {0}", connection.State);

                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "SELECT COUNT(1) AS CNT FROM (SELECT B.ID_STATUS, A.NIK, A.NAMA_LGKP,A.CURRENT_STATUS_CODE,A.CREATED, A.CREATED_USERNAME,A.LAST_UPDATED,A.LAST_UPDATED_USERNAME FROM DEMOGRAPHICS@DB221 A LEFT JOIN STATUS_ID B ON A.CURRENT_STATUS_CODE = B.CODE) A INNER JOIN (SELECT B.ID_STATUS, A.NIK, A.NAMA_LGKP,A.CURRENT_STATUS_CODE,A.CREATED, A.CREATED_USERNAME,A.LAST_UPDATED,A.LAST_UPDATED_USERNAME FROM DEMOGRAPHICS_ALL@DB2 A LEFT JOIN STATUS_ID B ON A.CURRENT_STATUS_CODE = B.CODE) B ON A.NIK = B.NIK WHERE A.ID_STATUS > B.ID_STATUS";
                    OracleDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        decimal cnt = (decimal)reader["CNT"];
                        Console.WriteLine("count data: {0}", cnt);
                        if (cnt > 0)
                        {
                            this.WriteToFile("5. Proccess Starting demogh all 2 update 2 with " + cnt.ToString() + " Data {0}");
                            do_select_table_five();
                        }
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("5. " + ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }

            }
        }
        public void do_select_table_five()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "SELECT count(1) as CNT  FROM all_tables WHERE TABLE_NAME = 'TEMP_DEMOGH'";
                    OracleDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        decimal cnt = (decimal)reader["CNT"];
                        Console.WriteLine("count table: {0}", cnt);
                        if (cnt > 0)
                        {
                            do_delete_temp_one_five();

                        }
                        else
                        {
                            do_create_table_five();
                        }
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("5. " + ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public void do_delete_temp_one_five()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "DROP TABLE TEMP_DEMOGH";
                    command.CommandType = CommandType.Text;
                    Console.WriteLine("DROP TABLE TEMP_DEMOGH");
                    command.ExecuteNonQuery();

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("5. " + ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
                do_create_table_five();
            }
        }
        public void do_delete_temp_last_five()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "DROP TABLE TEMP_DEMOGH";
                    command.CommandType = CommandType.Text;
                    Console.WriteLine("DROP TABLE TEMP_DEMOGH");
                    command.ExecuteNonQuery();
                    this.WriteToFile("5. Proccess Finish " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("5. " + ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public void do_create_table_five()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO CREATE TABLE TEMP_DEMOGH");
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    OracleTransaction commited;
                    commited = connection.BeginTransaction(IsolationLevel.ReadCommitted);
                    command.Transaction = commited;
                    command.CommandText = "CREATE TABLE TEMP_DEMOGH AS SELECT A.* FROM (SELECT B.ID_STATUS, A.NIK, A.NAMA_LGKP,A.CURRENT_STATUS_CODE,A.CREATED, A.CREATED_USERNAME,A.LAST_UPDATED,A.LAST_UPDATED_USERNAME FROM DEMOGRAPHICS@DB221 A LEFT JOIN STATUS_ID B ON A.CURRENT_STATUS_CODE = B.CODE) A INNER JOIN (SELECT B.ID_STATUS, A.NIK, A.NAMA_LGKP,A.CURRENT_STATUS_CODE,A.CREATED, A.CREATED_USERNAME,A.LAST_UPDATED,A.LAST_UPDATED_USERNAME FROM DEMOGRAPHICS_ALL@DB2 A LEFT JOIN STATUS_ID B ON A.CURRENT_STATUS_CODE = B.CODE) B ON A.NIK = B.NIK WHERE A.ID_STATUS > B.ID_STATUS";

                    Console.WriteLine("CREATE TABLE TEMP_DEMOGH");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    commited.Commit();
                    this.WriteToFile("5. Proccess Create Table " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine("do_create_table_five" + ex.Message);
                    this.WriteToFile("5. " + ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
                do_update_table_five();
            }
        }

        public void do_update_table_five()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO UPDATE TABLE TEMP_DEMOGH");
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "UPDATE DEMOGRAPHICS_ALL@DB2 A SET (A.CURRENT_STATUS_CODE, A.CREATED,A.CREATED_USERNAME,A.LAST_UPDATED,A.LAST_UPDATED_USERNAME) = (SELECT B.CURRENT_STATUS_CODE, B.CREATED,B.CREATED_USERNAME,B.LAST_UPDATED,B.LAST_UPDATED_USERNAME FROM TEMP_DEMOGH B WHERE A.NIK = B.NIK) WHERE EXISTS (SELECT 1 FROM TEMP_DEMOGH B WHERE A.NIK = B.NIK)";
                    Console.WriteLine("UPDATE TABLE TEMP_DEMOGH");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("5. Proccess Update Table " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("5. " + ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
                do_delete_temp_last_five();
            }
        }
        #endregion

        #region Demogh all 2 di update dari demogh 5 cetak
        public void create_temp_table_5cetak()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    Console.WriteLine("11. State Demog All 2 From Demog 221: {0}", connection.State);

                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "SELECT COUNT(1) AS CNT FROM (SELECT B.ID_STATUS, A.NIK, A.NAMA_LGKP,A.CURRENT_STATUS_CODE,A.CREATED, A.CREATED_USERNAME,A.LAST_UPDATED,A.LAST_UPDATED_USERNAME FROM DEMOGRAPHICS@DB5CETAK A LEFT JOIN STATUS_ID B ON A.CURRENT_STATUS_CODE = B.CODE) A INNER JOIN (SELECT B.ID_STATUS, A.NIK, A.NAMA_LGKP,A.CURRENT_STATUS_CODE,A.CREATED, A.CREATED_USERNAME,A.LAST_UPDATED,A.LAST_UPDATED_USERNAME FROM DEMOGRAPHICS_ALL@DB2 A LEFT JOIN STATUS_ID B ON A.CURRENT_STATUS_CODE = B.CODE) B ON A.NIK = B.NIK WHERE A.ID_STATUS > B.ID_STATUS";
                    OracleDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        decimal cnt = (decimal)reader["CNT"];
                        Console.WriteLine("count data: {0}", cnt);
                        if (cnt > 0)
                        {
                            this.WriteToFile("11. Proccess Starting demogh all 2 update 2 with " + cnt.ToString() + " Data {0}");
                            do_select_table_5cetak();
                        }
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("11. " + ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }

            }
        }
        public void do_select_table_5cetak()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "SELECT count(1) as CNT  FROM all_tables WHERE TABLE_NAME = 'TEMP_DEMOGH'";
                    OracleDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        decimal cnt = (decimal)reader["CNT"];
                        Console.WriteLine("count table: {0}", cnt);
                        if (cnt > 0)
                        {
                            do_delete_temp_one_5cetak();

                        }
                        else
                        {
                            do_create_table_5cetak();
                        }
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("11. " + ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public void do_delete_temp_one_5cetak()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "DROP TABLE TEMP_DEMOGH";
                    command.CommandType = CommandType.Text;
                    Console.WriteLine("DROP TABLE TEMP_DEMOGH");
                    command.ExecuteNonQuery();

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("11. " + ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
                do_create_table_5cetak();
            }
        }
        public void do_delete_temp_last_5cetak()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "DROP TABLE TEMP_DEMOGH";
                    command.CommandType = CommandType.Text;
                    Console.WriteLine("DROP TABLE TEMP_DEMOGH");
                    command.ExecuteNonQuery();
                    this.WriteToFile("11. Proccess Finish " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile(ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public void do_create_table_5cetak()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO CREATE TABLE TEMP_DEMOGH");
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    OracleTransaction commited;
                    commited = connection.BeginTransaction(IsolationLevel.ReadCommitted);
                    command.Transaction = commited;
                    command.CommandText = "CREATE TABLE TEMP_DEMOGH AS SELECT A.* FROM (SELECT B.ID_STATUS, A.NIK, A.NAMA_LGKP,A.CURRENT_STATUS_CODE,A.CREATED, A.CREATED_USERNAME,A.LAST_UPDATED,A.LAST_UPDATED_USERNAME FROM DEMOGRAPHICS@DB5CETAK A LEFT JOIN STATUS_ID B ON A.CURRENT_STATUS_CODE = B.CODE) A INNER JOIN (SELECT B.ID_STATUS, A.NIK, A.NAMA_LGKP,A.CURRENT_STATUS_CODE,A.CREATED, A.CREATED_USERNAME,A.LAST_UPDATED,A.LAST_UPDATED_USERNAME FROM DEMOGRAPHICS_ALL@DB2 A LEFT JOIN STATUS_ID B ON A.CURRENT_STATUS_CODE = B.CODE) B ON A.NIK = B.NIK WHERE A.ID_STATUS > B.ID_STATUS";

                    Console.WriteLine("CREATE TABLE TEMP_DEMOGH");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    commited.Commit();
                    this.WriteToFile("11. Proccess Create Table " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine("do_create_table_5cetak" + ex.Message);
                    this.WriteToFile("11. " + ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
                do_update_table_5cetak();
            }
        }

        public void do_update_table_5cetak()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO UPDATE TABLE TEMP_DEMOGH");
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "UPDATE DEMOGRAPHICS_ALL@DB2 A SET (A.CURRENT_STATUS_CODE, A.CREATED,A.CREATED_USERNAME,A.LAST_UPDATED,A.LAST_UPDATED_USERNAME) = (SELECT B.CURRENT_STATUS_CODE, B.CREATED,B.CREATED_USERNAME,B.LAST_UPDATED,B.LAST_UPDATED_USERNAME FROM TEMP_DEMOGH B WHERE A.NIK = B.NIK) WHERE EXISTS (SELECT 1 FROM TEMP_DEMOGH B WHERE A.NIK = B.NIK)";
                    Console.WriteLine("UPDATE TABLE TEMP_DEMOGH");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("11. Proccess Update Table " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("11. " + ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
                do_delete_temp_last_5cetak();
            }
        }
        #endregion

        #region Demogh all 2 di update dari demogh 5 rekam
        public void create_temp_table_5rekam()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    Console.WriteLine("12. State Demog All 2 From Demog 221: {0}", connection.State);

                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "SELECT COUNT(1) AS CNT FROM (SELECT B.ID_STATUS, A.NIK, A.NAMA_LGKP,A.CURRENT_STATUS_CODE,A.CREATED, A.CREATED_USERNAME,A.LAST_UPDATED,A.LAST_UPDATED_USERNAME FROM DEMOGRAPHICS@DB5 A LEFT JOIN STATUS_ID B ON A.CURRENT_STATUS_CODE = B.CODE) A INNER JOIN (SELECT B.ID_STATUS, A.NIK, A.NAMA_LGKP,A.CURRENT_STATUS_CODE,A.CREATED, A.CREATED_USERNAME,A.LAST_UPDATED,A.LAST_UPDATED_USERNAME FROM DEMOGRAPHICS_ALL@DB2 A LEFT JOIN STATUS_ID B ON A.CURRENT_STATUS_CODE = B.CODE) B ON A.NIK = B.NIK WHERE A.ID_STATUS > B.ID_STATUS";
                    OracleDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        decimal cnt = (decimal)reader["CNT"];
                        Console.WriteLine("count data: {0}", cnt);
                        if (cnt > 0)
                        {
                            this.WriteToFile("12. Proccess Starting demogh all 2 update 2 with " + cnt.ToString() + " Data {0}");
                            do_select_table_5rekam();
                        }
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("12. " + ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }

            }
        }
        public void do_select_table_5rekam()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "SELECT count(1) as CNT  FROM all_tables WHERE TABLE_NAME = 'TEMP_DEMOGH'";
                    OracleDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        decimal cnt = (decimal)reader["CNT"];
                        Console.WriteLine("count table: {0}", cnt);
                        if (cnt > 0)
                        {
                            do_delete_temp_one_5rekam();

                        }
                        else
                        {
                            do_create_table_5rekam();
                        }
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("12. " + ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public void do_delete_temp_one_5rekam()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "DROP TABLE TEMP_DEMOGH";
                    command.CommandType = CommandType.Text;
                    Console.WriteLine("DROP TABLE TEMP_DEMOGH");
                    command.ExecuteNonQuery();

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("12. " + ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
                do_create_table_5rekam();
            }
        }
        public void do_delete_temp_last_5rekam()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "DROP TABLE TEMP_DEMOGH";
                    command.CommandType = CommandType.Text;
                    Console.WriteLine("DROP TABLE TEMP_DEMOGH");
                    command.ExecuteNonQuery();
                    this.WriteToFile("12. Proccess Finish " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("12. " + ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public void do_create_table_5rekam()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO CREATE TABLE TEMP_DEMOGH");
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    OracleTransaction commited;
                    commited = connection.BeginTransaction(IsolationLevel.ReadCommitted);
                    command.Transaction = commited;
                    command.CommandText = "CREATE TABLE TEMP_DEMOGH AS SELECT A.* FROM (SELECT B.ID_STATUS, A.NIK, A.NAMA_LGKP,A.CURRENT_STATUS_CODE,A.CREATED, A.CREATED_USERNAME,A.LAST_UPDATED,A.LAST_UPDATED_USERNAME FROM DEMOGRAPHICS@DB5 A LEFT JOIN STATUS_ID B ON A.CURRENT_STATUS_CODE = B.CODE) A INNER JOIN (SELECT B.ID_STATUS, A.NIK, A.NAMA_LGKP,A.CURRENT_STATUS_CODE,A.CREATED, A.CREATED_USERNAME,A.LAST_UPDATED,A.LAST_UPDATED_USERNAME FROM DEMOGRAPHICS_ALL@DB2 A LEFT JOIN STATUS_ID B ON A.CURRENT_STATUS_CODE = B.CODE) B ON A.NIK = B.NIK WHERE A.ID_STATUS > B.ID_STATUS";

                    Console.WriteLine("CREATE TABLE TEMP_DEMOGH");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    commited.Commit();
                    this.WriteToFile("12. Proccess Create Table " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine("do_create_table_5rekam" + ex.Message);
                    this.WriteToFile("12. " + ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
                do_update_table_5rekam();
            }
        }

        public void do_update_table_5rekam()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO UPDATE TABLE TEMP_DEMOGH");
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "UPDATE DEMOGRAPHICS_ALL@DB2 A SET (A.CURRENT_STATUS_CODE, A.CREATED,A.CREATED_USERNAME,A.LAST_UPDATED,A.LAST_UPDATED_USERNAME) = (SELECT B.CURRENT_STATUS_CODE, B.CREATED,B.CREATED_USERNAME,B.LAST_UPDATED,B.LAST_UPDATED_USERNAME FROM TEMP_DEMOGH B WHERE A.NIK = B.NIK) WHERE EXISTS (SELECT 1 FROM TEMP_DEMOGH B WHERE A.NIK = B.NIK)";
                    Console.WriteLine("UPDATE TABLE TEMP_DEMOGH");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("12. Proccess Update Table " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("12. " + ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
                do_delete_temp_last_5rekam();
            }
        }
        #endregion

        #region Demogh 221 di update dari demogh all 2
        public void create_temp_table_six()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    Console.WriteLine("6. State Demog 221 From Demog All 2: {0}", connection.State);

                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "SELECT COUNT(1) AS CNT FROM (SELECT B.ID_STATUS, A.NIK, A.NAMA_LGKP,A.CURRENT_STATUS_CODE,A.CREATED, A.CREATED_USERNAME,A.LAST_UPDATED,A.LAST_UPDATED_USERNAME FROM DEMOGRAPHICS@DB221 A LEFT JOIN STATUS_ID B ON A.CURRENT_STATUS_CODE = B.CODE) A INNER JOIN (SELECT B.ID_STATUS, A.NIK, A.NAMA_LGKP,A.CURRENT_STATUS_CODE,A.CREATED, A.CREATED_USERNAME,A.LAST_UPDATED,A.LAST_UPDATED_USERNAME FROM DEMOGRAPHICS_ALL@DB2 A LEFT JOIN STATUS_ID B ON A.CURRENT_STATUS_CODE = B.CODE) B ON A.NIK = B.NIK WHERE A.ID_STATUS < B.ID_STATUS";
                    OracleDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        decimal cnt = (decimal)reader["CNT"];
                        Console.WriteLine("count data: {0}", cnt);
                        if (cnt > 0)
                        {
                            this.WriteToFile("6. Proccess Starting demogh all 2 update 221 with " + cnt.ToString() + " Data {0}");
                            do_select_table_six();
                        }
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("6. " + ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }

            }
        }
        public void do_select_table_six()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "SELECT count(1) as CNT  FROM all_tables WHERE TABLE_NAME = 'TEMP_DEMOGH'";
                    OracleDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        decimal cnt = (decimal)reader["CNT"];
                        Console.WriteLine("count table: {0}", cnt);
                        if (cnt > 0)
                        {
                            do_delete_temp_one_six();

                        }
                        else
                        {
                            do_create_table_six();
                        }
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("6. " + ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public void do_delete_temp_one_six()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "DROP TABLE TEMP_DEMOGH";
                    command.CommandType = CommandType.Text;
                    Console.WriteLine("DROP TABLE TEMP_DEMOGH");
                    command.ExecuteNonQuery();

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("6. " + ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
                do_create_table_six();
            }
        }
        public void do_delete_temp_last_six()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "DROP TABLE TEMP_DEMOGH";
                    command.CommandType = CommandType.Text;
                    Console.WriteLine("DROP TABLE TEMP_DEMOGH");
                    command.ExecuteNonQuery();
                    this.WriteToFile("6. Proccess Finish " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile(ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public void do_create_table_six()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO CREATE TABLE TEMP_DEMOGH");
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    OracleTransaction commited;
                    commited = connection.BeginTransaction(IsolationLevel.ReadCommitted);
                    command.Transaction = commited;
                    command.CommandText = "CREATE TABLE TEMP_DEMOGH AS SELECT B.* FROM (SELECT B.ID_STATUS, A.NIK, A.NAMA_LGKP,A.CURRENT_STATUS_CODE,A.CREATED, A.CREATED_USERNAME,A.LAST_UPDATED,A.LAST_UPDATED_USERNAME FROM DEMOGRAPHICS@DB221 A LEFT JOIN STATUS_ID B ON A.CURRENT_STATUS_CODE = B.CODE) A INNER JOIN (SELECT B.ID_STATUS, A.NIK, A.NAMA_LGKP,A.CURRENT_STATUS_CODE,A.CREATED, A.CREATED_USERNAME,A.LAST_UPDATED,A.LAST_UPDATED_USERNAME FROM DEMOGRAPHICS_ALL@DB2 A LEFT JOIN STATUS_ID B ON A.CURRENT_STATUS_CODE = B.CODE) B ON A.NIK = B.NIK WHERE A.ID_STATUS < B.ID_STATUS";

                    Console.WriteLine("CREATE TABLE TEMP_DEMOGH");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    commited.Commit();
                    this.WriteToFile("6. Proccess Create Table " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine("do_create_table_six" + ex.Message);
                    this.WriteToFile("6. " + ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
                do_update_table_six();
            }
        }

        public void do_update_table_six()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO UPDATE TABLE TEMP_DEMOGH");
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "UPDATE DEMOGRAPHICS@DB221 A SET (A.CURRENT_STATUS_CODE, A.TGL_CETAK_KTP,A.CREATED_USERNAME,A.LAST_UPDATED,A.LAST_UPDATED_USERNAME) = (SELECT B.CURRENT_STATUS_CODE, B.CREATED,B.CREATED_USERNAME,B.LAST_UPDATED,B.LAST_UPDATED_USERNAME FROM TEMP_DEMOGH B WHERE A.NIK = B.NIK) WHERE EXISTS (SELECT 1 FROM TEMP_DEMOGH B WHERE A.NIK = B.NIK)";
                    Console.WriteLine("UPDATE TABLE TEMP_DEMOGH");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("6. Proccess Update Table " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("6. " + ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
                do_delete_temp_last_six();
            }
        }
        #endregion

        #region Demogh 2 all di insert dari demogh 2
        public void create_temp_table_seven()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    Console.WriteLine("State Demog All 2 Insert From Demog 2: {0}", connection.State);

                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "SELECT COUNT(1) AS CNT FROM DEMOGRAPHICS@DB2 A LEFT OUTER JOIN DEMOGRAPHICS_ALL@DB2 B ON (A.NIK =B.NIK) WHERE B.NIK IS NULL";
                    OracleDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        decimal cnt = (decimal)reader["CNT"];
                        Console.WriteLine("count data: {0}", cnt);
                        if (cnt > 0)
                        {
                            this.WriteToFile("7. Proccess Starting demogh 2 insert all 2 with " + cnt.ToString() + " Data {0}");
                            do_update_table_seven();
                        }
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("7. " + ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }

            }
        }
       
        public void do_update_table_seven()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO INSERT TABLE DEMOGH ALL");
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "INSERT INTO DEMOGRAPHICS_ALL@DB2 ( NIK, NAMA_LGKP, TMPT_LHR, NAMA_PROP, NAMA_KAB, NAMA_KEC, NAMA_KEL, NO_PROP, NO_KAB, NO_KEC, NO_KEL, AGAMA, JENIS_KLMIN, GOL_DRH, STAT_KWN, TGL_LHR, IS_DIRTY, CURRENT_STATUS_CODE, CREATED, CREATED_USERNAME, NO_KTP, NO_PASPOR, NO_RT, NO_RW, PNYDNG_CCT, KLAIN_FSK, JENIS_PKRJN, KEBANGSAAN, GELAR, NO_AKTA_LHR, NO_AKTA_KWN, NO_AKTA_CRAI, NAMA_PET_REG, NAMA_LGKP_IBU, NIK_IBU, NAMA_LGKP_AYAH, NIK_AYAH, NO_KK, STAT_KTP, NAMA_KET_RT, NAMA_KET_RW, STAT_HBKEL, KET_AGAMA, TMPT_SBL, TGL_PJG_KTP, TGL_AKH_PASPOR, TGL_KWN, TGL_CRAI, TGL_ENTRI, TGL_UBAH, TGL_CETAK_KTP, TGL_GANTI_KTP, GLR_AGAMA, GLR_BANGSAWAN, GLR_AKADEMIS, EXCEPTION_CODE, LAST_UPDATED, LAST_UPDATED_USERNAME, UPLOAD_LOCATION, REGION_ID, LOCAL_ID, AKTA_LHR, AKTA_KWN, AKTA_CRAI, PDDK_AKH, STAT_HIDUP, FLAG_STATUS, NAMA_KEP, ALAMAT, DUSUN, KODE_POS, TELP, KWRNGRN, DOK_IMGR, TGL_DTBIT, TGL_AKH_DOK, SIAK_FLAG, COUNT_KTP, NO_DOK) SELECT  A.NIK, A.NAMA_LGKP, A.TMPT_LHR, A.NAMA_PROP, A.NAMA_KAB, A.NAMA_KEC, A.NAMA_KEL, A.NO_PROP, A.NO_KAB, A.NO_KEC, A.NO_KEL, A.AGAMA, A.JENIS_KLMIN, A.GOL_DRH, A.STAT_KWN, A.TGL_LHR, A.IS_DIRTY, A.CURRENT_STATUS_CODE, A.CREATED, A.CREATED_USERNAME, A.NO_KTP, A.NO_PASPOR, A.NO_RT, A.NO_RW, A.PNYDNG_CCT, A.KLAIN_FSK, A.JENIS_PKRJN, A.KEBANGSAAN, A.GELAR, A.NO_AKTA_LHR, A.NO_AKTA_KWN, A.NO_AKTA_CRAI, A.NAMA_PET_REG, A.NAMA_LGKP_IBU, A.NIK_IBU, A.NAMA_LGKP_AYAH, A.NIK_AYAH, A.NO_KK, A.STAT_KTP, A.NAMA_KET_RT, A.NAMA_KET_RW, A.STAT_HBKEL, A.KET_AGAMA, A.TMPT_SBL, A.TGL_PJG_KTP, A.TGL_AKH_PASPOR, A.TGL_KWN, A.TGL_CRAI, A.TGL_ENTRI, A.TGL_UBAH, A.TGL_CETAK_KTP, A.TGL_GANTI_KTP, A.GLR_AGAMA, A.GLR_BANGSAWAN, A.GLR_AKADEMIS, A.EXCEPTION_CODE, A.LAST_UPDATED, A.LAST_UPDATED_USERNAME, A.UPLOAD_LOCATION, A.REGION_ID, A.LOCAL_ID, A.AKTA_LHR, A.AKTA_KWN, A.AKTA_CRAI, A.PDDK_AKH, A.STAT_HIDUP, A.FLAG_STATUS, A.NAMA_KEP, A.ALAMAT, A.DUSUN, A.KODE_POS, A.TELP, A.KWRNGRN, A.DOK_IMGR, A.TGL_DTBIT, A.TGL_AKH_DOK, A.SIAK_FLAG, A.COUNT_KTP, A.NO_DOK FROM DEMOGRAPHICS@DB2 A WHERE NOT EXISTS (SELECT 1 FROM DEMOGRAPHICS_ALL@DB2 B WHERE A.NIK =B.NIK)";
                    Console.WriteLine("INSERT TABLE DEMOGH ALL");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("7. Proccess Insert Table " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("7. " + ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        #endregion

        #region Demogh 2 all di insert dari demogh 221
        public void create_temp_table_eight()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    Console.WriteLine("8. State Demog All 2 Insert From Demog 221: {0}", connection.State);

                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "SELECT COUNT(1) AS CNT FROM DEMOGRAPHICS@DB221 A LEFT OUTER JOIN DEMOGRAPHICS_ALL@DB2 B ON (A.NIK = B.NIK) WHERE B.NIK IS NULL";
                    OracleDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        decimal cnt = (decimal)reader["CNT"];
                        Console.WriteLine("count data: {0}", cnt);
                        if (cnt > 0)
                        {
                            this.WriteToFile("8. Proccess Starting demogh 221 insert all 2 with " + cnt.ToString() + " Data {0}");
                            do_update_table_eight();
                        }
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("8. " + ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }

            }
        }

        public void do_update_table_eight()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO INSERT TABLE DEMOGH ALL");
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "INSERT INTO DEMOGRAPHICS_ALL@DB2 ( NIK, NAMA_LGKP, TMPT_LHR, NAMA_PROP, NAMA_KAB, NAMA_KEC, NAMA_KEL, NO_PROP, NO_KAB, NO_KEC, NO_KEL, AGAMA, JENIS_KLMIN, GOL_DRH, STAT_KWN, TGL_LHR, IS_DIRTY, CURRENT_STATUS_CODE, CREATED, CREATED_USERNAME, NO_KTP, NO_PASPOR, NO_RT, NO_RW, PNYDNG_CCT, KLAIN_FSK, JENIS_PKRJN, KEBANGSAAN, GELAR, NO_AKTA_LHR, NO_AKTA_KWN, NO_AKTA_CRAI, NAMA_PET_REG, NAMA_LGKP_IBU, NIK_IBU, NAMA_LGKP_AYAH, NIK_AYAH, NO_KK, STAT_KTP, NAMA_KET_RT, NAMA_KET_RW, STAT_HBKEL, KET_AGAMA, TMPT_SBL, TGL_PJG_KTP, TGL_AKH_PASPOR, TGL_KWN, TGL_CRAI, TGL_ENTRI, TGL_UBAH, TGL_CETAK_KTP, TGL_GANTI_KTP, GLR_AGAMA, GLR_BANGSAWAN, GLR_AKADEMIS, EXCEPTION_CODE, LAST_UPDATED, LAST_UPDATED_USERNAME, UPLOAD_LOCATION, REGION_ID, LOCAL_ID, AKTA_LHR, AKTA_KWN, AKTA_CRAI, PDDK_AKH, STAT_HIDUP, FLAG_STATUS, NAMA_KEP, ALAMAT, DUSUN, KODE_POS, TELP, KWRNGRN, DOK_IMGR, TGL_DTBIT, TGL_AKH_DOK, SIAK_FLAG, COUNT_KTP, NO_DOK) SELECT  A.NIK, A.NAMA_LGKP, A.TMPT_LHR, A.NAMA_PROP, A.NAMA_KAB, A.NAMA_KEC, A.NAMA_KEL, A.NO_PROP, A.NO_KAB, A.NO_KEC, A.NO_KEL, A.AGAMA, A.JENIS_KLMIN, A.GOL_DRH, A.STAT_KWN, A.TGL_LHR, A.IS_DIRTY, A.CURRENT_STATUS_CODE, A.CREATED, A.CREATED_USERNAME, A.NO_KTP, A.NO_PASPOR, A.NO_RT, A.NO_RW, A.PNYDNG_CCT, A.KLAIN_FSK, A.JENIS_PKRJN, A.KEBANGSAAN, A.GELAR, A.NO_AKTA_LHR, A.NO_AKTA_KWN, A.NO_AKTA_CRAI, A.NAMA_PET_REG, A.NAMA_LGKP_IBU, A.NIK_IBU, A.NAMA_LGKP_AYAH, A.NIK_AYAH, A.NO_KK, A.STAT_KTP, A.NAMA_KET_RT, A.NAMA_KET_RW, A.STAT_HBKEL, A.KET_AGAMA, A.TMPT_SBL, A.TGL_PJG_KTP, A.TGL_AKH_PASPOR, A.TGL_KWN, A.TGL_CRAI, A.TGL_ENTRI, A.TGL_UBAH, A.TGL_CETAK_KTP, A.TGL_GANTI_KTP, A.GLR_AGAMA, A.GLR_BANGSAWAN, A.GLR_AKADEMIS, A.EXCEPTION_CODE, A.LAST_UPDATED, A.LAST_UPDATED_USERNAME, A.UPLOAD_LOCATION, A.REGION_ID, A.LOCAL_ID, A.AKTA_LHR, A.AKTA_KWN, A.AKTA_CRAI, A.PDDK_AKH, A.STAT_HIDUP, A.FLAG_STATUS, A.NAMA_KEP, A.ALAMAT, A.DUSUN, A.KODE_POS, A.TELP, A.KWRNGRN, A.DOK_IMGR, A.TGL_DTBIT, A.TGL_AKH_DOK, A.SIAK_FLAG, A.COUNT_KTP, A.NO_DOK FROM DEMOGRAPHICS@DB221 A WHERE NOT EXISTS (SELECT 1 FROM DEMOGRAPHICS_ALL@DB2 B WHERE A.NIK =B.NIK)";
                    Console.WriteLine("INSERT TABLE DEMOGH ALL");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("8. Proccess Insert Table " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("8. " + ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        #endregion

        #region Demogh 2 all di insert dari demogh 5rekam
        public void create_temp_table_db5rekam()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    Console.WriteLine("10. State Demog All 2 Insert From Demog 5 rekam: {0}", connection.State);

                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "SELECT COUNT(1) AS CNT FROM DEMOGRAPHICS@DB5 A LEFT OUTER JOIN DEMOGRAPHICS_ALL@DB2 B ON (A.NIK = B.NIK) WHERE B.NIK IS NULL";
                    OracleDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        decimal cnt = (decimal)reader["CNT"];
                        Console.WriteLine("count data: {0}", cnt);
                        if (cnt > 0)
                        {
                            this.WriteToFile("10. Proccess Starting demogh 221 insert all 2 with " + cnt.ToString() + " Data {0}");
                            do_update_table_db5rekam();
                        }
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("10. " + ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }

            }
        }

        public void do_update_table_db5rekam()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO INSERT TABLE DEMOGH ALL");
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "INSERT INTO DEMOGRAPHICS_ALL@DB2 ( NIK, NAMA_LGKP, TMPT_LHR, NAMA_PROP, NAMA_KAB, NAMA_KEC, NAMA_KEL, NO_PROP, NO_KAB, NO_KEC, NO_KEL, AGAMA, JENIS_KLMIN, GOL_DRH, STAT_KWN, TGL_LHR, IS_DIRTY, CURRENT_STATUS_CODE, CREATED, CREATED_USERNAME, NO_KTP, NO_PASPOR, NO_RT, NO_RW, PNYDNG_CCT, KLAIN_FSK, JENIS_PKRJN, KEBANGSAAN, GELAR, NO_AKTA_LHR, NO_AKTA_KWN, NO_AKTA_CRAI, NAMA_PET_REG, NAMA_LGKP_IBU, NIK_IBU, NAMA_LGKP_AYAH, NIK_AYAH, NO_KK, STAT_KTP, NAMA_KET_RT, NAMA_KET_RW, STAT_HBKEL, KET_AGAMA, TMPT_SBL, TGL_PJG_KTP, TGL_AKH_PASPOR, TGL_KWN, TGL_CRAI, TGL_ENTRI, TGL_UBAH, TGL_CETAK_KTP, TGL_GANTI_KTP, GLR_AGAMA, GLR_BANGSAWAN, GLR_AKADEMIS, EXCEPTION_CODE, LAST_UPDATED, LAST_UPDATED_USERNAME, UPLOAD_LOCATION, REGION_ID, LOCAL_ID, AKTA_LHR, AKTA_KWN, AKTA_CRAI, PDDK_AKH, STAT_HIDUP, FLAG_STATUS, NAMA_KEP, ALAMAT, DUSUN, KODE_POS, TELP, KWRNGRN, DOK_IMGR, TGL_DTBIT, TGL_AKH_DOK, SIAK_FLAG, COUNT_KTP, NO_DOK) SELECT  A.NIK, A.NAMA_LGKP, A.TMPT_LHR, A.NAMA_PROP, A.NAMA_KAB, A.NAMA_KEC, A.NAMA_KEL, A.NO_PROP, A.NO_KAB, A.NO_KEC, A.NO_KEL, A.AGAMA, A.JENIS_KLMIN, A.GOL_DRH, A.STAT_KWN, A.TGL_LHR, A.IS_DIRTY, A.CURRENT_STATUS_CODE, A.CREATED, A.CREATED_USERNAME, A.NO_KTP, A.NO_PASPOR, A.NO_RT, A.NO_RW, A.PNYDNG_CCT, A.KLAIN_FSK, A.JENIS_PKRJN, A.KEBANGSAAN, A.GELAR, A.NO_AKTA_LHR, A.NO_AKTA_KWN, A.NO_AKTA_CRAI, A.NAMA_PET_REG, A.NAMA_LGKP_IBU, A.NIK_IBU, A.NAMA_LGKP_AYAH, A.NIK_AYAH, A.NO_KK, A.STAT_KTP, A.NAMA_KET_RT, A.NAMA_KET_RW, A.STAT_HBKEL, A.KET_AGAMA, A.TMPT_SBL, A.TGL_PJG_KTP, A.TGL_AKH_PASPOR, A.TGL_KWN, A.TGL_CRAI, A.TGL_ENTRI, A.TGL_UBAH, A.TGL_CETAK_KTP, A.TGL_GANTI_KTP, A.GLR_AGAMA, A.GLR_BANGSAWAN, A.GLR_AKADEMIS, A.EXCEPTION_CODE, A.LAST_UPDATED, A.LAST_UPDATED_USERNAME, A.UPLOAD_LOCATION, A.REGION_ID, A.LOCAL_ID, A.AKTA_LHR, A.AKTA_KWN, A.AKTA_CRAI, A.PDDK_AKH, A.STAT_HIDUP, A.FLAG_STATUS, A.NAMA_KEP, A.ALAMAT, A.DUSUN, A.KODE_POS, A.TELP, A.KWRNGRN, A.DOK_IMGR, A.TGL_DTBIT, A.TGL_AKH_DOK, A.SIAK_FLAG, A.COUNT_KTP, A.NO_DOK FROM DEMOGRAPHICS@DB5 A WHERE NOT EXISTS (SELECT 1 FROM DEMOGRAPHICS_ALL@DB2 B WHERE A.NIK =B.NIK)";
                    Console.WriteLine("INSERT TABLE DEMOGH ALL");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("10. Proccess Insert Table " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("10. " + ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        #endregion

        #region Demogh 2 all di insert dari demogh 5cetak
        public void create_temp_table_db5cetak()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    Console.WriteLine("9. State Demog All 2 Insert From Demog 5 cetak: {0}", connection.State);

                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "SELECT COUNT(1) AS CNT FROM DEMOGRAPHICS@DB5 A LEFT OUTER JOIN DEMOGRAPHICS_ALL@DB2 B ON (A.NIK = B.NIK) WHERE B.NIK IS NULL";
                    OracleDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        decimal cnt = (decimal)reader["CNT"];
                        Console.WriteLine("count data: {0}", cnt);
                        if (cnt > 0)
                        {
                            this.WriteToFile("9. Proccess Starting demogh 5 insert all 2 with " + cnt.ToString() + " Data {0}");
                            do_update_table_db5cetak();
                        }
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("9. " + ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }

            }
        }

        public void do_update_table_db5cetak()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO INSERT TABLE DEMOGH ALL");
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "INSERT INTO DEMOGRAPHICS_ALL@DB2 ( NIK, NAMA_LGKP, TMPT_LHR, NAMA_PROP, NAMA_KAB, NAMA_KEC, NAMA_KEL, NO_PROP, NO_KAB, NO_KEC, NO_KEL, AGAMA, JENIS_KLMIN, GOL_DRH, STAT_KWN, TGL_LHR, IS_DIRTY, CURRENT_STATUS_CODE, CREATED, CREATED_USERNAME, NO_KTP, NO_PASPOR, NO_RT, NO_RW, PNYDNG_CCT, KLAIN_FSK, JENIS_PKRJN, KEBANGSAAN, GELAR, NO_AKTA_LHR, NO_AKTA_KWN, NO_AKTA_CRAI, NAMA_PET_REG, NAMA_LGKP_IBU, NIK_IBU, NAMA_LGKP_AYAH, NIK_AYAH, NO_KK, STAT_KTP, NAMA_KET_RT, NAMA_KET_RW, STAT_HBKEL, KET_AGAMA, TMPT_SBL, TGL_PJG_KTP, TGL_AKH_PASPOR, TGL_KWN, TGL_CRAI, TGL_ENTRI, TGL_UBAH, TGL_CETAK_KTP, TGL_GANTI_KTP, GLR_AGAMA, GLR_BANGSAWAN, GLR_AKADEMIS, EXCEPTION_CODE, LAST_UPDATED, LAST_UPDATED_USERNAME, UPLOAD_LOCATION, REGION_ID, LOCAL_ID, AKTA_LHR, AKTA_KWN, AKTA_CRAI, PDDK_AKH, STAT_HIDUP, FLAG_STATUS, NAMA_KEP, ALAMAT, DUSUN, KODE_POS, TELP, KWRNGRN, DOK_IMGR, TGL_DTBIT, TGL_AKH_DOK, SIAK_FLAG, COUNT_KTP, NO_DOK) SELECT  A.NIK, A.NAMA_LGKP, A.TMPT_LHR, A.NAMA_PROP, A.NAMA_KAB, A.NAMA_KEC, A.NAMA_KEL, A.NO_PROP, A.NO_KAB, A.NO_KEC, A.NO_KEL, A.AGAMA, A.JENIS_KLMIN, A.GOL_DRH, A.STAT_KWN, A.TGL_LHR, A.IS_DIRTY, A.CURRENT_STATUS_CODE, A.CREATED, A.CREATED_USERNAME, A.NO_KTP, A.NO_PASPOR, A.NO_RT, A.NO_RW, A.PNYDNG_CCT, A.KLAIN_FSK, A.JENIS_PKRJN, A.KEBANGSAAN, A.GELAR, A.NO_AKTA_LHR, A.NO_AKTA_KWN, A.NO_AKTA_CRAI, A.NAMA_PET_REG, A.NAMA_LGKP_IBU, A.NIK_IBU, A.NAMA_LGKP_AYAH, A.NIK_AYAH, A.NO_KK, A.STAT_KTP, A.NAMA_KET_RT, A.NAMA_KET_RW, A.STAT_HBKEL, A.KET_AGAMA, A.TMPT_SBL, A.TGL_PJG_KTP, A.TGL_AKH_PASPOR, A.TGL_KWN, A.TGL_CRAI, A.TGL_ENTRI, A.TGL_UBAH, A.TGL_CETAK_KTP, A.TGL_GANTI_KTP, A.GLR_AGAMA, A.GLR_BANGSAWAN, A.GLR_AKADEMIS, A.EXCEPTION_CODE, A.LAST_UPDATED, A.LAST_UPDATED_USERNAME, A.UPLOAD_LOCATION, A.REGION_ID, A.LOCAL_ID, A.AKTA_LHR, A.AKTA_KWN, A.AKTA_CRAI, A.PDDK_AKH, A.STAT_HIDUP, A.FLAG_STATUS, A.NAMA_KEP, A.ALAMAT, A.DUSUN, A.KODE_POS, A.TELP, A.KWRNGRN, A.DOK_IMGR, A.TGL_DTBIT, A.TGL_AKH_DOK, A.SIAK_FLAG, A.COUNT_KTP, A.NO_DOK FROM DEMOGRAPHICS@DB5CETAK A WHERE NOT EXISTS (SELECT 1 FROM DEMOGRAPHICS_ALL@DB2 B WHERE A.NIK =B.NIK)";
                    Console.WriteLine("INSERT TABLE DEMOGH ALL");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("9. Proccess Insert Table " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("9. " + ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        #endregion

        #region Demogh 221  di insert dari all 2
        public void create_temp_table_db221rekam()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    Console.WriteLine("State Demog 221 Insert From Demog all 2 : {0}", connection.State);

                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "SELECT COUNT(1) AS CNT FROM DEMOGRAPHICS_ALL@DB2 A LEFT OUTER JOIN DEMOGRAPHICS@DB221 B ON (A.NIK = B.NIK) WHERE B.NIK IS NULL";
                    OracleDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        decimal cnt = (decimal)reader["CNT"];
                        Console.WriteLine("count data: {0}", cnt);
                        if (cnt > 0)
                        {
                            this.WriteToFile("9. Proccess Starting Demog 221 Insert From Demog all 2 with " + cnt.ToString() + " Data {0}");
                            do_update_table_db221rekam();
                        }
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("9. " + ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }

            }
        }

        public void do_update_table_db221rekam()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO INSERT TABLE DEMOGH ALL");
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "INSERT INTO DEMOGRAPHICS@DB221 ( NIK, NAMA_LGKP, TMPT_LHR, NAMA_PROP, NAMA_KAB, NAMA_KEC, NAMA_KEL, NO_PROP, NO_KAB, NO_KEC, NO_KEL, AGAMA, JENIS_KLMIN, GOL_DRH, STAT_KWN, TGL_LHR, IS_DIRTY, CURRENT_STATUS_CODE, CREATED, CREATED_USERNAME, NO_KTP, NO_PASPOR, NO_RT, NO_RW, PNYDNG_CCT, KLAIN_FSK, JENIS_PKRJN, KEBANGSAAN, GELAR, NO_AKTA_LHR, NO_AKTA_KWN, NO_AKTA_CRAI, NAMA_PET_REG, NAMA_LGKP_IBU, NIK_IBU, NAMA_LGKP_AYAH, NIK_AYAH, NO_KK, STAT_KTP, NAMA_KET_RT, NAMA_KET_RW, STAT_HBKEL, KET_AGAMA, TMPT_SBL, TGL_PJG_KTP, TGL_AKH_PASPOR, TGL_KWN, TGL_CRAI, TGL_ENTRI, TGL_UBAH, TGL_CETAK_KTP, TGL_GANTI_KTP, GLR_AGAMA, GLR_BANGSAWAN, GLR_AKADEMIS, EXCEPTION_CODE, LAST_UPDATED, LAST_UPDATED_USERNAME, UPLOAD_LOCATION, REGION_ID, LOCAL_ID, AKTA_LHR, AKTA_KWN, AKTA_CRAI, PDDK_AKH, STAT_HIDUP, FLAG_STATUS, NAMA_KEP, ALAMAT, DUSUN, KODE_POS, TELP, KWRNGRN, DOK_IMGR, TGL_DTBIT, TGL_AKH_DOK, SIAK_FLAG, COUNT_KTP, NO_DOK) SELECT  A.NIK, A.NAMA_LGKP, A.TMPT_LHR, A.NAMA_PROP, A.NAMA_KAB, A.NAMA_KEC, A.NAMA_KEL, A.NO_PROP, A.NO_KAB, A.NO_KEC, A.NO_KEL, A.AGAMA, A.JENIS_KLMIN, A.GOL_DRH, A.STAT_KWN, A.TGL_LHR, A.IS_DIRTY, A.CURRENT_STATUS_CODE, A.CREATED, A.CREATED_USERNAME, A.NO_KTP, A.NO_PASPOR, A.NO_RT, A.NO_RW, A.PNYDNG_CCT, A.KLAIN_FSK, A.JENIS_PKRJN, A.KEBANGSAAN, A.GELAR, A.NO_AKTA_LHR, A.NO_AKTA_KWN, A.NO_AKTA_CRAI, A.NAMA_PET_REG, A.NAMA_LGKP_IBU, A.NIK_IBU, A.NAMA_LGKP_AYAH, A.NIK_AYAH, A.NO_KK, A.STAT_KTP, A.NAMA_KET_RT, A.NAMA_KET_RW, A.STAT_HBKEL, A.KET_AGAMA, A.TMPT_SBL, A.TGL_PJG_KTP, A.TGL_AKH_PASPOR, A.TGL_KWN, A.TGL_CRAI, A.TGL_ENTRI, A.TGL_UBAH, A.TGL_CETAK_KTP, A.TGL_GANTI_KTP, A.GLR_AGAMA, A.GLR_BANGSAWAN, A.GLR_AKADEMIS, A.EXCEPTION_CODE, A.LAST_UPDATED, A.LAST_UPDATED_USERNAME, A.UPLOAD_LOCATION, A.REGION_ID, A.LOCAL_ID, A.AKTA_LHR, A.AKTA_KWN, A.AKTA_CRAI, A.PDDK_AKH, A.STAT_HIDUP, A.FLAG_STATUS, A.NAMA_KEP, A.ALAMAT, A.DUSUN, A.KODE_POS, A.TELP, A.KWRNGRN, A.DOK_IMGR, A.TGL_DTBIT, A.TGL_AKH_DOK, A.SIAK_FLAG, A.COUNT_KTP, A.NO_DOK FROM DEMOGRAPHICS_ALL@DB2 A LEFT OUTER JOIN DEMOGRAPHICS@DB221 B ON A.NIK = B.NIK WHERE B.NIK IS NULL";
                    Console.WriteLine("INSERT TABLE DEMOGH ALL");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("9. Proccess Insert Table " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("9. " + ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        #endregion

        #region Demogh 5  di insert dari all 2
        public void create_temp_table_db5rekam_all()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    Console.WriteLine("State Demog 5rekam Insert From Demog all 2: {0}", connection.State);

                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "SELECT COUNT(1) AS CNT FROM DEMOGRAPHICS_ALL@DB2 A LEFT OUTER JOIN DEMOGRAPHICS@DB5 B ON (A.NIK = B.NIK) WHERE B.NIK IS NULL";
                    OracleDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        decimal cnt = (decimal)reader["CNT"];
                        Console.WriteLine("count data: {0}", cnt);
                        if (cnt > 0)
                        {
                            this.WriteToFile("9. Proccess Starting Demog 5rekam Insert From Demog all 2 with " + cnt.ToString() + " Data {0}");
                            do_update_table_db5rekam_all();
                        }
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("9. " + ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }

            }
        }

        public void do_update_table_db5rekam_all()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO INSERT TABLE DEMOGH ALL");
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "INSERT INTO DEMOGRAPHICS@DB5 ( NIK, NAMA_LGKP, TMPT_LHR, NAMA_PROP, NAMA_KAB, NAMA_KEC, NAMA_KEL, NO_PROP, NO_KAB, NO_KEC, NO_KEL, AGAMA, JENIS_KLMIN, GOL_DRH, STAT_KWN, TGL_LHR, IS_DIRTY, CURRENT_STATUS_CODE, CREATED, CREATED_USERNAME, NO_KTP, NO_PASPOR, NO_RT, NO_RW, PNYDNG_CCT, KLAIN_FSK, JENIS_PKRJN, KEBANGSAAN, GELAR, NO_AKTA_LHR, NO_AKTA_KWN, NO_AKTA_CRAI, NAMA_PET_REG, NAMA_LGKP_IBU, NIK_IBU, NAMA_LGKP_AYAH, NIK_AYAH, NO_KK, STAT_KTP, NAMA_KET_RT, NAMA_KET_RW, STAT_HBKEL, KET_AGAMA, TMPT_SBL, TGL_PJG_KTP, TGL_AKH_PASPOR, TGL_KWN, TGL_CRAI, TGL_ENTRI, TGL_UBAH, TGL_CETAK_KTP, TGL_GANTI_KTP, GLR_AGAMA, GLR_BANGSAWAN, GLR_AKADEMIS, EXCEPTION_CODE, LAST_UPDATED, LAST_UPDATED_USERNAME, UPLOAD_LOCATION, REGION_ID, LOCAL_ID, AKTA_LHR, AKTA_KWN, AKTA_CRAI, PDDK_AKH, STAT_HIDUP, FLAG_STATUS, NAMA_KEP, ALAMAT, DUSUN, KODE_POS, TELP, KWRNGRN, DOK_IMGR, TGL_DTBIT, TGL_AKH_DOK, SIAK_FLAG, COUNT_KTP, NO_DOK) SELECT  A.NIK, A.NAMA_LGKP, A.TMPT_LHR, A.NAMA_PROP, A.NAMA_KAB, A.NAMA_KEC, A.NAMA_KEL, A.NO_PROP, A.NO_KAB, A.NO_KEC, A.NO_KEL, A.AGAMA, A.JENIS_KLMIN, A.GOL_DRH, A.STAT_KWN, A.TGL_LHR, A.IS_DIRTY, A.CURRENT_STATUS_CODE, A.CREATED, A.CREATED_USERNAME, A.NO_KTP, A.NO_PASPOR, A.NO_RT, A.NO_RW, A.PNYDNG_CCT, A.KLAIN_FSK, A.JENIS_PKRJN, A.KEBANGSAAN, A.GELAR, A.NO_AKTA_LHR, A.NO_AKTA_KWN, A.NO_AKTA_CRAI, A.NAMA_PET_REG, A.NAMA_LGKP_IBU, A.NIK_IBU, A.NAMA_LGKP_AYAH, A.NIK_AYAH, A.NO_KK, A.STAT_KTP, A.NAMA_KET_RT, A.NAMA_KET_RW, A.STAT_HBKEL, A.KET_AGAMA, A.TMPT_SBL, A.TGL_PJG_KTP, A.TGL_AKH_PASPOR, A.TGL_KWN, A.TGL_CRAI, A.TGL_ENTRI, A.TGL_UBAH, A.TGL_CETAK_KTP, A.TGL_GANTI_KTP, A.GLR_AGAMA, A.GLR_BANGSAWAN, A.GLR_AKADEMIS, A.EXCEPTION_CODE, A.LAST_UPDATED, A.LAST_UPDATED_USERNAME, A.UPLOAD_LOCATION, A.REGION_ID, A.LOCAL_ID, A.AKTA_LHR, A.AKTA_KWN, A.AKTA_CRAI, A.PDDK_AKH, A.STAT_HIDUP, A.FLAG_STATUS, A.NAMA_KEP, A.ALAMAT, A.DUSUN, A.KODE_POS, A.TELP, A.KWRNGRN, A.DOK_IMGR, A.TGL_DTBIT, A.TGL_AKH_DOK, A.SIAK_FLAG, A.COUNT_KTP, A.NO_DOK FROM DEMOGRAPHICS_ALL@DB2 A LEFT OUTER JOIN DEMOGRAPHICS@DB5 B ON A.NIK = B.NIK WHERE B.NIK IS NULL";
                    Console.WriteLine("INSERT TABLE DEMOGH ALL");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("9. Proccess Insert Table " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("9. " + ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        #endregion

        #region Card Management Regional
        public void create_temp_table_card_management()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    Console.WriteLine("13. State Card Management: {0}", connection.State);

                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "SELECT COUNT(1) AS CNT FROM CARD_MANAGEMENT@DB5CETAK A LEFT OUTER JOIN CARD_MANAGEMENT@DB2 B ON (A.CHIP_ID = B.CHIP_ID) WHERE B.CHIP_ID IS NULL";
                    OracleDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        decimal cnt = (decimal)reader["CNT"];
                        Console.WriteLine("count data: {0}", cnt);
                        if (cnt > 0)
                        {
                            this.WriteToFile("13. Proccess Starting demogh 5 insert all 2 with " + cnt.ToString() + " Data {0}");
                            do_update_table_card_management();
                        }
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("13. " + ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }

            }
        }

        public void do_update_table_card_management()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO INSERT TABLE DEMOGH ALL");
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "INSERT INTO CARD_MANAGEMENT@DB2 ( CHIP_ID, FACTORY_OUT_DATE, CREATED, CREATED_USERNAME, NIK, PBS_IN_DATE, PBS_OUT_DATE, REGIONAL_IN_DATE, REGIONAL_OUT_DATE, PERSONALIZED_DATE, PERSONALIZED_LOCATION_CODE, ISSUED_DATE, ISSUED_LOCATION_CODE, TERMINATED_DATE, TERMINATED_REASON, CARD_REJECTED_INDICATOR, CARD_FAILURE_REASON, LAST_UPDATE, LAST_UPDATED_USERNAME, REPRINT_REASON) SELECT  A.CHIP_ID, A.FACTORY_OUT_DATE, A.CREATED, A.CREATED_USERNAME, A.NIK, A.PBS_IN_DATE, A.PBS_OUT_DATE, A.REGIONAL_IN_DATE, A.REGIONAL_OUT_DATE, A.PERSONALIZED_DATE, A.PERSONALIZED_LOCATION_CODE, A.ISSUED_DATE, ISSUED_LOCATION_CODE, A.TERMINATED_DATE, A.TERMINATED_REASON, A.CARD_REJECTED_INDICATOR, A.CARD_FAILURE_REASON, A.LAST_UPDATE, A.LAST_UPDATED_USERNAME, A.REPRINT_REASON FROM CARD_MANAGEMENT@DB5CETAK A WHERE NOT EXISTS (SELECT 1 FROM CARD_MANAGEMENT@DB2 B WHERE A.CHIP_ID =B.CHIP_ID)";
                    Console.WriteLine("INSERT TABLE DEMOGH ALL");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("13. Proccess Insert Table " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("13. " + ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        #endregion


        #region Update Demogh Card Printed
        public void do_update_table_nine()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO UPDATE CARD PRINTED");
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "UPDATE DEMOGRAPHICS@DB2 A SET A.CURRENT_STATUS_CODE = 'CARD_PRINTED' where A.CURRENT_STATUS_CODE <> 'CARD_PRINTED' AND EXISTS(SELECT 1 FROM CARD_MANAGEMENT@DB2 B WHERE A.NIK = B.NIK)";
                    Console.WriteLine("UPDATE CARD PRINTED");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("14. Proccess Update Table " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("14. " + ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        #endregion

        #region Update Demogh All Card Printed
        public void do_update_table_teen()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO UPDATE CARD PRINTED");
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "UPDATE DEMOGRAPHICS_ALL@DB2 A SET A.CURRENT_STATUS_CODE = 'CARD_PRINTED' WHERE A.CURRENT_STATUS_CODE <> 'CARD_PRINTED' AND EXISTS(SELECT 1 FROM CARD_MANAGEMENT@DB2 B WHERE A.NIK = B.NIK)";
                    Console.WriteLine("UPDATE CARD PRINTED");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("Proccess Update Table " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile(ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        #endregion

        #region Update Demogh Rekam Card Printed
        public void do_update_table_eleven()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    Console.WriteLine("READY TO UPDATE CARD PRINTED");
                    connection.ConnectionString = dbcon222;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "UPDATE DEMOGRAPHICS@DB221 A SET A.CURRENT_STATUS_CODE = 'CARD_PRINTED' WHERE A.CURRENT_STATUS_CODE <> 'CARD_PRINTED' AND EXISTS(SELECT 1 FROM CARD_MANAGEMENT B WHERE A.NIK = B.NIK)";
                    Console.WriteLine("UPDATE CARD PRINTED");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("Proccess Update Table " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile(ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        #endregion


        #region Update Demogh Rekam 221 FULL
        public void create_temp_table_sync()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                try
                {
                    connection.ConnectionString = dbcon221;
                    connection.Open();
                    Console.WriteLine("15. State Full Rekam: {0}", connection.State);

                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "SELECT COUNT(1) AS CNT FROM FACES@DB5 A LEFT OUTER JOIN FACES B ON (A.NIK = B.NIK) WHERE B.NIK IS NULL";
                    OracleDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        decimal cnt = (decimal)reader["CNT"];
                        Console.WriteLine("count data: {0}", cnt);
                        if (cnt > 0)
                        {
                            this.WriteToFile("15. Proccess Starting demogh State Full Rekam with " + cnt.ToString() + " Data {0}");
                            do_insert_sync();
                        }
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("15. " + ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }

            }
        }

        public void do_insert_sync()
        {
            using (OracleConnection connection = new OracleConnection())
            {
                #region FACES
                try
                {
                    Console.WriteLine("READY TO INSERT TABLE FACES");
                    connection.ConnectionString = dbcon221;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "INSERT INTO FACES SELECT  NIK ,CREATED ,CREATED_USERNAME ,IMAGE_TYPE_CODE ,FACE ,LAST_UPDATED ,LAST_UPDATED_USERNAME FROM  FACES@DB5 A WHERE NOT EXISTS (SELECT 1 FROM FACES B WHERE A.NIK = B.NIK)";
                    Console.WriteLine("INSERT FACES");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("15. Proccess Insert Table " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("15. " + ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
                #endregion

                #region FINGERS
                try
                {
                    Console.WriteLine("READY TO INSERT TABLE FINGERS");
                    connection.ConnectionString = dbcon221;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "INSERT INTO FINGERS SELECT NIK ,CREATED ,CREATED_USERNAME ,IMAGE_TYPE_CODE ,RIGHT_THUMB ,RIGHT_INDEX ,RIGHT_MIDDLE ,RIGHT_RING ,RIGHT_LITTLE ,LEFT_THUMB ,LEFT_INDEX ,LEFT_MIDDLE ,LEFT_RING ,LEFT_LITTLE ,LAST_UPDATED ,LAST_UPDATED_USERNAME ,OPERATOR_FINGER ,SUPERVISOR_FINGER FROM  FINGERS@DB5 A WHERE NOT EXISTS (SELECT 1 FROM FINGERS B WHERE A.NIK = B.NIK)";
                    Console.WriteLine("INSERT FINGERS");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("15. Proccess Insert Table " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("15. " + ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
                #endregion

                #region IRIS
                try
                {
                    Console.WriteLine("READY TO INSERT TABLE IRIS");
                    connection.ConnectionString = dbcon221;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "INSERT INTO IRIS SELECT NIK ,CREATED ,CREATED_USERNAME ,IMAGE_TYPE_CODE ,RIGHT_IRIS ,LEFT_IRIS ,LAST_UPDATED ,LAST_UPDATED_USERNAME FROM  IRIS@DB5 A WHERE NOT EXISTS (SELECT 1 FROM IRIS B WHERE A.NIK = B.NIK)";
                    Console.WriteLine("INSERT IRIS");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("15. Proccess Insert Table " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("15. " + ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
                #endregion

                #region SIGNATURES
                try
                {
                    Console.WriteLine("READY TO INSERT TABLE SIGNATURES");
                    connection.ConnectionString = dbcon221;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "INSERT INTO SIGNATURES SELECT NIK ,CREATED ,CREATED_USERNAME ,IMAGE_TYPE_CODE ,SIGNATURE ,RECEIPT ,LAST_UPDATED ,LAST_UPDATED_USERNAME FROM  SIGNATURES@DB5 A WHERE NOT EXISTS (SELECT 1 FROM SIGNATURES B WHERE A.NIK = B.NIK)";
                    Console.WriteLine("INSERT SIGNATURES");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("15. Proccess Insert Table " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("15. " + ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
                #endregion

                #region FACE_TEMPLATES
                try
                {
                    Console.WriteLine("READY TO INSERT TABLE FACE_TEMPLATES");
                    connection.ConnectionString = dbcon221;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "INSERT INTO FACE_TEMPLATES SELECT NIK ,CREATED ,CREATED_USERNAME ,TEMPLATE_TYPE_CODE ,FACE ,FACE_QUALITY ,LAST_UPDATED ,LAST_UPDATED_USERNAME FROM  FACE_TEMPLATES@DB5 A WHERE NOT EXISTS (SELECT 1 FROM FACE_TEMPLATES B WHERE A.NIK = B.NIK)";
                    Console.WriteLine("INSERT FACE_TEMPLATES");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("15. Proccess Insert Table " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("15. " + ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
                #endregion

                #region FINGER_TEMPLATES
                try
                {
                    Console.WriteLine("READY TO INSERT TABLE FINGER_TEMPLATES");
                    connection.ConnectionString = dbcon221;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "INSERT INTO FINGER_TEMPLATES SELECT NIK ,CREATED ,CREATED_USERNAME ,TEMPLATE_TYPE_CODE ,PROPRIETARY_TEMPLATE_TYPE_CODE ,PRIMARY_FINGER ,SECONDARY_FINGER ,CIVIL_RIGHT_THUMB ,CIVIL_RIGHT_INDEX ,CIVIL_RIGHT_MIDDLE ,CIVIL_RIGHT_RING ,CIVIL_RIGHT_LITTLE ,CIVIL_LEFT_THUMB ,CIVIL_LEFT_INDEX ,CIVIL_LEFT_MIDDLE ,CIVIL_LEFT_RING ,CIVIL_LEFT_LITTLE ,CIVIL_RIGHT_THUMB_QUALITY ,CIVIL_RIGHT_INDEX_QUALITY ,CIVIL_RIGHT_MIDDLE_QUALITY ,CIVIL_RIGHT_RING_QUALITY ,CIVIL_RIGHT_LITTLE_QUALITY ,CIVIL_LEFT_THUMB_QUALITY ,CIVIL_LEFT_INDEX_QUALITY ,CIVIL_LEFT_MIDDLE_QUALITY ,CIVIL_LEFT_RING_QUALITY ,CIVIL_LEFT_LITTLE_QUALITY ,LATENT_RIGHT_THUMB ,LATENT_RIGHT_INDEX ,LATENT_RIGHT_MIDDLE ,LATENT_RIGHT_RING ,LATENT_RIGHT_LITTLE ,LATENT_LEFT_THUMB ,LATENT_LEFT_INDEX ,LATENT_LEFT_MIDDLE ,LATENT_LEFT_RING ,LATENT_LEFT_LITTLE ,PRIMARY_FINGER_POS ,SECONDARY_FINGER_POS ,LAST_UPDATED ,LAST_UPDATED_USERNAME FROM  FINGER_TEMPLATES@DB5 A WHERE NOT EXISTS (SELECT 1 FROM FINGER_TEMPLATES B WHERE A.NIK = B.NIK)";
                    Console.WriteLine("INSERT FINGER_TEMPLATES");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("15. Proccess Insert Table " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("15. " + ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
                #endregion

                #region IRIS_TEMPLATES
                try
                {
                    Console.WriteLine("READY TO INSERT TABLE IRIS_TEMPLATES");
                    connection.ConnectionString = dbcon221;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "INSERT INTO IRIS_TEMPLATES SELECT NIK ,CREATED ,CREATED_USERNAME ,TEMPLATE_TYPE_CODE ,RIGHT_IRIS ,LEFT_IRIS ,RIGHT_IRIS_QUALITY ,LEFT_IRIS_QUALITY ,LAST_UPDATED ,LAST_UPDATED_USERNAME FROM  IRIS_TEMPLATES@DB5 A WHERE NOT EXISTS (SELECT 1 FROM IRIS_TEMPLATES B WHERE A.NIK = B.NIK)";
                    Console.WriteLine("INSERT IRIS_TEMPLATES");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("15. Proccess Insert Table " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("15. " + ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
                #endregion

                #region SIGNATURE_TEMPLATES
                try
                {
                    Console.WriteLine("READY TO INSERT TABLE SIGNATURE_TEMPLATES");
                    connection.ConnectionString = dbcon221;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "INSERT INTO SIGNATURE_TEMPLATES SELECT NIK ,SIGNATURE ,TEMPLATE_TYPE_CODE ,CREATED ,CREATED_USERNAME ,LAST_UPDATED ,LAST_UPDATED_USERNAME FROM  SIGNATURE_TEMPLATES@DB5 A WHERE NOT EXISTS (SELECT 1 FROM SIGNATURE_TEMPLATES B WHERE A.NIK = B.NIK)";
                    Console.WriteLine("INSERT SIGNATURE_TEMPLATES");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("15. Proccess Insert Table " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("15. " + ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
                #endregion

                #region BIOMETRICS_LOSSLESS
                try
                {
                    Console.WriteLine("READY TO INSERT TABLE BIOMETRICS_LOSSLESS");
                    connection.ConnectionString = dbcon221;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "INSERT INTO BIOMETRICS_LOSSLESS SELECT NIK ,CREATED ,CREATED_USERNAME ,FILE_NAME ,EXPORT_DATE ,IMPORT_DATE ,LAST_UPDATED ,LAST_UPDATED_USERNAME FROM  BIOMETRICS_LOSSLESS@DB5 A WHERE NOT EXISTS (SELECT 1 FROM BIOMETRICS_LOSSLESS B WHERE A.NIK = B.NIK)";
                    Console.WriteLine("INSERT BIOMETRICS_LOSSLESS");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("15. Proccess Insert Table " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("15. " + ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
                #endregion

                #region BIOMETRIC_EXCEPTIONS
                try
                {
                    Console.WriteLine("READY TO INSERT TABLE BIOMETRIC_EXCEPTIONS");
                    connection.ConnectionString = dbcon221;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "INSERT INTO BIOMETRIC_EXCEPTIONS SELECT NIK, CREATED, CREATED_USERNAME, IMAGE_TYPE_CODE, EXCEPTION_REASON_CODE, EXCEPTION_PROOF, EXCEPTION_REMARKS, FINGER_EXCEPTION_MAP, IRIS_EXCEPTION_MAP, LAST_UPDATED, LAST_UPDATED_USERNAME FROM  BIOMETRIC_EXCEPTIONS@DB5 A WHERE NOT EXISTS (SELECT 1 FROM BIOMETRIC_EXCEPTIONS B WHERE A.NIK = B.NIK)";
                    Console.WriteLine("INSERT BIOMETRIC_EXCEPTIONS");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("15. Proccess Insert Table " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("15. " + ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
                #endregion

                #region BIOMETRIC_DIAGNOSTICS
                try
                {
                    Console.WriteLine("READY TO INSERT TABLE BIOMETRIC_DIAGNOSTICS");
                    connection.ConnectionString = dbcon221;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "INSERT INTO BIOMETRIC_DIAGNOSTICS SELECT ID, NIK, VERSION, CLIENT_VERSION, CREATED, CREATED_USERNAME, AGE, GENDER, OCCUPATION, RACE, FP_CAPTURE_SDK_VERSION, FP_TEMPLATE_SDK_VERSION, IRIS_CAPTURE_SDK_VERSION, IRIS_TEMPLATE_SDK_VERSION, FACE_CAPTURE_SDK_VERSION, FACE_TEMPLATE_SDK_VERSION, SIG_CAPTURE_SDK_VERSION, CLIENT_REGISTRATION_ID, TENPRINT_DEVICE, IRIS_DEVICE, FACE_DEVICE, SIGNATURE_DEVICE, FACE_CAPTURE_ATTEMPTED_COUNT, FACE_CAPTURE_DURATION, SIG_CAPTURE_ATTEMPTED_COUNT, SIG_CAPTURE_DURATION, FP_CAPTURE_ATTEMPTED_COUNT, FP_CAPTURE_DURATION, FP_VERIF_ATTEMPTED_COUNT, FP_VERIF_DURATION, IRIS_CAPTURE_ATTEMPTED_COUNT, IRIS_CAPTURE_DURATION, OPER_OVERRIDE_INDICATOR, OPER_FP_VERIF_ATTEMPTED_COUNT, OPER_FP_VERIF_DURATION, BIOMETRIC_CAPTURE_DURATION, RECEIPT_PRINTED_INDICATOR, ABORTED_INDICATOR, SPVSR_FP_VERIF_ATTEMPTED_COUNT, SPVSR_FP_VERIF_DURATION, PRIMARY_FINGER_POSITION, SECONDARY_FINGER_POSITION, EXCEPTION_REASON_CODE, ABORTED_REASON, SUPERVISOR_USERNAME FROM  BIOMETRIC_DIAGNOSTICS@DB5 A WHERE NOT EXISTS (SELECT 1 FROM BIOMETRIC_DIAGNOSTICS B WHERE A.ID = B.ID)";
                    Console.WriteLine("INSERT BIOMETRIC_DIAGNOSTICS");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("15. Proccess Insert Table " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("15. " + ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
                #endregion

                #region AUDITS
                try
                {
                    Console.WriteLine("READY TO INSERT TABLE AUDITS");
                    connection.ConnectionString = dbcon221;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "INSERT INTO AUDITS SELECT NIK ,VERSION ,TYPE ,LOG ,CREATED ,CREATED_USERNAME ,LAST_UPDATED ,LAST_UPDATED_USERNAME FROM  AUDITS@DB5 A WHERE NOT EXISTS (SELECT 1 FROM AUDITS B WHERE A.NIK = B.NIK)";
                    Console.WriteLine("INSERT AUDITS");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("15. Proccess Insert Table " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("15. " + ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
                #endregion

                #region DUPLICATE_RESULTS
                try
                {
                    Console.WriteLine("READY TO INSERT TABLE DUPLICATE_RESULTS");
                    connection.ConnectionString = dbcon221;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "INSERT INTO DUPLICATE_RESULTS SELECT NIK, DUPLICATE_NIK, SCORE, MANUAL_DEDUPLICATED_INDICATOR, CREATED, CREATED_USERNAME, LAST_UPDATED, LAST_UPDATED_USERNAME FROM  DUPLICATE_RESULTS@DB5 A WHERE NOT EXISTS (SELECT 1 FROM DUPLICATE_RESULTS B WHERE A.NIK = B.NIK)";
                    Console.WriteLine("INSERT DUPLICATE_RESULTS");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("15. Proccess Insert Table " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("15. " + ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
                #endregion

                #region CARD_ISSUANCE_EVIDENCE
                try
                {
                    Console.WriteLine("READY TO INSERT TABLE CARD_ISSUANCE_EVIDENCE");
                    connection.ConnectionString = dbcon221;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "INSERT INTO CARD_ISSUANCE_EVIDENCE SELECT NIK ,PRIMARY_POSITION ,PRIMARY ,SECONDARY_POSITION ,SECONDARY ,AMPUTY_EVIDENCE ,CREATED ,CREATED_USERNAME ,LAST_UPDATED ,LAST_UPDATED_USERNAME ,OPERATOR_FINGER FROM  CARD_ISSUANCE_EVIDENCE@DB5 A WHERE NOT EXISTS (SELECT 1 FROM CARD_ISSUANCE_EVIDENCE B WHERE A.NIK = B.NIK)";
                    Console.WriteLine("INSERT CARD_ISSUANCE_EVIDENCE");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("15. Proccess Insert Table " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("15. " + ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
                #endregion

                #region MIDDLEWARE_DIAGNOSTICS
                try
                {
                    Console.WriteLine("READY TO INSERT TABLE MIDDLEWARE_DIAGNOSTICS");
                    connection.ConnectionString = dbcon221;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "INSERT INTO MIDDLEWARE_DIAGNOSTICS SELECT ID, NIK, VERSION, ABIS_VERSION, CREATED, CREATED_USERNAME, TCN, PARSE_DURATION, INCOMING_RECORD_VERSION, FINGER_TEMPLATE_SDK_VERSION, IRIS_TEMPLATE_SDK_VERSION, FACE_TEMPLATE_SDK_VERSION, NUMBER_OF_FINGER_SAMPLES, FP_TEMPLATE_CREATED_COUNT, FP_TEMPLATE_CREATE_DURATION, NUMBER_OF_FACE_SAMPLES, FACE_TEMPLATE_CREATED_COUNT, FACE_TEMPLATE_CREATE_DURATION, NUMBER_OF_IRIS_SAMPLES, IRIS_TEMPLATE_CREATED_COUNT, IRIS_TEMPLATE_CREATE_DURATION, ABIS_INSERT_DURATION, ABIS_IDENTIFIY_DURATION, ABIS_RESULTS_COUNT, DATABASE_SIZE, TRANSACTION_SUCCESS_INDICATOR, SEARCH_ENGINE_EX_INDICATOR, RETRY_COUNT, STATUS_CODE, ABORTED_INDICATOR, DURATION, ENROLL_TO_DEDUP_DURATION, FP_TEMPLATE_FAILED_CREATE_POS, FP_QUALITY, FACE_TEMPLATE_FAIL_CREATE_POS, FACE_QUALITY, IRIS_TEMPLATE_FAIL_CREATE_POS, IRIS_QUALITY, DUPLICATE_NIK, ABORTED_REASON FROM  MIDDLEWARE_DIAGNOSTICS@DB5 A WHERE NOT EXISTS (SELECT 1 FROM MIDDLEWARE_DIAGNOSTICS B WHERE A.ID = B.ID)";
                    Console.WriteLine("INSERT MIDDLEWARE_DIAGNOSTICS");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("15. Proccess Insert Table " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("15. " + ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
                #endregion

                #region REPROCESS_FAILED_NIKS
                try
                {
                    Console.WriteLine("READY TO INSERT TABLE REPROCESS_FAILED_NIKS");
                    connection.ConnectionString = dbcon221;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "INSERT INTO REPROCESS_FAILED_NIKS SELECT NIK, STATUS_STRING, REASON_CODE, CREATE_DATE, RETRY_COUNT FROM  REPROCESS_FAILED_NIKS@DB5 A WHERE NOT EXISTS (SELECT 1 FROM REPROCESS_FAILED_NIKS B WHERE A.NIK = B.NIK)";
                    Console.WriteLine("INSERT REPROCESS_FAILED_NIKS");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("15. Proccess Insert Table " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("15. " + ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
                #endregion

                #region MANUAL_DEDUP_DIAGNOSTICS
                try
                {
                    Console.WriteLine("READY TO INSERT TABLE MANUAL_DEDUP_DIAGNOSTICS");
                    connection.ConnectionString = dbcon221;
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    command.CommandText = "INSERT INTO MANUAL_DEDUP_DIAGNOSTICS SELECT ID, NIK, VERSION, CREATED, CREATED_USERNAME, DUPLICATE_NIK, MODALITY_INSPECTED, DURATION, DUPLICATE_INDICATOR, DUPLICATE_RANK, INSPECTED_USERNAME, SUPERVISOR_REQUIRED_INDICATOR, DECISION_FACTOR FROM  MANUAL_DEDUP_DIAGNOSTICS@DB5 A WHERE NOT EXISTS (SELECT 1 FROM MANUAL_DEDUP_DIAGNOSTICS B WHERE A.NIK = B.NIK)";
                    Console.WriteLine("INSERT MANUAL_DEDUP_DIAGNOSTICS");
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    this.WriteToFile("15. Proccess Insert Table " + " {0}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    this.WriteToFile("15. " + ex.Message + " {0}");
                }
                finally
                {
                    connection.Close();
                }
                #endregion
            }
        }
        #endregion



        private void WriteToFile(string text)
        {
            try
            {
                Directory.CreateDirectory(app_path + "\\logs\\" + DateTime.Now.ToString("yyyyMM") + "\\" + DateTime.Now.ToString("yyyyMMdd") + "\\");
                string path = app_path + "\\logs\\" + DateTime.Now.ToString("yyyyMM") + "\\" + DateTime.Now.ToString("yyyyMMdd") + "\\ProcessLog_" + DateTime.Now.ToString("yyyyMMdd") + ".txt";
                using (StreamWriter writer = new StreamWriter(path, true))
                {
                    writer.WriteLine(string.Format(text, DateTime.Now.ToString("yyyy-MM-dd H:mm:ss fffffff")));
                    writer.WriteLine(string.Format(""));
                    writer.Close();
                }
            }
            catch (Exception e)
            {

            }

        }
        private void ReadSystemFile(string text)
        {
            try
            {
                Directory.CreateDirectory(app_path + "\\logs\\" + DateTime.Now.ToString("yyyyMM") + "\\" + DateTime.Now.ToString("yyyyMMdd") + "\\");
                string path = app_path + "\\logs\\" + DateTime.Now.ToString("yyyyMM") + "\\" + DateTime.Now.ToString("yyyyMMdd") + "\\StatusLog_" + DateTime.Now.ToString("yyyyMMdd") + ".txt";
                using (StreamWriter writer = new StreamWriter(path, true))
                {
                    writer.WriteLine(string.Format(text, DateTime.Now.ToString("yyyy-MM-dd H:mm:ss fffffff")));
                    writer.WriteLine(string.Format(""));
                    writer.Close();
                }
            }
            catch (Exception e)
            {

            }

        }
    }
}
