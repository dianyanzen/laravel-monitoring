﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace yz_service_ektp
{
    public partial class do_check : ServiceBase
    {
        private System.Threading.Timer IntervalTimer;
        private volatile Boolean _AvoidDoubleWork;
        private string myDt = DateTime.Now.ToString("yyMMddHHmmss");
        private string app_path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location.ToString());
        //TimeSpan start = new TimeSpan(17, 0, 0); //10 o'clock
        //TimeSpan end = new TimeSpan(23, 59, 0); //12 o'clock
        //TimeSpan start2 = new TimeSpan(01, 0, 0); //10 o'clock
        //TimeSpan end2 = new TimeSpan(05, 0, 0); //12 o'clock
        //TimeSpan now = DateTime.Now.TimeOfDay;
        public do_check()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                myDt = DateTime.Now.ToString("yyMMddHHmmss");
                this._AvoidDoubleWork = false;
                int hours = Convert.ToInt32(0);
                int minutes = Convert.ToInt32(0);
                int seconds = yz_service_ektp.Properties.Settings.Default.timing;
                TimeSpan tsInterval = new TimeSpan(hours, minutes, seconds);
                IntervalTimer = new System.Threading.Timer(
                    new System.Threading.TimerCallback(IntervalTimer_Elapsed)
                    , null, tsInterval, tsInterval);
                this.WriteToFile("Starting Service {0}");
            }
            catch (Exception ex)
            {
                this.WriteToFile(ex.Message + " {0}");
            }
        }

        protected override void OnStop()
        {
            try
            {
                this.WriteToFile("Stoping Service {0}");
                IntervalTimer.Change(System.Threading.Timeout.Infinite, System.Threading.Timeout.Infinite);
                IntervalTimer.Dispose();
                IntervalTimer = null;
            }
            catch (Exception ex)
            {
                this.WriteToFile(ex.Message + " {0}");
            }
        }
        private void IntervalTimer_Elapsed(object state)
        {
            try
            {
                string myFolder = DateTime.Now.ToString("yyMMdd");
                string myDt = DateTime.Now.ToString("yyMMddHHmmss");
                if (!_AvoidDoubleWork)
                {
                    //if ((now > start) && (now < end))
                    //{
                        _AvoidDoubleWork = true;
                        yz_service_ektp.cls.Batch Check = new yz_service_ektp.cls.Batch();
                        Check.process();
                        _AvoidDoubleWork = false;
                    //}
                    //else if ((now > start2) && (now < end2))
                    //{
                    //    _AvoidDoubleWork = true;
                    //    yz_service_ektp.cls.Batch Check = new yz_service_ektp.cls.Batch();
                    //    Check.process();
                    //    _AvoidDoubleWork = false;
                    //}

                }
            }
            catch (Exception e)
            {
                this.WriteToFile(e.Message + " {0}");
            }

        }


        private void WriteToFile(string text)
        {
            try
            {
                Directory.CreateDirectory(app_path + "\\logs\\" + DateTime.Now.ToString("yyyyMM") + "\\" + DateTime.Now.ToString("yyyyMMdd") + "\\");
                string path = app_path + "\\logs\\" + DateTime.Now.ToString("yyyyMM") + "\\" + DateTime.Now.ToString("yyyyMMdd") + "\\CheckLog_" + DateTime.Now.ToString("yyyyMMdd") + ".txt";
                using (StreamWriter writer = new StreamWriter(path, true))
                {
                    writer.WriteLine(string.Format(text, DateTime.Now.ToString("yyyy-MM-dd H:mm:ss fffffff")));
                    writer.WriteLine(string.Format(""));
                    writer.Close();
                }
            }
            catch (Exception e)
            {

            }

        }
    }
}
